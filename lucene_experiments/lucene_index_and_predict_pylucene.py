import lucene
from org.apache.lucene import analysis, document, index, queryparser, search, store, util
from org.apache.lucene.search import similarities
from lupyne import engine
import os
import os.path as osp
import time
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from textblob import TextBlob
import sys

CURRENT_FILE_PATH = osp.dirname(osp.realpath(__file__))

DATA_PATH = osp.join(CURRENT_FILE_PATH, '../data')
DOCS_DIR = osp.join(DATA_PATH, 'new_lucene_index')
VAL_SET_PATH = osp.join(DATA_PATH, 'validation_set.tsv')
TRAIN_SET_PATH = osp.join(DATA_PATH, 'training_set.tsv')
ck_keywords_path = 'ck12_keywords.txt'
keywords_themes = ["biology.txt", "chemistry.txt", "earth-science.txt", "physics.txt"]
stemmer = SnowballStemmer('english')
keywords_by_theme = {}

def prepare_index(DOCS_DIR):
    indexers={}
    # Store the index in memory:
    analyzer = analysis.standard.StandardAnalyzer(util.Version.LUCENE_CURRENT)
    ram_dir = store.RAMDirectory()
    config = index.IndexWriterConfig(util.Version.LUCENE_CURRENT, analyzer)
    sim1 = similarities.DefaultSimilarity()
    sim2 = similarities.BM25Similarity()
    similarity  = similarities.MultiSimilarity([sim1, sim2])
    config.setSimilarity(similarity)
    writer = index.IndexWriter(ram_dir, config)
    #writer.setSimilarity(similarity)
    indexed_documents_count =0
    index_start = time.clock()
    for root, dirs, files in os.walk(DOCS_DIR):
        for f in files:
            f_path = osp.join(root, f)
            with open(f_path, 'r') as doc:
                content = doc.readlines()
                #content = ' '.join([x.strip() for x in content])  # concatenate whole file into one string
                for article in content:
                    if len(article.split()) >= 8:
                        article = stem_sentence_and_remove_stopwords(article)
                        doc = document.Document()
                        field_path = document.Field("path", f_path, document.TextField.TYPE_STORED)
                        doc.add(field_path)
                        field_content = document.Field("contents", article, document.TextField.TYPE_STORED)
                        doc.add(field_content)
                        writer.addDocument(doc)
                        indexed_documents_count += 1
    writer.commit()  # commit changes and refresh searcher
    writer.close()
    print 'Added', indexed_documents_count, 'documents to index, spent', time.clock() - index_start, 'sec'
    return ram_dir


def prepare_searcher(ram_dir):
    sim1 = similarities.DefaultSimilarity()
    sim2 = similarities.BM25Similarity()
    similarity  = similarities.MultiSimilarity([sim1, sim2])
    reader = index.IndexReader.open(ram_dir)
    searcher = search.IndexSearcher(reader)
    searcher.setSimilarity(similarity)
    return searcher, reader

def get_theme(article):
    max_words_count = 0
    max_theme = "default"
    count_list = []
    #return max_theme
    for current_theme, current_keywords in keywords_by_theme.iteritems():
        words_count = 0
        for word in article.split():
            if word.lower() in current_keywords:
                words_count += 1
        count_list.append(words_count)
        sorted(count_list, reverse=True)
        if words_count > max_words_count:
            max_words_count = words_count
            max_theme = current_theme
    if float(max_words_count) / len(article.split()) > 0.08:
        return max_theme
    elif max_words_count - count_list[1] > 2:
        return max_theme
    else:
        return "default"

def stem_sentence_and_remove_stopwords(sentence):
    result = ""
    sentence = ''.join(ch for ch in sentence if ch.isalnum() or ch.isspace() or ch == '.,-')
    for x in sentence.split():
        x = x.lower()
        if x not in stop:
            x = stemmer.stem(x)
            result += x + " "
    return result


def removeDuplicateWords(q):
    q = q.lower()
    s = q.split(' ')
    r = []
    for w in s:
        if w not in r:
            r.append(w)
    return ' '.join(r)

def remove_stopword(sentence):
    sentence_without_stop = ""
    for x in sentence.split():
        if x not in stop:
            sentence_without_stop += x + " "
    return  sentence_without_stop.strip()

def stem_sentence_and_remove_stopwords_boost(sentence):
    result = ""
    sentence = unicode(sentence, errors='ignore')
    blob = TextBlob(sentence)
    keywords = blob.noun_phrases
    sentence = ''.join(ch for ch in sentence if ch.isalnum() or ch.isspace() or ch == '.,-')
    sentence = sentence.lower()
    sentence = removeDuplicateWords(sentence)
    sentence = remove_stopword(sentence)
    setOfKeywords = set(keywords)
    allkeywords = set()
    for keyword in setOfKeywords:
        for word in keyword.split():
            allkeywords.add(word)
    if len(sentence.split()) <= 2 and len(allkeywords) == 0:
        for x in sentence.split():
            x = x.lower()
            if x not in stop:
                x = stemmer.stem(x)
                result += x + " "
        result = "\"" + result + "\"^1.4"
        return  result
    for word_to_replace in sentence.split():
        found=False
        for keyword in allkeywords:
            for word in keyword.split():
                if word_to_replace==word:
                    result = result + " " + stemmer.stem(word_to_replace) + "^1.4"
                    found = True
                    break
        if not found:
            result = result + " " + stemmer.stem(word_to_replace)
    return result

def stem_sentence_and_remove_stopwords_ck12_keywords(sentence, is_question):
    result = ""
    sentence = unicode(sentence, errors='ignore')
    blob = TextBlob(sentence)
    blob_keywords = blob.noun_phrases
    sentence = ''.join(ch for ch in sentence if ch.isalnum() or ch.isspace() or ch == '.,-')
    sentence = sentence.lower()
    sentence = removeDuplicateWords(sentence)

    allkeywords = set()
    setOfKeywords = set(blob_keywords)
    for keyword in setOfKeywords:
        for word in keyword.split():
            word = word.lower()
            if word not in stop:
                word = stemmer.stem(word)
                allkeywords.add(word)

    for x in sentence.split():
        x = x.lower()
        if x not in stop:
            x = stemmer.stem(x)
            result += x + " "
    keywords=[]
    ck_keywords.union(allkeywords)
    for keyword in ck_keywords:
        if len(keyword.split())>1 and keyword in result:
            keywords.append(keyword)
        else:
            if keyword in result.split():
                keywords.append(keyword)
    keywords_duplicate =[]
    for keyword in keywords:
        for keyword_comp in keywords:
            if keyword != keyword_comp and keyword in keyword_comp:
                keywords_duplicate.append(keyword)
                break
    for keyword in keywords_duplicate:
        keywords.remove(keyword)
    for keyword in keywords:
        if is_question:
            for word in keyword.split():
                if len(word) > 1:
                    result = result.replace(word, "\""+word+"\"^1.2")
        else:
            for word in keyword.split():
                if len(word) > 1:
                    result = result.replace(word, "\""+word+"\"^1.2")
    return result

def prepare_sentence(sentence,is_question):
    sentence = stem_sentence_and_remove_stopwords(sentence)
    return sentence


def predict_answer(searcher, question, answerA, answerB, answerC, answerD, verbose=True):
    question = prepare_sentence(question, True)
    answerA = prepare_sentence(answerA, False)
    answerB = prepare_sentence(answerB, False)
    answerC = prepare_sentence(answerC, False)
    answerD = prepare_sentence(answerD, False)
    analyzer = analysis.standard.StandardAnalyzer(util.Version.LUCENE_CURRENT)

    queryA = question + ' ' + answerA
    queryB = question + ' ' + answerB
    queryC = question + ' ' + answerC
    queryD = question + ' ' + answerD

    parser = queryparser.classic.QueryParser(util.Version.LUCENE_CURRENT, "contents", analyzer)
    queryLuceneA = parser.parse(queryA)
    queryLuceneB = parser.parse(queryB)
    queryLuceneC = parser.parse(queryC)
    queryLuceneD = parser.parse(queryD)


    hitsA = searcher.search(queryLuceneA, None, 10).scoreDocs  # parsing handled if necessary
    hitsB = searcher.search(queryLuceneB, None, 10).scoreDocs  # parsing handled if necessary
    hitsC = searcher.search(queryLuceneC, None, 10).scoreDocs  # parsing handled if necessary
    hitsD = searcher.search(queryLuceneD, None, 10).scoreDocs  # parsing handled if necessary

    scoreA = get_score_from_existing(searcher, hitsA, question, answerA)
    scoreB = get_score_from_existing(searcher, hitsB, question, answerB)
    scoreC = get_score_from_existing(searcher, hitsC, question, answerC)
    scoreD = get_score_from_existing(searcher, hitsD, question, answerD)
    # scoreA = get_total_score(hitsA)
    # scoreB = get_total_score(hitsB)
    # scoreC = get_total_score(hitsC)
    # scoreD = get_total_score(hitsD)

    max_score = max(scoreA, scoreB, scoreC, scoreD)
    if max_score == scoreA:
        ans = 'A'
    elif max_score == scoreB:
        ans = 'B'
    elif max_score == scoreC:
        ans = 'C'
    else:
        ans = 'D'

    return ans, scoreA, scoreB, scoreC, scoreD, len(answerA.split()), len(answerB.split()), len(answerC.split()), len(answerD.split()), len(question.split())

def get_total_score(hits):
    score = 0
    for hit in hits:
        score += hit.score
    return score

def get_score(hits):
    if hits:
        return hits[0].score
    else:
        return 0


def get_score_from_existing(searcher, hits, question, answer, index = 0, layer = 0):
    if layer == 10 or index >= len(hits):
        return 0
    while index < len(hits):
        question_exist = False
        for word in question.split():
            hit_doc = searcher.doc(hits[index].doc)
            text = hit_doc["contents"]
            if word in text.split():
                question_exist = True
                break
        if len(question.split()) ==0:
            question_exist =True
        answer_exist = False
        for word in answer.split():
            hit_doc = searcher.doc(hits[index].doc)
            text = hit_doc["contents"]
            if word in text.split():
                answer_exist = True
                break
        if len(answer.split())==0:
            answer_exist=True
        if question_exist and answer_exist:
            return hits[index].score + get_score_from_existing(searcher, hits, question, answer, index +1, layer + 1)
        else:
            return 0 + get_score_from_existing(searcher, hits, question, answer, index + 1, layer)

def get_score_from_query(hits, query):
    if hits:
        path = hits[0]['path']
        return get_answer_from_answer_page(path, query)
    else:
        return 0


def get_answer_from_question_page(pathQ, queryA, queryB, queryC, queryD):
    indexerQ = engine.Indexer()  # Indexer combines Writer and Searcher; RAMDirectory and StandardAnalyzer are defaults
    indexerQ.set('contents', stored=True)  # settings for all documents of indexer; indexed and tokenized is the default
    indexerQ.set('path', stored=True)
    with open(pathQ, 'r') as doc:
        content = doc.readlines()
        # content = ' '.join([x.strip() for x in content])  # concatenate whole file into one string
        # content = stem_sentence_and_remove_stopwords(content)
        for x in content:
            indexer.add(path=pathQ, contents=x)  # add document
    hitsA = indexer.search(queryA, count=100, scores=True, field='contents')  # parsing handled if necessary
    hitsB = indexer.search(queryB, count=100, scores=True, field='contents')  # parsing handled if necessary
    hitsC = indexer.search(queryC, count=100, scores=True, field='contents')  # parsing handled if necessary
    hitsD = indexer.search(queryD, count=100, scores=True, field='contents')  # parsing handled if necessary

    max_score = max(hitsA[0].score, hitsB[0].score, hitsC[0].score, hitsD[0].score)
    if max_score == hitsA[0].score:
        ans = 'A'
    elif max_score == hitsB[0].score:
        ans = 'B'
    elif max_score == hitsC[0].score:
        ans = 'C'
    else:
        ans = 'D'
    return ans, max_score

#0.4792

def get_answer_from_answer_page(path, query):
    indexerQ = engine.Indexer()  # Indexer combines Writer and Searcher; RAMDirectory and StandardAnalyzer are defaults
    indexerQ.set('contents', stored=True)  # settings for all documents of indexer; indexed and tokenized is the default
    indexerQ.set('path', stored=True)
    with open(path, 'r') as doc:
        content = doc.readlines()
        # content = ' '.join([x.strip() for x in content])  # concatenate whole file into one string
        # content = stem_sentence_and_remove_stopwords(content)
        for x in content:
            indexer.add(path=path, contents=x)  # add document
    hits = indexer.search(query, count=100, scores=True, field='contents')  # parsing handled if necessary
    return hits[0].score



def get_accuracy_on_train_set(searcher):
    true_answers = []
    predicted_answers = []
    with open('words_aq_count.csv', 'w') as words_aq_count:
        words_aq_count.write('id,lucene_score_A,lucene_score_B,lucene_score_C,lucene_score_D\n')
        with open('words_count.csv', 'w') as words_count:
            words_count.write('id,lucene_score_A,lucene_score_B,lucene_score_C,lucene_score_D\n')
            with open('lucene_scores.csv', 'w') as lucene_scores:
                lucene_scores.write('id,lucene_score_A,lucene_score_B,lucene_score_C,lucene_score_D\n')
                with open(TRAIN_SET_PATH, 'r') as train_file:
                    train_questions = [x.strip() for x in train_file.readlines()]
                    with open('resultTrain.csv', 'w') as result_file:
                        for s in train_questions[1:]:
                            id, question, right_answer, answerA, answerB, answerC, answerD = s.split('\t')
                            true_answers.append(right_answer)
                            ans, scoreA, scoreB, scoreC, scoreD, ansA, ansB, ansC, ansD, questionCount = predict_answer(searcher, question, answerA, answerB, answerC,answerD)
                            if ans != right_answer:
                                result_file.write(" ".join(
                                    [question, "\n", right_answer, ans, "\n", answerA, str(scoreA), "\n", answerB, str(scoreB), "\n",
                                     answerC, str(scoreC), "\n", answerD, str(scoreD)]) + "\n")
                            lucene_scores.write('{0},{1},{2},{3},{4}\n'.format(id, scoreA, scoreB, scoreC, scoreD))
                            words_count.write('{0},{1},{2},{3},{4}\n'.format(id, ansA, ansB, ansC, ansD))
                            words_aq_count.write('{0},{1},{2},{3},{4}\n'.format(id, ansA + questionCount, ansB+ questionCount, ansC+ questionCount, ansD+ questionCount))
                            predicted_answers.append(ans)

    count_of_right_answers = 0
    for i in xrange(0, len(true_answers)):
        if predicted_answers[i] == true_answers[i]:
            count_of_right_answers += 1
    acc = float(count_of_right_answers) / float(len(true_answers))
    return acc


def get_submission_from_val(searcher):
    with open('val_words_aq_count.csv', 'w') as words_aq_count:
        words_aq_count.write('id,lucene_score_A,lucene_score_B,lucene_score_C,lucene_score_D\n')
        with open('val_words_count.csv', 'w') as words_count:
            words_count.write('id,lucene_score_A,lucene_score_B,lucene_score_C,lucene_score_D\n')
            with open('val_lucene_scores.csv', 'w') as lucene_scores:
                lucene_scores.write('id,lucene_score_A,lucene_score_B,lucene_score_C,lucene_score_D\n')
                with open('result.csv', 'w') as result_file:
                    result_file.write('id,correctAnswer\n')
                    with open(VAL_SET_PATH, 'r') as val_file:
                        val_questions = [x.strip() for x in val_file.readlines()]

                        for s in val_questions[1:]:
                            id, question, answerA, answerB, answerC, answerD = s.split('\t')

                            ans, scoreA, scoreB, scoreC, scoreD, ansA, ansB, ansC, ansD, questionCount = predict_answer(searcher, question, answerA, answerB, answerC, answerD)
                            lucene_scores.write('{0},{1},{2},{3},{4}\n'.format(id, scoreA, scoreB, scoreC, scoreD))
                            words_count.write('{0},{1},{2},{3},{4}\n'.format(id, ansA, ansB, ansC, ansD))
                            words_aq_count.write('{0},{1},{2},{3},{4}\n'.format(id, ansA + questionCount, ansB+ questionCount, ansC+ questionCount, ansD+ questionCount))
                            result_file.write(id + ',' + ans + '\n')


if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding('utf8')
    stop = set(stopwords.words('english'))
    ck_keywords_list = []

    with open('../data/SMART_stopwords.txt', 'r') as smart_stopwwods_file:
       stopwords = set([x.strip() for x in smart_stopwwods_file.readlines() if len(x) > 1])
       stop = stop.union(stopwords)

    lucene.initVM()
    ram_dir = prepare_index(DOCS_DIR)
    searcher, reader = prepare_searcher(ram_dir)
    get_submission_from_val(searcher)
    print 'Accuracy on train set:', get_accuracy_on_train_set(searcher)
    ram_dir.close()
    reader.close()
