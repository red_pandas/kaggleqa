import lucene
from lupyne import engine
import os
import os.path as osp
import time
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from textblob import TextBlob
import sys

CURRENT_FILE_PATH = osp.dirname(osp.realpath(__file__))

DATA_PATH = osp.join(CURRENT_FILE_PATH, '../data')
DOCS_DIR = osp.join(DATA_PATH, 'new_lucene_index')
VAL_SET_PATH = osp.join(DATA_PATH, 'test_set.tsv')
TRAIN_SET_PATH = osp.join(DATA_PATH, 'training_set.tsv')
ck_keywords_path = 'ck12_keywords.txt'
keywords_themes = ["biology.txt", "chemistry.txt", "earth-science.txt", "physics.txt"]
stemmer = SnowballStemmer('english')
keywords_by_theme = {}

def prepare_index(DOCS_DIR, stemmer):
    indexers={}
    # Store the index in memory:
    for theme in keywords_themes:
        indexer = engine.Indexer()  # Indexer combines Writer and Searcher; RAMDirectory and StandardAnalyzer are defaults
        indexer.set('contents', stored=True)  # settings for all documents of indexer; indexed and tokenized is the default
        indexer.set('path', stored=True)
        indexers[theme] = indexer
    indexer = engine.Indexer()  # Indexer combines Writer and Searcher; RAMDirectory and StandardAnalyzer are defaults
    indexer.set('contents', stored=True)  # settings for all documents of indexer; indexed and tokenized is the default
    indexer.set('path', stored=True)
    indexers['default'] = indexer
    index_start = time.clock()
    indexed_documents_count = 0
    for root, dirs, files in os.walk(DOCS_DIR):
        for f in files:
            f_path = osp.join(root, f)
            with open(f_path, 'r') as doc:
                content = doc.readlines()
                document = ' '.join([x.strip() for x in content])  # concatenate whole file into one string
                article_theme = get_theme(document)
                for article in content:
                    if len(article.split()) >= 8:
                        #article_theme = get_theme(article)
                        #print article_theme + " " + str(article_len) + " " + " ".join([str(x) for x in count_list ])
                        article = stem_sentence_and_remove_stopwords(article)
                        if article_theme !="default":
                            indexers[article_theme].add(path=f_path, contents=article)  # add document
                        indexers["default"].add(path=f_path, contents=article)  # add document
                        indexed_documents_count += 1
    for indexer in indexers.values():
        indexer.commit()  # commit changes and refresh searcher
    print 'Added', indexed_documents_count, 'documents to index, spent', time.clock() - index_start, 'sec'
    return indexers

def get_theme(article):
    max_words_count = 0
    max_theme = "default"
    count_list = []
    #return max_theme
    if len(article.split()) >= 8:
        for current_theme, current_keywords in keywords_by_theme.iteritems():
            words_count = 0
            for word in article.split():
                if word.lower() in current_keywords:
                    words_count += 1
            count_list.append(words_count)
            sorted(count_list, reverse=True)
            if words_count > max_words_count:
                max_words_count = words_count
                max_theme = current_theme
        if float(max_words_count) / len(article.split()) > 0.08:
            return max_theme
        elif max_words_count - count_list[1] > 2:
            return max_theme
        else:
            return "default"
    else:
            return "default"

def get_theme_q_a(article):
    max_words_count = 0
    sec_max_count = 0
    sec_max_theme= "default"
    max_theme = "default"
    count_list = []
    #return max_theme
    if len(article.split()) >= 8:
        for current_theme, current_keywords in keywords_by_theme.iteritems():
            words_count = 0
            for word in article.split():
                if word.lower() in current_keywords:
                    words_count += 1
            count_list.append(words_count)
            sorted(count_list, reverse=True)
            if words_count > max_words_count:
                max_words_count = words_count
                max_theme = current_theme
            elif words_count > sec_max_count:
                sec_max_count = words_count
                sec_max_theme = current_theme
        if float(max_words_count) / len(article.split()) > 0.05:
            if sec_max_count > 0:
                return [max_theme,sec_max_theme]
            else:
                return [max_theme]
        elif max_words_count - count_list[2] > 2:
            if sec_max_count > 0:
                return [max_theme,sec_max_theme]
            else:
                return [max_theme]
        else:
            return ["default"]
    else:
        return ["default"]

def stem_sentence_and_remove_stopwords(sentence):
    result = ""
    sentence = ''.join(ch for ch in sentence if ch.isalnum() or ch.isspace() or ch == '.,-')
    for x in sentence.split():
        x = x.lower()
        if x not in stop:
            x = stemmer.stem(x)
            result += x + " "
    return result


def removeDuplicateWords(q):
    q = q.lower()
    s = q.split(' ')
    r = []
    for w in s:
        if w not in r:
            r.append(w)
    return ' '.join(r)

def remove_stopword(sentence):
    sentence_without_stop = ""
    for x in sentence.split():
        if x not in stop:
            sentence_without_stop += x + " "
    return  sentence_without_stop.strip()

def prepare_sentence(sentence,is_question):
    sentence = stem_sentence_and_remove_stopwords(sentence)
    return sentence

def predict_answer(indexer, question, answerA, answerB, answerC, answerD, verbose=True):
    question = prepare_sentence(question, True)
    answerA = prepare_sentence(answerA, False)
    answerB = prepare_sentence(answerB, False)
    answerC = prepare_sentence(answerC, False)
    answerD = prepare_sentence(answerD, False)

    queryA = question + ' ' + answerA
    queryB = question + ' ' + answerB
    queryC = question + ' ' + answerC
    queryD = question + ' ' + answerD
    themes = get_theme_q_a(question + " " + answerA + " " + answerB + " " + answerC + " "+ answerD)

    hitsA = []
    hitsB = []
    hitsC = []
    hitsD = []
    for  theme in themes:
        hitsA.append(indexer[theme].search(queryA, count=100, scores=True, field='contents') ) # parsing handled if necessary
        hitsB.append(indexer[theme].search(queryB, count=100, scores=True, field='contents'))  # parsing handled if necessary
        hitsC.append(indexer[theme].search(queryC, count=100, scores=True, field='contents') ) # parsing handled if necessary
        hitsD.append(indexer[theme].search(queryD, count=100, scores=True, field='contents'))  # parsing handled if necessary

    hitsA_default = indexer["default"].search(queryA, count=100, scores=True, field='contents')  # parsing handled if necessary
    hitsB_default = indexer["default"].search(queryB, count=100, scores=True, field='contents')  # parsing handled if necessary
    hitsC_default = indexer["default"].search(queryC, count=100, scores=True, field='contents')  # parsing handled if necessary
    hitsD_default = indexer["default"].search(queryD, count=100, scores=True, field='contents')  # parsing handled if necessary

    scoresA = []
    scoresB = []
    scoresC = []
    scoresD = []

    for i in range(0, len(hitsA)):
        scoresA.append(get_score_from_existing(hitsA[i], question, answerA))
        scoresB.append(get_score_from_existing(hitsB[i], question, answerB))
        scoresC.append(get_score_from_existing(hitsC[i], question, answerC))
        scoresD.append(get_score_from_existing(hitsD[i], question, answerD))

    scoreA_default = get_score_from_existing(hitsA_default, question, answerA)
    scoreB_default = get_score_from_existing(hitsB_default, question, answerB)
    scoreC_default = get_score_from_existing(hitsC_default, question, answerC)
    scoreD_default = get_score_from_existing(hitsD_default, question, answerD)

    scoreA = max(scoresA) if max(scoresA) > scoreA_default else scoreA_default
    scoreB = max(scoresB) if max(scoresB) > scoreB_default else scoreB_default
    scoreC = max(scoresC) if max(scoresC) > scoreC_default else scoreC_default
    scoreD = max(scoresD) if max(scoresD) > scoreD_default else scoreD_default

    max_score = max(scoreA, scoreB, scoreC, scoreD)
    if max_score == scoreA:
        ans = 'A'
    elif max_score == scoreB:
        ans = 'B'
    elif max_score == scoreC:
        ans = 'C'
    else:
        ans = 'D'

    return ans, scoreA, scoreB, scoreC, scoreD, len(answerA.split()), len(answerB.split()), len(answerC.split()), len(answerD.split()), len(question.split())


def get_score(hits):
    if hits:
        return hits[0].score
    else:
        return 0


def get_score_from_existing(hits, question, answer, index = 0, layer = 0):
    if layer == 10 or index >= len(hits):
        return 0
    if hits:
        while index < len(hits):
            question_exist = False
            for word in question.split():
                if word in hits[index]['contents'].split():
                    question_exist = True
                    break
            if len(question.split()) ==0:
                question_exist =True
            answer_exist = False
            for word in answer.split():
                if word in hits[index]['contents'].split():
                    answer_exist = True
                    break
            if len(answer.split())==0:
                answer_exist=True
            if question_exist and answer_exist:
                return hits[index].score + get_score_from_existing(hits, question, answer, index + 1, layer + 1)
            else:
                index += 1
        return 0 + get_score_from_existing(hits, question, answer, index + 1, layer + 1)
    return 0

def get_score_from_query(hits, query):
    if hits:
        path = hits[0]['path']
        return get_answer_from_answer_page(path, query)
    else:
        return 0


def get_answer_from_question_page(pathQ, queryA, queryB, queryC, queryD):
    indexerQ = engine.Indexer()  # Indexer combines Writer and Searcher; RAMDirectory and StandardAnalyzer are defaults
    indexerQ.set('contents', stored=True)  # settings for all documents of indexer; indexed and tokenized is the default
    indexerQ.set('path', stored=True)
    with open(pathQ, 'r') as doc:
        content = doc.readlines()
        # content = ' '.join([x.strip() for x in content])  # concatenate whole file into one string
        # content = stem_sentence_and_remove_stopwords(content)
        for x in content:
            indexer.add(path=pathQ, contents=x)  # add document
    hitsA = indexer.search(queryA, count=100, scores=True, field='contents')  # parsing handled if necessary
    hitsB = indexer.search(queryB, count=100, scores=True, field='contents')  # parsing handled if necessary
    hitsC = indexer.search(queryC, count=100, scores=True, field='contents')  # parsing handled if necessary
    hitsD = indexer.search(queryD, count=100, scores=True, field='contents')  # parsing handled if necessary

    max_score = max(hitsA[0].score, hitsB[0].score, hitsC[0].score, hitsD[0].score)
    if max_score == hitsA[0].score:
        ans = 'A'
    elif max_score == hitsB[0].score:
        ans = 'B'
    elif max_score == hitsC[0].score:
        ans = 'C'
    else:
        ans = 'D'
    return ans, max_score

#0.4792

def get_answer_from_answer_page(path, query):
    indexerQ = engine.Indexer()  # Indexer combines Writer and Searcher; RAMDirectory and StandardAnalyzer are defaults
    indexerQ.set('contents', stored=True)  # settings for all documents of indexer; indexed and tokenized is the default
    indexerQ.set('path', stored=True)
    with open(path, 'r') as doc:
        content = doc.readlines()
        # content = ' '.join([x.strip() for x in content])  # concatenate whole file into one string
        # content = stem_sentence_and_remove_stopwords(content)
        for x in content:
            indexer.add(path=path, contents=x)  # add document
    hits = indexer.search(query, count=100, scores=True, field='contents')  # parsing handled if necessary
    return hits[0].score



def get_accuracy_on_train_set(indexer):
    true_answers = []
    predicted_answers = []
    with open('words_aq_count.csv', 'w') as words_aq_count:
        words_aq_count.write('id,lucene_score_A,lucene_score_B,lucene_score_C,lucene_score_D\n')
        with open('words_count.csv', 'w') as words_count:
            words_count.write('id,lucene_score_A,lucene_score_B,lucene_score_C,lucene_score_D\n')
            with open('lucene_scores.csv', 'w') as lucene_scores:
                lucene_scores.write('id,lucene_score_A,lucene_score_B,lucene_score_C,lucene_score_D\n')
                with open(TRAIN_SET_PATH, 'r') as train_file:
                    train_questions = [x.strip() for x in train_file.readlines()]
                    with open('resultTrain.csv', 'w') as result_file:
                        for s in train_questions[1:]:
                            id, question, right_answer, answerA, answerB, answerC, answerD = s.split('\t')
                            true_answers.append(right_answer)
                            ans, scoreA, scoreB, scoreC, scoreD, ansA, ansB, ansC, ansD, questionCount = predict_answer(indexer, question, answerA, answerB, answerC,answerD)
                            if ans != right_answer:
                                result_file.write(" ".join(
                                    [question, "\n", right_answer, ans, "\n", answerA, str(scoreA), "\n", answerB, str(scoreB), "\n",
                                     answerC, str(scoreC), "\n", answerD, str(scoreD)]) + "\n")
                            lucene_scores.write('{0},{1},{2},{3},{4}\n'.format(id, scoreA, scoreB, scoreC, scoreD))
                            words_count.write('{0},{1},{2},{3},{4}\n'.format(id, ansA, ansB, ansC, ansD))
                            words_aq_count.write('{0},{1},{2},{3},{4}\n'.format(id, ansA + questionCount, ansB+ questionCount, ansC+ questionCount, ansD+ questionCount))
                            predicted_answers.append(ans)

    count_of_right_answers = 0
    for i in xrange(0, len(true_answers)):
        if predicted_answers[i] == true_answers[i]:
            count_of_right_answers += 1
    acc = float(count_of_right_answers) / float(len(true_answers))
    return acc


def get_submission_from_val(indexer):
    with open('val_words_aq_count.csv', 'w') as words_aq_count:
        words_aq_count.write('id,lucene_score_A,lucene_score_B,lucene_score_C,lucene_score_D\n')
        with open('val_words_count.csv', 'w') as words_count:
            words_count.write('id,lucene_score_A,lucene_score_B,lucene_score_C,lucene_score_D\n')
            with open('val_lucene_scores.csv', 'w') as lucene_scores:
                lucene_scores.write('id,lucene_score_A,lucene_score_B,lucene_score_C,lucene_score_D\n')
                with open('result.csv', 'w') as result_file:
                    result_file.write('id,correctAnswer\n')
                    with open(VAL_SET_PATH, 'r') as val_file:
                        val_questions = [x.strip() for x in val_file.readlines()]

                        for s in val_questions[1:]:
                            id, question, answerA, answerB, answerC, answerD = s.split('\t')

                            ans, scoreA, scoreB, scoreC, scoreD, ansA, ansB, ansC, ansD, questionCount = predict_answer(indexer, question, answerA, answerB, answerC, answerD)
                            lucene_scores.write('{0},{1},{2},{3},{4}\n'.format(id, scoreA, scoreB, scoreC, scoreD))
                            words_count.write('{0},{1},{2},{3},{4}\n'.format(id, ansA, ansB, ansC, ansD))
                            words_aq_count.write('{0},{1},{2},{3},{4}\n'.format(id, ansA + questionCount, ansB+ questionCount, ansC+ questionCount, ansD+ questionCount))
                            result_file.write(id + ',' + ans + '\n')


if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding('utf8')
    stop = set(stopwords.words('english'))
    ck_keywords_list = []

    with open('../data/SMART_stopwords.txt', 'r') as smart_stopwwods_file:
       stopwords = set([x.strip() for x in smart_stopwwods_file.readlines() if len(x) > 1])
       stop = stop.union(stopwords)

    for theme in keywords_themes:
        with open(theme,'r') as theme_file:
            keywords = set([remove_stopword(x.strip()) for x in theme_file.readlines()])
            ck_keywords_list.extend([stemmer.stem(x) for x in keywords])
            keywords_by_theme[theme] = keywords
    #with open(ck_keywords_path, 'r') as ck_file:
        #ck_keywords = [stemmer.stem(remove_stopword(x.strip())) for x in ck_file.readlines()]
    ck_keywords = set(ck_keywords_list)
    keywords_duplicate =[]
    for keyword in ck_keywords:
        if len(keyword) ==0:
            keywords_duplicate.append(keyword)
    for keyword in keywords_duplicate:
         ck_keywords.remove(keyword)
    lucene.initVM()

    indexer = prepare_index(DOCS_DIR, stemmer)
    get_submission_from_val(indexer)
    print 'Accuracy on train set:', get_accuracy_on_train_set(indexer)
