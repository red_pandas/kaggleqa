# Install openjdk 1.7
    sudo apt-get install default-jdk

# Install ant
    sudo apt-get install ant

# Install JCC
http://lucene.apache.org/pylucene/jcc/install.html

    svn co http://svn.apache.org/repos/asf/lucene/pylucene/trunk/jcc jcc
    cd jcc
    python setup.py build
    sudo python setup.py install

# Install pylucene
http://lucene.apache.org/pylucene/install.html

    wget http://apache-mirror.rbc.ru/pub/apache/lucene/pylucene/pylucene-4.10.1-1-src.tar.gz
    tar -xvzf pylucene-4.10.1-1-src.tar.gz
    cd pylucene-4.10.1-1

Install ant ivy-bootstrap

    cd lucene-java-4.10.1
    ant ivy-bootstrap
    cd ..

Edit Makefile: uncomment lines under "*Linux     (Ubuntu 11.10 64-bit, Python 2.7.2, OpenJDK 1.7, setuptools 0.6.16)*"

Then

    make
    make test
    sudo make install

# Install other python requirements (from lucene_experiments folder)

    pip install -r requirements.txt

# Install nltk stopwords corpora

    python
    python
    import nltk
    nltk.download()

Select Corpora -> stopwords -> Download. When finished, close the window

