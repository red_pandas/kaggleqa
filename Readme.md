# This is a repo for Kaggle Allen AI Science Challenge

You can find installation instructions for lucene-based solution inside lucene_experiments folder.

Have fun!

![All glory to red pandas!](http://i.imgur.com/ybeUzpy.jpg)

# Useful links
ck12 concepts textbooks

[Stripped html-only](https://www.dropbox.com/s/krq7196pkwyj010/ck12concepts.tar.gz?dl=0)

    wget https://www.dropbox.com/s/krq7196pkwyj010/ck12concepts.tar.gz?dl=0 ck12concepts.tar.gz
    tar -xzvf ck12concepts.tar.gz

[Original ck12books](http://www.ck12.org/flx/show/epub/user:anBkcmVjb3VydEBnbWFpbC5jb20./Concepts_b_v8_vdt.epub)

    wget http://www.ck12.org/flx/show/epub/user:anBkcmVjb3VydEBnbWFpbC5jb20./Concepts_b_v8_vdt.epub
    unzip Concepts_b_v8_vdt.epub -d ck12concepts

[lucene index to get 0.43125](https://www.dropbox.com/s/clpp16qhme8lk47/new_lucene_index.tar.gz?dl=0)

[wikipedia search results from bing](https://www.dropbox.com/s/o5nq3h7x67rh8m9/output.json.tar.gz?dl=0)

### Papers
[QA-TopicModel](https://www.dropbox.com/s/di1gwrqukvk9l36/QA-TopicModel-WBW-IJCAI2011-CameraReady.pdf?dl=0)

[QANTA](https://cs.umd.edu/~miyyer/qblearn/)

### Current approach
[Lucene baseline forum post](https://www.kaggle.com/c/the-allen-ai-science-challenge/forums/t/17024/how-to-get-started-with-lucene-wikipedia-benchmark/96950#post96950)
0.43125 public leaderboard commit 5d983

Steps to reproduce:

Get list of ck12 topic keywords.

    python util/scrape.py > data/ck12_list_keyword.txt

Download articles from wikipedia for these keywords.

    python util/get_wikipedia_data.py

Extract text between h3 tags from [ck12 textbooks](http://www.ck12.org/flx/show/epub/user:anBkcmVjb3VydEBnbWFpbC5jb20./Concepts_b_v8_vdt.epub) into separate documents.

    python util/get_data_from_ck12_textbook.py

Search bing with GoogleScraper with queries constructed by site:wikipedia.com + question. Then pick top 2 results from wikipedia and add to index.

    python util/contruct_bing_query_file.py
    googlescraper -s "bing" -p 1 -n 10 --keyword-file queries.txt --output-filename output.json
    python util/get_wikipedia_from_bing_top_2.py

Index all documents with lucene then search question + answer for each answer and pick highest scoring lucene answer as answer.

    python lucene_experiments/lucene_index_and_predict.py

Stemming and stopword removal makes result of public leaderbord worse.