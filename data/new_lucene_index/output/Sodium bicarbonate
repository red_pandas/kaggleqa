sodium bicarbonate (iupac name: sodium hydrogen carbonate) is a chemical compound with the formula nahco3 sodium bicarbonate is a white solid that is crystalline but often appears as a fine powder it has a slightly salty alkaline taste resembling that of washing soda (sodium carbonate) the natural mineral form is nahcolite it is a component of the mineral natron and is found dissolved in many mineral springs it is among the food additives encoded by european union identified as e 500 since it has long been known and is widely used the salt has many related names such as baking soda bread soda cooking soda and bicarbonate of soda the word saleratus from latin sal ratus meaning "aerated salt" was widely used in the 19th century for both sodium bicarbonate and potassium bicarbonate
== historyedit ==
the ancient egyptians used natural deposits of natron a mixture consisting mostly of sodium carbonate decahydrate and sodium bicarbonate the natron was ground up solvated and used as paint for hieroglyphics
in 1791 a french chemist nicolas leblanc produced sodium carbonate also known as soda ash in 1846 two new york bakers john dwight and austin church established the first factory to develop baking soda from sodium carbonate and carbon dioxide
this compound referred to as saleratus is mentioned in the novel captains courageous by rudyard kipling as being used extensively in the 1800s in commercial fishing to prevent freshly caught fish from spoiling
== productionedit ==
nahco3 is mainly prepared by the solvay process which is the reaction of sodium chloride ammonia and carbon dioxide in water calcium carbonate is used as the source of co2 and the resultant calcium oxide is used to recover the ammonia from the ammonium chloride the product shows a low purity (75%) pure product is obtained from sodium carbonate water and carbon dioxide as reported in one of the following reactions it is produced on the scale of about 100,000 tonnes/year (as of 2001)
nahco3 may be obtained by the reaction of carbon dioxide with an aqueous solution of sodium hydroxide the initial reaction produces sodium carbonate:
co2 + 2 naoh na2co3 + h2o
further addition of carbon dioxide produces sodium bicarbonate which at sufficiently high concentration will precipitate out of solution:
na2co3 + co2 + h2o 2 nahco3
commercial quantities of baking soda are also produced by a similar method: soda ash mined in the form of the ore trona is dissolved in water and treated with carbon dioxide sodium bicarbonate precipitates as a solid from this method:
na2co3 + co2 + h2o 2 nahco3
== miningedit ==
naturally occurring deposits of nahcolite (nahco3) are found in the eocene-age (55.833.9 mya) green river formation piceance basin in colorado nahcolite was deposited as beds during periods of high evaporation in the basin it is commercially mined using in situ leach techniques involving dissolution of the nahcolite by heated water pumped through the nahcolite beds and reconstituted through a natural cooling crystallisation process
== chemistryedit ==
sodium bicarbonate is an amphoteric compound aqueous solutions are mildly alkaline due to the formation of carbonic acid and hydroxide ion:
hco
3 + h2o h
2co
3 + oh
sodium bicarbonate can be used as a wash to remove any acidic impurities from a "crude" liquid producing a purer sample reaction of sodium bicarbonate and an acid produce a salt and carbonic acid which readily decomposes to carbon dioxide and water:
nahco3 + hcl nacl + h2co3
h2co3 h2o + co2(g)
sodium bicarbonate reacts with acetic acid (found in vinegar) producing sodium acetate water and carbon dioxide:
nahco3 + ch3cooh ch3coona + h2o + co2(g)
sodium bicarbonate reacts with bases such as sodium hydroxide to form carbonates:
nahco3 + naoh na2co3 + h2o
sodium bicarbonate reacts with carboxyl groups in proteins to give a brisk effervescence from the formation of co
2 this reaction is used to test for the presence of carboxylic groups in protein
=== thermal decompositionedit ===
above 50c sodium bicarbonate gradually decomposes into sodium carbonate water and carbon dioxide the conversion is fast at 200c:
2 nahco3 na2co3 + h2o + co2
most bicarbonates undergo this dehydration reaction further heating converts the carbonate into the oxide (at over 850c):
na2co3 na2o + co2
these conversions are relevant to the use of nahco3 as a fire-suppression agent ("bc powder") in some dry powder fire extinguishers
== applicationsedit ==
sodium bicarbonate has a wide variety of uses
=== pest controledit ===
used to kill cockroaches once consumed it causes internal organs of cockroaches to burst due to gas collection
=== paint and corrosion removaledit ===
sodium bicarbonate is used in a process for removing paint and corrosion called sodablasting; the process is particularly suitable for cleaning aluminium panels which can be distorted by other types of abrasive
=== ph balanceredit ===
it can be administered to pools spas and garden ponds to raise ph levels
=== mild disinfectantedit ===
it has weak disinfectant properties and it may be an effective fungicide against some organisms because baking soda will absorb musty smells it has become a reliable method for used-book sellers when making books less malodorous
=== fire extinguisheredit ===
sodium bicarbonate can be used to extinguish small grease or electrical fires by being thrown over the fire as heating of sodium bicarbonate releases carbon dioxide however it should not be applied to fires in deep fryers; the sudden release of gas may cause the grease to splatter sodium bicarbonate is used in bc dry chemical fire extinguishers as an alternative to the more corrosive ammonium phosphate in abc extinguishers the alkaline nature of sodium bicarbonate makes it the only dry chemical agent besides purple-k that was used in large-scale fire suppression systems installed in commercial kitchens because it can act as an alkali the agent has a mild saponification effect on hot grease which forms a smothering soapy foam
=== cookingedit ===
==== with acidsedit ====
sodium bicarbonate referred to as "baking soda" is primarily used in baking as a leavening agent it reacts with acidic components in batters releasing carbon dioxide which causes expansion of the batter and forms the characteristic texture and grain in pancakes cakes quick breads soda bread and other baked and fried foods acidic compounds that induce this reaction include phosphates cream of tartar lemon juice yogurt buttermilk cocoa and vinegar natural acids in sourdough can be leavened with the addition of small amounts as well sodium bicarbonate can be substituted for baking powder provided sufficient acid reagent is also added to the recipe many forms of baking powder contain sodium bicarbonate combined with calcium acid phosphate sodium aluminium phosphate or cream of tartar
sodium bicarbonate was sometimes used in cooking vegetables to make them softer although this has gone out of fashion as most people now prefer firmer vegetables however it is still used in asian and latin american cuisine to tenderise meats baking soda may react with acids in food including vitamin c (l-ascorbic acid) it is also used in breading such as for fried foods to enhance crispness and allow passages for steam to escape so the breading is not blown off during cooking
==== by heatingedit ====
heat causes sodium bicarbonate to act as a raising agent by releasing carbon dioxide when used in baking the carbon dioxide production starts at temperatures above 80c since the reaction occurs slowly at room temperature mixtures (cake batter etc.) can be allowed to stand without rising until they are heated in the oven
2nahco3 na2co3 + h2o + co2
=== neutralisation of acids and basesedit ===
sodium bicarbonate is amphoteric reacting with acids and bases it reacts violently with acids releasing co2 gas as a reaction product however sodium bicarbonate is not recommended for the clean-up of acid spills the heat produced increases the reactivity of the acid and the large amount of carbon dioxide produced may increase the area of the spill
a wide variety of applications follows from its neutralisation properties including reducing the spread of white phosphorus from incendiary bullets inside an afflicted soldier's wounds
=== medical usesedit ===
sodium bicarbonate mixed with water can be used as an antacid to treat acid indigestion and heartburn
intravenous sodium bicarbonate is an aqueous solution that is sometimes used for cases of acidosis or when insufficient sodium or bicarbonate ions are in the blood in cases of respiratory acidosis the infused bicarbonate ion drives the carbonic acid/bicarbonate buffer of plasma to the left and thus raises the ph it is for this reason that sodium bicarbonate is used in medically supervised cardiopulmonary resuscitation infusion of bicarbonate is indicated only when the blood ph is markedly (<7.17.0) low
it is used for treatment of hyperkalemia as it will drive k+ back into cells during periods of hypochloremic metabolic alkalosis since sodium bicarbonate can cause alkalosis it is sometimes used to treat aspirin overdoses aspirin requires an acidic environment for proper absorption and the basic environment diminishes aspirin absorption in the case of an overdose sodium bicarbonate has also been used in the treatment of tricyclic antidepressant overdose it can also be applied topically as a paste with three parts baking soda to one part water to relieve some kinds of insect bites and stings (as well as accompanying swelling)
adverse reactions to the administration of sodium bicarbonate can include metabolic alkalosis edema due to sodium overload congestive heart failure hyperosmolar syndrome hypervolemic hypernatremia and hypertension due to increased sodium in patients consuming a high-calcium or dairy-rich diet calcium supplements or calcium-containing antacids such as calcium carbonate (e.g. tums) the use of sodium bicarbonate can cause milk-alkali syndrome which can result in metastatic calcification kidney stones and kidney failure
sodium bicarbonate can be used to treat an allergic reaction to plants such as poison ivy poison oak or poison sumac to relieve some of the associated itching
bicarbonate of soda can also be useful in removing splinters from the skin
some alternative practitioners such as tullio simoncini have promoted baking soda as a cancer cure which the american cancer society has warned against due to both its unproven effectiveness and potential danger in use
sodium bicarbonate can be added to local anaesthetics to speed up the onset of their effects and make their injection less painful it is also a component of moffett's solution used in nasal surgery
=== personal hygieneedit ===
toothpaste containing sodium bicarbonate has in several studies been shown to have a better whitening and plaque removal effect than toothpastes without it
sodium bicarbonate is also used as an ingredient in some mouthwashes it has anticaries and abrasive properties it works as a mechanical cleanser on the teeth and gums neutralises the production of acid in the mouth and also acts as an antiseptic to help prevent infections
sodium bicarbonate in combination with other ingredients can be used to make a dry or wet deodorant it may also be used as a shampoo
sodium bicarbonate may be used as a buffering agent combined with table salt when creating a solution for nasal irrigation
it is used in eye hygiene to treat blepharitis this is done by addition of a tablespoon of sodium bicarbonate to cool water that was recently boiled followed by gentle scrubbing of the eyelash base with a cotton swab dipped in the solution
=== in sportsedit ===
small amounts of sodium bicarbonate have been shown to be useful as a supplement for athletes in speed-based events such as middle-distance running lasting from about one to seven minutes however overdose is a serious risk because sodium bicarbonate is slightly toxic; and gastrointestinal irritation is of particular concern additionally this practice causes a significant increase in dietary sodium
=== as a cleaning agentedit ===
a paste from baking soda can be very effective when used in cleaning and scrubbing for cleaning aluminium objects the use of sodium bicarbonate is discouraged as it attacks the thin unreactive protective oxide layer of this otherwise very reactive metal a solution in warm water will remove the tarnish from silver when the silver is in contact with a piece of aluminium foil a paste of sodium bicarbonate and water is useful in removing surface rust as the rust forms a water-soluble compound when in a concentrated alkaline solution cold water should be used as hot water solutions can corrode steel
baking soda is commonly added to washing machines as a replacement for softener and to remove odors from clothes sodium bicarbonate is also effective in removing heavy tea and coffee stains from cups when diluted with warm water
during the manhattan project to develop the atomic bomb in the early 1940s many scientists investigated the toxic properties of uranium they found that uranium oxides stick very well to cotton cloth but did not wash out with soap or laundry detergent the uranium would wash out with a 2% solution of sodium bicarbonate (baking soda) clothing can become contaminated with depleted uranium (du) dust and then normal laundering will not remove it those at risk of du dust exposure should have their clothing washed with about 6 ounces (170 g) of baking soda in 2 gallons (7.5 l) of water)
=== as a biopesticideedit ===
sodium bicarbonate can be an effective way of controlling fungal growth and in the united states is registered by the environmental protection agency as a biopesticide
=== cattle feed supplementsedit ===
sodium bicarbonate is sold as a cattle feed supplement in particular as a buffering agent for the rumen
== in popular cultureedit ==
=== filmedit ===
sodium bicarbonate as 'bicarbonate of soda' was a frequent source of punch lines for groucho marx in marx brothers movies in duck soup marx plays the leader of a nation at war in one scene he receives a message from the battlefield that his general is reporting a gas attack and groucho tells his aide "tell him to take a teaspoonful of bicarbonate of soda and a half a glass of water." in a night at the opera groucho's character addresses the opening night crowd at an opera by saying of the lead tenor "signor lassparri comes from a very famous family his mother was a well-known bass singer his father was the first man to stuff spaghetti with bicarbonate of soda thus causing and curing indigestion at the same time."
== difference between baking soda and baking powderedit ==
quite simply baking powder contains baking soda as well as a powdered acid and cornstarch in scientific terms baking soda is a pure substance; baking powder is a mixture
baking soda is alkaline so acid is used in baking powder to avoid a metallic taste when the chemical change during baking creates sodium carbonate however to avoid the over-flavouring of acidic taste non-acid ingredients such as whole milk or dutch-processed cocoa must be added
== see alsoedit ==
carbonic acid
soda bread
list of ineffective cancer treatments
list of minerals
nahcolite
natron
natrona (disambiguation)
trona
washing soda
== notesedit ==
== referencesedit ==
== further readingedit ==
bishop d; edge j; davis c; goodman c (may 2004) "induced metabolic alkalosis affects muscle metabolism and repeated-sprint ability" medicine and science in sports and exercise 36 (5): 80713 doi:10.1249/01.mss.0000126392.20025.17 issn 0195-9131 pmid 15126714
david r lide ed (2003) crc handbook of chemistry and physics (84th ed.) boca raton fl: crc press isbn 0-8493-0484-9
== external linksedit ==
international chemical safety card 1044
differences between baking soda and baking powder
