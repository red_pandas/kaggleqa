temperature measurement describes the process of measuring a current local temperature for immediate or later evaluation datasets consisting of repeated standardized measurements can be used to assess temperature trends
== history ==
attempts at standardized temperature measurement prior to the 17th century were crude at best for instance in 170 ad physician claudius galenus mixed equal portions of ice and boiling water to create a "neutral" temperature standard the modern scientific field has its origins in the works by florentine scientists in the 1600s including galileo constructing devices able to measure relative change in temperature but subject also to confounding with atmospheric pressure changes these early devices were called thermoscopes the first sealed thermometer was constructed in 1641 by the grand duke of toscani ferdinand ii the development of today's thermometers and temperature scales began in the early 18th century when gabriel fahrenheit produced a mercury thermometer and scale both developed by ole christensen rmer fahrenheit's scale is still in use alongside the celsius and kelvin scales
== technologies ==
many methods have been developed for measuring temperature most of these rely on measuring some physical property of a working material that varies with temperature one of the most common devices for measuring temperature is the glass thermometer this consists of a glass tube filled with mercury or some other liquid which acts as the working fluid temperature increase causes the fluid to expand so the temperature can be determined by measuring the volume of the fluid such thermometers are usually calibrated so that one can read the temperature simply by observing the level of the fluid in the thermometer another type of thermometer that is not really used much in practice but is important from a theoretical standpoint is the gas thermometer
other important devices for measuring temperature include:
thermocouples
thermistors
resistance temperature detector (rtd)
pyrometer
langmuir probes (for electron temperature of a plasma)
infrared
other thermometers
one must be careful when measuring temperature to ensure that the measuring instrument (thermometer thermocouple etc.) is really the same temperature as the material that is being measured under some conditions heat from the measuring instrument can cause a temperature gradient so the measured temperature is different from the actual temperature of the system in such a case the measured temperature will vary not only with the temperature of the system but also with the heat transfer properties of the system an extreme case of this effect gives rise to the wind chill factor where the weather feels colder under windy conditions than calm conditions even though the temperature is the same what is happening is that the wind increases the rate of heat transfer from the body resulting in a larger reduction in body temperature for the same ambient temperature
the theoretical basis for thermometers is the zeroth law of thermodynamics which postulates that if you have three bodies a b and c if a and b are at the same temperature and b and c are at the same temperature then a and c are at the same temperature b of course is the thermometer
the practical basis of thermometry is the existence of triple point cells triple points are conditions of pressure volume and temperature such that three phases are simultaneously present for example solid vapor and liquid for a single component there are no degrees of freedom at a triple point and any change in the three variables results in one or more of the phases vanishing from the cell therefore triple point cells can be used as universal references for temperature and pressure (see gibbs phase rule)
under some conditions it becomes possible to measure temperature by a direct use of the planck's law of black-body radiation for example the cosmic microwave background temperature has been measured from the spectrum of photons observed by satellite observations such as the wmap in the study of the quarkgluon plasma through heavy-ion collisions single particle spectra sometimes serve as a thermometer
=== non-invasive thermometry ===
during recent decades many thermometric techniques have been developed the most promising and widespread non-invasive thermometric techniques are based on the analysis of magnetic resonance images computerised tomography images and echotomography images these techniques allow monitoring temperature within tissues without introducing a sensing element
== surface air temperature ==
the temperature of the air near the surface of the earth is measured at meteorological observatories and weather stations usually using thermometers placed in a stevenson screen a standardized well-ventilated white-painted instrument shelter the thermometers should be positioned 1.252 m above the ground details of this setup are defined by the world meteorological organization (wmo)
a true daily mean could be obtained from a continuously-recording thermograph commonly it is approximated by the mean of discrete readings (e.g 24 hourly readings four 6-hourly readings etc.) or by the mean of the daily minimum and maximum readings (though the latter can result in mean temperatures up to 1 c cooler or warmer than the true mean depending on the time of observation)
the world's average surface air temperature is about 14 c for information on temperature changes relevant to climate change or earth's geologic past see: temperature record
== comparison of temperature scales ==
== standards ==
the american society of mechanical engineers (asme) has developed two separate and distinct standards on temperature measurement b40.200 and ptc 19.3 b40.200 provides guidelines for bimetallic-actuated filled-system and liquid-in-glass thermometers it also provides guidelines for thermowells ptc 19.3 provides guidelines for temperature measurement related to performance test codes with particular emphasis on basic sources of measurement errors and techniques for coping with them
=== us (asme) standards ===
b40.200-2008: thermometers direct reading and remotes reading
ptc 19.3-1974(r2004): performance test code for temperature measurement
== see also ==
timeline of temperature and pressure measurement technology
temperature conversion formulas
color temperature
planck temperature
temperature data logger
== references ==
