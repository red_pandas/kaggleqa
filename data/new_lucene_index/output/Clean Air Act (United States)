the clean air act is a united states federal law designed to control air pollution on a national level it is one of the united states' first and most influential modern environmental laws and one of the most comprehensive air quality laws in the world as with many other major u.s federal environmental statutes it is administered by the u.s environmental protection agency (epa) in coordination with state local and tribal governments its implementing regulations are codified at 40 c.f.r subchapter c parts 50-97
the 1955 air pollution control act was the first u.s federal legislation that pertained to air pollution; it also provided funds for federal government research of air pollution the first federal legislation to actually pertain to "controlling" air pollution was the clean air act of 1963 the 1963 act accomplished this by establishing a federal program within the u.s public health service and authorized research into techniques for monitoring and controlling air pollution in 1967 the air quality act enabled the federal government to increase its activities to investigate enforcing interstate air pollution transport and for the first time to perform far-reaching ambient monitoring studies and stationary source inspections the 1967 act also authorized expanded studies of air pollutant emission inventories ambient monitoring techniques and control techniques
major amendments to the law requiring regulatory controls for air pollution passed in 1970 1977 and 1990
the 1970 amendments greatly expanded the federal mandate requiring comprehensive federal and state regulations for both stationary (industrial) pollution sources and mobile sources it also significantly expanded federal enforcement also the environmental protection agency was established on december 2 1970 for the purpose of consolidating pertinent federal research monitoring standard-setting and enforcement activities into one agency that ensures environmental protection
the 1990 amendments addressed acid rain ozone depletion and toxic air pollution established a national permits program for stationary sources and increased enforcement authority the amendments also established new auto gasoline reformulation requirements set reid vapor pressure (rvp) standards to control evaporative emissions from gasoline and mandated new gasoline formulations sold from may to september in many states
the clean air act was the first major environmental law in the united states to include a provision for citizen suits numerous state and local governments have enacted similar legislation either implementing federal programs or filling in locally important gaps in federal programs
== components of air pollution prevention and control ==
=== title i - programs and activities ===
==== part a - air quality and emissions limitations ====
this section of the act declares that protecting and enhancing the nation's air quality promotes public health the law encourages prevention of regional air pollution and control programs it also provides technical and financial assistance for air pollution prevention at both state and local governments additional subchapters cover of cooperation research investigation training and other activities grants for air pollution planning and control programs and interstate air quality agencies and program cost limitations are also included in this section of the act
the act mandates air quality control regions designated as attainment vs non-attainment non-attainment areas do not meet national standards for primary or secondary ambient air quality attainment areas meet these standards while unclassifiable areas cannot be classified on the basis of the information that is available
air quality criteria national primary and secondary ambient air quality standards state implementation plans and performance standards for new stationary sources are also covered in part a the list of hazardous air pollutants established by the act includes acetaldehyde benzene chloroform phenols and selenium compounds the list also includes mineral fiber emissions from manufacturing or processing glass rock or slag fibers as well as radioactive atoms the list periodically can be modified the act lists unregulated radioactive pollutants such as cadmium arsenic and polycyclic organic matter and mandates listing them if they will cause or contribute to air pollution that endangers public health under section 7408 or 7412
the remaining subchapters cover smokestack heights state plan adequacy and estimating emissions of carbon monoxide volatile organic compounds and oxides of nitrogen from area and mobile sources measures to prevent unemployment or other economic disruption include using local coal or coal derivatives to comply with implementation requirements the final subchapter in this act focuses on land use authority
==== part b - ozone protection ====
because of advances in the atmospheric chemistry this section was replaced by title vi when the law was amended in 1990
this change in the law reflected significant changes in scientific understanding of ozone formation and depletion ozone absorbs uvc light and shorter wave uvb and lets through uva which is largely harmless to people ozone exists naturally in the stratosphere not the troposphere it is laterally distributed because it is destroyed by strong sunlight so there is more ozone at the poles ozone is created when o2 comes in contact with photons from solar radiation therefore a decrease in the intensity of solar radiation also results in a decrease in the formation of ozone in the stratosphere this exchange is known as the chapman mechanism:
o2 + uv photon 2 o (note that atmospheric oxygen as o is highly unstable)
o + o2 + m o3 (o3 is ozone) + m
m represents a third molecule needed to carry off the excess energy of the collision of o + o2
atmospheric freon and chlorofluorocarbons (cfcs) contribute to ozone depletion (chlorine is a catalytic agent in ozone destruction) following discovery of the ozone hole in 1985 the 1987 montreal protocol successfully implemented a plan to replace cfcs and was viewed by some environmentalists as an example of what is possible for the future of environmental issues if the political will is present
==== part c - prevention of significant deterioration of air quality ====
the clean air act requires permits to build or add to major stationary sources of air pollution this permitting process known as new source review (nsr) applies to sources in areas that meet air quality standards as well as areas that are unclassifiable permits in attainment or unclassifiable areas are referred to as prevention of significant deterioration (psd) permits while permits for sources located in nonattainment areas are referred to as nonattainment area (naa) permits
the fundamental goals of the psd program are to:
prevent new non-attainment areas by ensuring economic growth in harmony with existing clean air;
protect public health and welfare from any adverse effects;
preserve and enhance the air quality in national parks and other areas of special natural recreational scenic or historic value
==== part d - plan requirements for non-attainment areas ====
under the clean air act states are required to submit a plan for non-attainment areas to reach attainment status as soon as possible but in no more than five years based on the severity of the air pollution and the difficulty posed by obtaining cleaner air
the plan must include:
an inventory of all pollutants
permits
control measures means and techniques to reach standard qualifications
contingency measures
the plan must be approved or revised if required for approval and specify whether local governments or the state will implement and enforce the various changes achieving attainment status makes a request for reevaluation possible it must include a plan for maintenance of air quality
=== title ii - emission standards for moving sources ===
==== part a - motor vehicle emission and fuel standards (caa 201-219; usc 7521-7554) ====
subchapters of title ii cover state standards and grants prohibited acts and actions to restrain violations as well as a study of emissions from nonroad vehicles (other than locomotives) to determine whether they cause or contribute to air pollution motorcycles are treated in the same way as automobiles under the emission standards for new motor vehicles or motor vehicle engines the last few subchapters deal with high altitude performance adjustments motor vehicle compliance program fees prohibition on production of engines requiring leaded gasoline and urban bus standards
this part of the bill was extremely controversial the time it was passed the automobile industry argued that it could not meet the new standards senators expressed concern about impact on the economy specific new emissions standards for moving sources passed years later
==== part b - aircraft emission standards ====
many volatile organic compounds (vocs) are emitted over airports and affect the air quality in the region vocs include benzene formaldehyde and butadienes which are known to cause health problems such as birth defects cancer and skin irritation hundreds of tons of emissions from aircraft ground support equipment heating systems and shuttles and passenger vehicles are released into the air causing smog therefore major cities such as seattle denver and san francisco require a climate action plan as well as a greenhouse gas inventory additionally federal programs such as vale are working to offset costs for programs that reduce emissions
title ii sets emission standards for airlines and aircraft engines and adopts standards set by the international civil aviation organization (icao) however aircraft carbon dioxide emission standards have not been established by either icao nor the epa it is the responsibility of the secretary of transportation after consultation with the administrator to prescribe regulations that comply with section 7571 and ensure the necessary inspections take place
==== part c - clean fuel vehicles ====
trucks and automobiles play a large role in deleterious air quality harmful chemicals such as nitrogen oxide hydrocarbons carbon monoxide and sulfur dioxide are released from motor vehicles some of these also react with sunlight to produce photochemicals these harmful substances change the climate alter ocean ph and include toxins that may cause cancer birth defects or respiratory illness motor vehicles increased in the 1990s since approximately 58 percent of households owned two or more vehicles the clean fuel vehicle programs focused on alternative fuel use and petroleum fuels that met low emission vehicle (lev) levels compressed natural gas ethanol methanol liquefied petroleum gas and electricity are examples of cleaner alternative fuel programs such as the california clean fuels program and pilot program are increasing demand that for new fuels to be developed to reduce harmful emissions
the california pilot program incorporated under this section focuses on pollution control in ozone non-attainment areas the provisions apply to light-duty trucks and light-duty vehicles in california the also state requires that clean alternative fuels for sale at numerous locations with sufficient geographic distribution for convenience production of clean-fuel vehicles isn't mandated except as part of the california pilot program
=== title iii - general provisions ===
under the law prior to 1990 epa was required to construct a list of hazardous air pollutants as well as health-based standards for each one there were 188 air pollutants listed and the source from which they came the epa was given ten years to generate technology-based emission standards title iii is considered a second phase allowing the epa to assess lingering risks after the enactment of the first phase of emission standards title iii also enacts new standards with regard to the protection of public health
a citizen may file a lawsuit to obtain compliance with an emission standard issued by the epa or by a state unless there is an ongoing enforcement action being pursued by epa or the appropriate state agency
=== title iv - noise pollution ===
this title pre-dates the clean air act with the passage of the clean air act it became codified as title iv however another title iv was enacted in the 1970 amendments the second title iv was then appended to this title iv as title iv-a (see below)
this title established the epa office of noise abatement and control to reduce noise pollution in urban areas to minimize noise-related impacts on psychological and physiological effects on humans effects on wildlife and property (including values) and other noise-related issues the agency was also assigned to run experiments to study the effects of noise
=== title iv-a - acid deposition control ===
this title was added as part of the 1990 amendments it addresses the issue of acid rain which is caused by nitrogen oxides (nox) and sulfur dioxide (so2) emissions from electric power plants powered by fossil fuels and other industrial sources the 1990 amendments gave industries more pollution control options including switching to low-sulfur coal and/or adding devices that controlled the harmful emissions in some cases plants had to be closed down to prevent the dangerous chemicals from entering the atmosphere
title iv-a mandated a two-step process to reduce so2 emissions the first stage required more than 100 electric generating facilities larger than 100 megawatts to meet a 3.5 million ton so2 emission reduction by january 1995 the second stage gave facilities larger than 75 megawatts a january 2000 deadline
=== title v - permits ===
the 1990 amendments authorized a national operating permit program covering thousands of large industrial and commercial sources it required large businesses to address pollutants released into the air measure their quantity and have a plan to control and minimize them as well as to periodically report this consolidated requirements for a facility into a single document
in non-attainment areas permits were required for sources that emit as little as 50 25 or 10 tons per year of vocs depending on the severity of the regions non-attainment status
most permits are issued by state and local agencies if the state does not adequately monitor requirements the epa may take control the public may request to view the permits by contacting the epa the permit is limited to no more than five years and requires a renewal
=== title vi - stratospheric ozone protection ===
starting in 1990 title vi mandated regulations regarding the use and production of chemicals that harm the earths stratospheric ozone layer this ozone layer protects against harmful ultraviolet b sunlight linked to several medical conditions including cataracts and skin cancer
the ozone-destroying chemicals were classified into two groups class i and class ii class i consists of substances including chlorofluorocarbons that have an ozone depletion potential (odp) (hl) of 0.2 or higher class ii lists substances including hydrochlorofluorocarbons that are known to or may be detrimental to the stratosphere both groups have a timeline for phase-out:
for class i substances no more than seven years after being added to the list and
for class ii substances no more than ten years
title vi establishes methods for preventing harmful chemicals from entering the stratosphere in the first place including recycling or proper disposal of chemicals and finding substitutes that cause less or no damage the significant new alternatives policy (snap) program is epa's program to evaluate and regulate substitutes for the ozone-depleting chemicals that are being phased out under the stratospheric ozone protection provisions of the clean air act
over 190 countries signed the montreal protocol in 1987 agreeing to work to eliminate or limit the use of chemicals with ozone-destroying properties
== history ==
=== legislation ===
congress passed the first legislation to address air pollution with the 1955 air pollution control act that provided funds to the u.s public health service but did not formulate pollution regulation however the clean air act in 1963 created a research and regulatory program in the u.s public health service the act authorized development of emission standards for stationary sources but not mobile sources of air pollution the 1967 air quality act mandated enforcement of interstate air pollution standards and authorized ambient monitoring studies and stationary source inspections
in the clean air act extension of 1970 congress greatly expanded the federal mandate by requiring comprehensive federal and state regulations for both industrial and mobile sources the law established four new regulatory programs:
national ambient air quality standards (naaqs) epa was required to promulgate national standards for six criteria pollutants: carbon monoxide nitrogen dioxide sulfur dioxide particulate matter hydrocarbons and photochemical oxidants (some of the criteria pollutants were revised in subsequent legislation.)
state implementation plans (sips)
new source performance standards (nsps); and
national emissions standards for hazardous air pollutants (neshaps)
the 1970 law is sometimes called the "muskie act" because of the central role maine senator edmund muskie played in drafting the bill the epa was also created under the national environmental policy act about the same time as these additions were passed which was important to help implement the programs listed above
the clean air act amendments of 1977 required prevention of significant deterioration (psd) of air quality for areas attaining the naaqs and added requirements for non-attainment areas
the 1990 clean air act added regulatory programs for control of acid deposition (acid rain) and stationary source operating permits the amendments moved considerably beyond the original criteria pollutants expanding the neshap program with a list of 189 hazardous air pollutants to be controlled within hundreds of source categories according to a specific schedule the naaqs program was also expanded other new provisions covered stratospheric ozone protection increased enforcement authority and expanded research programs
=== history of the clean air act ===
==== introduction ====
the legal authority for federal programs regarding air pollution control is based on the 1990 clean air act amendments (1990 caaa) these are the latest in a series of amendments made to the clean air act (caa) often referred to as "the act." this legislation modified and extended federal legal authority provided by the earlier clean air acts of 1963 and 1970
the 1955 air pollution control act was the first federal legislation involving air pollution; it authorized $3 million per year to the u.s public health service for five years to fund federal level air pollution research air pollution control research and technical and training assistance to the states subsequently the act was extended for four years in 1959 with funding levels at $5 million per year the act was then amended in 1960 and 1962 although the 1955 act brought the air pollution issue to the federal level no federal regulations were formulated control and prevention of air pollution was instead delegated to state and local agencies
the clean air act of 1963 was the first federal legislation regarding air pollution control it established a federal program within the u.s public health service and authorized research into techniques for monitoring and controlling air pollution in 1967 the air quality act was enacted in order to expand federal government activities in accordance with this law enforcement proceedings were initiated in areas subject to interstate air pollution transport as part of these proceedings the federal government for the first time conducted extensive ambient monitoring studies and stationary source inspections
the air quality act of 1967 also authorized expanded studies of air pollutant emission inventories ambient monitoring techniques and control techniques
==== clean air act of 1970 ====
the clean air act of 1970 (1970 caa) authorized the development of comprehensive federal and state regulations to limit emissions from both stationary (industrial) sources and mobile sources four major regulatory programs affecting stationary sources were initiated:
the national ambient air quality standards [naaqs (pronounced "knacks")]
state implementation plans (sips)
new source performance standards (nsps)
and national emission standards for hazardous air pollutants (neshaps)
enforcement authority was substantially expanded this very important legislation was adopted at approximately the same time as the national environmental policy act that established the u.s environmental protection agency (epa); the epa was created on may 2 1971 in order to implement the various requirements included in the clean air act of 1970
==== clean air act amendments of 1977 ====
major amendments were added to the clean air act in 1977 (1977 caaa) the 1977 amendments primarily concerned provisions for the prevention of significant deterioration (psd) of air quality in areas attaining the naaqs the 1977 caaa also contained requirements pertaining to sources in non-attainment areas for naaqs a non-attainment area is a geographic area that does not meet one or more of the federal air quality standards both of these 1977 caaa established major permit review requirements to ensure attainment and maintenance of the naaqs
==== clean air act amendments of 1990 ====
another set of major amendments to the clean air act occurred in 1990 (1990 caaa) the 1990 caaa substantially increased the authority and responsibility of the federal government new regulatory programs were authorized for control of acid deposition (acid rain) and for the issuance of stationary source operating permits the neshaps were incorporated into a greatly expanded program for controlling toxic air pollutants the provisions for attainment and maintenance of naaqs were substantially modified and expanded other revisions included provisions regarding stratospheric ozone protection increased enforcement authority and expanded research programs
==== milestones ====
some of the principal milestones in the evolution of the clean air act are as follows:
the air pollution control act of 1955
first federal air pollution legislation
funded research on scope and sources of air pollution
clean air act of 1963
authorized a national program to address air pollution
authorized research into techniques to minimize air pollution
air quality act of 1967
authorized enforcement procedures involving interstate transport of pollutants
expanded research activities
clean air act of 1970
established national ambient air quality standards
established requirements for state implementation plans to achieve them
establishment of new source performance standards for new and modified stationary sources
establishment of national emission standards for hazardous air pollutants
increased enforcement authority
authorized control of motor vehicle emissions
1977 amendments to the clean air act of 1970
authorized provisions related to prevention of significant deterioration
authorized provisions relating to non-attainment areas
1990 amendments to the clean air act of 1970
authorized programs for acid deposition control
authorized controls for 189 toxic pollutants including those previously regulated by the national emission standards for hazardous air pollutants
established permit program requirements
expanded and modified provisions concerning national ambient air quality standards
expanded and modified enforcement authority
=== regulations ===
since the initial establishment of six mandated criteria pollutants (ozone particulate matter carbon monoxide nitrogen oxides sulfur dioxide and lead) advancements in testing and monitoring have led to the discovery of many other significant air pollutants
however with the act in place and its many improvements the u.s has seen many pollutant levels and associated cases of health complications drop according to the epa the 1990 clean air act amendments has prevented or will prevent:
this chart shows the health benefits of the clean air act programs that reduce levels of fine particles and ozone
in 1997 epa tightened the naaqs regarding permissible levels of the ground-level ozone that make up smog and the fine airborne particulate matter that makes up soot the decision came after months of public review of the proposed new standards as well as long and fierce internal discussion within the clinton administration leading to the most divisive environmental debate of that decade the new regulations were challenged in the courts by industry groups as a violation of the u.s constitution's nondelegation principle and eventually landed in the supreme court of the united states whose 2001 unanimous ruling in whitman v american trucking ass'ns inc largely upheld epa's actions
the clean air act (caa or act) directs epa to establish national ambient air quality standards (naaqs) for pollutants at levels that will protect public health epa and american lung association promoted the 2011 cross state air pollution rule (csapr) to control ozone and fine particles aim was to cut emissions half from 2005 to 2014 it was claimed to prevent each year 400,000 asthma cases and save ca 2m work and schooldays lost by respiratory illness some states (e.g texas) cities and power companies sued the case (epa v eme homer city generation) the appeals-court judges decided by two to one that the rule is too strict based on appeals the power companies were allowed to continue thousands of persons respiratory illnesses prolonged time in the usa according to the economist (2013) the supreme court decision may affect how the epa regulates other pollutants including the greenhouse gases
== roles of the federal government and states ==
although the 1990 clean air act is a federal law covering the entire country the states do much of the work to carry out the act the epa has allowed the individual states to elect responsibility for compliance with and regulation of the caa within their own borders in exchange for funding for example a state air pollution agency holds a hearing on a permit application by a power or chemical plant or fines a company for violating air pollution limits however election is not mandatory and in some cases states have chosen to not accept responsibility for enforcement of the act and force the epa to assume those duties
in order to take over compliance with the caa the states must write and submit a state implementation plan (sip) to the epa for approval a state implementation plan is a collection of the regulations a state will use to clean up polluted areas the states are obligated to notify the public of these plans through hearings that offer opportunities to comment in the development of each state implementation plan the sip becomes the state's legal guide for local enforcement of the caa for example rhode island law requires compliance with the federal caa through the sip the sip delegates permitting and enforcement responsibility to the state department of environmental management (ri-dem)
the federal law recognizes that states should lead in carrying out the clean air act because pollution control problems often require special understanding of local industries geography housing patterns etc however states are not allowed to have weaker pollution controls than the national minimum criteria set by epa epa must approve each sip and if a sip isn't acceptable epa can take over caa enforcement in that state
the united states government through the epa assists the states by providing scientific research expert studies engineering designs and money to support clean air programs
metropolitan planning organizations must approve all federally funded transportation projects in a given urban area if the mpo's plans do not federal highway administration and the federal transit administration have the authority to withhold funds if the plans do not conform with federal requirements including air quality standards in 2010 the epa directly fined the san joaquin valley air pollution control district $29 million for failure to meet ozone standards resulting in fees for county drivers and businesses this was the results of a federal appeals court case that required the epa to continue enforce older stronger standards and spurred debate in congress over amending the act
=== state programs ===
many states or concerned citizens of the state have established their own programs to help promote pollution clean-up strategies
for example,(in alphabetical order by state)
california - california's clean air project - designed to create a smoke-free gaming atmosphere in tribal casinos
georgia - the clean air campaign
illinois - illinois citizens for clean air and water - coalition of farmers and other citizens to reduce harmful effects of large-scale livestock production methods
new york - clean air ny
oklahoma - "breathe easy" - oklahoma statutes on smoking in public places and indoor workplaces (effective november 1 2010)
texas - drive clean across texas
virginia - virginia clean cities inc
== interstate air pollution ==
air pollution often travels from its source in one state to another state in many metropolitan areas people live in one state and work or shop in another; air pollution from cars and trucks may spread throughout the interstate area the 1990 clean air act provides for interstate commissions on air pollution control which are to develop regional strategies for cleaning up air pollution the 1990 amendments include other provisions to reduce interstate air pollution
the acid rain program created under title iv of the act authorizes emissions trading to reduce the overall cost of controlling emissions of sulfur dioxide
== leak detection and repair ==
the act requires industrial facilities to implement a leak detection and repair (ldar) program to monitor and audit a facility's fugitive emissions of volatile organic compounds (voc) the program is intended to identify and repair components such as valves pumps compressors flanges connectors and other components that may be leaking these components are the main source of the fugitive voc emissions
testing is done manually using a portable vapor analyzer that read in parts per million (ppm) monitoring frequency and the leak threshold is determined by various factors such as the type of component being tested and the chemical running through the line moving components such as pumps and agitators are monitored more frequently than non-moving components such as flanges and screwed connectors the regulations require that when a leak is detected the component be repaired within a set amount of days most facilities get 5 days for an initial repair attempt with no more than 15 days for a complete repair allowances for delaying the repairs beyond the allowed time are made for some components where repairing the component requires shutting process equipment down
== application to greenhouse gas emissions ==
epa began regulating greenhouse gases (ghgs) from mobile and stationary sources of air pollution under the clean air act for the first time on january 2 2011 standards for mobile sources have been established pursuant to section 202 of the caa and ghgs from stationary sources are controlled under the authority of part c of title i of the act
below is a table for the sources of greenhouse gases taken from data in 2008 of all greenhouse gases about 76 percent of the sources are manageable under the caa marked with an asterisk (*) all others are regulated independently if at all
== see also ==
air quality law
united states environmental law
alan carlin controversy over the epa carbon dioxide endangerment finding
commission on risk assessment and risk management
emission standard
emissions trading
encyclopedia of earth
environmental policy of the united states
startups shutdowns and malfunctions
the center for clean air policy (in the us)
== references ==
== external links ==
works related to clean air act at wikisource
epa's the plain english guide to the clean air act
epa enforcement and compliance history online
