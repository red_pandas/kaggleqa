the compound hydrogen chloride has the chemical formula hcl at room temperature it is a colorless gas which forms white fumes of hydrochloric acid upon contact with atmospheric humidity hydrogen chloride gas and hydrochloric acid are important in technology and industry hydrochloric acid the aqueous solution of hydrogen chloride is also commonly given the formula hcl
== chemistry ==
hydrogen chloride is a diatomic molecule consisting of a hydrogen atom h and a chlorine atom cl connected by a covalent single bond since the chlorine atom is much more electronegative than the hydrogen atom the covalent bond between the two atoms is quite polar consequently the molecule has a large dipole moment with a negative partial charge at the chlorine atom and a positive partial charge + at the hydrogen atom in part because of its high polarity hcl is very soluble in water (and in other polar solvents)
upon contact h2o and hcl combine to form hydronium cations h3o+ and chloride anions cl through a reversible chemical reaction:
hcl + h2o h3o+ + cl
the resulting solution is called hydrochloric acid and is a strong acid the acid dissociation or ionization constant ka is large which means hcl dissociates or ionizes practically completely in water even in the absence of water hydrogen chloride can still act as an acid for example hydrogen chloride can dissolve in certain other solvents such as methanol protonate molecules or ions and serve as an acid-catalyst for chemical reactions where anhydrous (water-free) conditions are desired
hcl + ch3oh ch3o+h2 + cl
because of its acidic nature hydrogen chloride is corrosive particularly in the presence of moisture
=== structure and properties ===
frozen hcl undergoes phase transition at 98.4 k x-ray powder diffraction of the frozen material shows that the material changes from an orthorhombic structure to a cubic one during this transition in both structures the chlorine atoms are in a face-centered array however the hydrogen atoms could not be located analysis of spectroscopic and dielectric data and determination of the structure of dcl (deuterium chloride) indicates that hcl forms zigzag chains in the solid as does hf (see figure on right)
the infrared spectrum of gaseous hydrogen chloride shown below consists of a number of sharp absorption lines grouped around 2886 cm1 (wavelength ~3.47 m) at room temperature almost all molecules are in the ground vibrational state v = 0 to promote an hcl molecule to the v = 1 state we would expect to see an infrared absorption about 2880 cm1 this absorption corresponding to the q-branch is not observed due to it being forbidden by symmetry instead two sets of signals (p- and r-branches) are seen owing to rotation of the molecules because of quantum mechanical selection rules only certain rotational modes are permitted they are characterized by the rotational quantum number j = 0 1 2 3  selection rules state that j is only able to take values of 1
e(j) = hbj(j+1)
the value of b is much smaller than e such that a much smaller amount of energy is required to rotate the molecule; for a typical molecule this lies within the microwave region however the vibrational energy of hcl molecule places its absorptions within the infrared region allowing a spectrum showing the rovibrational modes of this molecule to be easily collected using an ordinary infrared spectrometer with a conventional gas cell
naturally abundant chlorine consists of two isotopes 35cl and 37cl in a ratio of approximately 3:1 while the spring constants are very similar the reduced masses are different causing significant differences in the rotational energy thus doublets are observed on close inspection of each absorption line weighted in the same ratio of 3:1
== production ==
most hydrogen chloride produced on an industrial scale is used for hydrochloric acid production
=== direct synthesis ===
in the chlor-alkali industry brine (mixture of sodium chloride and water) solution is electrolyzed producing chlorine (cl2) sodium hydroxide and hydrogen (h2) the pure chlorine gas can be combined with hydrogen to produce hydrogen chloride in the presence of uv light:
cl2(g) + h2(g) 2 hcl(g)
as the reaction is exothermic the installation is called an hcl oven or hcl burner the resulting hydrogen chloride gas is absorbed in deionized water resulting in chemically pure hydrochloric acid this reaction can give a very pure product e.g for use in the food industry
=== organic synthesis ===
the largest production of hydrochloric acid is integrated with the formation of chlorinated and fluorinated organic compounds e.g. teflon freon and other cfcs as well as chloroacetic acid and pvc often this production of hydrochloric acid is integrated with captive use of it on-site in the chemical reactions hydrogen atoms on the hydrocarbon are replaced by chlorine atoms whereupon the released hydrogen atom recombines with the spare atom from the chlorine molecule forming hydrogen chloride fluorination is a subsequent chlorine-replacement reaction producing again hydrogen chloride:
rh + cl2 rcl + hcl
rcl + hf rf + hcl
the resulting hydrogen chloride gas is either reused directly or absorbed in water resulting in hydrochloric acid of technical or industrial grade
=== laboratory methods ===
small amounts of hcl gas for laboratory use can be generated in an hcl generator by dehydrating hydrochloric acid with either sulfuric acid or anhydrous calcium chloride alternatively hcl can be generated by the reaction of sulfuric acid with sodium chloride:
nacl + h2so4 nahso4 + hcl
this reaction occurs at room temperature provided there is nacl remaining in the generator and it is heated above 200 c the reaction proceeds further:
nacl + nahso4 hcl + na2so4
for such generators to function the reagents should be dry
hcl can also be prepared by the hydrolysis of certain reactive chloride compounds such as phosphorus chlorides thionyl chloride (socl2) and acyl chlorides for example cold water can be gradually dripped onto phosphorus pentachloride (pcl5) to give hcl:
pcl5 + h2o pocl3 + 2 hcl
high-purity streams of the gas require lecture bottles or cylinders both of which can be expensive in comparison the use of a generator requires only apparatus and materials commonly available in a laboratory
== applications ==
most hydrogen chloride is used in the production of hydrochloric acid it is also an important reagent in other industrial chemical transformations e.g.:
hydrochlorination of rubber
production of vinyl and alkyl chlorides
in the semiconductor industry it is used to both etch semiconductor crystals and to purify silicon via trichlorosilane (sihcl3)
it may also be used to treat cotton to delint it and to separate it from wool
in the laboratory anhydrous forms of the gas are particularly useful for generating chloride-based lewis acids which must be absolutely dry for their lewis sites to function it can also be used to dry the corresponding hydrated forms of these materials by passing it over as they are heated; the materials would otherwise fume hcl(g) themselves and decompose neither can these hydrates be dried using standard desiccator methods
== history ==
alchemists of the middle ages recognized that hydrochloric acid (then known as spirit of salt or acidum salis) released vaporous hydrogen chloride which was called marine acid air in the 17th century johann rudolf glauber used salt (sodium chloride) and sulfuric acid for the preparation of sodium sulfate releasing hydrogen chloride gas (see production below) in 1772 carl wilhelm scheele also reported this reaction and is sometimes credited with its discovery joseph priestley prepared hydrogen chloride in 1772 and in 1810 humphry davy established that it is composed of hydrogen and chlorine
during the industrial revolution demand for alkaline substances such as soda ash increased and nicolas leblanc developed a new industrial-scale process for producing the soda ash in the leblanc process salt was converted to soda ash using sulfuric acid limestone and coal giving hydrogen chloride as by-product initially this gas was vented to air but the alkali act of 1863 prohibited such release so then soda ash producers absorbed the hcl waste gas in water producing hydrochloric acid on an industrial scale later the hargreaves process was developed which is similar to the leblanc process except sulfur dioxide water and air are used instead of sulfuric acid in a reaction which is exothermic overall in the early 20th century the leblanc process was effectively replaced by the solvay process which did not produce hcl however hydrogen chloride production continued as a step in hydrochloric acid production
historical uses of hydrogen chloride in the 20th century include hydrochlorinations of alkynes in producing the chlorinated monomers chloroprene and vinyl chloride which are subsequently polymerized to make polychloroprene (neoprene) and polyvinyl chloride (pvc) respectively in the production of vinyl chloride acetylene (c2h2) is hydrochlorinated by adding the hcl across the triple bond of the c2h2 molecule turning the triple into a double bond yielding vinyl chloride
the "acetylene process" used until the 1960s for making chloroprene starts out by joining two acetylene molecules and then adds hcl to the joined intermediate across the triple bond to convert it to chloroprene as shown here:
this "acetylene process" has been replaced by a process which adds cl2 to one of the double bonds in 1,3-butadiene instead and subsequent elimination produces hcl instead as well as chloroprene
== safety ==
hydrogen chloride forms corrosive hydrochloric acid on contact with water found in body tissue inhalation of the fumes can cause coughing choking inflammation of the nose throat and upper respiratory tract and in severe cases pulmonary edema circulatory system failure and death skin contact can cause redness pain and severe skin burns hydrogen chloride may cause severe burns to the eye and permanent eye damage
the gas being strongly hydrophilic can be easily scrubbed from the exhaust gases of a reaction by bubbling it through water producing useful hydrochloric acid as a byproduct
any equipment handling hydrogen chloride gas must be checked on a routine basis; particularly valve stems and regulators the gas requires the use of specialized materials on all wetted parts of the flow path as it will interact with or corrode numerous materials hydrochloric acid alone will not; such as stainless and regular polymers
the occupational safety and health administration and the national institute for occupational safety and health have established occupational exposure limits for hydrogen chloride at a ceiling of 5 ppm (7 mg/m3)
== see also ==
chloride inorganic salts of hydrochloric acid
hydrochloride organic salts of hydrochloric acid
gastric acid hydrochloric acid secreted into the stomach to aid digestion of proteins
== references ==
== external links ==
international chemical safety card 0163
thames & kosmos chem c2000 experiment manual
