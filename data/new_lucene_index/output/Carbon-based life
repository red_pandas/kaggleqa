carbon forms the key component for all known life on earth complex molecules are made up of carbon bonded with other elements especially oxygen hydrogen and nitrogen and carbon is able to bond with all of these because of its four valence electrons carbon is abundant on earth it is also lightweight and relatively small in size making it easier for enzymes to manipulate carbon molecules it is often assumed in astrobiology that if life exists somewhere else in the universe it will also be carbon based critics refer to this assumption as carbon chauvinism
== characteristics ==
"what we normally think of as 'life' is based on chains of carbon atoms with a few other atoms such as nitrogen or phosphorus" per stephen hawking in a 2008 lecture "carbon [...] has the richest chemistry." the most important characteristics of carbon as a basis for the chemistry of life are that it has four valence bonds that the energy required to make or break a bond is at an appropriate level for building molecules which are stable and reactive because carbon atoms bond readily to other carbon atoms allows for the building of arbitrarily long complex molecules and polymers
== other candidates ==
there are not many other elements which even appear to be promising candidates for supporting life for example processes such as metabolism; the most frequently suggested alternative is silicon it is in the same group in the periodic table of elements has four valence bonds bonds to itself generally in the form of crystal lattices rather than long chains silicon compounds do not support the ability to readily re-combine in different permutations in a manner that would plausibly support lifelike processes
== key molecules ==
the most notable groups of chemicals used in the processes of living organisms include:
proteins which are the building blocks from which the structures of living organisms are constructed (this includes almost all enzymes which catalyse organic chemical reactions)
nucleic acids which carry genetic information
carbohydrates which store energy in a form that can be used by living cells
lipids which also store energy but in a more concentrated form and which may be stored for extended periods in the bodies of animals
== fiction ==
silicon has been a theme of non-carbon-based-life since it also has 4 bonding sites and is just below carbon on the periodic table of the elements this means silicon is very similar to carbon in its chemical characteristics in cinematic and literary science fiction when man-made machines cross from nonliving to living this new form would be an example of non-carbon-based life since the advent of the microprocessor in the late 1960s these machines are often classed as "silicon-based life" another example of "silicon-based life" is the episode "the devil in the dark" from star trek: the original series where a living rock creature's biochemistry is based on silicon
== see also ==
hypothetical types of biochemistry
chonps mnemonic acronym for the most common elements in living organisms: carbon hydrogen oxygen and nitrogen phosphorus sulfur
== references ==
== external links ==
"encyclopedia of astrobiology astronomy & spaceflight" retrieved 2006-03-14
"school of chemistry university of bristol united kingdom" retrieved 2000-01-01
