nitric acid (hno3) also known as aqua fortis and spirit of niter is a highly corrosive mineral acid
the pure compound is colorless but older samples tend to acquire a yellow cast due to decomposition into oxides of nitrogen and water most commercially available nitric acid has a concentration of 68% in water when the solution contains more than 86% hno3 it is referred to as fuming nitric acid depending on the amount of nitrogen dioxide present fuming nitric acid is further characterized as white fuming nitric acid or red fuming nitric acid at concentrations above 95%
nitric acid is the primary reagent used for nitration the addition of a nitro group typically to an organic molecule while some resulting nitro compounds are shock- and thermally-sensitive explosives a few are stable enough to be used in munitions and demolition while others are still more stable and used as pigments in inks and dyes nitric acid is also commonly used as a strong oxidizing agent
== physical and chemical propertiesedit ==
commercially available nitric acid is an azeotrope with water at a concentration of 68% hno3 which is the ordinary concentrated nitric acid of commerce this solution has a boiling temperature of 120.5 c at 1 atm two solid hydrates are known; the monohydrate (hno3h2o) and the trihydrate (hno33h2o)
nitric acid of commercial interest usually consists of the maximum boiling azeotrope of nitric acid and water which is approximately 68% hno3 (approx 15 molar) this is considered concentrated or technical grade while reagent grades are specified at 70% hno3 the density of concentrated nitric acid is 1.42 g/ml an older density scale is occasionally seen with concentrated nitric acid specified as 42 baum
=== contamination with nitrogen dioxideedit ===
nitric acid is subject to thermal or light decomposition: 4 hno3 2 h2o + 4 no2 + o2 this reaction may give rise to some non-negligible variations in the vapor pressure above the liquid because the nitrogen oxides produced dissolve partly or completely in the acid
the nitrogen dioxide (no2) remains dissolved in the nitric acid coloring it yellow or even red at higher temperatures while the pure acid tends to give off white fumes when exposed to air acid with dissolved nitrogen dioxide gives off reddish-brown vapors leading to the common name "red fuming acid" or "fuming nitric acid" the most concentrated form of nitric acid at standard temperature and pressure (stp) nitrogen oxides (nox) are soluble in nitric acid
=== fuming nitric acidedit ===
a commercial grade of fuming nitric acid contains 90% hno3 and has a density of 1.50 g/ml this grade is much used in the explosives industry it is not as volatile nor as corrosive as the anhydrous acid and has the approximate concentration of 21.4 molar
red fuming nitric acid or rfna contains substantial quantities of dissolved nitrogen dioxide (no2) leaving the solution with a reddish-brown color due to the dissolved nitrogen dioxide the density of red fuming nitric acid is lower at 1.490 g/ml
an inhibited fuming nitric acid (either iwfna or irfna) can be made by the addition of 0.6 to 0.7% hydrogen fluoride (hf) this fluoride is added for corrosion resistance in metal tanks the fluoride creates a metal fluoride layer that protects the metal
=== anhydrous nitric acidedit ===
white fuming nitric acid pure nitric acid or wfna is very close to anhydrous nitric acid it is available as 99.9% nitric acid by assay one specification for white fuming nitric acid is that it has a maximum of 2% water and a maximum of 0.5% dissolved no2 anhydrous nitric acid has a density of 1.513 g/ml and has the approximate concentration of 24 molar anhydrous nitric acid is a colorless mobile liquid with a density of 1.512 g/cm3 which solidifies at 42 c to form white crystals as it decomposes to no2 and water it obtains a yellow tint it boils at 83 c it is usually stored in a glass shatterproof amber bottle with twice the volume of head space to allow for pressure build up when received the pressure must be released and repeated monthly until finished
=== structure and bondingedit ===
the molecule is planar two of the n-o bonds are equivalent and relatively short (this can be explained by theories of resonance the canonical forms show double bond character in these two bonds causing them to be shorter than typical n-o bonds.) and the third n-o bond is elongated because the o is also attached to a proton
== reactionsedit ==
=== acid-base propertiesedit ===
nitric acid is normally considered to be a strong acid at ambient temperatures there is some disagreement over the value of the acid dissociation constant though the pka value is usually reported as less than 1 this means that the nitric acid in diluted solution is fully dissociated except in extremely acidic solutions the pka value rises to 1 at a temperature of 250 c
nitric acid can act as a base with respect to an acid such as sulfuric acid:
hno3 + 2h2so4 no2+ + h3o+ + 2hso4; k ~ 22
the nitronium ion no2+ is the active reagent in aromatic nitration reactions since nitric acid has both acidic and basic properties it can undergo an autoprotolysis reaction similar to the self-ionization of water:
2hno3 no2+ + no3 + h2o
=== reactions with metalsedit ===
nitric acid reacts with most metals but the details depend on the concentration of the acid and the nature of the metal dilute nitric acid behaves as a typical acid in its reaction with most metals magnesium manganese and zinc liberate h2 others give the nitrogen oxides
nitric acid can oxidize non-active metals such as copper and silver with these non-active or less electropositive metals the products depend on temperature and the acid concentration for example copper reacts with dilute nitric acid at ambient temperatures with a 3:8 stoichiometry:
3 cu + 8 hno3 3 cu2+ + 2 no + 4 h2o + 6 no3
the nitric oxide produced may react with atmospheric oxygen to give nitrogen dioxide with more concentrated nitric acid nitrogen dioxide is produced directly in a reaction with 1:4 stoichiometry
cu + 4 h+ + 2 no3 cu2+ + 2 no2 + 2 h2o
upon reaction with nitric acid most metals give the corresponding nitrates some metalloids and metals give the oxides; for instance sn as sb and ti are oxidized into sno2 as2o5 sb2o5 and tio2 respectively
some precious metals such as pure gold and platinum group metals do not react with nitric acid though pure gold does react with aqua regia a mixture of concentrated nitric acid and hydrochloric acid however some less noble metals (ag cu ) present in some gold alloys relatively poor in gold such as colored gold can be easily oxidized and dissolved by nitric acid leading to colour changes of the gold-alloy surface nitric acid is used as a cheap means in jewelry shops to quickly spot low-gold alloys (< 14 carats) and to rapidly assess the gold purity
being a powerful oxidizing agent nitric acid reacts violently with many non-metallic compounds and the reactions may be explosive depending on the acid concentration temperature and the reducing agent involved the end products can be variable reaction takes place with all metals except the noble metals series and certain alloys as a general rule oxidizing reactions occur primarily with the concentrated acid favoring the formation of nitrogen dioxide (no2) however the powerful oxidizing properties of nitric acid are thermodynamic in nature but sometimes its oxidation reactions are rather kinetically non-favored the presence of small amounts of nitrous acid (hno2) greatly enhance the rate of reaction
although chromium (cr) iron (fe) and aluminium (al) readily dissolve in dilute nitric acid the concentrated acid forms a metal oxide layer that protects the bulk of the metal from further oxidation the formation of this protective layer is called passivation typical passivation concentrations range from 2050% by volume (see astm a967-05) metals which are passivated by concentrated nitric acid are iron cobalt chromium nickel and aluminium
=== reactions with non-metalsedit ===
being a powerful oxidizing acid nitric acid reacts violently with many organic materials and the reactions may be explosive the hydroxyl group will typically strip a hydrogen from the organic molecule to form water and the remaining nitro group takes the hydrogen's place nitration of organic compounds with nitric acid is the primary method of synthesis of many common explosives such as nitroglycerin and trinitrotoluene (tnt) as very many less stable byproducts are possible these reactions must be carefully thermally controlled and the byproducts removed to isolate the desired product
reaction with non-metallic elements with the exceptions of nitrogen oxygen noble gases silicon and halogens other than iodine usually oxidizes them to their highest oxidation states as acids with the formation of nitrogen dioxide for concentrated acid and nitric oxide for dilute acid
c + 4 hno3 co2 + 4 no2 + 2 h2o
or
3 c + 4 hno3 3 co2 + 4 no + 2 h2o
concentrated nitric acid oxidizes i2 p4 and s8 into hio3 h3po4 and h2so4 respectively
=== xanthoproteic testedit ===
nitric acid reacts with proteins to form yellow nitrated products this reaction is known as the xanthoproteic reaction this test is carried out by adding concentrated nitric acid to the substance being tested and then heating the mixture if proteins that contain amino acids with aromatic rings are present the mixture turns yellow upon adding a strong base such as liquid ammonia the color turns orange these color changes are caused by nitrated aromatic rings in the protein xanthoproteic acid is formed when the acid contacts epithelial cells and is indicative of inadequate safety precautions when handling nitric acid
== productionedit ==
nitric acid is made by reaction of nitrogen dioxide (no2) with water
3 no2 + h2o 2 hno3 + no
normally the nitric oxide produced by the reaction is reoxidized by the oxygen in air to produce additional nitrogen dioxide
bubbling nitrogen dioxide through hydrogen peroxide can help to improve acid yield
2 no2 + h2o2 2 hno3
commercial grade nitric acid solutions are usually between 52% and 68% nitric acid production of nitric acid is via the ostwald process named after german chemist wilhelm ostwald in this process anhydrous ammonia is oxidized to nitric oxide in the presence of platinum or rhodium gauze catalyst at a high temperature of about 500 k and a pressure of 9 bar
4 nh3 (g) + 5 o2 (g) 4 no (g) + 6 h2o (g) (h = 905.2 kj)
nitric oxide is then reacted with oxygen in air to form nitrogen dioxide
2 no (g) + o2 (g) 2 no2 (g) (h = 114 kj/mol)
this is subsequently absorbed in water to form nitric acid and nitric oxide
3 no2 (g) + h2o (l) 2 hno3 (aq) + no (g) (h = 117 kj/mol)
the nitric oxide is cycled back for reoxidation alternatively if the last step is carried out in air:
4 no2 (g) + o2 (g) + 2 h2o (l) 4 hno3 (aq)
the aqueous hno3 obtained can be concentrated by distillation up to about 68% by mass further concentration to 98% can be achieved by dehydration with concentrated h2so4 by using ammonia derived from the haber process the final product can be produced from nitrogen hydrogen and oxygen which are derived from air and natural gas as the sole feedstocks
prior to the introduction of the haber process for the production of ammonia in 1913 nitric acid was produced using the birkelandeyde process also known as the arc process this process is based upon the oxidation of atmospheric nitrogen by atmospheric oxygen to nitric oxide at very high temperatures an electric arc was used to provide the high temperatures and yields of up to 4% nitric oxide were obtained the nitric oxide was cooled and oxidized by the remaining atmospheric oxygen to nitrogen dioxide and this was subsequently absorbed in dilute nitric acid the process was very energy intensive and was rapidly displaced by the ostwald process once cheap ammonia became available
=== laboratory synthesisedit ===
in laboratory nitric acid can be made by thermal decomposition of copper(ii) nitrate producing nitrogen dioxide and oxygen gases which are then passed through water to give nitric acid
2 cu(no3)2 2 cuo (s) + 4 no2 (g) + o2 (g)
an alternate route is by reaction of approximately equal masses of any nitrate salt such as sodium nitrate with 96% sulfuric acid (h2so4) and distilling this mixture at nitric acid's boiling point of 83 c a nonvolatile residue of the metal sulfate remains in the distillation vessel the red fuming nitric acid obtained may be converted to the white nitric acid
2 nano3 + h2so4 2 hno3 + na2so4
the dissolved nox are readily removed using reduced pressure at room temperature (1030 min at 200 mmhg or 27 kpa) to give white fuming nitric acid this procedure can also be performed under reduced pressure and temperature in one step in order to produce less nitrogen dioxide gas
dilute nitric acid may be concentrated by distillation up to 68% acid which is a maximum boiling azeotrope containing 32% water in the laboratory further concentration involves distillation with either sulfuric acid or magnesium nitrate which act as dehydrating agents such distillations must be done with all-glass apparatus at reduced pressure to prevent decomposition of the acid industrially highly concentrated nitric acid is produced by dissolving additional nitrogen dioxide in 68% nitric acid in an absorption tower dissolved nitrogen oxides are either stripped in the case of white fuming nitric acid or remain in solution to form red fuming nitric acid more recently electrochemical means have been developed to produce anhydrous acid from concentrated nitric acid feedstock
== usesedit ==
the main industrial use of nitric acid is for the production of fertilizers nitric acid is neutralized with ammonia to give ammonium nitrate this application consumes 7580% of the 26m tons produced annually (1987) the other main applications are for the production of explosives nylon precursors and specialty organic compounds
=== precursor to organic nitrogen compoundsedit ===
in organic synthesis industrial and otherwise the nitro group is a versatile functional group most derivatives of aniline are prepared via nitration of aromatic compounds followed by reduction nitrations entail combining nitric and sulfuric acids to generate the nitronium ion which electrophilically reacts with aromatic compounds such as benzene many explosives e.g tnt are prepared in this way
the precursor to nylon adipic acid is produced on a large scale by oxidation of cyclohexanone and cyclohexanol with nitric acid
=== rocket propellantedit ===
nitric acid has been used in various forms as the oxidizer in liquid-fueled rockets these forms include red fuming nitric acid white fuming nitric acid mixtures with sulfuric acid and these forms with hf inhibitor irfna (inhibited red fuming nitric acid) was one of 3 liquid fuel components for the bomarc missile
=== niche usesedit ===
==== analytical reagentedit ====
in elemental analysis by icp-ms icp-aes gfaa and flame aa dilute nitric acid (0.5 to 5.0%) is used as a matrix compound for determining metal traces in solutions ultrapure trace metal grade acid is required for such determination because small amounts of metal ions could affect the result of the analysis
it is also typically used in the digestion process of turbid water samples sludge samples solid samples as well as other types of unique samples which require elemental analysis via icp-ms icp-oes icp-aes gfaa and flame atomic absorption spectroscopy typically these digestions use a 50% solution of the purchased hno
3 mixed with type 1 di water
in electrochemistry nitric acid is used as a chemical doping agent for organic semiconductors and in purification processes for raw carbon nanotubes
==== woodworkingedit ====
in a low concentration (approximately 10%) nitric acid is often used to artificially age pine and maple the color produced is a grey-gold very much like very old wax or oil finished wood (wood finishing)
==== etchant and cleaning agentedit ====
the corrosive effects of nitric acid are exploited for a number of specialty applications such as pickling stainless steel or cleaning silicon wafers in electronics
a solution of nitric acid water and alcohol nital is used for etching of metals to reveal the microstructure iso 14104 is one of the standards detailing this well known procedure
commercially available aqueous blends of 530% nitric acid and 1540% phosphoric acid are commonly used for cleaning food and dairy equipment primarily to remove precipitated calcium and magnesium compounds (either deposited from the process stream or resulting from the use of hard water during production and cleaning) the phosphoric acid content helps to passivate ferrous alloys against corrosion by the dilute nitric acid
nitric acid can be used as a spot test for alkaloids like lsd giving a variety of colours depending on the alkaloid
== safetyedit ==
nitric acid is a corrosive acid and a powerful oxidizing agent the major hazard posed by it is chemical burns as it carries out acid hydrolysis with proteins (amide) and fats (ester) which consequently decomposes living tissue (e.g skin and flesh) concentrated nitric acid stains human skin yellow due to its reaction with the keratin these yellow stains turn orange when neutralized systemic effects are unlikely however and the substance is not considered a carcinogen or mutagen
the standard first aid treatment for acid spills on the skin is as for other corrosive agents irrigation with large quantities of water washing is continued for at least ten to fifteen minutes to cool the tissue surrounding the acid burn and to prevent secondary damage contaminated clothing is removed immediately and the underlying skin washed thoroughly
being a strong oxidizing agent reactions of nitric acid with compounds such as cyanides carbides metallic powders can be explosive and those with many organic compounds such as turpentine are violent and hypergolic (i.e self-igniting) hence it should be stored away from bases and organics
== historyedit ==
the first mention of nitric acid is in pseudo-geber's de inventione veritatis wherein it is obtained by calcining a mixture of niter alum and blue vitriol it was again described by albert the great in the 13th century and by ramon lull who prepared it by heating niter and clay and called it "eau forte" (aqua fortis)
glauber devised a process to obtain it by distillate potassium nitrate with sulfuric acid in 1776 lavoisier showed that it contained oxygen and in 1785 henry cavendish determined its precise composition and showed that it could be synthesized by passing a stream of electric sparks through moist air
== referencesedit ==
== external linksedit ==
niosh pocket guide to chemical hazards
national pollutant inventory nitric acid fact sheet
calculators: surface tensions and densities molarities and molalities of aqueous nitric acid
