sea snail is a common name for snails that normally live in saltwater in other words marine gastropods the taxonomic class gastropoda also includes snails that live in other habitats such as land snails and freshwater snails many species of sea snails are edible and exploited as food sources by humans
sea snails are marine gastropods with shells those marine gastropods with no shells or only internal shells are variously known by other common names including sea slug sea hare nudibranch etc the diversity within sea snails is enormous many very different clades of gastropods are either dominated by or consist exclusively of sea snails because of this great variability generalization about the feeding reproduction habitat or other traits of sea snails is not possible instead individual clades families genera or species must be assessed
== shells ==
the shells of most species of sea snails are spirally coiled some though have conical shells and these are often referred to by the common name of limpets in one unusual family (juliidae) the shell of the snail has become two hinged plates closely resembling those of a bivalve; this family is sometimes called the "bivalved gastropods"
their shells are found in a variety of shapes and sizes but are normally very small those of living species of sea snails range in size from syrinx aruanus the largest living shelled gastropod species at 91 cm to minute species whose shells are less than 1 mm at adult size because the shells of sea snails are strong and durable in many cases as a group they are well represented in the fossil record
== anatomy ==
sea snails are a very large group of animals and a very diverse one most snails that live in saltwater respire using a gill or gills a few species though have a lung are intertidal and are active only at low tide when they can move around in the air these air-breathing species include false limpets in the family siphonariidae and another group of false limpets in the family trimusculidae
many but not all sea snails have an operculum
== human uses ==
a number of species of sea snails are used by humans for food including abalone conch limpets whelks (such as the north american busycon species and the north atlantic buccinum undatum) and periwinkles including littorina littorea
the shells of sea snails are often found by humans as seashells commonly wash up on beaches because the shells of many sea snails are attractive and durable they have been used by humans to make necklaces and other jewelry from prehistoric times to the current day
the shells of a few species of large sea snails within the vetigastropoda have a thick layer of nacre and have been used as a source of mother of pearl historically the button industry relied on these species for a number of years
== use by other animals ==
the shells of sea snails are used for protection by many kinds of hermit crabs a hermit crab carries the shell by grasping the central columella of the shell using claspers on the tip of its abdomen
== definition ==
determining whether some gastropods should be called sea snails is not always easy some species that live in brackish water (such as certain neritids) can be listed as either freshwater snails or marine snails and some species that live at or just above the high tide level (for example species in the genus truncatella) are sometimes considered to be sea snails and sometimes listed as land snails
== taxonomy ==
=== 2005 taxonomy ===
the following cladogram is an overview of the main clades of living gastropods based on the taxonomy of bouchet & rocroi (2005) with taxa that contain saltwater or brackish water species marked in boldface (some of the highlighted taxa consist entirely of marine species but some of them also contain freshwater or land species.)
clade patellogastropoda
clade vetigastropoda
clade cocculiniformia
clade neritimorpha
clade cycloneritimorpha
clade caenogastropoda
informal group architaenioglossa
clade sorbeoconcha
clade hypsogastropoda
clade littorinimorpha
informal group ptenoglossa
clade neogastropoda
clade heterobranchia
informal group lower heterobranchia
informal group opisthobranchia
clade cephalaspidea
clade thecosomata
clade gymnosomata
clade aplysiomorpha
group acochlidiacea
clade sacoglossa
group cylindrobullida
clade umbraculida
clade nudipleura
clade pleurobranchomorpha
clade nudibranchia
clade euctenidiacea
clade dexiarchia
clade pseudoeuctenidiacea
clade cladobranchia
clade euarminida
clade dendronotida
clade aeolidida
informal group pulmonata
informal group basommatophora
clade eupulmonata
clade systellommatophora
clade stylommatophora
clade elasmognatha
clade orthurethra
informal group sigmurethra
== cultural references ==
in the animated american tv series spongebob squarepants sea snails are featured as some of the main animals seen in the show and are the bikini bottom equivalent of cats the main character spongebob has a pet sea snail named gary the character's eyes are well developed and colorful similar to the eyes of species in the sea snail family strombidae
== see also ==
freshwater snail
land snail
sea slug
slug
== references ==
