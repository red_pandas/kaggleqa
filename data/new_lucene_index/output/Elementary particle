in particle physics an elementary particle or fundamental particle is a particle whose substructure is unknown thus it is unknown whether it is composed of other particles known elementary particles include the fundamental fermions (quarks leptons antiquarks and antileptons) which generally are "matter particles" and "antimatter particles" as well as the fundamental bosons (gauge bosons and the higgs boson) which generally are "force particles" that mediate interactions among fermions a particle containing two or more elementary particles is a composite particle
everyday matter is composed of atoms once presumed to be matter's elementary particlesatom meaning "indivisible" in greekalthough the atom's existence remained controversial until about 1910 as some leading physicists regarded molecules as mathematical illusions and matter as ultimately composed of energy soon subatomic constituents of the atom were identified as the 1930s opened the electron and the proton had been observed along with the photon the particle of electromagnetic radiation at that time the recent advent of quantum mechanics was radically altering the conception of particles as a single particle could seemingly span a field as would a wave a paradox still eluding satisfactory explanation
via quantum theory protons and neutrons were found to contain quarksup quarks and down quarksnow considered elementary particles and within a molecule the electron's three degrees of freedom (charge spin orbital) can separate via wavefunction into three quasiparticles (holon spinon orbiton) yet a free electronwhich not orbiting an atomic nucleus lacks orbital motionappears unsplittable and remains regarded as an elementary particle
around 1980 an elementary particle's status as indeed elementaryan ultimate constituent of substancewas mostly discarded for a more practical outlook embodied in particle physics' standard model science's most experimentally successful theory many elaborations upon and theories beyond the standard model including the extremely popular supersymmetry double the number of elementary particles by hypothesizing that each known particle associates with a "shadow" partner far more massive although all such superpartners remain undiscovered meanwhile an elementary boson mediating gravitationthe gravitonremains hypothetical
== overviewedit ==
all elementary particles aredepending on their spineither bosons or fermions these are differentiated via the spinstatistics theorem of quantum statistics particles of half-integer spin exhibit fermidirac statistics and are fermions particles of integer spin in other words full-integer exhibit boseeinstein statistics and are bosons
elementary fermions:
matter particles
quarks:
up down
charm strange
top bottom
leptons:
electron electron neutrino (a.k.a. "neutrino")
muon muon neutrino
tau tau neutrino
antimatter particles
antiquarks
antileptons
elementary bosons:
force particles (gauge bosons):
photon
gluon (numbering eight)
w+ w and z0 bosons
graviton (hypothetical)
scalar boson
higgs boson
a particle's mass is quantified in units of energy versus the electron's (electronvolts) through conversion of energy into mass any particle can be produced through collision of other particles at high energy although the output particle might not contain the input particles for instance matter creation from colliding photons likewise the composite fermions protons were collided at nearly light speed to produce the relatively more massive higgs boson the most massive elementary particle the top quark rapidly decays but apparently does not contain lighter particles
when probed at energies available in experiments particles exhibit spherical sizes in operating particle physics' standard model elementary particles are usually represented for predictive utility as point particles which as zero-dimensional lack spatial extension though extremely successful the standard model is limited to the microcosm by its omission of gravitation and has some parameters arbitrarily added but unexplained seeking to resolve those shortcomings string theory posits that elementary particles are ultimately composed of one-dimensional energy strings whose absolute minimum size is the planck length
== common elementary particlesedit ==
according to the current models of big bang nucleosynthesis the primordial composition of visible matter of the universe should be about 75% hydrogen and 25% helium-4 (in mass) neutrons are made up of one up and two down quark while protons are made of two up and one down quark since the other common elementary particles (such as electrons neutrinos or weak bosons) are so light or so rare when compared to atomic nuclei we can neglect their mass contribution to the observable universe's total mass therefore one can conclude that most of the visible mass of the universe consists of protons and neutrons which like all baryons in turn consist of up quarks and down quarks
some estimates imply that there are roughly 1080 baryons (almost entirely protons and neutrons) in the observable universe
the number of protons in the observable universe is called the eddington number
in terms of number of particles some estimates imply that nearly all the matter excluding dark matter occurs in neutrinos and that roughly 1086 elementary particles of matter exist in the visible universe mostly neutrinos other estimates imply that roughly 1097 elementary particles exist in the visible universe (not including dark matter) mostly photons gravitons and other massless force carriers
== standard modeledit ==
the standard model of particle physics contains 12 flavors of elementary fermions plus their corresponding antiparticles as well as elementary bosons that mediate the forces and the higgs boson which was reported on july 4 2012 as having been likely detected by the two main experiments at the lhc (atlas and cms) however the standard model is widely considered to be a provisional theory rather than a truly fundamental one since it is not known if it is compatible with einstein's general relativity there may be hypothetical elementary particles not described by the standard model such as the graviton the particle that would carry the gravitational force and sparticles supersymmetric partners of the ordinary particles
=== fundamental fermionsedit ===
the 12 fundamental fermionic flavours are divided into three generations of four particles each six of the particles are quarks the remaining six are leptons three of which are neutrinos and the remaining three of which have an electric charge of 1: the electron and its two cousins the muon and the tau
==== antiparticlesedit ====
there are also 12 fundamental fermionic antiparticles that correspond to these 12 particles for example the antielectron (positron) e+ is the electron's antiparticle and has an electric charge of +1
==== quarksedit ====
isolated quarks and antiquarks have never been detected a fact explained by confinement every quark carries one of three color charges of the strong interaction; antiquarks similarly carry anticolor color-charged particles interact via gluon exchange in the same way that charged particles interact via photon exchange however gluons are themselves color-charged resulting in an amplification of the strong force as color-charged particles are separated unlike the electromagnetic force which diminishes as charged particles separate color-charged particles feel increasing force
however color-charged particles may combine to form color neutral composite particles called hadrons a quark may pair up with an antiquark: the quark has a color and the antiquark has the corresponding anticolor the color and anticolor cancel out forming a color neutral meson alternatively three quarks can exist together one quark being "red" another "blue" another "green" these three colored quarks together form a color-neutral baryon symmetrically three antiquarks with the colors "antired" "antiblue" and "antigreen" can form a color-neutral antibaryon
quarks also carry fractional electric charges but since they are confined within hadrons whose charges are all integral fractional charges have never been isolated note that quarks have electric charges of either +2/3 or 1/3 whereas antiquarks have corresponding electric charges of either 2/3 or +1/3
evidence for the existence of quarks comes from deep inelastic scattering: firing electrons at nuclei to determine the distribution of charge within nucleons (which are baryons) if the charge is uniform the electric field around the proton should be uniform and the electron should scatter elastically low-energy electrons do scatter in this way but above a particular energy the protons deflect some electrons through large angles the recoiling electron has much less energy and a jet of particles is emitted this inelastic scattering suggests that the charge in the proton is not uniform but split among smaller charged particles: quarks
=== fundamental bosonsedit ===
in the standard model vector (spin-1) bosons (gluons photons and the w and z bosons) mediate forces whereas the higgs boson (spin-0) is responsible for the intrinsic mass of particles bosons differ from fermions in the fact that multiple bosons can occupy the same quantum state (pauli exclusion principle) also bosons can be either elementary like photons or a combination like mesons the spin of bosons are integers instead of half integers
==== gluonsedit ====
gluons mediate the strong interaction which join quarks and thereby form hadrons which are either baryons (three quarks) or mesons (one quark and one antiquark) protons and neutrons are baryons joined by gluons to form the atomic nucleus like quarks gluons exhibit colour and anticolourunrelated to the concept of visual colorsometimes in combinations altogether eight variations of gluons
==== electroweak bosonsedit ====
there are three weak gauge bosons: w+ w and z0; these mediate the weak interaction the w bosons are known for their mediation in nuclear decay the w converts a neutron into a proton then decay into an electron and electron antineutrino pair the z0 does not convert charge but rather changes momentum and is the only mechanism for elastically scattering neutrinos the weak gauge bosons were discovered due to momentum change in electrons from neutrino-z exchange the massless photon mediates the electromagnetic interaction these four gauge bosons form the electroweak interaction among elementary particles
==== higgs bosonedit ====
although the weak and electromagnetic forces appear quite different to us at everyday energies the two forces are theorized to unify as a single electroweak force at high energies this prediction was clearly confirmed by measurements of cross-sections for high-energy electron-proton scattering at the hera collider at desy the differences at low energies is a consequence of the high masses of the w and z bosons which in turn are a consequence of the higgs mechanism through the process of spontaneous symmetry breaking the higgs selects a special direction in electroweak space that causes three electroweak particles to become very heavy (the weak bosons) and one to remain massless (the photon) on 4 july 2012 after many years of experimentally searching for evidence of its existence the higgs boson was announced to have been observed at cern's large hadron collider peter higgs who first posited the existence of the higgs boson was present at the announcement the higgs boson is believed to have a mass of approximately 125 gev the statistical significance of this discovery was reported as 5-sigma which implies a certainty of roughly 99.99994% in particle physics this is the level of significance required to officially label experimental observations as a discovery research into the properties of the newly discovered particle continues
==== gravitonedit ====
the graviton is hypothesized to mediate gravitation but remains undiscovered and yet is sometimes included in tables of elementary particles its spin would be twothus a bosonand it would lack charge or mass besides mediating an extremely feeble force the graviton would have its own antiparticle and rapidly annihilate rendering its detection extremely difficult even if it exists
== beyond the standard modeledit ==
although experimental evidence overwhelmingly confirms the predictions derived from the standard model some of its parameters were added arbitrarily not determined by a particular explanation which remain mysteries for instance the hierarchy problem theories beyond the standard model attempt to resolve these shortcomings
=== grand unificationedit ===
one extension of the standard model attempts to combine the electroweak interaction with the strong interaction into a single 'grand unified theory' (gut) such a force would be spontaneously broken into the three forces by a higgs-like mechanism the most dramatic prediction of grand unification is the existence of x and y bosons which cause proton decay however the non-observation of proton decay at the super-kamiokande neutrino observatory rules out the simplest guts including su(5) and so(10)
=== supersymmetryedit ===
supersymmetry extends the standard model by adding another class of symmetries to the lagrangian these symmetries exchange fermionic particles with bosonic ones such a symmetry predicts the existence of supersymmetric particles abbreviated as sparticles which include the sleptons squarks neutralinos and charginos each particle in the standard model would have a superpartner whose spin differs by 1/2 from the ordinary particle due to the breaking of supersymmetry the sparticles are much heavier than their ordinary counterparts; they are so heavy that existing particle colliders would not be powerful enough to produce them however some physicists believe that sparticles will be detected by the large hadron collider at cern
=== string theoryedit ===
string theory is a model of physics where all "particles" that make up matter are composed of strings (measuring at the planck length) that exist in an 11-dimensional (according to m-theory the leading version) universe these strings vibrate at different frequencies that determine mass electric charge color charge and spin a string can be open (a line) or closed in a loop (a one-dimensional sphere like a circle) as a string moves through space it sweeps out something called a world sheet string theory predicts 1- to 10-branes (a 1-brane being a string and a 10-brane being a 10-dimensional object) that prevent tears in the "fabric" of space using the uncertainty principle (e.g. the electron orbiting a hydrogen atom has the probability albeit small that it could be anywhere else in the universe at any given moment)
string theory proposes that our universe is merely a 4-brane inside which exist the 3 space dimensions and the 1 time dimension that we observe the remaining 6 theoretical dimensions either are very tiny and curled up (and too small to be macroscopically accessible) or simply do not/cannot exist in our universe (because they exist in a grander scheme called the "multiverse" outside our known universe)
some predictions of the string theory include existence of extremely massive counterparts of ordinary particles due to vibrational excitations of the fundamental string and existence of a massless spin-2 particle behaving like the graviton
=== technicoloredit ===
technicolor theories try to modify the standard model in a minimal way by introducing a new qcd-like interaction this means one adds a new theory of so-called techniquarks interacting via so called technigluons the main idea is that the higgs-boson is not an elementary particle but a bound state of these objects
=== preon theoryedit ===
according to preon theory there are one or more orders of particles more fundamental than those (or most of those) found in the standard model the most fundamental of these are normally called preons which is derived from "pre-quarks" in essence preon theory tries to do for the standard model what the standard model did for the particle zoo that came before it most models assume that almost everything in the standard model can be explained in terms of three to half a dozen more fundamental particles and the rules that govern their interactions interest in preons has waned since the simplest models were experimentally ruled out in the 1980s
=== acceleron theoryedit ===
accelerons are the hypothetical subatomic particles that integrally link the newfound mass of the neutrino and to the dark energy conjectured to be accelerating the expansion of the universe
in theory neutrinos are influenced by a new force resulting from their interactions with accelerons dark energy results as the universe tries to pull neutrinos apart
== see alsoedit ==
asymptotic freedom
list of particles
physical ontology
quantum field theory
quantum gravity
quantum triviality
uv fixed point
== notesedit ==
== further readingedit ==
=== general readersedit ===
feynman r.p & weinberg s (1987) elementary particles and the laws of physics: the 1986 dirac memorial lectures cambridge univ press
ford kenneth w (2005) the quantum world harvard univ press
brian greene (1999) the elegant universe w.w.norton & company isbn 0-393-05858-1
john gribbin (2000) q is for quantum an encyclopedia of particle physics simon & schuster isbn 0-684-85578-x
oerter robert (2006) the theory of almost everything: the standard model the unsung triumph of modern physics plume
schumm bruce a (2004) deep down things: the breathtaking beauty of particle physics johns hopkins university press isbn 0-8018-7971-x
martinus veltman (2003) facts and mysteries in elementary particle physics world scientific isbn 981-238-149-x
frank close (2004) particle physics: a very short introduction oxford: oxford university press isbn 0-19-280434-0
seiden abraham (2005) particle physics a comprehensive introduction addison wesley isbn 0-8053-8736-6
=== textbooksedit ===
bettini alessandro (2008) introduction to elementary particle physics cambridge univ press isbn 978-0-521-88021-3
coughlan g d. j e dodd and b m gripaios (2006) the ideas of particle physics: an introduction for scientists 3rd ed cambridge univ press an undergraduate text for those not majoring in physics
griffiths david j (1987) introduction to elementary particles john wiley & sons isbn 0-471-60386-4
kane gordon l (1987) modern elementary particle physics perseus books isbn 0-201-11749-5
perkins donald h (2000) introduction to high energy physics 4th ed cambridge univ press
== external linksedit ==
the most important address about the current experimental and theoretical knowledge about elementary particle physics is the particle data group where different international institutions collect all experimental data and give short reviews over the contemporary theoretical understanding
particle data group
other pages are:
greene brian "elementary particles" the elegant universe nova (pbs)
particleadventure.org a well-made introduction also for non physicists
cerncourier: season of higgs and melodrama
pentaquark information page
interactions.org particle physics news
symmetry magazine a joint fermilab/slac publication
"sized matter: perception of the extreme unseen" michigan university project for artistic visualisation of subatomic particles
elementary particles made thinkable an interactive visualisation allowing physical properties to be compared
