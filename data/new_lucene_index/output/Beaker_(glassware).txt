a beaker is a simple container for stirring mixing and heating liquids commonly used in many laboratories beakers are generally cylindrical in shape with a flat bottom most also have a small spout (or "beak") to aid pouring as shown in the picture beakers are available in a wide range of sizes from one millilitre up to several litres
== structure & common uses ==
standard or "low-form" beakers typically have a height about 1.4 times the diameter the common low form with a spout was devised by john joseph griffin and is therefore sometimes called a griffin beaker these are the most universal character and are used for various purposes - from preparing solutions and decanting supernatant fluids to holding waste fluids prior to disposal to performing simple reactions in short low form beakers are likely to be used in some way when performing just about any chemical experiment
"tall-form" (b) beakers have a height about twice their diameter these are sometimes called berzelius beakers and are mostly used for titration
flat beakers (c) are often called crystallizers because most are used to perform crystallization but they are also often used as a vessel for use in hot-bath heating these beakers usually do not have a flat scale
a beaker is distinguished from a flask by having straight rather than sloping sides the exception to this definition is a slightly conical-sided beaker called a philips beaker
== materials ==
beakers are commonly made of glass (today usually borosilicate glass) but can also be in metal (such as stainless steel or aluminium) or certain plastics (notably polythene polypropylene ptfe) a common use for polypropylene beakers is gamma spectral analysis of liquid and solid samples
== shape ==
beakers are often graduated that is marked on the side with lines indicating the volume contained for instance a 250 ml beaker might be marked with lines to indicate 50 100 150 200 and 250 ml of volume these marks are not intended for obtaining a precise measurement of volume (a graduated cylinder or a volumetric flask would be a more appropriate instrument for such a task) but rather an estimation most beakers are accurate to within ~10%
the presence of a spout means that the beaker cannot have a lid however when in use beakers may be covered by a watch glass to prevent contamination or loss of the contents but allowing venting via the spout alternatively a beaker may be covered with another larger beaker that has been inverted though a watch glass is preferable
== see also ==
beaker (drinkware)
beaker (archaeology)
beaker (disambiguation)
volumetric flask
stirring rod
test tube
graduated cylinder
scoop
== references ==
== further reading ==
astm e960 - 93 (2008) standard specification for laboratory glass beakers
