biomass in ecology is the mass of living biological organisms in a given area or ecosystem at a given time biomass can refer to species biomass which is the mass of one or more species or to community biomass which is the mass of all species in the community it can include microorganisms plants or animals the mass can be expressed as the average mass per unit area or as the total mass in the community
how biomass is measured depends on why it is being measured sometimes the biomass is regarded as the natural mass of organisms in situ just as they are for example in a salmon fishery the salmon biomass might be regarded as the total wet weight the salmon would have if they were taken out of the water in other contexts biomass can be measured in terms of the dried organic mass so perhaps only 30% of the actual weight might count the rest being water for other purposes only biological tissues count and teeth bones and shells are excluded in some applications biomass is measured as the mass of organically bound carbon (c) that is present
apart from bacteria the total live biomass on earth is about 560 billion tonnes c and the total annual primary production of biomass is just over 100 billion tonnes c/yr however the total live biomass of bacteria may exceed that of plants and animals the total amount of dna base pairs on earth as a possible approximation of global biodiversity is estimated at 5.0 x 1037 and weighs 50 billion tonnes in comparison the total mass of the biosphere has been estimated to be as much as 4 ttc (trillion tons of carbon)
== ecological pyramidsedit ==
an ecological pyramid is a graphical representation that shows for a given ecosystem the relationship between biomass or biological productivity and trophic levels
a biomass pyramid shows the amount of biomass at each trophic level
a productivity pyramid shows the production or turn-over in biomass at each trophic level
an ecological pyramid provides a snapshot in time of an ecological community
the bottom of the pyramid represents the primary producers (autotrophs) the primary producers take energy from the environment in the form of sunlight or inorganic chemicals and use it to create energy-rich molecules such as carbohydrates this mechanism is called primary production the pyramid then proceeds through the various trophic levels to the apex predators at the top
when energy is transferred from one trophic level to the next typically only ten percent is used to build new biomass the remaining ninety percent goes to metabolic processes or is dissipated as heat this energy loss means that productivity pyramids are never inverted and generally limits food chains to about six levels however in oceans biomass pyramids can be wholly or partially inverted with more biomass at higher levels
== terrestrial biomassedit ==
terrestrial biomass generally decreases markedly at each higher trophic level (plants herbivores carnivores) examples of terrestrial producers are grasses trees and shrubs these have a much higher biomass than the animals that consume them such as deer zebras and insects the level with the least biomass are the highest predators in the food chain such as foxes and eagles
in a temperate grassland grasses and other plants are the primary producers at the bottom of the pyramid then come the primary consumers such as grasshoppers voles and bison followed by the secondary consumers shrews hawks and small cats finally the tertiary consumers large cats and wolves the biomass pyramid decreases markedly at each higher level
== ocean biomassedit ==
ocean biomass in a reversal of terrestrial biomass can increase at higher trophic levels in the ocean the food chain typically starts with phytoplankton and follows the course:
phytoplankton zooplankton predatory zooplankton filter feeders predatory fish
phytoplankton are the main primary producers at the bottom of the marine food chain phytoplankton use photosynthesis to convert inorganic carbon into protoplasm they are then consumed by microscopic animals called zooplankton
zooplankton comprise the second level in the food chain and includes small crustaceans such as copepods and krill and the larva of fish squid lobsters and crabs
in turn small zooplankton are consumed by both larger predatory zooplankters such as krill and by forage fish which are small schooling filter feeding fish this makes up the third level in the food chain
the fourth trophic level consists of predatory fish marine mammals and seabirds that consume forage fish examples are swordfish seals and gannets
apex predators such as orcas which can consume seals and shortfin mako sharks which can consume swordfish make up the fifth trophic level baleen whales can consume zooplankton and krill directly leading to a food chain with only three or four trophic levels
marine environments can have inverted biomass pyramids in particular the biomass of consumers (copepods krill shrimp forage fish) is larger than the biomass of primary producers this happens because the ocean's primary producers are tiny phytoplankton that grow and reproduce rapidly so a small mass can have a fast rate of primary production in contrast terrestrial primary producers grow and reproduce slowly
there is an exception with cyanobacteria marine cyanobacteria are the smallest known photosynthetic organisms; the smallest of all prochlorococcus is just 0.5 to 0.8 micrometres across prochlorococcus is possibly the most plentiful species on earth: a single millilitre of surface seawater may contain 100,000 cells or more worldwide there are estimated to be several octillion (~1027) individuals prochlorococcus is ubiquitous between 40n and 40s and dominates in the oligotrophic (nutrient poor) regions of the oceans the bacterium accounts for an estimated 20% of the oxygen in the earth's atmosphere and forms part of the base of the ocean food chain
== bacterial biomassedit ==
there are typically 50 million bacterial cells in a gram of soil and a million bacterial cells in a millilitre of fresh water in all it has been estimated that there are about five million trillion trillion or 5 1030 (5 nonillion) bacteria on earth with a total biomass equaling that of plants some researchers believe that the total biomass of bacteria exceeds that of all plants and animals
== global biomassedit ==
estimates for the global biomass of species and higher level groups are not always consistent across the literature apart from bacteria the total global biomass has been estimated at about 560 billion tonnes c most of this biomass is found on land with only 5 to 10 billion tonnes c found in the oceans on land there is about 1,000 times more plant biomass (phytomass) than animal biomass (zoomass) about 18% of this plant biomass is eaten by the land animals however in the ocean the animal biomass is nearly 30 times larger than the plant biomass most ocean plant biomass is eaten by the ocean animals
humans comprise about 100 million tonnes of the earth's dry biomass domesticated animals about 700 million tonnes and crops about 2 billion tonnes the most successful animal species in terms of biomass may well be antarctic krill euphausia superba with a fresh biomass approaching 500 million tonnes although domestic cattle may also reach these immense figures however as a group the small aquatic crustaceans called copepods may form the largest animal biomass on earth a 2009 paper in science estimates for the first time the total world fish biomass as somewhere between 0.8 and 2.0 billion tonnes it has been estimated that about 1% of the global biomass is due to phytoplankton and a staggering 25% is due to fungi
== global rate of productionedit ==
net primary production is the rate at which new biomass is generated mainly due to photosynthesis global primary production can be estimated from satellite observations satellites scan the normalised difference vegetation index (ndvi) over terrestrial habitats and scan sea-surface chlorophyll levels over oceans this results in 56.4 billion tonnes c/yr (53.8%) for terrestrial primary production and 48.5 billion tonnes c/yr for oceanic primary production thus the total photoautotrophic primary production for the earth is about 104.9 billion tonnes c/yr this translates to about 426 gc/m/yr for land production (excluding areas with permanent ice cover) and 140 gc/m/yr for the oceans
however there is a much more significant difference in standing stockswhile accounting for almost half of total annual production oceanic autotrophs account for only about 0.2% of the total biomass autotrophs may have the highest global proportion of biomass but they are closely rivaled or surpassed by microbes
terrestrial freshwater ecosystems generate about 1.5% of the global net primary production
some global producers of biomass in order of productivity rates are
== see alsoedit ==
biomass (as in bioproducts)
natural organic matter
productivity (ecology)
primary nutritional groups
standing stock
lake pohjalampi - a biomass manipulation study
list of harvested aquatic animals by weight
== referencesedit ==
== further readingedit ==
foley ja; monfreda c; ramankutty n and zaks d (2007) our share of the planetary pie proceedings of the national academy of sciences of the usa 104(31): 1258512586 download
haberl h; erb kh; krausmann f; gaube v; bondeau a; plutzar c; gingrich s; lucht w and fischer-kowalski m (2007) quantifying and mapping the human appropriation of net primary production in earth's terrestrial ecosystems proceedings of the national academy of sciences of the usa 104(31):12942-12947 download
purves william k and orians gordon h (2007) life: the science of biology 8th ed w h freeman isbn 978-1-4292-0877-2
== external linksedit ==
counting bacteria
trophic levels
biomass distributions for high trophic-level fishes in the north atlantic 19002000
