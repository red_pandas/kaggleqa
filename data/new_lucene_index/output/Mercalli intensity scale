the mercalli intensity scale is a seismic scale used for measuring the intensity of an earthquake it measures the effects of an earthquake and is distinct from the moment magnitude usually reported for an earthquake (sometimes misreported as the richter magnitude) which is a measure of the energy released the intensity of an earthquake is not entirely determined by its magnitude it is not based on first physical principles but is instead empirically based on observed effects
the mercalli scale quantifies the effects of an earthquake on the earth's surface humans objects of nature and man-made structures on a scale from i (not felt) to xii (total destruction) values depend upon the distance from the earthquake with the highest intensities being around the epicentral area data gathered from people who have experienced the quake are used to determine an intensity value for their location the italian volcanologist giuseppe mercalli revised the widely used simple ten-degree rossiforel scale between 1884 and 1906 creating the mercalli intensity scale which is still used nowadays
in 1902 the ten-degree mercalli scale was expanded to twelve degrees by italian physicist adolfo cancani it was later completely re-written by the german geophysicist august heinrich sieberg and became known as the mercallicancanisieberg (mcs) scale
the mercallicancanisieberg scale was later modified and published in english by harry o wood and frank neumann in 1931 as the mercalliwoodneumann (mwn) scale it was later improved by charles richter the father of the richter magnitude scale
the scale is known today as the modified mercalli scale (mm) or modified mercalli intensity scale (mmi)
== modified mercalli intensity scale ==
the lower degrees of the modified mercalli intensity scale generally deal with the manner in which the earthquake is felt by people the higher numbers of the scale are based on observed structural damage
the large table gives modified mercalli scale intensities that are typically observed at locations near the epicenter of the earthquake
=== correlation with magnitude ===
the correlation between magnitude and intensity is far from total depending upon several factors including the depth of the earthquake terrain population density and damage for example on may 19 2011 an earthquake of magnitude 0.7 in central california united states 4 km deep was classified as of intensity iii by the united states geological survey (usgs) over 100 miles (160 km) away from the epicenter (and ii intensity almost 300 miles (480 km) from the epicenter) while a 4.5 magnitude quake in salta argentina 164 km deep was of intensity i
the small table is a rough guide to the degrees of the modified mercalli intensity scale the colors and descriptive names shown here differ from those used on certain shake maps in other articles
=== correlation with physical quantities ===
the mercalli scale is not defined in terms of more rigorous objectively quantifiable measurements such as shake amplitude shake frequency peak velocity or peak acceleration human-perceived shaking and building damages are best correlated with peak acceleration for lower-intensity events and with peak velocity for higher-intensity events
=== comparison to the moment magnitude scale ===
the effects of any one earthquake can vary greatly from place to place so there may be many mercalli intensity values measured for the same earthquake these values can be best displayed using a contoured map of equal intensity known as an isoseismal map however each earthquake has only one magnitude
== see also ==
japan meteorological agency seismic intensity scale
rohn emergency scale
seismic scale
spectral acceleration
strong ground motion
== references ==
sources
== external links ==
national earthquake information center (u.s.)
mercalli scale simulator northern illinois university
john n louie associate professor of seismology at the university of nevada
the modified mercalli intensity scale united states geological survey
