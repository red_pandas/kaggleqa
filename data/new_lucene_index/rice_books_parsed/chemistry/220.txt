
Reaction Yields
Reaction YieldsBy the end of this section, you will be able to:
Explain the concepts of theoretical yield and limiting reactants/reagents.
Derive the theoretical yield for a reaction under specified conditions.
Calculate the percent yield for a reaction.
The relative amounts of reactants and products represented in a balanced chemical equation are often referred to as stoichiometric amounts. All the exercises of the preceding module involved stoichiometric amounts of reactants. For example, when calculating the amount of product generated from a given amount of reactant, it was assumed that any other reactants required were available in stoichiometric amounts (or greater). In this module, more realistic situations are considered, in which reactants are not present in stoichiometric amounts.
Limiting Reactant
Consider another food analogy, making grilled cheese sandwiches ([link]):
1 slice of cheese+2 slices of bread1 sandwich1 slice of cheese+2 slices of bread1 sandwich
Stoichiometric amounts of sandwich ingredients for this recipe are bread and cheese slices in a 2:1 ratio. Provided with 28 slices of bread and 11 slices of cheese, one may prepare 11 sandwiches per the provided recipe, using all the provided cheese and having six slices of bread left over. In this scenario, the number of sandwiches prepared has been limited by the number of cheese slices, and the bread slices have been provided in excess.
Sandwich making can illustrate the concepts of limiting and excess reactants.




Consider this concept now with regard to a chemical process, the reaction of hydrogen with chlorine to yield hydrogen chloride:
H2(s)+Cl2(g)2HCl(g)H2(s)+Cl2(g)2HCl(g)The balanced equation shows the hydrogen and chlorine react in a 1:1 stoichiometric ratio. If these reactants are provided in any other amounts, one of the reactants will nearly always be entirely consumed, thus limiting the amount of product that may be generated. This substance is the limiting reactant, and the other substance is the excess reactant. Identifying the limiting and excess reactants for a given situation requires computing the molar amounts of each reactant provided and comparing them to the stoichiometric amounts represented in the balanced chemical equation. For example, imagine combining 3 moles of H2 and 2 moles of Cl2. This represents a 3:2 (or 1.5:1) ratio of hydrogen to chlorine present for reaction, which is greater than the stoichiometric ratio of 1:1. Hydrogen, therefore, is present in excess, and chlorine is the limiting reactant. Reaction of all the provided chlorine (2 mol) will consume 2 mol of the 3 mol of hydrogen provided, leaving 1 mol of hydrogen unreacted.
An alternative approach to identifying the limiting reactant involves comparing the amount of product expected for the complete reaction of each reactant. Each reactant amount is used to separately calculate the amount of product that would be formed per the reactions stoichiometry. The reactant yielding the lesser amount of product is the limiting reactant. For the example in the previous paragraph, complete reaction of the hydrogen would yield
mol HCl produced=3 molH22 mol HCl1 molH2=6 mol HClmol HCl produced=3 molH22 mol HCl1 molH2=6 mol HCl
Complete reaction of the provided chlorine would produce
mol HCl produced=2 molCl22 mol HCl1 molCl2=4 mol HClmol HCl produced=2 molCl22 mol HCl1 molCl2=4 mol HCl
The chlorine will be completely consumed once 4 moles of HCl have been produced. Since enough hydrogen was provided to yield 6 moles of HCl, there will be unreacted hydrogen remaining once this reaction is complete. Chlorine, therefore, is the limiting reactant and hydrogen is the excess reactant ([link]).
When H2 and Cl2 are combined in nonstoichiometric amounts, one of these reactants will limit the amount of HCl that can be produced. This illustration shows a reaction in which hydrogen is present in excess and chlorine is the limiting reactant.








View this interactive simulation illustrating the concepts of limiting and excess reactants.


Identifying the Limiting Reactant
Silicon nitride is a very hard, high-temperature-resistant ceramic used as a component of turbine blades in jet engines. It is prepared according to the following equation:
3Si(s)+2N2(g)Si3N4(s)3Si(s)+2N2(g)Si3N4(s)
Which is the limiting reactant when 2.00 g of Si and 1.50 g of N2 react?
Solution
Compute the provided molar amounts of reactants, and then compare these amounts to the balanced equation to identify the limiting reactant.
mol Si=2.00g Si1 mol Si28.09g Si=0.0712 mol Simol Si=2.00g Si1 mol Si28.09g Si=0.0712 mol Si
molN2=1.50gN21 molN228.09gN2=0.0535 molN2molN2=1.50gN21 molN228.09gN2=0.0535 molN2
The provided Si:N2 molar ratio is:
0.0712 mol Si0.0535 molN2=1.33 mol Si1 molN20.0712 mol Si0.0535 molN2=1.33 mol Si1 molN2
The stoichiometric Si:N2 ratio is:
3 mol Si2 molN2=1.5 mol Si1 molN23 mol Si2 molN2=1.5 mol Si1 molN2
Comparing these ratios shows that Si is provided in a less-than-stoichiometric amount, and so is the limiting reactant.
Alternatively, compute the amount of product expected for complete reaction of each of the provided reactants. The 0.0712 moles of silicon would yield
molSi3N4produced=0.0712 mol Si1molSi3N43 mol Si=0.0237 molSi3N4molSi3N4produced=0.0712 mol Si1molSi3N43 mol Si=0.0237 molSi3N4
while the 0.0535 moles of nitrogen would produce
molSi3N4produced=0.0535 molN21 molSi3N42 molN2=0.0268 molSi3N4molSi3N4produced=0.0535 molN21 molSi3N42 molN2=0.0268 molSi3N4
Since silicon yields the lesser amount of product, it is the limiting reactant.
Check Your Learning
Which is the limiting reactant when 5.00 g of H2 and 10.0 g of O2 react and form water?
Answer:
O2



Percent Yield
The amount of product that may be produced by a reaction under specified conditions, as calculated per the stoichiometry of an appropriate balanced chemical equation, is called the theoretical yield of the reaction. In practice, the amount of product obtained is called the actual yield, and it is often less than the theoretical yield for a number of reasons. Some reactions are inherently inefficient, being accompanied by side reactions that generate other products. Others are, by nature, incomplete (consider the partial reactions of weak acids and bases discussed earlier in this chapter). Some products are difficult to collect without some loss, and so less than perfect recovery will reduce the actual yield. The extent to which a reactions theoretical yield is achieved is commonly expressed as its percent yield:
percent yield=actual yieldtheoretical yield100%percent yield=actual yieldtheoretical yield100%
Actual and theoretical yields may be expressed as masses or molar amounts (or any other appropriate property; e.g., volume, if the product is a gas). As long as both yields are expressed using the same units, these units will cancel when percent yield is calculated.

Calculation of Percent Yield
Upon reaction of 1.274 g of copper sulfate with excess zinc metal, 0.392 g copper metal was obtained according to the equation:
CuSO4(aq)+Zn(s)Cu(s)+ZnSO4(aq)CuSO4(aq)+Zn(s)Cu(s)+ZnSO4(aq)
What is the percent yield?
Solution
The provided information identifies copper sulfate as the limiting reactant, and so the theoretical yield is found by the approach illustrated in the previous module, as shown here:
1.274gCuSO41molCuSO4159.62gCuSO41mol Cu1molCuSO463.55g Cu1mol Cu=0.5072 g Cu1.274gCuSO41molCuSO4159.62gCuSO41mol Cu1molCuSO463.55g Cu1mol Cu=0.5072 g Cu
Using this theoretical yield and the provided value for actual yield, the percent yield is calculated to be
percent yield=(actual yieldtheoretical yield)100percent yield=(actual yieldtheoretical yield)100
percent yield=(0.392 g Cu0.5072 g Cu)100=77.3%percent yield=(0.392 g Cu0.5072 g Cu)100=77.3%
Check Your Learning
What is the percent yield of a reaction that produces 12.5 g of the Freon CF2Cl2 from 32.9 g of CCl4 and excess HF?
CCl4+2HFCF2Cl2+2HClCCl4+2HFCF2Cl2+2HCl
Answer:
48.3%


Green Chemistry and Atom Economy
The purposeful design of chemical products and processes that minimize the use of environmentally hazardous substances and the generation of waste is known as green chemistry. Green chemistry is a philosophical approach that is being applied to many areas of science and technology, and its practice is summarized by guidelines known as the Twelve Principles of Green Chemistry (see details at this website). One of the 12 principles is aimed specifically at maximizing the efficiency of processes for synthesizing chemical products. The atom economy of a process is a measure of this efficiency, defined as the percentage by mass of the final product of a synthesis relative to the masses of all the reactants used:
atom economy=mass of productmass of reactants100%atom economy=mass of productmass of reactants100%
Though the definition of atom economy at first glance appears very similar to that for percent yield, be aware that this property represents a difference in the theoretical efficiencies of different chemical processes. The percent yield of a given chemical process, on the other hand, evaluates the efficiency of a process by comparing the yield of product actually obtained to the maximum yield predicted by stoichiometry.
The synthesis of the common nonprescription pain medication, ibuprofen, nicely illustrates the success of a green chemistry approach ([link]). First marketed in the early 1960s, ibuprofen was produced using a six-step synthesis that required 514 g of reactants to generate each mole (206 g) of ibuprofen, an atom economy of 40%. In the 1990s, an alternative process was developed by the BHC Company (now BASF Corporation) that requires only three steps and has an atom economy of ~80%, nearly twice that of the original process. The BHC process generates significantly less chemical waste; uses less-hazardous and recyclable materials; and provides significant cost-savings to the manufacturer (and, subsequently, the consumer). In recognition of the positive environmental impact of the BHC process, the company received the Environmental Protection Agencys Greener Synthetic Pathways Award in 1997.
(a) Ibuprofen is a popular nonprescription pain medication commonly sold as 200 mg tablets. (b) The BHC process for synthesizing ibuprofen requires only three steps and exhibits an impressive atom economy. (credit a: modification of work by Derrick Coetzee)






Key Concepts and Summary
When reactions are carried out using less-than-stoichiometric quantities of reactants, the amount of product generated will be determined by the limiting reactant. The amount of product generated by a chemical reaction is its actual yield. This yield is often less than the amount of product predicted by the stoichiometry of the balanced chemical equation representing the reaction (its theoretical yield). The extent to which a reaction generates the theoretical amount of product is expressed as its percent yield.

Key Equations

percent yield=(actual yieldtheoretical yield)100percent yield=(actual yieldtheoretical yield)100


Chemistry End of Chapter Exercises


The following quantities are placed in a container: 1.5  1024 atoms of hydrogen, 1.0 mol of sulfur, and 88.0 g of diatomic oxygen.
(a) What is the total mass in grams for the collection of all three elements?
(b) What is the total number of moles of atoms for the three elements?
(c) If the mixture of the three elements formed a compound with molecules that contain two hydrogen atoms, one sulfur atom, and four oxygen atoms, which substance is consumed first?
(d) How many atoms of each remaining element would remain unreacted in the change described in (c)?




What is the limiting reactant in a reaction that produces sodium chloride from 8 g of sodium and 8 g of diatomic chlorine?


The limiting reactant is Cl2.




Which of the postulates of Dalton's atomic theory explains why we can calculate a theoretical yield for a chemical reaction?




A student isolated 25 g of a compound following a procedure that would theoretically yield 81 g. What was his percent yield?



Percent yield=31%Percent yield=31%




A sample of 0.53 g of carbon dioxide was obtained by heating 1.31 g of calcium carbonate. What is the percent yield for this reaction?
CaCO3(s)CaO(s)+CO2(s)CaCO3(s)CaO(s)+CO2(s)




Freon-12, CCl2F2, is prepared from CCl4 by reaction with HF. The other product of this reaction is HCl. Outline the steps needed to determine the percent yield of a reaction that produces 12.5 g of CCl2F2 from 32.9 g of CCl4. Freon-12 has been banned and is no longer used as a refrigerant because it catalyzes the decomposition of ozone and has a very long lifetime in the atmosphere. Determine the percent yield.


gCCl4molCCl4molCCl2F2gCCl2F2,gCCl4molCCl4molCCl2F2gCCl2F2, percent yield=48.3%percent yield=48.3%




Citric acid, C6H8O7, a component of jams, jellies, and fruity soft drinks, is prepared industrially via fermentation of sucrose by the mold Aspergillus niger. The equation representing this reaction is
C12H22O11+H2O+3O22C6H8O7+4H2OC12H22O11+H2O+3O22C6H8O7+4H2O
What mass of citric acid is produced from exactly 1 metric ton (1.000  103 kg) of sucrose if the yield is 92.30%?




Toluene, C6H5CH3, is oxidized by air under carefully controlled conditions to benzoic acid, C6H5CO2H, which is used to prepare the food preservative sodium benzoate, C6H5CO2Na. What is the percent yield of a reaction that converts 1.000 kg of toluene to 1.21 kg of benzoic acid?
2C6H5CH3+3O22C6H5CO2H+2H2O2C6H5CH3+3O22C6H5CO2H+2H2O


percent yield=91.3%percent yield=91.3%




In a laboratory experiment, the reaction of 3.0 mol of H2 with 2.0 mol of I2 produced 1.0 mol of HI. Determine the theoretical yield in grams and the percent yield for this reaction.




Outline the steps needed to solve the following problem, then do the calculations. Ether, (C2H5)2O, which was originally used as an anesthetic but has been replaced by safer and more effective medications, is prepared by the reaction of ethanol with sulfuric acid.
2C2H5OH + H2SO4  (C2H5)2 + H2SO4H2O
What is the percent yield of ether if 1.17 L (d = 0.7134 g/mL) is isolated from the reaction of 1.500 L of C2H5OH(d = 0.7894 g/mL)?


Convert mass of ethanol to moles of ethanol; relate the moles of ethanol to the moles of ether produced using the stoichiometry of the balanced equation. Convert moles of ether to grams; divide the actual grams of ether (determined through the density) by the theoretical mass to determine the percent yield; 87.6%




Outline the steps needed to determine the limiting reactant when 30.0 g of propane, C3H8, is burned with 75.0 g of oxygen.
percent yield=0.8347g0.9525g100%=87.6%percent yield=0.8347g0.9525g100%=87.6%
Determine the limiting reactant.




Outline the steps needed to determine the limiting reactant when 0.50 g of Cr and 0.75 g of H3PO4 react according to the following chemical equation?
2Cr+2H3PO42CrPO4+3H22Cr+2H3PO42CrPO4+3H2
Determine the limiting reactant.


The conversion needed is mol CrmolH2PO4.mol CrmolH2PO4. Then compare the amount of Cr to the amount of acid present. Cr is the limiting reactant.




What is the limiting reactant when 1.50 g of lithium and 1.50 g of nitrogen combine to form lithium nitride, a component of advanced batteries, according to the following unbalanced equation?
Li+N2Li3NLi+N2Li3N




Uranium can be isolated from its ores by dissolving it as UO2(NO3)2, then separating it as solid UO2(C2O4)3H2O. Addition of 0.4031 g of sodium oxalate, Na2C2O4, to a solution containing 1.481 g of uranyl nitrate, UO2(NO2)2, yields 1.073 g of solid UO2(C2O4)3H2O.
Na2C2O4 + UO2(NO3)2 + 3H2O  UO2(C2O4)3H2O + 2NaNO3
Determine the limiting reactant and the percent yield of this reaction.

Na2C2O4 is the limiting reactant. percent yield = 86.6%




How many molecules of C2H4Cl2 can be prepared from 15 C2H4 molecules and 8 Cl2 molecules?




How many molecules of the sweetener saccharin can be prepared from 30 C atoms, 25 H atoms, 12 O atoms, 8 S atoms, and 14 N atoms?




Only four molecules can be made.




The phosphorus pentoxide used to produce phosphoric acid for cola soft drinks is prepared by burning phosphorus in oxygen.
(a) What is the limiting reactant when 0.200 mol of P4 and 0.200 mol of O2 react according to P4+5O2P4O10P4+5O2P4O10
(b) Calculate the percent yield if 10.0 g of P4O10 is isolated from the reaction.




Would you agree to buy 1 trillion (1,000,000,000,000) gold atoms for $5? Explain why or why not. Find the current price of gold at http://money.cnn.com/data/commodities/ (1 troy ounce=31.1 g)(1 troy ounce=31.1 g)

This amount cannot be weighted by ordinary balances and is worthless.



Glossary

actual yield
amount of product formed in a reaction



excess reactant
reactant present in an amount greater than required by the reaction stoichiometry



limiting reactant
reactant present in an amount lower than required by the reaction stoichiometry, thus limiting the amount of product generated


percent yield
measure of the efficiency of a reaction, expressed as a percentage of the theoretical yield



theoretical yield
amount of product that may be produced from a given amount of reactant(s) according to the reaction stoichiometry


