
Introduction
Introduction
        class="introduction"
      Writing and Balancing Chemical Equations
Classifying Chemical Reactions
Reaction Stoichiometry
Reaction Yields
Quantitative Chemical Analysis
class="key-equations" title="Key-Equations"
class="summary" title="Chapter Summary"
class="exercises" title="Exercises"
class="references" title="References"
Many modern rocket fuels are solid mixtures of substances combined in carefully measured amounts and ignited to yield a thrust-generating chemical reaction. (credit: modification of work by NASA)




Solid-fuel rockets are a central feature in the worlds space exploration programs, including the new Space Launch System being developed by the National Aeronautics and Space Administration (NASA) to replace the retired Space Shuttle fleet ([link]). The engines of these rockets rely on carefully prepared solid mixtures of chemicals combined in precisely measured amounts. Igniting the mixture initiates a vigorous chemical reaction that rapidly generates large amounts of gaseous products. These gases are ejected from the rocket engine through its nozzle, providing the thrust needed to propel heavy payloads into space. Both the nature of this chemical reaction and the relationships between the amounts of the substances being consumed and produced by the reaction are critically important considerations that determine the success of the technology. This chapter will describe how to symbolize chemical reactions using chemical equations, how to classify some common chemical reactions by identifying patterns of reactivity, and how to determine the quantitative relations between the amounts of substances involved in chemical reactionsthat is, the reaction stoichiometry.

