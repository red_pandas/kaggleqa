
Occurrence, Preparation, and Properties of Nitrogen
Occurrence, Preparation, and Properties of NitrogenBy the end of this section, you will be able to:

Describe the properties, preparation, and uses of nitrogen

Most pure nitrogen comes from the fractional distillation of liquid air. The atmosphere consists of 78% nitrogen by volume. This means there are more than 20 million tons of nitrogen over every square mile of the earths surface. Nitrogen is a component of proteins and of the genetic material (DNA/RNA) of all plants and animals.
Under ordinary conditions, nitrogen is a colorless, odorless, and tasteless gas. It boils at 77 K and freezes at 63 K. Liquid nitrogen is a useful coolant because it is inexpensive and has a low boiling point. Nitrogen is very unreactive because of the very strong triple bond between the nitrogen atoms. The only common reactions at room temperature occur with lithium to form Li3N, with certain transition metal complexes, and with hydrogen or oxygen in nitrogen-fixing bacteria. The general lack of reactivity of nitrogen makes the remarkable ability of some bacteria to synthesize nitrogen compounds using atmospheric nitrogen gas as the source one of the most exciting chemical events on our planet. This process is one type of nitrogen fixation. In this case, nitrogen fixation is the process where organisms convert atmospheric nitrogen into biologically useful chemicals. Nitrogen fixation also occurs when lightning passes through air, causing molecular nitrogen to react with oxygen to form nitrogen oxides, which are then carried down to the soil.
Nitrogen Fixation
All living organisms require nitrogen compounds for survival. Unfortunately, most of these organisms cannot absorb nitrogen from its most abundant sourcethe atmosphere. Atmospheric nitrogen consists of N2 molecules, which are very unreactive due to the strong nitrogen-nitrogen triple bond. However, a few organisms can overcome this problem through a process known as nitrogen fixation, illustrated in [link].
All living organisms require nitrogen. A few microorganisms are able to process atmospheric nitrogen using nitrogen fixation. (credit roots: modification of work by the United States Department of Agriculture; credit root nodules: modification of work by Louisa Howard)




Nitrogen fixation is the process where organisms convert atmospheric nitrogen into biologically useful chemicals. To date, the only known kind of biological organisms capable of nitrogen fixation are microorganisms. These organisms employ enzymes called nitrogenases, which contain iron and molybdenum. Many of these microorganisms live in a symbiotic relationship with plants, with the best-known example being the presence of rhizobia in the root nodules of legumes.

Large volumes of atmospheric nitrogen are necessary for making ammoniathe principal starting material used for preparation of large quantities of other nitrogen-containing compounds. Most other uses for elemental nitrogen depend on its inactivity. It is helpful when a chemical process requires an inert atmosphere. Canned foods and luncheon meats cannot oxidize in a pure nitrogen atmosphere, so they retain a better flavor and color, and spoil less rapidly, when sealed in nitrogen instead of air. This technology allows fresh produce to be available year-round, regardless of growing season.
There are compounds with nitrogen in all of its oxidation states from 3 to 5+. Much of the chemistry of nitrogen involves oxidation-reduction reactions. Some active metals (such as alkali metals and alkaline earth metals) can reduce nitrogen to form metal nitrides. In the remainder of this section, we will examine nitrogen-oxygen chemistry.
There are well-characterized nitrogen oxides in which nitrogen exhibits each of its positive oxidation numbers from 1+ to 5+. When ammonium nitrate is carefully heated, nitrous oxide (dinitrogen oxide) and water vapor form. Stronger heating generates nitrogen gas, oxygen gas, and water vapor. No one should ever attempt this reactionit can be very explosive. In 1947, there was a major ammonium nitrate explosion in Texas City, Texas, and, in 2013, there was another major explosion in West, Texas. In the last 100 years, there were nearly 30 similar disasters worldwide, resulting in the loss of numerous lives. In this oxidation-reduction reaction, the nitrogen in the nitrate ion oxidizes the nitrogen in the ammonium ion. Nitrous oxide, shown in [link], is a colorless gas possessing a mild, pleasing odor and a sweet taste. It finds application as an anesthetic for minor operations, especially in dentistry, under the name laughing gas.
Nitrous oxide, N2O, is an anesthetic that has these molecular (left) and resonance (right) structures.




Low yields of nitric oxide, NO, form when heating nitrogen and oxygen together. NO also forms when lightning passes through air during thunderstorms. Burning ammonia is the commercial method of preparing nitric oxide. In the laboratory, the reduction of nitric acid is the best method for preparing nitric oxide. When copper reacts with dilute nitric acid, nitric oxide is the principal reduction product:
3Cu(s)+8HNO3(aq)2NO(g)+3Cu(NO3)2(aq)+4H2O(l)3Cu(s)+8HNO3(aq)2NO(g)+3Cu(NO3)2(aq)+4H2O(l)
Gaseous nitric oxide is the most thermally stable of the nitrogen oxides and is the simplest known thermally stable molecule with an unpaired electron. It is one of the air pollutants generated by internal combustion engines, resulting from the reaction of atmospheric nitrogen and oxygen during the combustion process.
At room temperature, nitric oxide is a colorless gas consisting of diatomic molecules. As is often the case with molecules that contain an unpaired electron, two molecules combine to form a dimer by pairing their unpaired electrons to form a bond. Liquid and solid NO both contain N2O2 dimers, like that shown in [link]. Most substances with unpaired electrons exhibit color by absorbing visible light; however, NO is colorless because the absorption of light is not in the visible region of the spectrum.
This shows the equilibrium between NO and N2O2. The molecule, N2O2, absorbs light.




Cooling a mixture of equal parts nitric oxide and nitrogen dioxide to 21 C produces dinitrogen trioxide, a blue liquid consisting of N2O3 molecules (shown in [link]). Dinitrogen trioxide exists only in the liquid and solid states. When heated, it reverts to a mixture of NO and NO2.
Dinitrogen trioxide, N2O3, only exists in liquid or solid states and has these molecular (left) and resonance (right) structures.




It is possible to prepare nitrogen dioxide in the laboratory by heating the nitrate of a heavy metal, or by the reduction of concentrated nitric acid with copper metal, as shown in [link]. Commercially, it is possible to prepare nitrogen dioxide by oxidizing nitric oxide with air.
The reaction of copper metal with concentrated HNO3 produces a solution of Cu(NO3)2 and brown fumes of NO2. (credit: modification of work by Mark Ott)




The nitrogen dioxide molecule (illustrated in [link]) contains an unpaired electron, which is responsible for its color and paramagnetism. It is also responsible for the dimerization of NO2. At low pressures or at high temperatures, nitrogen dioxide has a deep brown color that is due to the presence of the NO2 molecule. At low temperatures, the color almost entirely disappears as dinitrogen tetraoxide, N2O4, forms. At room temperature, an equilibrium exists:
2NO2(g)N2O4(g)KP=6.862NO2(g)N2O4(g)KP=6.86
The molecular and resonance structures for nitrogen dioxide (NO2, left) and dinitrogen tetraoxide (N2O4, right) are shown.




Dinitrogen pentaoxide, N2O5 (illustrated in [link]), is a white solid that is formed by the dehydration of nitric acid by phosphorus(V) oxide (tetraphosphorus decoxide):
P4O10(s)+4HNO3(l)4HPO3(s)+2N2O5(s)P4O10(s)+4HNO3(l)4HPO3(s)+2N2O5(s)
It is unstable above room temperature, decomposing to N2O4 and O2.
This image shows the molecular structure and one resonance structure of a molecule of dinitrogen pentaoxide, N2O5.




The oxides of nitrogen(III), nitrogen(IV), and nitrogen(V) react with water and form nitrogen-containing oxyacids. Nitrogen(III) oxide, N2O3, is the anhydride of nitrous acid; HNO2 forms when N2O3 reacts with water. There are no stable oxyacids containing nitrogen with an oxidation state of 4+; therefore, nitrogen(IV) oxide, NO2, disproportionates in one of two ways when it reacts with water. In cold water, a mixture of HNO2 and HNO3 forms. At higher temperatures, HNO3 and NO will form. Nitrogen(V) oxide, N2O5, is the anhydride of nitric acid; HNO3 is produced when N2O5 reacts with water:
N2O5(s)+H2O(l)2HNO3(aq)N2O5(s)+H2O(l)2HNO3(aq)
The nitrogen oxides exhibit extensive oxidation-reduction behavior. Nitrous oxide resembles oxygen in its behavior when heated with combustible substances. N2O is a strong oxidizing agent that decomposes when heated to form nitrogen and oxygen. Because one-third of the gas liberated is oxygen, nitrous oxide supports combustion better than air (one-fifth oxygen). A glowing splinter bursts into flame when thrust into a bottle of this gas. Nitric oxide acts both as an oxidizing agent and as a reducing agent. For example:
oxidizing agent:P4(s)+6NO(g)P4O6(s)+3N2(g)oxidizing agent:P4(s)+6NO(g)P4O6(s)+3N2(g)
reducing agent:Cl2(g)+2NO(g)2ClNO(g)reducing agent:Cl2(g)+2NO(g)2ClNO(g)
Nitrogen dioxide (or dinitrogen tetraoxide) is a good oxidizing agent. For example:
NO2(g)+CO(g)NO(g)+CO2(g)NO2(g)+CO(g)NO(g)+CO2(g)
NO2(g)+2HCl(aq)NO(g)+Cl2(g)+H2O(l)NO2(g)+2HCl(aq)NO(g)+Cl2(g)+H2O(l)
Key Concepts and Summary
Nitrogen exhibits oxidation states ranging from 3 to 5+. Because of the stability of the NN triple bond, it requires a great deal of energy to make compounds from molecular nitrogen. Active metals such as the alkali metals and alkaline earth metals can reduce nitrogen to form metal nitrides. Nitrogen oxides and nitrogen hydrides are also important substances.

Chemistry End of Chapter Exercises


Write the Lewis structures for each of the following:
(a) NH2
(b) N2F4
(c) NH2NH2
(d) NF3
(e) N3N3


(a) NH2:


;
(b) N2F4:


;
(c) NH2:NH2:


;
(d) NF3:


;
(e) N3:N3:







For each of the following, indicate the hybridization of the nitrogen atom (for N3,N3, the central nitrogen).
(a) N2F4
(b) NH2NH2
(c) NF3
(d) N3N3




Explain how ammonia can function both as a Brnsted base and as a Lewis base.


Ammonia acts as a Brnsted base because it readily accepts protons and as a Lewis base in that it has an electron pair to donate.
Brnsted base: NH3+H3O+NH4++H2ONH3+H3O+NH4++H2O
Lewis base: 2NH3+Ag+[H3NAgNH3]+2NH3+Ag+[H3NAgNH3]+




Determine the oxidation state of nitrogen in each of the following. You may wish to review the chapter on chemical bonding for relevant examples.
(a) NCl3
(b) ClNO
(c) N2O5
(d) N2O3
(e) NO2NO2
(f) N2O4
(g) N2O
(h) NO3NO3
(i) HNO2
(j) HNO3




For each of the following, draw the Lewis structure, predict the ONO bond angle, and give the hybridization of the nitrogen. You may wish to review the chapters on chemical bonding and advanced theories of covalent bonding for relevant examples.
(a) NO2
(b) NO2NO2
(c) NO2+NO2+


(a) NO2:



Nitrogen is sp2 hybridized. The molecule has a bent geometry with an ONO bond angle of approximately 120.
(b) NO2:NO2:



Nitrogen is sp2 hybridized. The molecule has a bent geometry with an ONO bond angle slightly less than 120.
(c) NO2+:NO2+:



Nitrogen is sp hybridized. The molecule has a linear geometry with an ONO bond angle of 180.




How many grams of gaseous ammonia will the reaction of 3.0 g hydrogen gas and 3.0 g of nitrogen gas produce?




Although PF5 and AsF5 are stable, nitrogen does not form NF5 molecules. Explain this difference among members of the same group.


Nitrogen cannot form a NF5 molecule because it does not have d orbitals to bond with the additional two fluorine atoms.




The equivalence point for the titration of a 25.00-mL sample of CsOH solution with 0.1062 M HNO3 is at 35.27 mL. What is the concentration of the CsOH solution?



Glossary

nitrogen fixation
formation of nitrogen compounds from molecular nitrogen


