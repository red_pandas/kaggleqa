
Formation Constants for Complex Ions
Formation Constants for Complex Ions


Formation Constants for Complex Ions


Equilibrium
Kf



Al3++6F[AlF6]3Al3++6F[AlF6]3
7  1019


Cd2++4NH3[Cd(NH3)4]2+Cd2++4NH3[Cd(NH3)4]2+
1.3  107


Cd2++4CN[Cd(CN)4]2Cd2++4CN[Cd(CN)4]2
3  1018


Co2++6NH3[Co(NH3)6]2+Co2++6NH3[Co(NH3)6]2+
1.3  105


Co3++6NH3[Co(NH3)6]3+Co3++6NH3[Co(NH3)6]3+
2.3  1033


Cu++2CN[Cu(CN)2]Cu++2CN[Cu(CN)2]
1.0  1016


Cu2++4NH3[Cu(NH3)4]2+Cu2++4NH3[Cu(NH3)4]2+
1.7  1013


Fe2++6CN[Fe(CN)6]4Fe2++6CN[Fe(CN)6]4
1.5  1035


Fe3++6CN[Fe(CN)6]3Fe3++6CN[Fe(CN)6]3
2  1043


Fe3++6SCN[Fe(SCN)6]3Fe3++6SCN[Fe(SCN)6]3
3.2  103


Hg2++4Cl[HgCl4]2Hg2++4Cl[HgCl4]2
1.1  1016


Ni2++6NH3[Ni(NH3)6]2+Ni2++6NH3[Ni(NH3)6]2+
2.0  108


Ag++2Cl[AgCl2]Ag++2Cl[AgCl2]
1.8  105


Ag++2CN[Ag(CN)2]Ag++2CN[Ag(CN)2]
1  1021


Ag++2NH3[Ag(NH3)2]+Ag++2NH3[Ag(NH3)2]+
1.7  107


Zn2++4CN[Zn(CN)4]2Zn2++4CN[Zn(CN)4]2
2.1  1019


Zn2++4OH[Zn(OH)4]2Zn2++4OH[Zn(OH)4]2
2  1015


Fe3++SCN[Fe(SCN)]2+Fe3++SCN[Fe(SCN)]2+
8.9  102


Ag++4SCN[Ag(SCN)4]3Ag++4SCN[Ag(SCN)4]3
1.2  1010


Pb2++4I[PbI4]2Pb2++4I[PbI4]2
3.0  104


Pt2++4Cl[PtCl4]2Pt2++4Cl[PtCl4]2
1  1016


Cu2++4CN[Cu(CN)4]2Cu2++4CN[Cu(CN)4]2
1.0  1025


Co2++4SCN[Co(SCN)4]2Co2++4SCN[Co(SCN)4]2
1103



