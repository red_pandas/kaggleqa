
Aldehydes, Ketones, Carboxylic Acids, and Esters
Aldehydes, Ketones, Carboxylic Acids, and EstersBy the end of this section, you will be able to:
Describe the structure and properties of aldehydes, ketones, carboxylic acids and esters
Another class of organic molecules contains a carbon atom connected to an oxygen atom by a double bond, commonly called a carbonyl group. The trigonal planar carbon in the carbonyl group can attach to two other substituents leading to several subfamilies (aldehydes, ketones, carboxylic acids and esters) described in this section.
Aldehydes and Ketones
Both aldehydes and ketones contain a carbonyl group, a functional group with a carbon-oxygen double bond. The names for aldehyde and ketone compounds are derived using similar nomenclature rules as for alkanes and alcohols, and include the class-identifying suffixes -al and -one, respectively:



In an aldehyde, the carbonyl group is bonded to at least one hydrogen atom. In a ketone, the carbonyl group is bonded to two carbon atoms:









As text, an aldehyde group is represented as CHO; a ketone is represented as C(O) or CO.
In both aldehydes and ketones, the geometry around the carbon atom in the carbonyl group is trigonal planar; the carbon atom exhibits sp2 hybridization. Two of the sp2 orbitals on the carbon atom in the carbonyl group are used to form  bonds to the other carbon or hydrogen atoms in a molecule. The remaining sp2 hybrid orbital forms a  bond to the oxygen atom. The unhybridized p orbital on the carbon atom in the carbonyl group overlaps a p orbital on the oxygen atom to form the  bond in the double bond.
Like the C=OC=O bond in carbon dioxide, the C=OC=O bond of a carbonyl group is polar (recall that oxygen is significantly more electronegative than carbon, and the shared electrons are pulled toward the oxygen atom and away from the carbon atom). Many of the reactions of aldehydes and ketones start with the reaction between a Lewis base and the carbon atom at the positive end of the polar C=OC=O bond to yield an unstable intermediate that subsequently undergoes one or more structural rearrangements to form the final product ([link]).
The carbonyl group is polar, and the geometry of the bonds around the central carbon is trigonal planar.




The importance of molecular structure in the reactivity of organic compounds is illustrated by the reactions that produce aldehydes and ketones. We can prepare a carbonyl group by oxidation of an alcoholfor organic molecules, oxidation of a carbon atom is said to occur when a carbon-hydrogen bond is replaced by a carbon-oxygen bond. The reverse reactionreplacing a carbon-oxygen bond by a carbon-hydrogen bondis a reduction of that carbon atom. Recall that oxygen is generally assigned a 2 oxidation number unless it is elemental or attached to a fluorine. Hydrogen is generally assigned an oxidation number of +1 unless it is attached to a metal. Since carbon does not have a specific rule, its oxidation number is determined algebraically by factoring the atoms it is attached to and the overall charge of the molecule or ion. In general, a carbon atom attached to an oxygen atom will have a more positive oxidation number and a carbon atom attached to a hydrogen atom will have a more negative oxidation number. This should fit nicely with your understanding of the polarity of CO and CH bonds. The other reagents and possible products of these reactions are beyond the scope of this chapter, so we will focus only on the changes to the carbon atoms:




Oxidation and Reduction in Organic Chemistry
Methane represents the completely reduced form of an organic molecule that contains one carbon atom. Sequentially replacing each of the carbon-hydrogen bonds with a carbon-oxygen bond would lead to an alcohol, then an aldehyde, then a carboxylic acid (discussed later), and, finally, carbon dioxide:
CH4CH3OHCH2OHCO2HCO2CH4CH3OHCH2OHCO2HCO2
What are the oxidation numbers for the carbon atoms in the molecules shown here?
Solution
In this example, we can calculate the oxidation number (review the chapter on oxidation-reduction reactions if necessary) for the carbon atom in each case (note how this would become difficult for larger molecules with additional carbon atoms and hydrogen atoms, which is why organic chemists use the definition dealing with replacing CH bonds with CO bonds described). For CH4, the carbon atom carries a 4 oxidation number (the hydrogen atoms are assigned oxidation numbers of +1 and the carbon atom balances that by having an oxidation number of 4). For the alcohol (in this case, methanol), the carbon atom has an oxidation number of 2 (the oxygen atom is assigned 2, the four hydrogen atoms each are assigned +1, and the carbon atom balances the sum by having an oxidation number of 2; note that compared to the carbon atom in CH4, this carbon atom has lost two electrons so it was oxidized); for the aldehyde, the carbon atoms oxidation number is 0 (2 for the oxygen atom and +1 for each hydrogen atom already balances to 0, so the oxidation number for the carbon atom is 0); for the carboxylic acid, the carbon atoms oxidation number is +2 (two oxygen atoms each at 2 and two hydrogen atoms at +1); and for carbon dioxide, the carbon atoms oxidation number is +4 (here, the carbon atom needs to balance the 4 sum from the two oxygen atoms).
Check Your Learning
Indicate whether the marked carbon atoms in the three molecules here are oxidized or reduced relative to the marked carbon atom in ethanol:



There is no need to calculate oxidation states in this case; instead, just compare the types of atoms bonded to the marked carbon atoms:



Answer:
(a) reduced (bond to oxygen atom replaced by bond to hydrogen atom); (b) oxidized (one bond to hydrogen atom replaced by one bond to oxygen atom); (c) oxidized (2 bonds to hydrogen atoms have been replaced by bonds to an oxygen atom)


Aldehydes are commonly prepared by the oxidation of alcohols whose OH functional group is located on the carbon atom at the end of the chain of carbon atoms in the alcohol:



Alcohols that have their OH groups in the middle of the chain are necessary to synthesize a ketone, which requires the carbonyl group to be bonded to two other carbon atoms:



An alcohol with its OH group bonded to a carbon atom that is bonded to no or one other carbon atom will form an aldehyde. An alcohol with its OH group attached to two other carbon atoms will form a ketone. If three carbons are attached to the carbon bonded to the OH, the molecule will not have a CH bond to be replaced, so it will not be susceptible to oxidation.
Formaldehyde, an aldehyde with the formula HCHO, is a colorless gas with a pungent and irritating odor. It is sold in an aqueous solution called formalin, which contains about 37% formaldehyde by weight. Formaldehyde causes coagulation of proteins, so it kills bacteria (and any other living organism) and stops many of the biological processes that cause tissue to decay. Thus, formaldehyde is used for preserving tissue specimens and embalming bodies. It is also used to sterilize soil or other materials. Formaldehyde is used in the manufacture of Bakelite, a hard plastic having high chemical and electrical resistance.
Dimethyl ketone, CH3COCH3, commonly called acetone, is the simplest ketone. It is made commercially by fermenting corn or molasses, or by oxidation of 2-propanol. Acetone is a colorless liquid. Among its many uses are as a solvent for lacquer (including fingernail polish), cellulose acetate, cellulose nitrate, acetylene, plastics, and varnishes; as a paint and varnish remover; and as a solvent in the manufacture of pharmaceuticals and chemicals.

Carboxylic Acids and Esters
The odor of vinegar is caused by the presence of acetic acid, a carboxylic acid, in the vinegar. The odor of ripe bananas and many other fruits is due to the presence of esters, compounds that can be prepared by the reaction of a carboxylic acid with an alcohol. Because esters do not have hydrogen bonds between molecules, they have lower vapor pressures than the alcohols and carboxylic acids from which they are derived (see [link]).
Esters are responsible for the odors associated with various plants and their fruits.




Both carboxylic acids and esters contain a carbonyl group with a second oxygen atom bonded to the carbon atom in the carbonyl group by a single bond. In a carboxylic acid, the second oxygen atom also bonds to a hydrogen atom. In an ester, the second oxygen atom bonds to another carbon atom. The names for carboxylic acids and esters include prefixes that denote the lengths of the carbon chains in the molecules and are derived following nomenclature rules similar to those for inorganic acids and salts (see these examples):



The functional groups for an acid and for an ester are shown in red in these formulas.
The hydrogen atom in the functional group of a carboxylic acid will react with a base to form an ionic salt:



Carboxylic acids are weak acids (see the chapter on acids and bases), meaning they are not 100% ionized in water. Generally only about 1% of the molecules of a carboxylic acid dissolved in water are ionized at any given time. The remaining molecules are undissociated in solution.
We prepare carboxylic acids by the oxidation of aldehydes or alcohols whose OH functional group is located on the carbon atom at the end of the chain of carbon atoms in the alcohol:



Esters are produced by the reaction of acids with alcohols. For example, the ester ethyl acetate, CH3CO2CH2CH3, is formed when acetic acid reacts with ethanol:



The simplest carboxylic acid is formic acid, HCO2H, known since 1670. Its name comes from the Latin word formicus, which means ant; it was first isolated by the distillation of red ants. It is partially responsible for the pain and irritation of ant and wasp stings, and is responsible for a characteristic odor of ants that can be sometimes detected in their nests.
Acetic acid, CH3CO2H, constitutes 36% vinegar. Cider vinegar is produced by allowing apple juice to ferment without oxygen present. Yeast cells present in the juice carry out the fermentation reactions. The fermentation reactions change the sugar present in the juice to ethanol, then to acetic acid. Pure acetic acid has a penetrating odor and produces painful burns. It is an excellent solvent for many organic and some inorganic compounds, and it is essential in the production of cellulose acetate, a component of many synthetic fibers such as rayon.
The distinctive and attractive odors and flavors of many flowers, perfumes, and ripe fruits are due to the presence of one or more esters ([link]). Among the most important of the natural esters are fats (such as lard, tallow, and butter) and oils (such as linseed, cottonseed, and olive oils), which are esters of the trihydroxyl alcohol glycerine, C3H5(OH)3, with large carboxylic acids, such as palmitic acid, CH3(CH2)14CO2H, stearic acid, CH3(CH2)16CO2H, and oleic acid, CH3(CH2)7CH=CH(CH2)7CO2H.CH3(CH2)7CH=CH(CH2)7CO2H. Oleic acid is an unsaturated acid; it contains a C=CC=C double bond. Palmitic and stearic acids are saturated acids that contain no double or triple bonds.
Over 350 different volatile molecules (many members of the ester family) have been identified in strawberries. (credit: Rebecca Siegel)





Key Concepts and Summary
Functional groups related to the carbonyl group include the CHO group of an aldehyde, the CO group of a ketone, the CO2H group of a carboxylic acid, and the CO2R group of an ester. The carbonyl group, a carbon-oxygen double bond, is the key structure in these classes of organic molecules: Aldehydes contain at least one hydrogen atom attached to the carbonyl carbon atom, ketones contain two carbon groups attached to the carbonyl carbon atom, carboxylic acids contain a hydroxyl group attached to the carbonyl carbon atom, and esters contain an oxygen atom attached to another carbon group connected to the carbonyl carbon atom. All of these compounds contain oxidized carbon atoms relative to the carbon atom of an alcohol group.

Chemistry End of Chapter Exercises


Order the following molecules from least to most oxidized, based on the marked carbon atom:







Predict the products of oxidizing the molecules shown in this problem. In each case, identify the product that will result from the minimal increase in oxidation state for the highlighted carbon atom:
(a)



(b)



(c)





(a)


;
(b)


;
(c)







Predict the products of reducing the following molecules. In each case, identify the product that will result from the minimal decrease in oxidation state for the highlighted carbon atom:
(a)



(b)



(c)







Explain why it is not possible to prepare a ketone that contains only two carbon atoms.


A ketone contains a group bonded to two additional carbon atoms; thus, a minimum of three carbon atoms are needed.




How does hybridization of the substituted carbon atom change when an alcohol is converted into an aldehyde? An aldehyde to a carboxylic acid?




Fatty acids are carboxylic acids that have long hydrocarbon chains attached to a carboxylate group. How does a saturated fatty acid differ from an unsaturated fatty acid? How are they similar?


Since they are both carboxylic acids, they each contain the COOH functional group and its characteristics. The difference is the hydrocarbon chain in a saturated fatty acid contains no double or triple bonds, whereas the hydrocarbon chain in an unsaturated fatty acid contains one or more multiple bonds.




Write a condensed structural formula, such as CH3CH3, and describe the molecular geometry at each carbon atom.
(a) propene
(b) 1-butanol
(c) ethyl propyl ether
(d) cis-4-bromo-2-heptene
(e) 2,2,3-trimethylhexane
(f) formaldehyde




Write a condensed structural formula, such as CH3CH3, and describe the molecular geometry at each carbon atom.
(a) 2-propanol
(b) acetone
(c) dimethyl ether
(d) acetic acid
(e) 3-methyl-1-hexene


(a) CH3CH(OH)CH3: all carbons are tetrahedral; (b) CH3C(==O)CH3:CH3C(==O)CH3: the end carbons are tetrahedral and the central carbon is trigonal planar; (c) CH3OCH3: all are tetrahedral; (d) CH3COOH: the methyl carbon is tetrahedral and the acid carbon is trigonal planar; (e) CH3CH2CH2CH(CH3)CHCH2: all are tetrahedral except the right-most two carbons, which are trigonal planar




The foul odor of rancid butter is caused by butyric acid, CH3CH2CH2CO2H. 
(a) Draw the Lewis structure and determine the oxidation number and hybridization for each carbon atom in the molecule.
(b) The esters formed from butyric acid are pleasant-smelling compounds found in fruits and used in perfumes. Draw the Lewis structure for the ester formed from the reaction of butyric acid with 2-propanol.




Write the two-resonance structures for the acetate ion.









Write two complete, balanced equations for each of the following reactions, one using condensed formulas and one using Lewis structures:
(a) ethanol reacts with propionic acid
(b) benzoic acid, C6H5CO2H, is added to a solution of sodium hydroxide




Write two complete balanced equations for each of the following reactions, one using condensed formulas and one using Lewis structures.
(a) 1-butanol reacts with acetic acid
(b) propionic acid is poured onto solid calcium carbonate


(a) CH3CH2CH2CH2OH+CH3C(O)OHCH3C(O)OCH2CH2CH2CH3+H2O:CH3CH2CH2CH2OH+CH3C(O)OHCH3C(O)OCH2CH2CH2CH3+H2O:



;
(b) 2CH3CH2COOH+CaCO3(CH3CH2COO)2Ca+CO2+H2O:2CH3CH2COOH+CaCO3(CH3CH2COO)2Ca+CO2+H2O:







Yields in organic reactions are sometimes low. What is the percent yield of a process that produces 13.0 g of ethyl acetate from 10.0 g of CH3CO2H?




Alcohols A, B, and C all have the composition C4H10O. Molecules of alcohol A contain a branched carbon chain and can be oxidized to an aldehyde; molecules of alcohol B contain a linear carbon chain and can be oxidized to a ketone; and molecules of alcohol C can be oxidized to neither an aldehyde nor a ketone. Write the Lewis structures of these molecules.








Glossary

aldehyde
organic compound containing a carbonyl group bonded to two hydrogen atoms or a hydrogen atom and a carbon substituent


carbonyl group
carbon atom double bonded to an oxygen atom


carboxylic acid
organic compound containing a carbonyl group with an attached hydroxyl group


ester
organic compound containing a carbonyl group with an attached oxygen atom that is bonded to a carbon substituent


ketone
organic compound containing a carbonyl group with two carbon substituents attached to it


