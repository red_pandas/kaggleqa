
Structure and General Properties of the Nonmetals
Structure and General Properties of the NonmetalsBy the end of this section, you will be able to:

Describe structure and properties of nonmetals

The nonmetals are elements located in the upper right portion of the periodic table. Their properties and behavior are quite different from those of metals on the left side. Under normal conditions, more than half of the nonmetals are gases, one is a liquid, and the rest include some of the softest and hardest of solids. The nonmetals exhibit a rich variety of chemical behaviors. They include the most reactive and least reactive of elements, and they form many different ionic and covalent compounds. This section presents an overview of the properties and chemical behaviors of the nonmetals, as well as the chemistry of specific elements. Many of these nonmetals are important in biological systems.
In many cases, trends in electronegativity enable us to predict the type of bonding and the physical states in compounds involving the nonmetals. We know that electronegativity decreases as we move down a given group and increases as we move from left to right across a period. The nonmetals have higher electronegativities than do metals, and compounds formed between metals and nonmetals are generally ionic in nature because of the large differences in electronegativity between them. The metals form cations, the nonmetals form anions, and the resulting compounds are solids under normal conditions. On the other hand, compounds formed between two or more nonmetals have small differences in electronegativity between the atoms, and covalent bondingsharing of electronsresults. These substances tend to be molecular in nature and are gases, liquids, or volatile solids at room temperature and pressure.
In normal chemical processes, nonmetals do not form monatomic positive ions (cations) because their ionization energies are too high. All monatomic nonmetal ions are anions; examples include the chloride ion, Cl, the nitride ion, N3, and the selenide ion, Se2.
The common oxidation states that the nonmetals exhibit in their ionic and covalent compounds are shown in [link]. Remember that an element exhibits a positive oxidation state when combined with a more electronegative element and that it exhibits a negative oxidation state when combined with a less electronegative element.
Nonmetals exhibit these common oxidation states in ionic and covalent compounds.




The first member of each nonmetal group exhibits different behaviors, in many respects, from the other group members. The reasons for this include smaller size, greater ionization energy, and (most important) the fact that the first member of each group has only four valence orbitals (one 2s and three 2p) available for bonding, whereas other group members have empty d orbitals in their valence shells, making possible five, six, or even more bonds around the central atom. For example, nitrogen forms only NF3, whereas phosphorus forms both PF3 and PF5.
Another difference between the first group member and subsequent members is the greater ability of the first member to form  bonds. This is primarily a function of the smaller size of the first member of each group, which allows better overlap of atomic orbitals. Nonmetals, other than the first member of each group, rarely form  bonds to nonmetals that are the first member of a group. For example, sulfur-oxygen  bonds are well known, whereas sulfur does not normally form stable  bonds to itself.
The variety of oxidation states displayed by most of the nonmetals means that many of their chemical reactions involve changes in oxidation state through oxidation-reduction reactions. There are four general aspects of the oxidation-reduction chemistry:

Nonmetals oxidize most metals. The oxidation state of the metal becomes positive as it undergoes oxidation and that of the nonmetal becomes negative as it undergoes reduction. For example:
4Fe(s)+3O2(g)2Fe2O3(s)00+324Fe(s)+3O2(g)2Fe2O3(s)00+32
With the exception of nitrogen and carbon, which are poor oxidizing agents, a more electronegative nonmetal oxidizes a less electronegative nonmetal or the anion of the nonmetal:
S(s)+O2(g)2SO2(s)00+42S(s)+O2(g)2SO2(s)00+42
Cl2(g)+2I(aq)I2(s)+2Cl(aq)00Cl2(g)+2I(aq)I2(s)+2Cl(aq)00
Fluorine and oxygen are the strongest oxidizing agents within their respective groups; each oxidizes all the elements that lie below it in the group. Within any period, the strongest oxidizing agent is in group 17. A nonmetal often oxidizes an element that lies to its left in the same period. For example:
2As(s)+3Br2(l)2AsBr3(s)00+312As(s)+3Br2(l)2AsBr3(s)00+31
The stronger a nonmetal is as an oxidizing agent, the more difficult it is to oxidize the anion formed by the nonmetal. This means that the most stable negative ions are formed by elements at the top of the group or in group 17 of the period.
Fluorine and oxygen are the strongest oxidizing elements known. Fluorine does not form compounds in which it exhibits positive oxidation states; oxygen exhibits a positive oxidation state only when combined with fluorine. For example:
2F2(g)+2OH(aq)OF2(g)+2F(aq)+H2O(l)0+212F2(g)+2OH(aq)OF2(g)+2F(aq)+H2O(l)0+21

With the exception of most of the noble gases, all nonmetals form compounds with oxygen, yielding covalent oxides. Most of these oxides are acidic, that is, they react with water to form oxyacids. Recall from the acid-base chapter that an oxyacid is an acid consisting of hydrogen, oxygen, and some other element. Notable exceptions are carbon monoxide, CO, nitrous oxide, N2O, and nitric oxide, NO. There are three characteristics of these acidic oxides:

Oxides such as SO2 and N2O5, in which the nonmetal exhibits one of its common oxidation states, are acid anhydrides and react with water to form acids with no change in oxidation state. The product is an oxyacid. For example:
SO2(g)+H2O(l)H2SO3(aq)SO2(g)+H2O(l)H2SO3(aq)
N2O5(s)+H2O(l)2HNO3(aq)N2O5(s)+H2O(l)2HNO3(aq)
Those oxides such as NO2 and ClO2, in which the nonmetal does not exhibit one of its common oxidation states, also react with water. In these reactions, the nonmetal is both oxidized and reduced. For example:
3NO2(g)+H2O(l)2HNO3(aq)+NO(g)+4+5+23NO2(g)+H2O(l)2HNO3(aq)+NO(g)+4+5+2Reactions in which the same element is both oxidized and reduced are called disproportionation reactions.
The acid strength increases as the electronegativity of the central atom increases. To learn more, see the discussion in the chapter on acid-base chemistry.

The binary hydrogen compounds of the nonmetals also exhibit an acidic behavior in water, although only HCl, HBr, and HI are strong acids. The acid strength of the nonmetal hydrogen compounds increases from left to right across a period and down a group. For example, ammonia, NH3, is a weaker acid than is water, H2O, which is weaker than is hydrogen fluoride, HF. Water, H2O, is also a weaker acid than is hydrogen sulfide, H2S, which is weaker than is hydrogen selenide, H2Se. Weaker acidic character implies greater basic character.
Structures of the Nonmetals
The structures of the nonmetals differ dramatically from those of metals. Metals crystallize in closely packed arrays that do not contain molecules or covalent bonds. Nonmetal structures contain covalent bonds, and many nonmetals consist of individual molecules. The electrons in nonmetals are localized in covalent bonds, whereas in a metal, there is delocalization of the electrons throughout the solid.
The noble gases are all monatomic, whereas the other nonmetal gaseshydrogen, nitrogen, oxygen, fluorine, and chlorinenormally exist as the diatomic molecules H2, N2, O2, F2, and Cl2. The other halogens are also diatomic; Br2 is a liquid and I2 exists as a solid under normal conditions. The changes in state as one moves down the halogen family offer excellent examples of the increasing strength of intermolecular London forces with increasing molecular mass and increasing polarizability.
Oxygen has two allotropes: O2, dioxygen, and O3, ozone. Phosphorus has three common allotropes, commonly referred to by their colors: white, red, and black. Sulfur has several allotropes. There are also many carbon allotropes. Most people know of diamond, graphite, and charcoal, but fewer people know of the recent discovery of fullerenes, carbon nanotubes, and graphene.
Descriptions of the physical properties of three nonmetals that are characteristic of molecular solids follow.

Carbon
Carbon occurs in the uncombined (elemental) state in many forms, such as diamond, graphite, charcoal, coke, carbon black, graphene, and fullerene.
Diamond, shown in [link], is a very hard crystalline material that is colorless and transparent when pure. Each atom forms four single bonds to four other atoms at the corners of a tetrahedron (sp3 hybridization); this makes the diamond a giant molecule. Carbon-carbon single bonds are very strong, and, because they extend throughout the crystal to form a three-dimensional network, the crystals are very hard and have high melting points (~4400 C).(a) Diamond and (b) graphite are two forms of carbon. (c) In the crystal structure of diamond, the covalent bonds form three-dimensional tetrahedrons. (d) In the crystal structure of graphite, each planar layer is composed of six-membered rings. (credit a: modification of work by Fancy Diamonds/Flickr; credit b: modification of work from http://images-of-elements.com/carbon.php)




Graphite, also shown in [link], is a soft, slippery, grayish-black solid that conducts electricity. These properties relate to its structure, which consists of layers of carbon atoms, with each atom surrounded by three other carbon atoms in a trigonal planar arrangement. Each carbon atom in graphite forms three  bonds, one to each of its nearest neighbors, by means of sp2-hybrid orbitals. The unhybridized p orbital on each carbon atom will overlap unhybridized orbitals on adjacent carbon atoms in the same layer to form  bonds. Many resonance forms are necessary to describe the electronic structure of a graphite layer; [link] illustrates two of these forms.(a) Carbon atoms in graphite have unhybridized p orbitals. Each p orbital is perpendicular to the plane of carbon atoms. (b) These are two of the many resonance forms of graphite necessary to describe its electronic structure as a resonance hybrid.




Atoms within a graphite layer are bonded together tightly by the  and  bonds; however, the forces between layers are weak. London dispersion forces hold the layers together. To learn more, see the discussion of these weak forces in the chapter on liquids and solids. The weak forces between layers give graphite the soft, flaky character that makes it useful as the so-called lead in pencils and the slippery character that makes it useful as a lubricant. The loosely held electrons in the resonating  bonds can move throughout the solid and are responsible for the electrical conductivity of graphite.
Other forms of elemental carbon include carbon black, charcoal, and coke. Carbon black is an amorphous form of carbon prepared by the incomplete combustion of natural gas, CH4. It is possible to produce charcoal and coke by heating wood and coal, respectively, at high temperatures in the absence of air.
Recently, new forms of elemental carbon molecules have been identified in the soot generated by a smoky flame and in the vapor produced when graphite is heated to very high temperatures in a vacuum or in helium. One of these new forms, first isolated by Professor Richard Smalley and coworkers at Rice University, consists of icosahedral (soccer-ball-shaped) molecules that contain 60 carbon atoms, C60. This is buckminsterfullerene (often called bucky balls) after the architect Buckminster Fuller, who designed domed structures, which have a similar appearance ([link]).
The molecular structure of C60, buckminsterfullerene, is icosahedral.




Nanotubes and Graphene
Graphene and carbon nanotubes are two recently discovered allotropes of carbon. Both of the forms bear some relationship to graphite. Graphene is a single layer of graphite (one atom thick), as illustrated in [link], whereas carbon nanotubes roll the layer into a small tube, as illustrated in [link].
(a) Graphene and (b) carbon nanotubes are both allotropes of carbon.




Graphene is a very strong, lightweight, and efficient conductor of heat and electricity discovered in 2003. As in graphite, the carbon atoms form a layer of six-membered rings with sp2-hybridized carbon atoms at the corners. Resonance stabilizes the system and leads to its conductivity. Unlike graphite, there is no stacking of the layers to give a three-dimensional structure. Andre Geim and Kostya Novoselov at the University of Manchester won the 2010 Nobel Prize in Physics for their pioneering work characterizing graphene.
The simplest procedure for preparing graphene is to use a piece of adhesive tape to remove a single layer of graphene from the surface of a piece of graphite. This method works because there are only weak London dispersion forces between the layers in graphite. Alternative methods are to deposit a single layer of carbon atoms on the surface of some other material (ruthenium, iridium, or copper) or to synthesize it at the surface of silicon carbide via the sublimation of silicon.
There currently are no commercial applications of graphene. However, its unusual properties, such as high electron mobility and thermal conductivity, should make it suitable for the manufacture of many advanced electronic devices and for thermal management applications.
Carbon nanotubes are carbon allotropes, which have a cylindrical structure. Like graphite and graphene, nanotubes consist of rings of sp2-hybridized carbon atoms. Unlike graphite and graphene, which occur in layers, the layers wrap into a tube and bond together to produce a stable structure. The walls of the tube may be one atom or multiple atoms thick.
Carbon nanotubes are extremely strong materials that are harder than diamond. Depending upon the shape of the nanotube, it may be a conductor or semiconductor. For some applications, the conducting form is preferable, whereas other applications utilize the semiconducting form.
The basis for the synthesis of carbon nanotubes is the generation of carbon atoms in a vacuum. It is possible to produce carbon atoms by an electrical discharge through graphite, vaporization of graphite with a laser, and the decomposition of a carbon compound.
The strength of carbon nanotubes will eventually lead to some of their most exciting applications, as a thread produced from several nanotubes will support enormous weight. However, the current applications only employ bulk nanotubes. The addition of nanotubes to polymers improves the mechanical, thermal, and electrical properties of the bulk material. There are currently nanotubes in some bicycle parts, skis, baseball bats, fishing rods, and surfboards.


Phosphorus
The name phosphorus comes from the Greek words meaning light bringing. When phosphorus was first isolated, scientists noted that it glowed in the dark and burned when exposed to air. Phosphorus is the only member of its group that does not occur in the uncombined state in nature; it exists in many allotropic forms. We will consider two of those forms: white phosphorus and red phosphorus.
White phosphorus is a white, waxy solid that melts at 44.2 C and boils at 280 C. It is insoluble in water (in which it is storedsee [link]), is very soluble in carbon disulfide, and bursts into flame in air. As a solid, as a liquid, as a gas, and in solution, white phosphorus exists as P4 molecules with four phosphorus atoms at the corners of a regular tetrahedron, as illustrated in [link]. Each phosphorus atom covalently bonds to the other three atoms in the molecule by single covalent bonds. White phosphorus is the most reactive allotrope and is very toxic.
(a) Because white phosphorus bursts into flame in air, it is stored in water. (b) The structure of white phosphorus consists of P4 molecules arranged in a tetrahedron. (c) Red phosphorus is much less reactive than is white phosphorus. (d) The structure of red phosphorus consists of networks of P4 tetrahedra joined by P-P single bonds. (credit a: modification of work from http://images-of-elements.com/phosphorus.php)




Heating white phosphorus to 270300 C in the absence of air yields red phosphorus. Red phosphorus (shown in [link] and [link]) is denser, has a higher melting point (~600 C), is much less reactive, is essentially nontoxic, and is easier and safer to handle than is white phosphorus. Its structure is highly polymeric and appears to contain three-dimensional networks of P4 tetrahedra joined by P-P single bonds. Red phosphorus is insoluble in solvents that dissolve white phosphorus. When red phosphorus is heated, P4 molecules sublime from the solid.

Sulfur
The allotropy of sulfur is far greater and more complex than that of any other element. Sulfur is the brimstone referred to in the Bible and other places, and references to sulfur occur throughout recorded historyright up to the relatively recent discovery that it is a component of the atmospheres of Venus and of Io, a moon of Jupiter. The most common and most stable allotrope of sulfur is yellow, rhombic sulfur, so named because of the shape or its crystals. Rhombic sulfur is the form to which all other allotropes revert at room temperature. Crystals of rhombic sulfur melt at 113 C. Cooling this liquid gives long needles of monoclinic sulfur. This form is stable from 96 C to the melting point, 119 C. At room temperature, it gradually reverts to the rhombic form.
Both rhombic sulfur and monoclinic sulfur contain S8 molecules in which atoms form eight-membered, puckered rings that resemble crowns, as illustrated in [link]. Each sulfur atom is bonded to each of its two neighbors in the ring by covalent S-S single bonds.
These four sulfur allotropes show eight-membered, puckered rings. Each sulfur atom bonds to each of its two neighbors in the ring by covalent S-S single bonds. Here are (a) individual S8 rings, (b) S8 chains formed when the rings open, (c) longer chains formed by adding sulfur atoms to S8 chains, and (d) part of the very long sulfur chains formed at higher temperatures.




When rhombic sulfur melts, the straw-colored liquid is quite mobile; its viscosity is low because S8 molecules are essentially spherical and offer relatively little resistance as they move past each other. As the temperature rises, S-S bonds in the rings break, and polymeric chains of sulfur atoms result. These chains combine end to end, forming still longer chains that tangle with one another. The liquid gradually darkens in color and becomes so viscous that finally (at about 230 C) it does not pour easily. The dangling atoms at the ends of the chains of sulfur atoms are responsible for the dark red color because their electronic structure differs from those of sulfur atoms that have bonds to two adjacent sulfur atoms. This causes them to absorb light differently and results in a different visible color. Cooling the liquid rapidly produces a rubberlike amorphous mass, called plastic sulfur.
Sulfur boils at 445 C and forms a vapor consisting of S2, S6, and S8 molecules; at about 1000 C, the vapor density corresponds to the formula S2, which is a paramagnetic molecule like O2 with a similar electronic structure and a weak sulfur-sulfur double bond.
As seen in this discussion, an important feature of the structural behavior of the nonmetals is that the elements usually occur with eight electrons in their valence shells. If necessary, the elements form enough covalent bonds to supplement the electrons already present to possess an octet. For example, members of group 15 have five valence elements and require only three additional electrons to fill their valence shells. These elements form three covalent bonds in their free state: triple bonds in the N2 molecule or single bonds to three different atoms in arsenic and phosphorus. The elements of group 16 require only two additional electrons. Oxygen forms a double bond in the O2 molecule, and sulfur, selenium, and tellurium form two single bonds in various rings and chains. The halogens form diatomic molecules in which each atom is involved in only one bond. This provides the electron required necessary to complete the octet on the halogen atom. The noble gases do not form covalent bonds to other noble gas atoms because they already have a filled outer shell.

Key Concepts and Summary
Nonmetals have structures that are very different from those of the metals, primarily because they have greater electronegativity and electrons that are more tightly bound to individual atoms. Most nonmetal oxides are acid anhydrides, meaning that they react with water to form acidic solutions. Molecular structures are common for most of the nonmetals, and several have multiple allotropes with varying physical properties.

Chemistry End of Chapter Exercises


Carbon forms a number of allotropes, two of which are graphite and diamond. Silicon has a diamond structure. Why is there no allotrope of silicon with a graphite structure?




Nitrogen in the atmosphere exists as very stable diatomic molecules. Why does phosphorus form less stable P4 molecules instead of P2 molecules?


In the N2 molecule, the nitrogen atoms have an  bond and two  bonds holding the two atoms together. The presence of three strong bonds makes N2 a very stable molecule. Phosphorus is a third-period element, and as such, does not form  bonds efficiently; therefore, it must fulfill its bonding requirement by forming three  bonds.




Write balanced chemical equations for the reaction of the following acid anhydrides with water:
(a) SO3
(b) N2O3
(c) Cl2O7
(d) P4O10
(e) NO2




Determine the oxidation number of each element in each of the following compounds:
(a) HCN
(b) OF2
(c) AsCl3


(a) H = 1+, C = 2+, and N = 3; (b) O = 2+ and F = 1; (c) As = 3+ and Cl = 1




Determine the oxidation state of sulfur in each of the following:
(a) SO3
(b) SO2
(c) SO32SO32




Arrange the following in order of increasing electronegativity: F; Cl; O; and S.


S < Cl < O < F




Why does white phosphorus consist of tetrahedral P4 molecules while nitrogen consists of diatomic N2 molecules?



Glossary

acid anhydride
compound that reacts with water to form an acid or acidic solution


disproportionation reaction
chemical reaction where a single reactant is simultaneously reduced and oxidized; it is both the reducing agent and the oxidizing agent


