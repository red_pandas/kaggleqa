
The Periodic Table
The Periodic TableBy the end of this section, you will be able to:

State the periodic law and explain the organization of elements in the periodic table
Predict the general properties of elements based on their location within the periodic table
Identify metals, nonmetals, and metalloids by their properties and/or location on the periodic table

As early chemists worked to purify ores and discovered more elements, they realized that various elements could be grouped together by their similar chemical behaviors. One such grouping includes lithium (Li), sodium (Na), and potassium (K): These elements all are shiny, conduct heat and electricity well, and have similar chemical properties. A second grouping includes calcium (Ca), strontium (Sr), and barium (Ba), which also are shiny, good conductors of heat and electricity, and have chemical properties in common. However, the specific properties of these two groupings are notably different from each other. For example: Li, Na, and K are much more reactive than are Ca, Sr, and Ba; Li, Na, and K form compounds with oxygen in a ratio of two of their atoms to one oxygen atom, whereas Ca, Sr, and Ba form compounds with one of their atoms to one oxygen atom. Fluorine (F), chlorine (Cl), bromine (Br), and iodine (I) also exhibit similar properties to each other, but these properties are drastically different from those of any of the elements above.
Dimitri Mendeleev in Russia (1869) and Lothar Meyer in Germany (1870) independently recognized that there was a periodic relationship among the properties of the elements known at that time. Both published tables with the elements arranged according to increasing atomic mass. But Mendeleev went one step further than Meyer: He used his table to predict the existence of elements that would have the properties similar to aluminum and silicon, but were yet unknown. The discoveries of gallium (1875) and germanium (1886) provided great support for Mendeleevs work. Although Mendeleev and Meyer had a long dispute over priority, Mendeleevs contributions to the development of the periodic table are now more widely recognized ([link]).
(a) Dimitri Mendeleev is widely credited with creating (b) the first periodic table of the elements. (credit a: modification of work by Serge Lachinov; credit b: modification of work by Den fjttrade ankan/Wikimedia Commons)




By the twentieth century, it became apparent that the periodic relationship involved atomic numbers rather than atomic masses. The modern statement of this relationship, the periodic law, is as follows: the properties of the elements are periodic functions of their atomic numbers. A modern periodic table arranges the elements in increasing order of their atomic numbers and groups atoms with similar properties in the same vertical column ([link]). Each box represents an element and contains its atomic number, symbol, average atomic mass, and (sometimes) name. The elements are arranged in seven horizontal rows, called periods or series, and 18 vertical columns, called groups. Groups are labeled at the top of each column. In the United States, the labels traditionally were numerals with capital letters. However, IUPAC recommends that the numbers 1 through 18 be used, and these labels are more common. For the table to fit on a single page, parts of two of the rows, a total of 14 columns, are usually written below the main body of the table.
Elements in the periodic table are organized according to their properties.




Many elements differ dramatically in their chemical and physical properties, but some elements are similar in their behaviors. For example, many elements appear shiny, are malleable (able to be deformed without breaking) and ductile (can be drawn into wires), and conduct heat and electricity well. Other elements are not shiny, malleable, or ductile, and are poor conductors of heat and electricity. We can sort the elements into large classes with common properties: metals (elements that are shiny, malleable, good conductors of heat and electricityshaded yellow); nonmetals (elements that appear dull, poor conductors of heat and electricityshaded green); and metalloids (elements that conduct heat and electricity moderately well, and possess some properties of metals and some properties of nonmetalsshaded purple).
The elements can also be classified into the main-group elements (or representative elements) in the columns labeled 1, 2, and 1318; the transition metals in the columns labeled 312; and inner transition metals in the two rows at the bottom of the table (the top-row elements are called lanthanides and the bottom-row elements are actinides; [link]). The elements can be subdivided further by more specific properties, such as the composition of the compounds they form. For example, the elements in group 1 (the first column) form compounds that consist of one atom of the element and one atom of hydrogen. These elements (except hydrogen) are known as alkali metals, and they all have similar chemical properties. The elements in group 2 (the second column) form compounds consisting of one atom of the element and two atoms of hydrogen: These are called alkaline earth metals, with similar properties among members of that group. Other groups with specific names are the pnictogens (group 15), chalcogens (group 16), halogens (group 17), and the noble gases (group 18, also known as inert gases). The groups can also be referred to by the first element of the group: For example, the chalcogens can be called the oxygen group or oxygen family. Hydrogen is a unique, nonmetallic element with properties similar to both group 1A and group 7A elements. For that reason, hydrogen may be shown at the top of both groups, or by itself.
The periodic table organizes elements with similar properties into groups.







Click on this link for an interactive periodic table, which you can use to explore the properties of the elements (includes podcasts and videos of each element). You may also want to try this one that shows photos of all the elements.

Naming Groups of Elements
Atoms of each of the following elements are essential for life. Give the group name for the following elements:
(a) chlorine
(b) calcium
(c) sodium
(d) sulfur
Solution
The family names are as follows:
(a) halogen
(b) alkaline earth metal
(c) alkali metal
(d) chalcogen
Check Your Learning
Give the group name for each of the following elements:
(a) krypton
(b) selenium
(c) barium
(d) lithium
Answer:
(a) noble gas; (b) chalcogen; (c) alkaline earth metal; (d) alkali metal


In studying the periodic table, you might have noticed something about the atomic masses of some of the elements. Element 43 (technetium), element 61 (promethium), and most of the elements with atomic number 84 (polonium) and higher have their atomic mass given in square brackets. This is done for elements that consist entirely of unstable, radioactive isotopes (you will learn more about radioactivity in the nuclear chemistry chapter). An average atomic weight cannot be determined for these elements because their radioisotopes may vary significantly in relative abundance, depending on the source, or may not even exist in nature. The number in square brackets is the atomic mass number (and approximate atomic mass) of the most stable isotope of that element.
Key Concepts and Summary
The discovery of the periodic recurrence of similar properties among the elements led to the formulation of the periodic table, in which the elements are arranged in order of increasing atomic number in rows known as periods and columns known as groups. Elements in the same group of the periodic table have similar chemical properties. Elements can be classified as metals, metalloids, and nonmetals, or as a main-group elements, transition metals, and inner transition metals. Groups are numbered 118 from left to right. The elements in group 1 are known as the alkali metals; those in group 2 are the alkaline earth metals; those in 15 are the pnictogens; those in 16 are the chalcogens; those in 17 are the halogens; and those in 18 are the noble gases.

Chemistry End of Chapter Exercises


Using the periodic table, classify each of the following elements as a metal or a nonmetal, and then further classify each as a main-group (representative) element, transition metal, or inner transition metal:
(a) uranium
(b) bromine
(c) strontium
(d) neon
(e) gold
(f) americium
(g) rhodium
(h) sulfur
(i) carbon
(j) potassium

(a) metal, inner transition metal; (b) nonmetal, representative element; (c) metal, representative element; (d) nonmetal, representative element; (e) metal, transition metal; (f) metal, inner transition metal; (g) metal, transition metal; (h) nonmetal, representative element; (i) nonmetal, representative element; (j) metal, representative element




Using the periodic table, classify each of the following elements as a metal or a nonmetal, and then further classify each as a main-group (representative) element, transition metal, or inner transition metal:
(a) cobalt
(b) europium
(c) iodine
(d) indium
(e) lithium
(f) oxygen
(h) cadmium
(i) terbium
(j) rhenium




Using the periodic table, identify the lightest member of each of the following groups:
(a) noble gases
(b) alkaline earth metals
(c) alkali metals
(d) chalcogens


(a) He; (b) Be; (c) Li; (d) O




Using the periodic table, identify the heaviest member of each of the following groups:
(a) alkali metals
(b) chalcogens
(c) noble gases
(d) alkaline earth metals




Use the periodic table to give the name and symbol for each of the following elements:
(a) the noble gas in the same period as germanium
(b) the alkaline earth metal in the same period as selenium
(c) the halogen in the same period as lithium
(d) the chalcogen in the same period as cadmium


(a) krypton, Kr; (b) calcium, Ca; (c) fluorine, F; (d) tellurium, Te




Use the periodic table to give the name and symbol for each of the following elements:
(a) the halogen in the same period as the alkali metal with 11 protons
(b) the alkaline earth metal in the same period with the neutral noble gas with 18 electrons
(c) the noble gas in the same row as an isotope with 30 neutrons and 25 protons
(d) the noble gas in the same period as gold




Write a symbol for each of the following neutral isotopes. Include the atomic number and mass number for each.
(a) the alkali metal with 11 protons and a mass number of 23
(b) the noble gas element with and 75 neutrons in its nucleus and 54 electrons in the neutral atom
(c) the isotope with 33 protons and 40 neutrons in its nucleus
(d) the alkaline earth metal with 88 electrons and 138 neutrons





(a) 1123Na1123Na; 

(b) 54129Xe54129Xe; 

(c) 3373As3373As; 

(d) 88226Ra88226Ra





Write a symbol for each of the following neutral isotopes. Include the atomic number and mass number for each.
(a) the chalcogen with a mass number of 125
(b) the halogen whose longest-lived isotope is radioactive
(c) the noble gas, used in lighting, with 10 electrons and 10 neutrons
(d) the lightest alkali metal with three neutrons



Glossary

actinideinner transition metal in the bottom of the bottom two rows of the periodic table


alkali metalelement in group 1


alkaline earth metalelement in group 2


chalcogenelement in group 16


groupvertical column of the periodic table


halogenelement in group 17


inert gas(also, noble gas) element in group 18


inner transition metal(also, lanthanide or actinide) element in the bottom two rows; if in the first row, also called lanthanide, or if in the second row, also called actinide


lanthanideinner transition metal in the top of the bottom two rows of the periodic table


main-group element(also, representative element) element in columns 1, 2, and 1218


metalelement that is shiny, malleable, good conductor of heat and electricity


metalloidelement that conducts heat and electricity moderately well, and possesses some properties of metals and some properties of nonmetals


noble gas(also, inert gas) element in group 18


nonmetalelement that appears dull, poor conductor of heat and electricity


period(also, series) horizontal row of the periodic table


periodic lawproperties of the elements are periodic function of their atomic numbers.


periodic tabletable of the elements that places elements with similar chemical properties close together


pnictogenelement in group 15


representative element(also, main-group element) element in columns 1, 2, and 1218


series(also, period) horizontal row of the period table


transition metalelement in columns 311


