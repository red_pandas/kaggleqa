
Radioactive Decay
Radioactive DecayBy the end of this section, you will be able to:

Recognize common modes of radioactive decay
Identify common particles and energies involved in nuclear decay reactions
Write and balance nuclear decay equations
Calculate kinetic parameters for decay processes, including half-life
Describe common radiometric dating techniques

Following the somewhat serendipitous discovery of radioactivity by Becquerel, many prominent scientists began to investigate this new, intriguing phenomenon. Among them were Marie Curie (the first woman to win a Nobel Prize, and the only person to win two Nobel Prizes in different scienceschemistry and physics), who was the first to coin the term radioactivity, and Ernest Rutherford (of gold foil experiment fame), who investigated and named three of the most common types of radiation. During the beginning of the twentieth century, many radioactive substances were discovered, the properties of radiation were investigated and quantified, and a solid understanding of radiation and nuclear decay was developed.
The spontaneous change of an unstable nuclide into another is radioactive decay. The unstable nuclide is called the parent nuclide; the nuclide that results from the decay is known as the daughter nuclide. The daughter nuclide may be stable, or it may decay itself. The radiation produced during radioactive decay is such that the daughter nuclide lies closer to the band of stability than the parent nuclide, so the location of a nuclide relative to the band of stability can serve as a guide to the kind of decay it will undergo ([link]).
A nucleus of uranium-238 (the parent nuclide) undergoes  decay to form thorium-234 (the daughter nuclide). The alpha particle removes two protons (green) and two neutrons (gray) from the uranium-238 nucleus.








Although the radioactive decay of a nucleus is too small to see with the naked eye, we can indirectly view radioactive decay in an environment called a cloud chamber. Click here to learn about cloud chambers and to view an interesting Cloud Chamber Demonstration from the Jefferson Lab.

Types of Radioactive Decay
Ernest Rutherfords experiments involving the interaction of radiation with a magnetic or electric field ([link]) helped him determine that one type of radiation consisted of positively charged and relatively massive  particles; a second type was made up of negatively charged and much less massive  particles; and a third was uncharged electromagnetic waves,  rays. We now know that  particles are high-energy helium nuclei,  particles are high-energy electrons, and  radiation compose high-energy electromagnetic radiation. We classify different types of radioactive decay by the radiation produced.
Alpha particles, which are attracted to the negative plate and deflected by a relatively small amount, must be positively charged and relatively massive. Beta particles, which are attracted to the positive plate and deflected a relatively large amount, must be negatively charged and relatively light. Gamma rays, which are unaffected by the electric field, must be uncharged.




Alpha () decay is the emission of an  particle from the nucleus. For example, polonium-210 undergoes  decay:
84210Po

24He
+
82206Pb
or
84210Po

24
+
82206Pb
84210Po

24He
+
82206Pb
or
84210Po

24
+
82206Pb
Alpha decay occurs primarily in heavy nuclei (A > 200, Z > 83). Because the loss of an  particle gives a daughter nuclide with a mass number four units smaller and an atomic number two units smaller than those of the parent nuclide, the daughter nuclide has a larger n:p ratio than the parent nuclide. If the parent nuclide undergoing  decay lies below the band of stability (refer to [link]), the daughter nuclide will lie closer to the band.Beta () decay is the emission of an electron from a nucleus. Iodine-131 is an example of a nuclide that undergoes  decay:

53131I

-10e
+
54131X
or
53131I

-10
+
54131Xe

53131I

-10e
+
54131X
or
53131I

-10
+
54131Xe
Beta decay, which can be thought of as the conversion of a neutron into a proton and a  particle, is observed in nuclides with a large n:p ratio. The beta particle (electron) emitted is from the atomic nucleus and is not one of the electrons surrounding the nucleus. Such nuclei lie above the band of stability. Emission of an electron does not change the mass number of the nuclide but does increase the number of its protons and decrease the number of its neutrons. Consequently, the n:p ratio is decreased, and the daughter nuclide lies closer to the band of stability than did the parent nuclide.
Gamma emission ( emission) is observed when a nuclide is formed in an excited state and then decays to its ground state with the emission of a  ray, a quantum of high-energy electromagnetic radiation. The presence of a nucleus in an excited state is often indicated by an asterisk (*). Cobalt-60 emits  radiation and is used in many applications including cancer treatment:

2760Co*

00
+
2760Co

2760Co*

00
+
2760Co
There is no change in mass number or atomic number during the emission of a  ray unless the  emission accompanies one of the other modes of decay.
Positron emission (+ decay) is the emission of a positron from the nucleus. Oxygen-15 is an example of a nuclide that undergoes positron emission:

815O

+10e
+
715N
or
815O

+10
+
715N

815O

+10e
+
715N
or
815O

+10
+
715N
Positron emission is observed for nuclides in which the n:p ratio is low. These nuclides lie below the band of stability. Positron decay is the conversion of a proton into a neutron with the emission of a positron. The n:p ratio increases, and the daughter nuclide lies closer to the band of stability than did the parent nuclide.
Electron capture occurs when one of the inner electrons in an atom is captured by the atoms nucleus. For example, potassium-40 undergoes electron capture:

1940K
+
-10e

1840Ar

1940K
+
-10e

1840Ar
Electron capture occurs when an inner shell electron combines with a proton and is converted into a neutron. The loss of an inner shell electron leaves a vacancy that will be filled by one of the outer electrons. As the outer electron drops into the vacancy, it will emit energy. In most cases, the energy emitted will be in the form of an X-ray. Like positron emission, electron capture occurs for proton-rich nuclei that lie below the band of stability. Electron capture has the same effect on the nucleus as does positron emission: The atomic number is decreased by one and the mass number does not change. This increases the n:p ratio, and the daughter nuclide lies closer to the band of stability than did the parent nuclide. Whether electron capture or positron emission occurs is difficult to predict. The choice is primarily due to kinetic factors, with the one requiring the smaller activation energy being the one more likely to occur.
[link] summarizes these types of decay, along with their equations and changes in atomic and mass numbers.
This table summarizes the type, nuclear equation, representation, and any changes in the mass or atomic numbers for various types of decay.




PET Scan
Positron emission tomography (PET) scans use radiation to diagnose and track health conditions and monitor medical treatments by revealing how parts of a patients body function ([link]). To perform a PET scan, a positron-emitting radioisotope is produced in a cyclotron and then attached to a substance that is used by the part of the body being investigated. This tagged compound, or radiotracer, is then put into the patient (injected via IV or breathed in as a gas), and how it is used by the tissue reveals how that organ or other area of the body functions.
A PET scanner (a) uses radiation to provide an image of how part of a patients body functions. The scans it produces can be used to image a healthy brain (b) or can be used for diagnosing medical conditions such as Alzheimers disease (c). (credit a: modification of work by Jens Maus)




For example, F-18 is produced by proton bombardment of 18O (
818O
+
11p

918F
+
01n
)(
818O
+
11p

918F
+
01n
) and incorporated into a glucose analog called fludeoxyglucose (FDG). How FDG is used by the body provides critical diagnostic information; for example, since cancers use glucose differently than normal tissues, FDG can reveal cancers. The 18F emits positrons that interact with nearby electrons, producing a burst of gamma radiation. This energy is detected by the scanner and converted into a detailed, three-dimensional, color image that shows how that part of the patients body functions. Different levels of gamma radiation produce different amounts of brightness and colors in the image, which can then be interpreted by a radiologist to reveal what is going on. PET scans can detect heart damage and heart disease, help diagnose Alzheimers disease, indicate the part of a brain that is affected by epilepsy, reveal cancer, show what stage it is, and how much it has spread, and whether treatments are effective. Unlike magnetic resonance imaging and X-rays, which only show how something looks, the big advantage of PET scans is that they show how something functions. PET scans are now usually performed in conjunction with a computed tomography scan.

Radioactive Decay Series
The naturally occurring radioactive isotopes of the heaviest elements fall into chains of successive disintegrations, or decays, and all the species in one chain constitute a radioactive family, or radioactive decay series. Three of these series include most of the naturally radioactive elements of the periodic table. They are the uranium series, the actinide series, and the thorium series. The neptunium series is a fourth series, which is no longer significant on the earth because of the short half-lives of the species involved. Each series is characterized by a parent (first member) that has a long half-life and a series of daughter nuclides that ultimately lead to a stable end-productthat is, a nuclide on the band of stability ([link]). In all three series, the end-product is a stable isotope of lead. The neptunium series, previously thought to terminate with bismuth-209, terminates with thallium-205.
Uranium-238 undergoes a radioactive decay series consisting of 14 separate steps before producing stable lead-206. This series consists of eight  decays and six  decays.





Radioactive Half-Lives
Radioactive decay follows first-order kinetics. Since first-order reactions have already been covered in detail in the kinetics chapter, we will now apply those concepts to nuclear decay reactions. Each radioactive nuclide has a characteristic, constant half-life (t1/2), the time required for half of the atoms in a sample to decay. An isotopes half-life allows us to determine how long a sample of a useful isotope will be available, and how long a sample of an undesirable or dangerous isotope must be stored before it decays to a low-enough radiation level that is no longer a problem.
For example, cobalt-60, an isotope that emits gamma rays used to treat cancer, has a half-life of 5.27 years ([link]). In a given cobalt-60 source, since half of the 
2760Co

2760Co
 nuclei decay every 5.27 years, both the amount of material and the intensity of the radiation emitted is cut in half every 5.27 years. (Note that for a given substance, the intensity of radiation that it produces is directly proportional to the rate of decay of the substance and the amount of the substance.) This is as expected for a process following first-order kinetics. Thus, a cobalt-60 source that is used for cancer treatment must be replaced regularly to continue to be effective.For cobalt-60, which has a half-life of 5.27 years, 50% remains after 5.27 years (one half-life), 25% remains after 10.54 years (two half-lives), 12.5% remains after 15.81 years (three half-lives), and so on.




Since nuclear decay follows first-order kinetics, we can adapt the mathematical relationships used for first-order chemical reactions. We generally substitute the number of nuclei, N, for the concentration. If the rate is stated in nuclear decays per second, we refer to it as the activity of the radioactive sample. The rate for radioactive decay is:
decay rate = N with  = the decay constant for the particular radioisotope
The decay constant, , which is the same as a rate constant discussed in the kinetics chapter. It is possible to express the decay constant in terms of the half-life, t1/2:
=ln 2t1/2=0.693t1/2ort1/2=ln 2=0.693=ln 2t1/2=0.693t1/2ort1/2=ln 2=0.693
The first-order equations relating amount, N, and time are:
Nt=N0ektort=1ln(NtN0)Nt=N0ektort=1ln(NtN0)
where N0 is the initial number of nuclei or moles of the isotope, and Nt is the number of nuclei/moles remaining at time t. [link] applies these calculations to find the rates of radioactive decay for specific nuclides.
Rates of Radioactive Decay
2760Co

2760Co
 decays with a half-life of 5.27 years to produce 
2860Ni
.
2860Ni
.(a) What is the decay constant for the radioactive disintegration of cobalt-60?
(b) Calculate the fraction of a sample of the 
2760Co

2760Co
 isotope that will remain after 15 years.(c) How long does it take for a sample of 
2760Co

2760Co
 to disintegrate to the extent that only 2.0% of the original amount remains?Solution
(a) The value of the rate constant is given by:
=ln 2t1/2=0.6935.27y=0.132y1=ln 2t1/2=0.6935.27y=0.132y1
(b) The fraction of 
2760Co

2760Co
 that is left after time t is given by NtN0.NtN0. Rearranging the first-order relationship Nt = N0et to solve for this ratio yields:NtN0=et=e(0.132/y)(15.0/y)=0.138NtN0=et=e(0.132/y)(15.0/y)=0.138
The fraction of 
2760Co

2760Co
 that will remain after 15.0 years is 0.138. Or put another way, 13.8% of the 
2760Co

2760Co
 originally present will remain after 15 years.(c) 2.00% of the original amount of 
2760Co

2760Co
 is equal to 0.0200  N0. Substituting this into the equation for time for first-order kinetics, we have:t=1ln(NtN0)=10.132y1ln(0.0200N0N0)=29.6yt=1ln(NtN0)=10.132y1ln(0.0200N0N0)=29.6y
Check Your LearningRadon-222, 
86222Rn
,
86222Rn
, has a half-life of 3.823 days. How long will it take a sample of radon-222 with a mass of 0.750 g to decay into other elements, leaving only 0.100 g of radon-222?Answer:
11.1 days


Because each nuclide has a specific number of nucleons, a particular balance of repulsion and attraction, and its own degree of stability, the half-lives of radioactive nuclides vary widely. For example: the half-life of 
83209Bi

83209Bi
 is 1.9  1019 years; 
94239Ra

94239Ra
 is 24,000 years; 
86222Rn
86222Rn is 3.82 days; and element-111 (Rg for roentgenium) is 1.5  103 seconds. The half-lives of a number of radioactive isotopes important to medicine are shown in [link], and others are listed in Appendix M.

Half-lives of Radioactive Isotopes Important to Medicine


Type1
Decay Mode
Half-Life
Uses



F-18
+ decay
110. minutes
PET scans


Co-60
 decay,  decay
5.27 years
cancer treatment


Tc-99m
 decay
8.01 hours
scans of brain, lung, heart, bone


I-131
 decay
8.02 days
thyroid scans and treatment


Tl-201
electron capture
73 hours
heart and arteries scans; cardiac stress tests



Radiometric Dating
Several radioisotopes have half-lives and other properties that make them useful for purposes of dating the origin of objects such as archaeological artifacts, formerly living organisms, or geological formations. This process is radiometric dating and has been responsible for many breakthrough scientific discoveries about the geological history of the earth, the evolution of life, and the history of human civilization. We will explore some of the most common types of radioactive dating and how the particular isotopes work for each type.
Radioactive Dating Using Carbon-14
The radioactivity of carbon-14 provides a method for dating objects that were a part of a living organism. This method of radiometric dating, which is also called radiocarbon dating or carbon-14 dating, is accurate for dating carbon-containing substances that are up to about 30,000 years old, and can provide reasonably accurate dates up to a maximum of about 50,000 years old.
Naturally occurring carbon consists of three isotopes: 
612C
,
612C
, which constitutes about 99% of the carbon on earth; 
613C
,
613C
, about 1% of the total; and trace amounts of 
614C
.
614C
. Carbon-14 forms in the upper atmosphere by the reaction of nitrogen atoms with neutrons from cosmic rays in space:
714N
+
01n

614C
+
11H

714N
+
01n

614C
+
11H
All isotopes of carbon react with oxygen to produce CO2 molecules. The ratio of 
614C
O2
614C
O2 to 
612C
O2
612C
O2 depends on the ratio of 
614C
O
614C
O to 
612C
O
612C
O in the atmosphere. The natural abundance of 
614C
O
614C
O in the atmosphere is approximately 1 part per trillion; until recently, this has generally been constant over time, as seen is gas samples found trapped in ice. The incorporation of 
614C
614C
O2
614C
614C
O2 and 
612C
O2
612C
O2 into plants is a regular part of the photosynthesis process, which means that the 
614C
:
612C

614C
:
612C
 ratio found in a living plant is the same as the 
614C
:
612C

614C
:
612C
 ratio in the atmosphere. But when the plant dies, it no longer traps carbon through photosynthesis. Because 
612C

612C
 is a stable isotope and does not undergo radioactive decay, its concentration in the plant does not change. However, carbon-14 decays by  emission with a half-life of 5730 years:
614C

712N
+
-10e

614C

712N
+
-10e
Thus, the 
614C
:
612C

614C
:
612C
 ratio gradually decreases after the plant dies. The decrease in the ratio with time provides a measure of the time that has elapsed since the death of the plant (or other organism that ate the plant). [link] visually depicts this process.Along with stable carbon-12, radioactive carbon-14 is taken in by plants and animals, and remains at a constant level within them while they are alive. After death, the C-14 decays and the C-14:C-12 ratio in the remains decreases. Comparing this ratio to the C-14:C-12 ratio in living organisms allows us to determine how long ago the organism lived (and died).




For example, with the half-life of 
614C

614C
 being 5730 years, if the 
614C
:
612C

614C
:
612C
 ratio in a wooden object found in an archaeological dig is half what it is in a living tree, this indicates that the wooden object is 5730 years old. Highly accurate determinations of 
614C
:
612C

614C
:
612C
 ratios can be obtained from very small samples (as little as a milligram) by the use of a mass spectrometer.



Visit this website to perform simulations of radiometric dating.

Radiocarbon Dating
A tiny piece of paper (produced from formerly living plant matter) taken from the Dead Sea Scrolls has an activity of 10.8 disintegrations per minute per gram of carbon. If the initial C-14 activity was 13.6 disintegrations/min/g of C, estimate the age of the Dead Sea Scrolls.
Solution
The rate of decay (number of disintegrations/minute/gram of carbon) is proportional to the amount of radioactive C-14 left in the paper, so we can substitute the rates for the amounts, N, in the relationship:
t=1ln(NtN0)t=1ln(RatetRate0)t=1ln(NtN0)t=1ln(RatetRate0)
where the subscript 0 represents the time when the plants were cut to make the paper, and the subscript t represents the current time.
The decay constant can be determined from the half-life of C-14, 5730 years:
=ln 2t1/2=0.6935730 y=1.21104y1=ln 2t1/2=0.6935730 y=1.21104y1
Substituting and solving, we have:
t=1ln(RatetRate0)=11.21104y1ln(10.8dis/min/g C13.6dis/min/g C)=1910 yt=1ln(RatetRate0)=11.21104y1ln(10.8dis/min/g C13.6dis/min/g C)=1910 y
Therefore, the Dead Sea Scrolls are approximately 1900 years old ([link]).
Carbon-14 dating has shown that these pages from the Dead Sea Scrolls were written or copied on paper made from plants that died between 100 BC and AD 50.




Check Your Learning
More accurate dates of the reigns of ancient Egyptian pharaohs have been determined recently using plants that were preserved in their tombs. Samples of seeds and plant matter from King Tutankhamuns tomb have a C-14 decay rate of 9.07 disintegrations/min/g of C. How long ago did King Tuts reign come to an end?
Answer:
about 3350 years ago, or approximately 1340 BC


There have been some significant, well-documented changes to the 
614C
:
612C

614C
:
612C
 ratio. The accuracy of a straightforward application of this technique depends on the 
614C
:
612C

614C
:
612C
 ratio in a living plant being the same now as it was in an earlier era, but this is not always valid. Due to the increasing accumulation of CO2 molecules (largely 
612C
O2)
612C
O2) in the atmosphere caused by combustion of fossil fuels (in which essentially all of the 
614C

614C
 has decayed), the ratio of 
614C
:
612C

614C
:
612C
 in the atmosphere may be changing. This manmade increase in 
612C
O2
612C
O2 in the atmosphere causes the 
614C
:
612C

614C
:
612C
 ratio to decrease, and this in turn affects the ratio in currently living organisms on the earth. Fortunately, however, we can use other data, such as tree dating via examination of annual growth rings, to calculate correction factors. With these correction factors, accurate dates can be determined. In general, radioactive dating only works for about 10 half-lives; therefore, the limit for carbon-14 dating is about 57,000 years.
Radioactive Dating Using Nuclides Other than Carbon-14
Radioactive dating can also use other radioactive nuclides with longer half-lives to date older events. For example, uranium-238 (which decays in a series of steps into lead-206) can be used for establishing the age of rocks (and the approximate age of the oldest rocks on earth). Since U-238 has a half-life of 4.5 billion years, it takes that amount of time for half of the original U-238 to decay into Pb-206. In a sample of rock that does not contain appreciable amounts of Pb-208, the most abundant isotope of lead, we can assume that lead was not present when the rock was formed. Therefore, by measuring and analyzing the ratio of U-238:Pb-206, we can determine the age of the rock. This assumes that all of the lead-206 present came from the decay of uranium-238. If there is additional lead-206 present, which is indicated by the presence of other lead isotopes in the sample, it is necessary to make an adjustment. Potassium-argon dating uses a similar method. K-40 decays by positron emission and electron capture to form Ar-40 with a half-life of 1.25 billion years. If a rock sample is crushed and the amount of Ar-40 gas that escapes is measured, determination of the Ar-40:K-40 ratio yields the age of the rock. Other methods, such as rubidium-strontium dating (Rb-87 decays into Sr-87 with a half-life of 48.8 billion years), operate on the same principle. To estimate the lower limit for the earths age, scientists determine the age of various rocks and minerals, making the assumption that the earth is older than the oldest rocks and minerals in its crust. As of 2014, the oldest known rocks on earth are the Jack Hills zircons from Australia, found by uranium-lead dating to be almost 4.4 billion years old.
Radioactive Dating of Rocks
An igneous rock contains 9.58  105 g of U-238 and 2.51  105 g of Pb-206, and much, much smaller amounts of Pb-208. Determine the approximate time at which the rock formed.
Solution
The sample of rock contains very little Pb-208, the most common isotope of lead, so we can safely assume that all the Pb-206 in the rock was produced by the radioactive decay of U-238. When the rock formed, it contained all of the U-238 currently in it, plus some U-238 that has since undergone radioactive decay.
The amount of U-238 currently in the rock is:
9.58105g U(1 mol U238g U)=4.03107mol U9.58105g U(1 mol U238g U)=4.03107mol U
Because when one mole of U-238 decays, it produces one mole of Pb-206, the amount of U-238 that has undergone radioactive decay since the rock was formed is:
2.51105g Pb(1mol Pb206g Pb)(1 mol U1mol Pb)=1.22107mol U2.51105g Pb(1mol Pb206g Pb)(1 mol U1mol Pb)=1.22107mol U
The total amount of U-238 originally present in the rock is therefore:
4.03107mol+1.22107mol=5.25107mol U4.03107mol+1.22107mol=5.25107mol U
The amount of time that has passed since the formation of the rock is given by:
t=1ln(NtN0)t=1ln(NtN0)
with N0 representing the original amount of U-238 and Nt representing the present amount of U-238.
U-238 decays into Pb-206 with a half-life of 4.5  109 y, so the decay constant  is:
=ln 2t1/2=0.6934.5109y=1.541010y1=ln 2t1/2=0.6934.5109y=1.541010y1
Substituting and solving, we have:
t=11.541010y1ln(4.03107mol U5.25107mol U)=1.7109yt=11.541010y1ln(4.03107mol U5.25107mol U)=1.7109y
Therefore, the rock is approximately 1.7 billion years old.
Check Your Learning
A sample of rock contains 6.14  104 g of Rb-87 and 3.51  105 g of Sr-87. Calculate the age of the rock. (The half-life of the  decay of Rb-87 is 4.7  1010 y.)
Answer:
3.7  109 y




Key Concepts and Summary
Nuclei that have unstable n:p ratios undergo spontaneous radioactive decay. The most common types of radioactivity are  decay,  decay,  emission, positron emission, and electron capture. Nuclear reactions also often involve  rays, and some nuclei decay by electron capture. Each of these modes of decay leads to the formation of a new nucleus with a more stable n:p ratio. Some substances undergo radioactive decay series, proceeding through multiple decays before ending in a stable isotope. All nuclear decay processes follow first-order kinetics, and each radioisotope has its own characteristic half-life, the time that is required for half of its atoms to decay. Because of the large differences in stability among nuclides, there is a very wide range of half-lives of radioactive substances. Many of these substances have found useful applications in medical diagnosis and treatment, determining the age of archaeological and geological objects, and more.

Key Equations

decay rate = N
t1/2=ln 2=0.693t1/2=ln 2=0.693


Chemistry End of Chapter Exercises


What are the types of radiation emitted by the nuclei of radioactive elements?


 (helium nuclei),  (electrons), + (positrons), and  (neutrons) may be emitted from a radioactive element, all of which are particles;  rays also may be emitted.




What changes occur to the atomic number and mass of a nucleus during each of the following decay scenarios?
(a) an  particle is emitted
(b) a  particle is emitted
(c)  radiation is emitted
(d) a positron is emitted
(e) an electron is captured




What is the change in the nucleus that results from the following decay scenarios?
(a) emission of a  particle
(b) emission of a + particle
(c) capture of an electron


(a) conversion of a neutron to a proton: 
01n

11p
+
+10e
;
01n

11p
+
+10e
; (b) conversion of a proton to a neutron; the positron has the same mass as an electron and the same magnitude of positive charge as the electron has negative charge; when the n:p ratio of a nucleus is too low, a proton is converted into a neutron with the emission of a positron: 
11p

01n
+
+10e
;
11p

01n
+
+10e
; (c) In a proton-rich nucleus, an inner atomic electron can be absorbed. In simplest form, this changes a proton into a neutron: 
11p
+
-10e

01p

11p
+
-10e

01p




Many nuclides with atomic numbers greater than 83 decay by processes such as electron emission. Explain the observation that the emissions from these unstable nuclides also normally include  particles.




Why is electron capture accompanied by the emission of an X-ray?


The electron pulled into the nucleus was most likely found in the 1s orbital. As an electron falls from a higher energy level to replace it, the difference in the energy of the replacement electron in its two energy levels is given off as an X-ray.




Explain, in terms of [link], how unstable heavy nuclides (atomic number > 83) may decompose to form nuclides of greater stability (a) if they are below the band of stability and (b) if they are above the band of stability.



Which of the following nuclei is most likely to decay by positron emission? Explain your choice.
(a) chromium-53
(b) manganese-51
(c) iron-59


Manganese-51 is most likely to decay by positron emission. The n:p ratio for Cr-53 is 29242924 = 1.21; for Mn-51, it is 26252625 = 1.04; for Fe-59, it is 33263326 = 1.27. Positron decay occurs when the n:p ratio is low. Mn-51 has the lowest n:p ratio and therefore is most likely to decay by positron emission. Besides, 
2453Cr

2453Cr
 is a stable isotope, and 
2659Fe

2659Fe
 decays by beta emission.


The following nuclei do not lie in the band of stability. How would they be expected to decay? Explain your answer.
(a) 
1534P

1534P

(b) 
92239U

92239U

(c) 
2038Ca

2038Ca

(d) 
13H

13H

(e) 
94245Pu

94245Pu



The following nuclei do not lie in the band of stability. How would they be expected to decay?
(a) 
1528P

1528P

(b) 
92235U

92235U

(c) 
2037Ca

2037Ca

(d) 
39L
i
39L
i
(e) 
96245Cm

96245Cm



(a)  decay; (b)  decay; (c) positron emission; (d)  decay; (e)  decay


Predict by what mode(s) of spontaneous radioactive decay each of the following unstable isotopes might proceed:
(a) 
26H
e
26H
e
(b) 
3060Zn

3060Zn

(c) 
91235Pa

91235Pa

(d) 
94241Np

94241Np

(e) 18F
(f) 129Ba
(g) 237Pu


Write a nuclear reaction for each step in the formation of 
84218Po

84218Po
 from 
98238U
,
98238U
, which proceeds by a series of decay reactions involving the step-wise emission of , , , , ,  particles, in that order.



92238U

90234Th
+
24He
;
92238U

90234Th
+
24He
; 
90234Th

91234Pa
+
-10e
;
90234Th

91234Pa
+
-10e
; 
91234Pa

92234U
+
-10e
;
91234Pa

92234U
+
-10e
; 
92234U

90230Th
+
24He

92234U

90230Th
+
24He
 
90230Th

88226Ra
+
24He

90230Th

88226Ra
+
24He
 
88226Ra

86222Rn
+
24He
;
88226Ra

86222Rn
+
24He
; 
86222Rn

84218Po
+
24He

86222Rn

84218Po
+
24He



Write a nuclear reaction for each step in the formation of 
82208Pb

82208Pb
 from 
90228T
h,
90228T
h, which proceeds by a series of decay reactions involving the step-wise emission of , , , , , ,  particles, in that order.





Define the term half-life and illustrate it with an example.


Half-life is the time required for half the atoms in a sample to decay. Example (answers may vary): For C-14, the half-life is 5770 years. A 10-g sample of C-14 would contain 5 g of C-14 after 5770 years; a 0.20-g sample of C-14 would contain 0.10 g after 5770 years.




A 1.00  106-g sample of nobelium, 
102254No
,
102254No
, has a half-life of 55 seconds after it is formed. What is the percentage of 
102254No

102254No
 remaining at the following times?(a) 5.0 min after it forms
(b) 1.0 h after it forms




239Pu is a nuclear waste byproduct with a half-life of 24,000 y. What fraction of the 239Pu present today will be present in 1000 y?


(12)0.04=0.973(12)0.04=0.973 or 97.3%




The isotope 208Tl undergoes  decay with a half-life of 3.1 min.
(a) What isotope is produced by the decay?
(b) How long will it take for 99.0% of a sample of pure 208Tl to decay?
(c) What percentage of a sample of pure 208Tl remains un-decayed after 1.0 h?




If 1.000 g of 
88226Ra

88226Ra
 produces 0.0001 mL of the gas 
86222Rn

86222Rn
 at STP (standard temperature and pressure) in 24 h, what is the half-life of 226Ra in years?

2  103 y




The isotope 
3890Sr

3890Sr
 is one of the extremely hazardous species in the residues from nuclear power generation. The strontium in a 0.500-g sample diminishes to 0.393 g in 10.0 y. Calculate the half-life.



Technetium-99 is often used for assessing heart, liver, and lung damage because certain technetium compounds are absorbed by damaged tissues. It has a half-life of 6.0 h. Calculate the rate constant for the decay of 
4399Tc
.
4399Tc
.

0.12 h1




What is the age of mummified primate skin that contains 8.25% of the original quantity of 14C?




A sample of rock was found to contain 8.23 mg of rubidium-87 and 0.47 mg of strontium-87.
(a) Calculate the age of the rock if the half-life of the decay of rubidium by  emission is 4.7  1010 y.
(b) If some 
3887Sr

3887Sr
 was initially present in the rock, would the rock be younger, older, or the same age as the age calculated in (a)? Explain your answer.

(a) 3.8 billion years;
(b) The rock would be younger than the age calculated in part (a). If Sr was originally in the rock, the amount produced by radioactive decay would equal the present amount minus the initial amount. As this amount would be smaller than the amount used to calculate the age of the rock and the age is proportional to the amount of Sr, the rock would be younger.



A laboratory investigation shows that a sample of uranium ore contains 5.37 mg of 
92238U

92238U
 and 2.52 mg of 
82206Pb
.
82206Pb
. Calculate the age of the ore. The half-life of 
92238U

92238U
 is 4.5  109 yr.



Plutonium was detected in trace amounts in natural uranium deposits by Glenn Seaborg and his associates in 1941. They proposed that the source of this 239Pu was the capture of neutrons by 238U nuclei. Why is this plutonium not likely to have been trapped at the time the solar system formed 4.7  109 years ago?


c = 0; This shows that no Pu-239 could remain since the formation of the earth. Consequently, the plutonium now present could not have been formed with the uranium.



A 
47Be
47Be atom (mass = 7.0169 amu) decays into a 
37L
i
37L
i atom (mass = 7.0160 amu) by electron capture. How much energy (in millions of electron volts, MeV) is produced by this reaction?


A 
58B

58B
 atom (mass = 8.0246 amu) decays into a 
48B

48B
 atom (mass = 8.0053 amu) by loss of a + particle (mass = 0.00055 amu) or by electron capture. How much energy (in millions of electron volts) is produced by this reaction?


17.5 MeV



Isotopes such as 26Al (half-life: 7.2  105 years) are believed to have been present in our solar system as it formed, but have since decayed and are now called extinct nuclides.
(a) 26Al decays by + emission or electron capture. Write the equations for these two nuclear transformations.
(b) The earth was formed about 4.7  109 (4.7 billion) years ago. How old was the earth when 99.999999% of the 26Al originally present had decayed?




Write a balanced equation for each of the following nuclear reactions:
(a) bismuth-212 decays into polonium-212
(b) beryllium-8 and a positron are produced by the decay of an unstable nucleus
(c) neptunium-239 forms from the reaction of uranium-238 with a neutron and then spontaneously converts into plutonium-239
(d) strontium-90 decays into yttrium-90


(a) 
83212Bi

84212Po
+
-10e
;
83212Bi

84212Po
+
-10e
; (b) 
58B

48B
e+
-10e
;
58B

48B
e+
-10e
; (c) 
92238U
+
01n

93239Np
+
-10N
p,
92238U
+
01n

93239Np
+
-10N
p, 
93239Np

94239Pu
+
-10e
;
93239Np

94239Pu
+
-10e
; (d) 
3890Sr

3990Y
+
-10e

3890Sr

3990Y
+
-10e




Write a balanced equation for each of the following nuclear reactions:
(a) mercury-180 decays into platinum-176
(b) zirconium-90 and an electron are produced by the decay of an unstable nucleus
(c) thorium-232 decays and produces an alpha particle and a radium-228 nucleus, which decays into actinium-228 by beta decay
(d) neon-19 decays into fluorine-19



Footnotes1 The m in Tc-99m stands for metastable, indicating that this is an unstable, high-energy state of Tc-99. Metastable isotopes emit  radiation to rid themselves of excess energy and become (more) stable.Glossary

alpha () decay
loss of an alpha particle during radioactive decay

beta () decay
breakdown of a neutron into a proton, which remains in the nucleus, and an electron, which is emitted as a beta particle

daughter nuclide
nuclide produced by the radioactive decay of another nuclide; may be stable or may decay further

electron capture
combination of a core electron with a proton to yield a neutron within the nucleus

gamma () emission
decay of an excited-state nuclide accompanied by emission of a gamma ray

half-life (t1/2)
 time required for half of the atoms in a radioactive sample to decay

parent nuclide
unstable nuclide that changes spontaneously into another (daughter) nuclide

positron emission
(also, + decay) conversion of a proton into a neutron, which remains in the nucleus, and a positron, which is emitted

radioactive decay
spontaneous decay of an unstable nuclide into another nuclide

radioactive decay series
chains of successive disintegrations (radioactive decays) that ultimately lead to a stable end-product

radiocarbon dating
highly accurate means of dating objects 30,00050,000 years old that were derived from once-living matter; achieved by calculating the ratio of 
614C
:
612C

614C
:
612C
 in the object vs. the ratio of 
614C
:
612C

614C
:
612C
 in the present-day atmosphere

radiometric dating
use of radioisotopes and their properties to date the formation of objects such as archeological artifacts, formerly living organisms, or geological formations


