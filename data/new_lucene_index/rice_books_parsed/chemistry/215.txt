
Introduction
Introduction
        class="introduction"
      
Early Ideas in Atomic Theory
Evolution of Atomic Theory
Atomic Structure and Symbolism
Chemical Formulas
The Periodic Table
Molecular and Ionic Compounds
Chemical Nomenclature

class="key-equations" title="Key-Equations"
class="summary" title="Chapter Summary"
class="exercises" title="Exercises"
class="references" title="References"
Analysis of molecules in an exhaled breath can provide valuable information, leading to early diagnosis of diseases or detection of environmental exposure to harmful substances. (credit: modification of work by Paul Flowers)




Your overall health and susceptibility to disease depends upon the complex interaction between your genetic makeup and environmental exposure, with the outcome difficult to predict. Early detection of biomarkers, substances that indicate an organisms disease or physiological state, could allow diagnosis and treatment before a condition becomes serious or irreversible. Recent studies have shown that your exhaled breath can contain molecules that may be biomarkers for recent exposure to environmental contaminants or for pathological conditions ranging from asthma to lung cancer. Scientists are working to develop biomarker fingerprints that could be used to diagnose a specific disease based on the amounts and identities of certain molecules in a patients exhaled breath. An essential concept underlying this goal is that of a molecules identity, which is determined by the numbers and types of atoms it contains, and how they are bonded together. This chapter will describe some of the fundamental chemical principles related to the composition of matter, including those central to the concept of molecular identity.

