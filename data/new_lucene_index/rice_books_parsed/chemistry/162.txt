
Properties of Liquids
Properties of LiquidsBy the end of this section, you will be able to:

Distinguish between adhesive and cohesive forces
Define viscosity, surface tension, and capillary rise
Describe the roles of intermolecular attractive forces in each of these properties/phenomena

When you pour a glass of water, or fill a car with gasoline, you observe that water and gasoline flow freely. But when you pour syrup on pancakes or add oil to a car engine, you note that syrup and motor oil do not flow as readily. The viscosity of a liquid is a measure of its resistance to flow. Water, gasoline, and other liquids that flow freely have a low viscosity. Honey, syrup, motor oil, and other liquids that do not flow freely, like those shown in [link], have higher viscosities. We can measure viscosity by measuring the rate at which a metal ball falls through a liquid (the ball falls more slowly through a more viscous liquid) or by measuring the rate at which a liquid flows through a narrow tube (more viscous liquids flow more slowly).
(a) Honey and (b) motor oil are examples of liquids with high viscosities; they flow slowly. (credit a: modification of work by Scott Bauer; credit b: modification of work by David Nagy)




The IMFs between the molecules of a liquid, the size and shape of the molecules, and the temperature determine how easily a liquid flows. As [link] shows, the more structurally complex are the molecules in a liquid and the stronger the IMFs between them, the more difficult it is for them to move past each other and the greater is the viscosity of the liquid. As the temperature increases, the molecules move more rapidly and their kinetic energies are better able to overcome the forces that hold them together; thus, the viscosity of the liquid decreases.


Viscosities of Common Substances at 25 C


Substance
Formula
Viscosity (mPas)



water
H2O
0.890


mercury
Hg
1.526


ethanol
C2H5OH
1.074


octane
C8H18
0.508


ethylene glycol
CH2(OH)CH2(OH)
16.1


honey
variable
~2,00010,000


motor oil
variable
~50500


The various IMFs between identical molecules of a substance are examples of cohesive forces. The molecules within a liquid are surrounded by other molecules and are attracted equally in all directions by the cohesive forces within the liquid. However, the molecules on the surface of a liquid are attracted only by about one-half as many molecules. Because of the unbalanced molecular attractions on the surface molecules, liquids contract to form a shape that minimizes the number of molecules on the surfacethat is, the shape with the minimum surface area. A small drop of liquid tends to assume a spherical shape, as shown in [link], because in a sphere, the ratio of surface area to volume is at a minimum. Larger drops are more greatly affected by gravity, air resistance, surface interactions, and so on, and as a result, are less spherical.
Attractive forces result in a spherical water drop that minimizes surface area; cohesive forces hold the sphere together; adhesive forces keep the drop attached to the web. (credit photo: modification of work by OliBac/Flickr)




Surface tension is defined as the energy required to increase the surface area of a liquid, or the force required to increase the length of a liquid surface by a given amount. This property results from the cohesive forces between molecules at the surface of a liquid, and it causes the surface of a liquid to behave like a stretched rubber membrane. Surface tensions of several liquids are presented in [link]. Among common liquids, water exhibits a distinctly high surface tension due to strong hydrogen bonding between its molecules. As a result of this high surface tension, the surface of water represents a relatively tough skin that can withstand considerable force without breaking. A steel needle carefully placed on water will float. Some insects, like the one shown in [link], even though they are denser than water, move on its surface because they are supported by the surface tension.


Surface Tensions of Common Substances at 25 C


Substance
Formula
Surface Tension (mN/m)



water
H2O
71.99


mercury
Hg
458.48


ethanol
C2H5OH
21.97


octane
C8H18
21.14


ethylene glycol
CH2(OH)CH2(OH)
47.99


Surface tension (right) prevents this insect, a water strider, from sinking into the water.




The IMFs of attraction between two different molecules are called adhesive forces. Consider what happens when water comes into contact with some surface. If the adhesive forces between water molecules and the molecules of the surface are weak compared to the cohesive forces between the water molecules, the water does not wet the surface. For example, water does not wet waxed surfaces or many plastics such as polyethylene. Water forms drops on these surfaces because the cohesive forces within the drops are greater than the adhesive forces between the water and the plastic. Water spreads out on glass because the adhesive force between water and glass is greater than the cohesive forces within the water. When water is confined in a glass tube, its meniscus (surface) has a concave shape because the water wets the glass and creeps up the side of the tube. On the other hand, the cohesive forces between mercury atoms are much greater than the adhesive forces between mercury and glass. Mercury therefore does not wet glass, and it forms a convex meniscus when confined in a tube because the cohesive forces within the mercury tend to draw it into a drop ([link]).
Differences in the relative strengths of cohesive and adhesive forces result in different meniscus shapes for mercury (left) and water (right) in glass tubes. (credit: Mark Ott)




If you place one end of a paper towel in spilled wine, as shown in [link], the liquid wicks up the paper towel. A similar process occurs in a cloth towel when you use it to dry off after a shower. These are examples of capillary actionwhen a liquid flows within a porous material due to the attraction of the liquid molecules to the surface of the material and to other liquid molecules. The adhesive forces between the liquid and the porous material, combined with the cohesive forces within the liquid, may be strong enough to move the liquid upward against gravity.
Wine wicks up a paper towel (left) because of the strong attractions of water (and ethanol) molecules to the OH groups on the towels cellulose fibers and the strong attractions of water molecules to other water (and ethanol) molecules (right). (credit photo: modification of work by Mark Blaser)




Towels soak up liquids like water because the fibers of a towel are made of molecules that are attracted to water molecules. Most cloth towels are made of cotton, and paper towels are generally made from paper pulp. Both consist of long molecules of cellulose that contain many OH groups. Water molecules are attracted to these OH groups and form hydrogen bonds with them, which draws the H2O molecules up the cellulose molecules. The water molecules are also attracted to each other, so large amounts of water are drawn up the cellulose fibers.
Capillary action can also occur when one end of a small diameter tube is immersed in a liquid, as illustrated in [link]. If the liquid molecules are strongly attracted to the tube molecules, the liquid creeps up the inside of the tube until the weight of the liquid and the adhesive forces are in balance. The smaller the diameter of the tube is, the higher the liquid climbs. It is partly by capillary action occurring in plant cells called xylem that water and dissolved nutrients are brought from the soil up through the roots and into a plant. Capillary action is the basis for thin layer chromatography, a laboratory technique commonly used to separate small quantities of mixtures. You depend on a constant supply of tears to keep your eyes lubricated and on capillary action to pump tear fluid away.
Depending upon the relative strengths of adhesive and cohesive forces, a liquid may rise (such as water) or fall (such as mercury) in a glass capillary tube. The extent of the rise (or fall) is directly proportional to the surface tension of the liquid and inversely proportional to the density of the liquid and the radius of the tube.




The height to which a liquid will rise in a capillary tube is determined by several factors as shown in the following equation:
h=2Tcosrgh=2Tcosrg
In this equation, h is the height of the liquid inside the capillary tube relative to the surface of the liquid outside the tube, T is the surface tension of the liquid,  is the contact angle between the liquid and the tube, r is the radius of the tube,  is the density of the liquid, and g is the acceleration due to gravity, 9.8 m/s2. When the tube is made of a material to which the liquid molecules are strongly attracted, they will spread out completely on the surface, which corresponds to a contact angle of 0. This is the situation for water rising in a glass tube.

Capillary Rise
At 25 C, how high will water rise in a glass capillary tube with an inner diameter of 0.25 mm?
For water, T = 71.99 mN/m and  = 1.0 g/cm3.
Solution
The liquid will rise to a height h given by: h=2Tcosrgh=2Tcosrg
The Newton is defined as a kg m/s2, and so the provided surface tension is equivalent to 0.07199 kg/s2. The provided density must be converted into units that will cancel appropriately:  = 1000 kg/m3. The diameter of the tube in meters is 0.00025 m, so the radius is 0.000125 m. For a glass tube immersed in water, the contact angle is  = 0, so cos  = 1. Finally, acceleration due to gravity on the earth is g = 9.8 m/s2. Substituting these values into the equation, and cancelling units, we have:
h=2(0.07199kg/s2)(0.000125m)(1000kg/m3)(9.8m/s2)=0.12m=12 cmh=2(0.07199kg/s2)(0.000125m)(1000kg/m3)(9.8m/s2)=0.12m=12 cm
Check Your Learning
Water rises in a glass capillary tube to a height of 8.4 cm. What is the diameter of the capillary tube?
Answer:
diameter = 0.36 mm


Biomedical Applications of Capillary Action
Many medical tests require drawing a small amount of blood, for example to determine the amount of glucose in someone with diabetes or the hematocrit level in an athlete. This procedure can be easily done because of capillary action, the ability of a liquid to flow up a small tube against gravity, as shown in [link]. When your finger is pricked, a drop of blood forms and holds together due to surface tensionthe unbalanced intermolecular attractions at the surface of the drop. Then, when the open end of a narrow-diameter glass tube touches the drop of blood, the adhesive forces between the molecules in the blood and those at the glass surface draw the blood up the tube. How far the blood goes up the tube depends on the diameter of the tube (and the type of fluid). A small tube has a relatively large surface area for a given volume of blood, which results in larger (relative) attractive forces, allowing the blood to be drawn farther up the tube. The liquid itself is held together by its own cohesive forces. When the weight of the liquid in the tube generates a downward force equal to the upward force associated with capillary action, the liquid stops rising.
Blood is collected for medical analysis by capillary action, which draws blood into a small diameter glass tube. (credit: modification of work by Centers for Disease Control and Prevention)





Key Concepts and Summary
The intermolecular forces between molecules in the liquid state vary depending upon their chemical identities and result in corresponding variations in various physical properties. Cohesive forces between like molecules are responsible for a liquids viscosity (resistance to flow) and surface tension (elasticity of a liquid surface). Adhesive forces between the molecules of a liquid and different molecules composing a surface in contact with the liquid are responsible for phenomena such as surface wetting and capillary rise.

Key Equations

h=2Tcosrgh=2Tcosrg


Chemistry End of Chapter Exercises


The test tubes shown here contain equal amounts of the specified motor oils. Identical metal spheres were dropped at the same time into each of the tubes, and a brief moment later, the spheres had fallen to the heights indicated in the illustration.
Rank the motor oils in order of increasing viscosity, and explain your reasoning:







Although steel is denser than water, a steel needle or paper clip placed carefully lengthwise on the surface of still water can be made to float. Explain at a molecular level how this is possible:
(credit: Cory Zanker)






The water molecules have strong intermolecular forces of hydrogen bonding. The water molecules are thus attracted strongly to one another and exhibit a relatively large surface tension, forming a type of skin at its surface. This skin can support a bug or paper clip if gently placed on the water.




The surface tension and viscosity values for diethyl ether, acetone, ethanol, and ethylene glycol are shown here.



(a) Explain their differences in viscosity in terms of the size and shape of their molecules and their IMFs.
(b) Explain their differences in surface tension in terms of the size and shape of their molecules and their IMFs:




You may have heard someone use the figure of speech slower than molasses in winter to describe a process that occurs slowly. Explain why this is an apt idiom, using concepts of molecular size and shape, molecular interactions, and the effect of changing temperature.


Temperature has an effect on intermolecular forces: the higher the temperature, the greater the kinetic energies of the molecules and the greater the extent to which their intermolecular forces are overcome, and so the more fluid (less viscous) the liquid; the lower the temperature, the lesser the intermolecular forces are overcome, and so the less viscous the liquid.




It is often recommended that you let your car engine run idle to warm up before driving, especially on cold winter days. While the benefit of prolonged idling is dubious, it is certainly true that a warm engine is more fuel efficient than a cold one. Explain the reason for this.




The surface tension and viscosity of water at several different temperatures are given in this table.


Water
Surface Tension (mN/m)
Viscosity (mPa s)



0 C
75.6
1.79


20 C
72.8
1.00


60 C
66.2
0.47


100 C
58.9
0.28


(a) As temperature increases, what happens to the surface tension of water? Explain why this occurs, in terms of molecular interactions and the effect of changing temperature.
(b) As temperature increases, what happens to the viscosity of water? Explain why this occurs, in terms of molecular interactions and the effect of changing temperature.


(a) As the water reaches higher temperatures, the increased kinetic energies of its molecules are more effective in overcoming hydrogen bonding, and so its surface tension decreases. Surface tension and intermolecular forces are directly related. (b) The same trend in viscosity is seen as in surface tension, and for the same reason.




At 25 C, how high will water rise in a glass capillary tube with an inner diameter of 0.63 mm? Refer to [link] for the required information.




Water rises in a glass capillary tube to a height of 17 cm. What is the diameter of the capillary tube?


9.5  105 m



Glossary

adhesive force
force of attraction between molecules of different chemical identities


capillary action
flow of liquid within a porous material due to the attraction of the liquid molecules to the surface of the material and to other liquid molecules


cohesive force
force of attraction between identical molecules


surface tension
energy required to increase the area, or length, of a liquid surface by a given amount


viscosity
measure of a liquids resistance to flow


