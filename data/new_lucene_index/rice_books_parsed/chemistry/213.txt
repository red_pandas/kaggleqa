
Occurrence, Preparation, and Compounds of Hydrogen
Occurrence, Preparation, and Compounds of HydrogenBy the end of this section, you will be able to:

Describe the properties, preparation, and compounds of hydrogen

Hydrogen is the most abundant element in the universe. The sun and other stars are composed largely of hydrogen. Astronomers estimate that 90% of the atoms in the universe are hydrogen atoms. Hydrogen is a component of more compounds than any other element. Water is the most abundant compound of hydrogen found on earth. Hydrogen is an important part of petroleum, many minerals, cellulose and starch, sugar, fats, oils, alcohols, acids, and thousands of other substances.
At ordinary temperatures, hydrogen is a colorless, odorless, tasteless, and nonpoisonous gas consisting of the diatomic molecule H2. Hydrogen is composed of three isotopes, and unlike other elements, these isotopes have different names and chemical symbols: protium, 1H, deuterium, 2H (or D), and tritium 3H (or T). In a naturally occurring sample of hydrogen, there is one atom of deuterium for every 7000 H atoms and one atom of radioactive tritium for every 1018 H atoms. The chemical properties of the different isotopes are very similar because they have identical electron structures, but they differ in some physical properties because of their differing atomic masses. Elemental deuterium and tritium have lower vapor pressure than ordinary hydrogen. Consequently, when liquid hydrogen evaporates, the heavier isotopes are concentrated in the last portions to evaporate. Electrolysis of heavy water, D2O, yields deuterium. Most tritium originates from nuclear reactions.
Preparation of Hydrogen
Elemental hydrogen must be prepared from compounds by breaking chemical bonds. The most common methods of preparing hydrogen follow.
From Steam and Carbon or Hydrocarbons
Water is the cheapest and most abundant source of hydrogen. Passing steam over coke (an impure form of elemental carbon) at 1000 C produces a mixture of carbon monoxide and hydrogen known as water gas:
C(s)+H2O(g)1000CCO(g)+H2(g)water gasC(s)+H2O(g)1000CCO(g)+H2(g)water gas
Water gas is as an industrial fuel. It is possible to produce additional hydrogen by mixing the water gas with steam in the presence of a catalyst to convert the CO to CO2. This reaction is the water gas shift reaction.
It is also possible to prepare a mixture of hydrogen and carbon monoxide by passing hydrocarbons from natural gas or petroleum and steam over a nickel-based catalyst. Propane is an example of a hydrocarbon reactant:
C3H8(g)+3H2O(g)catalyst900C3CO(g)+7H2(g)C3H8(g)+3H2O(g)catalyst900C3CO(g)+7H2(g)

Electrolysis
Hydrogen forms when direct current electricity passes through water containing an electrolyte such as H2SO4, as illustrated in [link]. Bubbles of hydrogen form at the cathode, and oxygen evolves at the anode. The net reaction is:
2H2O(l)+electrical energy2H2(g)+O2(g)2H2O(l)+electrical energy2H2(g)+O2(g)
The electrolysis of water produces hydrogen and oxygen. Because there are twice as many hydrogen atoms as oxygen atoms and both elements are diatomic, there is twice the volume of hydrogen produced at the cathode as there is oxygen produced at the anode.





Reaction of Metals with Acids
This is the most convenient laboratory method of producing hydrogen. Metals with lower reduction potentials reduce the hydrogen ion in dilute acids to produce hydrogen gas and metal salts. For example, as shown in [link], iron in dilute hydrochloric acid produces hydrogen gas and iron(II) chloride:
Fe(s)+2H3O+(aq)+2Cl(aq)Fe2+(aq)+2Cl(aq)+H2(g)+2H2O(l)Fe(s)+2H3O+(aq)+2Cl(aq)Fe2+(aq)+2Cl(aq)+H2(g)+2H2O(l)
The reaction of iron with an acid produces hydrogen. Here, iron reacts with hydrochloric acid. (credit: Mark Ott)





Reaction of Ionic Metal Hydrides with Water
It is possible to produce hydrogen from the reaction of hydrides of the active metals, which contain the very strongly basic H anion, with water:
CaH2(s)+2H2O(l)Ca2+(aq)+2OH(aq)+2H2(g)CaH2(s)+2H2O(l)Ca2+(aq)+2OH(aq)+2H2(g)
Metal hydrides are expensive but convenient sources of hydrogen, especially where space and weight are important factors. They are important in the inflation of life jackets, life rafts, and military balloons.


Reactions
Under normal conditions, hydrogen is relatively inactive chemically, but when heated, it enters into many chemical reactions.
Two thirds of the worlds hydrogen production is devoted to the manufacture of ammonia, which is a fertilizer and used in the manufacture of nitric acid. Large quantities of hydrogen are also important in the process of hydrogenation, discussed in the chapter on organic chemistry.
It is possible to use hydrogen as a nonpolluting fuel. The reaction of hydrogen with oxygen is a very exothermic reaction, releasing 286 kJ of energy per mole of water formed. Hydrogen burns without explosion under controlled conditions. The oxygen-hydrogen torch, because of the high heat of combustion of hydrogen, can achieve temperatures up to 2800 C. The hot flame of this torch is useful in cutting thick sheets of many metals. Liquid hydrogen is also an important rocket fuel ([link]).
Before the fleets retirement in 2011, liquid hydrogen and liquid oxygen were used in the three main engines of a space shuttle. Two compartments in the large tank held these liquids until the shuttle was launched. (credit: reynermedia/Flickr)




An uncombined hydrogen atom consists of a nucleus and one valence electron in the 1s orbital. The n = 1 valence shell has a capacity for two electrons, and hydrogen can rightfully occupy two locations in the periodic table. It is possible to consider hydrogen a group 1 element because hydrogen can lose an electron to form the cation, H+. It is also possible to consider hydrogen to be a group 17 element because it needs only one electron to fill its valence orbital to form a hydride ion, H, or it can share an electron to form a single, covalent bond. In reality, hydrogen is a unique element that almost deserves its own location in the periodic table.
Reactions with Elements
When heated, hydrogen reacts with the metals of group 1 and with Ca, Sr, and Ba (the more active metals in group 2). The compounds formed are crystalline, ionic hydrides that contain the hydride anion, H, a strong reducing agent and a strong base, which reacts vigorously with water and other acids to form hydrogen gas.
The reactions of hydrogen with nonmetals generally produce acidic hydrogen compounds with hydrogen in the 1+ oxidation state. The reactions become more exothermic and vigorous as the electronegativity of the nonmetal increases. Hydrogen reacts with nitrogen and sulfur only when heated, but it reacts explosively with fluorine (forming HF) and, under some conditions, with chlorine (forming HCl). A mixture of hydrogen and oxygen explodes if ignited. Because of the explosive nature of the reaction, it is necessary to exercise caution when handling hydrogen (or any other combustible gas) to avoid the formation of an explosive mixture in a confined space. Although most hydrides of the nonmetals are acidic, ammonia and phosphine (PH3) are very, very weak acids and generally function as bases. There is a summary of these reactions of hydrogen with the elements in [link].


Chemical Reactions of Hydrogen with Other Elements


General Equation
Comments



MH or MH2MOH or M(OH)2+H2MH or MH2MOH or M(OH)2+H2
ionic hydrides with group 1 and Ca, Sr, and Ba


H2+C(no reaction)H2+C(no reaction)



3H2+N22NH33H2+N22NH3
requires high pressure and temperature; low yield


2H2+O22H2O2H2+O22H2O
exothermic and potentially explosive


H2+SH2SH2+SH2S
requires heating; low yield


H2+X22HXH2+X22HX
X = F, Cl, Br, and I; explosive with F2; low yield with I2



Reaction with Compounds
Hydrogen reduces the heated oxides of many metals, with the formation of the metal and water vapor. For example, passing hydrogen over heated CuO forms copper and water.
Hydrogen may also reduce the metal ions in some metal oxides to lower oxidation states:
H2(g)+MnO2(s)MnO(s)+H2O(g)H2(g)+MnO2(s)MnO(s)+H2O(g)


Hydrogen Compounds
Other than the noble gases, each of the nonmetals forms compounds with hydrogen. For brevity, we will discuss only a few hydrogen compounds of the nonmetals here.
Nitrogen Hydrogen Compounds
Ammonia, NH3, forms naturally when any nitrogen-containing organic material decomposes in the absence of air. The laboratory preparation of ammonia is by the reaction of an ammonium salt with a strong base such as sodium hydroxide. The acid-base reaction with the weakly acidic ammonium ion gives ammonia, illustrated in [link]. Ammonia also forms when ionic nitrides react with water. The nitride ion is a much stronger base than the hydroxide ion:
Mg3N2(s)+6H2O(l)3Mg(OH)2(s)+2NH3(g)Mg3N2(s)+6H2O(l)3Mg(OH)2(s)+2NH3(g)
The commercial production of ammonia is by the direct combination of the elements in the Haber process:
N2(g)+3H2(g)catalyst2NH3(g)H=92 kJN2(g)+3H2(g)catalyst2NH3(g)H=92 kJ
The structure of ammonia is shown with a central nitrogen atom and three hydrogen atoms.




Ammonia is a colorless gas with a sharp, pungent odor. Smelling salts utilize this powerful odor. Gaseous ammonia readily liquefies to give a colorless liquid that boils at 33 C. Due to intermolecular hydrogen bonding, the enthalpy of vaporization of liquid ammonia is higher than that of any other liquid except water, so ammonia is useful as a refrigerant. Ammonia is quite soluble in water (658 L at STP dissolves in 1 L H2O).
The chemical properties of ammonia are as follows:

Ammonia acts as a Brnsted base, as discussed in the chapter on acid-base chemistry. The ammonium ion is similar in size to the potassium ion; compounds of the two ions exhibit many similarities in their structures and solubilities.
Ammonia can display acidic behavior, although it is a much weaker acid than water. Like other acids, ammonia reacts with metals, although it is so weak that high temperatures are necessary. Hydrogen and (depending on the stoichiometry) amides (salts of NH2),NH2), imides (salts of NH2), or nitrides (salts of N3) form.
The nitrogen atom in ammonia has its lowest possible oxidation state (3) and thus is not susceptible to reduction. However, it can be oxidized. Ammonia burns in air, giving NO and water. Hot ammonia and the ammonium ion are active reducing agents. Of particular interest are the oxidations of ammonium ion by nitrite ion, NO2,NO2, to yield pure nitrogen and by nitrate ion to yield nitrous oxide, N2O.
There are a number of compounds that we can consider derivatives of ammonia through the replacement of one or more hydrogen atoms with some other atom or group of atoms. Inorganic derivations include chloramine, NH2Cl, and hydrazine, N2H4:




Chloramine, NH2Cl, results from the reaction of sodium hypochlorite, NaOCl, with ammonia in basic solution. In the presence of a large excess of ammonia at low temperature, the chloramine reacts further to produce hydrazine, N2H4:
NH3(aq)+OCl(aq)NH2Cl(aq)+OH(aq)NH3(aq)+OCl(aq)NH2Cl(aq)+OH(aq)
NH2Cl(aq)+NH3(aq)+OH(aq)N2H4(aq)+Cl(aq)+H2O(l)NH2Cl(aq)+NH3(aq)+OH(aq)N2H4(aq)+Cl(aq)+H2O(l)
Anhydrous hydrazine is relatively stable in spite of its positive free energy of formation:
N2(g)+2H2(g)N2H4(l)Gf=149.2kJmol1N2(g)+2H2(g)N2H4(l)Gf=149.2kJmol1
Hydrazine is a fuming, colorless liquid that has some physical properties remarkably similar to those of H2O (it melts at 2 C, boils at 113.5 C, and has a density at 25 C of 1.00 g/mL). It burns rapidly and completely in air with substantial evolution of heat:
N2H4(l)+O2(g)N2(g)+2H2O(l)H=621.5kJmol1N2H4(l)+O2(g)N2(g)+2H2O(l)H=621.5kJmol1
Like ammonia, hydrazine is both a Brnsted base and a Lewis base, although it is weaker than ammonia. It reacts with strong acids and forms two series of salts that contain the N2H5+N2H5+ and N2H62+N2H62+ ions, respectively. Some rockets use hydrazine as a fuel.

Phosphorus Hydrogen Compounds
The most important hydride of phosphorus is phosphine, PH3, a gaseous analog of ammonia in terms of both formula and structure. Unlike ammonia, it is not possible to form phosphine by direct union of the elements. There are two methods for the preparation of phosphine. One method is by the action of an acid on an ionic phosphide. The other method is the disproportionation of white phosphorus with hot concentrated base to produce phosphine and the hydrogen phosphite ion:
AlP(s)+3H3O+(aq)PH3(g)+Al3+(aq)+3H2O(l)AlP(s)+3H3O+(aq)PH3(g)+Al3+(aq)+3H2O(l)
P4(s)+4OH(aq)+2H2O(l)2HPO32(aq)+2PH3(g)P4(s)+4OH(aq)+2H2O(l)2HPO32(aq)+2PH3(g)
Phosphine is a colorless, very poisonous gas, which has an odor like that of decaying fish. Heat easily decomposes phosphine (4PH3P4+6H2),(4PH3P4+6H2), and the compound burns in air. The major uses of phosphine are as a fumigant for grains and in semiconductor processing. Like ammonia, gaseous phosphine unites with gaseous hydrogen halides, forming phosphonium compounds like PH4Cl and PH4I. Phosphine is a much weaker base than ammonia; therefore, these compounds decompose in water, and the insoluble PH3 escapes from solution.

Sulfur Hydrogen Compounds
Hydrogen sulfide, H2S, is a colorless gas that is responsible for the offensive odor of rotten eggs and of many hot springs. Hydrogen sulfide is as toxic as hydrogen cyanide; therefore, it is necessary to exercise great care in handling it. Hydrogen sulfide is particularly deceptive because it paralyzes the olfactory nerves; after a short exposure, one does not smell it.
The production of hydrogen sulfide by the direct reaction of the elements (H2 + S) is unsatisfactory because the yield is low. A more effective preparation method is the reaction of a metal sulfide with a dilute acid. For example:
FeS(s)+2H3O+(aq)Fe2+(aq)+H2S(g)+2H2O(l)FeS(s)+2H3O+(aq)Fe2+(aq)+H2S(g)+2H2O(l)
It is easy to oxidize the sulfur in metal sulfides and in hydrogen sulfide, making metal sulfides and H2S good reducing agents. In acidic solutions, hydrogen sulfide reduces Fe3+ to Fe2+, MnO4MnO4 to Mn2+, Cr2O72Cr2O72 to Cr3+, and HNO3 to NO2. The sulfur in H2S usually oxidizes to elemental sulfur, unless a large excess of the oxidizing agent is present. In which case, the sulfide may oxidize to SO32SO32 or SO42SO42 (or to SO2 or SO3 in the absence of water):
2H2S(g)+O2(g)2S(s)+2H2O(l)2H2S(g)+O2(g)2S(s)+2H2O(l)
This oxidation process leads to the removal of the hydrogen sulfide found in many sources of natural gas. The deposits of sulfur in volcanic regions may be the result of the oxidation of H2S present in volcanic gases.
Hydrogen sulfide is a weak diprotic acid that dissolves in water to form hydrosulfuric acid. The acid ionizes in two stages, yielding hydrogen sulfide ions, HS, in the first stage and sulfide ions, S2, in the second. Since hydrogen sulfide is a weak acid, aqueous solutions of soluble sulfides and hydrogen sulfides are basic:
S2(aq)+H2O(l)HS(aq)+OH(aq)S2(aq)+H2O(l)HS(aq)+OH(aq)
HS(aq)+H2O(l)H2S(g)+OH(aq)HS(aq)+H2O(l)H2S(g)+OH(aq)

Halogen Hydrogen Compounds
Binary compounds containing only hydrogen and a halogen are hydrogen halides. At room temperature, the pure hydrogen halides HF, HCl, HBr, and HI are gases.
In general, it is possible to prepare the halides by the general techniques used to prepare other acids. Fluorine, chlorine, and bromine react directly with hydrogen to form the respective hydrogen halide. This is a commercially important reaction for preparing hydrogen chloride and hydrogen bromide.
The acid-base reaction between a nonvolatile strong acid and a metal halide will yield a hydrogen halide. The escape of the gaseous hydrogen halide drives the reaction to completion. For example, the usual method of preparing hydrogen fluoride is by heating a mixture of calcium fluoride, CaF2, and concentrated sulfuric acid:
CaF2(s)+H2SO4(aq)CaSO4(s)+2HF(g)CaF2(s)+H2SO4(aq)CaSO4(s)+2HF(g)
Gaseous hydrogen fluoride is also a by-product in the preparation of phosphate fertilizers by the reaction of fluoroapatite, Ca5(PO4)3F, with sulfuric acid. The reaction of concentrated sulfuric acid with a chloride salt produces hydrogen chloride both commercially and in the laboratory.
In most cases, sodium chloride is the chloride of choice because it is the least expensive chloride. Hydrogen bromide and hydrogen iodide cannot be prepared using sulfuric acid because this acid is an oxidizing agent capable of oxidizing both bromide and iodide. However, it is possible to prepare both hydrogen bromide and hydrogen iodide using an acid such as phosphoric acid because it is a weaker oxidizing agent. For example:
H3PO4(l)+Br(aq)HBr(g)+H2PO4(aq)H3PO4(l)+Br(aq)HBr(g)+H2PO4(aq)
All of the hydrogen halides are very soluble in water, forming hydrohalic acids. With the exception of hydrogen fluoride, which has a strong hydrogen-fluoride bond, they are strong acids. Reactions of hydrohalic acids with metals, metal hydroxides, oxides, or carbonates produce salts of the halides. Most chloride salts are soluble in water. AgCl, PbCl2, and Hg2Cl2 are the commonly encountered exceptions.
The halide ions give the substances the properties associated with X(aq). The heavier halide ions (Cl, Br, and I) can act as reducing agents, and the lighter halogens or other oxidizing agents will oxidize them:
Cl2(aq)+2e2Cl(aq)E=1.36VCl2(aq)+2e2Cl(aq)E=1.36V
Br2(aq)+2e2Br(aq)E=1.09VBr2(aq)+2e2Br(aq)E=1.09V
I2(aq)+2e2I(aq)E=0.54VI2(aq)+2e2I(aq)E=0.54V
For example, bromine oxidizes iodine:
Br2(aq)+2HI(aq)2HBr(aq)+I2(aq)E=0.55VBr2(aq)+2HI(aq)2HBr(aq)+I2(aq)E=0.55V
Hydrofluoric acid is unique in its reactions with sand (silicon dioxide) and with glass, which is a mixture of silicates:
SiO2(s)+4HF(aq)SiF4(g)+2H2O(l)SiO2(s)+4HF(aq)SiF4(g)+2H2O(l)
CaSiO3(s)+6HF(aq)CaF2(s)+SiF4(g)+3H2O(l)CaSiO3(s)+6HF(aq)CaF2(s)+SiF4(g)+3H2O(l)
The volatile silicon tetrafluoride escapes from these reactions. Because hydrogen fluoride attacks glass, it can frost or etch glass and is used to etch markings on thermometers, burets, and other glassware.
The largest use for hydrogen fluoride is in production of hydrochlorofluorocarbons for refrigerants, in plastics, and in propellants. The second largest use is in the manufacture of cryolite, Na3AlF6, which is important in the production of aluminum. The acid is also important in the production of other inorganic fluorides (such as BF3), which serve as catalysts in the industrial synthesis of certain organic compounds.
Hydrochloric acid is relatively inexpensive. It is an important and versatile acid in industry and is important for the manufacture of metal chlorides, dyes, glue, glucose, and various other chemicals. A considerable amount is also important for the activation of oil wells and as pickle liquoran acid used to remove oxide coating from iron or steel that is to be galvanized, tinned, or enameled. The amounts of hydrobromic acid and hydroiodic acid used commercially are insignificant by comparison.


Key Concepts and Summary
Hydrogen is the most abundant element in the universe and its chemistry is truly unique. Although it has some chemical reactivity that is similar to that of the alkali metals, hydrogen has many of the same chemical properties of a nonmetal with a relatively low electronegativity. It forms ionic hydrides with active metals, covalent compounds in which it has an oxidation state of 1 with less electronegative elements, and covalent compounds in which it has an oxidation state of 1+ with more electronegative nonmetals. It reacts explosively with oxygen, fluorine, and chlorine, less readily with bromine, and much less readily with iodine, sulfur, and nitrogen. Hydrogen reduces the oxides of metals with lower reduction potentials than chromium to form the metal and water. The hydrogen halides are all acidic when dissolved in water.

Chemistry End of Chapter Exercises


Why does hydrogen not exhibit an oxidation state of 1 when bonded to nonmetals?


The electronegativity of the nonmetals is greater than that of hydrogen. Thus, the negative charge is better represented on the nonmetal, which has the greater tendency to attract electrons in the bond to itself.




The reaction of calcium hydride, CaH2, with water can be characterized as a Lewis acid-base reaction:
CaH2(s)+2H2O(l)Ca(OH)2(aq)+2H2(g)CaH2(s)+2H2O(l)Ca(OH)2(aq)+2H2(g)
Identify the Lewis acid and the Lewis base among the reactants. The reaction is also an oxidation-reduction reaction. Identify the oxidizing agent, the reducing agent, and the changes in oxidation number that occur in the reaction.




In drawing Lewis structures, we learn that a hydrogen atom forms only one bond in a covalent compound. Why?


Hydrogen has only one orbital with which to bond to other atoms. Consequently, only one two-electron bond can form.




What mass of CaH2 is necessary to react with water to provide enough hydrogen gas to fill a balloon at 20 C and 0.8 atm pressure with a volume of 4.5 L? The balanced equation is:
CaH2(s)+2H2O(l)Ca(OH)2(aq)+2H2(g)CaH2(s)+2H2O(l)Ca(OH)2(aq)+2H2(g)




What mass of hydrogen gas results from the reaction of 8.5 g of KH with water?
KH+H2OKOH+H2KH+H2OKOH+H2


0.43 g H2



Glossary

Haber process
main industrial process used to produce ammonia from nitrogen and hydrogen; involves the use of an iron catalyst and elevated temperatures and pressures


hydrogen halide
binary compound formed between hydrogen and the halogens: HF, HCl, HBr, and HI


hydrogenation
addition of hydrogen (H2) to reduce a compound


