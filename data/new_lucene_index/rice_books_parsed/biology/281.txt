
Bryophytes
BryophytesBy the end of this section, you will be able to:

Identify the main characteristics of bryophytes
Describe the distinguishing traits of liverworts, hornworts, and mosses
Chart the development of land adaptations in the bryophytes
Describe the events in the bryophyte lifecycle

Bryophytes are the group of plants that are the closest extant relative of early terrestrial plants. The first bryophytes (liverworts) most likely appeared in the Ordovician period, about 450 million years ago. Because of the lack of lignin and other resistant structures, the likelihood of bryophytes forming fossils is rather small. Some spores protected by sporopollenin have survived and are attributed to early bryophytes. By the Silurian period, however, vascular plants had spread through the continents. This compelling fact is used as evidence that non-vascular plants must have preceded the Silurian period.
More than 25,000 species of bryophytes thrive in mostly damp habitats, although some live in deserts. They constitute the major flora of inhospitable environments like the tundra, where their small size and tolerance to desiccation offer distinct advantages. They generally lack lignin and do not have actual tracheids (xylem cells specialized for water conduction). Rather, water and nutrients circulate inside specialized conducting cells. Although the term non-tracheophyte is more accurate, bryophytes are commonly called nonvascular plants.
In a bryophyte, all the conspicuous vegetative organsincluding the photosynthetic leaf-like structures, the thallus, stem, and the rhizoid that anchors the plant to its substratebelong to the haploid organism or gametophyte. The sporophyte is barely noticeable. The gametes formed by bryophytes swim with a flagellum, as do gametes in a few of the tracheophytes. The sporangiumthe multicellular sexual reproductive structureis present in bryophytes and absent in the majority of algae. The bryophyte embryo also remains attached to the parent plant, which protects and nourishes it. This is a characteristic of land plants.
The bryophytes are divided into three phyla: the liverworts or Hepaticophyta, the hornworts or Anthocerotophyta, and the mosses or true Bryophyta.
Liverworts
Liverworts (Hepaticophyta) are viewed as the plants most closely related to the ancestor that moved to land. Liverworts have colonized every terrestrial habitat on Earth and diversified to more than 7000 existing species ([link]). Some gametophytes form lobate green structures, as seen in [link]. The shape is similar to the lobes of the liver, and hence provides the origin of the name given to the phylum.This 1904 drawing shows the variety of forms of Hepaticophyta.


A liverwort, Lunularia cruciata, displays its lobate, flat thallus. The organism in the photograph is in the gametophyte stage.


Openings that allow the movement of gases may be observed in liverworts. However, these are not stomata, because they do not actively open and close. The plant takes up water over its entire surface and has no cuticle to prevent desiccation. [link] represents the lifecycle of a liverwort. The cycle starts with the release of haploid spores from the sporangium that developed on the sporophyte. Spores disseminated by wind or water germinate into flattened thalli attached to the substrate by thin, single-celled filaments. Male and female gametangia develop on separate, individual plants. Once released, male gametes swim with the aid of their flagella to the female gametangium (the archegonium), and fertilization ensues. The zygote grows into a small sporophyte still attached to the parent gametophyte. It will give rise, by meiosis, to the next generation of spores. Liverwort plants can also reproduce asexually, by the breaking of branches or the spreading of leaf fragments called gemmae. In this latter type of reproduction, the gemmaesmall, intact, complete pieces of plant that are produced in a cup on the surface of the thallus (shown in [link])are splashed out of the cup by raindrops. The gemmae then land nearby and develop into gametophytes.
The life cycle of a typical liverwort is shown. (credit: modification of work by Mariana Ruiz Villareal)



Hornworts
The hornworts (Anthocerotophyta) belong to the broad bryophyte group. They have colonized a variety of habitats on land, although they are never far from a source of moisture. The short, blue-green gametophyte is the dominant phase of the lifecycle of a hornwort. The narrow, pipe-like sporophyte is the defining characteristic of the group. The sporophytes emerge from the parent gametophyte and continue to grow throughout the life of the plant ([link]).
Hornworts grow a tall and slender sporophyte. (credit: modification of work by Jason Hollinger)


Stomata appear in the hornworts and are abundant on the sporophyte. Photosynthetic cells in the thallus contain a single chloroplast. Meristem cells at the base of the plant keep dividing and adding to its height. Many hornworts establish symbiotic relationships with cyanobacteria that fix nitrogen from the environment.
The lifecycle of hornworts ([link]) follows the general pattern of alternation of generations. The gametophytes grow as flat thalli on the soil with embedded gametangia. Flagellated sperm swim to the archegonia and fertilize eggs. The zygote develops into a long and slender sporophyte that eventually splits open, releasing spores. Thin cells called pseudoelaters surround the spores and help propel them further in the environment. Unlike the elaters observed in horsetails, the hornwort pseudoelaters are single-celled structures. The haploid spores germinate and give rise to the next generation of gametophyte.
The alternation of generation in hornworts is shown. (credit: modification of work by Smith609/Wikimedia Commons based on original work by Mariana Ruiz Villareal)



Mosses
More than 10,000 species of mosses have been catalogued. Their habitats vary from the tundra, where they are the main vegetation, to the understory of tropical forests. In the tundra, the mosses shallow rhizoids allow them to fasten to a substrate without penetrating the frozen soil. Mosses slow down erosion, store moisture and soil nutrients, and provide shelter for small animals as well as food for larger herbivores, such as the musk ox. Mosses are very sensitive to air pollution and are used to monitor air quality. They are also sensitive to copper salts, so these salts are a common ingredient of compounds marketed to eliminate mosses from lawns.
Mosses form diminutive gametophytes, which are the dominant phase of the lifecycle. Green, flat structuresresembling true leaves, but lacking vascular tissueare attached in a spiral to a central stalk. The plants absorb water and nutrients directly through these leaf-like structures. Some mosses have small branches. Some primitive traits of green algae, such as flagellated sperm, are still present in mosses that are dependent on water for reproduction. Other features of mosses are clearly adaptations to dry land. For example, stomata are present on the stems of the sporophyte, and a primitive vascular system runs up the sporophytes stalk. Additionally, mosses are anchored to the substratewhether it is soil, rock, or roof tilesby multicellular rhizoids. These structures are precursors of roots. They originate from the base of the gametophyte, but are not the major route for the absorption of water and minerals. The lack of a true root system explains why it is so easy to rip moss mats from a tree trunk. The moss lifecycle follows the pattern of alternation of generations as shown in [link]. The most familiar structure is the haploid gametophyte, which germinates from a haploid spore and forms first a protonemausually, a tangle of single-celled filaments that hug the ground. Cells akin to an apical meristem actively divide and give rise to a gametophore, consisting of a photosynthetic stem and foliage-like structures. Rhizoids form at the base of the gametophore. Gametangia of both sexes develop on separate gametophores. The male organ (the antheridium) produces many sperm, whereas the archegonium (the female organ) forms a single egg. At fertilization, the sperm swims down the neck to the venter and unites with the egg inside the archegonium. The zygote, protected by the archegonium, divides and grows into a sporophyte, still attached by its foot to the gametophyte.
Art Connection
This illustration shows the life cycle of mosses. (credit: modification of work by Mariana Ruiz Villareal)


Which of the following statements about the moss life cycle is false?
The mature gametophyte is haploid.
The sporophyte produces haploid spores.
The calyptra buds to form a mature gametophyte.
The zygote is housed in the venter.

The slender seta (plural, setae), as seen in [link], contains tubular cells that transfer nutrients from the base of the sporophyte (the foot) to the sporangium or capsule.
This photograph shows the long slender stems, called setae, connected to capsules of the moss Thamnobryum alopecurum. (credit: modification of work by Hermann Schachner)


A structure called a peristome increases the spread of spores after the tip of the capsule falls off at dispersal. The concentric tissue around the mouth of the capsule is made of triangular, close-fitting units, a little like teeth; these open and close depending on moisture levels, and periodically release spores.

Section SummarySeedless nonvascular plants are small, having the gametophyte as the dominant stage of the lifecycle. Without a vascular system and roots, they absorb water and nutrients on all their exposed surfaces. Collectively known as bryophytes, the three main groups include the liverworts, the hornworts, and the mosses. Liverworts are the most primitive plants and are closely related to the first land plants. Hornworts developed stomata and possess a single chloroplast per cell. Mosses have simple conductive cells and are attached to the substrate by rhizoids. They colonize harsh habitats and can regain moisture after drying out. The moss sporangium is a complex structure that allows release of spores away from the parent plant.

Art Connections


[link] Which of the following statements about the moss life cycle is false?

The mature gametophyte is haploid.
The sporophyte produces haploid spores.
The rhizoid buds to form a mature gametophyte.
The zygote is housed in the venter.



[link] C.



Review Questions

Which of the following structures is not found in bryophytes?

a cellulose cell wall
chloroplast
sporangium
root



D




Stomata appear in which group of plants?

Charales
liverworts
hornworts
mosses



C




The chromosome complement in a moss protonema is:
1n
2n
3n
varies with the size of the protonema


A




Why do mosses grow well in the Arctic tundra?

They grow better at cold temperatures.
They do not require moisture.
They do not have true roots and can grow on hard surfaces.
There are no herbivores in the tundra.



C


Free Response


In areas where it rains often, mosses grow on roofs. How do mosses survive on roofs without soil?


Mosses absorb water and nutrients carried by the rain and do not need soil because they do not derive much nutrition from the soil.




What are the three classes of bryophytes?


The bryophytes are divided into three phyla: the liverworts or Hepaticophyta, the hornworts or Anthocerotophyta, and the mosses or true Bryophyta.



Glossary

capsule case of the sporangium in mosses


gemma (plural, gemmae) leaf fragment that spreads for asexual reproduction


hornworts group of non-vascular plants in which stomata appear


liverworts most primitive group of the non-vascular plants


mosses group of bryophytes in which a primitive conductive system appears


peristome tissue that surrounds the opening of the capsule and allows periodic release of spores


protonema tangle of single celled filaments that forms from the haploid spore


rhizoids thin filaments that anchor the plant to the substrate


seta stalk that supports the capsule in mosses


