
Evolution of Seed Plants
Evolution of Seed PlantsBy the end of this section, you will be able to:

Explain when seed plants first appeared and when gymnosperms became the dominant plant group
Describe the two major innovations that allowed seed plants to reproduce in the absence of water
Discuss the purpose of pollen grains and seeds
Describe the significance of angiosperms bearing both flowers and fruit

The first plants to colonize land were most likely closely related to modern day mosses (bryophytes) and are thought to have appeared about 500 million years ago. They were followed by liverworts (also bryophytes) and primitive vascular plantsthe pterophytesfrom which modern ferns are derived. The lifecycle of bryophytes and pterophytes is characterized by the alternation of generations, like gymnosperms and angiosperms; what sets bryophytes and pterophytes apart from gymnosperms and angiosperms is their reproductive requirement for water. The completion of the bryophyte and pterophyte life cycle requires water because the male gametophyte releases sperm, which must swimpropelled by their flagellato reach and fertilize the female gamete or egg. After fertilization, the zygote matures and grows into a sporophyte, which in turn will form sporangia or "spore vessels." In the sporangia, mother cells undergo meiosis and produce the haploid spores. Release of spores in a suitable environment will lead to germination and a new generation of gametophytes.
In seed plants, the evolutionary trend led to a dominant sporophyte generation, and at the same time, a systematic reduction in the size of the gametophyte: from a conspicuous structure to a microscopic cluster of cells enclosed in the tissues of the sporophyte. Whereas lower vascular plants, such as club mosses and ferns, are mostly homosporous (produce only one type of spore), all seed plants, or spermatophytes, are heterosporous. They form two types of spores: megaspores (female) and microspores (male). Megaspores develop into female gametophytes that produce eggs, and microspores mature into male gametophytes that generate sperm. Because the gametophytes mature within the spores, they are not free-living, as are the gametophytes of other seedless vascular plants. Heterosporous seedless plants are seen as the evolutionary forerunners of seed plants.
Seeds and pollentwo critical adaptations to drought, and to reproduction that doesnt require waterdistinguish seed plants from other (seedless) vascular plants. Both adaptations were required for the colonization of land begun by the bryophytes and their ancestors. Fossils place the earliest distinct seed plants at about 350 million years ago. The first reliable record of gymnosperms dates their appearance to the Pennsylvanian period, about 319 million years ago ([link]). Gymnosperms were preceded by progymnosperms, the first naked seed plants, which arose about 380 million years ago. Progymnosperms were a transitional group of plants that superficially resembled conifers (cone bearers) because they produced wood from the secondary growth of the vascular tissues; however, they still reproduced like ferns, releasing spores into the environment. Gymnosperms dominated the landscape in the early (Triassic) and middle (Jurassic) Mesozoic era. Angiosperms surpassed gymnosperms by the middle of the Cretaceous (about 100 million years ago) in the late Mesozoic era, and today are the most abundant plant group in most terrestrial biomes.
Various plant species evolved in different eras. (credit: United States Geological Survey)




Pollen and seed were innovative structures that allowed seed plants to break their dependence on water for reproduction and development of the embryo, and to conquer dry land. The pollen grains are the male gametophytes, which contain the sperm (gametes) of the plant. The small haploid (1n) cells are encased in a protective coat that prevents desiccation (drying out) and mechanical damage. Pollen grains can travel far from their original sporophyte, spreading the plants genes. The seed offers the embryo protection, nourishment, and a mechanism to maintain dormancy for tens or even thousands of years, ensuring germination can occur when growth conditions are optimal. Seeds therefore allow plants to disperse the next generation through both space and time. With such evolutionary advantages, seed plants have become the most successful and familiar group of plants, in part because of their size and striking appearance.
Evolution of Gymnosperms
The fossil plant Elkinsia polymorpha, a "seed fern" from the Devonian periodabout 400million years agois considered the earliest seed plant known to date. Seed ferns ([link]) produced their seeds along their branches without specialized structures. What makes them the first true seed plants is that they developed structures called cupules to enclose and protect the ovulethe female gametophyte and associated tissueswhich develops into a seed upon fertilization. Seed plants resembling modern tree ferns became more numerous and diverse in the coal swamps of the Carboniferous period.
This fossilized leaf is from Glossopteris, a seed fern that thrived during the Permian age (290240million years ago). (credit: D.L. Schmidt, USGS)



Fossil records indicate the first gymnosperms (progymnosperms) most likely originated in the Paleozoic era, during the middle Devonian period: about 390 million years ago. Following the wet Mississippian and Pennsylvanian periods, which were dominated by giant fern trees, the Permian period was dry. This gave a reproductive edge to seed plants, which are better adapted to survive dry spells. The Ginkgoales, a group of gymnosperms with only one surviving speciesthe Gingko bilobawere the first gymnosperms to appear during the lower Jurassic. Gymnosperms expanded in the Mesozoic era (about 240 million years ago), supplanting ferns in the landscape, and reaching their greatest diversity during this time. The Jurassic period was as much the age of the cycads (palm-tree-like gymnosperms) as the age of the dinosaurs. Gingkoales and the more familiar conifers also dotted the landscape. Although angiosperms (flowering plants) are the major form of plant life in most biomes, gymnosperms still dominate some ecosystems, such as the taiga (boreal forests) and the alpine forests at higher mountain elevations ([link]) because of their adaptation to cold and dry growth conditions.
This boreal forest (taiga) has low-lying plants and conifer trees. (credit: L.B. Brubaker, NOAA)




Seeds and Pollen as an Evolutionary Adaptation to Dry Land
Unlike bryophyte and fern spores (which are haploid cells dependent on moisture for rapid development of gametophytes), seeds contain a diploid embryo that will germinate into a sporophyte. Storage tissue to sustain growth and a protective coat give seeds their superior evolutionary advantage. Several layers of hardened tissue prevent desiccation, and free reproduction from the need for a constant supply of water. Furthermore, seeds remain in a state of dormancyinduced by desiccation and the hormone abscisic aciduntil conditions for growth become favorable. Whether blown by the wind, floating on water, or carried away by animals, seeds are scattered in an expanding geographic range, thus avoiding competition with the parent plant.
Pollen grains ([link]) are male gametophytes and are carried by wind, water, or a pollinator. The whole structure is protected from desiccation and can reach the female organs without dependence on water. Male gametes reach female gametophyte and the egg cell gamete though a pollen tube: an extension of a cell within the pollen grain. The sperm of modern gymnosperms lack flagella, but in cycads and the Gingko, the sperm still possess flagella that allow them to swim down the pollen tube to the female gamete; however, they are enclosed in a pollen grain.
This fossilized pollen is from a Buckbean fen core found in Yellowstone National Park, Wyoming. The pollen is magnified 1,054 times. (credit: R.G. Baker, USGS; scale-bar data from Matt Russell)




Evolution of Angiosperms
Undisputed fossil records place the massive appearance and diversification of angiosperms in the middle to late Mesozoic era. Angiosperms (seed in a vessel) produce a flower containing male and/or female reproductive structures. Fossil evidence ([link]) indicates that flowering plants first appeared in the Lower Cretaceous, about 125 million years ago, and were rapidly diversifying by the Middle Cretaceous, about 100 million years ago. Earlier traces of angiosperms are scarce. Fossilized pollen recovered from Jurassic geological material has been attributed to angiosperms. A few early Cretaceous rocks show clear imprints of leaves resembling angiosperm leaves. By the mid-Cretaceous, a staggering number of diverse flowering plants crowd the fossil record. The same geological period is also marked by the appearance of many modern groups of insects, including pollinating insects that played a key role in ecology and the evolution of flowering plants.
Although several hypotheses have been offered to explain this sudden profusion and variety of flowering plants, none have garnered the consensus of paleobotanists (scientists who study ancient plants). New data in comparative genomics and paleobotany have, however, shed some light on the evolution of angiosperms. Rather than being derived from gymnosperms, angiosperms form a sister clade (a species and its descendents) that developed in parallel with the gymnosperms. The two innovative structures of flowers and fruit represent an improved reproductive strategy that served to protect the embryo, while increasing genetic variability and range. Paleobotanists debate whether angiosperms evolved from small woody bushes, or were basal angiosperms related to tropical grasses. Both views draw support from cladistics studies, and the so-called woody magnoliid hypothesiswhich proposes that the early ancestors of angiosperms were shrubsalso offers molecular biological evidence.
The most primitive living angiosperm is considered to be Amborella trichopoda, a small plant native to the rainforest of New Caledonia, an island in the South Pacific. Analysis of the genome of A. trichopoda has shown that it is related to all existing flowering plants and belongs to the oldest confirmed branch of the angiosperm family tree. A few other angiosperm groups called basal angiosperms, are viewed as primitive because they branched off early from the phylogenetic tree. Most modern angiosperms are classified as either monocots or eudicots, based on the structure of their leaves and embryos. Basal angiosperms, such as water lilies, are considered more primitive because they share morphological traits with both monocots and eudicots.
This leaf imprint shows a Ficus speciosissima, an angiosperm that flourished during the Cretaceous period. (credit: W. T. Lee, USGS)


Flowers and Fruits as an Evolutionary AdaptationAngiosperms produce their gametes in separate organs, which are usually housed in a flower. Both fertilization and embryo development take place inside an anatomical structure that provides a stable system of sexual reproduction largely sheltered from environmental fluctuations. Flowering plants are the most diverse phylum on Earth after insects; flowers come in a bewildering array of sizes, shapes, colors, smells, and arrangements. Most flowers have a mutualistic pollinator, with the distinctive features of flowers reflecting the nature of the pollination agent. The relationship between pollinator and flower characteristics is one of the great examples of coevolution.
Following fertilization of the egg, the ovule grows into a seed. The surrounding tissues of the ovary thicken, developing into a fruit that will protect the seed and often ensure its dispersal over a wide geographic range. Not all fruits develop from an ovary; such structures are false fruits. Like flowers, fruit can vary tremendously in appearance, size, smell, and taste. Tomatoes, walnut shells and avocados are all examples of fruit. As with pollen and seeds, fruits also act as agents of dispersal. Some may be carried away by the wind. Many attract animals that will eat the fruit and pass the seeds through their digestive systems, then deposit the seeds in another location. Cockleburs are covered with stiff, hooked spines that can hook into fur (or clothing) and hitch a ride on an animal for long distances. The cockleburs that clung to the velvet trousers of an enterprising Swiss hiker, George de Mestral, inspired his invention of the loop and hook fastener he named Velcro.
Evolution Connection
Building Phylogenetic Trees with Analysis of DNA Sequence AlignmentsAll living organisms display patterns of relationships derived from their evolutionary history. Phylogeny is the science that describes the relative connections between organisms, in terms of ancestral and descendant species. Phylogenetic trees, such as the plant evolutionary history shown in [link], are tree-like branching diagrams that depict these relationships. Species are found at the tips of the branches. Each branching point, called a node, is the point at which a single taxonomic group (taxon), such as a species, separates into two or more species.
This phylogenetic tree shows the evolutionary relationships of plants.


Phylogenetic trees have been built to describe the relationships between species since Darwins time. Traditional methods involve comparison of homologous anatomical structures and embryonic development, assuming that closely related organisms share anatomical features during embryo development. Some traits that disappear in the adult are present in the embryo; for example, a human fetus, at one point, has a tail. The study of fossil records shows the intermediate stages that link an ancestral form to its descendants. Most of these approaches are imprecise and lend themselves to multiple interpretations. As the tools of molecular biology and computational analysis have been developed and perfected in recent years, a new generation of tree-building methods has taken shape. The key assumption is that genes for essential proteins or RNA structures, such as the ribosomal RNA, are inherently conserved because mutations (changes in the DNA sequence) could compromise the survival of the organism. DNA from minute amounts of living organisms or fossils can be amplified by polymerase chain reaction (PCR) and sequenced, targeting the regions of the genome that are most likely to be conserved between species. The genes encoding the ribosomal RNA from the small 18S subunit and plastid genes are frequently chosen for DNA alignment analysis.
Once the sequences of interest are obtained, they are compared with existing sequences in databases such as GenBank, which is maintained by The National Center for Biotechnology Information. A number of computational tools are available to align and analyze sequences. Sophisticated computer analysis programs determine the percentage of sequence identity or homology. Sequence homology can be used to estimate the evolutionary distance between two DNA sequences and reflect the time elapsed since the genes separated from a common ancestor. Molecular analysis has revolutionized phylogenetic trees. In some cases, prior results from morphological studies have been confirmed: for example, confirming Amborella trichopoda as the most primitive angiosperm known. However, some groups and relationships have been rearranged as a result of DNA analysis.

Section SummarySeed plants appeared about one million years ago, during the Carboniferous period. Two major innovationsseed and pollenallowed seed plants to reproduce in the absence of water. The gametophytes of seed plants shrank, while the sporophytes became prominent structures and the diploid stage became the longest phase of the lifecycle. Gymnosperms became the dominant group during the Triassic. In these, pollen grains and seeds protect against desiccation. The seed, unlike a spore, is a diploid embryo surrounded by storage tissue and protective layers. It is equipped to delay germination until growth conditions are optimal. Angiosperms bear both flowers and fruit. The structures protect the gametes and the embryo during its development. Angiosperms appeared during the Mesozoic era and have become the dominant plant life in terrestrial habitats.

Review Questions

Seed plants are ________.

all homosporous.
mostly homosporous with some heterosporous.
mostly heterosporous with some homosporous.
all heterosporous.



D




Besides the seed, what other major structure diminishes a plants reliance on water for reproduction?

flower
fruit
pollen
spore



A




In which of the following geological periods would gymnosperms dominate the landscape?

Carboniferous
Permian
Triassic
Eocene (present)



C




Which of the following structures widens the geographic range of a species and is an agent of dispersal?

seed
flower
leaf
root



A


Free Response


The Triassic Period was marked by the increase in number and variety of angiosperms. Insects also diversified enormously during the same period. Can you propose the reason or reasons that could foster coevolution?


Both pollination and herbivory contributed to diversity, with plants needing to attract some insects and repel others.




What role did the adaptations of seed and pollen play in the development and expansion of seed plants?


Seeds and pollen allowed plants to reproduce in absence of water. This allowed them to expand their range onto dry land and to survive drought conditions.



Glossary

flower branches specialized for reproduction found in some seed-bearing plants, containing either specialized male or female organs or both male and female organs


fruit thickened tissue derived from ovary wall that protects the embryo after fertilization and facilitates seed dispersal


ovule female gametophyte


pollen grain structure containing the male gametophyte of the plant


pollen tube extension from the pollen grain that delivers sperm to the egg cell


progymnosperm transitional group of plants that resembled conifers because they produced wood, yet still reproduced like ferns


seed structure containing the embryo, storage tissue and protective coat


spermatophyte seed plant; from the Greek sperm (seed) and phyte (plant)


