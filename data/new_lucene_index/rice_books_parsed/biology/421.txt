
Historical Basis of Modern Understanding
Historical Basis of Modern UnderstandingBy the end of this section, you will be able to:

Explain transformation of DNA
Describe the key experiments that helped identify that DNA is the genetic material
State and explain Chargaffs rules

Modern understandings of DNA have evolved from the discovery of nucleic acid to the development of the double-helix model. In the 1860s, Friedrich Miescher ([link]), a physician by profession, was the first person to isolate phosphate-rich chemicals from white blood cells or leukocytes. He named these chemicals (which would eventually be known as RNA and DNA) nuclein because they were isolated from the nuclei of the cells.
Friedrich Miescher (18441895) discovered nucleic acids.


Link to Learning



To see Miescher conduct an experiment step-by-step, click through this review of how he discovered the key role of DNA and proteins in the nucleus.
A half century later, British bacteriologist Frederick Griffith was perhaps the first person to show that hereditary information could be transferred from one cell to another horizontally, rather than by descent. In 1928, he reported the first demonstration of bacterial transformation, a process in which external DNA is taken up by a cell, thereby changing morphology and physiology. He was working with Streptococcus pneumoniae, the bacterium that causes pneumonia. Griffith worked with two strains, rough (R) and smooth (S). The R strain is non-pathogenic (does not cause disease) and is called rough because its outer surface is a cell wall and lacks a capsule; as a result, the cell surface appears uneven under the microscope. The S strain is pathogenic (disease-causing) and has a capsule outside its cell wall. As a result, it has a smooth appearance under the microscope. Griffith injected the live R strain into mice and they survived. In another experiment, when he injected mice with the heat-killed S strain, they also survived. In a third set of experiments, a mixture of live R strain and heat-killed S strain were injected into mice, andto his surprisethe mice died. Upon isolating the live bacteria from the dead mouse, only the S strain of bacteria was recovered. When this isolated S strain was injected into fresh mice, the mice died. Griffith concluded that something had passed from the heat-killed S strain into the live R strain and transformed it into the pathogenic S strain, and he called this the transforming principle ([link]). These experiments are now famously known as Griffith's transformation experiments.
Two strains of S. pneumoniae were used in Griffiths transformation experiments. The R strain is non-pathogenic. The S strain is pathogenic and causes death. When Griffith injected a mouse with the heat-killed S strain and a live R strain, the mouse died. The S strain was recovered from the dead mouse. Thus, Griffith concluded that something had passed from the heat-killed S strain to the R strain, transforming the R strain into S strain in the process. (credit "living mouse": modification of work by NIH; credit "dead mouse": modification of work by Sarah Marriage)


Scientists Oswald Avery, Colin MacLeod, and Maclyn McCarty (1944) were interested in exploring this transforming principle further. They isolated the S strain from the dead mice and isolated the proteins and nucleic acids, namely RNA and DNA, as these were possible candidates for the molecule of heredity. They conducted a systematic elimination study. They used enzymes that specifically degraded each component and then used each mixture separately to transform the R strain. They found that when DNA was degraded, the resulting mixture was no longer able to transform the bacteria, whereas all of the other combinations were able to transform the bacteria. This led them to conclude that DNA was the transforming principle.
Career Connection
Forensic Scientists and DNA AnalysisDNA evidence was used for the first time to solve an immigration case. The story started with a teenage boy returning to London from Ghana to be with his mother. Immigration authorities at the airport were suspicious of him, thinking that he was traveling on a forged passport. After much persuasion, he was allowed to go live with his mother, but the immigration authorities did not drop the case against him. All types of evidence, including photographs, were provided to the authorities, but deportation proceedings were started nevertheless. Around the same time, Dr. Alec Jeffreys of Leicester University in the United Kingdom had invented a technique known as DNA fingerprinting. The immigration authorities approached Dr. Jeffreys for help. He took DNA samples from the mother and three of her children, plus an unrelated mother, and compared the samples with the boys DNA. Because the biological father was not in the picture, DNA from the three children was compared with the boys DNA. He found a match in the boys DNA for both the mother and his three siblings. He concluded that the boy was indeed the mothers son.
Forensic scientists analyze many items, including documents, handwriting, firearms, and biological samples. They analyze the DNA content of hair, semen, saliva, and blood, and compare it with a database of DNA profiles of known criminals. Analysis includes DNA isolation, sequencing, and sequence analysis; most forensic DNA analysis involves polymerase chain reaction (PCR) amplification of short tandem repeat (STR) loci and electrophoresis to determine the length of the PCR-amplified fragment. Only mitochondrial DNA is sequenced for forensics. Forensic scientists are expected to appear at court hearings to present their findings. They are usually employed in crime labs of city and state government agencies. Geneticists experimenting with DNA techniques also work for scientific and research organizations, pharmaceutical industries, and college and university labs. Students wishing to pursue a career as a forensic scientist should have at least a bachelor's degree in chemistry, biology, or physics, and preferably some experience working in a laboratory.
Experiments conducted by Martha Chase and Alfred Hershey in 1952 provided confirmatory evidence that DNA was the genetic material and not proteins. Chase and Hershey were studying a bacteriophage, which is a virus that infects bacteria. Viruses typically have a simple structure: a protein coat, called the capsid, and a nucleic acid core that contains the genetic material, either DNA or RNA. The bacteriophage infects the host bacterial cell by attaching to its surface, and then it injects its nucleic acids inside the cell. The phage DNA makes multiple copies of itself using the host machinery, and eventually the host cell bursts, releasing a large number of bacteriophages. Hershey and Chase labeled one batch of phage with radioactive sulfur, 35S, to label the protein coat. Another batch of phage were labeled with radioactive phosphorus, 32P. Because phosphorous is found in DNA, but not protein, the DNA and not the protein would be tagged with radioactive phosphorus.
Each batch of phage was allowed to infect the cells separately. After infection, the phage bacterial suspension was put in a blender, which caused the phage coat to be detached from the host cell. The phage and bacterial suspension was spun down in a centrifuge. The heavier bacterial cells settled down and formed a pellet, whereas the lighter phage particles stayed in the supernatant. In the tube that contained phage labeled with 35S, the supernatant contained the radioactively labeled phage, whereas no radioactivity was detected in the pellet. In the tube that contained the phage labeled with 32P, the radioactivity was detected in the pellet that contained the heavier bacterial cells, and no radioactivity was detected in the supernatant.  Hershey and Chase concluded that it was the phage DNA that was injected
into the cell and carried information to produce more phage particles, thus providing evidence that DNA was the genetic material and not proteins ([link]).In Hershey and Chase's experiments, bacteria were infected with phage radiolabeled with either 35S, which labels protein, or 32P, which labels DNA. Only 32P entered the bacterial cells, indicating that DNA is the genetic material.


Around this same time, Austrian biochemist Erwin Chargaff examined the content of DNA in different species and found that the amounts of adenine, thymine, guanine, and cytosine were not found in equal quantities, and that it varied from species to species, but not between individuals of the same species. He found that the amount of adenine equals the amount of thymine, and the amount of cytosine equals the amount of guanine, or A = T and G = C. This is also known as Chargaffs rules. This finding proved immensely useful when Watson and Crick were getting ready to propose their DNA double helix model.
Section SummaryDNA was first isolated from white blood cells by Friedrich Miescher, who called it nuclein because it was isolated from nuclei. Frederick Griffith's experiments with strains of Streptococcus pneumoniae provided the first hint that DNA may be the transforming principle. Avery, MacLeod, and McCarty proved that DNA is required for the transformation of bacteria. Later experiments by Hershey and Chase using bacteriophage T2 proved that DNA is the genetic material. Chargaff found that the ratio of A = T and C = G, and that the percentage content of A, T, G, and C is different for different species.

Review Questions
If DNA of a particular species was analyzed and it was found that it contains 27 percent A, what would be the percentage of C?

27 percent
30 percent
23 percent
54 percent



C



The experiments by Hershey and Chase helped confirm that DNA was the hereditary material on the basis of the finding that:

radioactive phage were found in the pellet
radioactive cells were found in the supernatant
radioactive sulfur was found inside the cell
radioactive phosphorus was found in the cell



D



Free Response

Explain Griffith's transformation experiments. What did he conclude from them?

Live R cells acquired genetic information from the heat-killed S cells that transformed the R cells into S cells.



Why were radioactive sulfur and phosphorous used to label bacteriophage in Hershey and Chase's experiments?

Sulfur is an element found in proteins and phosphorus is a component of nucleic acids.



Glossary
transformation process in which external DNA is taken up by a cell

