
Birds
BirdsBy the end of this section, you will be able to:

Describe the evolutionary history of birds
Describe the derived characteristics in birds that facilitate flight

The most obvious characteristic that sets birds apart from other modern vertebrates is the presence of feathers, which are modified scales. While vertebrates like bats fly without feathers, birds rely on feathers and wings, along with other modifications of body structure and physiology, for flight.
Characteristics of Birds
Birds are endothermic, and because they fly, they require large amounts of energy, necessitating a high metabolic rate. Like mammals, which are also endothermic, birds have an insulating covering that keeps heat in the body: feathers. Specialized feathers called down feathers are especially insulating, trapping air in spaces between each feather to decrease the rate of heat loss. Certain parts of a birds body are covered in down feathers, and the base of other feathers have a downy portion, whereas newly hatched birds are covered in down.
Feathers not only act as insulation but also allow for flight, enabling the lift and thrust necessary to become airborne. The feathers on a wing are flexible, so the collective feathers move and separate as air moves through them, reducing the drag on the wing. Flight feathers are asymmetrical, which affects airflow over them and provides some of the lifting and thrusting force required for flight ([link]). Two types of flight feathers are found on the wings, primary feathers and secondary feathers. Primary feathers are located at the tip of the wing and provide thrust. Secondary feathers are located closer to the body, attach to the forearm portion of the wing and provide lift. Contour feathers are the feathers found on the body, and they help reduce drag produced by wind resistance during flight. They create a smooth, aerodynamic surface so that air moves smoothly over the birds body, allowing for efficient flight.
Primary feathers are located at the wing tip and provide thrust; secondary feathers are located close to the body and provide lift.


Flapping of the entire wing occurs primarily through the actions of the chest muscles, the pectoralis and the supracoracoideus. These muscles are highly developed in birds and account for a higher percentage of body mass than in most mammals. These attach to a blade-shaped keel, like that of a boat, located on the sternum. The sternum of birds is larger than that of other vertebrates, which accommodates the large muscles required to generate enough upward force to generate lift with the flapping of the wings. Another skeletal modification found in most birds is the fusion of the two clavicles (collarbones), forming the furcula or wishbone. The furcula is flexible enough to bend and provide support to the shoulder girdle during flapping.
An important requirement of flight is a low body weight. As body weight increases, the muscle output required for flying increases. The largest living bird is the ostrich, and while it is much smaller than the largest mammals, it is flightless. For birds that do fly, reduction in body weight makes flight easier. Several modifications are found in birds to reduce body weight, including pneumatization of bones. Pneumatic bones are bones that are hollow, rather than filled with tissue ([link]). They contain air spaces that are sometimes connected to air sacs, and they have struts of bone to provide structural reinforcement. Pneumatic bones are not found in all birds, and they are more extensive in large birds than in small birds. Not all bones of the skeleton are pneumatic, although the skulls of almost all birds are.
Many birds have hollow, pneumatic bones, which make flight easier.


Other modifications that reduce weight include the lack of a urinary bladder. Birds possess a cloaca, a structure that allows water to be reabsorbed from waste back into the bloodstream. Uric acid is not expelled as a liquid but is concentrated into urate salts, which are expelled along with fecal matter. In this way, water is not held in the urinary bladder, which would increase body weight. Most bird species only possess one ovary rather than two, further reducing body mass.
The air sacs that extend into bones to form pneumatic bones also join with the lungs and function in respiration. Unlike mammalian lungs in which air flows in two directions, as it is breathed in and out, airflow through bird lungs travels in one direction ([link]). Air sacs allow for this unidirectional airflow, which also creates a cross-current exchange system with the blood. In a cross-current or counter-current system, the air flows in one direction and the blood flows in the opposite direction, creating a very efficient means of gas exchange.
Avian respiration is an efficient system of gas exchange with air flowing unidirectionally. During inhalation, air passes from the trachea into posterior air sacs, then through the lungs to anterior air sacs. The air sacs are connected to the hollow interior of bones. During exhalation, air from air sacs passes into the lungs and out the trachea. (credit: modification of work by L. Shyamal)



Evolution of Birds
The evolutionary history of birds is still somewhat unclear. Due to the fragility of bird bones, they do not fossilize as well as other vertebrates. Birds are diapsids, meaning they have two fenestrations or openings in their skulls. Birds belong to a group of diapsids called the archosaurs, which also includes crocodiles and dinosaurs. It is commonly accepted that birds evolved from dinosaurs.
Dinosaurs (including birds) are further subdivided into two groups, the Saurischia (lizard like) and the Ornithischia (bird like). Despite the names of these groups, it was not the bird-like dinosaurs that gave rise to modern birds. Rather, Saurischia diverged into two groups: One included the long-necked herbivorous dinosaurs, such as Apatosaurus. The second group, bipedal predators called theropods, includes birds. This course of evolution is suggested by similarities between theropod fossils and birds, specifically in the structure of the hip and wrist bones, as well as the presence of the wishbone, formed by the fusing of the clavicles.One important fossil of an animal intermediate to dinosaurs and birds is Archaeopteryx, which is from the Jurassic period ([link]). Archaeopteryx is important in establishing the relationship between birds and dinosaurs, because it is an intermediate fossil, meaning it has characteristics of both dinosaurs and birds. Some scientists propose classifying it as a bird, but others prefer to classify it as a dinosaur. The fossilized skeleton of Archaeopteryx looks like that of a dinosaur, and it had teeth whereas birds do not, but it also had feathers modified for flight, a trait associated only with birds among modern animals. Fossils of older feathered dinosaurs exist, but the feathers do not have the characteristics of flight feathers.(a) Archaeopteryx lived in the late Jurassic Period around 150 million years ago. It had teeth like a dinosaur, but had (b) flight feathers like modern birds, which can be seen in this fossil.


It is still unclear exactly how flight evolved in birds. Two main theories exist, the arboreal (tree) hypothesis and the terrestrial (land) hypothesis. The arboreal hypothesis posits that tree-dwelling precursors to modern birds jumped from branch to branch using their feathers for gliding before becoming fully capable of flapping flight. In contrast to this, the terrestrial hypothesis holds that running was the stimulus for flight, as wings could be used to improve running and then became used for flapping flight. Like the question of how flight evolved, the question of how endothermy evolved in birds still is unanswered. Feathers provide insulation, but this is only beneficial if body heat is being produced internally. Similarly, internal heat production is only viable if insulation is present to retain that heat. It has been suggested that one or the otherfeathers or endothermyevolved in response to some other selective pressure.
During the Cretaceous period, a group known as the Enantiornithes was the dominant bird type ([link]). Enantiornithes means opposite birds, which refers to the fact that certain bones of the feet are joined differently than the way the bones are joined in modern birds. These birds formed an evolutionary line separate from modern birds, and they did not survive past the Cretaceous. Along with the Enantiornithes, Ornithurae birds (the evolutionary line that includes modern birds) were also present in the Cretaceous. After the extinction of Enantiornithes, modern birds became the dominant bird, with a large radiation occurring during the Cenozoic Era. Referred to as Neornithes (new birds), modern birds are now classified into two groups, the Paleognathae (old jaw) or ratites, a group of flightless birds including ostriches, emus, rheas, and kiwis, and the Neognathae (new jaw), which includes all other birds.
Shanweiniao cooperorum was a species of Enantiornithes that did not survive past the Cretaceous period. (credit: Nobu Tamura)



Career Connection
Veterinarian
Veterinarians treat diseases, disorders, and injuries in animals, primarily vertebrates. They treat pets, livestock, and animals in zoos and laboratories. Veterinarians usually treat dogs and cats, but also treat birds, reptiles, rabbits, and other animals that are kept as pets. Veterinarians that work with farms and ranches treat pigs, goats, cows, sheep, and horses.
Veterinarians are required to complete a degree in veterinary medicine, which includes taking courses in animal physiology, anatomy, microbiology, and pathology, among many other courses. The physiology and biochemistry of different vertebrate species differ greatly.
Veterinarians are also trained to perform surgery on many different vertebrate species, which requires an understanding of the vastly different anatomies of various species. For example, the stomach of ruminants like cows has four compartments versus one compartment for non-ruminants. Birds also have unique anatomical adaptations that allow for flight.
Some veterinarians conduct research in academic settings, broadening our knowledge of animals and medical science. One area of research involves understanding the transmission of animal diseases to humans, called zoonotic diseases. For example, one area of great concern is the transmission of the avian flu virus to humans. One type of avian flu virus, H5N1, is a highly pathogenic strain that has been spreading in birds in Asia, Europe, Africa, and the Middle East. Although the virus does not cross over easily to humans, there have been cases of bird-to-human transmission. More research is needed to understand how this virus can cross the species barrier and how its spread can be prevented.


Section SummaryBirds are endothermic, meaning they produce their own body heat and regulate their internal temperature independently of the external temperature. Feathers not only act as insulation but also allow for flight, providing lift with secondary feathers and thrust with primary feathers. Pneumatic bones are bones that are hollow rather than filled with tissue, containing air spaces that are sometimes connected to air sacs. Airflow through bird lungs travels in one direction, creating a cross-current exchange with the blood. Birds are diapsids and belong to a group called the archosaurs. Birds are thought to have evolved from theropod dinosaurs. The oldest known fossil of a bird is that of Archaeopteryx, which is from the Jurassic period. Modern birds are now classified into two groups, Paleognathae and Neognathae.

Review Questions
A bird or feathered dinosaur is ________.

Neornithes
Archaeopteryx
Enantiornithes
Paleognathae



B



Which of the following feather types helps to reduce drag produced by wind resistance during flight?

flight feathers
primary feathers
secondary feathers
contour feathers



D



Free Response

Explain why birds are thought to have evolved from theropod dinosaurs.

This is suggested by similarities observed between theropod fossils and birds, specifically in the design of the hip and wrist bones, as well as the presence of a furcula, or wishbone, formed by the fusing of the clavicles.



Describe three skeletal adaptations that allow for flight in birds.

The sternum of birds is larger than that of other vertebrates, which accommodates the force required for flapping. Another skeletal modification is the fusion of the clavicles, forming the furcula or wishbone. The furcula is flexible enough to bend during flapping and provides support to the shoulder girdle during flapping. Birds also have pneumatic bones that are hollow rather than filled with tissue.



Glossary
Archaeopteryx transition species from dinosaur to bird from the Jurassic period
contour feather feather that creates an aerodynamic surface for efficient flight
down feather feather specialized for insulation
Enantiornithes dominant bird group during the Cretaceous period
flight feather feather specialized for flight
furcula wishbone formed by the fusing of the clavicles
Neognathae birds other than the Paleognathae
Neornithes modern birds
Paleognathae ratites; flightless birds, including ostriches and emus
pneumatic bone air-filled bone
primary feather feather located at the tip of the wing that provides thrust
secondary feather feather located at the base of the wing that provides lift
theropod dinosaur group ancestral to birds

