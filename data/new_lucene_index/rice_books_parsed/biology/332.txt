
Prokaryotic Gene Regulation
Prokaryotic Gene RegulationBy the end of this section, you will be able to:

Describe the steps involved in prokaryotic gene regulation
Explain the roles of activators, inducers, and repressors in gene regulation

The DNA of prokaryotes is organized into a circular chromosome supercoiled in the nucleoid region of the cell cytoplasm. Proteins that are needed for a specific function, or that are involved in the same biochemical pathway, are encoded together in blocks called operons. For example, all of the genes needed to use lactose as an energy source are coded next to each other in the lactose (or lac) operon.
In prokaryotic cells, there are three types of regulatory molecules that can affect the expression of operons: repressors, activators, and inducers. Repressors are proteins that suppress transcription of a gene in response to an external stimulus, whereas activators are proteins that increase the transcription of a gene in response to an external stimulus. Finally, inducers are small molecules that either activate or repress transcription depending on the needs of the cell and the availability of substrate.
The trp Operon: A Repressor Operon
Bacteria such as E. coli need amino acids to survive. Tryptophan is one such amino acid that E. coli can ingest from the environment. E. coli can also synthesize tryptophan using enzymes that are encoded by five genes. These five genes are next to each other in what is called the tryptophan (trp) operon ([link]). If tryptophan is present in the environment, then E. coli does not need to synthesize it and the switch controlling the activation of the genes in the trp operon is switched off. However, when tryptophan availability is low, the switch controlling the operon is turned on, transcription is initiated, the genes are expressed, and tryptophan is synthesized.
The five genes that are needed to synthesize tryptophan in E. coli are located next to each other in the trp operon. When tryptophan is plentiful, two tryptophan molecules bind the repressor protein at the operator sequence. This physically blocks the RNA polymerase from transcribing the tryptophan genes. When tryptophan is absent, the repressor protein does not bind to the operator and the genes are transcribed.


A DNA sequence that codes for proteins is referred to as the coding region. The five coding regions for the tryptophan biosynthesis enzymes are arranged sequentially on the chromosome in the operon. Just before the coding region is the transcriptional start site. This is the region of DNA to which RNA polymerase binds to initiate transcription. The promoter sequence is upstream of the transcriptional start site; each operon has a sequence within or near the promoter to which proteins (activators or repressors) can bind and regulate transcription.
A DNA sequence called the operator sequence is encoded between the promoter region and the first trp coding gene. This operator contains the DNA code to which the repressor protein can bind. When tryptophan is present in the cell, two tryptophan molecules bind to the trp repressor, which changes shape to bind to the trp operator. Binding of the tryptophanrepressor complex at the operator physically prevents the RNA polymerase from binding, and transcribing the downstream genes.
When tryptophan is not present in the cell, the repressor by itself does not bind to the operator; therefore, the operon is active and tryptophan is synthesized. Because the repressor protein actively binds to the operator to keep the genes turned off, the trp operon is negatively regulated and the proteins that bind to the operator to silence trp expression are negative regulators.
Link to Learning



Watch this video to learn more about the trp operon.

Catabolite Activator Protein (CAP): An Activator Regulator
Just as the trp operon is negatively regulated by tryptophan molecules, there are proteins that bind to the operator sequences that act as a positive regulator to turn genes on and activate them. For example, when glucose is scarce, E. coli bacteria can turn to other sugar sources for fuel. To do this, new genes to process these alternate genes must be transcribed. When glucose levels drop, cyclic AMP (cAMP) begins to accumulate in the cell. The cAMP molecule is a signaling molecule that is involved in glucose and energy metabolism in E. coli. When glucose levels decline in the cell, accumulating cAMP binds to the positive regulator catabolite activator protein (CAP), a protein that binds to the promoters of operons that control the processing of alternative sugars. When cAMP binds to CAP, the complex binds to the promoter region of the genes that are needed to use the alternate sugar sources ([link]). In these operons, a CAP binding site is located upstream of the RNA polymerase binding site in the promoter. This increases the binding ability of RNA polymerase to the promoter region and the transcription of the genes.
When glucose levels fall, E. coli may use other sugars for fuel but must transcribe new genes to do so. As glucose supplies become limited, cAMP levels increase. This cAMP binds to the CAP protein, a positive regulator that binds to an operator region upstream of the genes required to use other sugar sources.



The lac Operon: An Inducer Operon
The third type of gene regulation in prokaryotic cells occurs through inducible operons, which have proteins that bind to activate or repress transcription depending on the local environment and the needs of the cell. The lac operon is a typical inducible operon. As mentioned previously, E. coli is able to use other sugars as energy sources when glucose concentrations are low. To do so, the cAMPCAP protein complex serves as a positive regulator to induce transcription. One such sugar source is lactose. The lac operon encodes the genes necessary to acquire and process the lactose from the local environment. CAP binds to the operator sequence upstream of the promoter that initiates transcription of the lac operon. However, for the lac operon to be activated, two conditions must be met. First, the level of glucose must be very low or non-existent. Second, lactose must be present. Only when glucose is absent and lactose is present will the lac operon be transcribed ([link]). This makes sense for the cell, because it would be energetically wasteful to create the proteins to process lactose if glucose was plentiful or lactose was not available.
Art Connection

Transcription of the lac operon is carefully regulated so that its expression only occurs when glucose is limited and lactose is present to serve as an alternative fuel source.



In E. coli, the trp operon is on by default, while the lac operon is off. Why do you think this is the case?
If glucose is absent, then CAP can bind to the operator sequence to activate transcription. If lactose is absent, then the repressor binds to the operator to prevent transcription. If either of these requirements is met, then transcription remains off. Only when both conditions are satisfied is the lac operon transcribed ([link]).

Signals that Induce or Repress Transcription of the lac Operon

GlucoseCAP bindsLactoseRepressor binds Transcription


+--+No
+-+-Some
-+-+No
-++-Yes

Link to Learning



Watch an animated tutorial about the workings of lac operon here.
Section SummaryThe regulation of gene expression in prokaryotic cells occurs at the transcriptional level. There are three ways to control the transcription of an operon: repressive control, activator control, and inducible control. Repressive control, typified by the trp operon, uses proteins bound to the operator sequence to physically prevent the binding of RNA polymerase and the activation of transcription. Therefore, if tryptophan is not needed, the repressor is bound to the operator and transcription remains off. Activator control, typified by the action of CAP, increases the binding ability of RNA polymerase to the promoter when CAP is bound. In this case, low levels of glucose result in the binding of cAMP to CAP. CAP then binds the promoter, which allows RNA polymerase to bind to the promoter better. In the last examplethe lac operontwo conditions must be met to initiate transcription. Glucose must not be present, and lactose must be available for the lac operon to be transcribed. If glucose is absent, CAP binds to the operator. If lactose is present, the repressor protein does not bind to its operator. Only when both conditions are met will RNA polymerase bind to the promoter to induce transcription.

Art Connections

[link] In E. coli, the trp operon is on by default, while the lac operon is off. Why do you think that this is the case?


[link] Tryptophan is an amino acid essential for making proteins, so the cell always needs to have some on hand. However, if plenty of tryptophan is present, it is wasteful to make more, and the expression of the trp receptor is repressed. Lactose, a sugar found in milk, is not always available. It makes no sense to make the enzymes necessary to digest an energy source that is not available, so the lac operon is only turned on when lactose is present.



Review Questions
If glucose is absent, but so is lactose, the lac operon will be ________.

activated
repressed
activated, but only partially
mutated



B



Prokaryotic cells lack a nucleus. Therefore, the genes in prokaryotic cells are:

all expressed, all of the time
transcribed and translated almost simultaneously
transcriptionally controlled because translation begins before transcription ends
b and c are both true



D



Free Response

Describe how transcription in prokaryotic cells can be altered by external stimulation such as excess lactose in the environment.

Environmental stimuli can increase or induce transcription in prokaryotic cells. In this example, lactose in the environment will induce the transcription of the lac operon, but only if glucose is not available in the environment.



What is the difference between a repressible and an inducible operon?

A repressible operon uses a protein bound to the promoter region of a gene to keep the gene repressed or silent. This repressor must be actively removed in order to transcribe the gene. An inducible operon is either activated or repressed depending on the needs of the cell and what is available in the local environment.



Glossary
activator protein that binds to prokaryotic operators to increase transcription
catabolite activator protein (CAP) protein that complexes with cAMP to bind to the promoter sequences of operons that control sugar processing when glucose is not available
inducible operon operon that can be activated or repressed depending on cellular needs and the surrounding environment
lac operon operon in prokaryotic cells that encodes genes required for processing and intake of lactose
negative regulator protein that prevents transcription
operator region of DNA outside of the promoter region that binds activators or repressors that control gene expression in prokaryotic cells
operon collection of genes involved in a pathway that are transcribed together as a single mRNA in prokaryotic cells
positive regulator protein that increases transcription
repressor protein that binds to the operator of prokaryotic genes to prevent transcription
transcriptional start site site at which transcription begins
trp operon series of genes necessary to synthesize tryptophan in prokaryotic cells
tryptophan amino acid that can be synthesized by prokaryotic cells when necessary

