
The Peripheral Nervous System
The Peripheral Nervous SystemBy the end of this section, you will be able to:

Describe the organization and functions of the sympathetic and parasympathetic nervous systems
Describe the organization and function of the sensory-somatic nervous system

The peripheral nervous system (PNS) is the connection between the central nervous system and the rest of the body. The CNS is like the power plant of the nervous system. It creates the signals that control the functions of the body. The PNS is like the wires that go to individual houses. Without those wires, the signals produced by the CNS could not control the body (and the CNS would not be able to receive sensory information from the body either).
The PNS can be broken down into the autonomic nervous system, which controls bodily functions without conscious control, and the sensory-somatic nervous system, which transmits sensory information from the skin, muscles, and sensory organs to the CNS and sends motor commands from the CNS to the muscles.
Autonomic Nervous System
Art Connection
In the autonomic nervous system, a preganglionic neuron of the CNS synapses with a postganglionic neuron of the PNS. The postganglionic neuron, in turn, acts on a target organ. Autonomic responses are mediated by the sympathetic and the parasympathetic systems, which are antagonistic to one another. The sympathetic system activates the fight or flight response, while the parasympathetic system activates the rest and digest response.


 Which of the following statements is false?

The parasympathetic pathway is responsible for resting the body, while the sympathetic pathway is responsible for preparing for an emergency.
Most preganglionic neurons in the sympathetic pathway originate in the spinal cord.
Slowing of the heartbeat is a parasympathetic response.
Parasympathetic neurons are responsible for releasing norepinephrine on the target organ, while sympathetic neurons are responsible for releasing acetylcholine.


The autonomic nervous system serves as the relay between the CNS and the internal organs. It controls the lungs, the heart, smooth muscle, and exocrine and endocrine glands. The autonomic nervous system controls these organs largely without conscious control; it can continuously monitor the conditions of these different systems and implement changes as needed. Signaling to the target tissue usually involves two synapses: a preganglionic neuron (originating in the CNS) synapses to a neuron in a ganglion that, in turn, synapses on the target organ, as illustrated in [link]. There are two divisions of the autonomic nervous system that often have opposing effects: the sympathetic nervous system and the parasympathetic nervous system.
Sympathetic Nervous System
The sympathetic nervous system is responsible for the fight or flight response that occurs when an animal encounters a dangerous situation. One way to remember this is to think of the surprise a person feels when encountering a snake (snake and sympathetic both begin with s). Examples of functions controlled by the sympathetic nervous system include an accelerated heart rate and inhibited digestion. These functions help prepare an organisms body for the physical strain required to escape a potentially dangerous situation or to fend off a predator.
The sympathetic and parasympathetic nervous systems often have opposing effects on target organs.


Most preganglionic neurons in the sympathetic nervous system originate in the spinal cord, as illustrated in [link]. The axons of these neurons release acetylcholine on postganglionic neurons within sympathetic ganglia (the sympathetic ganglia form a chain that extends alongside the spinal cord). The acetylcholine activates the postganglionic neurons. Postganglionic neurons then release norepinephrine onto target organs. As anyone who has ever felt a rush before a big test, speech, or athletic event can attest, the effects of the sympathetic nervous system are quite pervasive. This is both because one preganglionic neuron synapses on multiple postganglionic neurons, amplifying the effect of the original synapse, and because the adrenal gland also releases norepinephrine (and the closely related hormone epinephrine) into the blood stream. The physiological effects of this norepinephrine release include dilating the trachea and bronchi (making it easier for the animal to breathe), increasing heart rate, and moving blood from the skin to the heart, muscles, and brain (so the animal can think and run). The strength and speed of the sympathetic response helps an organism avoid danger, and scientists have found evidence that it may also increase LTPallowing the animal to remember the dangerous situation and avoid it in the future.

Parasympathetic Nervous System
While the sympathetic nervous system is activated in stressful situations, the parasympathetic nervous system allows an animal to rest and digest. One way to remember this is to think that during a restful situation like a picnic, the parasympathetic nervous system is in control (picnic and parasympathetic both start with p). Parasympathetic preganglionic neurons have cell bodies located in the brainstem and in the sacral (toward the bottom) spinal cord, as shown in [link]. The axons of the preganglionic neurons release acetylcholine on the postganglionic neurons, which are generally located very near the target organs. Most postganglionic neurons release acetylcholine onto target organs, although some release nitric oxide.
The parasympathetic nervous system resets organ function after the sympathetic nervous system is activated (the common adrenaline dump you feel after a fight-or-flight event). Effects of acetylcholine release on target organs include slowing of heart rate, lowered blood pressure, and stimulation of digestion.


Sensory-Somatic Nervous System
The sensory-somatic nervous system is made up of cranial and spinal nerves and contains both sensory and motor neurons. Sensory neurons transmit sensory information from the skin, skeletal muscle, and sensory organs to the CNS. Motor neurons transmit messages about desired movement from the CNS to the muscles to make them contract. Without its sensory-somatic nervous system, an animal would be unable to process any information about its environment (what it sees, feels, hears, and so on) and could not control motor movements. Unlike the autonomic nervous system, which has two synapses between the CNS and the target organ, sensory and motor neurons have only one synapseone ending of the neuron is at the organ and the other directly contacts a CNS neuron. Acetylcholine is the main neurotransmitter released at these synapses.
Humans have 12 cranial nerves, nerves that emerge from or enter the skull (cranium), as opposed to the spinal nerves, which emerge from the vertebral column. Each cranial nerve is accorded a name, which are detailed in [link]. Some cranial nerves transmit only sensory information. For example, the olfactory nerve transmits information about smells from the nose to the brainstem. Other cranial nerves transmit almost solely motor information. For example, the oculomotor nerve controls the opening and closing of the eyelid and some eye movements. Other cranial nerves contain a mix of sensory and motor fibers. For example, the glossopharyngeal nerve has a role in both taste (sensory) and swallowing (motor).
The human brain contains 12 cranial nerves that receive sensory input and control motor output for the head and neck.


Spinal nerves transmit sensory and motor information between the spinal cord and the rest of the body. Each of the 31 spinal nerves (in humans) contains both sensory and motor axons. The sensory neuron cell bodies are grouped in structures called dorsal root ganglia and are shown in [link]. Each sensory neuron has one projectionwith a sensory receptor ending in skin, muscle, or sensory organsand another that synapses with a neuron in the dorsal spinal cord. Motor neurons have cell bodies in the ventral gray matter of the spinal cord that project to muscle through the ventral root. These neurons are usually stimulated by interneurons within the spinal cord but are sometimes directly stimulated by sensory neurons.
Spinal nerves contain both sensory and motor axons. The somas of sensory neurons are located in dorsal root ganglia. The somas of motor neurons are found in the ventral portion of the gray matter of the spinal cord.



Section SummaryThe peripheral nervous system contains both the autonomic and sensory-somatic nervous systems. The autonomic nervous system provides unconscious control over visceral functions and has two divisions: the sympathetic and parasympathetic nervous systems. The sympathetic nervous system is activated in stressful situations to prepare the animal for a fight or flight response. The parasympathetic nervous system is active during restful periods. The sensory-somatic nervous system is made of cranial and spinal nerves that transmit sensory information from skin and muscle to the CNS and motor commands from the CNS to the muscles.

Art Connections


[link] Which of the following statements is false?

The parasympathetic pathway is responsible for relaxing the body, while the sympathetic pathway is responsible for preparing for an emergency.
Most preganglionic neurons in the sympathetic pathway originate in the spinal cord.
Slowing of the heartbeat is a parasympathetic response.
Parasympathetic neurons are responsible for releasing norepinephrine on the target organ, while sympathetic neurons are responsible for releasing acetylcholine.



[link] D



Review Questions
Activation of the sympathetic nervous system causes:

increased blood flow into the skin
a decreased heart rate
an increased heart rate
increased digestion



C



Where are parasympathetic preganglionic cell bodies located?
cerebellum
brainstem
dorsal root ganglia
skin


B



________ is released by motor nerve endings onto muscle.

Acetylcholine
Norepinephrine
Dopamine
Serotonin



A



Free Response

What are the main differences between the sympathetic and parasympathetic branches of the autonomic nervous system?

The sympathetic nervous system prepares the body for fight or flight, whereas the parasympathetic nervous system allows the body to rest and digest. Sympathetic neurons release norepinephrine onto target organs; parasympathetic neurons release acetylcholine. Sympathetic neuron cell bodies are located in sympathetic ganglia. Parasympathetic neuron cell bodies are located in the brainstem and sacral spinal cord. Activation of the sympathetic nervous system increases heart rate and blood pressure and decreases digestion and blood flow to the skin. Activation of the parasympathetic nervous system decreases heart rate and blood pressure and increases digestion and blood flow to the skin.



What are the main functions of the sensory-somatic nervous system?

The sensory-somatic nervous system transmits sensory information from the skin, muscles, and sensory organs to the CNS. It also sends motor commands from the CNS to the muscles, causing them to contract.



Glossary
acetylcholine neurotransmitter released by neurons in the central nervous system and peripheral nervous system
autonomic nervous system part of the peripheral nervous system that controls bodily functions
cranial nerve sensory and/or motor nerve that emanates from the brain
norepinephrine neurotransmitter and hormone released by activation of the sympathetic nervous system
parasympathetic nervous system division of autonomic nervous system that regulates visceral functions during rest and digestion
sensory-somatic nervous system system of sensory and motor nerves
spinal nerve nerve projecting between skin or muscle and spinal cord
sympathetic nervous system division of autonomic nervous system activated during stressful fight or flight situations

