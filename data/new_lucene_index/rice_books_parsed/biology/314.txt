
Introduction
Introduction
        class="introduction"
      
class="summary" title="Sections Summary"
class="art-exercise" title="Art Connections"
class="multiple-choice" title="Multiple Choice"
class="free-response" title="Free Response"
Seedless plants, like these horsetails (Equisetum sp.), thrive in damp, shaded environments under a tree canopy where dryness is rare. (credit: modification of work by Jerry Kirkhart)


An incredible variety of seedless plants populates the terrestrial landscape. Mosses may grow on a tree trunk, and horsetails may display their jointed stems and spindly leaves across the forest floor. Today, seedless plants represent only a small fraction of the plants in our environment; yet, three hundred million years ago, seedless plants dominated the landscape and grew in the enormous swampy forests of the Carboniferous period. Their decomposition created large deposits of coal that we mine today.
Current evolutionary thought holds that all plantsgreen algae as well as land dwellersare monophyletic; that is, they are descendants of a single common ancestor. The evolutionary transition from water to land imposed severe constraints on plants. They had to develop strategies to avoid drying out, to disperse reproductive cells in air, for structural support, and for capturing and filtering sunlight. While seed plants developed adaptations that allowed them to populate even the most arid habitats on Earth, full independence from water did not happen in all plants. Most seedless plants still require a moist environment.
