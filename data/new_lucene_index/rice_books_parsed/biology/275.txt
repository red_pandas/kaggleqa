
Introduction
Introduction
        class="introduction"
      
class="summary" title="Sections Summary"
class="art-exercise" title="Art Connections"
class="multiple-choice" title="Multiple Choice"
class="free-response" title="Free Response"
Living things may be single-celled or complex, multicellular organisms. They may be plants, animals, fungi, bacteria, or archaea. This diversity results from evolution. (credit "wolf": modification of work by Gary Kramer; credit "coral": modification of work by William Harrigan, NOAA; credit "river": modification of work by Vojtch Dostl; credit "fish" modification of work by Christian Mehlfhrer; credit "mushroom": modification of work by Cory Zanker; credit "tree": modification of work by Joseph Kranak; credit "bee": modification of work by Cory Zanker)


All life on Earth is related. Evolutionary theory states that humans, beetles, plants, and bacteria all share a common ancestor, but that millions of years of evolution have shaped each of these organisms into the forms seen today. Scientists consider evolution a key concept to understanding life. Natural selection is one of the most dominant evolutionary forces. Natural selection acts to promote traits and behaviors that increase an organisms chances of survival and reproduction, while eliminating those traits and behaviors that are to the organisms detriment. But natural selection can only, as its name implies, selectit cannot create. The introduction of novel traits and behaviors falls on the shoulders of another evolutionary forcemutation. Mutation and other sources of variation among individuals, as well as the evolutionary forces that act upon them, alter populations and species. This combination of processes has led to the world of life we see today.

