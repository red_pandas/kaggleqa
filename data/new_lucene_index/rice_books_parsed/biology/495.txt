
Structure of Prokaryotes
Structure of ProkaryotesBy the end of this section, you will be able to:

Describe the basic structure of a typical prokaryote
Describe important differences in structure between Archaea and Bacteria

There are many differences between prokaryotic and eukaryotic cells. However, all cells have four common structures: the plasma membrane, which functions as a barrier for the cell and separates the cell from its environment; the cytoplasm, a jelly-like substance inside the cell; nucleic acids, the genetic material of the cell; and ribosomes, where protein synthesis takes place. Prokaryotes come in various shapes, but many fall into three categories: cocci (spherical), bacilli (rod-shaped), and spirilli (spiral-shaped) ([link]).
Prokaryotes fall into three basic categories based on their shape, visualized here using scanning electron microscopy: (a) cocci, or spherical (a pair is shown); (b) bacilli, or rod-shaped; and (c) spirilli, or spiral-shaped. (credit a: modification of work by Janice Haney Carr, Dr. Richard Facklam, CDC; credit c: modification of work by Dr. David Cox; scale-bar data from Matt Russell)




The Prokaryotic Cell
Recall that prokaryotes ([link]) are unicellular organisms that lack organelles or other internal membrane-bound structures. Therefore, they do not have a nucleus but instead generally have a single chromosomea piece of circular, double-stranded DNA located in an area of the cell called the nucleoid. Most prokaryotes have a cell wall outside the plasma membrane.
The features of a typical prokaryotic cell are shown.




Recall that prokaryotes are divided into two different domains, Bacteria and Archaea, which together with Eukarya, comprise the three domains of life ([link]).
Bacteria and Archaea are both prokaryotes but differ enough to be placed in separate domains. An ancestor of modern Archaea is believed to have given rise to Eukarya, the third domain of life. Archaeal and bacterial phyla are shown; the evolutionary relationship between these phyla is still open to debate.




The composition of the cell wall differs significantly between the domains Bacteria and Archaea. The composition of their cell walls also differs from the eukaryotic cell walls found in plants (cellulose) or fungi and insects (chitin). The cell wall functions as a protective layer, and it is responsible for the organisms shape. Some bacteria have an outer capsule outside the cell wall. Other structures are present in some prokaryotic species, but not in others ([link]). For example, the capsule found in some species enables the organism to attach to surfaces, protects it from dehydration and attack by phagocytic cells, and makes pathogens more resistant to our immune responses. Some species also have flagella (singular, flagellum) used for locomotion, and pili (singular, pilus) used for attachment to surfaces. Plasmids, which consist of extra-chromosomal DNA, are also present in many species of bacteria and archaea.
Characteristics of phyla of Bacteria are described in [link] and [link]; Archaea are described in [link].Phylum Proteobacteria is one of up to 52 bacteria phyla. Proteobacteria is further subdivided into five classes, Alpha through Epsilon. (credit Rickettsia rickettsia: modification of work by CDC; credit Spirillum minus: modification of work by Wolframm Adlassnig; credit Vibrio cholera: modification of work by Janice Haney Carr, CDC; credit Desulfovibrio vulgaris: modification of work by Graham Bradley; credit Campylobacter: modification of work by De Wood, Pooley, USDA, ARS, EMU; scale-bar data from Matt Russell)


Chlamydia, Spirochetes, Cyanobacteria, and Gram-positive bacteria are described in this table. Note that bacterial shape is not phylum-dependent; bacteria within a phylum may be cocci, rod-shaped, or spiral. (credit Chlamydia trachomatis: modification of work by Dr. Lance Liotta Laboratory, NCI; credit Treponema pallidum: modification of work by Dr. David Cox, CDC; credit Phormidium: modification of work by USGS; credit Clostridium difficile: modification of work by Lois S. Wiggs, CDC; scale-bar data from Matt Russell)


Archaea are separated into four phyla: the Korarchaeota, Euryarchaeota, Crenarchaeota, and Nanoarchaeota. (credit Halobacterium: modification of work by NASA; credit Nanoarchaeotum equitans: modification of work by Karl O. Stetter; credit korarchaeota: modification of work by Office of Science of the U.S. Dept. of Energy; scale-bar data from Matt Russell)


The Plasma Membrane
The plasma membrane is a thin lipid bilayer (6 to 8 nanometers) that completely surrounds the cell and separates the inside from the outside. Its selectively permeable nature keeps ions, proteins, and other molecules within the cell and prevents them from diffusing into the extracellular environment, while other molecules may move through the membrane. Recall that the general structure of a cell membrane is a phospholipid bilayer composed of two layers of lipid molecules. In archaeal cell membranes, isoprene (phytanyl) chains linked to glycerol replace the fatty acids linked to glycerol in bacterial membranes. Some archaeal membranes are lipid monolayers instead of bilayers ([link]).
Archaeal phospholipids differ from those found in Bacteria and Eukarya in two ways. First, they have branched phytanyl sidechains instead of linear ones. Second, an ether bond instead of an ester bond connects the lipid to the glycerol.





The Cell Wall
The cytoplasm of prokaryotic cells has a high concentration of dissolved solutes. Therefore, the osmotic pressure within the cell is relatively high. The cell wall is a protective layer that surrounds some cells and gives them shape and rigidity. It is located outside the cell membrane and prevents osmotic lysis (bursting due to increasing volume). The chemical composition of the cell walls varies between archaea and bacteria, and also varies between bacterial species.
Bacterial cell walls contain peptidoglycan, composed of polysaccharide chains that are cross-linked by unusual peptides containing both L- and D-amino acids including D-glutamic acid and D-alanine. Proteins normally have only L-amino acids; as a consequence, many of our antibiotics work by mimicking D-amino acids and therefore have specific effects on bacterial cell wall development. There are more than 100 different forms of peptidoglycan. S-layer (surface layer) proteins are also present on the outside of cell walls of both archaea and bacteria.
Bacteria are divided into two major groups: Gram positive and Gram negative, based on their reaction to Gram staining. Note that all Gram-positive bacteria belong to one phylum; bacteria in the other phyla (Proteobacteria, Chlamydias, Spirochetes, Cyanobacteria, and others) are Gram-negative. The Gram staining method is named after its inventor, Danish scientist Hans Christian Gram (18531938). The different bacterial responses to the staining procedure are ultimately due to cell wall structure. Gram-positive organisms typically lack the outer membrane found in Gram-negative organisms ([link]). Up to 90 percent of the cell wall in Gram-positive bacteria is composed of peptidoglycan, and most of the rest is composed of acidic substances called teichoic acids. Teichoic acids may be covalently linked to lipids in the plasma membrane to form lipoteichoic acids. Lipoteichoic acids anchor the cell wall to the cell membrane. Gram-negative bacteria have a relatively thin cell wall composed of a few layers of peptidoglycan (only 10 percent of the total cell wall), surrounded by an outer envelope containing lipopolysaccharides (LPS) and lipoproteins. This outer envelope is sometimes referred to as a second lipid bilayer. The chemistry of this outer envelope is very different, however, from that of the typical lipid bilayer that forms plasma membranes.Art Connection
Bacteria are divided into two major groups: Gram positive and Gram negative. Both groups have a cell wall composed of peptidoglycan: in Gram-positive bacteria, the wall is thick, whereas in Gram-negative bacteria, the wall is thin. In Gram-negative bacteria, the cell wall is surrounded by an outer membrane that contains lipopolysaccharides and lipoproteins. Porins are proteins in this cell membrane that allow substances to pass through the outer membrane of Gram-negative bacteria. In Gram-positive bacteria, lipoteichoic acid anchors the cell wall to the cell membrane. (credit: modification of work by "Franciscosp2"/Wikimedia Commons)


Which of the following statements is true?
Gram-positive bacteria have a single cell wall anchored to the cell membrane by lipoteichoic acid.
Porins allow entry of substances into both Gram-positive and Gram-negative bacteria.
The cell wall of Gram-negative bacteria is thick, and the cell wall of Gram-positive bacteria is thin.
Gram-negative bacteria have a cell wall made of peptidoglycan, whereas Gram-positive bacteria have a cell wall made of lipoteichoic acid.

Archaean cell walls do not have peptidoglycan. There are four different types of Archaean cell walls. One type is composed of pseudopeptidoglycan, which is similar to peptidoglycan in morphology but contains different sugars in the polysaccharide chain. The other three types of cell walls are composed of polysaccharides, glycoproteins, or pure protein.


Structural Differences and Similarities between Bacteria and Archaea


Structural Characteristic
Bacteria
Archaea



Cell type
Prokaryotic
Prokaryotic


Cell morphology
Variable
Variable


Cell wall
Contains peptidoglycan
Does not contain peptidoglycan


Cell membrane type
Lipid bilayer
Lipid bilayer or lipid monolayer


Plasma membrane lipids
Fatty acids
Phytanyl groups




Reproduction
Reproduction in prokaryotes is asexual and usually takes place by binary fission. Recall that the DNA of a prokaryote exists as a single, circular chromosome. Prokaryotes do not undergo mitosis. Rather the chromosome is replicated and the two resulting copies separate from one another, due to the growth of the cell. The prokaryote, now enlarged, is pinched inward at its equator and the two resulting cells, which are clones, separate. Binary fission does not provide an opportunity for genetic recombination or genetic diversity, but prokaryotes can share genes by three other mechanisms.
In transformation, the prokaryote takes in DNA found in its environment that is shed by other prokaryotes. If a nonpathogenic bacterium takes up DNA for a toxin gene from a pathogen and incorporates the new DNA into its own chromosome, it too may become pathogenic. In transduction, bacteriophages, the viruses that infect bacteria, sometimes also move short pieces of chromosomal DNA from one bacterium to another. Transduction results in a recombinant organism. Archaea are not affected by bacteriophages but instead have their own viruses that translocate genetic material from one individual to another. In conjugation, DNA is transferred from one prokaryote to another by means of a pilus, which brings the organisms into contact with one another. The DNA transferred can be in the form of a plasmid or as a hybrid, containing both plasmid and chromosomal DNA. These three processes of DNA exchange are shown in [link].Reproduction can be very rapid: a few minutes for some species. This short generation time coupled with mechanisms of genetic recombination and high rates of mutation result in the rapid evolution of prokaryotes, allowing them to respond to environmental changes (such as the introduction of an antibiotic) very quickly.
Besides binary fission, there are three other mechanisms by which prokaryotes can exchange DNA. In (a) transformation, the cell takes up prokaryotic DNA directly from the environment. The DNA may remain separate as plasmid DNA or be incorporated into the host genome. In (b) transduction, a bacteriophage injects DNA into the cell that contains a small fragment of DNA from a different prokaryote. In (c) conjugation, DNA is transferred from one cell to another via a mating bridge that connects the two cells after the sex pilus draws the two bacteria close enough to form the bridge.


Evolution Connection
The Evolution of ProkaryotesHow do scientists answer questions about the evolution of prokaryotes? Unlike with animals, artifacts in the fossil record of prokaryotes offer very little information. Fossils of ancient prokaryotes look like tiny bubbles in rock. Some scientists turn to genetics and to the principle of the molecular clock, which holds that the more recently two species have diverged, the more similar their genes (and thus proteins) will be. Conversely, species that diverged long ago will have more genes that are dissimilar.
Scientists at the NASA Astrobiology Institute and at the European Molecular Biology Laboratory collaborated to analyze the molecular evolution of 32 specific proteins common to 72 species of prokaryotes.1 The model they derived from their data indicates that three important groups of bacteriaActinobacteria, Deinococcus, and Cyanobacteria (which the authors call Terrabacteria)were the first to colonize land. (Recall that Deinococcus is a genus of prokaryotea bacteriumthat is highly resistant to ionizing radiation.) Cyanobacteria are photosynthesizers, while Actinobacteria are a group of very common bacteria that include species important in decomposition of organic wastes.The timelines of divergence suggest that bacteria (members of the domain Bacteria) diverged from common ancestral species between 2.5 and 3.2 billion years ago, whereas archaea diverged earlier: between 3.1 and 4.1 billion years ago. Eukarya later diverged off the Archaean line. The work further suggests that stromatolites that formed prior to the advent of cyanobacteria (about 2.6 billion years ago) photosynthesized in an anoxic environment and that because of the modifications of the Terrabacteria for land (resistance to drying and the possession of compounds that protect the organism from excess light), photosynthesis using oxygen may be closely linked to adaptations to survive on land.

Section SummaryProkaryotes (domains Archaea and Bacteria) are single-celled organisms lacking a nucleus. They have a single piece of circular DNA in the nucleoid area of the cell. Most prokaryotes have a cell wall that lies outside the boundary of the plasma membrane. Some prokaryotes may have additional structures such as a capsule, flagella, and pili. Bacteria and Archaea differ in the lipid composition of their cell membranes and the characteristics of the cell wall. In archaeal membranes, phytanyl units, rather than fatty acids, are linked to glycerol. Some archaeal membranes are lipid monolayers instead of bilayers.
The cell wall is located outside the cell membrane and prevents osmotic lysis. The chemical composition of cell walls varies between species. Bacterial cell walls contain peptidoglycan. Archaean cell walls do not have peptidoglycan, but they may have pseudopeptidoglycan, polysaccharides, glycoproteins, or protein-based cell walls. Bacteria can be divided into two major groups: Gram positive and Gram negative, based on the Gram stain reaction. Gram-positive organisms have a thick cell wall, together with teichoic acids. Gram-negative organisms have a thin cell wall and an outer envelope containing lipopolysaccharides and lipoproteins.

Art Connections


[link] Which of the following statements is true?
Gram-positive bacteria have a single cell wall anchored to the cell membrane by lipoteichoic acid.
Porins allow entry of substances into both Gram-positive and Gram-negative bacteria.
The cell wall of Gram-negative bacteria is thick, and the cell wall of Gram-positive bacteria is thin.
Gram-negative bacteria have a cell wall made of peptidoglycan, whereas Gram-positive bacteria have a cell wall made of lipoteichoic acid.


[link] A



Review Questions

The presence of a membrane-enclosed nucleus is a characteristic of ________.

prokaryotic cells
eukaryotic cells
all cells
viruses



B




Which of the following consist of prokaryotic cells?

bacteria and fungi
archaea and fungi
protists and animals
bacteria and archaea



D




The cell wall is ________.

interior to the cell membrane
exterior to the cell membrane
a part of the cell membrane
interior or exterior, depending on the particular cell



B




Organisms most likely to be found in extreme environments are ________.

fungi
bacteria
viruses
archaea



B




Prokaryotes stain as Gram-positive or Gram-negative because of differences in the cell _______.

wall
cytoplasm
nucleus
chromosome



A




Pseudopeptidoglycan is a characteristic of the walls of ________.

eukaryotic cells
bacterial prokaryotic cells
archaean prokaryotic cells
bacterial and archaean prokaryotic cells



C




The lipopolysaccharide layer (LPS) is a characteristic of the wall of ________.

archaean cells
Gram-negative bacteria
bacterial prokaryotic cells
eukaryotic cells



B



Free Response


Mention three differences between bacteria and archaea.


Responses will vary. A possible answer is: Bacteria contain peptidoglycan in the cell wall; archaea do not. The cell membrane in bacteria is a lipid bilayer; in archaea, it can be a lipid bilayer or a monolayer. Bacteria contain fatty acids on the cell membrane, whereas archaea contain phytanyl.




Explain the statement that both types, bacteria and archaea, have the same basic structures, but built from different chemical components.


Both bacteria and archaea have cell membranes and they both contain a hydrophobic portion. In the case of bacteria, it is a fatty acid; in the case of archaea, it is a hydrocarbon (phytanyl). Both bacteria and archaea have a cell wall that protects them. In the case of bacteria, it is composed of peptidoglycan, whereas in the case of archaea, it is pseudopeptidoglycan, polysaccharides, glycoproteins, or pure protein. Bacterial and archaeal flagella also differ in their chemical structure.



Footnotes1 Battistuzzi, FU, Feijao, A, and Hedges, SB. A genomic timescale of prokaryote evolution: Insights into the origin of methanogenesis, phototrophy, and the colonization of land. BioMed Central: Evolutionary Biology 4 (2004): 44, doi:10.1186/1471-2148-4-44.Glossary

capsule external structure that enables a prokaryote to attach to surfaces and protects it from dehydration


conjugation process by which prokaryotes move DNA from one individual to another using a pilus


Gram negative bacterium whose cell wall contains little peptidoglycan but has an outer membrane


Gram positive bacterium that contains mainly peptidoglycan in its cell walls


peptidoglycan material composed of polysaccharide chains cross-linked to unusual peptides


pilus surface appendage of some prokaryotes used for attachment to surfaces including other prokaryotes


pseudopeptidoglycan component of archaea cell walls that is similar to peptidoglycan in morphology but contains different sugars


S-layer surface-layer protein present on the outside of cell walls of archaea and bacteria


teichoic acid polymer associated with the cell wall of Gram-positive bacteria


transduction process by which a bacteriophage moves DNA from one prokaryote to another


transformation process by which a prokaryote takes in DNA found in its environment that is shed by other prokaryotes


