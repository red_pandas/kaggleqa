
Biotechnology
BiotechnologyBy the end of this section, you will be able to:

Describe gel electrophoresis
Explain molecular and reproductive cloning
Describe uses of biotechnology in medicine and agriculture

Biotechnology is the use of biological agents for technological advancement. Biotechnology was used for breeding livestock and crops long before the scientific basis of these techniques was understood. Since the discovery of the structure of DNA in 1953, the field of biotechnology has grown rapidly through both academic research and private companies. The primary applications of this technology are in medicine (production of vaccines and antibiotics) and agriculture (genetic modification of crops, such as to increase yields). Biotechnology also has many industrial applications, such as fermentation, the treatment of oil spills, and the production of biofuels ([link]).
Antibiotics are chemicals produced by fungi, bacteria, and other organisms that have antimicrobial properties. The first antibiotic discovered was penicillin. Antibiotics are now commercially produced and tested for their potential to inhibit bacterial growth. (credit "advertisement": modification of work by NIH; credit "test plate": modification of work by Don Stalons/CDC; scale-bar data from Matt Russell)


Basic Techniques to Manipulate Genetic Material (DNA and RNA)
To understand the basic techniques used to work with nucleic acids, remember that nucleic acids are macromolecules made of nucleotides (a sugar, a phosphate, and a nitrogenous base) linked by phosphodiester bonds. The phosphate groups on these molecules each have a net negative charge. An entire set of DNA molecules in the nucleus is called the genome. DNA has two complementary strands linked by hydrogen bonds between the paired bases. The two strands can be separated by exposure to high temperatures (DNA denaturation) and can be reannealed by cooling. The DNA can be replicated by the DNA polymerase enzyme. Unlike DNA, which is located in the nucleus of eukaryotic cells, RNA molecules leave the nucleus. The most common type of RNA that is analyzed is the messenger RNA (mRNA) because it represents the protein-coding genes that are actively expressed. However, RNA molecules present some other challenges to analysis, as they are often less stable than DNA.DNA and RNA Extraction
To study or manipulate nucleic acids, the DNA or RNA must first be isolated or extracted from the cells. Various techniques are used to extract different types of DNA ([link]). Most nucleic acid extraction techniques involve steps to break open the cell and use enzymatic reactions to destroy all macromolecules that are not desired (such as degradation of unwanted molecules and separation from the DNA sample). Cells are broken using a lysis buffer (a solution which is mostly a detergent); lysis means to split. These enzymes break apart lipid molecules in the cell membranes and nuclear membranes. Macromolecules are inactivated using enzymes such as proteases that break down proteins, and ribonucleases (RNAses) that break down RNA. The DNA is then precipitated using alcohol. Human genomic DNA is usually visible as a gelatinous, white mass. The DNA samples can be stored frozen at 80C for several years.
This diagram shows the basic method used for extraction of DNA.


RNA analysis is performed to study gene expression patterns in cells. RNA is naturally very unstable because RNAses are commonly present in nature and very difficult to inactivate. Similar to DNA, RNA extraction involves the use of various buffers and enzymes to inactivate macromolecules and preserve the RNA.

Gel Electrophoresis
Because nucleic acids are negatively charged ions at neutral or basic pH in an aqueous environment, they can be mobilized by an electric field. Gel electrophoresis is a technique used to separate molecules on the basis of size, using this charge. The nucleic acids can be separated as whole chromosomes or fragments. The nucleic acids are loaded into a slot near the negative electrode of a semisolid, porous gel matrix and pulled toward the positive electrode at the opposite end of the gel. Smaller molecules move through the pores in the gel faster than larger molecules; this difference in the rate of migration separates the fragments on the basis of size. There are molecular weight standard samples that can be run alongside the molecules to provide a size comparison. Nucleic acids in a gel matrix can be observed using various fluorescent or colored dyes. Distinct nucleic acid fragments appear as bands at specific distances from the top of the gel (the negative electrode end) on the basis of their size ([link]). A mixture of genomic DNA fragments of varying sizes appear as a long smear, whereas uncut genomic DNA is usually too large to run through the gel and forms a single large band at the top of the gel.
Shown are DNA fragments from seven samples run on a gel, stained with a fluorescent dye, and viewed under UV light. (credit: James Jacob, Tompkins Cortland Community College)



Amplification of Nucleic Acid Fragments by Polymerase Chain Reaction
Although genomic DNA is visible to the naked eye when it is extracted in bulk, DNA analysis often requires focusing on one or more specific regions of the genome. Polymerase chain reaction (PCR) is a technique used to amplify specific regions of DNA for further analysis ([link]). PCR is used for many purposes in laboratories, such as the cloning of gene fragments to analyze genetic diseases, identification of contaminant foreign DNA in a sample, and the amplification of DNA for sequencing. More practical applications include the determination of paternity and detection of genetic diseases.
Polymerase chain reaction, or PCR, is used to amplify a specific sequence of DNA.  Primersshort pieces of DNA complementary to each end of the target sequenceare combined with genomic DNA, Taq polymerase, and deoxynucleotides. Taq polymerase is a DNA polymerase isolated from the thermostable bacterium Thermus aquaticus that is able to withstand the high temperatures used in PCR. Thermus aquaticus grows in the Lower Geyser Basin of Yellowstone National Park. Reverse transcriptase PCR (RT-PCR) is similar to PCR, but cDNA is made from an RNA template before PCR begins.


DNA fragments can also be amplified from an RNA template in a process called reverse transcriptase PCR (RT-PCR).  The first step is to recreate the original DNA template strand (called cDNA) by applying DNA nucleotides to the mRNA. This process is called reverse transcription. This requires the presence of an enzyme called reverse transcriptase. After the cDNA is made, regular PCR can be used to amplify it.Link to Learning

 
Deepen your understanding of the polymerase chain reaction by clicking through this interactive exercise.
Hybridization, Southern Blotting, and Northern Blotting
Nucleic acid samples, such as fragmented genomic DNA and RNA extracts, can be probed for the presence of certain sequences. Short DNA fragments called probes are designed and labeled with radioactive or fluorescent dyes to aid detection. Gel electrophoresis separates the nucleic acid fragments according to their size. The fragments in the gel are then transferred onto a nylon membrane in a procedure called blotting ([link]). The nucleic acid fragments that are bound to the surface of the membrane can then be probed with specific radioactively or fluorescently labeled probe sequences. When DNA is transferred to a nylon membrane, the technique is called Southern blotting, and when RNA is transferred to a nylon membrane, it is called northern blotting. Southern blots are used to detect the presence of certain DNA sequences in a given genome, and northern blots are used to detect gene expression.
Southern blotting is used to find a particular sequence in a sample of DNA. DNA fragments are separated on a gel, transferred to a nylon membrane, and incubated with a DNA probe complementary to the sequence of interest.  Northern blotting is similar to Southern blotting, but RNA is run on the gel instead of DNA. In western blotting, proteins are run on a gel and detected using antibodies.




Molecular Cloning
In general, the word cloning means the creation of a perfect replica; however, in biology, the re-creation of a whole organism is referred to as reproductive cloning. Long before attempts were made to clone an entire organism, researchers learned how to reproduce desired regions or fragments of the genome, a process that is referred to as molecular cloning.
Cloning small fragments of the genome allows for the manipulation and study of specific genes (and their protein products), or noncoding regions in isolation. A plasmid (also called a vector) is a small circular DNA molecule that replicates independently of the chromosomal DNA. In cloning, the plasmid molecules can be used to provide a "folder" in which to insert a desired DNA fragment. Plasmids are usually introduced into a bacterial host for proliferation. In the bacterial context, the fragment of DNA from the human genome (or the genome of another organism that is being studied) is referred to as foreign DNA, or a transgene, to differentiate it from the DNA of the bacterium, which is called the host DNA.
Plasmids occur naturally in bacterial populations (such as Escherichia coli) and have genes that can contribute favorable traits to the organism, such as antibiotic resistance (the ability to be unaffected by antibiotics). Plasmids have been repurposed and engineered as vectors for molecular cloning and the large-scale production of important reagents, such as insulin and human growth hormone. An important feature of plasmid vectors is the ease with which a foreign DNA fragment can be introduced via the multiple cloning site (MCS). The MCS is a short DNA sequence containing multiple sites that can be cut with different commonly available restriction endonucleases. Restriction endonucleases recognize specific DNA sequences and cut them in a predictable manner; they are naturally produced by bacteria as a defense mechanism against foreign DNA. Many restriction endonucleases make staggered cuts in the two strands of DNA, such that the cut ends have a 2- or 4-base single-stranded overhang. Because these overhangs are capable of annealing with complementary overhangs, these are called sticky ends. Addition of an enzyme called DNA ligase permanently joins the DNA fragments via phosphodiester bonds. In this way, any DNA fragment generated by restriction endonuclease cleavage can be spliced between the two ends of a plasmid DNA that has been cut with the same restriction endonuclease ([link]).
Recombinant DNA Molecules
Plasmids with foreign DNA inserted into them are called recombinant DNA molecules because they are created artificially and do not occur in nature. They are also called chimeric molecules because the origin of different parts of the molecules can be traced back to different species of biological organisms or even to chemical synthesis. Proteins that are expressed from recombinant DNA molecules are called recombinant proteins. Not all recombinant plasmids are capable of expressing genes. The recombinant DNA may need to be moved into a different vector (or host) that is better designed for gene expression. Plasmids may also be engineered to express proteins only when stimulated by certain environmental factors, so that scientists can control the expression of the recombinant proteins.
Art Connection
This diagram shows the steps involved in molecular cloning.



You are working in a molecular biology lab and, unbeknownst to you, your lab partner left the foreign genomic DNA that you are planning to clone on the lab bench overnight instead of storing it in the freezer. As a result, it was degraded by nucleases, but still used in the experiment. The plasmid, on the other hand, is fine. What results would you expect from your molecular cloning experiment?
There will be no colonies on the bacterial plate.
There will be blue colonies only.
There will be blue and white colonies.
The will be white colonies only.
Link to Learning

 
View an animation of recombination in cloning from the DNA Learning Center.


Cellular Cloning
Unicellular organisms, such as bacteria and yeast, naturally produce clones of themselves when they replicate asexually by binary fission; this is known as cellular cloning. The nuclear DNA duplicates by the process of mitosis, which creates an exact replica of the genetic material.

Reproductive Cloning
Reproductive cloning is a method used to make a clone or an identical copy of an entire multicellular organism. Most multicellular organisms undergo reproduction by sexual means, which involves genetic hybridization of two individuals (parents), making it impossible for generation of an identical copy or a clone of either parent. Recent advances in biotechnology have made it possible to artificially induce asexual reproduction of mammals in the laboratory.
Parthenogenesis, or virgin birth, occurs when an embryo grows and develops without the fertilization of the egg occurring; this is a form of asexual reproduction. An example of parthenogenesis occurs in species in which the female lays an egg and if the egg is fertilized, it is a diploid egg and the individual develops into a female; if the egg is not fertilized, it remains a haploid egg and develops into a male. The unfertilized egg is called a parthenogenic, or virgin, egg. Some insects and reptiles lay parthenogenic eggs that can develop into adults.
Sexual reproduction requires two cells; when the haploid egg and sperm cells fuse, a diploid zygote results. The zygote nucleus contains the genetic information to produce a new individual. However, early embryonic development requires the cytoplasmic material contained in the egg cell. This idea forms the basis for reproductive cloning. Therefore, if the haploid nucleus of an egg cell is replaced with a diploid nucleus from the cell of any individual of the same species (called a donor), it will become a zygote that is genetically identical to the donor. Somatic cell nuclear transfer is the technique of transferring a diploid nucleus into an enucleated egg. It can be used for either therapeutic cloning or reproductive cloning.
The first cloned animal was Dolly, a sheep who was born in 1996. The success rate of reproductive cloning at the time was very low. Dolly lived for seven years and died of respiratory complications ([link]). There is speculation that because the cell DNA belongs to an older individual, the age of the DNA may affect the life expectancy of a cloned individual. Since Dolly, several animals such as horses, bulls, and goats have been successfully cloned, although these individuals often exhibit facial, limb, and cardiac abnormalities. There have been attempts at producing cloned human embryos as sources of embryonic stem cells, sometimes referred to as cloning for therapeutic purposes. Therapeutic cloning produces stem cells to attempt to remedy detrimental diseases or defects (unlike reproductive cloning, which aims to reproduce an organism). Still, therapeutic cloning efforts have met with resistance because of bioethical considerations.
Art Connection
Dolly the sheep was the first mammal to be cloned. To create Dolly, the nucleus was removed from a donor egg cell. The nucleus from a second sheep was then introduced into the cell, which was allowed to divide to the blastocyst stage before being implanted in a surrogate mother. (credit: modification of work by "Squidonius"/Wikimedia Commons)



Do you think Dolly was a Finn-Dorset or a Scottish Blackface sheep?

Genetic Engineering
Genetic engineering is the alteration of an organisms genotype using recombinant DNA technology to modify an organisms DNA to achieve desirable traits. The addition of foreign DNA in the form of recombinant DNA vectors generated by molecular cloning is the most common method of genetic engineering. The organism that receives the recombinant DNA is called a genetically modified organism (GMO). If the foreign DNA that is introduced comes from a different species, the host organism is called transgenic. Bacteria, plants, and animals have been genetically modified since the early 1970s for academic, medical, agricultural, and industrial purposes. In the US, GMOs such as Roundup-ready soybeans and borer-resistant corn are part of many common processed foods.
Gene Targeting
Although classical methods of studying the function of genes began with a given phenotype and determined the genetic basis of that phenotype, modern techniques allow researchers to start at the DNA sequence level and ask: "What does this gene or DNA element do?" This technique, called reverse genetics, has resulted in reversing the classic genetic methodology.  This method would be similar to damaging a body part to determine its function. An insect that loses a wing cannot fly, which means that the function of the wing is flight. The classical genetic method would compare insects that cannot fly with insects that can fly, and observe that the non-flying insects have lost wings. Similarly, mutating or deleting genes provides researchers with clues about gene function. The methods used to disable gene function are collectively called gene targeting. Gene targeting is the use of recombinant DNA vectors to alter the expression of a particular gene, either by introducing mutations in a gene, or by eliminating the expression of a certain gene by deleting a part or all of the gene sequence from the genome of an organism.


Biotechnology in Medicine and Agriculture
It is easy to see how biotechnology can be used for medicinal purposes. Knowledge of the genetic makeup of our species, the genetic basis of heritable diseases, and the invention of technology to manipulate and fix mutant genes provides methods to treat the disease. Biotechnology in agriculture can enhance resistance to disease, pest, and environmental stress, and improve both crop yield and quality.
Genetic Diagnosis and Gene Therapy
The process of testing for suspected genetic defects before administering treatment is called genetic diagnosis by genetic testing. Depending on the inheritance patterns of a disease-causing gene, family members are advised to undergo genetic testing. For example, women diagnosed with breast cancer are usually advised to have a biopsy so that the medical team can determine the genetic basis of cancer development. Treatment plans are based on the findings of genetic tests that determine the type of cancer. If the cancer is caused by inherited gene mutations, other female relatives are also advised to undergo genetic testing and periodic screening for breast cancer. Genetic testing is also offered for fetuses (or embryos with in vitro fertilization) to determine the presence or absence of disease-causing genes in families with specific debilitating diseases.
Gene therapy is a genetic engineering technique used to cure disease. In its simplest form, it involves the introduction of a good gene at a random location in the genome to aid the cure of a disease that is caused by a mutated gene. The good gene is usually introduced into diseased cells as part of a vector transmitted by a virus that can infect the host cell and deliver the foreign DNA ([link]). More advanced forms of gene therapy try to correct the mutation at the original site in the genome, such as is the case with treatment of severe combined immunodeficiency (SCID).
Gene therapy using an adenovirus vector can be used to cure certain genetic diseases in which a person has a defective gene. (credit: NIH)




Production of Vaccines, Antibiotics, and Hormones
Traditional vaccination strategies use weakened or inactive forms of microorganisms to mount the initial immune response. Modern techniques use the genes of microorganisms cloned into vectors to mass produce the desired antigen. The antigen is then introduced into the body to stimulate the primary immune response and trigger immune memory. Genes cloned from the influenza virus have been used to combat the constantly changing strains of this virus.
Antibiotics are a biotechnological product. They are naturally produced by microorganisms, such as fungi, to attain an advantage over bacterial populations. Antibiotics are produced on a large scale by cultivating and manipulating fungal cells.
Recombinant DNA technology was used to produce large-scale quantities of human insulin in E. coli as early as 1978. Previously, it was only possible to treat diabetes with pig insulin, which caused allergic reactions in humans because of differences in the gene product. In addition, human growth hormone (HGH) is used to treat growth disorders in children. The HGH gene was cloned from a cDNA library and inserted into E. coli cells by cloning it into a bacterial vector.

Transgenic Animals
Although several recombinant proteins used in medicine are successfully produced in bacteria, some proteins require a eukaryotic animal host for proper processing. For this reason, the desired genes are cloned and expressed in animals, such as sheep, goats, chickens, and mice. Animals that have been modified to express recombinant DNA are called transgenic animals. Several human proteins are expressed in the milk of transgenic sheep and goats, and some are expressed in the eggs of chickens. Mice have been used extensively for expressing and studying the effects of recombinant genes and mutations.

Transgenic Plants
Manipulating the DNA of plants (i.e., creating GMOs) has helped to create desirable traits, such as disease resistance, herbicide and pesticide resistance, better nutritional value, and better shelf-life ([link]). Plants are the most important source of food for the human population. Farmers developed ways to select for plant varieties with desirable traits long before modern-day biotechnology practices were established.
Corn, a major agricultural crop used to create products for a variety of industries, is often modified through plant biotechnology. (credit: Keith Weller, USDA)


Plants that have received recombinant DNA from other species are called transgenic plants. Because they are not natural, transgenic plants and other GMOs are closely monitored by government agencies to ensure that they are fit for human consumption and do not endanger other plant and animal life. Because foreign genes can spread to other species in the environment, extensive testing is required to ensure ecological stability. Staples like corn, potatoes, and tomatoes were the first crop plants to be genetically engineered.
Transformation of Plants Using Agrobacterium tumefaciens
Gene transfer occurs naturally between species in microbial populations. Many viruses that cause human diseases, such as cancer, act by incorporating their DNA into the human genome. In plants, tumors caused by the bacterium Agrobacterium tumefaciens occur by transfer of DNA from the bacterium to the plant. Although the tumors do not kill the plants, they make the plants stunted and more susceptible to harsh environmental conditions. Many plants, such as walnuts, grapes, nut trees, and beets, are affected by A. tumefaciens. The artificial introduction of DNA into plant cells is more challenging than in animal cells because of the thick plant cell wall.
Researchers used the natural transfer of DNA from Agrobacterium to a plant host to introduce DNA fragments of their choice into plant hosts. In nature, the disease-causing A. tumefaciens have a set of plasmids, called the Ti plasmids (tumor-inducing plasmids), that contain genes for the production of tumors in plants. DNA from the Ti plasmid integrates into the infected plant cells genome. Researchers manipulate the Ti plasmids to remove the tumor-causing genes and insert the desired DNA fragment for transfer into the plant genome.  The Ti plasmids carry antibiotic resistance genes to aid selection and can be propagated in E. coli cells as well.

The Organic Insecticide Bacillus thuringiensis
Bacillus thuringiensis (Bt) is a bacterium that produces protein crystals during sporulation that are toxic to many insect species that affect plants. Bt toxin has to be ingested by insects for the toxin to be activated. Insects that have eaten Bt toxin stop feeding on the plants within a few hours. After the toxin is activated in the intestines of the insects, death occurs within a couple of days. Modern biotechnology has allowed plants to encode their own crystal Bt toxin that acts against insects. The crystal toxin genes have been cloned from Bt and introduced into plants. Bt toxin has been found to be safe for the environment, non-toxic to humans and other mammals, and is approved for use by organic farmers as a natural insecticide.

Flavr Savr Tomato
The first GM crop to be introduced into the market was the Flavr Savr Tomato produced in 1994. Antisense RNA technology was used to slow down the process of softening and rotting caused by fungal infections, which led to increased shelf life of the GM tomatoes. Additional genetic modification improved the flavor of this tomato. The Flavr Savr tomato did not successfully stay in the market because of problems maintaining and shipping the crop.


Section SummaryNucleic acids can be isolated from cells for the purposes of further analysis by breaking open the cells and enzymatically destroying all other major macromolecules. Fragmented or whole chromosomes can be separated on the basis of size by gel electrophoresis. Short stretches of DNA or RNA can be amplified by PCR. Southern and northern blotting can be used to detect the presence of specific short sequences in a DNA or RNA sample. The term cloning may refer to cloning small DNA fragments (molecular cloning), cloning cell populations (cellular cloning), or cloning entire organisms (reproductive cloning). Genetic testing is performed to identify disease-causing genes, and gene therapy is used to cure an inheritable disease.
Transgenic organisms possess DNA from a different species, usually generated by molecular cloning techniques. Vaccines, antibiotics, and hormones are examples of products obtained by recombinant DNA technology. Transgenic plants are usually created to improve characteristics of crop plants.

Art Connections

[link] You are working in a molecular biology lab and, unbeknownst to you, your lab partner left the foreign genomic DNA that you are planning to clone on the lab bench overnight instead of storing it in the freezer. As a result, it was degraded by nucleases, but still used in the experiment. The plasmid, on the other hand, is fine. What results would you expect from your molecular cloning experiment?

There will be no colonies on the bacterial plate.
 There will be blue colonies only.
 There will be blue and white colonies.
 The will be white colonies only.



[link] B. The experiment would result in blue colonies only.


[link] Do you think Dolly was a Finn-Dorset or a Scottish Blackface sheep?


[link] Dolly was a Finn-Dorset sheep because even though the original cell came from a Scottish blackface sheep and the surrogate mother was a Scottish blackface, the DNA came from a Finn-Dorset.


Review Questions
GMOs are created by ________.

generating genomic DNA fragments with restriction endonucleases
introducing recombinant DNA into an organism by any means
overexpressing proteins in E. coli.
all of the above


 B 


Gene therapy can be used to introduce foreign DNA into cells ________.

for molecular cloning
by PCR
of tissues to cure inheritable disease
all of the above


 C 


Insulin produced by molecular cloning:

is of pig origin
is a recombinant protein
is made by the human pancreas
is recombinant DNA


 B 


Bt toxin is considered to be ________.

a gene for modifying insect DNA
an organic insecticide produced by bacteria
useful for humans to fight against insects
a recombinant protein


 B 


The Flavr Savr Tomato:

is a variety of vine-ripened tomato in the supermarket
was created to have better flavor and shelf-life
does not undergo soft rot
all of the above


 D 


Free Response

Describe the process of Southern blotting.

Southern blotting is the transfer of DNA that has been enzymatically cut into fragments and run on an agarose gel onto a nylon membrane. The DNA fragments that are on the nylon membrane can be denatured to make them single-stranded, and then probed with small DNA fragments that are radioactively or fluorescently labeled, to detect the presence of specific sequences. An example of the use of Southern blotting would be in analyzing the presence, absence, or variation of a disease gene in genomic DNA from a group of patients.



A researcher wants to study cancer cells from a patient with breast cancer. Is cloning the cancer cells an option?

Cellular cloning of the breast cancer cells will establish a cell line, which can be used for further analysis


How would a scientist introduce a gene for herbicide resistance into a plant?

By identifying an herbicide resistance gene and cloning it into a plant expression vector system, like the Ti plasmid system from Agrobacterium tumefaciens. The scientist would then introduce it into the plant cells by transformation, and select cells that have taken up and integrated the herbicide-resistance gene into the genome.



If you had a chance to get your genome sequenced, what are some questions you might be able to have answered about yourself?

What diseases am I prone to and what precautions should I take? Am I a carrier for any disease-causing genes that may be passed on to children?


Glossary
antibiotic resistance ability of an organism to be unaffected by the actions of an antibiotic
biotechnology use of biological agents for technological advancement
cellular cloning production of identical cell populations by binary fission
clone exact replica
foreign DNA DNA that belongs to a different species or DNA that is artificially synthesized
gel electrophoresis technique used to separate molecules on the basis of size using electric charge
gene targeting method for altering the sequence of a specific gene by introducing the modified version on a vector
gene therapy technique used to cure inheritable diseases by replacing mutant genes with good genes
genetic diagnosis diagnosis of the potential for disease development by analyzing disease-causing genes
genetic engineering alteration of the genetic makeup of an organism
genetic testing process of testing for the presence of disease-causing genes
genetically modified organism (GMO) organism whose genome has been artificially changed
host DNA DNA that is present in the genome of the organism of interest
lysis buffer solution used to break the cell membrane and release cell contents
molecular cloning cloning of DNA fragments
multiple cloning site (MCS)  site that can be recognized by multiple restriction endonucleases
northern blotting transfer of RNA from a gel to a nylon membrane
polymerase chain reaction (PCR)  technique used to amplify DNA
probe small DNA fragment used to determine if the complementary sequence is present in a DNA sample
protease enzyme that breaks down proteins
recombinant DNA combination of DNA fragments generated by molecular cloning that does not exist in nature; also known as a chimeric molecule
recombinant protein protein product of a gene derived by molecular cloning
reproductive cloning cloning of entire organisms
restriction endonuclease enzyme that can recognize and cleave specific DNA sequences
reverse genetics method of determining the function of a gene by starting with the gene itself instead of starting with the gene product
reverse transcriptase PCR (RT-PCR)  PCR technique that involves converting RNA to DNA by reverse transcriptase
ribonuclease enzyme that breaks down RNA
Southern blotting transfer of DNA from a gel to a nylon membrane
Ti plasmid plasmid system derived from Agrobacterium tumifaciens that has been used by scientists to introduce foreign DNA into plant cells
transgenic organism that receives DNA from a different species

