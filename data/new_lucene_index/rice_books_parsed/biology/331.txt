
Transport of Water and Solutes in Plants
Transport of Water and Solutes in PlantsBy the end of this section, you will be able to:

Define water potential and explain how it is influenced by solutes, pressure, gravity, and the matric potential
Describe how water potential, evapotranspiration, and stomatal regulation influence how water is transported in plants
Explain how photosynthates are transported in plants

The structure of plant roots, stems, and leaves facilitates the transport of water, nutrients, and photosynthates throughout the plant. The phloem and xylem are the main tissues responsible for this movement. Water potential, evapotranspiration, and stomatal regulation influence how water and nutrients are transported in plants. To understand how these processes work, we must first understand the energetics of water potential.
Water Potential
Plants are phenomenal hydraulic engineers. Using only the basic laws of physics and the simple manipulation of potential energy, plants can move water to the top of a 116-meter-tall tree ([link]a). Plants can also use hydraulics to generate enough force to split rocks and buckle sidewalks ([link]b). Plants achieve this because of water potential.With heights nearing 116 meters, (a) coastal redwoods (Sequoia sempervirens) are the tallest trees in the world. Plant roots can easily generate enough force to (b) buckle and break concrete sidewalks, much to the dismay of homeowners and city maintenance departments. (credit a: modification of work by Bernt Rostad; credit b: modification of work by Pedestrians Educating Drivers on Safety, Inc.)


Water potential is a measure of the potential energy in water. Plant physiologists are not interested in the energy in any one particular aqueous system, but are very interested in water movement between two systems. In practical terms, therefore, water potential is the difference in potential energy between a given water sample and pure water (at atmospheric pressure and ambient temperature). Water potential is denoted by the Greek letter  (psi) and is expressed in units of pressure (pressure is a form of energy) called megapascals (MPa). The potential of pure water (wpure H2O) is, by convenience of definition, designated a value of zero (even though pure water contains plenty of potential energy, that energy is ignored). Water potential values for the water in a plant root, stem, or leaf are therefore expressed relative to wpure H2O.
The water potential in plant solutions is influenced by solute concentration, pressure, gravity, and factors called matrix effects. Water potential can be broken down into its individual components using the following equation:





system


=


total


=

s

+

p

+

g

+

m






system


=


total


=

s

+

p

+

g

+

m

where s, p, g, and m refer to the solute, pressure, gravity, and matric potentials, respectively. System can refer to the water potential of the soil water (soil), root water (root), stem water (stem), leaf water (leaf) or the water in the atmosphere (atmosphere): whichever aqueous system is under consideration. As the individual components change, they raise or lower the total water potential of a system. When this happens, water moves to equilibrate, moving from the system or compartment with a higher water potential to the system or compartment with a lower water potential. This brings the difference in water potential between the two systems () back to zero ( = 0). Therefore, for water to move through the plant from the soil to the air (a process called transpiration), soil must be > root > stem > leaf > atmosphere.
Water only moves in response to , not in response to the individual components. However, because the individual components influence the total system, by manipulating the individual components (especially s), a plant can control water movement.
Solute Potential
Solute potential (s), also called osmotic potential, is negative in a plant cell and zero in distilled water. Typical values for cell cytoplasm are 0.5 to 1.0 MPa. Solutes reduce water potential (resulting in a negative w) by consuming some of the potential energy available in the water. Solute molecules can dissolve in water because water molecules can bind to them via hydrogen bonds; a hydrophobic molecule like oil, which cannot bind to water, cannot go into solution. The energy in the hydrogen bonds between solute molecules and water is no longer available to do work in the system because it is tied up in the bond. In other words, the amount of available potential energy is reduced when solutes are added to an aqueous system. Thus, s decreases with increasing solute concentration. Because s is one of the four components of system or total, a decrease in s will cause a decrease in total. The internal water potential of a plant cell is more negative than pure water because of the cytoplasms high solute content ([link]). Because of this difference in water potential water will move from the soil into a plants root cells via the process of osmosis. This is why solute potential is sometimes called osmotic potential.Plant cells can metabolically manipulate s (and by extension, total) by adding or removing solute molecules. Therefore, plants have control over total via their ability to exert metabolic control over s.


In this example with a semipermeable membrane between two aqueous systems, water will move from a region of higher to lower water potential until equilibrium is reached. Solutes (s), pressure (p), and gravity (g) influence total water potential for each side of the tube (total right or left), and therefore, the difference between total on each side (). (m , the potential due to interaction of water with solid substrates, is ignored in this example because glass is not especially hydrophilic). Water moves in response to the difference in water potential between two systems (the left and right sides of the tube).



Positive water potential is placed on the left side of the tube by increasing p such that the water level rises on the right side. Could you equalize the water level on each side of the tube by adding solute, and if so, how?



Pressure Potential
Pressure potential (p), also called turgor potential, may be positive or negative ([link]). Because pressure is an expression of energy, the higher the pressure, the more potential energy in a system, and vice versa. Therefore, a positive p (compression) increases total, and a negative p (tension) decreases total. Positive pressure inside cells is contained by the cell wall, producing turgor pressure. Pressure potentials are typically around 0.60.8 MPa, but can reach as high as 1.5 MPa in a well-watered plant. A p of 1.5 MPa equates to 210 pounds per square inch (1.5 MPa x 140 lb in-2 MPa-1 = 210 lb/in-2). As a comparison, most automobile tires are kept at a pressure of 3034 psi. An example of the effect of turgor pressure is the wilting of leaves and their restoration after the plant has been watered ([link]). Water is lost from the leaves via transpiration (approaching p = 0 MPa at the wilting point) and restored by uptake via the roots.A plant can manipulate p via its ability to manipulate s and by the process of osmosis. If a plant cell increases the cytoplasmic solute concentration, s will decline, total will decline, the  between the cell and the surrounding tissue will decline, water will move into the cell by osmosis, and p will increase. p is also under indirect plant control via the opening and closing of stomata. Stomatal openings allow water to evaporate from the leaf, reducing p and total of the leaf and increasing ii between the water in the leaf and the petiole, thereby allowing water to flow from the petiole into the leaf.When (a) total water potential (total) is lower outside the cells than inside, water moves out of the cells and the plant wilts. When (b) the total water potential is higher outside the plant cells than inside, water moves into the cells, resulting in turgor pressure (p) and keeping the plant erect. (credit: modification of work by Victor M. Vicente Selvas)



Gravity Potential
Gravity potential (g) is always negative to zero in a plant with no height. It always removes or consumes potential energy from the system. The force of gravity pulls water downwards to the soil, reducing the total amount of potential energy in the water in the plant (total). The taller the plant, the taller the water column, and the more influential g becomes. On a cellular scale and in short plants, this effect is negligible and easily ignored. However, over the height of a tall tree like a giant coastal redwood, the gravitational pull of 0.1 MPa m-1 is equivalent to an extra 1 MPa of resistance that must be overcome for water to reach the leaves of the tallest trees. Plants are unable to manipulate g.

Matric PotentialMatric potential (m) is always negative to zero. In a dry system, it can be as low as 2 MPa in a dry seed, and it is zero in a water-saturated system. The binding of water to a matrix always removes or consumes potential energy from the system. m is similar to solute potential because it involves tying up the energy in an aqueous system by forming hydrogen bonds between the water and some other component. However, in solute potential, the other components are soluble, hydrophilic solute molecules, whereas in m, the other components are insoluble, hydrophilic molecules of the plant cell wall. Every plant cell has a cellulosic cell wall and the cellulose in the cell walls is hydrophilic, producing a matrix for adhesion of water: hence the name matric potential. m is very large (negative) in dry tissues such as seeds or drought-affected soils. However, it quickly goes to zero as the seed takes up water or the soil hydrates. m cannot be manipulated by the plant and is typically ignored in well-watered roots, stems, and leaves.
Movement of Water and Minerals in the Xylem
Solutes, pressure, gravity, and matric potential are all important for the transport of water in plants. Water moves from an area of higher total water potential (higher Gibbs free energy) to an area of lower total water potential. Gibbs free energy is the energy associated with a chemical reaction that can be used to do work. This is expressed as .
Transpiration is the loss of water from the plant through evaporation at the leaf surface. It is the main driver of water movement in the xylem. Transpiration is caused by the evaporation of water at the leafatmosphere interface; it creates negative pressure (tension) equivalent to 2 MPa at the leaf surface. This value varies greatly depending on the vapor pressure deficit, which can be negligible at high relative humidity (RH) and substantial at low RH. Water from the roots is pulled up by this tension. At night, when stomata shut and transpiration stops, the water is held in the stem and leaf by the adhesion of water to the cell walls of the xylem vessels and tracheids, and the cohesion of water molecules to each other. This is called the cohesiontension theory of sap ascent.
Inside the leaf at the cellular level, water on the surface of mesophyll cells saturates the cellulose microfibrils of the primary cell wall. The leaf contains many large intercellular air spaces for the exchange of oxygen for carbon dioxide, which is required for photosynthesis. The wet cell wall is exposed to this leaf internal air space, and the water on the surface of the cells evaporates into the air spaces, decreasing the thin film on the surface of the mesophyll cells. This decrease creates a greater tension on the water in the mesophyll cells ([link]), thereby increasing the pull on the water in the xylem vessels. The xylem vessels and tracheids are structurally adapted to cope with large changes in pressure. Rings in the vessels maintain their tubular shape, much like the rings on a vacuum cleaner hose keep the hose open while it is under pressure. Small perforations between vessel elements reduce the number and size of gas bubbles that can form via a process called cavitation. The formation of gas bubbles in xylem interrupts the continuous stream of water from the base to the top of the plant, causing a break termed an embolism in the flow of xylem sap. The taller the tree, the greater the tension forces needed to pull water, and the more cavitation events. In larger trees, the resulting embolisms can plug xylem vessels, making them non-functional.
Art Connection
The cohesiontension theory of sap ascent is shown. Evaporation from the mesophyll cells produces a negative water potential gradient that causes water to move upwards from the roots through the xylem.


Which of the following statements is false?
Negative water potential draws water into the root hairs. Cohesion and adhesion draw water up the xylem. Transpiration draws water from the leaf.
Negative water potential draws water into the root hairs. Cohesion and adhesion draw water up the phloem. Transpiration draws water from the leaf.
Water potential decreases from the roots to the top of the plant.
Water enters the plants through root hairs and exits through stoma.

Transpirationthe loss of water vapor to the atmosphere through stomatais a passive process, meaning that metabolic energy in the form of ATP is not required for water movement. The energy driving transpiration is the difference in energy between the water in the soil and the water in the atmosphere. However, transpiration is tightly controlled.
Control of Transpiration
The atmosphere to which the leaf is exposed drives transpiration, but also causes massive water loss from the plant. Up to 90 percent of the water taken up by roots may be lost through transpiration.
Leaves are covered by a waxy cuticle on the outer surface that prevents the loss of water. Regulation of transpiration, therefore, is achieved primarily through the opening and closing of stomata on the leaf surface. Stomata are surrounded by two specialized cells called guard cells, which open and close in response to environmental cues such as light intensity and quality, leaf water status, and carbon dioxide concentrations. Stomata must open to allow air containing carbon dioxide and oxygen to diffuse into the leaf for photosynthesis and respiration. When stomata are open, however, water vapor is lost to the external environment, increasing the rate of transpiration. Therefore, plants must maintain a balance between efficient photosynthesis and water loss.
Plants have evolved over time to adapt to their local environment and reduce transpiration([link]). Desert plant (xerophytes) and plants that grow on other plants (epiphytes) have limited access to water. Such plants usually have a much thicker waxy cuticle than those growing in more moderate, well-watered environments (mesophytes). Aquatic plants (hydrophytes) also have their own set of anatomical and morphological leaf adaptations.
Plants are suited to their local environment. (a) Xerophytes, like this prickly pear cactus (Opuntia sp.) and (b) epiphytes such as this tropical Aeschynanthus perrottetii have adapted to very limited water resources. The leaves of a prickly pear are modified into spines, which lowers the surface-to-volume ratio and reduces water loss. Photosynthesis takes place in the stem, which also stores water. (b) A. perottetii leaves have a waxy cuticle that prevents water loss. (c) Goldenrod (Solidago sp.) is a mesophyte, well suited for moderate environments. (d) Hydrophytes, like this fragrant water lily (Nymphaea odorata), are adapted to thrive in aquatic environments. (credit a: modification of work by Jon Sullivan; credit b: modification of work by L. Shyamal/Wikimedia Commons; credit c: modification of work by Huw Williams; credit d: modification of work by Jason Hollinger)


Xerophytes and epiphytes often have a thick covering of trichomes or of stomata that are sunken below the leafs surface. Trichomes are specialized hair-like epidermal cells that secrete oils and substances. These adaptations impede air flow across the stomatal pore and reduce transpiration. Multiple epidermal layers are also commonly found in these types of plants.


Transportation of Photosynthates in the Phloem
Plants need an energy source to grow. In seeds and bulbs, food is stored in polymers (such as starch) that are converted by metabolic processes into sucrose for newly developing plants. Once green shoots and leaves are growing, plants are able to produce their own food by photosynthesizing. The products of photosynthesis are called photosynthates, which are usually in the form of simple sugars such as sucrose.
Structures that produce photosynthates for the growing plant are referred to as sources. Sugars produced in sources, such as leaves, need to be delivered to growing parts of the plant via the phloem in a process called translocation. The points of sugar delivery, such as roots, young shoots, and developing seeds, are called sinks. Seeds, tubers, and bulbs can be either a source or a sink, depending on the plants stage of development and the season.
The products from the source are usually translocated to the nearest sink through the phloem. For example, the highest leaves will send photosynthates upward to the growing shoot tip, whereas lower leaves will direct photosynthates downward to the roots. Intermediate leaves will send products in both directions, unlike the flow in the xylem, which is always unidirectional (soil to leaf to atmosphere). The pattern of photosynthate flow changes as the plant grows and develops. Photosynthates are directed primarily to the roots early on, to shoots and leaves during vegetative growth, and to seeds and fruits during reproductive development. They are also directed to tubers for storage.
Translocation: Transport from Source to Sink
Photosynthates, such as sucrose, are produced in the mesophyll cells of photosynthesizing leaves. From there they are translocated through the phloem to where they are used or stored. Mesophyll cells are connected by cytoplasmic channels called plasmodesmata. Photosynthates move through these channels to reach phloem sieve-tube elements (STEs) in the vascular bundles. From the mesophyll cells, the photosynthates are loaded into the phloem STEs. The sucrose is actively transported against its concentration gradient (a process requiring ATP) into the phloem cells using the electrochemical potential of the proton gradient. This is coupled to the uptake of sucrose with a carrier protein called the sucrose-H+ symporter.Phloem STEs have reduced cytoplasmic contents, and are connected by a sieve plate with pores that allow for pressure-driven bulk flow, or translocation, of phloem sap. Companion cells are associated with STEs. They assist with metabolic activities and produce energy for the STEs ([link]).
Phloem is comprised of cells called sieve-tube elements. Phloem sap travels through perforations called sieve tube plates. Neighboring companion cells carry out metabolic functions for the sieve-tube elements and provide them with energy. Lateral sieve areas connect the sieve-tube elements to the companion cells.


Once in the phloem, the photosynthates are translocated to the closest sink. Phloem sap is an aqueous solution that contains up to 30 percent sugar, minerals, amino acids, and plant growth regulators. The high percentage of sugar decreases s, which decreases the total water potential and causes water to move by osmosis from the adjacent xylem into the phloem tubes, thereby increasing pressure. This increase in total water potential causes the bulk flow of phloem from source to sink ([link]). Sucrose concentration in the sink cells is lower than in the phloem STEs because the sink sucrose has been metabolized for growth, or converted to starch for storage or other polymers, such as cellulose, for structural integrity. Unloading at the sink end of the phloem tube occurs by either diffusion or active transport of sucrose molecules from an area of high concentration to one of low concentration. Water diffuses from the phloem by osmosis and is then transpired or recycled via the xylem back into the phloem sap.Sucrose is actively transported from source cells into companion cells and then into the sieve-tube elements. This reduces the water potential, which causes water to enter the phloem from the xylem. The resulting positive pressure forces the sucrose-water mixture down toward the roots, where sucrose is unloaded. Transpiration causes water to return to the leaves through the xylem vessels.




Section SummaryWater potential () is a measure of the difference in potential energy between a water sample and pure water. The water potential in plant solutions is influenced by solute concentration, pressure, gravity, and matric potential. Water potential and transpiration influence how water is transported through the xylem in plants. These processes are regulated by stomatal opening and closing. Photosynthates (mainly sucrose) move from sources to sinks through the plants phloem. Sucrose is actively loaded into the sieve-tube elements of the phloem. The increased solute concentration causes water to move by osmosis from the xylem into the phloem. The positive pressure that is produced pushes water and solutes down the pressure gradient. The sucrose is unloaded into the sink, and the water returns to the xylem vessels.
Art Connections

[link] Positive water potential is placed on the left side of the tube by increasing p such that the water level rises on the right side. Could you equalize the water level on each side of the tube by adding solute, and if so, how?


[link] Yes, you can equalize the water level by adding the solute to the left side of the tube such that water moves toward the left until the water levels are equal.



[link] Which of the following statements is false?

Negative water potential draws water into the root hairs. Cohesion and adhesion draw water up the xylem. Transpiration draws water from the leaf.
Negative water potential draws water into the root hairs. Cohesion and adhesion draw water up the phloem. Transpiration draws water from the leaf.
Water potential decreases from the roots to the top of the plant.
Water enters the plants through root hairs and exits through stoma.



[link] B.

Review Questions

When stomata open, what occurs?

Water vapor is lost to the external environment, increasing the rate of transpiration.
Water vapor is lost to the external environment, decreasing the rate of transpiration.
Water vapor enters the spaces in the mesophyll, increasing the rate of transpiration.
Water vapor enters the spaces in the mesophyll, increasing the rate of transpiration.



A




Which cells are responsible for the movement of photosynthates through a plant?

tracheids, vessel elements
tracheids, companion cells
vessel elements, companion cells
sieve-tube elements, companion cells



D



Free Response


The process of bulk flow transports fluids in a plant. Describe the two main bulk flow processes.

The process of bulk flow moves water up the xylem and moves photosynthates (solutes) up and down the phloem.



Glossary

cuticle waxy covering on the outside of the leaf and stem that prevents the loss of water


megapascal (MPa) pressure units that measure water potential


sink growing parts of a plant, such as roots and young leaves, which require photosynthate


source organ that produces photosynthate for a plant


translocation mass transport of photosynthates from source to sink in vascular plants


transpiration loss of water vapor to the atmosphere through stomata


water potential (w) the potential energy of a water solution per unit volume in relation to pure water at atmospheric pressure and ambient temperature


