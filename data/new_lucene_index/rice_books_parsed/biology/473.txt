
Introduction
Introduction
        class="introduction"
      
class="summary" title="Sections Summary"
class="art-exercise" title="Art Connections"
class="multiple-choice" title="Multiple Choice"
class="free-response" title="Free Response"
An arctic fox is a complex animal, well adapted to its environment. It changes coat color with the seasons, and has longer fur in winter to trap heat. (credit: modification of work by Keith Morehouse, USFWS)


The arctic fox is an example of a complex animal that has adapted to its environment and illustrates the relationships between an animals form and function. The structures of animals consist of primary tissues that make up more complex organs and organ systems. Homeostasis allows an animal to maintain a balance between its internal and external environments.

