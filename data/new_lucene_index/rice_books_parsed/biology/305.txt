
Introduction
Introduction
        class="introduction"
      
class="summary" title="Sections Summary"
class="art-exercise" title="Art Connections"
class="multiple-choice" title="Multiple Choice"
class="free-response" title="Free Response"
An athletes nervous system is hard at work during the planning and execution of a movement as precise as a high jump. Parts of the nervous system are involved in determining how hard to push off and when to turn, as well as controlling the muscles throughout the body that make this complicated movement possible without knocking the bar downall in just a few seconds. (credit: modification of work by Shane T. McCoy, U.S. Navy)


When youre reading this book, your nervous system is performing several functions simultaneously. The visual system is processing what is seen on the page; the motor system controls the turn of the pages (or click of the mouse); the prefrontal cortex maintains attention. Even fundamental functions, like breathing and regulation of body temperature, are controlled by the nervous system. A nervous system is an organisms control center: it processes sensory information from outside (and inside) the body and controls all behaviorsfrom eating to sleeping to finding a mate.

