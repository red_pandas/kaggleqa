
Introduction to the Second Law of Thermodynamics: Heat Engines and Their Efficiency
Introduction to the Second Law of Thermodynamics: Heat Engines and Their Efficiency
State the expressions of the second law of thermodynamics.
Calculate the efficiency and carbon dioxide emission of a coal-fired electricity plant, using second law characteristics.
Describe and define the Otto cycle.

These ice floes melt during the Arctic summer. Some of them refreeze in the winter, but the second law of thermodynamics predicts that it would be extremely unlikely for the water molecules contained in these particular floes to reform the distinctive alligator-like shape they formed when the picture was taken in the summer of 2009. (credit: Patrick Kelley, U.S. Coast Guard, U.S. Geological Survey) 


The second law of thermodynamics deals with the direction taken by spontaneous processes. Many processes occur spontaneously in one direction onlythat is, they are irreversible, under a given set of conditions. Although irreversibility is seen in day-to-day lifea broken glass does not resume its original state, for instancecomplete irreversibility is a statistical statement that cannot be seen during the lifetime of the universe. More precisely, an irreversible process is one that depends on path. If the process can go in only one direction, then the reverse path differs fundamentally and the process cannot be reversible. For example, as noted in the previous section, heat involves the transfer of energy from higher to lower temperature. A cold object in contact with a hot one never gets colder, transferring heat to the hot object and making it hotter. Furthermore, mechanical energy, such as kinetic energy, can be completely converted to thermal energy by friction, but the reverse is impossible. A hot stationary object never spontaneously cools off and starts moving. Yet another example is the expansion of a puff of gas introduced into one corner of a vacuum chamber. The gas expands to fill the chamber, but it never regroups in the corner. The random motion of the gas molecules could take them all back to the corner, but this is never observed to happen. (See [link].)
Examples of one-way processes in nature. (a) Heat transfer occurs spontaneously from hot to cold and not from cold to hot. (b) The brakes of this car convert its kinetic energy to heat transfer to the environment. The reverse process is impossible. (c) The burst of gas let into this vacuum chamber quickly expands to uniformly fill every part of the chamber. The random motions of the gas molecules will never return them to the corner.


The fact that certain processes never occur suggests that there is a law forbidding them to occur. The first law of thermodynamics would allow them to occurnone of those processes violate conservation of energy. The law that forbids these processes is called the second law of thermodynamics. We shall see that the second law can be stated in many ways that may seem different, but which in fact are equivalent. Like all natural laws, the second law of thermodynamics gives insights into nature, and its several statements imply that it is broadly applicable, fundamentally affecting many apparently disparate processes.
The already familiar direction of heat transfer from hot to cold is the basis of our first version of the second law of thermodynamics.
The Second Law of Thermodynamics (first expression)
Heat transfer occurs spontaneously from higher- to lower-temperature bodies but never spontaneously in the reverse direction.Another way of stating this: It is impossible for any process to have as its sole result heat transfer from a cooler to a hotter object.
Heat Engines
Now let us consider a device that uses heat transfer to do work. As noted in the previous section, such a device is called a heat engine, and one is shown schematically in [link](b). Gasoline and diesel engines, jet engines, and steam turbines are all heat engines that do work by using part of the heat transfer from some source. Heat transfer from the hot object (or hot reservoir) is denoted as QhQh size 12{Q rSub { size 8{h} } } {}, while heat transfer into the cold object (or cold reservoir) is QcQc size 12{Q rSub { size 8{c} } } {}, and the work done by the engine is WW size 12{W} {}. The temperatures of the hot and cold reservoirs are ThTh size 12{T rSub { size 8{h} } } {} and TcTc size 12{T rSub { size 8{c} } } {}, respectively.(a) Heat transfer occurs spontaneously from a hot object to a cold one, consistent with the second law of thermodynamics. (b) A heat engine, represented here by a circle, uses part of the heat transfer to do work. The hot and cold objects are called the hot and cold reservoirs. QhQh size 12{Q rSub { size 8{h} } } {} is the heat transfer out of the hot reservoir, WW size 12{W} {} is the work output, and QcQc size 12{Q rSub { size 8{c} } } {} is the heat transfer into the cold reservoir.


Because the hot reservoir is heated externally, which is energy intensive, it is important that the work is done as efficiently as possible. In fact, we would like WW size 12{W} {} to equal QhQh size 12{Q rSub { size 8{h} } } {}, and for there to be no heat transfer to the environment (Qc=0Qc=0 size 12{Q rSub { size 8{c} } =0} {}). Unfortunately, this is impossible. The second law of thermodynamics also states, with regard to using heat transfer to do work (the second expression of the second law):
The Second Law of Thermodynamics (second expression)It is impossible in any system for heat transfer from a reservoir to completely convert to work in a cyclical process in which the system returns to its initial state.A cyclical process brings a system, such as the gas in a cylinder, back to its original state at the end of every cycle. Most heat engines, such as reciprocating piston engines and rotating turbines, use cyclical processes. The second law, just stated in its second form, clearly states that such engines cannot have perfect conversion of heat transfer into work done. Before going into the underlying reasons for the limits on converting heat transfer into work, we need to explore the relationships among WW size 12{W} {}, QhQh size 12{Q rSub { size 8{h} } } {}, and QcQc size 12{Q rSub { size 8{c} } } {}, and to define the efficiency of a cyclical heat engine. As noted, a cyclical process brings the system back to its original condition at the end of every cycle. Such a systems internal energy UU is the same at the beginning and end of every cyclethat is, U=0U=0 size 12{U=0} {}. The first law of thermodynamics states thatU=QW,U=QW, size 12{U=Q - W} {}
where QQ size 12{Q} {} is the net heat transfer during the cycle (Q=QhQcQ=QhQc size 12{Q=Q rSub { size 8{h} }  - Q rSub { size 8{c} } } {}) and WW size 12{W} {} is the net work done by the system. Since U=0U=0 size 12{U=0} {} for a complete cycle, we have0=QW,0=QW, size 12{0=Q - W} {}
so that
W=Q.W=Q. size 12{W=Q} {}
Thus the net work done by the system equals the net heat transfer into the system, or
W=QhQc(cyclical process),W=QhQc(cyclical process), size 12{W=Q rSub { size 8{h} }  - Q rSub { size 8{c} } } {}just as shown schematically in [link](b). The problem is that in all processes, there is some heat transfer QcQc size 12{Q rSub { size 8{c} } } {} to the environmentand usually a very significant amount at that.In the conversion of energy to work, we are always faced with the problem of getting less out than we put in. We define conversion efficiency EffEff size 12{ ital "Eff"} {} to be the ratio of useful work output to the energy input (or, in other words, the ratio of what we get to what we spend). In that spirit, we define the efficiency of a heat engine to be its net work output WW size 12{W} {} divided by heat transfer to the engine QhQh size 12{Q rSub { size 8{h} } } {}; that is,
Eff=WQh.Eff=WQh. size 12{ ital "Eff"= {  {W}  over  {Q rSub { size 8{h} } } } } {}
Since W=QhQcW=QhQc size 12{W=Q rSub { size 8{h} } -Q rSub { size 8{c} } } {} in a cyclical process, we can also express this as
Eff=QhQcQh=1QcQh(cyclical process),Eff=QhQcQh=1QcQh(cyclical process), size 12{ ital "Eff"= {  {Q rSub { size 8{h} }  - Q rSub { size 8{c} } }  over  {Q rSub { size 8{h} } } } =1 -  {  {Q rSub { size 8{c} } }  over  {Q rSub { size 8{h} } } } } {}making it clear that an efficiency of 1, or 100%, is possible only if there is no heat transfer to the environment (Qc=0Qc=0 size 12{Q rSub { size 8{c} } =0} {}). Note that all QQ size 12{Q} {}s are positive. The direction of heat transfer is indicated by a plus or minus sign. For example, QcQc size 12{Q rSub { size 8{c} } } {} is out of the system and so is preceded by a minus sign.Daily Work Done by a Coal-Fired Power Station, Its Efficiency and Carbon Dioxide EmissionsA coal-fired power station is a huge heat engine. It uses heat transfer from burning coal to do work to turn turbines, which are used to generate electricity. In a single day, a large coal power station has 2.501014J2.501014J size 12{2 "." "50" times "10" rSup { size 8{"14"} } J} {} of heat transfer from coal and 1.481014J1.481014J size 12{1 "." "48" times "10" rSup { size 8{"14"} } J} {} of heat transfer into the environment. (a) What is the work done by the power station? (b) What is the efficiency of the power station? (c) In the combustion process, the following chemical reaction occurs: C+O2CO2C+O2CO2 size 12{C+O rSub { size 8{2} }  rightarrow "CO" rSub { size 8{2} } } {}. This implies that every 12 kg of coal puts 12 kg + 16 kg + 16 kg = 44 kg of carbon dioxide into the atmosphere. Assuming that 1 kg of coal can provide 2.5106J2.5106J size 12{2 "." 5 times "10" rSup { size 8{6} } J} {} of heat transfer upon combustion, how much CO2CO2 size 12{"CO" rSub { size 8{2} } } {} is emitted per day by this power plant? Strategy for (a)We can use W=QhQcW=QhQc size 12{W=Q rSub { size 8{h} }  - Q rSub { size 8{c} } } {} to find the work output WW size 12{W} {}, assuming a cyclical process is used in the power station. In this process, water is boiled under pressure to form high-temperature steam, which is used to run steam turbine-generators, and then condensed back to water to start the cycle again.
Solution for (a)Work output is given by:
W=QhQc.W=QhQc. size 12{W=Q rSub { size 8{h} }  - Q rSub { size 8{c} } } {}
Substituting the given values:




 W
 =





2

.
50


10

14



J

1

.
48


10

14


J






 =


1
.
02


10

14


J
.











 W
 =





2

.
50


10

14



J

1

.
48


10

14


J






 =


1
.
02


10

14


J
.







alignl { stack {
 size 12{W=2 "." "50""10" rSup { size 8{"14"} } " J" +- 1 "." "48""10" rSup { size 8{"14"} } " J"}  {} # 
=1 "." "02""10" rSup { size 8{"14"} } " J" "."  {} 
} } {}
 Strategy for (b)The efficiency can be calculated with Eff=WQhEff=WQh size 12{ ital "Eff"= {  {W}  over  {Q rSub { size 8{h} } } } } {} since QhQh size 12{Q rSub { size 8{h} } } {} is given and work WW size 12{W} {} was found in the first part of this example.
Solution for (b)Efficiency is given by: Eff=WQhEff=WQh size 12{ ital "Eff"= {  {W}  over  {Q rSub { size 8{h} } } } } {}. The work  
W

W
 was just found to be  


1.02  10
14

J




1.02  10
14

J

, and QhQh size 12{Q rSub { size 8{h} } } {}  is given, so the efficiency is



 Eff
 =






1
.

02


10

14



J


2
.

50


10

14



J








 =



0
.
408
,or
40
.
8%












 Eff
 =






1
.

02


10

14



J


2
.

50


10

14



J








 =



0
.
408
,or
40
.
8%








alignl { stack {
 size 12{ ital "Eff"= {  {1 "." "02" times "10" rSup { size 8{"14"} } J}  over  {2 "." "50" times "10" rSup { size 8{"14"} } J} } }  {} # 
=0 "." "408"", or ""40" "." 8% {} 
} } {}

Strategy for (c)The daily consumption of coal is calculated using the information that each day there is 2.501014 J2.501014 J size 12{2 "." "50""10" rSup { size 8{"14"} } " J"} {} of heat transfer from coal. In the combustion process, we have C+O2CO2C+O2CO2 size 12{C+O rSub { size 8{2} }  rightarrow "CO" rSub { size 8{2} } } {}. So every 12 kg of coal puts 12 kg + 16 kg + 16 kg = 44 kg of CO2CO2 size 12{"CO" rSub { size 8{2} } } {} into the atmosphere.Solution for (c)The daily coal consumption is2.501014J2.50106J/kg=1.0108kg.2.501014J2.50106J/kg=1.0108kg. size 12{ {  {2 "." "50""10" rSup { size 8{"14"} } " J"}  over  {2 "." "50""10" rSup { size 8{6} } " J/kg"} } =1 "." 0"10" rSup { size 8{7} } " J/kg"} {}Assuming that the coal is pure and that all the coal goes toward producing carbon dioxide, the carbon dioxide produced per day is
1.0108kg coal44 kg CO212 kg coal=3.7108kg CO2.1.0108kg coal44 kg CO212 kg coal=3.7108kg CO2. size 12{1 "." 0"10" rSup { size 8{7} } " kg coal" {  {"44 kg CO" rSub { size 8{2} } }  over  {"12 kg coal"} } =3 "." 7"10" rSup { size 8{7} } " kg CO" rSub { size 8{2} } } {}This is 370,000 metric tons of CO2CO2 size 12{"CO" rSub { size 8{2} } } {} produced every day.Discussion
If all the work output is converted to electricity in a period of one day, the average power output is 1180 MW (this is left to you as an end-of-chapter problem). This value is about the size of a large-scale conventional power plant. The efficiency found is acceptably close to the value of 42% given for coal power stations. It means that fully 59.2% of the energy is heat transfer to the environment, which usually results in warming lakes, rivers, or the ocean near the power station, and is implicated in a warming planet generally. While the laws of thermodynamics limit the efficiency of such plantsincluding plants fired by nuclear fuel, oil, and natural gasthe heat transfer to the environment could be, and sometimes is, used for heating homes or for industrial processes. The generally low cost of energy has not made it economical to make better use of the waste heat transfer from most heat engines. Coal-fired power plants produce the greatest amount of CO2CO2 size 12{"CO" rSub { size 8{2} } } {} per unit energy output (compared to natural gas or oil), making coal the least efficient fossil fuel.With the information given in [link], we can find characteristics such as the efficiency of a heat engine without any knowledge of how the heat engine operates, but looking further into the mechanism of the engine will give us greater insight. [link] illustrates the operation of the common four-stroke gasoline engine. The four steps shown complete this heat engines cycle, bringing the gasoline-air mixture back to its original condition.The Otto cycle shown in [link](a) is used in four-stroke internal combustion engines, although in fact the true Otto cycle paths do not correspond exactly to the strokes of the engine.
The adiabatic process AB corresponds to the nearly adiabatic compression stroke of the gasoline engine. In both cases, work is done on the system (the gas mixture in the cylinder), increasing its temperature and pressure. Along path BC of the Otto cycle, heat transfer QhQh size 12{Q rSub { size 8{h} } } {} into the gas occurs at constant volume, causing a further increase in pressure and temperature. This process corresponds to burning fuel in an internal combustion engine, and takes place so rapidly that the volume is nearly constant. Path CD in the Otto cycle is an adiabatic expansion that does work on the outside world, just as the power stroke of an internal combustion engine does in its nearly adiabatic expansion. The work done by the system along path CD is greater than the work done on the system along path AB, because the pressure is greater, and so there is a net work output. Along path DA in the Otto cycle, heat transfer QcQc size 12{Q rSub { size 8{c} } } {} from the gas at constant volume reduces its temperature and pressure, returning it to its original state. In an internal combustion engine, this process corresponds to the exhaust of hot gases and the intake of an air-gasoline mixture at a considerably lower temperature. In both cases, heat transfer into the environment occurs along this final path.
The net work done by a cyclical process is the area inside the closed path on a PVPV size 12{ ital "PV"} {} diagram, such as that inside path ABCDA in [link]. Note that in every imaginable cyclical process, it is absolutely necessary for heat transfer from the system to occur in order to get a net work output. In the Otto cycle, heat transfer occurs along path DA. If no heat transfer occurs, then the return path is the same, and the net work output is zero. The lower the temperature on the path AB, the less work has to be done to compress the gas. The area inside the closed path is then greater, and so the engine does more work and is thus more efficient. Similarly, the higher the temperature along path CD, the more work output there is. (See [link].) So efficiency is related to the temperatures of the hot and cold reservoirs. In the next section, we shall see what the absolute limit to the efficiency of a heat engine is, and how it is related to temperature.In the four-stroke internal combustion gasoline engine, heat transfer into work takes place in the cyclical process shown here. The piston is connected to a rotating crankshaft, which both takes work out of and does work on the gas in the cylinder. (a) Air is mixed with fuel during the intake stroke. (b) During the compression stroke, the air-fuel mixture is rapidly compressed in a nearly adiabatic process, as the piston rises with the valves closed. Work is done on the gas. (c) The power stroke has two distinct parts. First, the air-fuel mixture is ignited, converting chemical potential energy into thermal energy almost instantaneously, which leads to a great increase in pressure. Then the piston descends, and the gas does work by exerting a force through a distance in a nearly adiabatic process. (d) The exhaust stroke expels the hot gas to prepare the engine for another cycle, starting again with the intake stroke.


PVPV size 12{ ital "PV"} {} diagram for a simplified Otto cycle, analogous to that employed in an internal combustion engine. Point A corresponds to the start of the compression stroke of an internal combustion engine. Paths AB and CD are adiabatic and correspond to the compression and power strokes of an internal combustion engine, respectively. Paths BC and DA are isochoric and accomplish similar results to the ignition and exhaust-intake portions, respectively, of the internal combustion engines cycle. Work is done on the gas along path AB, but more work is done by the gas along path CD, so that there is a net work output.


This Otto cycle produces a greater work output than the one in [link], because the starting temperature of path CD is higher and the starting temperature of path AB is lower. The area inside the loop is greater, corresponding to greater net work output.



Section SummaryThe two expressions of the second law of thermodynamics are: (i) Heat transfer occurs spontaneously from higher- to lower-temperature bodies but never spontaneously in the reverse direction; and (ii) It is impossible in any system for heat transfer from a reservoir to completely convert to work in a cyclical process in which the system returns to its initial state.
Irreversible processes depend on path and do not return to their original state. Cyclical processes are processes that return to their original state at the end of every cycle.
In a cyclical process, such as a heat engine, the net work done by the system equals the net heat transfer into the system, or 


W
=

Q
h



Q
c




W
=

Q
h



Q
c


 , where  


Q
h





Q
h


 is the heat transfer from the hot object (hot reservoir), and  


Q
c





Q
c


 is the heat transfer into the cold object (cold reservoir).
Efficiency can be expressed as 

Eff=WQhEff=WQh size 12{ ital "Eff"= {  {W}  over  {Q rSub { size 8{h} } } } } {},

 the ratio of work output divided by the amount of energy input.
The four-stroke gasoline engine is often explained in terms of the Otto cycle, which is a repeating sequence of processes that convert heat into work.
Conceptual Questions


Imagine you are driving a car up Pikes Peak in Colorado. To raise a car weighing 1000 kilograms a distance of 100 meters would require about a million joules. You could raise a car 12.5 kilometers with the energy in a gallon of gas. Driving up Pike's Peak (a mere 3000-meter climb) should consume a little less than a quart of gas. But other considerations have to be taken into account. Explain, in terms of efficiency, what factors may keep you from realizing your ideal energy use on this trip.




Is a temperature difference necessary to operate a heat engine? State why or why not.




Definitions of efficiency vary depending on how energy is being converted. Compare the definitions of efficiency for the human body and heat engines. How does the definition of efficiency in each relate to the type of energy being converted into doing work?




Whyother than the fact that the second law of thermodynamics says reversible engines are the most efficientshould heat engines employing reversible processes be more efficient than those employing irreversible processes? Consider that dissipative mechanisms are one cause of irreversibility.



Problem Exercises

A certain heat engine does 10.0 kJ of work and 8.50 kJ of heat transfer occurs to the environment in a cyclical process. (a) What was the heat transfer into this engine? (b) What was the engines efficiency?


(a)  


18.5

kJ




18.5

kJ

(b) 54.1%



With 2.56106J2.56106J size 12{2 "." "56""10" rSup { size 8{6} } " J"} {} of heat transfer into this engine, a given cyclical heat engine can do only 1.50105J1.50105J size 12{1 "." "50""10" rSup { size 8{5} } " J"} {} of work. (a) What is the engines efficiency? (b) How much heat transfer to the environment takes place?



(a) What is the work output of a cyclical heat engine having a 22.0% efficiency and 6.00109 J6.00109 J size 12{6 "." "00""10" rSup { size 8{9} } " J"} {} of heat transfer into the engine? (b) How much heat transfer occurs to the environment?

(a) 


1.32  10
9

J




1.32  10
9

J

(b) 


4.68  10
9

J




4.68  10
9

J





(a) What is the efficiency of a cyclical heat engine in which 75.0 kJ of heat transfer occurs to the environment for every 95.0 kJ of heat transfer into the engine? (b) How much work does it produce for 100 kJ of heat transfer into the engine?



The engine of a large ship does 2.00108 J2.00108 J size 12{2 "." "00""10" rSup { size 8{8} } " J"} {} of work with an efficiency of 5.00%. (a) How much heat transfer occurs to the environment? (b) How many barrels of fuel are consumed, if each barrel produces 6.00109 J6.00109 J size 12{6 "." "00""10" rSup { size 8{9} } " J"} {} of heat transfer when burned?

(a)  


3.80  10
9

J




3.80  10
9

J

(b) 0.667 barrels



(a) How much heat transfer occurs to the environment by an electrical power station that uses 1.251014 J1.251014 J size 12{1 "." "25""10" rSup { size 8{"14"} } " J"} {} of heat transfer into the engine with an efficiency of 42.0%? (b) What is the ratio of heat transfer to the environment to work output? (c) How much work is done?


Assume that the turbines at a coal-powered power plant were upgraded, resulting in an improvement in efficiency of 3.32%. Assume that prior to the upgrade the power station had an efficiency of 36% and that the heat transfer into the engine in one day is still the same at 2.501014 J2.501014 J size 12{2 "." "50""10" rSup { size 8{"14"} } " J"} {}. (a) How much more electrical energy is produced due to the upgrade? (b) How much less heat transfer occurs to the environment due to the upgrade?

(a)  


8.30  10
12

J




8.30  10
12

J

, 

which is 3.32% of 




2.50  10
14

J




2.50  10
14

J

 .(b)  


8.30  10
12

J




8.30  10
12

J

, where the negative sign indicates a reduction in heat transfer to the environment.


This problem compares the energy output and heat transfer to the environment by two different types of nuclear power stationsone with the normal efficiency of 34.0%, and another with an improved efficiency of 40.0%. Suppose both have the same heat transfer into the engine in one day, 2.501014 J2.501014 J size 12{2 "." "50""10" rSup { size 8{"14"} } " J"} {}. (a) How much more electrical energy is produced by the more efficient power station? (b) How much less heat transfer occurs to the environment by the more efficient power station? (One type of more efficient nuclear power station, the gas-cooled reactor, has not been reliable enough to be economically feasible in spite of its greater efficiency.)


Glossary
irreversible process any process that depends on path direction
second law of thermodynamics heat transfer flows from a hotter to a cooler object, never the reverse, and some heat energy in any process is lost to available work in a cyclical process
cyclical process a process in which the path returns to its original state at the end of every cycle
Otto cycle a thermodynamic cycle, consisting of a pair of adiabatic processes and a pair of isochoric processes, that converts heat into work, e.g., the four-stroke engine cycle of intake, compression, ignition, and exhaust

