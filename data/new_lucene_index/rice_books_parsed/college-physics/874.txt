
Transformers
Transformers
Explain how a transformer works.
Calculate voltage, current, and/or number of turns given the other quantities.

Transformers do what their name impliesthey transform voltages from one value to another (The term voltage is used rather than emf, because transformers have internal resistance). For example, many cell phones, laptops, video games, and power tools and small appliances have a transformer built into their plug-in unit (like that in [link]) that changes 120 V or 240 V AC into whatever voltage the device uses. Transformers are also used at several points in the power distribution systems, such as illustrated in [link]. Power is sent long distances at high voltages, because less current is required for a given amount of power, and this means less line loss, as was discussed previously. But high voltages pose greater hazards, so that transformers are employed to produce lower voltage at the users location.
The plug-in transformer has become increasingly familiar with the proliferation of electronic devices that operate on voltages other than common 120 V AC. Most are in the 3 to 12 V range. (credit: Shop Xtreme)


Transformers change voltages at several points in a power distribution system. Electric power is usually generated at greater than 10 kV, and transmitted long distances at voltages over 200 kVsometimes as great as 700 kVto limit energy losses. Local power distribution to neighborhoods or industries goes through a substation and is sent short distances at voltages ranging from 5 to 13 kV. This is reduced to 120, 240, or 480 V for safety at the individual user site.


The type of transformer considered in this textsee [link]is based on Faradays law of induction and is very similar in construction to the apparatus Faraday used to demonstrate magnetic fields could cause currents. The two coils are called the primary and secondary coils. In normal use, the input voltage is placed on the primary, and the secondary produces the transformed output voltage. Not only does the iron core trap the magnetic field created by the primary coil, its magnetization increases the field strength. Since the input voltage is AC, a time-varying magnetic flux is sent to the secondary, inducing its AC output voltage.
A typical construction of a simple transformer has two coils wound on a ferromagnetic core that is laminated to minimize eddy currents. The magnetic field created by the primary is mostly confined to and increased by the core, which transmits it to the secondary coil. Any change in current in the primary induces a current in the secondary.


For the simple transformer shown in [link], the output voltage VsVs size 12{V rSub { size 8{s} } } {} depends almost entirely on the input voltage VpVp size 12{V rSub { size 8{p} } } {} and the ratio of the number of loops in the primary and secondary coils. Faradays law of induction for the secondary coil gives its induced output voltage VsVs size 12{V rSub { size 8{s} } } {} to be
Vs=Nst,Vs=Nst, size 12{V rSub { size 8{s} } = - N rSub { size 8{s} }  {  {}  over  {t} } } {}
where NsNs size 12{N rSub { size 8{s} } } {} is the number of loops in the secondary coil and  size 12{} {}/ tt size 12{t} {} is the rate of change of magnetic flux. Note that the output voltage equals the induced emf (Vs=emfsVs=emfs size 12{V rSub { size 8{s} } ="emf" rSub { size 8{s} } } {}), provided coil resistance is small (a reasonable assumption for transformers). The cross-sectional area of the coils is the same on either side, as is the magnetic field strength, and so 

/t/t size 12{} {}



is the same on either side. The input primary voltage VpVp size 12{V rSub { size 8{p} } } {} is also related to changing flux byVp=Npt.Vp=Npt. size 12{V rSub { size 8{p} } = - N rSub { size 8{p} }  {  {}  over  {t} } } {}
The reason for this is a little more subtle. Lenzs law tells us that the primary coil opposes the change in flux caused by the input voltage VpVp size 12{V rSub { size 8{p} } } {}, hence the minus sign (This is an example of self-inductance, a topic to be explored in some detail in later sections). Assuming negligible coil resistance, Kirchhoffs loop rule tells us that the induced emf exactly equals the input voltage. Taking the ratio of these last two equations yields a useful relationship:VsVp=NsNp.VsVp=NsNp. size 12{ {  {V rSub { size 8{s} } }  over  {V rSub { size 8{p} } } } = {  {N rSub { size 8{s} } }  over  {N rSub { size 8{p} } } } } {}
This is known as the transformer equation, and it simply states that the ratio of the secondary to primary voltages in a transformer equals the ratio of the number of loops in their coils.
The output voltage of a transformer can be less than, greater than, or equal to the input voltage, depending on the ratio of the number of loops in their coils. Some transformers even provide a variable output by allowing connection to be made at different points on the secondary coil. A step-up transformer is one that increases voltage, whereas a step-down transformer decreases voltage. Assuming, as we have, that resistance is negligible, the electrical power output of a transformer equals its input. This is nearly true in practicetransformer efficiency often exceeds 99%. Equating the power input and output,
Pp=IpVp=IsVs=Ps.Pp=IpVp=IsVs=Ps. size 12{P rSub { size 8{p} } =I rSub { size 8{p} } V rSub { size 8{p} } =I rSub { size 8{s} } V rSub { size 8{s} } =P rSub { size 8{s} } } {}
Rearranging terms gives
VsVp=IpIs.VsVp=IpIs. size 12{ {  {V rSub { size 8{s} } }  over  {V rSub { size 8{p} } } } = {  {I rSub { size 8{p} } }  over  {I rSub { size 8{s} } } } } {}
Combining this with VsVp=NsNpVsVp=NsNp size 12{ {  {V rSub { size 8{s} } }  over  {V rSub { size 8{p} } } } = {  {N rSub { size 8{s} } }  over  {N rSub { size 8{p} } } } } {}, we find that






I

s



I

p



=


N

p



N

s













I

s



I

p



=


N

p



N

s







 size 12{ {  {I rSub { size 8{s} } }  over  {I rSub { size 8{p} } } } = {  {N rSub { size 8{p} } }  over  {N rSub { size 8{s} } } } } {}


is the relationship between the output and input currents of a transformer. So if voltage increases, current decreases. Conversely, if voltage decreases, current increases.
Calculating Characteristics of a Step-Up Transformer
A portable x-ray unit has a step-up transformer, the 120 V input of which is transformed to the 100 kV output needed by the x-ray tube. The primary has 50 loops and draws a current of 10.00 A when in use. (a) What is the number of loops in the secondary? (b) Find the current output of the secondary.
Strategy and Solution for (a)

We solve VsVp=NsNpVsVp=NsNp size 12{ {  {V rSub { size 8{s} } }  over  {V rSub { size 8{p} } } } = {  {N rSub { size 8{s} } }  over  {N rSub { size 8{p} } } } } {} for NsNs size 12{N rSub { size 8{s} } } {}, the number of loops in the secondary, and enter the known values. This gives
Ns
=
NpVsVp

=
(50)100,000 V120 V=4.17104.Ns
=
NpVsVp

=
(50)100,000 V120 V=4.17104.alignl { stack {
 size 12{N rSub { size 8{s} } =N rSub { size 8{p} }  {  {V rSub { size 8{s} } }  over  {V rSub { size 8{p} } } } }  {} # 
"      "= \( "50" \)  {  {"100,000 V"}  over  {"120"" V"} } =4 "." "17" times "10" rSup { size 8{4} }  {} 
} } {}
Discussion for (a)

A large number of loops in the secondary (compared with the primary) is required to produce such a large voltage. This would be true for neon sign transformers and those supplying high voltage inside TVs and CRTs.

Strategy and Solution for (b)

We can similarly find the output current of the secondary by solving IsIp=NpNsIsIp=NpNs size 12{ {  {I rSub { size 8{s} } }  over  {I rSub { size 8{p} } } } = {  {N rSub { size 8{p} } }  over  {N rSub { size 8{s} } } } } {} for IsIs size 12{I rSub { size 8{s} } } {} and entering known values. This gives
Is
=

IpNpNs


=
(10.00 A)504.17104=12.0 mA.Is
=

IpNpNs


=
(10.00 A)504.17104=12.0 mA.alignl { stack {
 size 12{I rSub { size 8{s} } =I rSub { size 8{p} }  {  {N rSub { size 8{p} } }  over  {N rSub { size 8{s} } } } }  {} # 
"     "= \( "10" "." "00 A" \)  {  {"50"}  over  {4 "." "17" times "10" rSup { size 8{4} } } } ="12" "." 0" mA" {} 
} } {}
Discussion for (b)

As expected, the current output is significantly less than the input. In certain spectacular demonstrations, very large voltages are used to produce long arcs, but they are relatively safe because the transformer output does not supply a large current. Note that the power input here is Pp=IpVp=(10.00A)(120V)=1.20kWPp=IpVp=(10.00A)(120V)=1.20kW size 12{P rSub { size 8{p} } =I rSub { size 8{p} } V rSub { size 8{p} } = \( "10" "." "00"`A \)  \( "120"`V \) =1 "." "20"`"kW"} {}. This equals the power output Pp=IsVs=(12.0mA)(100kV)=1.20kWPp=IsVs=(12.0mA)(100kV)=1.20kW size 12{P rSub { size 8{p} } =I rSub { size 8{s} } V rSub { size 8{s} } = \( "12" "." 0`"mA" \)  \( "100"`"kV" \) =1 "." "20"`"kW"} {}, as we assumed in the derivation of the equations used.
The fact that transformers are based on Faradays law of induction makes it clear why we cannot use transformers to change DC voltages. If there is no change in primary voltage, there is no voltage induced in the secondary. One possibility is to connect DC to the primary coil through a switch. As the switch is opened and closed, the secondary produces a voltage like that in [link]. This is not really a practical alternative, and AC is in common use wherever it is necessary to increase or decrease voltages.
Transformers do not work for pure DC voltage input, but if it is switched on and off as on the top graph, the output will look something like that on the bottom graph. This is not the sinusoidal AC most AC appliances need.



Calculating Characteristics of a Step-Down Transformer
A battery charger meant for a series connection of ten nickel-cadmium batteries (total emf of 12.5 V DC) needs to have a 15.0 V output to charge the batteries. It uses a step-down transformer with a 200-loop primary and a 120 V input. (a) How many loops should there be in the secondary coil? (b) If the charging current is 16.0 A, what is the input current?

Strategy and Solution for (a)

You would expect the secondary to have a small number of loops. Solving VsVp=NsNpVsVp=NsNp size 12{ {  {V rSub { size 8{s} } }  over  {V rSub { size 8{p} } } } = {  {N rSub { size 8{s} } }  over  {N rSub { size 8{p} } } } } {} for NsNs size 12{N rSub { size 8{s} } } {} and entering known values gives

Ns
=

NpVsVp

=
(200)15.0 V120 V=25.
Ns
=

NpVsVp

=
(200)15.0 V120 V=25.alignl { stack {
 size 12{N rSub { size 8{s} } =N rSub { size 8{p} }  {  {V rSub { size 8{s} } }  over  {V rSub { size 8{p} } } } }  {} # 
"     "= \( "200" \)  {  {"15" "." "0 V"}  over  {"120 V"} } ="25" {} 
} } {}
Strategy and Solution for (b)

The current input can be obtained by solving IsIp=NpNsIsIp=NpNs size 12{ {  {I rSub { size 8{s} } }  over  {I rSub { size 8{p} } } } = {  {N rSub { size 8{p} } }  over  {N rSub { size 8{s} } } } } {} for IpIp size 12{I rSub { size 8{p} } } {} and entering known values. This gives

Ip
=
IsNsNp

=
(16.0 A)25200=2.00 A.
Ip
=
IsNsNp

=
(16.0 A)25200=2.00 A.alignl { stack {
 size 12{I rSub { size 8{p} } =I rSub { size 8{s} }  {  {N rSub { size 8{s} } }  over  {N rSub { size 8{p} } } } }  {} # 
"     "= \( "16" "." "0 A" \)  {  {"25"}  over  {"200"} } =2 "." "00"" A" {} 
} } {}
Discussion

The number of loops in the secondary is small, as expected for a step-down transformer. We also see that a small input current produces a larger output current in a step-down transformer. When transformers are used to operate large magnets, they sometimes have a small number of very heavy loops in the secondary. This allows the secondary to have low internal resistance and produce large currents. Note again that this solution is based on the assumption of 100% efficiencyor power out equals power in (Pp=PsPp=Ps size 12{P rSub { size 8{p} } =P rSub { size 8{s} } } {})reasonable for good transformers. In this case the primary and secondary power is 240 W. (Verify this for yourself as a consistency check.) Note that the Ni-Cd batteries need to be charged from a DC power source (as would a 12 V battery). So the AC output of the secondary coil needs to be converted into DC. This is done using something called a rectifier, which uses devices called diodes that allow only a one-way flow of current.
Transformers have many applications in electrical safety systems, which are discussed in Electrical Safety: Systems and Devices.PhET Explorations: Generator
Generate electricity with a bar magnet! Discover the physics behind the phenomena by exploring magnets and how you can use them to make a bulb light. Generator



Section SummaryTransformers use induction to transform voltages from one value to another.
For a transformer, the voltages across the primary and secondary coils are related by
    VsVp=NsNp,VsVp=NsNp, size 12{ {  {V rSub { size 8{s} } }  over  {V rSub { size 8{p} } } } = {  {N rSub { size 8{s} } }  over  {N rSub { size 8{p} } } } } {}
    
where VpVp size 12{V rSub { size 8{p} } } {} and VsVs size 12{V rSub { size 8{s} } } {} are the voltages across primary and secondary coils having NpNp size 12{N rSub { size 8{p} } } {} and NsNs size 12{N rSub { size 8{s} } } {} turns.
The currents IpIp size 12{I rSub { size 8{p} } } {} and IsIs size 12{I rSub { size 8{s} } } {} in the primary and secondary coils are related by IsIp=NpNsIsIp=NpNs size 12{ {  {I rSub { size 8{s} } }  over  {I rSub { size 8{p} } } } = {  {N rSub { size 8{p} } }  over  {N rSub { size 8{s} } } } } {}.
A step-up transformer increases voltage and decreases current, whereas a step-down transformer decreases voltage and increases current.
Conceptual Questions


Explain what causes physical vibrations in transformers at twice the frequency of the AC power involved.


Problems & Exercises


A plug-in transformer, like that in [link], supplies 9.00 V to a video game system. (a) How many turns are in its secondary coil, if its input voltage is 120 V and the primary coil has 400 turns? (b) What is its input current when its output is 1.30 A?


(a) 30.0
(b) 



9
.

75


10



2





A








9
.

75


10



2





A




 size 12{9 "." "75" times "10" rSup { size 8{ - 2} } `A} {}





An American traveler in New Zealand carries a transformer to convert New Zealands standard 240 V to 120 V so that she can use some small appliances on her trip. (a) What is the ratio of turns in the primary and secondary coils of her transformer? (b) What is the ratio of input to output current? (c) How could a New Zealander traveling in the United States use this same transformer to power her 240 V appliances from 120 V?


A cassette recorder uses a plug-in transformer to convert 120 V to 12.0 V, with a maximum current output of 200 mA. (a) What is the current input? (b) What is the power input? (c) Is this amount of power reasonable for a small appliance?


(a) 20.0 mA
(b) 2.40 W
(c) Yes, this amount of power is quite reasonable for a small appliance.



(a) What is the voltage output of a transformer used for rechargeable flashlight batteries, if its primary has 500 turns, its secondary 4 turns, and the input voltage is 120 V? (b) What input current is required to produce a 4.00 A output? (c) What is the power input?



(a) The plug-in transformer for a laptop computer puts out 7.50 V and can supply a maximum current of 2.00 A. What is the maximum input current if the input voltage is 240 V? Assume 100% efficiency. (b) If the actual efficiency is less than 100%, would the input current need to be greater or smaller? Explain.


(a) 0.063 A(b) Greater input current needed.




A multipurpose transformer has a secondary coil with several points at which a voltage can be extracted, giving outputs of 5.60, 12.0, and 480 V. (a) The input voltage is 240 V to a primary coil of 280 turns. What are the numbers of turns in the parts of the secondary used to produce the output voltages? (b) If the maximum input current is 5.00 A, what are the maximum output currents (each used alone)?



A large power plant generates electricity at 12.0 kV. Its old transformer once converted the voltage to 335 kV. The secondary of this transformer is being replaced so that its output can be 750 kV for more efficient cross-country transmission on upgraded transmission lines. (a) What is the ratio of turns in the new secondary compared with the old secondary? (b) What is the ratio of new current output to old output (at 335 kV) for the same power? (c) If the upgraded transmission lines have the same resistance, what is the ratio of new line power loss to old?


(a) 2.2(b) 0.45(c) 0.20, or 20.0%



If the power output in the previous problem is 1000 MW and line resistance is 2.00 2.00 , what were the old and new line losses?


Unreasonable Results 
The 335 kV AC electricity from a power transmission line is fed into the primary coil of a transformer. The ratio of the number of turns in the secondary to the number in the primary is Ns/Np=1000Ns/Np=1000 size 12{N rSub { size 8{s} } /N rSub { size 8{p} } ="1000"} {}. (a) What voltage is induced in the secondary? (b) What is unreasonable about this result? (c) Which assumption or premise is responsible?

(a) 335 MV(b) way too high, well beyond the breakdown voltage of air over reasonable distances(c) input voltage is too high




Construct Your Own Problem 
Consider a double transformer to be used to create very large voltages. The device consists of two stages. The first is a transformer that produces a much larger output voltage than its input. The output of the first transformer is used as input to a second transformer that further increases the voltage. Construct a problem in which you calculate the output voltage of the final stage based on the input voltage of the first stage and the number of turns or loops in both parts of both transformers (four coils in all). Also calculate the maximum output current of the final stage based on the input current. Discuss the possibility of power losses in the devices and the effect on the output current and power.

Glossary
transformer a device that transforms voltages from one value to another using induction
transformer equation the equation  showing that the ratio of the secondary to primary voltages in a transformer equals the ratio of the number of loops in their coils; VsVp=NsNpVsVp=NsNp size 12{ {  {V rSub { size 8{s} } }  over  {V rSub { size 8{p} } } } = {  {N rSub { size 8{s} } }  over  {N rSub { size 8{p} } } } } {}
step-up transformer a transformer that increases voltage
step-down transformer a transformer that decreases voltage

