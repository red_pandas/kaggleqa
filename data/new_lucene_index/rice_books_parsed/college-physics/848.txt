
Rotation Angle and Angular Velocity
Rotation Angle and Angular Velocity
Define arc length, rotation angle, radius of curvature and angular velocity.
Calculate the angular velocity of a car wheel spin.

In Kinematics, we studied motion along a straight line and introduced such concepts as displacement, velocity, and acceleration. Two-Dimensional Kinematics dealt with motion in two dimensions. Projectile motion is a special case of two-dimensional kinematics in which the object is projected into the air, while being subject to the gravitational force, and lands a distance away. In this chapter, we consider situations where the object does not land but moves in a curve. We begin the study of uniform circular motion by defining two angular quantities needed to describe rotational motion.Rotation AngleWhen objects rotate about some axisfor example, when the CD (compact disc) in [link] rotates about its centereach point in the object follows a circular arc. Consider a line from the center of the CD to its edge. Each pit used to record sound along this line moves through the same angle in the same amount of time. The rotation angle is the amount of rotation and is analogous to linear distance. We define the rotation angle  size 12{} {} to be the ratio of the arc length to the radius of curvature:=sr.=sr. size 12{= {  {s}  over  {r} } "."} {}
All points on a CD travel in circular arcs. The pits along a line from the center to the edge all move through the same angle  size 12{} {} in a time tt size 12{t} {}.



The radius of a circle is rotated through an angle  size 12{} {}. The arc length ss size 12{s} {} is described on the circumference. 


The arc lengthss size 12{s} {} is the distance traveled along a circular path as shown in [link] Note that rr size 12{r} {} is the radius of curvature of the circular path.We know that for one complete revolution, the arc length is the circumference of a circle of radius rr size 12{r} {}. The circumference of a circle is 2r2r size 12{2r} {}. Thus for one complete revolution the rotation angle is
=2rr=2.=2rr=2. size 12{= {  {2r}  over  {r} } =2"."} {}This result is the basis for defining the units used to measure rotation angles,  size 12{} {} to be radians (rad), defined so that2rad = 1 revolution.2rad = 1 revolution. size 12{2" rad "=" 1 revolution."} {}A comparison of some useful angles expressed in both degrees and radians is shown in [link].Comparison of Angular Units

Degree Measures
Radian Measure






30








30




 size 12{"30"} {}








6









6




 size 12{ {  {}  over  {6} } } {}








60








60




 size 12{"60"} {}








3









3




 size 12{ {  {}  over  {3} } } {}








90








90




 size 12{"90"} {}








2









2




 size 12{ {  {}  over  {2} } } {}








120








120




 size 12{"120"} {}







2
3








2
3




 size 12{ {  {2}  over  {3} } } {}








135








135




 size 12{"135"} {}







3
4








3
4




 size 12{ {  {3}  over  {4} } } {}








180








180




 size 12{"180"} {}

















 size 12{} {}


Points 1 and 2 rotate through the same angle ( size 12{} {}), but point 2 moves through a greater arc length ss size 12{ left (s right )} {} because it is at a greater distance from the center of rotation (r)(r) size 12{ \( r \) } {}. 


If =2=2 size 12{=2} {} rad, then the CD has made one complete revolution, and every point on the CD is back at its original position. Because there are 360360 size 12{"360"} {} in a circle or one revolution, the relationship between radians and degrees is thus



2



rad
=
360









2



rad
=
360





 size 12{2" rad"="360" rSup { size 8{ circ } } } {}

so that1rad=360257.3.1rad=360257.3. size 12{1" rad"= {  {"360" rSup { size 8{ circ } } }  over  {2} } ="57" "." 3 rSup { size 8{ circ } } "."} {}Angular Velocity
How fast is an object rotating? We define angular velocity  size 12{} {} as the rate of change of an angle. In symbols, this is
=







t


,=







t


, size 12{= {  {}  over  {t} } ","} {}where an angular rotation  size 12{} {} takes place in a time tt size 12{t} {}. The greater the rotation angle in a given amount of time, the greater the angular velocity. The units for angular velocity are radians per second (rad/s).Angular velocity  size 12{} {} is analogous to linear velocity vv size 12{v} {}. To get the precise relationship between angular and linear velocity, we again consider a pit on the rotating CD. This pit moves an arc length ss size 12{s} {} in a time tt size 12{t} {}, and so it has a linear velocity


v=



s



t


.v=



s



t


. size 12{v= {  {s}  over  {t} } "."} {}
From =sr=sr size 12{= {  {s}  over  {r} } } {} we see that s=
r


s=
r


 size 12{s=r} {}. Substituting this into the expression for vv size 12{v} {} givesv=


r





t


=r.v=


r





t


=r. size 12{v= {  {r}  over  {t} } =r"."} {}
We write this relationship in two different ways and gain two different insights:
v=ror=vr.v=ror=vr. size 12{v=r``"or "= {  {v}  over  {r} } "."} {}The first relationship in v=ror=vrv=ror=vr size 12{v=r``"or "= {  {v}  over  {r} } } {} states that the linear velocity vv size 12{v} {} is proportional to the distance from the center of rotation, thus, it is largest for a point on the rim (largest rr size 12{r} {}), as you might expect. We can also call this linear speed vv size 12{v} {} of a point on the rim the tangential speed. The second relationship in v=ror=vrv=ror=vr size 12{v=r``"or "= {  {v}  over  {r} } } {} can be illustrated by considering the tire of a moving car. Note that the speed of a point on the rim of the tire is the same as the speed vv size 12{v} {} of the car. See [link]. So the faster the car moves, the faster the tire spinslarge vv size 12{v} {} means a large  size 12{} {}, because v=rv=r size 12{v=r} {}. Similarly, a larger-radius tire rotating at the same angular velocity ( size 12{} {}) will produce a greater linear speed (vv size 12{v} {}) for the car.
A car moving at a velocity vv size 12{v} {} to the right has a tire rotating with an angular velocity  size 12{} {}.The speed of the tread of the tire relative to the axle is vv size 12{v} {}, the same as if the car were jacked up. Thus the car moves forward at linear velocity v=rv=r size 12{v=r} {}, where rr size 12{r} {} is the tire radius. A larger angular velocity for the tire means a greater velocity for the car.  
      


How Fast Does a Car Tire Spin?
Calculate the angular velocity of a 0.300 m radius car tire when the car travels at 15.0m/s15.0m/s size 12{"15" "." 0`"m/s"} {} (about 54km/h54km/h size 12{"54"`"km/h"} {}). See [link].Strategy
Because the linear speed of the tire rim is the same as the speed of the car, we have 



v
=
15.0 m/s
.





v
=
15.0 m/s
.


 size 12{v} {}
  

The radius of the tire is given to be 



r
=
0.300 m
.





r
=
0.300 m
.


 size 12{r} {}
 Knowing 



vv size 12{v} {} and rr size 12{r} {}, we can use the second relationship in v=r,=vrv=r,=vr size 12{v=r,``= {  {v}  over  {r} } } {} to calculate the angular velocity.
Solution

To calculate the angular velocity, we will use the following relationship:=vr.=vr. size 12{= {  {v}  over  {r} } "."} {}Substituting the knowns,
=15.0m/s0.300m=50.0rad/s.=15.0m/s0.300m=50.0rad/s. size 12{= {  {"15" "." 0" m/s"}  over  {0 "." "300"" m"} } ="50" "." 0" rad/s."} {}
Discussion

When we cancel units in the above calculation, we get 50.0/s. But the angular velocity must have units of rad/s. Because radians are actually unitless (radians are defined as a ratio of distance), we can simply insert them into the answer for the angular velocity. Also note that if an earth mover with much larger tires, say 1.20 m in radius, were moving at the same speed of 15.0 m/s, its tires would rotate more slowly. They would have an angular velocity =(15.0m/s)/(1.20m)=12.5rad/s.=(15.0m/s)/(1.20m)=12.5rad/s. size 12{= \( "15" "." 0`"m/s" \) / \( 1 "." "20"`m \) ="12" "." 5`"rad/s."} {}
Both  size 12{} {} and vv size 12{v} {} have directions (hence they are angular and linear velocities, respectively). Angular velocity has only two directions with respect to the axis of rotationit is either clockwise or counterclockwise. Linear velocity is tangent to the path, as illustrated in [link].
Take-Home Experiment
Tie an object to the end of a string and swing it around in a horizontal circle above your head (swing at your wrist). Maintain uniform speed as the object swings and measure the angular velocity of the motion. What is the approximate speed of the object? Identify a point close to your hand and take appropriate measurements to calculate the linear speed at this point. Identify other circular motions and measure their angular velocities.
As an object moves in a circle, here a fly on the edge of an old-fashioned vinyl record, its instantaneous velocity is always tangent to the circle. The direction of the angular velocity is clockwise in this case. 



PhET Explorations: Ladybug Revolution
Ladybug Revolution



Join the ladybug in an exploration of rotational motion. Rotate the merry-go-round to change its angle, or choose a constant angular velocity or angular acceleration. Explore how circular motion relates to the bug's x,y position, velocity, and acceleration using vectors or graphs.Section SummaryUniform circular motion is motion in a circle at constant speed. The rotation angle  size 12{} {} is defined as the ratio of the arc length to the radius of curvature:
    
    =sr,=sr, size 12{= {  {s}  over  {r} } ","} {}where arc length ss size 12{s} {} is distance traveled along a circular path and rr size 12{r} {} is the radius of curvature of the circular path. The quantity  size 12{} {} is measured in units of radians (rad), for which2rad=360=1revolution.2rad=360=1revolution. size 12{2`"rad"=`"360""="`1`"revolution."} {}

The conversion between radians and degrees is 1rad=57.31rad=57.3 size 12{1"rad"="57" "." 3} {}.
Angular velocity  size 12{} {} is the rate of change of an angle,
    
    =t,=t, size 12{= {  {}  over  {t} } ","} {}where a rotation  size 12{} {} takes place in a time tt size 12{t} {}. The units of angular velocity are radians per second (rad/s). Linear velocity vv size 12{v} {} and angular velocity  size 12{} {} are related byv=ror=vr.v=ror=vr. size 12{v=r``"or "= {  {v}  over  {r} } "."} {}
Conceptual Questions
There is an analogy between rotational and linear physical quantities. What rotational quantities are analogous to distance and velocity?


Problem Exercises

Semi-trailer trucks have an odometer on one hub of a trailer wheel. The hub is weighted so that it does not rotate, but it contains gears to count the number of wheel revolutionsit then calculates the distance traveled. If the wheel has a 1.15 m diameter and goes through 200,000 rotations, how many kilometers should the odometer read?
723 km

Microwave ovens rotate at a rate of about 6 rev/min. What is this in revolutions per second? What is the angular velocity in radians per second?
An automobile with 0.260 m radius tires travels 80,000 km before wearing them out. How many revolutions do the tires make, neglecting any backing up and any change in radius due to wear?
5107rotations5107rotations size 12{9 "." "79" times "10" rSup { size 8{8} } `"rotations"} {}
(a) What is the period of rotation of Earth in seconds? (b) What is the angular velocity of Earth? (c) Given that Earth has a radius of 6.4106m6.4106m size 12{6 "." 4 times "10" rSup { size 8{6} } } {} at its equator, what is the linear velocity at Earths surface?
A baseball pitcher brings his arm forward during a pitch, rotating the forearm about the elbow. If the velocity of the ball in the pitchers hand is 35.0 m/s and the ball is 0.300 m from the elbow joint, what is the angular velocity of the forearm?

117 rad/s
In lacrosse, a ball is thrown from a net on the end of a stick by rotating the stick and forearm about the elbow. If the angular velocity of the ball about the elbow joint is 30.0 rad/s and the ball is 1.30 m from the elbow joint, what is the velocity of the ball?
A truck with 0.420-m-radius tires travels at 32.0 m/s. What is the angular velocity of the rotating tires in radians per second? What is this in rev/min?
76.2 rad/s 728 rpm
Integrated Concepts When kicking a football, the kicker rotates his leg about the hip joint.
(a) If the velocity of the tip of the kickers shoe is 35.0 m/s and the hip joint is 1.05 m from the tip of the shoe, what is the shoe tips angular velocity?
(b) The shoe is in contact with the initially stationary 0.500 kg football for 20.0 ms. What average force is exerted on the football to give it a velocity of 20.0 m/s? (c) Find the maximum range of the football, neglecting air resistance.


(a) 33.3 rad/s
(b) 500 N
(c) 40.8 m
Construct Your Own Problem
Consider an amusement park ride in which participants are rotated about a vertical axis in a cylinder with vertical walls. Once the angular velocity reaches its full value, the floor drops away and friction between the walls and the riders prevents them from sliding down. Construct a problem in which you calculate the necessary angular velocity that assures the riders will not slide down the wall. Include a free body diagram of a single rider. Among the variables to consider are the radius of the cylinder and the coefficients of friction between the riders clothing and the wall.

Glossary
arc length  ss size 12{s} {}, the distance traveled by an object along a circular path
pit a tiny indentation on the spiral track moulded into the top of the polycarbonate layer of CD
rotation angle the ratio of the arc length to the radius of curvature on a circular path:
                    






=


s

r











=


s

r





 size 12{= {  {s}  over  {r} } } {}


radius of curvature radius of a circular path
radians a unit of angle measurement
angular velocity  size 12{} {},  the rate of change of the angle with which an object moves on a circular path

