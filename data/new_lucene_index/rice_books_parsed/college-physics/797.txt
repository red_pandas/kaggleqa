
Introduction to Fluid Statics
Introduction to Fluid Statics
        class="introduction"
      
class="section-summary" title="Section Summary"
class="conceptual-questions" title="Conceptual Questions"
class="problems-exercises" title="Problems & Exercises"
The fluid essential to all life has a beauty of its own. It also helps support the weight of this swimmer. (credit: Terren, Wikimedia Commons)


Much of what we value in life is fluid: a breath of fresh winter air; the hot blue flame in our gas cooker; the water we drink, swim in, and bathe in; the blood in our veins. What exactly is a fluid? Can we understand fluids with the laws already presented, or will new laws emerge from their study? The physical characteristics of static or stationary fluids and some of the laws that govern their behavior are the topics of this chapter. Fluid Dynamics and Its Biological and Medical Applications explores aspects of fluid flow.
