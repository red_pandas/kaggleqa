
Physics of the Eye
Physics of the Eye
Explain the image formation by the eye.
Explain why peripheral images lack detail and color.
Define refractive indices.
Analyze the accommodation of the eye for distant and near vision.

The eye is perhaps the most interesting of all optical instruments. The eye is remarkable in how it forms images and in the richness of detail and color it can detect. However, our eyes commonly need some correction, to reach what is called normal vision, but should be called ideal rather than normal. Image formation by our eyes and common vision correction are easy to analyze with the optics discussed in Geometric Optics.[link] shows the basic anatomy of the eye. The cornea and lens form a system that, to a good approximation, acts as a single thin lens. For clear vision, a real image must be projected onto the light-sensitive retina, which lies at a fixed distance from the lens. The lens of the eye adjusts its power to produce an image on the retina for objects at different distances. The center of the image falls on the fovea, which has the greatest density of light receptors and the greatest acuity (sharpness) in the visual field. The variable opening (or pupil) of the eye along with chemical adaptation allows the eye to detect light intensities from the lowest observable to 10101010 size 12{"10" rSup { size 8{"10"} } } {} times greater (without damage). This is an incredible range of detection. Our eyes perform a vast number of functions, such as sense direction, movement, sophisticated colors, and distance. Processing of visual nerve impulses begins with interconnections in the retina and continues in the brain. The optic nerve conveys signals received by the eye to the brain. 
The cornea and lens of an eye act together to form a real image on the light-sensing retina, which has its densest concentration of receptors in the fovea and a blind spot over the optic nerve. The power of the lens of an eye is adjustable to provide an image on the retina for varying object distances. Layers of tissues with varying indices of refraction in the lens are shown here. However, they have been omitted from other pictures for clarity.


Refractive indices are crucial to image formation using lenses. [link] shows refractive indices relevant to the eye. The biggest change in the refractive index, and bending of rays, occurs at the cornea rather than the lens. The ray diagram in [link] shows image formation by the cornea and lens of the eye. The rays bend according to the refractive indices provided in [link]. The cornea provides about two-thirds of the power of the eye, owing to the fact that speed of light changes considerably while traveling from air into cornea. The lens provides the remaining power needed to produce an image on the retina. The cornea and lens can be treated as a single thin lens, even though the light rays pass through several layers of material (such as cornea, aqueous humor, several layers in the lens, and vitreous humor), changing direction at each interface. The image formed is much like the one produced by a single convex lens. This is a case 1 image. Images formed in the eye are inverted but the brain inverts them once more to make them seem upright.Refractive Indices Relevant to the EyeMaterial
Index of Refraction

    Water1.33Air 1.0Cornea 1.38Aqueous humor 1.34Lens 1.41 average (varies throughout the lens, greatest in center)Vitreous humor 1.34An image is formed on the retina with light rays converging most at the cornea and upon entering and exiting the lens. Rays from the top and bottom of the object are traced and produce an inverted real image on the retina. The distance to the object is drawn smaller than scale.


As noted, the image must fall precisely on the retina to produce clear vision  that is, the image distance didi size 12{d rSub { size 8{i} } } {} must equal the lens-to-retina distance. Because the lens-to-retina distance does not change, the image distance didi size 12{d rSub { size 8{i} } } {} must be the same for objects at all distances. The eye manages this by varying the power (and focal length) of the lens to accommodate for objects at various distances. The process of adjusting the eyes focal length is called accommodation. A person with normal (ideal) vision can see objects clearly at distances ranging from 25 cm to essentially infinity. However, although the near point (the shortest distance at which a sharp focus can be obtained) increases with age (becoming meters for some older people), we will consider it to be 25 cm in our treatment here.[link] shows the accommodation of the eye for distant and near vision. Since light rays from a nearby object can diverge and still enter the eye, the lens must be more converging (more powerful) for close vision than for distant vision. To be more converging, the lens is made thicker by the action of the ciliary muscle surrounding it. The eye is most relaxed when viewing distant objects, one reason that microscopes and telescopes are designed to produce distant images. Vision of very distant objects is called totally relaxed, while close vision is termed accommodated, with the closest vision being fully accommodated.
Relaxed and accommodated vision for distant and close objects. (a) Light rays from the same point on a distant object must be nearly parallel while entering the eye and more easily converge to produce an image on the retina. (b) Light rays from a nearby object can diverge more and still enter the eye. A more powerful lens is needed to converge them on the retina than if they were parallel.


We will use the thin lens equations to examine image formation by the eye quantitatively. First, note the power of a lens is given as p=1/fp=1/f size 12{p= {1} slash {f} } {}, so we rewrite the thin lens equations as




P
=


1

d

o



+

1

d

i











P
=


1

d

o



+

1

d

i








and







h

i



h

o



=




d

i



d

o





=
m.










h

i



h

o



=




d

i



d

o





=
m.




We understand that didi size 12{d rSub { size 8{i} } } {} must equal the lens-to-retina distance to obtain clear vision, and that normal vision is possible for objects at distances do=25 cmdo=25 cm to infinity.Take-Home Experiment: The Pupil
Look at the central transparent area of someones eye, the pupil, in normal room light. Estimate the diameter of the pupil. Now turn off the lights and darken the room. After a few minutes turn on the lights and promptly estimate the diameter of the pupil. What happens to the pupil as the eye adjusts to the room light? Explain your observations. 
The eye can detect an impressive amount of detail, considering how small the image is on the retina. To get some idea of how small the image can be, consider the following example.Size of Image on Retina
What is the size of the image on the retina of a 1.201021.20102 size 12{1 "." "20" times "10" rSup { size 8{ - 2} } } {} cm diameter human hair, held at arms length (60.0 cm) away? Take the lens-to-retina distance to be 2.00 cm.
Strategy
We want to find the height of the image hihi, given the height of the object is ho=1.20102ho=1.20102 size 12{h rSub { size 8{0} } =1 "." "20" times "10" rSup { size 8{ - 2} } } {} cm. We also know that the object is 60.0 cm away, so that do=60.0 cmdo=60.0 cm size 12{d rSub { size 8{0} } } {}. For clear vision, the image distance must equal the lens-to-retina distance, and so di=2.00 cmdi=2.00 cm size 12{d rSub { size 8{i} } } {}  . The equation hiho=dido=mhiho=dido=m size 12{ {  {h rSub { size 8{i} } }  over  {h rSub { size 8{o} } } } = -  {  {d rSub { size 8{i} } }  over  {d rSub { size 8{o} } } } =m} {} can be used to find hihi with the known information.
Solution

The only unknown variable in the equation hiho=dido=mhiho=dido=m size 12{ {  {h rSub { size 8{i} } }  over  {h rSub { size 8{o} } } } = -  {  {d rSub { size 8{i} } }  over  {d rSub { size 8{o} } } } =m} {} is hihi size 12{h rSub { size 8{i} } } {}:





h

i



h

o



=




d

i



d

o





.







h

i



h

o



=




d

i



d

o





.


Rearranging to isolate hihi size 12{h rSub { size 8{i} } } {} yields




h

i


=




h

o






d

i



d

o





.






h

i


=




h

o






d

i



d

o





.


Substituting the known values gives




 
h

i


 =







(


1.20



10


2



cm
)


2.00 cm


60.0 cm






 =




4.00


10


4



cm
.









 
h

i


 =







(


1.20



10


2



cm
)


2.00 cm


60.0 cm






 =




4.00


10


4



cm
.







Discussion

This truly small image is not the smallest discerniblethat is, the limit to visual acuity is even smaller than this. Limitations on visual acuity have to do with the wave properties of light and will be discussed in the next chapter. Some limitation is also due to the inherent anatomy of the eye and processing that occurs in our brain.

Power Range of the Eye
Calculate the power of the eye when viewing objects at the greatest and smallest distances possible with normal vision, assuming a lens-to-retina distance of 2.00 cm (a typical value).

Strategy

For clear vision, the image must be on the retina, and so di=2.00 cmdi=2.00 cm   here. For distant vision, dodo, and for close vision, do=25.0 cmdo=25.0 cm, as discussed earlier. The equation P=1do+1diP=1do+1di as written just above, can be used directly to solve for PP in both cases, since we know didi and 
dodo size 12{d rSub { size 8{0} } } {}. Power has units of diopters, where 
1 D=1/m1 D=1/m size 12{"1D"="1/m"} {}, and so we should express all distances in meters.
Solution

For distant vision,





P
=


1

d

o



+

1

d

i





=


1


+

1

0.0200 m


.









P
=


1

d

o



+

1

d

i





=


1


+

1

0.0200 m


.





Since 1/=01/=0 size 12{1/ infinity =0} {}, this givesP=0+50.0/m=50.0 D (distant vision).P=0+50.0/m=50.0 D (distant vision). Now, for close vision,





P
 =







1

d

o



+

1

d

i





=


1

0.250 m


+

1

0.0200 m







=




4.00

m

+


50.0

m

=

4.00 D
+
50.0 D


=


54.0 D (close vision).










P
 =







1

d

o



+

1

d

i





=


1

0.250 m


+

1

0.0200 m







=




4.00

m

+


50.0

m

=

4.00 D
+
50.0 D


=


54.0 D (close vision).





alignl { stack {
 size 12{P= {  {1}  over  {d rSub { size 8{o} } } } + {  {1}  over  {d rSub { size 8{i} } } } = {  {1}  over  {0 "." "250 m"} } + {  {1}  over  {0 "." "0200 m"} } }  {} # 
= {  {4 "." "00"}  over  {m} } + {  {"50" "." 0}  over  {m} } =4 "." "00"D+"50" "." 0D {} # 
="54" "." 0D {} 
} } {}


Discussion

For an eye with this typical 2.00 cm lens-to-retina distance, the power of the eye ranges from 50.0 D (for distant totally relaxed vision) to 54.0 D (for close fully accommodated vision), which is an 8% increase. This increase in power for close vision is consistent with the preceding discussion and the ray tracing in [link]. An 8% ability to accommodate is considered normal but is typical for people who are about 40 years old. Younger people have greater accommodation ability, whereas older people gradually lose the ability to accommodate. When an optometrist identifies accommodation as a problem in elder people, it is most likely due to stiffening of the lens. The lens of the eye changes with age in ways that tend to preserve the ability to see distant objects clearly but do not allow the eye to accommodate for close vision, a condition called presbyopia (literally, elder eye). To correct this vision defect, we place a converging, positive power lens in front of the eye, such as found in reading glasses. Commonly available reading glasses are rated by their power in diopters, typically ranging from 1.0 to 3.5 D.
Section SummaryImage formation by the eye is adequately described by the thin lens equations:
    P=1do+1di

 and

hiho=dido=m.P=1do+1di andhiho=dido=m. size 12{ {  {h rSub { size 8{i} } }  over  {h rSub { size 8{o} } } } = -  {  {d rSub { size 8{i} } }  over  {d rSub { size 8{o} } } } =m} {}
The eye produces a real image on the retina by adjusting its focal length and power in a process called accommodation.
For close vision, the eye is fully accommodated and has its greatest power, whereas for distant vision, it is totally relaxed and has its smallest power.
The loss of the ability to accommodate with age is called presbyopia, which is corrected by the use of a converging lens to add power for close vision.
Conceptual Questions
If the lens of a persons eye is removed because of cataracts (as has been done since ancient times), why would you expect a spectacle lens of about 16 D to be prescribed?

 A cataract is cloudiness in the lens of the eye. Is light dispersed or diffused by it?



When laser light is shone into a relaxed normal-vision eye to repair a tear by spot-welding the retina to the back of the eye, the rays entering the eye must be parallel. Why?
  

How does the power of a dry contact lens compare with its power when resting on the tear layer of the eye? Explain.


Why is your vision so blurry when you open your eyes while swimming under water? How does a face mask enable clear vision?

Problem Exercises
Unless otherwise stated, the lens-to-retina distance is 2.00 cm.

 What is the power of the eye when viewing an object 50.0 cm away?






52.0 D







52.0 D






 Calculate the power of the eye when viewing an object 3.00 m away.


(a) The print in many books averages 3.50 mm in height. How high is the image of the print on the retina when the book is held 30.0 cm from the eye? 
(b) Compare the size of the print to the sizes of rods and cones in the fovea and discuss the possible details observable in the letters. (The eye-brain system can perform better because of interconnections and higher order image processing.)


(a)
        





0

.
233 mm










0

.
233 mm




 size 12{ - 0 "." "233"" mm"} {}


(b) The size of the rods and the cones is smaller than the image height, so we can distinguish letters on a page.
 
Suppose a certain persons visual acuity is such that he can see objects clearly that form an image 4.00 m4.00 m high on his retina. What is the maximum distance at which he can read the 75.0 cm high letters on the side of an airplane?
  


 People who do very detailed work close up, such as jewellers, often can see objects clearly at much closer distance than the normal 25 cm.
   
(a) What is the power of the eyes of a woman who can see an object clearly at a distance of only 8.00 cm?
  

(b) What is the size of an image of a 1.00 mm object, such as lettering inside a ring, held at this distance? 
  

(c) What would the size of the image be if the object were held at the normal 25.0 cm distance?
  


(a)  +62.5 D+62.5 D(b)  0.250 mm0.250 mm(c)  0.0800 mm0.0800 mm

Glossary
accommodation the ability of the eye to adjust its focal length is known as accommodation
presbyopia a condition in which the lens of the eye becomes progressively unable to focus on objects close to the viewer

