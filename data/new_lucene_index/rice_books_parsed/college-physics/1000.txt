
Ultrasound
Ultrasound
Define acoustic impedance and intensity reflection coefficient.
Describe medical and other uses of ultrasound technology.
Calculate acoustic impedance using density values and the speed of ultrasound.
Calculate the velocity of a moving object using Doppler-shifted ultrasound.

Ultrasound is used in medicine to painlessly and noninvasively monitor patient health and diagnose a wide range of disorders. (credit: abbybatchelder, Flickr)


Any sound with a frequency above 20,000 Hz (or 20 kHz)that is, above the highest audible frequencyis defined to be ultrasound. In practice, it is possible to create ultrasound frequencies up to more than a gigahertz. (Higher frequencies are difficult to create; furthermore, they propagate poorly because they are very strongly absorbed.) Ultrasound has a tremendous number of applications, which range from burglar alarms to use in cleaning delicate objects to the guidance systems of bats. We begin our discussion of ultrasound with some of its applications in medicine, in which it is used extensively both for diagnosis and for therapy.
Characteristics of Ultrasound
The characteristics of ultrasound, such as frequency and intensity, are wave properties common to all types of waves. Ultrasound also has a wavelength that limits the fineness of detail it can detect. This characteristic is true of all waves. We can never observe details significantly smaller than the wavelength of our probe; for example, we will never see individual atoms with visible light, because the atoms are so small compared with the wavelength of light.

Ultrasound in Medical Therapy
Ultrasound, like any wave, carries energy that can be absorbed by the medium carrying it, producing effects that vary with intensity. When focused to intensities of 103103 to 105105 W/m2W/m2, ultrasound can be used to shatter gallstones or pulverize cancerous tissue in surgical procedures. (See [link].) Intensities this great can damage individual cells, variously causing their protoplasm to stream inside them, altering their permeability, or rupturing their walls through cavitation. Cavitation is the creation of vapor cavities in a fluidthe longitudinal vibrations in ultrasound alternatively compress and expand the medium, and at sufficient amplitudes the expansion separates molecules. Most cavitation damage is done when the cavities collapse, producing even greater shock pressures.The tip of this small probe oscillates at 23 kHz with such a large amplitude that it pulverizes tissue on contact. The debris is then aspirated. The speed of the tip may exceed the speed of sound in tissue, thus creating shock waves and cavitation, rather than a smooth simple harmonic oscillatortype wave.


Most of the energy carried by high-intensity ultrasound in tissue is converted to thermal energy. In fact, intensities of 103103 size 12{"10" rSup { size 8{3} } } {} to 104W/m2104W/m2 size 12{"10" rSup { size 8{4} } "W/m" rSup { size 8{2} } } {} are commonly used for deep-heat treatments called ultrasound diathermy. Frequencies of 0.8 to 1 MHz are typical. In both athletics and physical therapy, ultrasound diathermy is most often applied to injured or overworked muscles to relieve pain and improve flexibility. Skill is needed by the therapist to avoid bone burns and other tissue damage caused by overheating and cavitation, sometimes made worse by reflection and focusing of the ultrasound by joint and bone tissue.In some instances, you may encounter a different decibel scale, called the sound pressure level, when ultrasound travels in water or in human and other biological tissues. We shall not use the scale here, but it is notable that numbers for sound pressure levels range 60 to 70 dB higher than you would quote for  size 12{} {}, the sound intensity level used in this text. Should you encounter a sound pressure level of 220 decibels, then, it is not an astronomically high intensity, but equivalent to about 155 dBhigh enough to destroy tissue, but not as unreasonably high as it might seem at first.
Ultrasound in Medical Diagnostics
When used for imaging, ultrasonic waves are emitted from a transducer, a crystal exhibiting the piezoelectric effect (the expansion and contraction of a substance when a voltage is applied across it, causing a vibration of the crystal). These high-frequency vibrations are transmitted into any tissue in contact with the transducer. Similarly, if a pressure is applied to the crystal (in the form of a wave reflected off tissue layers), a voltage is produced which can be recorded. The crystal therefore acts as both a transmitter and a receiver of sound. Ultrasound is also partially absorbed by tissue on its path, both on its journey away from the transducer and on its return journey. From the time between when the original signal is sent and when the reflections from various boundaries between media are received, (as well as a measure of the intensity loss of the signal), the nature and position of each boundary between tissues and organs may be deduced.
Reflections at boundaries between two different media occur because of differences in a characteristic known as the acoustic impedance ZZ size 12{Z} {} of each substance. Impedance is defined as
Z=v,Z=v, size 12{Z= ital "pv"} {}where  is the density of the medium (in kg/m3kg/m3) and vv size 12{v} {} is the speed of sound through the medium (in m/s). The units for ZZ are therefore kg/(m2s)kg/(m2s).[link] shows the density and speed of sound through various media (including various soft tissues) and the associated acoustic impedances. Note that the acoustic impedances for soft tissue do not vary much but that there is a big difference between the acoustic impedance of soft tissue and air and also between soft tissue and bone.
The Ultrasound Properties of Various Media, Including Soft Tissue Found in the Body
Medium
Density (kg/m3)
Speed of Ultrasound (m/s)

              Acoustic Impedance 


(

kg/
(


m

2



s

)

)





(

kg/
(


m

2



s

)

)





Air
1.3
330
429429

Water
1000
1500
1.51061.5106

Blood
1060
1570
1.661061.66106

Fat
925
1450
1.341061.34106

Muscle (average)
1075
1590
1.701061.70106

Bone (varies)
14001900
4080
5.71065.7106 to 7.81067.8106

Barium titanate (transducer material)
5600
5500
30.810630.8106
At the boundary between media of different acoustic impedances, some of the wave energy is reflected and some is transmitted. The greater the difference in acoustic impedance between the two media, the greater the reflection and the smaller the transmission.
The intensity reflection coefficient aa size 12{a} {} is defined as the ratio of the intensity of the reflected wave relative to the incident (transmitted) wave. This statement can be written mathematically asa=Z2Z12Z1+Z22,a=Z2Z12Z1+Z22, size 12{a= {  { left (Z rSub { size 8{2} }  - Z rSub { size 8{1} }  right ) rSup { size 8{2} } }  over  { left (Z rSub { size 8{1} } +Z rSub { size 8{2} }  right ) rSup { size 8{2} } } } } {}where Z1Z1 size 12{Z rSub { size 8{1} } } {} and Z2Z2 size 12{Z rSub { size 8{2} } } {} are the acoustic impedances of the two media making up the boundary. A reflection coefficient of zero (corresponding to total transmission and no reflection) occurs when the acoustic impedances of the two media are the same. An impedance match (no reflection) provides an efficient coupling of sound energy from one medium to another. The image formed in an ultrasound is made by tracking reflections (as shown in [link]) and mapping the intensity of the reflected sound waves in a two-dimensional plane.
Calculate Acoustic Impedance and Intensity Reflection Coefficient: Ultrasound and Fat Tissue
(a) Using the values for density and the speed of ultrasound given in [link], show that the acoustic impedance of fat tissue is indeed 1.34106kg/(m2s)1.34106kg/(m2s).(b) Calculate the intensity reflection coefficient of ultrasound when going from fat to muscle tissue.
Strategy for (a)
The acoustic impedance can be calculated using Z=vZ=v size 12{Z= ital "pv"} {} and the values for  and vv found in [link].
    Solution for (a)
(1) Substitute known values from [link] into Z=vZ=v size 12{Z= ital "pv"} {}.





Z
=
v

=


925 kg

/m

3







1450 m/s












Z
=
v

=


925 kg

/m

3







1450 m/s






 size 12{Z=v= left ("925"" kg/m" rSup { size 8{3} }  right ) left ("1450"" m/s" right )} {}

(2) Calculate to find the acoustic impedance of fat tissue.1.34106kg/(m2s)1.34106kg/(m2s)This value is the same as the value given for the acoustic impedance of fat tissue.
Strategy for (b)
The intensity reflection coefficient for any boundary between two media is given by a=Z2Z12Z1+Z22a=Z2Z12Z1+Z22 size 12{a= {  { left (Z rSub { size 8{2} }  - Z rSub { size 8{1} }  right ) rSup { size 8{2} } }  over  { left (Z rSub { size 8{1} } + Z rSub { size 8{2} }  right ) rSup { size 8{2} } } } } {}, and the acoustic impedance of muscle is given in [link].Solution for (b)Substitute known values into a=Z2Z12Z1+Z22a=Z2Z12Z1+Z22 size 12{a= {  { left (Z rSub { size 8{2} }  - Z rSub { size 8{1} }  right ) rSup { size 8{2} } }  over  { left (Z rSub { size 8{1} } + Z rSub { size 8{2} }  right ) rSup { size 8{2} } } } } {} to find the intensity reflection coefficient:






a
=





Z

2




Z

1





2






Z

1


+

Z

2





2




=




1
.

34


10

6




kg/(m

2



  s)

1.70


10

6




kg/(m

2


  s)



2





1
.

70


10

6




kg/(m

2



  s)
+
1

.

34


10

6




kg/(m

2


  s)



2




=
0

.
014











a
=





Z

2




Z

1





2






Z

1


+

Z

2





2




=




1
.

34


10

6




kg/(m

2



  s)

1.70


10

6




kg/(m

2


  s)



2





1
.

70


10

6




kg/(m

2



  s)
+
1

.

34


10

6




kg/(m

2


  s)



2




=
0

.
014




 size 12{a= {  { left (Z rSub { size 8{2} }  - Z rSub { size 8{1} }  right ) rSup { size 8{2} } }  over  { left (Z rSub { size 8{1} } +Z rSub { size 8{2} }  right ) rSup { size 8{2} } } } = {  { left (1 "." "34" times "10" rSup { size 8{6} } "kgm" rSup { size 8{"-2"} } s rSup { size 8{ - 1} }  - 1 "." "70" times "10" rSup { size 8{6} } "kgm" rSup { size 8{"-2"} } s rSup { size 8{"-1"} }  right ) rSup { size 8{2} } }  over  { left (1 "." "70" times "10" rSup { size 8{6} } "kgm" rSup { size 8{"-2"} } s rSup { size 8{ - 1} } +1 "." "34" times "10" rSup { size 8{6} } "kgm" rSup { size 8{"-2"} } s rSup { size 8{"-1"} }  right ) rSup { size 8{2} } } } =0 "." "014"} {}

Discussion
This result means that only 1.4% of the incident intensity is reflected, with the remaining being transmitted.

The applications of ultrasound in medical diagnostics have produced untold benefits with no known risks. Diagnostic intensities are too low (about 102W/m2102W/m2 size 12{"10" rSup { size 8{ - 2} } "W/m" rSup { size 8{2} } } {}) to cause thermal damage. More significantly, ultrasound has been in use for several decades and detailed follow-up studies do not show evidence of ill effects, quite unlike the case for x-rays.(a) An ultrasound speaker doubles as a microphone. Brief bleeps are broadcast, and echoes are recorded from various depths. (b) Graph of echo intensity versus time. The time for echoes to return is directly proportional to the distance of the reflector, yielding this information noninvasively.


The most common ultrasound applications produce an image like that shown in [link]. The speaker-microphone broadcasts a directional beam, sweeping the beam across the area of interest. This is accomplished by having multiple ultrasound sources in the probes head, which are phased to interfere constructively in a given, adjustable direction. Echoes are measured as a function of position as well as depth. A computer constructs an image that reveals the shape and density of internal structures.
(a) An ultrasonic image is produced by sweeping the ultrasonic beam across the area of interest, in this case the womans abdomen. Data are recorded and analyzed in a computer, providing a two-dimensional image. (b) Ultrasound image of 12-week-old fetus. (credit: Margaret W. Carruthers, Flickr)


How much detail can ultrasound reveal? The image in [link] is typical of low-cost systems, but that in [link] shows the remarkable detail possible with more advanced systems, including 3D imaging. Ultrasound today is commonly used in prenatal care. Such imaging can be used to see if the fetus is developing at a normal rate, and help in the determination of serious problems early in the pregnancy. Ultrasound is also in wide use to image the chambers of the heart and the flow of blood within the beating heart, using the Doppler effect (echocardiology).
Whenever a wave is used as a probe, it is very difficult to detect details smaller than its wavelength  size 12{} {}. Indeed, current technology cannot do quite this well. Abdominal scans may use a 7-MHz frequency, and the speed of sound in tissue is about 1540 m/sso the wavelength limit to detail would be =vwf=1540 m/s7106Hz=0.22 mm=vwf=1540 m/s7106Hz=0.22 mm size 12{= {  {v rSub { size 8{w} } }  over  {f} } = {  {"1540""m/s"}  over  {7 times "10" rSup { size 8{6} } "Hz"} } =0 "." "22""mm"} {}. In practice, 1-mm detail is attainable, which is sufficient for many purposes. Higher-frequency ultrasound would allow greater detail, but it does not penetrate as well as lower frequencies do. The accepted rule of thumb is that you can effectively scan to a depth of about  500500 size 12{} {} into tissue. For 7 MHz, this penetration limit is 5000.22 mm5000.22 mm, which is 0.11 m. Higher frequencies may be employed in smaller organs, such as the eye, but are not practical for looking deep into the body.A 3D ultrasound image of a fetus. As well as for the detection of any abnormalities, such scans have also been shown to be useful for strengthening the emotional bonding between parents and their unborn child. (credit: Jennie Cu, Wikimedia Commons)


In addition to shape information, ultrasonic scans can produce density information superior to that found in X-rays, because the intensity of a reflected sound is related to changes in density. Sound is most strongly reflected at places where density changes are greatest.
Another major use of ultrasound in medical diagnostics is to detect motion and determine velocity through the Doppler shift of an echo, known as Doppler-shifted ultrasound. This technique is used to monitor fetal heartbeat, measure blood velocity, and detect occlusions in blood vessels, for example. (See [link].) The magnitude of the Doppler shift in an echo is directly proportional to the velocity of whatever reflects the sound. Because an echo is involved, there is actually a double shift. The first occurs because the reflector (say a fetal heart) is a moving observer and receives a Doppler-shifted frequency. The reflector then acts as a moving source, producing a second Doppler shift.
This Doppler-shifted ultrasonic image of a partially occluded artery uses color to indicate velocity. The highest velocities are in red, while the lowest are blue. The blood must move faster through the constriction to carry the same flow. (credit: Arning C, Grzyska U, Wikimedia Commons)


A clever technique is used to measure the Doppler shift in an echo. The frequency of the echoed sound is superimposed on the broadcast frequency, producing beats. The beat frequency is FB=f1f2FB=f1f2 size 12{F rSub { size 8{b} } = \lline f rSub { size 8{1} }  - f rSub { size 8{2} }  \rline } {}, and so it is directly proportional to the Doppler shift (f1f2f1f2 size 12{f rSub { size 8{1} }  - f rSub { size 8{2} } } {}) and hence, the reflectors velocity. The advantage in this technique is that the Doppler shift is small (because the reflectors velocity is small), so that great accuracy would be needed to measure the shift directly. But measuring the beat frequency is easy, and it is not affected if the broadcast frequency varies somewhat. Furthermore, the beat frequency is in the audible range and can be amplified for audio feedback to the medical observer.Uses for Doppler-Shifted Radar
Doppler-shifted radar echoes are used to measure wind velocities in storms as well as aircraft and automobile speeds. The principle is the same as for Doppler-shifted ultrasound. There is evidence that bats and dolphins may also sense the velocity of an object (such as prey) reflecting their ultrasound signals by observing its Doppler shift.
Calculate Velocity of Blood: Doppler-Shifted Ultrasound
Ultrasound that has a frequency of 2.50 MHz is sent toward blood in an artery that is moving toward the source at 20.0 cm/s, as illustrated in [link]. Use the speed of sound in human tissue as 1540 m/s. (Assume that the frequency of 2.50 MHz is accurate to seven significant figures.)What frequency does the blood receive?
What frequency returns to the source?
What beat frequency is produced if the source and returning frequencies are mixed?

Ultrasound is partly reflected by blood cells and plasma back toward the speaker-microphone. Because the cells are moving, two Doppler shifts are producedone for blood as a moving observer, and the other for the reflected sound coming from a moving source. The magnitude of the shift is directly proportional to blood velocity.


Strategy
The first two questions can be answered using fobs=fsvwvwvsfobs=fsvwvwvs size 12{f rSub { size 8{"obs"} } =f rSub { size 8{s} }  left ( {  {v rSub { size 8{w} } }  over  {v rSub { size 8{w} }  +- v rSub { size 8{s} } } }  right )} {} and fobs=fsvwvobsvwfobs=fsvwvobsvw size 12{f rSub { size 8{"obs"} } =f rSub { size 8{s} }  left ( {  {v rSub { size 8{w} }  +- v rSub { size 8{"obs"} } }  over  {v rSub { size 8{w}  } } }  right )} {} for the Doppler shift. The last question asks for beat frequency, which is the difference between the original and returning frequencies.
Solution for (a)

(1) Identify knowns:
The blood is a moving observer, and so the frequency it receives is given by fobs=fsvwvobsvw.fobs=fsvwvobsvw. size 12{f rSub { size 8{"obs"} } =f rSub { size 8{s} }  left ( {  {v rSub { size 8{w} }  +- v rSub { size 8{"obs"} } }  over  {v rSub { size 8{w}  } } }  right )} {}
vbvb size 12{v rSub { size 8{b} } } {} is the blood velocity (vobsvobs size 12{v rSub { size 8{"obs"} } } {} here) and the plus sign is chosen because the motion is toward the source.(2) Enter the given values into the equation.





f

obs


=


2,
500
,
000 Hz






1540 m/s

+
0

.
2 m/s

1540 m/s












f

obs


=


2,
500
,
000 Hz






1540 m/s

+
0

.
2 m/s

1540 m/s






 size 12{f rSub { size 8{"obs"} } = left (2,"500","000"" Hz" right ) left ( {  {"1540"" m/s"+0 "." "2 m/s"}  over  {"1540 m/s"} }  right )} {}
 (3) Calculate to find the frequency: 20,500,325 Hz.
Solution for (b)

(1) Identify knowns:
The blood acts as a moving source.
The microphone acts as a stationary observer.
The frequency leaving the blood is 2,500,325 Hz, but it is shifted upward as given byfobs=fsvwvwvb.fobs=fsvwvwvb. size 12{f rSub { size 8{"obs"} } =f rSub { size 8{s} }  left ( {  {v rSub { size 8{w} } }  over  {v rSub { size 8{w} }  +- v rSub { size 8{b} } } }  right )} {}fobsfobs is the frequency received by the speaker-microphone.
The source velocity is vbvb size 12{v rSub { size 8{b} } } {}.
The minus sign is used because the motion is toward the observer.
The minus sign is used because the motion is toward the observer.
(2) Enter the given values into the equation:





f

obs


=


2,
500
,
325 Hz






1540 m/s



1540 m/s 

0

.
200 m/s













f

obs


=


2,
500
,
325 Hz






1540 m/s



1540 m/s 

0

.
200 m/s







 size 12{f rSub { size 8{"obs"} } = left (2,"500","325"" Hz" right ) left ( {  {"1540"" m/s"}  over  {"1540 m/s " - 0 "." "200"" m/s"} }  right )} {}

(3) Calculate to find the frequency returning to the source: 2,500,649 Hz.
Solution for (c)

(1) Identify knowns:
The beat frequency is simply the absolute value of the difference between fsfs size 12{f rSub { size 8{s} } } {} and fobsfobs size 12{f rSub { size 8{"obs"} } } {}, as stated in: fB=fobsfs.fB=fobsfs.(2) Substitute known values:






2,
500
,
649


Hz

2,

500
,
000

Hz












2,
500
,
649


Hz

2,

500
,
000

Hz






 size 12{ lline 2,"500","649"`"Hz" - 2,"500","000"`"Hz" rline } {}

(3) Calculate to find the beat frequency: 649 Hz.DiscussionThe Doppler shifts are quite small compared with the original frequency of 2.50 MHz. It is far easier to measure the beat frequency than it is to measure the echo frequency with an accuracy great enough to see shifts of a few hundred hertz out of a couple of megahertz. Furthermore, variations in the source frequency do not greatly affect the beat frequency, because both fsfs size 12{f rSub { size 8{s} } } {} and fobsfobs size 12{f rSub { size 8{"obs"} } } {}would increase or decrease. Those changes subtract out in fB=fobsfs.fB=fobsfs.
Industrial and Other Applications of Ultrasound
Industrial, retail, and research applications of ultrasound are common. A few are discussed here. Ultrasonic cleaners have many uses. Jewelry, machined parts, and other objects that have odd shapes and crevices are immersed in a cleaning fluid that is agitated with ultrasound typically about 40 kHz in frequency. The intensity is great enough to cause cavitation, which is responsible for most of the cleansing action. Because cavitation-produced shock pressures are large and well transmitted in a fluid, they reach into small crevices where even a low-surface-tension cleaning fluid might not penetrate.Sonar is a familiar application of ultrasound. Sonar typically employs ultrasonic frequencies in the range from 30.0 to 100 kHz. Bats, dolphins, submarines, and even some birds use ultrasonic sonar. Echoes are analyzed to give distance and size information both for guidance and finding prey. In most sonar applications, the sound reflects quite well because the objects of interest have significantly different density than the medium in which they travel. When the Doppler shift is observed, velocity information can also be obtained. Submarine sonar can be used to obtain such information, and there is evidence that some bats also sense velocity from their echoes.Similarly, there are a range of relatively inexpensive devices that measure distance by timing ultrasonic echoes. Many cameras, for example, use such information to focus automatically. Some doors open when their ultrasonic ranging devices detect a nearby object, and certain home security lights turn on when their ultrasonic rangers observe motion. Ultrasonic measuring tapes also exist to measure such things as room dimensions. Sinks in public restrooms are sometimes automated with ultrasound devices to turn faucets on and off when people wash their hands. These devices reduce the spread of germs and can conserve water.Ultrasound is used for nondestructive testing in industry and by the military. Because ultrasound reflects well from any large change in density, it can reveal cracks and voids in solids, such as aircraft wings, that are too small to be seen with x-rays. For similar reasons, ultrasound is also good for measuring the thickness of coatings, particularly where there are several layers involved.Basic research in solid state physics employs ultrasound. Its attenuation is related to a number of physical characteristics, making it a useful probe. Among these characteristics are structural changes such as those found in liquid crystals, the transition of a material to a superconducting phase, as well as density and other properties.
These examples of the uses of ultrasound are meant to whet the appetites of the curious, as well as to illustrate the underlying physics of ultrasound. There are many more applications, as you can easily discover for yourself.

Check Your Understanding

Why is it possible to use ultrasound both to observe a fetus in the womb and also to destroy cancerous tumors in the body?


Ultrasound can be used medically at different intensities. Lower intensities do not cause damage and are used for medical imaging. Higher intensities can pulverize and destroy targeted substances in the body, such as tumors.




Section SummaryThe acoustic impedance is defined as:
    Z=v,Z=v, is the density of a medium through which the sound travels and vv is the speed of sound through that medium.
The intensity reflection coefficient aa, a measure of the ratio of the intensity of the wave reflected off a boundary between two media relative to the intensity of the incident wave, is given by
    



a
=





Z

2




Z

1





2






Z

1


+

Z

2





2





.






a
=





Z

2




Z

1





2






Z

1


+

Z

2





2





.


 size 12{a= {  { left (Z rSub { size 8{2} }  - Z rSub { size 8{1} }  right ) rSup { size 8{2} } }  over  { left (Z rSub { size 8{1} } +Z rSub { size 8{2} }  right ) rSup { size 8{2} } } } } {}


The intensity reflection coefficient is a unitless quantity.
Conceptual Questions


If audible sound follows a rule of thumb similar to that for ultrasound, in terms of its absorption, would you expect the high or low frequencies from your neighbors stereo to penetrate into your house? How does this expectation compare with your experience?


Elephants and whales are known to use infrasound to communicate over very large distances. What are the advantages of infrasound for long distance communication?



It is more difficult to obtain a high-resolution ultrasound image in the abdominal region of someone who is overweight than for someone who has a slight build. Explain why this statement is accurate.



Suppose you read that 210-dB ultrasound is being used to pulverize cancerous tumors. You calculate the intensity in watts per centimeter squared and find it is unreasonably high (105W/cm2105W/cm2 size 12{"10" rSup { size 8{5} } "W/cm" rSup { size 8{2} } } {}). What is a possible explanation?

Problems & Exercises
Unless otherwise indicated, for problems in this section, assume that the speed of sound through human tissues is 1540 m/s.



What is the sound intensity level in decibels of ultrasound of intensity 105W/m2105W/m2, used to pulverize tissue during surgery?

170 dB


Is 155-dB ultrasound in the range of intensities used for deep heating? Calculate the intensity of this ultrasound and compare this intensity with values quoted in the text.


Find the sound intensity level in decibels of 2.00102W/m22.00102W/m2ultrasound used in medical diagnostics.

103 dB


The time delay between transmission and the arrival of the reflected wave of a signal using ultrasound traveling through a piece of fat tissue was 0.13 ms. At what depth did this reflection occur?

In the clinical use of ultrasound, transducers are always coupled to the skin by a thin layer of gel or oil, replacing the air that would otherwise exist between the transducer and the skin. (a) Using the values of acoustic impedance given in [link]  calculate the intensity reflection coefficient between transducer material and air. (b) Calculate the intensity reflection coefficient between transducer material and gel (assuming for this problem that its acoustic impedance is identical to that of water). (c) Based on the results of your calculations, explain why the gel is used.

(a) 1.00
(b) 0.823(c) Gel is used to facilitate the transmission of the ultrasound between the transducer and the patients body.


(a) Calculate the minimum frequency of ultrasound that will allow you to see details as small as 0.250 mm in human tissue. (b) What is the effective depth to which this sound is effective as a diagnostic probe?


(a) Find the size of the smallest detail observable in human tissue with 20.0-MHz ultrasound. (b) Is its effective penetration depth great enough to examine the entire eye (about 3.00 cm is needed)? (c) What is the wavelength of such ultrasound in 0C0C air?

(a) 77.0 m77.0 m
(b) Effective penetration depth = 3.85 cm, which is enough to examine the eye. (c)  16.6 m16.6 m



(a) Echo times are measured by diagnostic ultrasound scanners to determine distances to reflecting surfaces in a patient. What is the difference in echo times for tissues that are 3.50 and 3.60 cm beneath the surface? (This difference is the minimum resolving time for the scanner to see details as small as 0.100 cm, or 1.00 mm. Discrimination of smaller time differences is needed to see smaller details.) (b) Discuss whether the period  TT
 of this ultrasound must be smaller than the minimum time resolution. If so, what is the minimum frequency of the ultrasound and is that out of the normal range for diagnostic ultrasound?

(a) How far apart are two layers of tissue that produce echoes having round-trip times (used to measure distances) that differ by  0.750 s0.750 s
? (b) What minimum frequency must the ultrasound have to see detail this small?

(a) 5.78104m5.78104m
(b) 2.67106Hz2.67106Hz


(a) A bat uses ultrasound to find its way among trees. If this bat can detect echoes 1.00 ms apart, what minimum distance between objects can it detect? (b) Could this distance explain the difficulty that bats have finding an open door when they accidentally get into a house?


A dolphin is able to tell in the dark that the ultrasound echoes received from two sharks come from two different objects only if the sharks are separated by 3.50 m, one being that much farther away than the other. (a) If the ultrasound has a frequency of 100 kHz, show this ability is not limited by its wavelength. (b) If this ability is due to the dolphins ability to detect the arrival times of echoes, what is the minimum time difference the dolphin can perceive?

(a) vw
=
1540 m/s
=
f


=

1540 m/s

100103Hz


=
0.0154 m < 3.50 m.
vw
=
1540 m/s
=
f


=

1540 m/s

100103Hz


=
0.0154 m < 3.50 m.

Because the wavelength is much shorter than the distance in question, the wavelength is not the limiting factor.(b) 4.55 ms

A diagnostic ultrasound echo is reflected from moving blood and returns with a frequency 500 Hz higher than its original 2.00 MHz. What is the velocity of the blood? (Assume that the frequency of 2.00 MHz is accurate to seven significant figures and 500 Hz is accurate to three significant figures.)

Ultrasound reflected from an oncoming bloodstream that is moving at 30.0 cm/s is mixed with the original frequency of 2.50 MHz to produce beats. What is the beat frequency? (Assume that the frequency of 2.50 MHz is accurate to seven significant figures.)

974 Hz(Note: extra digits were retained in order to show the difference.)

Glossary
acoustic impedance property of medium that makes the propagation of sound waves more difficult
intensity reflection coefficient a measure of the ratio of the intensity of the wave reflected off a boundary between two media relative to the intensity of the incident wave
Doppler-shifted ultrasound a medical technique to detect motion and determine velocity through the Doppler shift of an echo

