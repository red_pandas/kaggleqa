
Physical Quantities and Units
Physical Quantities and Units
Perform unit conversions both in the SI and English units.

Explain the most common prefixes in the SI units and be able to write them in scientific notation.

The distance from Earth to the Moon may seem immense, but it is just a tiny fraction of the distances from Earth to other celestial bodies. (credit: NASA)


The range of objects and phenomena studied in physics is immense. From the incredibly short lifetime of a nucleus to the age of the Earth, from the tiny sizes of sub-nuclear particles to the vast distance to the edges of the known universe, from the force exerted by a jumping flea to the force between Earth and the Sun, there are enough factors of 10 to challenge the imagination of even the most experienced scientist. Giving numerical values for physical quantities and equations for physical principles allows us to understand nature much more deeply than does qualitative description alone. To comprehend these vast ranges, we must also have accepted units in which to express them. And we shall find that (even in the potentially mundane discussion of meters, kilograms, and seconds) a profound simplicity of nature appearsall physical quantities can be expressed as combinations of only four fundamental physical quantities: length, mass, time, and electric current.We define a physical quantity either by  specifying how it is measured or by stating how it is calculated from other measurements. For example, we define distance and time by specifying methods for measuring them, whereas we define average speed by stating that it is calculated as distance traveled divided by time of travel.
Measurements of physical quantities are expressed in terms of units, which are standardized values. For example, the length of a race, which is a physical quantity, can be expressed in units of meters (for sprinters) or kilometers (for distance runners). Without standardized units, it would be extremely difficult for scientists to express and compare measured values in a meaningful way. (See [link].)
Distances given in unknown units are maddeningly useless.


There are two major systems of units used in the world: SI units (also known as the metric system) and English units (also known as the customary or imperial system). English units were historically used in nations once ruled by the British Empire and are still widely used in the United States. Virtually every other country in the world now uses SI units as the standard; the metric system is also the standard system agreed upon by scientists and mathematicians. The acronym SI is derived from the French Systme International.SI Units: Fundamental and Derived Units
[link] gives the fundamental SI units that are used throughout this textbook. This text uses non-SI units in a few applications where they are in very common use, such as the measurement of blood pressure in millimeters of mercury (mm Hg). Whenever non-SI units are discussed, they will be tied to SI units through conversions.
Fundamental SI Units
Length
Mass
Time

Electric Current


meter (m)
kilogram (kg)
second (s)
ampere (A)
It is an intriguing fact that some physical quantities are more fundamental than others and that the most fundamental physical quantities can be defined only in terms of the procedure used to measure them. The units in which they are measured are thus called fundamental units. In this textbook, the fundamental physical quantities are taken to be length, mass, time, and electric current. (Note that electric current will not be introduced until much later in this text.) All other physical quantities, such as force and electric charge, can be expressed as algebraic combinations of length, mass, time, and current (for example, speed is length divided by time); these units are called derived units.
Units of Time, Length, and Mass: The Second, Meter, and KilogramThe Second
The SI unit for time, the second(abbreviated s), has a long history. For many years it was defined as 1/86,400 of a mean solar day. More recently, a new standard was adopted to gain greater accuracy and to define the second in terms of a non-varying, or constant, physical phenomenon (because the solar day is getting longer due to very gradual slowing of the Earths rotation). Cesium atoms can be made to vibrate in a very steady way, and these vibrations can be readily observed and counted. In 1967 the second was redefined as the time required for 9,192,631,770 of these vibrations. (See [link].) Accuracy in the fundamental units is essential, because all measurements are ultimately expressed in terms of fundamental units and can be no more accurate than are the fundamental units themselves.
An atomic clock such as this one uses the vibrations of cesium atoms to keep time to a precision of better than a microsecond per year. The fundamental unit of time, the second, is based on such clocks. This image is looking down from the top of an atomic fountain nearly 30 feet tall! (credit: Steve Jurvetson/Flickr)



The Meter
The SI unit for length is the meter (abbreviated m); its definition has also changed over time to become more accurate and precise. The meter was first defined in 1791 as 1/10,000,000 of the distance from the equator to the North Pole. This measurement was improved in 1889 by redefining the meter to be the distance between two engraved lines on a platinum-iridium bar now kept near Paris. By 1960, it had become possible to define the meter even more accurately in terms of the wavelength of light, so it was again redefined as 1,650,763.73 wavelengths of orange light emitted by krypton atoms. In 1983, the meter was given its present definition (partly for greater accuracy) as the distance light travels in a vacuum in 1/299,792,458 of a second. (See [link].) This change defines the speed of light to be exactly 299,792,458 meters per second. The length of the meter will change if the speed of light is someday measured with greater accuracy.
The Kilogram
The SI unit for mass is the kilogram (abbreviated kg); it is defined to be the mass of a platinum-iridium cylinder kept with the old meter standard at the International Bureau of Weights and Measures near Paris. Exact replicas of the standard kilogram are also kept at the United States National Institute of Standards and Technology, or NIST, located in Gaithersburg, Maryland outside of Washington D.C., and at other locations around the world. The determination of all other masses can be ultimately traced to a comparison with the standard mass.
The meter is defined to be the distance light travels in 1/299,792,458 of a second in a vacuum. Distance traveled is speed multiplied by time.


Electric current and its accompanying unit, the ampere, will be introduced in Introduction to Electric Current, Resistance, and Ohm's Law when electricity and magnetism are covered. The initial modules in this textbook are concerned with mechanics, fluids, heat, and waves. In these subjects all pertinent physical quantities can be expressed in terms of the fundamental units of length, mass, and time.
Metric Prefixes
SI units are part of the metric system. The metric system is convenient for scientific and engineering calculations because the units are categorized by factors of 10. [link] gives metric prefixes and symbols used to denote various factors of 10.
Metric systems have the advantage that conversions of units involve only powers of 10. There are 100 centimeters in a meter, 1000 meters in a kilometer, and so on. In nonmetric systems, such as the system of U.S. customary units, the relationships are not as simplethere are 12 inches in a foot, 5280 feet in a mile, and so on. Another advantage of the metric system is that the same unit can be used over extremely large ranges of values simply by using an appropriate metric prefix. For example, distances in meters are suitable in construction, while distances in kilometers are appropriate for air travel, and the tiny measure of nanometers are convenient in optical design. With the metric system there is no need to invent new units for particular applications.
The term order of magnitude refers to the scale of a value expressed in the metric system. Each power of 1010 size 12{"10"}  in the metric system represents a different order of magnitude. For example, 101,102,103101,102,103 size 12{"10" rSup { size 8{1} } ,`"10" rSup { size 8{2} } ,`"10" rSup { size 8{3} } } {}, and so forth are all different orders of magnitude. All quantities that can be expressed as a product of a specific power of 1010 size 12{"10"}  are said to be of the same order of magnitude. For example, the number 800800 size 12{"800"}  can be written as 81028102, and the number 450450 can be written as 4.5102.4.5102. Thus, the numbers 800800 and 450450 are of the same order of magnitude: 102.102. Order of magnitude can be thought of as a ballpark estimate for the scale of a value. The diameter of an atom is on the order of 109m,109m, while the diameter of the Sun is on the order of 109m.109m.The Quest for Microscopic Standards for Basic Units
The fundamental units described in this chapter are those that produce the greatest accuracy and precision in measurement. There is a sense among physicists that, because there is an underlying microscopic substructure to matter, it would be most satisfying to base our standards of measurement on microscopic objects and fundamental physical phenomena such as the speed of light. A microscopic standard has been accomplished for the standard of time, which is based on the oscillations of the cesium atom.
The standard for length was once based on the wavelength of light (a small-scale length) emitted by a certain type of atom, but it has been supplanted by the more precise measurement of the speed of light. If it becomes possible to measure the mass of atoms or a particular arrangement of atoms such as a silicon sphere to greater precision than the kilogram standard, it may become possible to base mass measurements on the small scale. There are also possibilities that electrical phenomena on the small scale may someday allow us to base a unit of charge on the charge of electrons and protons, but at present current and charge are related to large-scale currents and forces between wires.
Metric Prefixes for Powers of 10 and their Symbols

Prefix
Symbol
Value1
Example (some are approximate)



exa
E





10

18









10

18





 size 12{"10" rSup { size 8{"18"} } } {}


exameter
Em






10

18


m









10

18


m




 size 12{"10" rSup { size 8{"18"} } " m"} {}


distance light travels in a century


peta
P





10

15









10

15





 size 12{"10" rSup { size 8{"15"} } } {}


petasecond
Ps






10

15


s









10

15


s




 size 12{"10" rSup { size 8{"15"} } " s"} {}


30 million years


tera
T





10

12









10

12





 size 12{"10" rSup { size 8{"12"} } } {}


terawatt
TW






10

12



W









10

12



W




 size 12{"10" rSup { size 8{"12"} } `W} {}


powerful laser output


giga
G





10

9









10

9





 size 12{"10" rSup { size 8{9} } } {}


gigahertz
GHz






10

9



Hz









10

9



Hz




 size 12{"10" rSup { size 8{9} } `"Hz"} {}


a microwave frequency


mega
M





10

6









10

6





 size 12{"10" rSup { size 8{6} } } {}


megacurie
MCi






10

6



Ci









10

6



Ci




 size 12{"10" rSup { size 8{6} } `"Ci"} {}


high radioactivity


kilo
k





10

3









10

3





 size 12{"10" rSup { size 8{3} } } {}


kilometer
km






10

3


m









10

3


m




 size 12{"10" rSup { size 8{3} } " m"} {}


about 6/10 mile


hecto
h





10

2









10

2





 size 12{"10" rSup { size 8{2} } } {}


hectoliter
hL






10

2


L









10

2


L




 size 12{"10" rSup { size 8{2} } " L"} {}


26 gallons


deka
da





10

1









10

1





 size 12{"10" rSup { size 8{1} } } {}


dekagram
dag






10

1



g









10

1



g




 size 12{"10" rSup { size 8{1} } `g} {}


teaspoon of butter




100100 size 12{"10" rSup { size 8{0} } } {} (=1)






deci
d





10



1










10



1






 size 12{"10" rSup { size 8{ - 1} } } {}


deciliter
dL






10



1




L









10



1




L




 size 12{"10" rSup { size 8{ - 1} } `L} {}


less than half a soda


centi
c





10



2










10



2






 size 12{"10" rSup { size 8{ - 2} } } {}


centimeter
cm






10



2




m









10



2




m




 size 12{"10" rSup { size 8{ - 2} } `m} {}


fingertip thickness


milli
m





10



3










10



3






 size 12{"10" rSup { size 8{ - 3} } } {}


millimeter
mm






10



3




m









10



3




m




 size 12{"10" rSup { size 8{ - 3} } `m} {}


flea at its shoulders


micro






10



6










10



6






 size 12{"10" rSup { size 8{ - 6} } } {}


micrometer
m






10



6




m









10



6




m




 size 12{"10" rSup { size 8{ - 6} } `m} {}


detail in microscope


nano
n





10



9










10



9






 size 12{"10" rSup { size 8{ - 9} } } {}


nanogram
ng






10



9




g









10



9




g




 size 12{"10" rSup { size 8{ - 9} } `g} {}


small speck of dust


pico
p





10



12










10



12






 size 12{"10" rSup { size 8{ - "12"} } } {}


picofarad
pF






10



12



F









10



12



F




 size 12{"10" rSup { size 8{ - "12"} } F} {}


small capacitor in radio


femto
f





10



15










10



15






 size 12{"10" rSup { size 8{ - "15"} } } {}


femtometer
fm






10



15




m









10



15




m




 size 12{"10" rSup { size 8{ - "15"} } `m} {}


size of a proton


atto
a





10



18










10



18






 size 12{"10" rSup { size 8{ - "18"} } } {}


attosecond
as






10



18




s









10



18




s




 size 12{"10" rSup { size 8{ - "18"} } `s} {}


time light crosses an atom


Known Ranges of Length, Mass, and Time
The vastness of the universe and the breadth over which physics applies are illustrated by the wide range of examples of known lengths, masses, and times in [link]. Examination of this table will give you some feeling for the range of possible topics and numerical values. (See [link] and [link].)
Tiny phytoplankton swims among crystals of ice in the Antarctic Sea. They range from a few micrometers to as much as 2 millimeters in length. (credit: Prof. Gordon T. Taylor, Stony Brook University; NOAA Corps Collections)


Galaxies collide 2.4 billion light years away from Earth. The tremendous range of observable phenomena in nature challenges the imagination. (credit: NASA/CXC/UVic./A. Mahdavi et al. Optical/lensing: CFHT/UVic./H. Hoekstra et al.)



Unit Conversion and Dimensional Analysis
It is often necessary to convert from one type of unit to another. For example, if you are reading a European cookbook, some quantities may be expressed in units of liters and you need to convert them to cups. Or, perhaps you are reading walking directions from one location to another and you are interested in how many miles you will be walking. In this case, you will need to convert units of feet to miles.
Let us consider a simple example of how to convert units. Let us say that we want to convert 80 meters (m) to kilometers (km).
The first thing to do is to list the units that you have and the units that you want to convert to. In this case, we have units in meters and we want to convert to kilometers.
Next, we need to determine a conversion factor relating meters to kilometers. A conversion factor is a ratio expressing how many of one unit are equal to another unit. For example, there are 12 inches in 1 foot, 100 centimeters in 1 meter, 60 seconds in 1 minute, and so on. In this case, we know that there are 1,000 meters in 1 kilometer.
Now we can set up our unit conversion. We will write the units that we have and then multiply them by the conversion factor so that the units cancel out, as shown:
80 m1 km1000m=0.080 km.80 m1 km1000m=0.080 km. size 12{"80"" m" times  {  {"1 km"}  over  {"1000 m"} } =0 "." "080"`"km"} {}Note that the unwanted m unit cancels, leaving only the desired km unit. You can use this method to convert between any types of unit.
Click [link] for a more complete list of conversion factors.Approximate Values of Length, Mass, and Time

Lengths in meters
Masses in kilograms (more precise values in parentheses)
Times in seconds (more precise values in parentheses)








10



18










10



18








Present experimental limit to smallest observable detail





10



30










10



30






 size 12{"10" rSup { size 8{ - "30"} } } {}


Mass of an electron 
9.111031kg

9.111031kg size 12{9 "." "11" times "10" rSup { size 8{ - "31"} } `"kg"} {}





10



23









10



23





 size 12{"10" rSup { size 8{ - "23"} } } {}


Time for light to cross a proton







10



15









10



15





 size 12{"10" rSup { size 8{ - "15"} } } {}


Diameter of a proton





10



27










10



27






 size 12{"10" rSup { size 8{ - "27"} } } {}


Mass of a hydrogen atom 1.671027kg1.671027kg size 12{1 "." "67" times "10" rSup { size 8{ - "27"} } `"kg"} {} 





10



22










10



22






 size 12{"10" rSup { size 8{ - "22"} } } {}


Mean life of an extremely unstable nucleus







10



14










10



14






 size 12{"10" rSup { size 8{ - "14"} } } {}


Diameter of a uranium nucleus





10



15










10



15






 size 12{"10" rSup { size 8{ - "15"} } } {}


Mass of a bacterium





10



15










10



15






 size 12{"10" rSup { size 8{ - "15"} } } {}


Time for one oscillation of visible light







10



10










10



10






 size 12{"10" rSup { size 8{ - "10"} } } {}


Diameter of a hydrogen atom





10



5










10



5






 size 12{"10" rSup { size 8{ - 5} } } {}


Mass of a mosquito





10



13










10



13






 size 12{"10" rSup { size 8{ - "13"} } } {}


Time for one vibration of an atom in a solid







10



8










10



8






 size 12{"10" rSup { size 8{ - 8} } } {}


Thickness of membranes in cells of living organisms





10



2










10



2






 size 12{"10" rSup { size 8{ - 2} } } {}


Mass of a hummingbird





10



8










10



8






 size 12{"10" rSup { size 8{ - 8} } } {}


Time for one oscillation of an FM radio wave







10



6










10



6






 size 12{"10" rSup { size 8{ - 6} } } {}


Wavelength of visible light
1
1
 size 12{"1"} {} 
Mass of a liter of water (about a quart)





10



3










10



3






 size 12{"10" rSup { size 8{ - 3} } } {}


Duration of a nerve impulse







10



3










10



3






 size 12{"10" rSup { size 8{ - 3} } } {}


Size of a grain of sand





10

2









10

2





 size 12{"10" rSup { size 8{2} } } {}


Mass of a person
1
1
 size 12{"1"} {} 
Time for one heartbeat


1
1
 size 12{"1"} {} 
Height of a 4-year-old child





10

3









10

3





 size 12{"10" rSup { size 8{3} } } {}


Mass of a car





10

5









10

5





 size 12{"10" rSup { size 8{5} } } {}


One day 8.64104s8.64104s size 12{8 "." "64" times "10" rSup { size 8{4} } `s} {}







10

2









10

2





 size 12{"10" rSup { size 8{2} } } {}


Length of a football field





10

8









10

8





 size 12{"10" rSup { size 8{8} } } {}


Mass of a large ship 





10

7









10

7





 size 12{"10" rSup { size 8{7} } } {}


One year (y) 3.16107s3.16107s size 12{3 "." "16" times "10" rSup { size 8{7} } `s} {}







10

4









10

4





 size 12{"10" rSup { size 8{4} } } {}


Greatest ocean depth





10

12









10

12





 size 12{"10" rSup { size 8{"12"} } } {}


Mass of a large iceberg





10

9









10

9





 size 12{"10" rSup { size 8{9} } } {}


About half the life expectancy of a human







10

7









10

7





 size 12{"10" rSup { size 8{7} } } {}


Diameter of the Earth





10

15









10

15





 size 12{"10" rSup { size 8{"15"} } } {}


Mass of the nucleus of a comet





10

11









10

11





 size 12{"10" rSup { size 8{"11"} } } {}


Recorded history







10

11









10

11





 size 12{"10" rSup { size 8{"11"} } } {}


Distance from the Earth to the Sun





10

23









10

23





 size 12{"10" rSup { size 8{"23"} } } {}


Mass of the Moon 7.351022kg7.351022kg size 12{7 "." "35" times "10" rSup { size 8{"22"} } `"kg"} {}





10

17









10

17





 size 12{"10" rSup { size 8{"17"} } } {}


Age of the Earth







10

16









10

16





 size 12{"10" rSup { size 8{"16"} } } {}


Distance traveled by light in 1 year (a light year)





10

25









10

25





 size 12{"10" rSup { size 8{"25"} } } {}


Mass of the Earth 5.971024kg5.971024kg size 12{5 "." "97" times "10" rSup { size 8{"24"} } `"kg"} {}





10

18









10

18





 size 12{"10" rSup { size 8{"18"} } } {}


Age of the universe







10

21








10

21




 size 12{"10" rSup { size 8{"21"} } } {}


Diameter of the Milky Way galaxy





10

30









10

30





 size 12{"10" rSup { size 8{"30"} } } {}


Mass of the Sun 1.991030kg1.991030kg size 12{1 "." "99" times "10" rSup { size 8{"30"} } `"kg"} {}









10

22








10

22




 size 12{"10" rSup { size 8{"22"} } } {}


Distance from the Earth to the nearest large galaxy (Andromeda)





10

42








10

42




 size 12{"10" rSup { size 8{"42"} } } {}


Mass of the Milky Way galaxy (current upper limit)








10

26









10

26





 size 12{"10" rSup { size 8{"26"} } } {}


Distance from the Earth to the edges of the known universe





10

53









10

53





 size 12{"10" rSup { size 8{"53"} } } {}


Mass of the known universe (current upper limit)


Unit Conversions: A Short Drive Home
Suppose that you drive the 10.0 km from your university to home in 20.0 min. Calculate your average speed (a) in kilometers per hour (km/h) and (b) in meters per second (m/s). (Note: Average speed is distance traveled divided by time of travel.)

Strategy

First we calculate the average speed using the given units. Then we can get the average speed into the desired units by picking the correct conversion factor and multiplying by it. The correct conversion factor is the one that cancels the unwanted unit and leaves the desired unit in its place.

Solution for (a)

(1) Calculate average speed. Average speed is distance traveled divided by time of travel. (Take this definition as a given for nowaverage speed and other motion concepts will be covered in a later module.) In equation form,
average speed = distancetime.average speed = distancetime. size 12{"average speed = " {  {"distance"}  over  {"time"} } } {}(2) Substitute the given values for distance and time.
average speed = 10.0km20.0min=0.500kmmin.average speed = 10.0km20.0min=0.500kmmin. size 12{"average speed = " {  {"10" "." 0" km"}  over  {"20" "." 0" min"} } =0 "." "500"  {  {"km"}  over  {"min"} } } {}(3) Convert km/min to km/h: multiply by the conversion factor that will cancel minutes and leave hours. That conversion factor is 60 min/hr60 min/hr size 12{"60 min/hr"}{}. Thus,average speed = 0.500kmmin60min1h=30.0kmh.average speed = 0.500kmmin60min1h=30.0kmh. size 12{"average speed = "0 "." "500"  {  {"km"}  over  {"min"} }  times  {  {"60"" min"}  over  {1" h"} } ="30" "." 0  {  {"km"}  over  {h} } } {}
Discussion for (a)

To check your answer, consider the following:
(1) Be sure that you have properly cancelled the units in the unit conversion. If you have written the unit conversion factor upside down, the units will not cancel properly in the equation. If you accidentally get the ratio upside down, then the units will not cancel; rather, they will give you the wrong units as follows:
kmmin1hr60min=160kmhrmin2,kmmin1hr60min=160kmhrmin2, size 12{ {  {"km"}  over  {"min"} }  times  {  {1`"hr"}  over  {"60"`"min"} } = {  {1}  over  {"60"} }  {  {"km" cdot "hr"}  over  {"min"} } } {}which are obviously not the desired units of km/h.
(2) Check that the units of the final answer are the desired units. The problem asked us to solve for average speed in units of km/h and we have indeed obtained these units.
(3) Check the significant figures. Because each of the values given in the problem has three significant figures, the answer should also have three significant figures. The answer 30.0 km/hr does indeed have three significant figures, so this is appropriate. Note that the significant figures in the conversion factor are not relevant because an hour is defined to be 60 minutes, so the precision of the conversion factor is perfect.(4) Next, check whether the answer is reasonable. Let us consider some information from the problemif you travel 10 km in a third of an hour (20 min), you would travel three times that far in an hour. The answer does seem reasonable.

Solution for (b)

There are several ways to convert the average speed into meters per second.
(1) Start with the answer to (a) and convert km/h to m/s. Two conversion factors are neededone to convert hours to seconds, and another to convert kilometers to meters.
(2) Multiplying by these yields
Average speed=30.0kmh1h3,600 s1,000m1 km,Average speed=30.0kmh1h3,600 s1,000m1 km, size 12{"Average"`"speed"="30" "." 0 {  {"km"}  over  {h} }  times  {  {1" h"}  over  {"3,600 s"} }  times  {  {1,"000"" m"}  over  {"1 km"} } } {}Average speed=8.33ms.Average speed=8.33ms. size 12{"Average"`"speed"=8 "." "33" {  {m}  over  {s} } } {}
Discussion for (b)

If we had started with 0.500 km/min, we would have needed different conversion factors, but the answer would have been the same: 8.33 m/s.You may have noted that the answers in the worked example just covered were given to three digits. Why? When do you need to be concerned about the number of digits in something you calculate? Why not write down all the digits your calculator produces? The module Accuracy, Precision, and Significant Figures will help you answer these questions.
Nonstandard Units
While there are numerous types of units that we are all familiar with, there are others that are much more obscure. For example, a firkin is a unit of volume that was once used to measure beer. One firkin equals about 34 liters. To learn more about nonstandard units, use a dictionary or encyclopedia to research different weights and measures. Take note of any unusual units, such as a barleycorn, that are not listed in the text. Think about how the unit is defined and state its relationship to SI units.

Check Your Understanding
Some hummingbirds beat their wings more than 50 times per second. A scientist is measuring the time it takes for a hummingbird to beat its wings once. Which fundamental unit should the scientist use to describe the measurement? Which factor of 10 is the scientist likely to use to describe the motion precisely? Identify the metric prefix that corresponds to this factor of 10. 

The scientist will measure the time between each movement using the fundamental unit of seconds. Because the wings beat so fast, the scientist will probably need to measure in milliseconds, or 103103 size 12{"10" rSup { size 8{ - 3} } } {} seconds. (50 beats per second corresponds to 20 milliseconds per beat.)
Check Your Understanding

One cubic centimeter is equal to one milliliter. What does this tell you about the different units in the SI metric system?

The fundamental unit of length (meter) is probably used to create the derived unit of volume (liter). The measure of a milliliter is dependent on the measure of a centimeter.


Summary
Physical quantities are a characteristic or property of an object that can be measured or calculated from other measurements.
Units are standards for expressing and comparing the measurement of physical quantities. All units can be expressed as combinations of four fundamental units.
The four fundamental units we will use in this text are the meter (for length), the kilogram (for mass), the second (for time), and the ampere (for electric current). These units are part of the metric system, which uses powers of 10 to relate quantities over the vast ranges encountered in nature.
The four fundamental units are abbreviated as follows: meter, m; kilogram, kg; second, s; and ampere, A. The metric system also uses a standard set of prefixes to denote each order of magnitude greater than or lesser than the fundamental unit itself.
Unit conversions involve changing a value expressed in one type of unit to another type of unit. This is done by using conversion factors, which are ratios relating equal quantities of different units.

Conceptual Questions

Identify some advantages of metric units.


Problems & Exercises
The speed limit on some interstate highways is roughly 100 km/h. (a) What is this in meters per second? (b) How many miles per hour is this?







27
.

8 m/s








27
.

8 m/s




 size 12{"27" "." 8`"m/s"} {}



62.1 mph62.1 mph size 12{"62" "." 1" mph"} {}
 



A car is traveling at a speed of 33 m/s33 m/s size 12{"33"" m/s"} {}. (a) What is its speed in kilometers per hour? (b) Is it exceeding the 90 km/h90 km/h size 12{"90"" km/h"} {} speed limit?

Show that 1.0 m/s=3.6 km/h1.0 m/s=3.6 km/h size 12{1 "." 0`"m/s"=3 "." "6 km/h"} {}. Hint: Show the explicit steps involved in converting 1.0 m/s=3.6 km/h.1.0 m/s=3.6 km/h. size 12{1 "." 0`"m/s"=3 "." "6 km/h"} {}







1.0 m

s

=




1
.
0 m

s



3600 s
1 hr





1 km

1000 m












1.0 m

s

=




1
.
0 m

s



3600 s
1 hr





1 km

1000 m






 size 12{ {  {1 "." "0 m"}  over  {s} } = {  {1 "." "0 m"}  over  {s} }  times  {  {"3600 s"}  over  {"hr"} }  times  {  {1`"km"}  over  {"1000 m"} } } {}

=3.6 km/h=3.6 km/h size 12{ {}=3 "." 6" km/h"} {}.


American football is played on a 100-yd-long field, excluding the end zones. How long is the field in meters? (Assume that 1 meter equals 3.281 feet.)


Soccer fields vary in size. A large soccer field is 115 m long and 85 m wide. What are its dimensions in feet and inches? (Assume that 1 meter equals 3.281 feet.)


length: 377 ft377 ft size 12{"377"" ft"} {}; 4.53103in.4.53103in. size 12{4 "." "53" times "10" rSup { size 8{3} } " in" "." } {} width: 280ft280ft size 12{"280"" ft"} {}; 3.3103in3.3103in size 12{3 "." 3 times "10" rSup { size 8{3} } " in"} {}.


What is the height in meters of a person who is 6 ft 1.0 in. tall? (Assume that 1 meter equals 39.37 in.)


Mount Everest, at 29,028 feet, is the tallest mountain on the Earth. What is its height in kilometers? (Assume that 1 kilometer equals 3,281 feet.)






8
.
847 km








8
.
847 km




 size 12{8 "." "847"" km"} {}





The speed of sound is measured to be 342 m/s342 m/s size 12{"342"" m/s"} {} on a certain day. What is this in km/h?


Tectonic plates are large segments of the Earths crust that move slowly. Suppose that one such plate has an average speed of 4.0 cm/year. (a) What distance does it move in 1 s at this speed? (b) What is its speed in kilometers per million years?


(a) 1.3109m1.3109m size 12{1 "." 3 times "10" rSup { size 8{ - 9} } " m"} {}(b) 40 km/My40 km/My size 12{"40"" km/My"} {}

(a) Refer to [link] to determine the average distance between the Earth and the Sun. Then calculate the average speed of the Earth in its orbit in kilometers per second. (b) What is this in meters per second?

Footnotes1 See Appendix A for a discussion of powers of 10.Glossary
physical quantity a characteristic or property of an object that can be measured or calculated from other measurements
units  a standard used for expressing and comparing measurements
SI units the international system of units that scientists in most countries have agreed to use; includes units such as meters, liters, and grams
English units system of measurement used in the United States; includes units of measurement such as feet, gallons, and pounds
fundamental units units that can only be expressed relative to the procedure used to measure them
derived units units that can be calculated using algebraic combinations of the fundamental units
second the SI unit for time, abbreviated (s)
meter the SI unit for length, abbreviated (m)
kilogram the SI unit for mass, abbreviated (kg)
metric system a system in which values can be calculated in factors of 10
order of magnitude refers to the size of a quantity as it relates to a power of 10
conversion factor a ratio expressing how many of one unit are equal to another unit

