
Energy and the Simple Harmonic Oscillator
Energy and the Simple Harmonic Oscillator
Determine the maximum speed of an oscillating system.

To study the energy of a simple harmonic oscillator, we first consider all the forms of energy it can have We know from Hookes Law: Stress and Strain Revisited that the energy stored in the deformation of a simple harmonic oscillator is a form of potential energy given by:




PE
el

=

1
2



kx

2






.






PE
el

=

1
2



kx

2






.

 size 12{"PE" size 8{"el"}= {  {1}  over  {2} }  ital "kx" rSup { size 8{2} } } {}

Because a simple harmonic oscillator has no dissipative forces, the other important form of energy is kinetic energy KEKE size 12{ ital "KE"} {}. Conservation of energy for these two forms is:





KE

+


PE

el




=
constant










KE

+


PE

el




=
constant




 size 12{ ital "KE"+ ital "PE" rSub { size 8{e1} } ="constant"} {}

or





1
2



 
mv


2



+

1
2




 
kx


2



=
constant.










1
2



 
mv


2



+

1
2




 
kx


2



=
constant.





 size 12{ {  {1}  over  {2} }  ital "mv" rSup { size 8{2} } + {  {1}  over  {2} }  ital "kx" rSup { size 8{2} } ="constant"} {}

This statement of conservation of energy is valid for all simple harmonic oscillators, including ones where the gravitational force plays a role
Namely, for a simple pendulum we replace the velocity with v=Lv=L size 12{v=L} {}, the spring constant with k=mg/Lk=mg/L size 12{k= ital "mg"/L} {}, and the displacement term with x=Lx=L size 12{x=L} {}. Thus




1
2


 
mL


2







2


+

1
2




mgL






2


=
constant.










1
2


 
mL


2







2


+

1
2




mgL






2


=
constant.





 size 12{ {  {1}  over  {2} }  ital "mL" rSup { size 8{2} }  rSup { size 8{2} } + {  {1}  over  {2} }  ital "mgL" rSup { size 8{2} } ="constant"} {}

In the case of undamped simple harmonic motion, the energy oscillates back and forth between kinetic and potential, going completely from one to the other as the system oscillates. So for the simple example of an object on a frictionless surface attached to a spring, as shown again in [link], the motion starts with all of the energy stored in the spring. As the object starts to move, the elastic potential energy is converted to kinetic energy, becoming entirely kinetic energy at the equilibrium position. It is then converted back into elastic potential energy by the spring, the velocity becomes zero when the kinetic energy is completely converted, and so on. This concept provides extra insight here and in later applications of simple harmonic motion, such as alternating current circuits.

The transformation of energy in simple harmonic motion is illustrated for an object attached to a spring on a frictionless surface.



The conservation of energy principle can be used to derive an expression for velocity vv size 12{v} {}. If we start our simple harmonic motion with zero velocity and maximum displacement (x=Xx=X size 12{x=X} {}), then the total energy is 12kX2.12kX2. size 12{ {  {1}  over  {2} }  ital "kX" rSup { size 8{2} } } {}This total energy is constant and is shifted back and forth between kinetic energy and potential energy, at most times being shared by each. The conservation of energy for this system in equation form is thus:





1
2



 
mv


2



+

1
2




 
kx


2



=

1
2



 
kX


2





.







1
2



 
mv


2



+

1
2




 
kx


2



=

1
2



 
kX


2





.


 size 12{ {  {1}  over  {2} }  ital "mv" rSup { size 8{2} } + {  {1}  over  {2} }  ital "kx" rSup { size 8{2} } = {  {1}  over  {2} }  ital "kX" rSup { size 8{2} } } {}

Solving this equation for vv size 12{v} {} yields:




v
=





k
m




X

2




x

2









.






v
=





k
m




X

2




x

2









.


 size 12{v= +-  sqrt { {  {k}  over  {m} }  left (X rSup { size 8{2} }  - x rSup { size 8{2} }  right )} } {}

Manipulating this expression algebraically gives:





v
=




k
m




X


1



x

2



X

2














v
=




k
m




X


1



x

2



X

2









 size 12{v= +-  sqrt { {  {k}  over  {m} } } X sqrt {1 -  {  {x rSup { size 8{2} } }  over  {X rSup { size 8{2} } } } } } {}

and so





v
=



v
max







1



x

2



X

2









,







v
=



v
max







1



x

2



X

2









,


 size 12{v= +- v size 8{"max" sqrt {1 -  {  {x rSup { size 8{2} } }  over  {X rSup { size 8{2} } } } } }} {}

where





v
max


=


k
m



X


.







v
max


=


k
m



X


.


 size 12{v size 8{"max"}= sqrt { {  {k}  over  {m} } } X} {}

From this expression, we see that the velocity is a maximum (vmaxvmax) at x=0x=0 size 12{x=0} {}, as stated earlier in vt=vmaxsin2tTvt=vmaxsin2tT. Notice that the maximum velocity depends on three factors. Maximum velocity is directly proportional to amplitude. As you might guess, the greater the maximum displacement the greater the maximum velocity. Maximum velocity is also greater for stiffer systems, because they exert greater force for the same displacement. This observation is seen in the expression for vmax;vmax; it is proportional to the square root of the force constant kk. Finally, the maximum velocity is smaller for objects that have larger masses, because the maximum velocity is inversely proportional to the square root of mm. For a given force, objects that have large masses accelerate more slowly.A similar calculation for the simple pendulum produces a similar result, namely:








max


=


g
L






max





.









max


=


g
L






max





.

 size 12{ rSub { size 8{"max"} } = sqrt { {  {g}  over  {L} } }  rSub { size 8{"max"} } } {}

Determine the Maximum Speed of an Oscillating System: A Bumpy Road
Suppose that a car is 900 kg and has a suspension system that has a force constant k=6.53104N/mk=6.53104N/m size 12{k=6 "." "53" times "10" rSup { size 8{4} } `"N/m"} {}. The car hits a bump and bounces with an amplitude of 0.100 m. What is its maximum vertical velocity if you assume no damping occurs?
Strategy

We can use the expression for vmaxvmax size 12{v rSub { size 8{"max"} } } {} given in vmax=kmXvmax=kmX size 12{v size 8{"max"}= sqrt { {  {k}  over  {m} } } X} {} to determine the maximum vertical velocity. The variables mm size 12{m} {} and kk size 12{k} {} are given in the problem statement, and the maximum displacement XX size 12{X} {} is 0.100 m.
Solution

Identify known.
Substitute known values into vmax=kmXvmax=kmX size 12{v size 8{"max"}= sqrt { {  {k}  over  {m} } } X} {}:
    




v
max




=



6
.

53


10

4




N/m


900

 kg




(0
.
100

 m)


.







v
max




=



6
.

53


10

4




N/m


900

 kg




(0
.
100

 m)


.


 size 12{v size 8{"max"}= sqrt { {  {6 "." "53" times "10" rSup { size 8{4} } "N/m"}  over  {"900"" kg"} } } 0 "." "100"" m"} {}


Calculate to find vmax= 0.852 m/s.vmax= 0.852 m/s. size 12{v rSub { size 8{"max"} } } {} 

Discussion

This answer seems reasonable for a bouncing car. There are other ways to use conservation of energy to find vmaxvmax size 12{v rSub { size 8{"max"} } } {}. We could use it directly, as was done in the example featured in Hookes Law: Stress and Strain Revisited.The small vertical displacement 

yy size 12{v rSub { size 8{"max"} } } {}

of an oscillating simple pendulum, starting from its equilibrium position, is given as



y
(
t

)
=
a

 sin 
t


,






y
(
t

)
=
a

 sin 
t


,


 size 12{y \( t \) =a"sin"t} {}

where aa size 12{a} {} is the amplitude,  size 12{} {} is the angular velocity and tt size 12{t} {} is the time taken. Substituting =2T=2T size 12{= {  {2}  over  {T} } } {}, we have



y

t
=
a

sin


 2t
T




.






y

t
=
a

sin


 2t
T




.


 size 12{y left (t right )=a"sin" left ( {  {2t}  over  {T} }  right )} {}

Thus, the displacement of pendulum is a function of time as shown above. Also the velocity of the pendulum is given by 




v
(
t

)
=

2a
T


cos


 2t
T




,






v
(
t

)
=

2a
T


cos


 2t
T




,


 size 12{v \( t \) = {  {2a}  over  {T} } "cos" left ( {  {2t}  over  {T} }  right )} {}

so the motion of the pendulum is a function of time.
Check Your Understanding
Why does it hurt more if your hand is snapped with a ruler than with a loose spring, even if the displacement of each system is equal?
The ruler is a stiffer system, which carries greater force for the same amount of displacement. The ruler snaps your hand with greater force, which hurts more.

Check Your Understanding
You are observing a simple harmonic oscillator. Identify one way you could decrease the maximum velocity of the system.
 You could increase the mass of the object that is oscillating.

Section SummaryEnergy in the simple harmonic oscillator is shared between elastic potential energy and kinetic energy, with the total being constant:
    




1
2



 
mv


2



+

1
2






kx


2



=
 constant.










1
2



 
mv


2



+

1
2






kx


2



=
 constant.





 size 12{ {  {1}  over  {2} }  ital "mv" rSup { size 8{2} } + {  {1}  over  {2} }  ital "kx" rSup { size 8{2} } =" constant"} {}


Maximum velocity depends on three factors: it is directly proportional to amplitude, it is greater for stiffer systems, and it is smaller for objects that have larger masses:
    





v

max


=


k
m



X


.








v

max


=


k
m



X


.


 size 12{v rSub { size 8{"max"} } = sqrt { {  {k}  over  {m} } } X} {}


Conceptual Questions

Explain in terms of energy how dissipative forces such as friction reduce the amplitude of a harmonic oscillator. Also explain how a driving mechanism can compensate. (A pendulum clock is such a system.)


Problems & Exercises
The length of nylon rope from which a mountain climber is suspended has a force constant of 1.40104N/m1.40104N/m size 12{1 "." "40" times "10" rSup { size 8{4} } "N/m"} {}.
  (a) What is the frequency at which he bounces, given his mass plus and the mass of his equipment are 90.0 kg?
(b) How much would this rope stretch to break the climbers fall if he free-falls 2.00 m before the rope runs out of slack? Hint: Use conservation of energy.
(c) Repeat both parts of this problem in the situation where twice this length of nylon rope is used.


(a) 1.99 Hz1.99 Hz size 12{ "1.99 Hz" } {}
(b) 50.2 cm(c) 1.41 Hz, 0.710 m

Engineering Application
Near the top of the Citigroup Center building in New York City, there is an object with mass of 4.00105kg4.00105kg size 12{4 "." "00" times "10" rSup { size 8{5} } "kg"} {} on springs that have adjustable force constants. Its function is to dampen wind-driven oscillations of the building by oscillating at the same frequency as the building is being driventhe driving force is transferred to the object, which oscillates instead of the entire building. (a) What effective force constant should the springs have to make the object oscillate with a period of 2.00 s? (b) What energy is stored in the springs for a 2.00-m displacement from equilibrium?

(a) 3.95106N/m3.95106N/m size 12{3 "." "95" times "10" rSup { size 8{6} } "N/m"} {}(b) 7.90106J7.90106J size 12{7 "." "90" times "10" rSup { size 8{6} } "J"} {}

