
The Pauli Exclusion Principle
The Pauli Exclusion Principle
Define the composition of an atom along with its electrons, neutrons, and protons.
Explain the Pauli exclusion principle and its application to the atom.
Specify the shell and subshell symbols and their positions.
Define the position of electrons in different shells of an atom.
State the position of each element in the periodic table according to shell filling.

Multiple-Electron Atoms
All atoms except hydrogen are multiple-electron atoms. The physical and chemical properties of elements are directly related to the number of electrons a neutral atom has. The periodic table of the elements groups elements with similar properties into columns. This systematic organization is related to the number of electrons in a neutral atom, called the atomic number, ZZ size 12{n} {}. We shall see in this section that the exclusion principle is key to the underlying explanations, and that it applies far beyond the realm of atomic physics.In 1925, the Austrian physicist Wolfgang Pauli (see [link]) proposed the following rule: No two electrons can have the same set of quantum numbers. That is, no two electrons can be in the same state. This statement is known as the Pauli exclusion principle, because it excludes electrons from being in the same state. The Pauli exclusion principle is extremely powerful and very broadly applicable. It applies to any identical particles with half-integral intrinsic spinthat is, having s=1/2, 3/2, ...s=1/2, 3/2, ... size 12{s=1/2,`3/2,  "."   "."   "."   "." } {} Thus no two electrons can have the same set of quantum numbers.Pauli Exclusion Principle
No two electrons can have the same set of quantum numbers. That is, no two electrons can be in the same state.

The Austrian physicist Wolfgang Pauli (19001958) played a major role in the development of quantum mechanics. He proposed the exclusion principle; hypothesized the existence of an important particle, called the neutrino, before it was directly observed; made fundamental contributions to several areas of theoretical physics; and influenced many students who went on to do important work of their own. (credit: Nobel Foundation, via Wikimedia Commons)


Let us examine how the exclusion principle applies to electrons in atoms. The quantum numbers involved were defined in Quantum Numbers and Rules as n, l,ml, sn, l,ml, s, and msms size 12{m rSub { size 8{s} } } {}. Since ss size 12{s} {} is always 1/21/2 size 12{1/2} {} for electrons, it is redundant to list ss, and so we omit it and specify the state of an electron by a set of four numbers n,l,ml,msn,l,ml,ms. For example, the quantum numbers 2, 1, 0,1/22, 1, 0,1/2 size 12{ left (2,` 1,` 0,`  - 1/2 right )} {} completely specify the state of an electron in an atom.Since no two electrons can have the same set of quantum numbers, there are limits to how many of them can be in the same energy state. Note that nn size 12{n} {} determines the energy state in the absence of a magnetic field. So we first choose nn size 12{n} {}, and then we see how many electrons can be in this energy state or energy level. Consider the n=1n=1 size 12{n=1} {} level, for example. The only value ll size 12{l} {} can have is 0 (see [link] for a list of possible values once nn size 12{n} {} is known), and thus mlml can only be 0. The spin projection msms can be either +1/2+1/2 or 1/21/2, and so there can be two electrons in the n=1n=1 state. One has quantum numbers 1, 0, 0,+1/21, 0, 0,+1/2, and the other has 1, 0, 0,1/21, 0, 0,1/2. [link] illustrates that there can be one or two electrons having n=1n=1 size 12{n=1} {}, but not three.The Pauli exclusion principle explains why some configurations of electrons are allowed while others are not. Since electrons cannot have the same set of quantum numbers, a maximum of two can be in the n=1n=1 size 12{n=1} {} level, and a third electron must reside in the higher-energy n=2n=2 size 12{n=2} {} level. If there are two electrons in the n=1n=1 size 12{n=1} {} level, their spins must be in opposite directions. (More precisely, their spin projections must differ.)



Shells and Subshells
Because of the Pauli exclusion principle, only hydrogen and helium can have all of their electrons in the n=1n=1 size 12{n=1} {} state. Lithium (see the periodic table) has three electrons, and so one must be in the n=2n=2 size 12{n=2} {} level. This leads to the concept of shells and shell filling. As we progress up in the number of electrons, we go from hydrogen to helium, lithium, beryllium, boron, and so on, and we see that there are limits to the number of electrons for each value of nn size 12{n} {}. Higher values of the shell nn size 12{n} {} correspond to higher energies, and they can allow more electrons because of the various combinations of l,mll,ml size 12{l, m rSub { size 8{l} } } {}, and msms size 12{m rSub { size 8{s} } } {} that are possible. Each value of the principal quantum number nn size 12{n} {} thus corresponds to an atomic shell into which a limited number of electrons can go. Shells and the number of electrons in them determine the physical and chemical properties of atoms, since it is the outermost electrons that interact most with anything outside the atom.The probability clouds of electrons with the lowest value of ll size 12{l} {} are closest to the nucleus and, thus, more tightly bound. Thus when shells fill, they start with l=0l=0 size 12{l=0} {}, progress to l=1l=1 size 12{l=1} {}, and so on. Each value of ll size 12{l} {} thus corresponds to a subshell.
The table given below lists symbols traditionally used to denote shells and subshells.
Shell and Subshell Symbols

Shell
Subshell







n






n



 size 12{n} {}






l






l



 size 12{l} {}


Symbol


1
0




s






s



 size 12{s} {}



2
1




p






p



 size 12{p} {}



3
2




d






d



 size 12{d} {}



4
3




f






f



 size 12{f} {}



5
4




g






g



 size 12{g} {}




5




h






h



 size 12{h} {}




61




i






i



 size 12{i} {}


To denote shells and subshells, we write nlnl size 12{ ital "nl"} {} with a number for nn size 12{n} {} and a letter for ll size 12{l} {}. For example, an electron in the n=1n=1 size 12{n=1} {} state must have l=0l=0 size 12{l=1} {}, and it is denoted as a 1s1s size 12{1s} {} electron. Two electrons in the n=1n=1 size 12{n=1} {} state is denoted as 1s21s2 size 12{1s rSup { size 8{2} } } {}. Another example is an electron in the n=2n=2 size 12{n=2} {} state with l=1l=1 size 12{l=1} {}, written as 2p2p size 12{2p} {}. The case of three electrons with these quantum numbers is written 2p32p3 size 12{2p rSup { size 8{3} } } {}. This notation, called spectroscopic notation, is generalized as shown in [link].


Counting the number of possible combinations of quantum numbers allowed by the exclusion principle, we can determine how many electrons it takes to fill each subshell and shell.
How Many Electrons Can Be in This Shell?
List all the possible sets of quantum numbers for the n=2n=2 size 12{n=2} {} shell, and determine the number of electrons that can be in the shell and each of its subshells.

Strategy

Given n=2n=2 size 12{n=2} {} for the shell, the rules for quantum numbers limit ll size 12{l} {} to be 0 or 1. The shell therefore has two subshells, labeled 2s2s size 12{2s} {} and 2p2p size 12{2p} {}. Since the lowest ll size 12{l} {} subshell fills first, we start with the 2s2s size 12{2s} {} subshell possibilities and then proceed with the 2p2p size 12{2p} {} subshell.

Solution

It is convenient to list the possible quantum numbers in a table, as shown below.




Discussion

It is laborious to make a table like this every time we want to know how many electrons can be in a shell or subshell. There exist general rules that are easy to apply, as we shall now see.

The number of electrons that can be in a subshell depends entirely on the value of ll size 12{l} {}. Once ll size 12{l} {} is known, there are a fixed number of values of mlml size 12{m rSub { size 8{l} } } {}, each of which can have two values for msms size 12{m rSub { size 8{s} } } {} First, since mlml size 12{m rSub { size 8{l} } } {} goes from ll size 12{ - l} {} to l in steps of 1, there are 2l+12l+1 size 12{2l+1} {} possibilities. This number is multiplied by 2, since each electron can be spin up or spin down. Thus the maximum number of electrons that can be in a subshell is 22l+122l+1 size 12{2 left (2l+1 right )} {}.For example, the 2s2s size 12{2s} {} subshell in [link] has a maximum of 2 electrons in it, since 22l+1=20+1=222l+1=20+1=2 size 12{2 left (2l+1 right )=2 left (0+1 right )=2} {} for this subshell. Similarly, the 2p2p size 12{2p} {} subshell has a maximum of 6 electrons, since 22l+1=22+1=622l+1=22+1=6 size 12{2 left (2l+1 right )=2 left (2+1 right )=6} {}. For a shell, the maximum number is the sum of what can fit in the subshells. Some algebra shows that the maximum number of electrons that can be in a shell is 2n22n2 size 12{2n rSup { size 8{2} } } {}.For example, for the first shell n=1n=1 size 12{n=1} {}, and so 2n2=22n2=2 size 12{2n rSup { size 8{2} } =2} {}. We have already seen that only two electrons can be in the n=1n=1 size 12{n=1} {} shell. Similarly, for the second shell, n=2n=2 size 12{n=2} {}, and so 2n2=82n2=8 size 12{2n rSup { size 8{2} } =8} {}. As found in [link], the total number of electrons in the n=2n=2 size 12{n=2} {} shell is 8.  Subshells and Totals for n=3n=3 size 12{n=3} {}
How many subshells are in the n=3n=3 size 12{n=3} {} shell? Identify each subshell, calculate the maximum number of electrons that will fit into each, and verify that the total is 2n22n2 size 12{2n rSup { size 8{2} } } {}.
Strategy

Subshells are determined by the value of ll size 12{l} {}; thus, we first determine which values of ll size 12{ ital "ls"} {} are allowed, and then we apply the equation maximum number of electrons that can be in a subshell =22l+1=22l+1 size 12{2 left (2l+1 right )} {} to find the number of electrons in each subshell.
Solution

Since n=3n=3 size 12{n=3} {}, we know that 
l

l
 can be 
0, 10, 1, or 
22; thus, there are three possible subshells. In standard notation, they are labeled the 3s3s, 3p3p, and 3d3d size 12{3d} {} subshells. We have already seen that 2 electrons can be in an ss state, and 6 in a pp size 12{p} {} state, but let us use the equation maximum number of electrons that can be in a subshell = 22l+122l+1 size 12{2 left (2l+1 right )} {} to calculate the maximum number in each:









3
              

s
              

has
              


l
               

=
               

0
               


;
              

thus,
              

2
              





2l
                  

+
                  

1
                  




=
               

2
               






0
                  

+
                  

1
                  




=
               

2
               













3
              

p
              

has
              


l
               

=
               

1; thus, 2
               






2l
                  

+
                  

1
                  




=
               

2
               






2
                  

+
                  

1
                  




=
               

6
               












3
              

d
              

has
              


l
               

=
               

2; thus, 2
               






2l
                  

+
                  

1
                  




=
               

2
               






4
                  

+
                  

1
                  




=
               

10
               













Total
               

=
               

18
               












(
              

in the
              


n
               

=
               

3 shell
               


)
              


















3
              

s
              

has
              


l
               

=
               

0
               


;
              

thus,
              

2
              





2l
                  

+
                  

1
                  




=
               

2
               






0
                  

+
                  

1
                  




=
               

2
               













3
              

p
              

has
              


l
               

=
               

1; thus, 2
               






2l
                  

+
                  

1
                  




=
               

2
               






2
                  

+
                  

1
                  




=
               

6
               












3
              

d
              

has
              


l
               

=
               

2; thus, 2
               






2l
                  

+
                  

1
                  




=
               

2
               






4
                  

+
                  

1
                  




=
               

10
               













Total
               

=
               

18
               












(
              

in the
              


n
               

=
               

3 shell
               


)
              








The equation maximum number of electrons that can be in a shell = 2n22n2 size 12{2n rSup { size 8{2} } } {} gives the maximum number in the n=3n=3 size 12{n=3} {} shell to beMaximum number of electrons=2n2=232=29=18.Maximum number of electrons=2n2=232=29=18.
Discussion

The total number of electrons in the three possible subshells is thus the same as the formula 2n22n2 size 12{2n rSup { size 8{2} } } {}. In standard (spectroscopic) notation, a filled n=3n=3 size 12{n=3} {} shell is denoted as 3s23p63d103s23p63d10 size 12{3s rSup { size 8{2} } 3p rSup { size 8{6} } 3d rSup { size 8{"10"} } } {}.  Shells do not fill in a simple manner. Before the n=3n=3 size 12{n=3} {} shell is completely filled, for example, we begin to find electrons in the n=4n=4 size 12{n=4} {} shell.

Shell Filling and the Periodic Table
[link] shows electron configurations for the first 20 elements in the periodic table, starting with hydrogen and its single electron and ending with calcium. The Pauli exclusion principle determines the maximum number of electrons allowed in each shell and subshell. But the order in which the shells and subshells are filled is complicated because of the large numbers of interactions between electrons.
Electron Configurations of Elements Hydrogen Through Calcium
Element
Number of electrons (Z)
Ground state configuration

H
1




1

s

1








1

s

1





 size 12{1s rSup { size 8{1} } } {}








He
2




1

s

2








1

s

2





 size 12{1s rSup { size 8{2} } } {}








Li
3




1

s

2








1

s

2





 size 12{1s rSup { size 8{2} } } {}






2

s

1








2

s

1





 size 12{2s rSup { size 8{1} } } {}







Be
4
"




2

s

2








2

s

2





 size 12{2s rSup { size 8{2} } } {}







B
5
"




2

s

2








2

s

2





 size 12{2s rSup { size 8{2} } } {}






2

p

1








2

p

1





 size 12{2p rSup { size 8{1} } } {}






C
6
"




2

s

2








2

s

2





 size 12{2s rSup { size 8{2} } } {}






2

p

2








2

p

2





 size 12{2p rSup { size 8{2} } } {}






N
7
"




2

s

2








2

s

2





 size 12{2s rSup { size 8{2} } } {}






2

p

3








2

p

3





 size 12{2p rSup { size 8{3} } } {}






O
8
"




2

s

2








2

s

2





 size 12{2s rSup { size 8{2} } } {}






2

p

4








2

p

4





 size 12{2p rSup { size 8{4} } } {}






F
9
"




2

s

2








2

s

2





 size 12{2s rSup { size 8{2} } } {}






2

p

5








2

p

5





 size 12{2p rSup { size 8{5} } } {}






Ne
10
"




2

s

2








2

s

2





 size 12{2s rSup { size 8{2} } } {}






2

p

6








2

p

6





 size 12{2p rSup { size 8{6} } } {}






Na
11
"




2

s

2








2

s

2





 size 12{2s rSup { size 8{2} } } {}






2

p

6








2

p

6





 size 12{2p rSup { size 8{6} } } {}






3

s

1








3

s

1





 size 12{3s rSup { size 8{1} } } {}





Mg
12
"
"
"




3

s

2








3

s

2





 size 12{3s rSup { size 8{2} } } {}





Al
13
"
"
"




3

s

2








3

s

2





 size 12{3s rSup { size 8{2} } } {}






3

p

1








3

p

1





 size 12{3p rSup { size 8{1} } } {}




Si
14
"
"
"




3

s

2








3

s

2





 size 12{3s rSup { size 8{2} } } {}






3

p

2








3

p

2





 size 12{3p rSup { size 8{2} } } {}




P
15
"
"
"




3

s

2








3

s

2





 size 12{3s rSup { size 8{2} } } {}






3

p

3








3

p

3





 size 12{3p rSup { size 8{3} } } {}




S
16
"
"
"




3

s

2








3

s

2





 size 12{3s rSup { size 8{2} } } {}






3

p

4








3

p

4





 size 12{3p rSup { size 8{4} } } {}




Cl
17
"
"
"




3

s

2








3

s

2





 size 12{3s rSup { size 8{2} } } {}






3

p

5








3

p

5





 size 12{3p rSup { size 8{5} } } {}




Ar
18
"
"
"




3

s

2








3

s

2





 size 12{3s rSup { size 8{2} } } {}






3

p

6








3

p

6





 size 12{3p rSup { size 8{6} } } {}




K
19
"
"
"




3

s

2








3

s

2





 size 12{3s rSup { size 8{2} } } {}






3

p

6








3

p

6





 size 12{3p rSup { size 8{6} } } {}






4

s

1








4

s

1





 size 12{4s rSup { size 8{1} } } {}



Ca
20
"
"
"
"
"




4

s

2








4

s

2





 size 12{4s rSup { size 8{2} } } {}


Examining the above table, you can see that as the number of electrons in an atom increases from 1 in hydrogen to 2 in helium and so on, the lowest-energy shell gets filled firstthat is, the n=1n=1 size 12{n=1} {} shell fills first, and then the n=2n=2 size 12{n=2} {} shell begins to fill. Within a shell, the subshells fill starting with the lowest ll size 12{l} {}, or with the ss size 12{s} {} subshell, then the pp size 12{p} {}, and so on, usually until all subshells are filled. The first exception to this occurs for potassium, where the 4s4s size 12{4s} {} subshell begins to fill before any electrons go into the 3d3d size 12{3d} {} subshell. The next exception is not shown in [link]; it occurs for rubidium, where the 5s5s size 12{5s} {} subshell starts to fill before the 4d4d size 12{4d} {} subshell. The reason for these exceptions is that l=0l=0 size 12{l=0} {} electrons have probability clouds that penetrate closer to the nucleus and, thus, are more tightly bound (lower in energy).[link] shows the periodic table of the elements, through element 118. Of special interest are elements in the main groups, namely, those in the columns numbered 1, 2, 13, 14, 15, 16, 17, and 18.
Periodic table of the elements (credit: National Institute of Standards and Technology, U.S. Department of Commerce)


The number of electrons in the outermost subshell determines the atoms chemical properties, since it is these electrons that are farthest from the nucleus and thus interact most with other atoms. If the outermost subshell can accept or give up an electron easily, then the atom will be highly reactive chemically. Each group in the periodic table is characterized by its outermost electron configuration. Perhaps the most familiar is Group 18 (Group VIII), the noble gases (helium, neon, argon, etc.). These gases are all characterized by a filled outer subshell that is particularly stable. This means that they have large ionization energies and do not readily give up an electron. Furthermore, if they were to accept an extra electron, it would be in a significantly higher level and thus loosely bound. Chemical reactions often involve sharing electrons. Noble gases can be forced into unstable chemical compounds only under high pressure and temperature.
Group 17 (Group VII) contains the halogens, such as fluorine, chlorine, iodine and bromine, each of which has one less electron than a neighboring noble gas. Each halogen has 5 pp size 12{p} {} electrons (a p5p5 size 12{p} {}  configuration), while the pp size 12{p} {} subshell can hold 6 electrons. This means the halogens have one vacancy in their outermost subshell. They thus readily accept an extra electron (it becomes tightly bound, closing the shell as in noble gases) and are highly reactive chemically. The halogens are also likely to form singly negative ions, such as C1C1 size 12{C1 rSup { size 8{ - {}} } } {}, fitting an extra electron into the vacancy in the outer subshell. In contrast, alkali metals, such as sodium and potassium, all have a single ss size 12{s} {} electron in their outermost subshell (an 



s

1









s

1





 size 12{s rSup { size 8{1} } } {}

     configuration) and are members of Group 1 (Group I). These elements easily give up their extra electron and are thus highly reactive chemically. As you might expect, they also tend to form singly positive ions, such as Na+Na+ size 12{"Na" rSup { size 8{+{}} } } {}, by losing their loosely bound outermost electron. They are metals (conductors), because the loosely bound outer electron can move freely.Of course, other groups are also of interest. Carbon, silicon, and germanium, for example, have similar chemistries and are in Group 4 (Group IV). Carbon, in particular, is extraordinary in its ability to form many types of bonds and to be part of long chains, such as inorganic molecules. The large group of what are called transitional elements is characterized by the filling of the dd size 12{d} {} subshells and crossing of energy levels. Heavier groups, such as the lanthanide series, are more complextheir shells do not fill in simple order. But the groups recognized by chemists such as Mendeleev have an explanation in the substructure of atoms.
PhET Explorations: Build an Atom
Build an atom out of protons, neutrons, and electrons, and see how the element, charge, and mass change. Then play a game to test your ideas!
Build an Atom



Section SummaryThe state of a system is completely described by a complete set of quantum numbers. This set is written as n, l,ml,msn, l,ml,ms.
The Pauli exclusion principle says that no two electrons can have the same set of quantum numbers; that is, no two electrons can be in the same state.
This exclusion limits the number of electrons in atomic shells and subshells. Each value of nn size 12{n} {} corresponds to a shell, and each value of ll size 12{l} {} corresponds to a subshell.
The maximum number of electrons that can be in a subshell is 22l+122l+1 size 12{2 left (2l+1 right )} {}.
The maximum number of electrons that can be in a shell is 2n22n2 size 12{2n rSup { size 8{2} } } {}.
Conceptual Questions

Identify the shell, subshell, and number of electrons for the following: (a) 2p32p3 size 12{2p rSup { size 8{3} } } {}. (b) 4d94d9 size 12{4d rSup { size 8{9} } } {}. (c) 3s13s1 size 12{3s rSup { size 8{1} } } {}. (d) 5g165g16 size 12{5g rSup { size 8{"16"} } } {}.

Which of the following are not allowed? State which rule is violated for any that are not allowed. (a) 1p31p3 size 12{1p rSup { size 8{3} } } {} (b) 2p82p8 size 12{2p rSup { size 8{8} } } {}(c) 3g113g11 size 12{3g rSup { size 8{"11"} } } {} (d) 4f24f2 size 12{4f rSup { size 8{2} } } {}

Problem Exercises


(a) How many electrons can be in the n=4n=4 size 12{n=4} {} shell?
(b) What are its subshells, and how many electrons can be in each?


(a) 32. (b) 2 in s, 6 in p, 10 in d,2 in s, 6 in p, 10 in d, and 14 in ff size 12{f} {}, for a total of 32.



(a) What is the minimum value of 1 for a subshell that has 11 electrons in it? (b) If this subshell is in the 


n
=
5



n
=
5

 shell, what is the spectroscopic notation for this atom?

(a) If one subshell of an atom has 9 electrons in it, what is the minimum value of ll size 12{l} {}?  (b) What is the spectroscopic notation for this atom, if this subshell is part of the n=3n=3 size 12{n=3} {} shell?


(a) 2
(b) 3d93d9 size 12{3d rSup { size 8{9} } } {}


(a) List all possible sets of quantum numbers n,l,ml,msn,l,ml,ms for the n=3n=3 shell, and determine the number of electrons that can be in the shell and each of its subshells.(b) Show that the number of electrons in the shell equals 2n22n2 size 12{2n rSup { size 8{2} } } {} and that the number in each subshell is 22l+122l+1 size 12{2 left (2l+1 right )} {}.

Which of the following spectroscopic notations are not allowed? (a) 



5

s
1




5

s
1




(b)


1

d
1




1

d
1



(c)


4

s
3




4

s
3



(d) 


3

p
7




3

p
7



(e) 


5

g
15




5

g
15


. State which rule is violated for each that is not allowed.

(b) 

nl

nl
  is violated,(c) cannot have 3 electrons in 
s

s
  subshell since  


3
>
(
2
l
+
1
)
=
2




3
>
(
2
l
+
1
)
=
2


(d) cannot have 7 electrons in 
p

p
  subshell since  


7
>
(
2
l
+
1
)
=
2
(
2
+
1
)
=
6



7
>
(
2
l
+
1
)
=
2
(
2
+
1
)
=
6




Which of the following spectroscopic notations are allowed (that is, which violate none of the rules regarding values of quantum numbers)? (a) 1s11s1 size 12{1s rSup { size 8{1} } } {}(b) 1d31d3 size 12{1d rSup { size 8{3} } } {}(c) 4s24s2 size 12{4s rSup { size 8{2} } } {} (d) 3p73p7 size 12{3p rSup { size 8{7} } } {}(e) 6h206h20 size 12{6h rSup { size 8{"20"} } } {}


(a) Using the Pauli exclusion principle and the rules relating the allowed values of the quantum numbers n,l,ml,msn,l,ml,ms size 12{ left (n,`l,`m rSub { size 8{l} } ,`m rSub { size 8{s} }  right )} {}, prove that the maximum number of electrons in a subshell is 2n22n2 size 12{2n rSup { size 8{2} } } {}. (b) In a similar manner, prove that the maximum number of electrons in a shell is 2n2.


(a) The number of different values of mlml size 12{m rSub { size 8{l} } } {} is l,(l1),...,0l,(l1),...,0 for each l>0l>0 size 12{l>0} {} and one for l=0(2l+1).l=0(2l+1). size 12{l=0 drarrow  \( 2l+1 \)   "." } {} Also an overall factor of 2 since each mlml size 12{m rSub { size 8{l} } } {} can have msms size 12{m rSub { size 8{s} } } {} equal to either +1/2+1/2 size 12{+1/2} {} or 1/22(2l+1)1/22(2l+1) size 12{ - 1/2 drarrow 2 \( 2l+1 \) } {}. (b) for each value of ll size 12{l} {}, you get 2(2l+1)2(2l+1) size 12{2 \( 2l+1 \) } {}
=0, 1, 2, ...,(n1)2(2)(0)+1+(2)(1)+1+....+(2)(n1)+1=


21+3+...+(2n3)+(2n1)




n
terms


=0, 1, 2, ...,(n1)2(2)(0)+1+(2)(1)+1+....+(2)(n1)+1=


21+3+...+(2n3)+(2n1)




n
terms


 size 12{ {}=0, 1," 2, " "."  "."  "." ", " \( "n1" \)   drarrow 2 left lbrace  left [ \( 2 \)  \( 0 \) +1 right ]+ left [ \( 2 \)  \( 1 \) +1 right ]+ "."  "."  "."  "." + left [ \( 2 \)  \( n - 1 \) +1 right ] right rbrace = {2 left [1+3+ "."  "."  "." + \( 2n - 3 \) + \( 2n - 1 \)  right ]} underbrace { size 8{n"  terms"} } } {} to see that the expression in the box is =n2,=n2, imagine taking (n1)(n1) size 12{ \( n - 1 \) } {} from the last term and adding it to first term =21+(n1)+3+...+(2n3)+(2n1)(n1)=2n+3+....+(2n3)+n.=21+(n1)+3+...+(2n3)+(2n1)(n1)=2n+3+....+(2n3)+n. size 12{ {}=2 left [1+ \( n"1" \) +3+ "."  "."  "." + \( 2n - 3 \) + \( 2n - 1 \)  \( n - 1 \)  right ]=2 left [n+3+ "."  "."  "."  "." + \( 2n - 3 \) +n right ]  "." } {} Now take (n3)(n3) size 12{ \( n - 3 \) } {} from penultimate term and add to the second term 2
n+n+...+n+n



n
terms


=2n22
n+n+...+n+n



n
terms


=2n2 size 12{2 { left [n+n+ "."  "."  "." +n+n right ]} underbrace { size 8{n" terms"} } =2n rSup { size 8{2} } } {}.

Integrated Concepts
Estimate the density of a nucleus by calculating the density of a proton, taking it to be a sphere 1.2 fm in diameter. Compare your result with the value estimated in this chapter.


Integrated Concepts
The electric and magnetic forces on an electron in the CRT in [link] are supposed to be in opposite directions. Verify this by determining the direction of each force for the situation shown. Explain how you obtain the directions (that is, identify the rules used).

The electric force on the electron is up (toward the positively charged plate). The magnetic force is down (by the RHR).



(a) What is the distance between the slits of a diffraction grating that produces a first-order maximum for the first Balmer line at an angle of 
20.0

20.0
?(b) At what angle will the fourth line of the Balmer series appear in first order?(c) At what angle will the second-order maximum be for the first line?


Integrated Concepts
A galaxy moving away from the earth has a speed of 


0.0100c



0.0100c

. What wavelength do we observe for an ni=7ni=7 size 12{n rSub { size 8{i} } =7} {} to nf=2nf=2 size 12{n rSub { size 8{f} } =2} {} transition for hydrogen in that galaxy?

401 nm



Integrated Concepts
Calculate the velocity of a star moving relative to the earth if you observe a wavelength of 91.0 nm for ionized hydrogen capturing an electron directly into the lowest orbital (that is, a ni=ni= size 12{n rSub { size 8{i} } = infinity } {} to nf=1nf=1 size 12{n rSub { size 8{f} } =1} {}, or a Lyman series transition).

Integrated Concepts
In a Millikan oil-drop experiment using a setup like that in [link], a 500-V potential difference is applied to plates separated by 2.50 cm. (a) What is the mass of an oil drop having two extra electrons that is suspended motionless by the field between the plates? (b) What is the diameter of the drop, assuming it is a sphere with the density of olive oil?

(a) 6.541016 kg6.541016 kg size 12{6 "." "53" times "10" rSup { size 8{ - "16"} } " kg"} {}(b) 5.54107 m5.54107 m size 12{5 "." "53" times "10" rSup { size 8{ - 7} } " m"} {}
 Integrated Concepts
What double-slit separation would produce a first-order maximum at 3.003.00 size 12{3 "." "00"} {} for 25.0-keV x rays? The small answer indicates that the wave character of x rays is best determined by having them interact with very small objects such as atoms and molecules.
 Integrated Concepts 
In a laboratory experiment designed to duplicate Thomsons determination of qe/meqe/me size 12{q rSub { size 8{e} } /m rSub { size 8{e} } } {}, a beam of electrons having a velocity of 6.00107m/s6.00107m/s size 12{6 "." "00" times "10" rSup { size 8{7} } `"m/s"} {} enters a 5.00103T5.00103T magnetic field. The beam moves perpendicular to the field in a path having a 6.80-cm radius of curvature. Determine qe/meqe/me from these observations, and compare the result with the known value.





1
.

76


10

11



 C/kg








1
.

76


10

11



 C/kg




 size 12{1 "." "76" times "10" rSup { size 8{"11"} } " C/kg"} {}

    , which agrees with the known value of 



1
.

759


10

11



 C/kg








1
.

759


10

11



 C/kg




 size 12{1 "." "759" times "10" rSup { size 8{"11"} } " C/kg"} {}
 to within the precision of the measurement
    

Integrated Concepts Find the value of  
l

l
, the orbital angular momentum quantum number, for the moon around the earth. The extremely large value obtained implies that it is impossible to tell the difference between adjacent quantized orbits for macroscopic objects.
 Integrated Concepts
Particles called muons exist in cosmic rays and can be created in particle accelerators. Muons are very similar to electrons, having the same charge and spin, but they have a mass 207 times greater. When muons are captured by an atom, they orbit just like an electron but with a smaller radius, since the mass in aB=h242mekqe2=0.5291010 maB=h242mekqe2=0.5291010 m is 207 meme.(a) Calculate the radius of the n=1n=1 size 12{n=1} {} orbit for a muon in a uranium ion (Z=92Z=92 size 12{Z="92"} {}).
(b) Compare this with the 7.5-fm radius of a uranium nucleus. Note that since the muon orbits inside the electron, it falls into a hydrogen-like orbit. Since your answer is less than the radius of the nucleus, you can see that the photons emitted as the muon falls into its lowest orbit can give information about the nucleus.

(a) 2.78 fm
(b) 0.37 of the nuclear radius.


Integrated Concepts Calculate the minimum amount of energy in joules needed to create a population inversion in a helium-neon laser containing 



1
.

00


10



4












1
.

00


10



4








 size 12{1 "." "00" times "10" rSup { size 8{ - 4} } } {}

     moles of neon.
 Integrated Concepts
A carbon dioxide laser used in surgery emits infrared radiation with a wavelength of  

10.6 m

10.6 m
. In 1.00 ms, this laser raised the temperature of 1.00 cm31.00 cm3 of flesh to 100C100C and evaporated it. (a) How many photons were required? You may assume flesh has the same heat of vaporization as water. (b) What was the minimum power output during the flash?

(a) 1.3410231.341023 size 12{1 "." "34" times "10" rSup { size 8{"23"} } } {}
(b) 2.52 MW


 Integrated Concepts
 Suppose an MRI scanner uses 100-MHz radio waves.
(a) Calculate the photon energy.
(b) How does this compare to typical molecular binding energies?

 Integrated Concepts
 (a) An excimer laser used for vision correction emits 193-nm UV. Calculate the photon energy in eV.
(b) These photons are used to evaporate corneal tissue, which is very similar to water in its properties. Calculate the amount of energy needed per molecule of water to make the phase change from liquid to gas. That is, divide the heat of vaporization in kJ/kg by the number of water molecules in a kilogram.
(c) Convert this to eV and compare to the photon energy. Discuss the implications.


(a) 6.42 eV(b) 7.271020 J/molecule7.271020 J/molecule size 12{6 "." "75" times "10" rSup { size 8{ - "20"} } " J/molecule"} {}(c) 0.454 eV, 14.1 times less than a single UV photon. Therefore, each photon will evaporate approximately 14 molecules of tissue. This gives the surgeon a rather precise method of removing corneal tissue from the surface of the eye.

 Integrated Concepts
 A neighboring galaxy rotates on its axis so that stars on one side move toward us as fast as 200 km/s, while those on the other side move away as fast as 200 km/s. This causes the EM radiation we receive to be Doppler shifted by velocities over the entire range of 200 km/s. What range of wavelengths will we observe for the 656.0-nm line in the Balmer series of hydrogen emitted by stars in this galaxy. (This is called line broadening.)

 Integrated Concepts
A pulsar is a rapidly spinning remnant of a supernova. It rotates on its axis, sweeping hydrogen along with it so that hydrogen on one side moves toward us as fast as 50.0 km/s, while that on the other side moves away as fast as 50.0 km/s. This means that the EM radiation we receive will be Doppler shifted over a range of 50.0 km/s50.0 km/s size 12{ +- "50" "." 0" km/s"} {}. What range of wavelengths will we observe for the 91.20-nm line in the Lyman series of hydrogen? (Such line broadening is observed and actually provides part of the evidence for rapid rotation.)

91.18 nm to 91.22 nm


 Integrated Concepts
 Prove that the velocity of charged particles moving along a straight path through perpendicular electric and magnetic fields is v=E/Bv=E/B size 12{v=E/B} {}. Thus crossed electric and magnetic fields can be used as a velocity selector independent of the charge and mass of the particle involved.

 Unreasonable Results
(a) What voltage must be applied to an X-ray tube to obtain 0.0100-fm-wavelength X-rays for use in exploring the details of nuclei? (b) What is unreasonable about this result? (c) Which assumptions are unreasonable or inconsistent?


(a) 1.241011 V1.241011 V size 12{1 "." "24" times "10" rSup { size 8{"11"} } " V"} {}(b) The voltage is extremely large compared with any practical value.
(c) The assumption of such a short wavelength by this method is unreasonable.

 Unreasonable Results
A student in a physics laboratory observes a hydrogen spectrum with a diffraction grating for the purpose of measuring the wavelengths of the emitted radiation. In the spectrum, she observes a yellow line and finds its wavelength to be 589 nm. (a) Assuming this is part of the Balmer series, determine nini size 12{n rSub { size 8{i} } } {}, the principal quantum number of the initial state. (b) What is unreasonable about this result? (c) Which assumptions are unreasonable or inconsistent?
 Construct Your Own Problem
 The solar corona is so hot that most atoms in it are ionized. Consider a hydrogen-like atom in the corona that has only a single electron. Construct a problem in which you calculate selected spectral energies and wavelengths of the Lyman, Balmer, or other series of this atom that could be used to identify its presence in a very hot gas. You will need to choose the atomic number of the atom, identify the element, and choose which spectral lines to consider.

 Construct Your Own Problem
Consider the Doppler-shifted hydrogen spectrum received from a rapidly receding galaxy. Construct a problem in which you calculate the energies of selected spectral lines in the Balmer series and examine whether they can be described with a formula like that in the equation 1=R1nf21ni21=R1nf21ni2 size 12{ {  {1}  over  {} } =R left ( {  {1}  over  {n rSub { size 8{f} }  rSup { size 8{2} } } }  -  {  {1}  over  {n rSub { size 8{i} }  rSup { size 8{2} } } }  right )} {}, but with a different constant 
R

R
.

Footnotes1 It is unusual to deal with subshells having 
l

l
 greater than 6, but when encountered, they continue to be labeled in alphabetical order.Glossary
atomic number the number of protons in the nucleus of an atom
Pauli exclusion principle a principle that states that no two electrons can have the same set of quantum numbers; that is, no two electrons can be in the same state
shell a probability cloud for electrons that has a single principal quantum number
subshell the probability cloud for electrons that has a single angular momentum quantum number 
l

l
 

