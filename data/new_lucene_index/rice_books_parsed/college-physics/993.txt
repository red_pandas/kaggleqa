
Introduction: Further Applications of Newtons Laws
Introduction: Further Applications of Newtons Laws
        class="introduction"
      
class="section-summary" title="Section Summary"
class="conceptual-questions" title="Conceptual Questions"
class="problems-exercises" title="Problems & Exercises"
Total hip replacement surgery has become a common procedure. The head (or ball) of the patients femur fits into a cup that has a hard plastic-like inner lining. (credit: National Institutes of Health, via Wikimedia Commons)



Describe the forces on the hip joint. What means are taken to ensure that this will be a good movable joint? From the photograph (for an adult) in [link], estimate the dimensions of the artificial device.It is difficult to categorize forces into various types (aside from the four basic forces discussed in previous chapter). We know that a net force affects the motion, position, and shape of an object. It is useful at this point to look at some particularly interesting and common forces that will provide further applications of Newtons laws of motion. We have in mind the forces of friction, air or liquid drag, and deformation.

