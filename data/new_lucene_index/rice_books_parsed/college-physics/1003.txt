
Single Slit Diffraction
Single Slit DiffractionDiscuss the single slit diffraction pattern.
Light passing through a single slit forms a diffraction pattern somewhat different from those formed by double slits or diffraction gratings. [link] shows a single slit diffraction pattern. Note that the central maximum is larger than those on either side, and that the intensity decreases rapidly on either side. In contrast, a diffraction grating produces evenly spaced lines that dim slowly on either side of center.
(a) Single slit diffraction pattern. Monochromatic light passing through a single slit has a central maximum and many smaller and dimmer maxima on either side. The central maximum is six times higher than shown. (b) The drawing shows the bright central maximum and dimmer and thinner maxima on either side.


The analysis of single slit diffraction is illustrated in [link]. Here we consider light coming from different parts of the same slit. According to Huygenss principle, every part of the wavefront in the slit emits wavelets. These are like rays that start out in phase and head in all directions. (Each ray is perpendicular to the wavefront of a wavelet.) Assuming the screen is very far away compared with the size of the slit, rays heading toward a common destination are nearly parallel. When they travel straight ahead, as in [link](a), they remain in phase, and a central maximum is obtained. However, when rays travel at an angle  size 12{} {} relative to the original direction of the beam, each travels a different distance to a common location, and they can arrive in or out of phase. In [link](b), the ray from the bottom travels a distance of one wavelength  size 12{} {} farther than the ray from the top. Thus a ray from the center travels a distance /2/2 size 12{/2} {} farther than the one on the left, arrives out of phase, and interferes destructively. A ray from slightly above the center and one from slightly above the bottom will also cancel one another. In fact, each ray from the slit will have another to interfere destructively, and a minimum in intensity will occur at this angle. There will be another minimum at the same angle to the right of the incident direction of the light.Light passing through a single slit is diffracted in all directions and may interfere constructively or destructively, depending on the angle. The difference in path length for rays from either side of the slit is seen to be Dsin Dsin  size 12{D`"sin"`} {}.


At the larger angle shown in [link](c), the path lengths differ by 3/23/2 size 12{3/2} {} for rays from the top and bottom of the slit. One ray travels a distance  size 12{} {} different from the ray from the bottom and arrives in phase, interfering constructively. Two rays, each from slightly above those two, will also add constructively. Most rays from the slit will have another to interfere with constructively, and a maximum in intensity will occur at this angle. However, all rays do not interfere constructively for this situation, and so the maximum is not as intense as the central maximum. Finally, in [link](d), the angle shown is large enough to produce a second minimum. As seen in the figure, the difference in path length for rays from either side of the slit is DsinDsin size 12{D`"sin"} {}, and we see that a destructive minimum is obtained when this distance is an integral multiple of the wavelength.A graph of single slit diffraction intensity showing the central maximum to be wider and much more intense than those to the sides. In fact the central maximum is six times higher than shown here.


Thus, to obtain destructive interference for a single slit,Dsin=m,form=1,1,2,2,3, (destructive),Dsin=m,form=1,1,2,2,3, (destructive), size 12{D`"sin"= ital "m",~m="1,"`"2,"`"3,"` dotslow } {}where DD size 12{D} {} is the slit width,  size 12{} {} is the lights wavelength,  size 12{} {} is the angle relative to the original direction of the light, and mm size 12{m} {} is the order of the minimum. [link] shows a graph of intensity for single slit interference, and it is apparent that the maxima on either side of the central maximum are much less intense and not as wide. This is consistent with the illustration in [link](b).Calculating Single Slit Diffraction
Visible light of wavelength 550 nm falls on a single slit and produces its second diffraction minimum at an angle of 45.045.0 size 12{"45" "." 0} {} relative to the incident direction of the light. (a) What is the width of the slit? (b) At what angle is the first minimum produced?A graph of the single slit diffraction pattern is analyzed in this example.



Strategy

From the given information, and assuming the screen is far away from the slit, we can use the equation Dsin=mDsin=m size 12{D`"sin"= ital "m"} {} first to find DD size 12{D} {}, and again to find the angle for the first minimum 11 size 12{ rSub { size 8{1} } } {}.
Solution for (a)

We are given that =550 nm=550 nm size 12{="500"`"nm"} {}, m=2m=2 size 12{m=2} {}, and 2=45.02=45.0. Solving the equation Dsin=mDsin=m size 12{D`"sin"= ital "m"} {} for DD size 12{D} {} and substituting known values gives



 D
 =





m

sin



2





=


2
(
550 nm
)


sin 45.0







 =




1100


10



9





0.707





 =


1.56



10



6


.










 D
 =





m

sin



2





=


2
(
550 nm
)


sin 45.0







 =




1100


10



9





0.707





 =


1.56



10



6


.







Solution for (b)

Solving the equation Dsin=mDsin=m size 12{D`"sin"= ital "m"} {} for sin1sin1 size 12{"sin" rSub { size 8{1} } } {} and substituting the known values givessin1=mD=1550109m1.56106m.sin1=mD=1550109m1.56106m. size 12{"sin" rSub { size 8{1} } = {  {m}  over  {D} } = {  {1 left ("550" times "10" rSup { size 8{ - 9} } m right )}  over  {1 "." "56" times "10" rSup { size 8{ - 6} } m} } } {}Thus the angle 11 size 12{ rSub { size 8{1} } } {} is
1=sin10.354=20.7.1=sin10.354=20.7. size 12{ rSub { size 8{1} } ="sin" rSup { size 8{ - 1} } 0 "." "354"="20" "." 7} {}
Discussion

We see that the slit is narrow (it is only a few times greater than the wavelength of light). This is consistent with the fact that light must interact with an object comparable in size to its wavelength in order to exhibit significant wave effects such as this single slit diffraction pattern. We also see that the central maximum extends 20.720.7 on either side of the original beam, for a width of about 4141. The angle between the first and second minima is only about 24(45.020.7)24(45.020.7) size 12{"24"` \( "45" "." 0 - "20" "." 7 \) } {}. Thus the second maximum is only about half as wide as the central maximum.
Section SummaryA single slit produces an interference pattern characterized by a broad central maximum with narrower and dimmer maxima to the sides.
There is destructive interference for a single slit when 
Dsin=m,
(form=1,1,
2,2,3,)Dsin=m,
(form=1,1,
2,2,3,) size 12{D`"sin"= ital "m",~m="1,"`"2,"`"3,"` dotslow } {}, where DD is the slit width, 
  is the lights wavelength, 
  is the angle relative to the original direction of the light, and 
mm  is the order of the minimum. Note that there is no 
m=0m=0 size 12{m=0} {} minimum.

Conceptual Questions


As the width of the slit producing a single-slit diffraction pattern is reduced, how will the diffraction pattern produced change?


Problems & Exercises

(a) At what angle is the first minimum for 550-nm light falling on a single slit of width 1.00m1.00m size 12{1 "." "00"`"m"} {}? (b) Will there be a second minimum?

(a) 33.433.4 size 12{"33" "." 4} {}(b) No




(a) Calculate the angle at which a 2.00-m2.00-m size 12{2 "." "00""-m"} {}-wide slit produces its first minimum for 410-nm violet light. (b) Where is the first minimum for 700-nm red light?



(a) How wide is a single slit that produces its first minimum for 633-nm light at an angle of 28.028.0 size 12{"28" "." 0} {}? (b) At what angle will the second minimum be?

(a) 1.35106m1.35106m size 12{1 "." "35" times "10" rSup { size 8{ - 6} } `m} {}(b) 69.969.9 size 12{"69" "." 9} {}



(a) What is the width of a single slit that produces its first minimum at 60.060.0 size 12{"60" "." 0} {} for 600-nm light? (b) Find the wavelength of light that has its first minimum at 62.062.0 size 12{"62" "." 0} {}.


Find the wavelength of light that has its third minimum at an angle of 48.648.6 size 12{"48" "." 6} {} when it falls on a single slit of width 3.00m3.00m size 12{3 "." "00"`"m"} {}.

750 nm




Calculate the wavelength of light that produces its first minimum at an angle of 36.936.9 size 12{"36" "." 9} {} when falling on a single slit of width 1.00m1.00m size 12{1 "." "00"`"m"} {}.


(a) Sodium vapor light averaging 589 nm in wavelength falls on a single slit of width 7.50m7.50m size 12{7 "." "50"`"m"} {}. At what angle does it produces its second minimum? (b) What is the highest-order minimum produced?

(a) 9.049.04 size 12{9 "." "04"} {}(b) 12




(a) Find the angle of the third diffraction minimum for 633-nm light falling on a slit of width 20.0m20.0m size 12{"20" "." 0`"m"} {}. (b) What slit width would place this minimum at 85.085.0 size 12{"85" "." 0} {}? Explicitly show how you follow the steps in Problem-Solving Strategies for Wave Optics


(a) Find the angle between the first minima for the two sodium vapor lines, which have wavelengths of 589.1 and 589.6 nm, when they fall upon a single slit of width 2.00m2.00m size 12{2 "." "00"`"m"} {}. (b) What is the distance between these minima if the diffraction pattern falls on a screen 1.00 m from the slit? (c) Discuss the ease or difficulty of measuring such a distance.

(a) 0.01500.0150 size 12{0 "." "0150"} {}(b) 0.262 mm
(c) This distance is not easily measured by human eye, but under a microscope or magnifying glass it is quite easily measurable.




(a) What is the minimum width of a single slit (in multiples of  size 12{} {}) that will produce a first minimum for a wavelength  size 12{} {}? (b) What is its minimum width if it produces 50 minima? (c) 1000 minima?



(a) If a single slit produces a first minimum at 14.514.5 size 12{"14" "." 5} {}, at what angle is the second-order minimum? (b) What is the angle of the third-order minimum? (c) Is there a fourth-order minimum? (d) Use your answers to illustrate how the angular width of the central maximum is about twice the angular width of the next maximum (which is the angle between the first and second minima).

(a) 30.130.1 size 12{"30" "." 1} {}(b) 48.748.7 size 12{"48" "." 7} {}(c) No
(d) 21=(2)(14.5)=29,21=30.0514.5=15.5621=(2)(14.5)=29,21=30.0514.5=15.56 size 12{2 rSub { size 8{1} } = \( 2 \)  \( "14" "." 5 \) ="29",~ rSub { size 8{2} }  -  rSub { size 8{1} } ="30" "." "05" - "14" "." 5"=""15" "." "56"} {}. Thus, 29(2)(15.56)=31.129(2)(15.56)=31.1 size 12{"29" approx  \( 2 \)  \( "15" "." "56" \) ="31" "." 1} {}.



A double slit produces a diffraction pattern that is a combination of single and double slit interference. Find the ratio of the width of the slits to the separation between them, if the first minimum of the single slit pattern falls on the fifth maximum of the double slit pattern. (This will greatly reduce the intensity of the fifth maximum.)




Integrated Concepts

A water break at the entrance to a harbor consists of a rock barrier with a 50.0-m-wide opening. Ocean waves of 20.0-m wavelength approach the opening straight on. At what angle to the incident direction are the boats inside the harbor most protected against wave action?


23.623.6 size 12{"23" "." 6} {} and 53.153.1 size 12{"53" "." 1} {}




Integrated Concepts

An aircraft maintenance technician walks past a tall hangar door that acts like a single slit for sound entering the hangar. Outside the door, on a line perpendicular to the opening in the door, a jet engine makes a 600-Hz sound. At what angle with the door will the technician observe the first minimum in sound intensity if the vertical opening is 0.800 m wide and the speed of sound is 340 m/s?


Glossary
destructive interference for a single slit occurs when Dsin=m,(form=1,1,2,2,3,)Dsin=m,(form=1,1,2,2,3,) size 12{D`"sin"= ital "m",~m="1,"`"2,"`"3,"` dotslow } {}, where DD size 12{D} {} is the slit width,  size 12{} {} is the lights wavelength,  size 12{} {} is the angle relative to the original direction of the light, and mm size 12{m} {} is the order of the minimum

