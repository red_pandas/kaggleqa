
Thermal Expansion of Solids and Liquids
Thermal Expansion of Solids and Liquids
Define and describe thermal expansion.
Calculate the linear expansion of an object given its initial length, change in temperature, and coefficient of linear expansion.
Calculate the volume expansion of an object given its initial volume, change in temperature, and coefficient of volume expansion.
Calculate thermal stress on an object given its original volume, temperature change, volume change, and bulk modulus.

Thermal expansion joints like these in the Auckland Harbour Bridge in New Zealand allow bridges to change length without buckling. (credit: Ingolfson, Wikimedia Commons)The expansion of alcohol in a thermometer is one of many commonly encountered examples of thermal expansion, the change in size or volume of a given mass with temperature. Hot air rises because its volume increases, which causes the hot airs density to be smaller than the density of surrounding air, causing a buoyant (upward) force on the hot air. The same happens in all liquids and gases, driving natural heat transfer upwards in homes, oceans, and weather systems. Solids also undergo thermal expansion. Railroad tracks and bridges, for example, have expansion joints to allow them to freely expand and contract with temperature changes. 
What are the basic properties of thermal expansion? First, thermal expansion is clearly related to temperature change. The greater the temperature change, the more a bimetallic strip will bend. Second, it depends on the material. In a thermometer, for example, the expansion of alcohol is much greater than the expansion of the glass containing it. 
What is the underlying cause of thermal expansion? As is discussed in Kinetic Theory: Atomic and Molecular Explanation of Pressure and Temperature, an increase in temperature implies an increase in the kinetic energy of the individual atoms. In a solid, unlike in a gas, the atoms or molecules are closely packed together, but their kinetic energy (in the form of small, rapid vibrations) pushes neighboring atoms or molecules apart from each other. This neighbor-to-neighbor pushing results in a slightly greater distance, on average, between neighbors, and adds up to a larger size for the whole body. For most substances under ordinary conditions, there is no preferred direction, and an increase in temperature will increase the solids size by a certain fraction in each dimension. Linear Thermal ExpansionThermal Expansion in One Dimension
The change in length LL size 12{L} {} is proportional to length LL size 12{L} {}. The dependence of thermal expansion on temperature, substance, and length is summarized in the equationL=LT,L=LT, size 12{L=LT} {}where LL size 12{L} {} is the change in length LL size 12{L} {}, TT size 12{T} {} is the change in temperature, and  size 12{} {} is the coefficient of linear expansion, which varies slightly with temperature. 
[link] lists representative values of the coefficient of linear expansion, which may have units of 1/C1/C size 12{1/C} {} or 1/K. Because the size of a kelvin and a degree Celsius are the same, both  size 12{} {} and TT size 12{T} {} can be expressed in units of kelvins or degrees Celsius. The equation L=LT L=LT  size 12{L=LT} {} is accurate for small changes in temperature and can be used for large changes in temperature if an average value of  size 12{} {} is used.Thermal Expansion Coefficients at 20C20C size 12{"20"C} {}1

Material
Coefficient of linear expansion (1/C)(1/C) size 12{  \( 1/C \) } {}
Coefficient of volume expansion (1/C)(1/C) size 12{  \( 1/C \) } {}



Solids



Aluminum





25


10



6











25


10



6







 size 12{"25" times "10" rSup { size 8{6} } } {}







75


10



6











75


10



6







 size 12{"75""10" rSup { size 8{ +- 6} } } {}




Brass





19


10



6











19


10



6







 size 12{"19" times "10" rSup { size 8{6} } } {}







56


10



6











56


10



6







 size 12{"56""10" rSup { size 8{ +- 6} } } {}




Copper





17


10



6











17


10



6







 size 12{"17" times "10" rSup { size 8{6} } } {}







51


10



6











51


10



6







 size 12{"51" times "10" rSup { size 8{6} } } {}




Gold





14


10



6











14


10



6







 size 12{"14" times "10" rSup { size 8{6} } } {}







42


10



6











42


10



6







 size 12{"42" times "10" rSup { size 8{6} } } {}




Iron or Steel





12


10



6











12


10



6







 size 12{"12" times "10" rSup { size 8{6} } } {}







35


10



6











35


10



6







 size 12{"35" times "10" rSup { size 8{6} } } {}




Invar (Nickel-iron alloy)





0
.

9


10



6












0
.

9


10



6








 size 12{0 "." 9 times "10" rSup { size 8{6} } } {}







2
.

7


10



6












2
.

7


10



6








 size 12{2 "." 7 times "10" rSup { size 8{6} } } {}




Lead





29


10



6











29


10



6







 size 12{"29" times "10" rSup { size 8{6} } } {}







87


10



6











87


10



6







 size 12{"87" times "10" rSup { size 8{6} } } {}




Silver





18


10



6











18


10



6







 size 12{"18" times "10" rSup { size 8{6} } } {}







54


10



6











54


10



6







 size 12{"54" times "10" rSup { size 8{6} } } {}




Glass (ordinary)





9


10



6











9


10



6







 size 12{9 times "10" rSup { size 8{6} } } {}







27


10



6











27


10



6







 size 12{"27" times "10" rSup { size 8{6} } } {}




Glass (Pyrex)





3


10



6











3


10



6







 size 12{3 times "10" rSup { size 8{6} } } {}







9


10



6











9


10



6







 size 12{9 times "10" rSup { size 8{6} } } {}




Quartz





0
.
4


10



6











0
.
4


10



6







 size 12{0 "." 4"10" rSup { size 8{ +- 6} } } {}







1


10



6











1


10



6







 size 12{1 times "10" rSup { size 8{6} } } {}




Concrete, Brick





~
12


10



6











~
12


10



6







 size 12{ "~" "12""10" rSup { size 8{ +- 6} } } {}







~

36


10



6












~

36


10



6








 size 12{ "~" "36" times "10" rSup { size 8{6} } } {}




Marble (average)





2
.
5


10



6











2
.
5


10



6







 size 12{2 "." 5"10" rSup { size 8{ +- 6} } } {}







7
.

5


10



6












7
.

5


10



6








 size 12{7 "." 5 times "10" rSup { size 8{6} } } {}





Liquids



Ether






1650


10



6











1650


10



6







 size 12{"1650" times "10" rSup { size 8{6} } } {}




Ethyl alcohol






1100


10



6











1100


10



6







 size 12{"1100" times "10" rSup { size 8{6} } } {}




Petrol






950


10



6











950


10



6







 size 12{"950" times "10" rSup { size 8{6} } } {}




Glycerin






500


10



6











500


10



6







 size 12{"500" times "10" rSup { size 8{6} } } {}




Mercury






180


10



6











180


10



6







 size 12{"180" times "10" rSup { size 8{6} } } {}




Water






210


10



6











210


10



6







 size 12{"210" times "10" rSup { size 8{6} } } {}





Gases



Air and most other gases at atmospheric pressure






3400


10



6











3400


10



6







 size 12{"3400" times "10" rSup { size 8{6} } } {}



Calculating Linear Thermal Expansion: The Golden Gate Bridge
The main span of San Franciscos Golden Gate Bridge is 1275 m long at its coldest. The bridge is exposed to temperatures ranging from 15C15C size 12{"15"C} {} to 40C40C size 12{"40"C} {}. What is its change in length between these temperatures? Assume that the bridge is made entirely of steel.Strategy
Use the equation for linear thermal expansion L=LTL=LT size 12{L=L`T} {} to calculate the change in length , LL size 12{L} {}. Use the coefficient of linear expansion,  size 12{} {}, for steel from [link], and note that the change in temperature, TT size 12{T} {}, is 55C55C size 12{"55"C} {}.
Solution

Plug all of the known values into the equation to solve for LL size 12{L} {}. L=LT=12106C1275 m55C=0.84 m.L=LT=12106C1275 m55C=0.84 m. size 12{L=LT= left ( {  {"12" times "10" rSup { size 8{ - 6} } }  over  {C} }  right ) left ("1275 m" right ) left ("55"C right )=0 "." "84 m"} {}
Discussion

Although not large compared with the length of the bridge, this change in length is observable. It is generally spread over many expansion joints so that the expansion at each joint is small.

Thermal Expansion in Two and Three DimensionsObjects expand in all dimensions, as illustrated in [link]. That is, their areas and volumes, as well as their lengths, increase with temperature. Holes also get larger with temperature. If you cut a hole in a metal plate, the remaining material will expand exactly as it would if the plug was still in place. The plug would get bigger, and so the hole must get bigger too. (Think of the ring of neighboring atoms or molecules on the wall of the hole as pushing each other farther apart as temperature increases. Obviously, the ring of neighbors must get slightly larger, so the hole gets slightly larger). Thermal Expansion in Two DimensionsFor small temperature changes, the change in area AA size 12{A} {} is given byA=2AT,A=2AT, size 12{A=2AT} {}where AA size 12{A} {} is the change in area AA size 12{A} {}, TT size 12{T} {} is the change in temperature, and  size 12{} {} is the coefficient of linear expansion, which varies slightly with temperature. In general, objects expand in all directions as temperature increases. In these drawings, the original boundaries of the objects are shown with solid lines, and the expanded boundaries with dashed lines. (a) Area increases because both length and width increase. The area of a circular plug also increases. (b) If the plug is removed, the hole it leaves becomes larger with increasing temperature, just as if the expanding plug were still in place. (c) Volume also increases, because all three dimensions increase.


Thermal Expansion in Three Dimensions
The change in volume VV size 12{V} {} is very nearly V=3VTV=3VT size 12{V=3VT} {}. This equation is usually written as V=VT,V=VT, size 12{V=VT} {}where  size 12{} {} is the coefficient of volume expansion and 33 size 12{ approx 3} {}. Note that the values of  size 12{} {} in [link] are almost exactly equal to 33 size 12{3} {}.
In general, objects will expand with increasing temperature. Water is the most important exception to this rule. Water expands with increasing temperature (its density decreases) when it is at temperatures greater than 4C (40F)4C (40F) size 12{4"C " \( "40"F \) } {}. However, it expands with decreasing temperature when it is between +4C+4C size 12{+4C} {} and 0C0C size 12{0C} {}(40F(40F size 12{ \( "40"F} {} to 32F)32F) size 12{"32"F \) } {}. Water is densest at +4C+4C size 12{+4C} {}. (See [link].) Perhaps the most striking effect of this phenomenon is the freezing of water in a pond. When water near the surface cools down to 4C4C size 12{4C} {} it is denser than the remaining water and thus will sink to the bottom. This turnover results in a layer of warmer water near the surface, which is then cooled. Eventually the pond has a uniform temperature of 4C4C size 12{4C} {}. If the temperature in the surface layer drops below 4C4C size 12{4C} {}, the water is less dense than the water below, and thus stays near the top. As a result, the pond surface can completely freeze over. The ice on top of liquid water provides an insulating layer from winters harsh exterior air temperatures. Fish and other aquatic life can survive in 4C4C size 12{4C} {} water beneath ice, due to this unusual characteristic of water. It also produces circulation of water in the pond that is necessary for a healthy ecosystem of the body of water. 
The density of water as a function of temperature. Note that the thermal expansion is actually very small. The maximum density at +4C+4C size 12{+4C} {} is only 0.0075% greater than the density at 2C2C size 12{2C} {}, and 0.012% greater than that at 0C0C size 12{0C} {}.



Making Connections: Real-World ConnectionsFilling the Tank
Differences in the thermal expansion of materials can lead to interesting effects at the gas station. One example is the dripping of gasoline from a freshly filled tank on a hot day. Gasoline starts out at the temperature of the ground under the gas station, which is cooler than the air temperature above. The gasoline cools the steel tank when it is filled. Both gasoline and steel tank expand as they warm to air temperature, but gasoline expands much more than steel, and so it may overflow. 
This difference in expansion can also cause problems when interpreting the gasoline gauge. The actual amount (mass) of gasoline left in the tank when the gauge hits empty is a lot less in the summer than in the winter. The gasoline has the same volume as it does in the winter when the add fuel light goes on, but because the gasoline has expanded, there is less mass. If you are used to getting another 40 miles on empty in the winter, bewareyou will probably run out much more quickly in the summer.
Because the gas expands more than the gas tank with increasing temperature, you cant drive as many miles on empty in the summer as you can in the winter. (credit: Hector Alejandro, Flickr)


Calculating Thermal Expansion: Gas vs. Gas TankSuppose your 60.0-L (15.9-gal) steel gasoline tank is full of gas, so both the tank and the gasoline have a temperature of 15.0C15.0C size 12{"15" "." 0C} {}. How much gasoline has spilled by the time they warm to 35.0C35.0C size 12{"35" "." 0C} {}?
Strategy

The tank and gasoline increase in volume, but the gasoline increases more, so the amount spilled is the difference in their volume changes. (The gasoline tank can be treated as solid steel.) We can use the equation for volume expansion to calculate the change in volume of the gasoline and of the tank.

Solution

1. Use the equation for volume expansion to calculate the increase in volume of the steel tank:
Vs=sVsT.Vs=sVsT. size 12{V rSub { size 8{s} } = rSub { size 8{s} } V rSub { size 8{s} } T} {}2. The increase in volume of the gasoline is given by this equation:
Vgas=gasVgasT.Vgas=gasVgasT. size 12{V rSub { size 8{"gas"} } = rSub { size 8{"gas"} } V rSub { size 8{"gas"} } T} {}3. Find the difference in volume to determine the amount spilled as
Vspill=VgasVs.Vspill=VgasVs. size 12{V rSub { size 8{"spill"} } =V rSub { size 8{"gas"} }  - V rSub { size 8{s} } } {}Alternatively, we can combine these three equations into a single equation. (Note that the original volumes are equal.) 

Vspill
=
gassVT


=







950

35





10



6



/


C


60.0L20.0C


=
1.10L.

Vspill
=
gassVT


=







950

35





10



6



/


C


60.0L20.0C


=
1.10L.

Discussion

This amount is significant, particularly for a 60.0-L tank. The effect is so striking because the gasoline and steel expand quickly. The rate of change in thermal properties is discussed in Heat and Heat Transfer Methods.If you try to cap the tank tightly to prevent overflow, you will find that it leaks anyway, either around the cap or by bursting the tank. Tightly constricting the expanding gas is equivalent to compressing it, and both liquids and solids resist being compressed with extremely large forces. To avoid rupturing rigid containers, these containers have air gaps, which allow them to expand and contract without stressing them.

Thermal Stress
Thermal stress is created by thermal expansion or contraction (see Elasticity: Stress and Strain for a discussion of stress and strain). Thermal stress can be destructive, such as when expanding gasoline ruptures a tank. It can also be useful, for example, when two parts are joined together by heating one in manufacturing, then slipping it over the other and allowing the combination to cool. Thermal stress can explain many phenomena, such as the weathering of rocks and pavement by the expansion of ice when it freezes. Calculating Thermal Stress: Gas PressureWhat pressure would be created in the gasoline tank considered in [link], if the gasoline increases in temperature from 15.0C15.0C size 12{"15" "." 0C} {} to 35.0C35.0C size 12{"35" "." 0C} {} without being allowed to expand?  Assume that the bulk modulus BB size 12{B} {} for gasoline is 1.00109 N/m21.00109 N/m2 size 12{1 "." "00" times "10" rSup { size 8{9} } " N/m" rSup { size 8{2} } } {}. (For more on bulk modulus, see Elasticity: Stress and Strain.)
Strategy

To solve this problem, we must use the following equation, which relates a change in volume VV size 12{DV} {} to pressure:




V
=

1
B



F
A


V

0


,









V
=

1
B



F
A


V

0


,




 size 12{V= {  {1}  over  {B} }  {  {F}  over  {A} } V rSub { size 8{0} } ,} {}

where F/AF/A size 12{F/A} {} is pressure, V0V0 size 12{V rSub { size 8{0} } } {} is the original volume, and BB size 12{B} {} is the bulk modulus of the material involved. We will use the amount spilled in [link] as the change in volume, VV size 12{V} {}. 
Solution

1. Rearrange the equation for calculating pressure:






P
=

F
A


=

V

V

0




B
.










P
=

F
A


=

V

V

0




B
.




 size 12{P= {  {F}  over  {A} } = {  {V}  over  {V rSub { size 8{0} } } } B "." } {}

2. Insert the known values. The bulk modulus for gasoline is B=1.00109 N/m2B=1.00109 N/m2 size 12{B=1 "." "00""10" rSup { size 8{9} } " N/m" rSup { size 8{2} } } {}. In the previous example, the change in volume V=1.10 LV=1.10 L size 12{DV=1 "." "10"" L"} {} is the amount that would spill. Here, V0=60.0 LV0=60.0 L size 12{V rSub { size 8{0} } ="60" "." 0" L"} {} is the original volume of the gasoline. Substituting these values into the equation, we obtain




P
=


1
.
10 L


60
.
0 L






1
.

00


10

9



Pa


=
1

.

83


10

7



Pa
.









P
=


1
.
10 L


60
.
0 L






1
.

00


10

9



Pa


=
1

.

83


10

7



Pa
.




 size 12{P= {  {1 "." "10 L"}  over  {"60" "." "0 L"} }  left (1 "." "00" times "10" rSup { size 8{9} } " Pa" right )=1 "." "83" times "10" rSup { size 8{7} } " Pa" "." } {}


Discussion

This pressure is about 2500lb/in22500lb/in2 size 12{"2500"" lb/in" rSup { size 8{2} } } {}, much more than a gasoline tank can handle.Forces and pressures created by thermal stress are typically as great as that in the example above. Railroad tracks and roadways can buckle on hot days if they lack sufficient expansion joints. (See [link].) Power lines sag more in the summer than in the winter, and will snap in cold weather if there is insufficient slack. Cracks open and close in plaster walls as a house warms and cools. Glass cooking pans will crack if cooled rapidly or unevenly, because of differential contraction and the stresses it creates. (Pyrex is less susceptible because of its small coefficient of thermal expansion.) Nuclear reactor pressure vessels are threatened by overly rapid cooling, and although none have failed, several have been cooled faster than considered desirable. Biological cells are ruptured when foods are frozen, detracting from their taste. Repeated thawing and freezing accentuate the damage. Even the oceans can be affected. A significant portion of the rise in sea level that is resulting from global warming is due to the thermal expansion of sea water.Thermal stress contributes to the formation of potholes. (credit: Editor5807, Wikimedia Commons)


Metal is regularly used in the human body for hip and knee implants. Most implants need to be replaced over time because, among other things, metal does not bond with bone. Researchers are trying to find better metal coatings that would allow metal-to-bone bonding. One challenge is to find a coating that has an expansion coefficient similar to that of metal. If the expansion coefficients are too different, the thermal stresses during the manufacturing process lead to cracks at the coating-metal interface.
Another example of thermal stress is found in the mouth. Dental fillings can expand differently from tooth enamel. It can give pain when eating ice cream or having a hot drink. Cracks might occur in the filling. Metal fillings (gold, silver, etc.) are being replaced by composite fillings (porcelain), which have smaller coefficients of expansion, and are closer to those of teeth.
Check Your Understanding

Two blocks, A and B, are made of the same material. Block A has dimensions lwh=L2LLlwh=L2LL size 12{l times w times h=L times 2L times L} {} and Block B has dimensions 2L2L2L2L2L2L size 12{2L times 2L times 2L} {}. If the temperature changes, what is (a) the change in the volume of the two blocks, (b) the change in the cross-sectional area lwlw size 12{l times w} {}, and (c) the change in the height hh size 12{h} {} of the two blocks?




(a) The change in volume is proportional to the original volume. Block A has a volume of L2LL=2L3.L2LL=2L3. size 12{L2LL=2L rSup { size 8{3} }  "." } {}.  Block B has a volume of 2L2L2L=8L3,2L2L2L=8L3, size 12{2L2L2L=8L rSup { size 8{3} } ,} {} which is 4 times that of Block A. Thus the change in volume of Block B should be 4 times the change in volume of Block A. (b) The change in area is proportional to the area. The cross-sectional area of Block A is L2L=2L2,L2L=2L2, size 12{L2L=2L rSup { size 8{2} } ,} {} while that of Block B is 2L2L=4L2.2L2L=4L2. size 12{2L2L=4L rSup { size 8{2} }  "." } {} Because cross-sectional area of Block B is twice that of Block A, the change in the cross-sectional area of Block B is twice that of Block A. (c) The change in height is proportional to the original height. Because the original height of Block B is twice that of A, the change in the height of Block B is twice that of Block A. 


Section SummaryThermal expansion is the increase, or decrease, of the size (length, area, or volume) of a body due to a change in temperature.
Thermal expansion is large for gases, and relatively small, but not negligible, for liquids and solids. 
Linear thermal expansion is
      L=LT,L=LT, size 12{L=LT} {}
where LL size 12{L} {} is the change in length LL size 12{L} {}, TT size 12{T} {} is the change in temperature, and  size 12{} {} is the coefficient of linear expansion, which varies slightly with temperature.
      
The change in area due to thermal expansion is
      A=2AT,A=2AT, size 12{A=2AT} {}
where AA size 12{A} {} is the change in area.
      
The change in volume due to thermal expansion is
      V=VT,V=VT, size 12{V=VT} {}
where  size 12{} {} is the coefficient of volume expansion and 33 size 12{ approx 3} {}. Thermal stress is created when thermal expansion is constrained.
      
Conceptual Questions

Thermal stresses caused by uneven cooling can easily break glass cookware. Explain why Pyrex, a glass with a small coefficient of linear expansion, is less susceptible.


Water expands significantly when it freezes: a volume increase of about 9% occurs. As a result of this expansion and because of the formation and growth of crystals as water freezes, anywhere from 10% to 30% of biological cells are burst when animal or plant material is frozen. Discuss the implications of this cell damage for the prospect of preserving human bodies by freezing so that they can be thawed at some future date when it is hoped that all diseases are curable.


One method of getting a tight fit, say of a metal peg in a hole in a metal block, is to manufacture the peg slightly larger than the hole. The peg is then inserted when at a different temperature than the block. Should the block be hotter or colder than the peg during insertion? Explain your answer.


Does it really help to run hot water over a tight metal lid on a glass jar before trying to open it? Explain your answer.


Liquids and solids expand with increasing temperature, because the kinetic energy of a bodys atoms and molecules increases. Explain why some materials shrink with increasing temperature. 

Problems & Exercises The height of the Washington Monument is measured to be 170 m on a day when the temperature is 35.0C35.0C size 12{"35" "." 0C} {}. What will its height be on a day when the temperature falls to 10.0C10.0C size 12{"10" "." 0C} {}? Although the monument is made of limestone, assume that its thermal coefficient of expansion is the same as marbles. 


169.98 m 


 How much taller does the Eiffel Tower become at the end of a day when the temperature has increased by 15C15C size 12{"15"C} {}? Its original height is 321 m and you can assume it is made of steel.

 What is the change in length of a 3.00-cm-long column of mercury if its temperature changes from 37.0C37.0C size 12{"37" "." 0C} {} to 40.0C40.0C size 12{"40" "." 0C} {}, assuming the mercury is unconstrained?






5
.

4


10



6




 m








5
.

4


10



6




 m




 size 12{5 "." 4 times "10" rSup { size 8{ - 6} } " m"} {}



 How large an expansion gap should be left between steel railroad rails if they may reach a maximum temperature 35.0C35.0C size 12{"35" "." 0C} {} greater than when they were laid? Their original length is 10.0 m.

 You are looking to purchase a small piece of land in Hong Kong. The price is only $60,000 per square meter! The land title says the dimensions are 20 m 30 m.20 m 30 m. size 12{"20"" m "` times "30 m" "." } {} By how much would the total price change if you measured the parcel with a steel tape measure on a day when the temperature was 20C20C size 12{"20"C} {} above normal?

Because the area gets smaller, the price of the land DECREASES by ~$17,000.~$17,000. size 12{ "~" $"17","000" "." } {}


 Global warming will produce rising sea levels partly due to melting ice caps but also due to the expansion of water as average ocean temperatures rise. To get some idea of the size of this effect, calculate the change in length of a column of water 1.00 km high for a temperature increase of 1.00C.1.00C. size 12{1 "." "00"C "." } {} Note that this calculation is only approximate because ocean warming is not uniform with depth.

 Show that 60.0 L of gasoline originally at 15.0C15.0C size 12{"15" "." 0C} {} will expand to 61.1 L when it warms to 35.0C,35.0C, size 12{"35" "." 0"C,"} {} as claimed in [link].







V


=




V

0


+
V

=

V

0


(

1
+
T

)




=



(
60
.
00 L
)



1
+




950


10



6




/


C





35
.
0


C  

15

.
0

C










=



61
.
1

L














V


=




V

0


+
V

=

V

0


(

1
+
T

)




=



(
60
.
00 L
)



1
+




950


10



6




/


C





35
.
0


C  

15

.
0

C










=



61
.
1

L












(a) Suppose a meter stick made of steel and one made of invar (an alloy of iron and nickel) are the same length at 0C0C size 12{0C} {}. What is their difference in length at 22.0C22.0C size 12{"22" "." 0C} {}? (b) Repeat the calculation for two 30.0-m-long surveyors tapes. 


(a) If a 500-mL glass beaker is filled to the brim with ethyl alcohol at a temperature of 5.00C,5.00C, size 12{5 "." "00""C,"} {} how much will overflow when its temperature reaches 22.0C22.0C size 12{"22" "." 0C} {}? (b) How much less water would overflow under the same conditions?


(a) 9.35 mL(b) 7.56 mL


 Most automobiles have a coolant reservoir to catch radiator fluid that may overflow when the engine is hot. A radiator is made of copper and is filled to its 16.0-L capacity when at 10.0C.10.0C. size 12{"10" "." 0C "." } {} What volume of radiator fluid will overflow when the radiator and fluid reach their 95.0C95.0C size 12{"95" "." 0C} {} operating temperature, given that the fluids volume coefficient of expansion is =400106/C=400106/C size 12{="400""10" rSup { size 8{ +- 6} } /C} {}? Note that this coefficient is approximate, because most car radiators have operating temperatures of greater than 95.0C.95.0C. size 12{"95" "." 0C "." } {}
 A physicist makes a cup of instant coffee and notices that, as the coffee cools, its level drops 3.00 mm in the glass cup. Show that this decrease cannot be due to thermal contraction by calculating the decrease in level if the 350 cm3350 cm3 size 12{"350"" cm" rSup { size 8{3} } } {} of coffee is in a 7.00-cm-diameter cup and decreases in temperature from 95.0C95.0C size 12{"95" "." 0C} {}to45.0C.45.0C. size 12{"45" "." 0C "." } {} (Most of the drop in level is actually due to escaping bubbles of air.)

0.832 mm


 (a) The density of water at 0C0C size 12{0C} {} is very nearly 1000 kg/m31000 kg/m3 size 12{"1000"" kg/m" rSup { size 8{3} } } {} (it is actually 999.84 kg/m3999.84 kg/m3 size 12{9"99" "." "84 kg/m" rSup { size 8{3} } } {}), whereas the density of ice at 0C0C size 12{0C} {} is 917 kg/m3917 kg/m3 size 12{9"17 kg/m" rSup { size 8{3} } } {}. Calculate the pressure necessary to keep ice from expanding when it freezes, neglecting the effect such a large pressure would have on the freezing temperature. (This problem gives you only an indication of how large the forces associated with freezing water might be.) (b) What are the implications of this result for biological cells that are frozen?
 Show that 3,3, size 12{3,} {} by calculating the change in volume VV size 12{V} {} of a cube with sides of length L.L. size 12{L "." } {}

We know how the length changes with temperature: L=L0TL=L0T size 12{L=L rSub { size 8{0} } T} {}. Also we know that the volume of a cube is related to its length by V=L3V=L3 size 12{V=L rSup { size 8{3} } } {}, so the final volume is then V=V0+V=L0+L3V=V0+V=L0+L3 size 12{V=V rSub { size 8{0} } +V= left (L rSub { size 8{0} } +L right ) rSup { size 8{3} } } {}. Substituting for LL size 12{DL} {} gives





V
=





L

0


+

L

0



T



3



=

L
0
3





1
+
T



3


.










V
=





L

0


+

L

0



T



3



=

L
0
3





1
+
T



3


.




 size 12{ V= left (L rSub { size 8{0} } +L rSub { size 8{0} } T right ) rSup { size 8{3} } =L rSub { size 8{0}   rSup { size 8{3} } }  left (1+T right ) rSup { size 8{3} }  "." } {}

Now, because TT size 12{T} {} is small, we can use the binomial expansion:VL031+3T=

L
0
3

+3L03T.VL031+3T=

L
0
3

+3L03T. size 12{V approx L rSub { size 8{0} }  rSup { size 8{3} }  left (1+3T right )=L rSub { size 8{0}   rSup { size 8{3} } } +3L rSub { size 8{0} }  rSup { size 8{3} } T} {}So writing the length terms in terms of volumes gives V=V0+VV0+3V0T,V=V0+VV0+3V0T, size 12{V=V rSub { size 8{0} } +V approx V rSub { size 8{0} } +3V rSub { size 8{0} } T,} {} and soV=V0T3V0T,or3.V=V0T3V0T,or3. size 12{V=V rSub { size 8{0} } T approx 3V rSub { size 8{0} } T,`"or"` approx 3} {}
Footnotes1  Values for liquids and gases are approximate.Glossary
thermal expansion the change in size or volume of an object with change in temperature
coefficient of linear expansion  size 12{} {}, the change in length, per unit length, per 1C1C size 12{1C} {} change in temperature; a constant used in the calculation of linear expansion; the coefficient of linear expansion depends on the material and to some degree on the temperature of the material
coefficient of volume expansion  size 12{} {}, the change in volume, per unit volume, per 1C1C size 12{1C} {} change in temperature
thermal stress stress caused by thermal expansion or contraction

