
Electric Field: Concept of a Field Revisited
Electric Field: Concept of a Field Revisited
Describe a force field and calculate the strength of an electric field due to a point charge.
Calculate the force exerted on a test charge by an electric field.
Explain the relationship between electrical force (F) on a test charge and electrical field strength (E).

Contact forces, such as between a baseball and a bat, are explained on the small scale by the interaction of the charges in atoms and molecules in close proximity. They interact through forces that include the Coulomb force. Action at a distance is a force between objects that are not close enough for their atoms to touch. That is, they are separated by more than a few atomic diameters.
For example, a charged rubber comb attracts neutral bits of paper from a distance via the Coulomb force. It is very useful to think of an object being surrounded in space by a force field. The force field carries the force to another object (called a test object) some distance away.
Concept of a Field
A field is a way of conceptualizing and mapping the force that surrounds any object and acts on another object at a distance without apparent physical connection. For example, the gravitational field surrounding the earth (and all other masses) represents the gravitational force that would be experienced if another mass were placed at a given point within the field.
In the same way, the Coulomb force field surrounding any charge extends throughout space. Using Coulombs law, F=k|q1q2|/r2F=k|q1q2|/r2 size 12{F= { ital "kq" rSub { size 8{1} } q rSub { size 8{2} } } slash {r rSup { size 8{2} } } } {}, its magnitude is given by the equation 

F=k|qQ|/r2F=k|qQ|/r2 size 12{F= { ital "kqQ"} slash {r rSup { size 8{2} } } } {}, for a point charge (a particle having a charge QQ size 12{Q} {}) acting on a test charge qq size 12{q} {} at a distance rr size 12{r} {} (see [link]). Both the magnitude and direction of the Coulomb force field depend on QQ size 12{Q} {} and the test charge qq size 12{q} {}.The Coulomb force field due to a positive charge QQ size 12{Q} {} is shown acting on two different charges. Both charges are the same distance from QQ size 12{Q} {}. (a) Since q1q1 size 12{q rSub { size 8{1} } } {} is positive, the force F1F1 size 12{F rSub { size 8{1} } } {} acting on it is repulsive. (b) The charge q2q2 size 12{q rSub { size 8{2} } } {} is negative and greater in magnitude than q1q1 size 12{q rSub { size 8{1} } } {}, and so the force F2F2 size 12{F rSub { size 8{2} } } {} acting on it is attractive and stronger than F1F1 size 12{F rSub { size 8{1} } } {}. The Coulomb force field is thus not unique at any point in space, because it depends on the test charges q1q1 size 12{q rSub { size 8{1} } } {} and q2q2 size 12{q rSub { size 8{2} } } {} as well as the charge QQ size 12{Q} {}.


To simplify things, we would prefer to have a field that depends only on QQ size 12{Q} {} and not on the test charge qq size 12{q} {}. The electric field is defined in such a manner that it represents only the charge creating it and is unique at every point in space. Specifically, the electric field EE size 12{E} {} is defined to be the ratio of the Coulomb force to the test charge:




E
=

F

q




,





E
=

F

q




,

 size 12{E= {  {F}  over  {q,} } } {}

where FF size 12{F} {} is the electrostatic force (or Coulomb force) exerted on a positive test charge 
qq size 12{q} {}. It is understood that 
EE size 12{E} {} is in the same direction as 
FF size 12{F} {}. It is also assumed that qq size 12{q} {} is so small that it does not alter the charge distribution creating the electric field. The units of electric field are newtons per coulomb (N/C). If the electric field is known, then the electrostatic force on any charge qq size 12{q} {} is simply obtained by multiplying charge times electric field, or 



F
=
q
E







F
=
q
E



 size 12{F=qE} {}
. Consider the electric field due to a point charge QQ size 12{Q} {}. According to Coulombs law, the force it exerts on a test charge 

qq size 12{q} {} is 

F=k|qQ|/r2F=k|qQ|/r2 size 12{F= { ital "kqQ"} slash {r rSup { size 8{2} } } } {}. Thus the magnitude of the electric field, 
EE size 12{E} {}, for a point charge is





E
=|

F
q
|

=
k
|




qQ




qr

2



|
=
k


|Q|

r

2





.







E
=|

F
q
|

=
k
|




qQ




qr

2



|
=
k


|Q|

r

2





.

 size 12{E= {  {F}  over  {q} } =k {  { ital "qQ"}  over  { ital "qr" rSup { size 8{2} } } } =k {  {Q}  over  {r rSup { size 8{2} } } } } {}

Since the test charge cancels, we see that





E
=
k


|Q|

r

2






.






E
=
k


|Q|

r

2






.

 size 12{E=k {  {Q}  over  {r rSup { size 8{2} } } } } {}

The electric field is thus seen to depend only on the charge QQ size 12{Q} {} and the distance rr size 12{r} {}; it is completely independent of the test charge qq size 12{q} {}.
Calculating the Electric Field of a Point Charge
Calculate the strength and direction of the electric field EE size 12{E} {} due to a point charge of 2.00 nC (nano-Coulombs) at a distance of 5.00 mm from the charge.

Strategy

We can find the electric field created by a point charge by using the equation E=kQ/r2E=kQ/r2 size 12{E= { ital "kQ"} slash {r rSup { size 8{2} } } } {}.

Solution

Here Q=2.00109Q=2.00109 size 12{Q=2 "." "00" times "10" rSup { size 8{ - 9} } } {} C and r=5.00103r=5.00103 size 12{r=5 "." "00" times "10" rSup { size 8{ - 3} } } {} m. Entering those values into the above equation gives




 E
 =





k


Q

r

2








 =


(

8.99


10

9




N


m

2




/C

2



)



(

2.00


10



9




C
)


(

5.00


10



3




m

)

2








 =



7.19


10

5



N/C.









 E
 =





k


Q

r

2








 =


(

8.99


10

9




N


m

2




/C

2



)



(

2.00


10



9




C
)


(

5.00


10



3




m

)

2








 =



7.19


10

5



N/C.





alignl { stack {
 size 12{E=k {  {Q}  over  {r rSup { size 8{2} } } } }  {} # 
= \( 9 "." "00" times "10" rSup { size 8{9} } N cdot m rSup { size 8{2} } "/C" rSup { size 8{2} }  \)  times  {  { \( 2 "." "00" times "10" rSup { size 8{ - 9} } C \) }  over  { \( 5 "." "00" times "10" rSup { size 8{ - 3} } m \)  rSup { size 8{2} } } }  {} # 
=7 "." "20" times "10" rSup { size 8{5} } "N/C" {} 
} } {}


Discussion

This electric field strength is the same at any point 5.00 mm away from the charge QQ size 12{Q} {} that creates the field. It is positive, meaning that it has a direction pointing away from the charge QQ size 12{Q} {}.

Calculating the Force Exerted on a Point Charge by an Electric FieldWhat force does the electric field found in the previous example exert on a point charge of 0.250C0.250C?
Strategy

Since we know the electric field strength and the charge in the field, the force on that charge can be calculated using the definition of electric field E=F/qE=F/q size 12{E= {F} slash {q} } {} rearranged to F=qEF=qE size 12{F= ital "qE"} {}.
Solution

The magnitude of the force on a charge q=0.250Cq=0.250C size 12{q= - 0 "." "250""C"} {} exerted by a field of strength E=7.20105E=7.20105 size 12{E=7 "." "20" times "10" rSup { size 8{5} } } {} N/C is thus,



 F
 =






qE






 =





(

0.250



10

6



C
)
(
7.20



10

5



N/C
)





 =


0.180 N.









 F
 =






qE






 =





(

0.250



10

6



C
)
(
7.20



10

5



N/C
)





 =


0.180 N.





alignl { stack {
 size 12{F= ital "qE"}  {} # 
 size 12{ {}= \( "-0" "." "250" times "10" rSup { size 8{"-6"} } `C \)  \( 7 "." "20" times "10" rSup { size 8{5} } `"N/C" \) }  {} # 
="-0" "." "180"`N {} 
} } {}

Because  qq is negative, the force is directed opposite to the direction of the field.
Discussion

The force is attractive, as expected for unlike charges. (The field was created by a positive charge and here acts on a negative charge.) The charges in this example are typical of common static electricity, and the modest attractive force obtained is similar to forces experienced in static cling and similar situations.
PhET Explorations: Electric Field of Dreams
Play ball! Add charges to the Field of Dreams and see how they react to the electric field. Turn on a background electric field and adjust the direction and magnitude.Electric Field of Dreams



Section SummaryThe electrostatic force field surrounding a charged object extends out into space in all directions.
The electrostatic force exerted by a point charge on a test charge at a distance rr size 12{r} {} depends on the charge of both charges, as well as the distance between the two.
The electric field EE size 12{E} {} is defined to be
    



E
=

F

q
,









E
=

F

q
,





 size 12{E= {  {F}  over  {q,} } } {}

where FF size 12{F} {} is the Coulomb or electrostatic force exerted on a small positive test charge qq size 12{q} {}. EE size 12{E} {} has units of N/C.
The magnitude of the electric field EE size 12{E} {} created by a point charge QQ size 12{Q} {} is
    




E
=
k


|Q|

r

2






.






E
=
k


|Q|

r

2






.

 size 12{E=k {  {Q}  over  {r rSup { size 8{2} } } } } {}

where rr size 12{r} {} is the distance from QQ size 12{Q} {}. The electric field EE size 12{E} {} is a vector and fields due to multiple charges add like vectors.
Conceptual Questions
Why must the test charge qq size 12{q} {} in the definition of the electric field be vanishingly small?


Are the direction and magnitude of the Coulomb force unique at a given point in space? What about the electric field?


Problem Exercises What is the magnitude and direction of an electric field that exerts a 2.0010-5N2.0010-5N size 12{2 "." "00" times "10" rSup { size 8{5} } N} {} upward force on a 1.75C1.75C charge?
 What is the magnitude and direction of the force exerted on a 3.50C3.50C charge by a 250 N/C electric field that points due east?

8.751048.75104 size 12{8 "." "75" times "10" rSup { size 8{ - 4} } } {} N


 Calculate the magnitude of the electric field 2.00 m from a point charge of 5.00 mC (such as found on the terminal of a Van de Graaff).

 (a) What magnitude point charge creates a 10,000 N/C electric field at a distance of 0.250 m? (b) How large is the field at 10.0 m?


(a) 6.94108C6.94108C size 12{ {underline  {6 "." "94" times "10" rSup { size 8{ - 8} } " C"}} } {}(b) 6.25 N/C6.25 N/C size 12{ {underline  {6 "." "25"" N/C"}} } {}

 Calculate the initial (from rest) acceleration of a proton in a 5.00106N/C5.00106N/C size 12{5 "." "00" times "10" rSup { size 8{6} } "N/C"} {} electric field (such as created by a research Van de Graaff). Explicitly show how you follow the steps in the Problem-Solving Strategy for electrostatics.
 (a) Find the direction and magnitude of an electric field that exerts a 4.801017N4.801017N size 12{4 "." "80" times "10" rSup { size 8{ - "17"} } N} {} westward force on an electron. (b) What magnitude and direction force does this field exert on a proton?

(a) 300 N/C (east)300 N/C (east) size 12{ {underline  {"300"" N/C " \( "eas"}}  {underline  {t \) }} } {}(b) 4.801017 N (east)4.801017 N (east) size 12{ {underline  {4 "." "80" times "10" rSup { size 8{ - "17"} } " N " \( "east" \) }} } {}


Glossary
field a map of the amount and direction of a force acting on other objects, extending out into space
point charge A charged particle, designated QQ size 12{Q} {}, generating an electric field
test charge A particle (designated qq size 12{q} {}) with either a positive or negative charge set down within an electric field generated by a point charge

