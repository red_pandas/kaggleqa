
Bernoullis Equation
Bernoullis Equation
Explain the terms in Bernoullis equation.
Explain how Bernoullis equation is related to conservation of energy.
Explain how to derive Bernoullis principle from Bernoullis equation.
Calculate with Bernoullis principle.
List some applications of Bernoullis principle.

When a fluid flows into a narrower channel, its speed increases. That means its kinetic energy also increases. Where does that change in kinetic energy come from? The increased kinetic energy comes from the net work done on the fluid to push it into the channel and the work done on the fluid by the gravitational force, if the fluid changes vertical position. Recall the work-energy theorem,
Wnet=12mv212
mv02.Wnet=12mv212
mv02.There is a pressure difference when the channel narrows. This pressure difference results in a net force on the fluid: recall that pressure times area equals force. The net work done increases the fluids kinetic energy. As a result, the pressure will drop in a rapidly-moving fluid, whether or not the fluid is confined to a tube.
There are a number of common examples of pressure dropping in rapidly-moving fluids. Shower curtains have a disagreeable habit of bulging into the shower stall when the shower is on. The high-velocity stream of water and air creates a region of lower pressure inside the shower, and standard atmospheric pressure on the other side. The pressure difference results in a net force inward pushing the curtain in. You may also have noticed that when passing a truck on the highway, your car tends to veer toward it. The reason is the samethe high velocity of the air between the car and the truck creates a region of lower pressure, and the vehicles are pushed together by greater pressure on the outside. (See [link].) This effect was observed as far back as the mid-1800s, when it was found that trains passing in opposite directions tipped precariously toward one another.
An overhead view of a car passing a truck on a highway. Air passing between the vehicles flows in a narrower channel and must increase its speed (v2v2 size 12{v rSub { size 8{2} } } {} is greater than v1v1 size 12{v rSub { size 8{1} } } {}), causing the pressure between them to drop (PiPi size 12{P rSub { size 8{i} } } {} is less than PoPo size 12{P rSub { size 8{o} } } {}). Greater pressure on the outside pushes the car and truck together.


Making Connections: Take-Home Investigation with a Sheet of Paper
Hold the short edge of a sheet of paper parallel to your mouth with one hand on each side of your mouth. The page should slant downward over your hands. Blow over the top of the page. Describe what happens and explain the reason for this behavior.
Bernoullis Equation
The relationship between pressure and velocity in fluids is described quantitatively by Bernoullis equation, named after its discoverer, the Swiss scientist Daniel Bernoulli (17001782). Bernoullis equation states that for an incompressible, frictionless fluid, the following sum is constant:
P+12v2+gh=constant,P+12v2+gh=constant, size 12{P+ {  {1}  over  {2} } v rSup { size 8{2} } + ital "gh"="constant,"} {}where PP size 12{P} {} is the absolute pressure,  size 12{} {} is the fluid density, vv size 12{v} {} is the velocity of the fluid, hh size 12{h} {} is the height above some reference point, and gg size 12{g} {} is the acceleration due to gravity. If we follow a small volume of fluid along its path, various quantities in the sum may change, but the total remains constant. Let the subscripts 1 and 2 refer to any two points along the path that the bit of fluid follows; Bernoullis equation becomes






P

1


+

1
2



v

1


2


+





gh

1




=


P

2


+

1
2




v

2


2



+





gh

2




.










P

1


+

1
2



v

1


2


+





gh

1




=


P

2


+

1
2




v

2


2



+





gh

2




.




 size 12{P rSub { size 8{1} } + {  {1}  over  {2} } v rSub { size 8{1} } "" lSup { size 8{2} } + ital "gh" rSub { size 8{1} } =P rSub { size 8{2} } + {  {1}  over  {2} } v rSub { size 8{2} } "" lSup { size 8{2} } + ital "gh" rSub { size 8{2} }  "." } {}

Bernoullis equation is a form of the conservation of energy principle. Note that the second and third terms are the kinetic and potential energy with mm size 12{m} {} replaced by  size 12{} {}. In fact, each term in the equation has units of energy per unit volume. We can prove this for the second term by substituting =m/V=m/V size 12{=m/V} {}  into it and gathering terms:12v2=12mv2V=KEV.12v2=12mv2V=KEV. size 12{ {  {1}  over  {2} } v rSup { size 8{2} } = {  { {  {1}  over  {2} }  ital "mv" rSup { size 8{2} } }  over  {V} } = {  {"KE"}  over  {V} } "."} {}So 12v212v2 size 12{ {  { size 8{1} }  over  { size 8{2} } } v rSup { size 8{2} } } {} is the kinetic energy per unit volume. Making the same substitution into the third term in the equation, we find
gh=mghV=PEgV,gh=mghV=PEgV, size 12{ ital "gh"= {  { ital "mgh"}  over  {V} } = {  {"PE" rSub { size 8{"g"} } }  over  {V} } "."} {}so  gh gh size 12{ ital "gh"} {} is the gravitational potential energy per unit volume. Note that pressure PP size 12{P} {} has units of energy per unit volume, too. Since P=F/AP=F/A size 12{P=F/A} {}, its units are N/m2N/m2 size 12{"N/m" rSup { size 8{2} } } {}. If we multiply these by m/m, we obtain Nm/m3=J/m3Nm/m3=J/m3 size 12{N cdot "m/m" rSup { size 8{3} } ="J/m" rSup { size 8{3} } } {}, or energy per unit volume. Bernoullis equation is, in fact, just a convenient statement of conservation of energy for an incompressible fluid in the absence of friction.Making Connections: Conservation of Energy
Conservation of energy applied to fluid flow produces Bernoullis equation. The net work done by the fluids pressure results in changes in the fluids KEKE size 12{"KE"} {} and PEgPEg size 12{"PE" rSub { size 8{g} } } {} per unit volume. If other forms of energy are involved in fluid flow, Bernoullis equation can be modified to take these forms into account. Such forms of energy include thermal energy dissipated because of fluid viscosity.

The general form of Bernoullis equation has three terms in it, and it is broadly applicable. To understand it better, we will look at a number of specific situations that simplify and illustrate its use and meaning.

Bernoullis Equation for Static Fluids
Let us first consider the very simple situation where the fluid is staticthat is, v1=v2=0v1=v2=0 size 12{v rSub { size 8{1} } =v rSub { size 8{2} } =0} {}. Bernoullis equation in that case is
P1+gh1=P2+gh2.P1+gh1=P2+gh2. size 12{P rSub { size 8{1} } + ital "gh" rSub { size 8{1} } =P rSub { size 8{2} } + ital "gh" rSub { size 8{2} } "."} {}We can further simplify the equation by taking h2=0h2=0 size 12{h rSub { size 8{2} } =0} {} (we can always choose some height to be zero, just as we often have done for other situations involving the gravitational force, and take all other heights to be relative to this). In that case, we getP2=P1+gh1.P2=P1+gh1. size 12{P rSub { size 8{2} } =P rSub { size 8{1} } + ital "gh" rSub { size 8{1} } "."} {}This equation tells us that, in static fluids, pressure increases with depth. As we go from point 1 to point 2 in the fluid, the depth increases by h1h1 size 12{h rSub { size 8{1} } } {}, and consequently, P2P2 size 12{P rSub { size 8{2} } } {} is greater than P1P1 size 12{P rSub { size 8{1} } } {} by an amount gh1gh1 size 12{ ital "gh" rSub { size 8{1} } } {}. In the very simplest case, 

P1P1 size 12{P rSub { size 8{1} } } {} is zero at the top of the fluid, and we get the familiar relationship 

P=ghP=gh size 12{P= ital "gh"} {}. (Recall that 

P=ghP=gh size 12{P=hg} {} and 

PEg=mgh.PEg=mgh. size 12{"PE" rSub { size 8{g} } = ital "mgh"} {}) Bernoullis equation includes the fact that the pressure due to the weight of a fluid is 

ghgh size 12{ ital "gh"} {}. Although we introduce Bernoullis equation for fluid flow, it includes much of what we studied for static fluids in the preceding chapter.
Bernoullis PrincipleBernoullis Equation at Constant Depth
Another important situation is one in which the fluid moves but its depth is constantthat is, h1=h2h1=h2 size 12{h rSub { size 8{1} } =h rSub { size 8{2} } } {}. Under that condition, Bernoullis equation becomes





P

1


+

1
2



v

1


2


=


P

2


+

1
2




v

2


2


.









P

1


+

1
2



v

1


2


=


P

2


+

1
2




v

2


2


.



 size 12{P rSub { size 8{1} } + {  {1}  over  {2} } v rSub { size 8{1} } "" lSup { size 8{2} } =P rSub { size 8{2} } + {  {1}  over  {2} } v rSub { size 8{2} } "" lSup { size 8{2} }  "." } {}

Situations in which fluid flows at a constant depth are so important that this equation is often called Bernoullis principle. It is Bernoullis equation for fluids at constant depth. (Note again that this applies to a small volume of fluid as we follow it along its path.) As we have just discussed, pressure drops as speed increases in a moving fluid. We can see this from Bernoullis principle. For example, if v2v2 size 12{v rSub { size 8{2} } } {} is greater than v1v1 size 12{v rSub { size 8{1} } } {} in the equation, then P2P2 size 12{P rSub { size 8{2} } } {} must be less than P1P1 size 12{P rSub { size 8{1} } } {} for the equality to hold.
Calculating Pressure: Pressure Drops as a Fluid Speeds Up 
In [link], we found that the speed of water in a hose increased from 1.96 m/s to 25.5 m/s going from the hose to the nozzle. Calculate the pressure in the hose, given that the absolute pressure in the nozzle is 1.01105N/m21.01105N/m2 size 12{1 "." "01" times "10" rSup { size 8{5} } `"N/m" rSup { size 8{2} } } {} (atmospheric, as it must be) and assuming level, frictionless flow.Strategy
Level flow means constant depth, so Bernoullis principle applies. We use the subscript 1 for values in the hose and 2 for those in the nozzle. We are thus asked to find P1P1 size 12{P rSub { size 8{1} } } {}.
Solution
Solving Bernoullis principle for P1P1 size 12{P rSub { size 8{1} } } {} yields






P

1


=


P

2


+

1
2




v

2


2





1
2



v

1


2


=


P

2


+

1
2




(

v

2


2






v

1


2


)
.










P

1


=


P

2


+

1
2




v

2


2





1
2



v

1


2


=


P

2


+

1
2




(

v

2


2






v

1


2


)
.




 size 12{P rSub { size 8{1} } =P rSub { size 8{2} } + {  {1}  over  {2} } v rSub { size 8{2} } "" lSup { size 8{2} }  -  {  {1}  over  {2} } v rSub { size 8{1} } "" lSup { size 8{2} } =P rSub { size 8{2} } + {  {1}  over  {2} }  \( v rSub { size 8{2} } "" lSup { size 8{2} }  - v rSub { size 8{1} } "" lSup { size 8{2} }  \)  "." } {}

Substituting known values,

P1
=
1.01105 N/m2



+12(103
 kg/m3)(25.5 m/s)2(1.96 m/s)2


=
4.24105 N/m2.

P1
=
1.01105 N/m2



+12(103
 kg/m3)(25.5 m/s)2(1.96 m/s)2


=
4.24105 N/m2.
Discussion
This absolute pressure in the hose is greater than in the nozzle, as expected since 
v

v
 is greater in the nozzle. The pressure P2P2 size 12{P rSub { size 8{2} } } {} in the nozzle must be atmospheric since it emerges into the atmosphere without other changes in conditions.

Applications of Bernoullis Principle
There are a number of devices and situations in which fluid flows at a constant height and, thus, can be analyzed with Bernoullis principle.
Entrainment
People have long put the Bernoulli principle to work by using reduced pressure in high-velocity fluids to move things about. With a higher pressure on the outside, the high-velocity fluid forces other fluids into the stream. This process is called entrainment. Entrainment devices have been in use since ancient times, particularly as pumps to raise water small heights, as in draining swamps, fields, or other low-lying areas. Some other devices that use the concept of entrainment are shown in [link].
Examples of entrainment devices that use increased fluid speed to create low pressures, which then entrain one fluid into another. (a) A Bunsen burner uses an adjustable gas nozzle, entraining air for proper combustion. (b) An atomizer uses a squeeze bulb to create a jet of air that entrains drops of perfume. Paint sprayers and carburetors use very similar techniques to move their respective liquids. (c) A common aspirator uses a high-speed stream of water to create a region of lower pressure. Aspirators may be used as suction pumps in dental and surgical situations or for draining a flooded basement or producing a reduced pressure in a vessel. (d) The chimney of a water heater is designed to entrain air into the pipe leading through the ceiling.



Wings and SailsThe airplane wing is a beautiful example of Bernoullis principle in action. [link](a) shows the characteristic shape of a wing. The wing is tilted upward at a small angle and the upper surface is longer, causing air to flow faster over it. The pressure on top of the wing is therefore reduced, creating a net upward force or lift. (Wings can also gain lift by pushing air downward, utilizing the conservation of momentum principle. The deflected air molecules result in an upward force on the wing  Newtons third law.) Sails also have the characteristic shape of a wing. (See [link](b).) The pressure on the front side of the sail, PfrontPfront size 12{P rSub { size 8{"front"} } } {}, is lower than the pressure on the back of the sail, PbackPback size 12{P rSub { size 8{"back"} } } {}. This results in a forward force and even allows you to sail into the wind.
Making Connections: Take-Home Investigation with Two Strips of Paper
For a good illustration of Bernoullis principle, make two strips of paper, each about 15 cm long and 4 cm wide. Hold the small end of one strip up to your lips and let it drape over your finger. Blow across the paper. What happens? Now hold two strips of paper up to your lips, separated by your fingers. Blow between the strips. What happens?
Velocity measurement
[link] shows two devices that measure fluid velocity based on Bernoullis principle. The manometer in [link](a) is connected to two tubes that are small enough not to appreciably disturb the flow. The tube facing the oncoming fluid creates a dead spot having zero velocity (v1=0v1=0 size 12{v rSub { size 8{1} } =0} {}) in front of it, while fluid passing the other tube has velocity v2v2 size 12{v rSub { size 8{2} } } {}. This means that Bernoullis principle as stated in 





P

1


+

1
2

 

v

1


2


=


P

2


+

1
2




v
2
2










P

1


+

1
2

 

v

1


2


=


P

2


+

1
2




v
2
2




 size 12{P rSub { size 8{1} } + {  {1}  over  {2} } v rSub { size 8{1} } "" lSup { size 8{2} } =P rSub { size 8{2} } + {  {1}  over  {2} } v rSub { size 8{2} } "" lSup { size 8{2} } } {}
 
     becomes





P

1


=


P

2


+

1
2




v

2


2


.










P

1


=


P

2


+

1
2




v

2


2


.




 size 12{P rSub { size 8{1} } =P rSub { size 8{2} } + {  {1}  over  {2} } v rSub { size 8{2} } "" lSup { size 8{2} }  "." } {}

(a) The Bernoulli principle helps explain lift generated by a wing. (b) Sails use the same technique to generate part of their thrust.


Thus pressure P2P2 size 12{P rSub { size 8{2} } } {} over the second opening is reduced by 12v2212v22 size 12{ {  { size 8{1} }  over  { size 8{2} } } v rSub { size 8{2} }  rSup { size 8{2} } } {}, and so the fluid in the manometer rises by 
h

h
 on the side connected to the second opening, whereh12v22.h12v22. size 12{h prop  {  {1}  over  {2} } v rSub { size 8{2} }  rSup { size 8{2} } "."} {}(Recall that the symbol  size 12{ prop } {} means proportional to.) Solving for v2v2 size 12{v rSub { size 8{2} } } {}, we see thatv2h.v2h. size 12{v rSub { size 8{2} }  prop  sqrt {h} "."} {}[link](b) shows a version of this device that is in common use for measuring various fluid velocities; such devices are frequently used as air speed indicators in aircraft.Measurement of fluid speed based on Bernoullis principle. (a) A manometer is connected to two tubes that are close together and small enough not to disturb the flow. Tube 1 is open at the end facing the flow. A dead spot having zero speed is created there. Tube 2 has an opening on the side, and so the fluid has a speed 
v

v
 across the opening; thus, pressure there drops. The difference in pressure at the manometer is 12v2212v22 size 12{ {  { size 8{1} }  over  { size 8{2} } } v rSub { size 8{2} }  rSup { size 8{2} } } {}, and so 
h

h
 is proportional to 12v2212v22 size 12{ {  { size 8{1} }  over  { size 8{2} } } v rSub { size 8{2} }  rSup { size 8{2} } } {}. (b) This type of velocity measuring device is a Prandtl tube, also known as a pitot tube.




SummaryBernoullis equation states that the sum on each side of the following equation is constant, or the same at any two points in an incompressible frictionless fluid:
P1+12v12+gh1=P2+12v22+gh2.P1+12v12+gh1=P2+12v22+gh2. size 12{P rSub { size 8{1} } + {  { size 8{1} }  over  { size 8{2} } } v rSub { size 8{1} }  rSup { size 8{2} } + ital "gh" rSub { size 8{1} } =P rSub { size 8{2} } + {  { size 8{1} }  over  { size 8{2} } } v rSub { size 8{2} }  rSup { size 8{2} } + ital "gh" rSub { size 8{2} } } {}
Bernoullis principle is Bernoullis equation applied to situations in which depth is constant. The terms involving depth (or height h ) subtract out, yielding
P1+12v12=P2+12v22.P1+12v12=P2+12v22. size 12{P rSub { size 8{1} } + {  { size 8{1} }  over  { size 8{2} } } v rSub { size 8{1} }  rSup { size 8{2} } =P rSub { size 8{2} } + {  { size 8{1} }  over  { size 8{2} } } v rSub { size 8{2} }  rSup { size 8{2} } } {}
Bernoullis principle has many applications, including entrainment, wings and sails, and velocity measurement.

Conceptual Questions

You can squirt water a considerably greater distance by placing your thumb over the end of a garden hose and then releasing, than by leaving it completely uncovered. Explain how this works.



Water is shot nearly vertically upward in a decorative fountain and the stream is observed to broaden as it rises. Conversely, a stream of water falling straight down from a faucet narrows. Explain why, and discuss whether surface tension enhances or reduces the effect in each case.



Look back to [link]. Answer the following two questions. Why is PoPo size 12{P rSub { size 8{o} } } {} less than atmospheric? Why is PoPo size 12{P rSub { size 8{o} } } {} greater than PiPi size 12{P rSub { size 8{i} } } {}?


Give an example of entrainment not mentioned in the text.



Many entrainment devices have a constriction, called a Venturi, such as shown in [link]. How does this bolster entrainment?

A tube with a narrow segment designed to enhance entrainment is called a Venturi. These are very commonly used in carburetors and aspirators.




Some chimney pipes have a T-shape, with a crosspiece on top that helps draw up gases whenever there is even a slight breeze. Explain how this works in terms of Bernoullis principle.   




Is there a limit to the height to which an entrainment device can raise a fluid? Explain your answer.




Why is it preferable for airplanes to take off into the wind rather than with the wind?




Roofs are sometimes pushed off vertically during a tropical cyclone, and buildings sometimes explode outward when hit by a tornado. Use Bernoullis principle to explain these phenomena.




Why does a sailboat need a keel?




It is dangerous to stand close to railroad tracks when a rapidly moving commuter train passes. Explain why atmospheric pressure would push you toward the moving train.




Water pressure inside a hose nozzle can be less than atmospheric pressure due to the Bernoulli effect. Explain in terms of energy how the water can emerge from the nozzle against the opposing atmospheric pressure.




A perfume bottle or atomizer sprays a fluid that is in the bottle. ([link].) How does the fluid rise up in the vertical tube in the bottle?
Atomizer: perfume bottle with tube to carry perfume up through the bottle. (credit: Antonia Foy, Flickr)






If you lower the window on a car while moving, an empty plastic bag can sometimes fly out the window. Why does this happen?


Problems & Exercises

Verify that pressure has units of energy per unit volume.










P
               


=
               






Force
                  



Area
                  




,
              





(
              

P
              


)
                  

units
                      




=
                 




N/m
                  

2
                      


=
                

N
                 


                 


m/m
                  

3
                      


=
               


J/m
                

3
                    





=


energy/volume
           













P
               


=
               






Force
                  



Area
                  




,
              





(
              

P
              


)
                  

units
                      




=
                 




N/m
                  

2
                      


=
                

N
                 


                 


m/m
                  

3
                      


=
               


J/m
                

3
                    





=


energy/volume
           






alignl { stack {
 size 12{P= {  {"Force"}  over  {"Area"} } ,}  {} # 
 size 12{ \( P \)  rSub { size 8{"units"} } ="N/m" rSup { size 8{2} } =N cdot "m/m" rSup { size 8{3} } ="J/m" rSup { size 8{3} } }  {} # 
="energy/volume" {} 
} } {}
     

Suppose you have a wind speed gauge like the pitot tube shown in [link](b). By what factor must wind speed increase to double the value of hh size 12{h} {} in the manometer? Is this independent of the moving fluid and the fluid in the manometer?

If the pressure reading of your pitot tube is 15.0 mm Hg at a speed of 200 km/h, what will it be at 700 km/h at the same altitude?

184 mm Hg


Calculate the maximum height to which water could be squirted with the hose in [link] example if it: (a) Emerges from the nozzle. (b) Emerges with the nozzle removed, assuming the same flow rate.

Every few years, winds in Boulder, Colorado, attain sustained speeds of 45.0 m/s (about 100 mi/h) when the jet stream descends during early spring. Approximately what is the force due to the Bernoulli effect on a roof having an area of 220m2220m2 size 12{"220"`m rSup { size 8{2} } } {}? Typical air density in Boulder is 1.14kg/m31.14kg/m3 size 12{1 "." "14"`"kg/m" rSup { size 8{3} } } {}, and the corresponding atmospheric pressure is 8.89104N/m28.89104N/m2 size 12{8 "." "89" times "10" rSup { size 8{4} } `"N/m" rSup { size 8{2} } } {}. (Bernoullis principle as stated in the text assumes laminar flow. Using the principle here produces only an approximate result, because there is significant turbulence.)





2
.

54


10

5



 N








2
.

54


10

5



 N




 size 12{2 "." "54" times "10" rSup { size 8{5} } " N"} {}




(a) Calculate the approximate force on a square meter of sail, given the horizontal velocity of the wind is 6.00 m/s parallel to its front surface and 3.50 m/s along its back surface. Take the density of air to be 1.29 kg/m31.29 kg/m3 size 12{1 "." "29"`"kg/m" rSup { size 8{3} } } {}. (The calculation, based on Bernoullis principle, is approximate due to the effects of turbulence.) (b) Discuss whether this force is great enough to be effective for propelling a sailboat.

(a) What is the pressure drop due to the Bernoulli effect as water goes into a 3.00-cm-diameter nozzle from a 9.00-cm-diameter fire hose while carrying a flow of 40.0 L/s? (b) To what maximum height above the nozzle can this water rise? (The actual height will be significantly smaller due to air resistance.)

(a) 1.58106 N/m21.58106 N/m2 size 12{1 "." "58" times "10" rSup { size 8{6} } " N/m" rSup { size 8{2} } } {}(b) 163 m


 (a) Using Bernoullis equation, show that the measured fluid speed vv for a pitot tube, like the one in [link](b), is given byv=2gh1/2,v=2gh1/2, size 12{v= left ( {  {2 { {}} sup { ' }gh}  over  {} }  right ) rSup { size 8{ {1} slash {2} } } } {}where hh size 12{h} {} is the height of the manometer fluid, 

 size 12{ { {}} sup { ' }} {} is the density of the manometer fluid,  size 12{} {} is the density of the moving fluid, and gg size 12{g} {} is the acceleration due to gravity. (Note that vv size 12{v} {} is indeed proportional to the square root of hh size 12{h} {}, as stated in the text.) (b) Calculate vv size 12{v} {} for moving air if a mercury manometers hh size 12{h} {} is 0.200 m.
Glossary
Bernoullis equation the equation resulting from applying conservation of energy to an incompressible frictionless fluid: P + 1/2pv2 + pgh = constant , through the fluid
Bernoullis principle Bernoullis equation applied at constant depth: P1 + 1/2pv12 = P2 + 1/2pv22

