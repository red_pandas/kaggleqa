
Sound Intensity and Sound Level
Sound Intensity and Sound Level
Define intensity, sound intensity, and sound pressure level.
Calculate sound intensity levels in decibels (dB).

Noise on crowded roadways like this one in Delhi makes it hard to hear others unless they shout. (credit: Lingaraj G J, Flickr)


In a quiet forest, you can sometimes hear a single leaf fall to the ground. After settling into bed, you may hear your blood pulsing through your ears. But when a passing motorist has his stereo turned up, you cannot even hear what the person next to you in your car is saying. We are all very familiar with the loudness of sounds and aware that they are related to how energetically the source is vibrating. In cartoons depicting a screaming person (or an animal making a loud noise), the cartoonist often shows an open mouth with a vibrating uvula, the hanging tissue at the back of the mouth, to suggest a loud sound coming from the throat [link]. High noise exposure is hazardous to hearing, and it is common for musicians to have hearing losses that are sufficiently severe that they interfere with the musicians abilities to perform. The relevant physical quantity is sound intensity, a concept that is valid for all sounds whether or not they are in the audible range.Intensity is defined to be the power per unit area carried by a wave. Power is the rate at which energy is transferred by the wave. In equation form, intensity II size 12{I} {} is
I=PA,I=PA, size 12{I= {  {P}  over  {A} } } {}where PP size 12{P} {} is the power through an area AA size 12{A} {}. The SI unit for II size 12{I} {} is W/m2W/m2. The intensity of a sound wave is related to its amplitude squared by the following relationship:I=(p)22vw.I=(p)22vw. size 12{I= {  { left (p right )}  over  {2 ital "pv" size 8{m}} }  rSup {2} } {}Here pp is the pressure variation or pressure amplitude (half the difference between the maximum and minimum pressure in the sound wave) in units of pascals (Pa) or N/m2N/m2. (We are using a lower case pp  for pressure to distinguish it from power, denoted by  PP above.) The energy (as kinetic energy 
mv22mv22) of an oscillating element of air due to a traveling sound wave is proportional to its amplitude squared. In this equation, 
 is the density of the material in which the sound wave travels, in units of kg/m3kg/m3, and vwvw is the speed of sound in the medium, in units of m/s. The pressure variation is proportional to the amplitude of the oscillation, and so II size 12{I} {} varies as (p)2(p)2 size 12{ \( p \)  rSup { size 8{2} } } {} ([link]). This relationship is consistent with the fact that the sound wave is produced by some vibration; the greater its pressure amplitude, the more the air is compressed in the sound it creates.Graphs of the gauge pressures in two sound waves of different intensities. The more intense sound is produced by a source that has larger-amplitude oscillations and has greater pressure maxima and minima. Because pressures are higher in the greater-intensity sound, it can exert larger forces on the objects it encounters.


Sound intensity levels are quoted in decibels (dB) much more often than sound intensities in watts per meter squared. Decibels are the unit of choice in the scientific literature as well as in the popular media. The reasons for this choice of units are related to how we perceive sounds. How our ears perceive sound can be more accurately described by the logarithm of the intensity rather than directly to the intensity. The sound intensity level  size 12{} {} in decibels of a sound having an intensity II in watts per meter squared is defined to bedB=10log10II0,dB=10log10II0, size 12{ left ("dB" right )="10""log" rSub { size 8{"10"} }  left ( {  {I}  over  {I rSub { size 8{0} } } }  right )} {}where I0=1012W/m2I0=1012W/m2 size 12{I rSub { size 8{0} } ="10" rSup { size 8{ - "12"} } "W/m" rSup { size 8{2} } } {} is a reference intensity. In particular, I0I0 size 12{I rSub { size 8{0} } } {} is the lowest or threshold intensity of sound a person with normal hearing can perceive at a frequency of 1000 Hz. Sound intensity level is not the same as intensity. Because  size 12{} {} is defined in terms of a ratio, it is a unitless quantity telling you the level of the sound relative to a fixed standard (1012W/m21012W/m2 size 12{"10" rSup { size 8{ - "12"} } "W/m" rSup { size 8{2} } } {}, in this case). The units of decibels (dB) are used to indicate this ratio is multiplied by 10 in its definition. The bel, upon which the decibel is based, is named for Alexander Graham Bell, the inventor of the telephone.Sound Intensity Levels and Intensities
Sound intensity level  (dB)
Intensity I(W/m2)

Example/effect

00
1101211012
Threshold of hearing at 1000 Hz

1010
1101111011
Rustle of leaves

2020
1101011010
Whisper at 1 m distance

3030
11091109
Quiet home

4040
11081108
Average home

5050
11071107
Average office, soft music

6060
11061106
Normal conversation

7070
11051105
Noisy office, busy traffic

8080
11041104
Loud radio, classroom lecture

9090
11031103
Inside a heavy truck; damage from prolonged exposure1


100100
11021102
Noisy factory, siren at 30 m; damage from 8 h per day exposure


110110
11011101
Damage from 30 min per day exposure

120120
11
Loud rock concert, pneumatic chipper at 2 m; threshold of pain


140140
11021102
Jet airplane at 30 m; severe pain, damage in seconds

160160
11041104
Bursting of eardrums
The decibel level of a sound having the threshold intensity of 1012W/m21012W/m2 size 12{"10" rSup { size 8{ - "12"} } "W/m" rSup { size 8{2} } } {} is =0dB=0dB size 12{=0"dB"} {}, because log101=0log101=0 size 12{"log" rSub { size 8{"10"} } 1=0} {}. That is, the threshold of hearing is 0 decibels. [link] gives levels in decibels and intensities in watts per meter squared for some familiar sounds.One of the more striking things about the intensities in [link] is that the intensity in watts per meter squared is quite small for most sounds. The ear is sensitive to as little as a trillionth of a watt per meter squaredeven more impressive when you realize that the area of the eardrum is only about 1 cm21 cm2, so that only 10161016 size 12{"10" rSup { size 8{ - "16"} } } {} W falls on it at the threshold of hearing! Air molecules in a sound wave of this intensity vibrate over a distance of less than one molecular diameter, and the gauge pressures involved are less than 109109 size 12{"10" rSup { size 8{ - 9} } } {} atm.Another impressive feature of the sounds in [link] is their numerical range. Sound intensity varies by a factor of 10121012 size 12{"10" rSup { size 8{"12"} } } {} from threshold to a sound that causes damage in seconds. You are unaware of this tremendous range in sound intensity because how your ears respond can be described approximately as the logarithm of intensity. Thus, sound intensity levels in decibels fit your experience better than intensities in watts per meter squared. The decibel scale is also easier to relate to because most people are more accustomed to dealing with numbers such as 0, 53, or 120 than numbers such as 1.0010111.001011 size 12{1 "." "00" times "10" rSup { size 8{ - "11"} } } {}.One more observation readily verified by examining [link] or using I=(p)2vw2I=(p)2vw2 is that each factor of 10 in intensity corresponds to 10 dB. For example, a 90 dB sound compared with a 60 dB sound is 30 dB greater, or three factors of 10 (that is, 103103 times) as intense. Another example is that if one sound is 107107 as intense as another, it is 70 dB higher. See [link].Ratios of Intensities and Corresponding Differences in Sound Intensity Levels
I2/I1I2/I1
2121

2.0

           3.0 dB
            

5.0

              7.0 dB
            

10.0

10.0 dB
            
Calculating Sound Intensity Levels: Sound Waves
Calculate the sound intensity level in decibels for a sound wave traveling in air at 0C0C
 and having a pressure amplitude of 0.656 Pa.
Strategy

We are given pp size 12{p} {}, so we can calculate II using the equation 

I=(p)2
/


(
2
pvw
)2


I=(p)2
/


(
2
pvw
)2


. Using II, we can calculate 

 straight from its definition in 

dB=10log10(I/I0)dB=10log10(I/I0).
Solution

(1) Identify knowns:
Sound travels at 331 m/s in air at 0C0C.Air has a density of 1.29 kg/m31.29 kg/m3 at atmospheric pressure and 0C0C.(2) Enter these values and the pressure amplitude into 

I=
(p)

2

/
(
2vw
)

I=
(p)

2

/
(
2vw
)
:
I=
(p)2
2

vw

=0.656 Pa221.29 kg/m3331 m/s=5.04104 W/m2.
I=
(p)2
2

vw

=0.656 Pa221.29 kg/m3331 m/s=5.04104 W/m2. size 12{I= {  { left (p right ) rSup { size 8{2} } }  over  {2 ital "pv" size 8{m}} } = {  { left (0 "." "656"" Pa" right ) rSup { size 8{2} } }  over  {2 left (1 "." "29"" kg/m" rSup { size 8{3} }  right ) left ("331"" m/s" right )} } =5 "." "04" times "10" rSup { size 8{ - 4} } " W/m" rSup { size 8{2} } } {}(3) Enter the value for II and the known value for I0I0 into dB=10log10(I/I0)dB=10log10(I/I0). Calculate to find the sound intensity level in decibels:10 log10(5.04  108)=10(8.70)dB=87 dB.10 log10(5.04  108)=10(8.70)dB=87 dB.
Discussion

This 87 dB sound has an intensity five times as great as an 80 dB sound. So a factor of five in intensity corresponds to a difference of 7 dB in sound intensity level. This value is true for any intensities differing by a factor of five.
Change Intensity Levels of a Sound: What Happens to the Decibel Level?Show that if one sound is twice as intense as another, it has a sound level about 3 dB higher.
StrategyYou are given that the ratio of two intensities is 2 to 1, and are then asked to find the difference in their sound levels in decibels. You can solve this problem using of the properties of logarithms.
Solution

(1) Identify knowns:
The ratio of the two intensities is 2 to 1, or:
I2I1=2.00.I2I1=2.00. size 12{ {  {I rSub { size 8{2} } }  over  {I rSub { size 8{1} } } } =2 "." "00"} {}We wish to show that the difference in sound levels is about 3 dB. That is, we want to show:21=3 dB.21=3 dB. size 12{ rSub { size 8{2} }  -  rSub { size 8{1} } =3" dB"} {}Note that:





log

10



b


log

10




a
=

log

10





b
a


.









log

10



b


log

10




a
=

log

10





b
a


.




 size 12{"log" rSub { size 8{"10"} } b - "log" rSub { size 8{"10"} } a="log" rSub { size 8{"10"} }  left ( {  {b}  over  {a} }  right ) "." } {}

(2) Use the definition of  to get:21=10 log10I2I1=10log102.00=100.301dB.21=10 log10I2I1=10log102.00=100.301dB. size 12{ rSub { size 8{2} }  -  rSub { size 8{1} } ="10 log" rSub { size 8{"10"} }  left ( {  {I rSub { size 8{2} } }  over  {I rSub { size 8{1} } } }  right )="10"" log" rSub { size 8{"10"} } 2 "." "00"="10" "."  left (0 "." "301" right )"dB"} {}Thus,
21=3.01 dB.21=3.01 dB. size 12{ rSub { size 8{2} }  -  rSub { size 8{1} } =3 "." "01"" dB"} {}Discussion
This means that the two sound intensity levels differ by 3.01 dB, or about 3 dB, as advertised. Note that because only the ratio I2/I1I2/I1 is given (and not the actual intensities), this result is true for any intensities that differ by a factor of two. For example, a 56.0 dB sound is twice as intense as a 53.0 dB sound, a 97.0 dB sound is half as intense as a 100 dB sound, and so on.It should be noted at this point that there is another decibel scale in use, called the sound pressure level, based on the ratio of the pressure amplitude to a reference pressure. This scale is used particularly in applications where sound travels in water. It is beyond the scope of most introductory texts to treat this scale because it is not commonly used for sounds in air, but it is important to note that very different decibel levels may be encountered when sound pressure levels are quoted. For example, ocean noise pollution produced by ships may be as great as 200 dB expressed in the sound pressure level, where the more familiar sound intensity level we use here would be something under 140 dB for the same sound.
Take-Home Investigation: Feeling Sound
Find a CD player and a CD that has rock music. Place the player on a light table, insert the CD into the player, and start playing the CD. Place your hand gently on the table next to the speakers. Increase the volume and note the level when the table just begins to vibrate as the rock music plays. Increase the reading on the volume control until it doubles. What has happened to the vibrations?
Check Your Understanding Describe how amplitude is related to the loudness of a sound.
Amplitude is directly proportional to the experience of loudness. As amplitude increases, loudness increases.
Check Your UnderstandingIdentify common sounds at the levels of 10 dB, 50 dB, and 100 dB.
10 dB: Running fingers through your hair.50 dB: Inside a quiet home with no television or radio.100 dB: Take-off of a jet plane.

Section Summary

Intensity is the same for a sound wave as was defined for all waves; it is
    I=PA,I=PA, size 12{I= {  {P}  over  {A} } } {}
where PP is the power crossing area AA. The SI unit for II is watts per meter squared. The intensity of a sound wave is also related to the pressure amplitude pp
I=




(p)

2



2
v


w




,I=




(p)

2



2
v


w




, size 12{I= {  { left (p right )}  over  {2 ital "pv" size 8{m}} }  rSup {2} } {}
where  size 12{p} {} is the density of the medium in which the sound wave travels and vwvw size 12{p} {} is the speed of sound in the medium.
Sound intensity level in units of decibels (dB) is
    dB=10log10II0,dB=10log10II0, size 12{ left ("dB" right )="10""log" rSub { size 8{"10"} }  left ( {  {I}  over  {I rSub { size 8{0} } } }  right )} {}
where 

I0

=

10
12

W/

m2



I0

=

10
12

W/

m2

 is the threshold intensity of hearing. 

Conceptual Questions

Six members of a synchronized swim team wear earplugs to protect themselves against water pressure at depths, but they can still hear the music and perform the combinations in the water perfectly. One day, they were asked to leave the pool so the dive team could practice a few dives, and they tried to practice on a mat, but seemed to have a lot more difficulty. Why might this be?


A community is concerned about a plan to bring train service to their downtown from the towns outskirts. The current sound intensity level, even though the rail yard is blocks away, is 70 dB downtown. The mayor assures the public that there will be a difference of only 30 dB in sound in the downtown area. Should the townspeople be concerned? Why?

Problems & Exercises What is the intensity in watts per meter squared of 85.0-dB sound?


3.16104W/m23.16104W/m2

 The warning tag on a lawn mower states that it produces noise at a level of 91.0 dB. What is this in watts per meter squared?

 A sound wave traveling in 20C20C air has a pressure amplitude of 0.5Pa. What is the intensity of the wave?
3.04104W/m23.04104W/m2 What intensity level does the sound in the preceding problem correspond to?

 What sound intensity level in dB is produced by earphones that create an intensity of  


4.00


10
2



W/m
2




4.00


10
2



W/m
2


?

106 dB

 Show that an intensity of 1012W/m21012W/m2 is the same as 1016W/cm21016W/cm2.
 (a) What is the decibel level of a sound that is twice as intense as a 90.0-dB sound? (b) What is the decibel level of a sound that is one-fifth as intense as a 90.0-dB sound?


(a) 93 dB(b) 83 dB

 (a) What is the intensity of a sound that has a level 7.00 dB lower than a 4.00109W/m24.00109W/m2 sound? (b) What is the intensity of a sound that is 3.00 dB higher than a 4.00109W/m24.00109W/m2 sound?
 (a) How much more intense is a sound that has a level 17.0 dB higher than another? (b) If one sound has a level 23.0 dB less than another, what is the ratio of their intensities?


(a) 50.1(b) 5.011035.01103
 or 12001200
 People with good hearing can perceive sounds as low in level as 8.00 dB8.00 dB at a frequency of 3000 Hz. What is the intensity of this sound in watts per meter squared?
 If a large housefly 3.0 m away from you makes a noise of 40.0 dB, what is the noise level of 1000 flies at that distance, assuming interference has a negligible effect?


70.0 dB

 Ten cars in a circle at a boom box competition produce a 120-dB sound intensity level at the center of the circle. What is the average sound intensity level produced there by each stereo, assuming interference effects can be neglected?

 The amplitude of a sound wave is measured in terms of its maximum gauge pressure. By what factor does the amplitude of a sound wave increase if the sound intensity level goes up by 40.0 dB?


100

 If a sound intensity level of 0 dB at 1000 Hz corresponds to a maximum gauge pressure (sound amplitude) of 109atm109atm, what is the maximum gauge pressure in a 60-dB sound? What is the maximum gauge pressure in a 120-dB sound?
 An 8-hour exposure to a sound intensity level of 90.0 dB may cause hearing damage. What energy in joules falls on a 0.800-cm-diameter eardrum so exposed?


1.45103J1.45103J
 (a) Ear trumpets were never very common, but they did aid people with hearing losses by gathering sound over a large area and concentrating it on the smaller area of the eardrum. What decibel increase does an ear trumpet produce if its sound gathering area is 900 cm2900 cm2 and the area of the eardrum is 0.500 cm20.500 cm2, but the trumpet only has an efficiency of 5.00% in transmitting the sound to the eardrum? (b) Comment on the usefulness of the decibel increase found in part (a).
 Sound is more effectively transmitted into a stethoscope by direct contact than through the air, and it is further intensified by being concentrated on the smaller area of the eardrum. It is reasonable to assume that sound is transmitted into a stethoscope 100 times as effectively compared with transmission though the air. What, then, is the gain in decibels produced by a stethoscope that has a sound gathering area of 15.0 cm215.0 cm2, and concentrates the sound onto two eardrums with a total area of 0.900 cm20.900 cm2 with an efficiency of 40.0%?

28.2 dB

 Loudspeakers can produce intense sounds with surprisingly small energy input in spite of their low efficiencies. Calculate the power input needed to produce a 90.0-dB sound intensity level for a 12.0-cm-diameter speaker that has an efficiency of 1.00%. (This value is the sound intensity level right at the speaker.)


Footnotes1   Several government agencies and health-related professional associations recommend that 85 dB not be exceeded for 8-hour daily exposures in the absence of hearing protection.Glossary
intensity the power per unit area carried by a wave
sound intensity level a unitless quantity telling you the level of the sound relative to a fixed standard
sound pressure level the ratio of the pressure amplitude to a reference pressure

