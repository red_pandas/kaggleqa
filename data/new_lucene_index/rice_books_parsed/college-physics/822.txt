
Kirchhoffs Rules
Kirchhoffs Rules
Analyze a complex circuit using Kirchhoffs rules, using the conventions for determining the correct signs of various terms.

Many complex circuits, such as the one in [link], cannot be analyzed with the series-parallel techniques developed in Resistors in Series and Parallel and Electromotive Force: Terminal Voltage. There are, however, two circuit analysis rules that can be used to analyze any circuit, simple or complex. These rules are special cases of the laws of conservation of charge and conservation of energy. The rules are known as Kirchhoffs rules, after their inventor Gustav Kirchhoff (18241887).This circuit cannot be reduced to a combination of series and parallel connections. Kirchhoffs rules, special applications of the laws of conservation of charge and energy, can be used to analyze it. (Note: The script E in the figure represents electromotive force, emf.)


Kirchhoffs Rules

Kirchhoffs first rulethe junction rule. The sum of all currents entering a junction must equal the sum of all currents leaving the junction.
Kirchhoffs second rulethe loop rule. The algebraic sum of changes in potential around any closed circuit path (loop) must be zero.


Explanations of the two rules will now be given, followed by problem-solving hints for applying Kirchhoffs rules, and a worked example that uses them.
Kirchhoffs First Rule
Kirchhoffs first rule (the junction rule) is an application of the conservation of charge to a junction; it is illustrated in [link]. Current is the flow of charge, and charge is conserved; thus, whatever charge flows into the junction must flow out. Kirchhoffs first rule requires that I1=I2+I3I1=I2+I3 size 12{I rSub { size 8{1} } =I rSub { size 8{2} } +I rSub { size 8{3} } } {} (see figure). Equations like this can and will be used to analyze circuits and to solve circuit problems.
Making Connections: Conservation Laws
Kirchhoffs rules for circuit analysis are applications of conservation laws to circuits. The first rule is the application of conservation of charge, while the second rule is the application of conservation of energy. Conservation laws, even used in a specific application, such as circuit analysis, are so basic as to form the foundation of that application.

The junction rule. The diagram shows an example of Kirchhoffs first rule where the sum of the currents into a junction equals the sum of the currents out of a junction. In this case, the current going into the junction splits and comes out as two currents, so that I1=I2+I3I1=I2+I3 size 12{I rSub { size 8{1} } =I rSub { size 8{2} } +I rSub { size 8{3} } } {}. Here I1I1 size 12{I rSub { size 8{1} } } {} must be 11 A, since I2I2 size 12{I rSub { size 8{2} } } {} is 7 A and I3I3 size 12{I rSub { size 8{3} } } {} is 4 A.



Kirchhoffs Second Rule
Kirchhoffs second rule (the loop rule) is an application of conservation of energy. The loop rule is stated in terms of potential, VV size 12{V} {}, rather than potential energy, but the two are related since PEelec=qVPEelec=qV size 12{ ital "PE" rSub { size 8{"elec"} } = ital "qV"} {}. Recall that emf is the potential difference of a source when no current is flowing.  In a closed loop, whatever energy is supplied by emf must be transferred into other forms by devices in the loop, since there are no other ways in which energy can be transferred into or out of the circuit. [link] illustrates the changes in potential in a simple series circuit loop.Kirchhoffs second rule requires emfIrIR1IR2=0emfIrIR1IR2=0 size 12{"emf" -  ital "Ir" -  ital "IR" rSub { size 8{1} }  -  ital "IR" rSub { size 8{2} } =0} {}. Rearranged, this is emf=Ir+IR1+IR2emf=Ir+IR1+IR2 size 12{"emf"= ital "Ir"+ ital "IR" rSub { size 8{1} } + ital "IR" rSub { size 8{2} } } {}, which means the emf equals the sum of the IRIR size 12{ ital "IR"} {} (voltage) drops in the loop.The loop rule. An example of Kirchhoffs second rule where the sum of the changes in potential around a closed loop must be zero. (a) In this standard schematic of a simple series circuit, the emf supplies 18 V, which is reduced to zero by the resistances, with 1 V across the internal resistance, and 12 V and 5 V across the two load resistances, for a total of 18 V. (b) This perspective view represents the potential as something like a roller coaster, where charge is raised in potential by the emf and lowered by the resistances. (Note that the script E stands for emf.)



Applying Kirchhoffs Rules
By applying Kirchhoffs rules, we generate equations that allow us to find the unknowns in circuits. The unknowns may be currents, emfs, or resistances. Each time a rule is applied, an equation is produced. If there are as many independent equations as unknowns, then the problem can be solved. There are two decisions you must make when applying Kirchhoffs rules. These decisions determine the signs of various quantities in the equations you obtain from applying the rules.
When applying Kirchhoffs first rule, the junction rule, you must label the current in each branch and decide in what direction it is going. For example, in [link], [link], and [link], currents are labeled I1I1 size 12{I rSub { size 8{1} } } {}, I2I2 size 12{I rSub { size 8{2} } } {}, I3I3 size 12{I rSub { size 8{3} } } {}, and II size 12{I} {}, and arrows indicate their directions. There is no risk here, for if you choose the wrong direction, the current will be of the correct magnitude but negative.
When applying Kirchhoffs second rule, the loop rule, you must identify a closed loop and decide in which direction to go around it, clockwise or counterclockwise. For example, in [link] the loop was traversed in the same direction as the current (clockwise). Again, there is no risk; going around the circuit in the opposite direction reverses the sign of every term in the equation, which is like multiplying both sides of the equation by 1.1.
[link] and the following points will help you get the plus or minus signs right when applying the loop rule. Note that the resistors and emfs are traversed by going from a to b. In many circuits, it will be necessary to construct more than one loop. In traversing each loop, one needs to be consistent for the sign of the change in potential. (See [link].)Each of these resistors and voltage sources is traversed from a to b. The potential changes are shown beneath each element and are explained in the text. (Note that the script E stands for emf.)


When a resistor is traversed in the same direction as the current, the change in potential is IRIR size 12{- ital "IR"} {}. (See [link].)
When a resistor is traversed in the direction opposite to the current, the change in potential is +IR+IR size 12{+ ital "IR"} {}. (See [link].)
When an emf is traversed from  to + (the same direction it moves positive charge), the change in potential is +emf. (See [link].)
When an emf is traversed from + to  (opposite to the direction it moves positive charge), the change in potential is 



















 size 12{ - {}} {}
emf. (See [link].)
Calculating Current: Using Kirchhoffs Rules
Find the currents flowing in the circuit in [link].
This circuit is similar to that in [link], but the resistances and emfs are specified. (Each emf is denoted by script E.) The currents in each branch are labeled and assumed to move in the directions shown. This example uses Kirchhoffs rules to find the currents.


Strategy
This circuit is sufficiently complex that the currents cannot be found using Ohms law and the series-parallel techniquesit is necessary to use Kirchhoffs rules. Currents have been labeled I1I1 size 12{I rSub { size 8{1} } } {}, I2I2 size 12{I rSub { size 8{2} } } {}, and I3I3 size 12{I rSub { size 8{3} } } {} in the figure and assumptions have been made about their directions. Locations on the diagram have been labeled with letters a through h. In the solution we will apply the junction and loop rules, seeking three independent equations to allow us to solve for the three unknown currents.Solution
We begin by applying Kirchhoffs first or junction rule at point a. This gives
I1=I2+I3,I1=I2+I3, size 12{I rSub { size 8{1} } =I rSub { size 8{2} } +I rSub { size 8{3} } } {}since I1I1 size 12{I rSub { size 8{1} } } {} flows into the junction, while I2I2 size 12{I rSub { size 8{2} } } {} and I3I3 size 12{I rSub { size 8{3} } } {} flow out. Applying the junction rule at e produces exactly the same equation, so that no new information is obtained. This is a single equation with three unknownsthree independent equations are needed, and so the loop rule must be applied.
Now we consider the loop abcdea. Going from a to b, we traverse R2R2 size 12{R rSub { size 8{2} } } {} in the same (assumed) direction of the current I2I2 size 12{I rSub { size 8{2} } } {}, and so the change in potential is I2R2I2R2 size 12{ - I rSub { size 8{2} } R rSub { size 8{2} } } {}. Then going from b to c, we go from  to +, so that the change in potential is +emf1+emf1 size 12{+"emf" rSub { size 8{1} } } {}. Traversing the internal resistance r1r1 size 12{r rSub { size 8{1} } } {} from c to d gives I2r1I2r1 size 12{ - I rSub { size 8{2} } r rSub { size 8{1} } } {}. Completing the loop by going from d to a again traverses a resistor in the same direction as its current, giving a change in potential of I1R1I1R1 size 12{ - I rSub { size 8{1} } R rSub { size 8{1} } } {}.The loop rule states that the changes in potential sum to zero. Thus,
I2R2+emf1I2r1I1R1=I2(R2+r1)+emf1I1R1=0.I2R2+emf1I2r1I1R1=I2(R2+r1)+emf1I1R1=0. size 12{ - I rSub { size 8{2} } R rSub { size 8{2} } +"emf" rSub { size 8{1} }  - I rSub { size 8{2} } r rSub { size 8{1} }  - I rSub { size 8{1} } R rSub { size 8{1} } = - I rSub { size 8{2} }  \( R rSub { size 8{2} } +r rSub { size 8{1} }  \) +"emf" rSub { size 8{1} }  - I rSub { size 8{1} } R rSub { size 8{1} } =0} {}Substituting values from the circuit diagram for the resistances and emf, and canceling the ampere unit gives3I2+186I1=0.3I2+186I1=0. size 12{ - 3I rSub { size 8{2} } +"18" - 6I rSub { size 8{1} } =0} {}Now applying the loop rule to aefgha (we could have chosen abcdefgha as well) similarly gives
+I1R1+I3R3+I3r2emf2= +I1R1+I3R3+r2emf2=0.+I1R1+I3R3+I3r2emf2= +I1R1+I3R3+r2emf2=0. size 12{+I rSub { size 8{1} } R rSub { size 8{1} } +I rSub { size 8{3} } R rSub { size 8{3} } +I rSub { size 8{3} } r rSub { size 8{2} }  - "emf" rSub { size 8{2} } "=+"I rSub { size 8{1} } R rSub { size 8{1} } +I rSub { size 8{3} }  left (R rSub { size 8{3} } +r rSub { size 8{2} }  right ) - "emf" rSub { size 8{2} } =0} {}Note that the signs are reversed compared with the other loop, because elements are traversed in the opposite direction. With values entered, this becomes
+6I1+2I345=0.+6I1+2I345=0. size 12{+6I rSub { size 8{1} } +2I rSub { size 8{3} }  - "45"=0} {}These three equations are sufficient to solve for the three unknown currents. First, solve the second equation for I2I2 size 12{I rSub { size 8{2} } } {}:
I2=62I1.I2=62I1. size 12{I rSub { size 8{2} } =6 - 2I rSub { size 8{1} } } {}Now solve the third equation for I3I3 size 12{I rSub { size 8{3} } } {}:
I3=22.53I1.I3=22.53I1. size 12{I rSub { size 8{3} } ="22" "." 5 - 3I rSub { size 8{1} } } {}Substituting these two new equations into the first one allows us to find a value for I1I1 size 12{I rSub { size 8{1} } } {}:
I1=I2+I3=(62I1)+(22.5
3I1)=28.5
5I1.I1=I2+I3=(62I1)+(22.5
3I1)=28.5
5I1. size 12{I rSub { size 8{1} } =I rSub { size 8{2} } +I rSub { size 8{3} } = \( 6 - 2I rSub { size 8{1} }  \) + \( "22" "." 5 - 3I rSub { size 8{1} }  \) ="28" "." 5 - 5I rSub { size 8{1} } } {}Combining terms gives
6I1=28.5, and6I1=28.5, and size 12{6I rSub { size 8{1} } ="28" "." 5} {}I1=4.75 A.I1=4.75 A. size 12{I rSub { size 8{1} } =4 "." "75"" A"} {}Substituting this value for I1I1 size 12{I rSub { size 8{1} } } {} back into the fourth equation gives







I

2


=

6


2I


1




=

6

9.50













I

2


=

6


2I


1




=

6

9.50






 size 12{I rSub { size 8{2} } =6 - 2I rSub { size 8{1} } =6 - 9 "." "50"} {}
I2=3.50 A.I2=3.50 A. size 12{I rSub { size 8{2} } = - 3 "." "50"" A"} {}The minus sign means I2I2 size 12{I rSub { size 8{2} } } {} flows in the direction opposite to that assumed in [link].
Finally, substituting the value for I1I1 size 12{I rSub { size 8{1} } } {} into the fifth equation gives






I

3


=
22.5



3I

1



=
22.5

14

.
25










I

3


=
22.5



3I

1



=
22.5

14

.
25




 size 12{I rSub { size 8{3} } ="22" "." 5 - 3I rSub { size 8{1} } ="22" "." 5 - "14" "." "25"} {}
I3=8.25 A.I3=8.25 A. size 12{I rSub { size 8{3} } =8 "." "25"" A"} {}Discussion
Just as a check, we note that indeed I1=I2+I3I1=I2+I3 size 12{I rSub { size 8{1} } =I rSub { size 8{2} } +I rSub { size 8{3} } } {}. The results could also have been checked by entering all of the values into the equation for the abcdefgha loop.

Problem-Solving Strategies for Kirchhoffs Rules
Make certain there is a clear circuit diagram on which you can label all known and unknown resistances, emfs, and currents. If a current is unknown, you must assign it a direction. This is necessary for determining the signs of potential changes. If you assign the direction incorrectly, the current will be found to have a negative valueno harm done.
Apply the junction rule to any junction in the circuit. Each time the junction rule is applied, you should get an equation with a current that does not appear in a previous applicationif not, then the equation is redundant.
Apply the loop rule to as many loops as needed to solve for the unknowns in the problem. (There must be as many independent equations as unknowns.) To apply the loop rule, you must choose a direction to go around the loop. Then carefully and consistently determine the signs of the potential changes for each element using the four bulleted points discussed above in conjunction with [link].
Solve the simultaneous equations for the unknowns. This may involve many algebraic steps, requiring careful checking and rechecking.
Check to see whether the answers are reasonable and consistent. The numbers should be of the correct order of magnitude, neither exceedingly large nor vanishingly small. The signs should be reasonablefor example, no resistance should be negative. Check to see that the values obtained satisfy the various equations obtained from applying the rules. The currents should satisfy the junction rule, for example.

The material in this section is correct in theory. We should be able to verify it by making measurements of current and voltage. In fact, some of the devices used to make such measurements are straightforward applications of the principles covered so far and are explored in the next modules. As we shall see, a very basic, even profound, fact resultsmaking a measurement alters the quantity being measured.
Check Your Understanding

Can Kirchhoffs rules be applied to simple series and parallel circuits or are they restricted for use in more complicated circuits that are not combinations of series and parallel?



Kirchhoff's rules can be applied to any circuit since they are applications to circuits of two conservation laws. Conservation laws are the most broadly applicable principles in physics. It is usually mathematically simpler to use the rules for series and parallel in simpler circuits so we emphasize Kirchhoffs rules for use in more complicated situations. But the rules for series and parallel can be derived from Kirchhoffs rules. Moreover, Kirchhoffs rules can be expanded to devices other than resistors and emfs, such as capacitors, and are one of the basic analysis devices in circuit analysis.




Section Summary
Kirchhoffs rules can be used to analyze any circuit, simple or complex.
Kirchhoffs first rulethe junction rule: The sum of all currents entering a junction must equal the sum of all currents leaving the junction.
Kirchhoffs second rulethe loop rule: The algebraic sum of changes in potential around any closed circuit path (loop) must be zero.
The two rules are based, respectively, on the laws of conservation of charge and energy.
When calculating potential and current using Kirchhoffs rules, a set of conventions must be followed for determining the correct signs of various terms.
The simpler series and parallel rules are special cases of Kirchhoffs rules.

Conceptual Questions

Can all of the currents going into the junction in [link] be positive? Explain.






Apply the junction rule to junction b in [link]. Is any new information gained by applying the junction rule at e? (In the figure, each emf is represented by script E.)





(a) What is the potential difference going from point a to point b in [link]? (b) What is the potential difference going from c to b? (c) From e to g? (d) From e to d?


Apply the loop rule to loop afedcba in [link].


Apply the loop rule to loops abgefa and cbgedc in [link].


Problem Exercises
Apply the loop rule to loop abcdefgha in [link].









I

2






R

2


+

emf

1





 
 I


2






r

1


+

 
 I


3






R

3


+

 
 I


3







r

2


-

emf

2



=
 0











I

2






R

2


+

emf

1





 
 I


2






r

1


+

 
 I


3






R

3


+

 
 I


3







r

2


-

emf

2



=
 0




 size 12{ {underline  {-I rSub { size 8{2} } R rSub { size 8{3} } + "emf" rSub { size 8{1} } - ital " I" rSub { size 8{2} } r rSub { size 8{1} }  + ital " I" rSub { size 8{3} } r rSub { size 8{3} }  + ital " I" rSub { size 8{3} } r rSub { size 8{2} }   +-  "emf" rSub { size 8{2} } =" 0"}} } {}




Apply the loop rule to loop aedcba in [link].


Verify the second equation in [link] by substituting the values found for the currents I1I1 size 12{I rSub { size 8{1} } } {} and I2I2 size 12{I rSub { size 8{2} } } {}.

Verify the third equation in [link] by substituting the values found for the currents I1I1 size 12{I rSub { size 8{1} } } {} and I3I3 size 12{I rSub { size 8{3} } } {}.

Apply the junction rule at point a in [link].










I

3


=




 I

1




+


 I

2













I

3


=




 I

1




+


 I

2








 size 12{I rSub { size 8{3} }  = ital " I" rSub { size 8{1} }  + ital " I" rSub { size 8{2} } } {}




Apply the loop rule to loop abcdefghija in [link].


Apply the loop rule to loop akledcba in [link].








emf

2


-


 I

2






r

2


-


 I

2






R

2


+


 I
1





R

5


+

I

1






r

1


-

emf

1



+


 I
1





R

1


=
0











emf

2


-


 I

2






r

2


-


 I

2






R

2


+


 I
1





R

5


+

I

1






r

1


-

emf

1



+


 I
1





R

1


=
0





 size 12{ {underline  { "emf" rSub { size 8{2} }   +-  ital " I" rSub { size 8{2} } r rSub { size 8{2} }   +-  ital " I" rSub { size 8{2} } R rSub { size 8{2} }  + ital " I" rSub { size 8{1} } R rSub { size 8{5} }  +I rSub { size 8{1} } r rSub { size 8{1} }   +-  "emf" rSub { size 8{1} }  + ital " I" rSub { size 8{1} } R rSub { size 8{1} }  = 0}} } {}




Find the currents flowing in the circuit in [link]. Explicitly show how you follow the steps in the Problem-Solving Strategies for Series and Parallel Resistors.

Solve [link], but use loop abcdefgha instead of loop akledcba. Explicitly show how you follow the steps in the Problem-Solving Strategies for Series and Parallel Resistors.

(a) I1=4.75 AI1=4.75 A size 12{I rSub { size 8{1} }  =4 cdot "75 A"} {}(b) I2 =-3.5 AI2 =-3.5 A size 12{I rSub { size 8{"2 "} }  =  +- 3 "." "5 A"} {}{}(c) I3=8.25 AI3=8.25 A size 12{I rSub { size 8{3} } =8 "." "25"" A"} {}


Find the currents flowing in the circuit in [link].


Unreasonable Results
Consider the circuit in [link], and suppose that the emfs are unknown and the currents are given to be I1=5.00 AI1=5.00 A, I2=3.0 AI2=3.0 A size 12{I rSub { size 8{2} } =3 "." 0" A"} {}, and I3=2.00 AI3=2.00 A size 12{I rSub { size 8{3} } "=-"2 "." "00"" A"} {}. (a) Could you find the emfs? (b) What is wrong with the assumptions?




(a) No, you would get inconsistent equations to solve.
(b) I1I2+I3I1I2+I3 size 12{I rSub { size 8{1} }  <> I rSub { size 8{2} } +I rSub { size 8{3} } } {}. The assumed currents violate the junction rule.


Glossary
Kirchhoffs rules a set of two rules, based on conservation of charge and energy, governing current and changes in potential in an electric circuit
junction rule Kirchhoffs first rule, which applies the conservation of charge to a junction; current is the flow of charge; thus, whatever charge flows into the junction must flow out; the rule can be stated I1=I2+I3I1=I2+I3 size 12{I rSub { size 8{1} } =I rSub { size 8{2} } +I rSub { size 8{3} } } {}
loop rule Kirchhoffs second rule, which states that in a closed loop, whatever energy is supplied by emf must be transferred into other forms by devices in the loop, since there are no other ways in which energy can be transferred into or out of the circuit. Thus, the emf equals the sum of the IRIR size 12{ ital "IR"} {} (voltage) drops in the loop and can be stated: emf=Ir+IR1+IR2emf=Ir+IR1+IR2 size 12{"emf"= ital "Ir"+ ital "IR" rSub { size 8{1} } + ital "IR" rSub { size 8{2} } } {}
conservation laws require that energy and charge be conserved in a system

