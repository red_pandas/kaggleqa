
Kinetic Energy and the Work-Energy Theorem
Kinetic Energy and the Work-Energy Theorem
Explain work as a transfer of energy and net work as the work done by the net force.
Explain and apply the work-energy theorem.

Work Transfers Energy
What happens to the work done on a system? Energy is transferred into the system, but in what form? Does it remain in the system or move on? The answers depend on the situation. For example, if the lawn mower in [link](a) is pushed just hard enough to keep it going at a constant speed, then energy put into the mower by the person is removed continuously by friction, and eventually leaves the system in the form of heat transfer. In contrast, work done on the briefcase by the person carrying it up stairs in [link](d) is stored in the briefcase-Earth system and can be recovered at any time, as shown in [link](e). In fact, the building of the pyramids in ancient Egypt is an example of storing energy in a system by doing work on the system. Some of the energy imparted to the stone blocks in lifting them during construction of the pyramids remains in the stone-Earth system and has the potential to do work.In this section we begin the study of various types of work and forms of energy. We will find that some types of work leave the energy of a system constant, for example, whereas others change the system in some way, such as making it move. We will also develop definitions of important forms of energy, such as the energy of motion.
Net Work and the Work-Energy Theorem
We know from the study of Newtons laws in Dynamics: Force and Newton's Laws of Motion that net force causes acceleration. We will see in this section that work done by the net force gives a system energy of motion, and in the process we will also find an expression for the energy of motion.
Let us start by considering the total, or net, work done on a system. Net work is defined to be the sum of work done by all external forcesthat is, net work is the work done by the net external force FnetFnet size 12{F rSub { size 8{"net"} } } {}. In equation form, this is Wnet=FnetdcosWnet=Fnetdcos size 12{W rSub { size 8{"net"} } =F rSub { size 8{"net"} } d"cos"} {} where  size 12{} {} is the angle between the force vector and the displacement vector.[link](a) shows a graph of force versus displacement for the component of the force in the direction of the displacementthat is, an FcosFcos size 12{F"cos"} {} vs. dd size 12{d} {} graph. In this case, FcosFcos size 12{F"cos"} {} is constant. You can see that the area under the graph is FdcosFdcos size 12{F"cos"} {}, or the work done. [link](b) shows a more general process where the force varies. The area under the curve is divided into strips, each having an average force (Fcos)i(ave)(Fcos)i(ave) size 12{ \( F"cos" \)  rSub { size 8{i \( "ave" \) } } } {}. The work done is (Fcos)i(ave)di(Fcos)i(ave)di size 12{ \( F"cos" \)  rSub { size 8{i \( "ave" \) } } d rSub { size 8{i} } } {} for each strip, and the total work done is the sum of the WiWi size 12{W rSub { size 8{i} } } {}. Thus the total work done is the total area under the curve, a useful property to which we shall refer later.(a) A graph of FcosFcos vs. dd size 12{d} {}, when FcosFcos size 12{F"cos"} {} is constant. The area under the curve represents the work done by the force. (b) A graph of FcosFcos size 12{F"cos"q} {} vs. dd size 12{d} {} in which the force varies. The work done for each interval is the area of each strip; thus, the total area under the curve equals the total work done.


Net work will be simpler to examine if we consider a one-dimensional situation where a force is used to accelerate an object in a direction parallel to its initial velocity. Such a situation occurs for the package on the roller belt conveyor system shown in [link].A package on a roller belt is pushed horizontally through a distance 
d

d
. 


The force of gravity and the normal force acting on the package are perpendicular to the displacement and do no work. Moreover, they are also equal in magnitude and opposite in direction so they cancel in calculating the net force. The net force arises solely from the horizontal applied force FappFapp and the horizontal friction force ff. Thus, as expected, the net force is parallel to the displacement, so that =0=0 and cos=1cos=1 size 12{"cos"q=1} {}, and the net work is given byWnet=Fnetd.Wnet=Fnetd. size 12{W rSub { size 8{"net"} } =F rSub { size 8{"net"} } d} {}The effect of the net force FnetFnet size 12{F rSub { size 8{"net"} } } {} is to accelerate the package from v0v0 size 12{v rSub { size 8{0} } } {} to vv size 12{v} {}. The kinetic energy of the package increases, indicating that the net work done on the system is positive. (See [link].) By using Newtons second law, and doing some algebra, we can reach an interesting conclusion. Substituting Fnet=maFnet=ma size 12{F rSub { size 8{"net"} } = ital "ma"} {} from Newtons second law givesWnet=mad.Wnet=mad. size 12{W rSub { size 8{"net"} } = ital "mad"} {}
To get a relationship between net work and the speed given to a system by the net force acting on it, we take d=xx0d=xx0 size 12{d=x - x rSub { size 8{0} } } {} and use the equation studied in Motion Equations for Constant Acceleration in One Dimension for the change in speed over a distance dd if the acceleration has the constant value 
aa; namely, 
v2=v02+2adv2=v02+2ad (note that 
aa appears in the expression for the net work). Solving for acceleration gives 
a=v2v022da=v2v022d. When 
aa is substituted into the preceding expression for 
WnetWnet, we obtainWnet=mv2v022dd.Wnet=mv2v022dd.The dd size 12{d} {} cancels, and we rearrange this to obtain



W







net





=

1
2






mv

2






1
2




mv


0


2


.








W







net





=

1
2






mv

2






1
2




mv


0


2


.




 size 12{w"" lSub { size 8{ ital "net"} }  =  {  {1}  over  {2} }   ital "mv" rSup { size 8{2} }   -   {  {1}  over  {2} }   ital "mv""" lSub { size 8{0} } "" lSup { size 8{2} }  "." } {}


This expression is called the work-energy theorem, and it actually applies in general (even for forces that vary in direction and magnitude), although we have derived it for the special case of a constant force parallel to the displacement. The theorem implies that the net work on a system equals the change in the quantity 12mv212mv2 size 12{ {  {1}  over  {2} }  ital "mv" rSup { size 8{2} } } {}. This quantity is our first example of a form of energy.
The Work-Energy Theorem
The net work on a system equals the change in the quantity 12mv212mv2 size 12{ {  { size 8{1} }  over  { size 8{2} } }  ital "mv" rSup { size 8{2} } } {}.





W


net


=

1
2





mv

2






1
2


 
mv


0


2










W


net


=

1
2





mv

2






1
2


 
mv


0


2





 size 12{w"" lSub { size 8{ ital "net"} }  =  {  {1}  over  {2} }   ital "mv" rSup { size 8{2} }   -   {  {1}  over  {2} }   ital "mv""" lSub { size 8{0} } "" lSup { size 8{2} }  "." } {}


The quantity 12mv212mv2 size 12{ {  {1}  over  {2} }  ital "mv" rSup { size 8{2} } } {} in the work-energy theorem is defined to be the translational kinetic energy (KE) of a mass mm size 12{m} {} moving at a speed vv size 12{v} {}. (Translational kinetic energy is distinct from rotational kinetic energy, which is considered later.) In equation form, the translational kinetic energy,




KE
=

1
2





mv

2




,









KE
=

1
2





mv

2




,




 size 12{"KE"= {  {1}  over  {2} }  ital "mv" rSup { size 8{2} } ,} {}

is the energy associated with translational motion. Kinetic energy is a form of energy associated with the motion of a particle, single body, or system of objects moving together. We are aware that it takes energy to get an object, like a car or the package in [link], up to speed, but it may be a bit surprising that kinetic energy is proportional to speed squared. This proportionality means, for example, that a car traveling at 100 km/h has four times the kinetic energy it has at 50 km/h, helping to explain why high-speed collisions are so devastating. We will now consider a series of examples to illustrate various aspects of work and energy.

Calculating the Kinetic Energy of a Package
Suppose a 30.0-kg package on the roller belt conveyor system in [link] is moving at 0.500 m/s. What is its kinetic energy?
Strategy 
Because the mass mm and speed vv are given, the kinetic energy can be calculated from its definition as given in the equation KE=12mv2KE=12mv2 size 12{"KE"= {  {1}  over  {2} }  ital "mv" rSup { size 8{2} } } {}.Solution The kinetic energy is given by





KE
=

1
2





mv

2




.









KE
=

1
2





mv

2




.




 size 12{"KE"= {  {1}  over  {2} }  ital "mv" rSup { size 8{2} }  "." } {}

Entering known values gives





KE
=
0

.
5
(
30.0 kg
)
(
0.500 m/s

)

2


,









KE
=
0

.
5
(
30.0 kg
)
(
0.500 m/s

)

2


,




 size 12{"KE"=0 "." 5 \( "30" "." 0" kg" \)  \( 0 "." "500"" m/s" \)  rSup { size 8{2} } ,} {}

which yields




KE
=
3.75 kg



m

2





/s

2


=

3.75 J.








KE
=
3.75 kg



m

2





/s

2


=

3.75 J.




 size 12{"KE"=3 "." "75"`"kg" cdot m rSup { size 8{2} } "/s" rSup { size 8{2} } =3 "." "75"`J "." } {}

Discussion 
Note that the unit of kinetic energy is the joule, the same as the unit of work, as mentioned when work was first defined. It is also interesting that, although this is a fairly massive package, its kinetic energy is not large at this relatively low speed. This fact is consistent with the observation that people can move packages like this without exhausting themselves.
 Determining the Work to Accelerate a Package
Suppose that you push on the 30.0-kg package in [link] with a constant force of 120 N through a distance of 0.800 m, and that the opposing friction force averages 5.00 N.(a) Calculate the net work done on the package. (b) Solve the same problem as in part (a), this time by finding the work done by each force that contributes to the net force.Strategy and Concept for (a) This is a motion in one dimension problem, because the downward force (from the weight of the package) and the normal force have equal magnitude and opposite direction, so that they cancel in calculating the net force, while the applied force, friction, and the displacement are all horizontal. (See [link].) As expected, the net work is the net force times distance.
Solution for (a) The net force is the push force minus friction, or Fnet = 120 N  5.00 N = 115 NFnet = 120 N  5.00 N = 115 N size 12{F rSub { size 8{"net"} } " = 120 N  5" "." "00 N = 115 N"} {}. Thus the net work is





W

net


 =






F

net




d
=


115 N




0.800 m






 =

92.0 N

m
=
92.0 J.










W

net


 =






F

net




d
=


115 N




0.800 m






 =

92.0 N

m
=
92.0 J.




alignl { stack {
 size 12{W rSub { size 8{"net"} } =F rSub { size 8{"net"} } d= left ("115"`N right ) left (0 "." "800"`m right )}  {} # 
"          "="92" "." 0`N cdot m="92" "." 0`J "."  {} 
} } {}

Discussion for (a) This value is the net work done on the package. The person actually does more work than this, because friction opposes the motion. Friction does negative work and removes some of the energy the person expends and converts it to thermal energy. The net work equals the sum of the work done by each individual force.
Strategy and Concept for (b) The forces acting on the package are gravity, the normal force, the force of friction, and the applied force. The normal force and force of gravity are each perpendicular to the displacement, and therefore do no work. 
Solution for (b) The applied force does work.





W

app


 =






F

app



d
cos



0


=

F

app



d





 =




120 N



0.800 m




 =


 96.0 J












W

app


 =






F

app



d
cos



0


=

F

app



d





 =




120 N



0.800 m




 =


 96.0 J






alignl { stack {
 size 12{W rSub { size 8{"app"} } =F rSub { size 8{"app"} } d"cos" left (0 right )=F rSub { size 8{"app"} } d}  {} # 
"        "= left ("120 N" right ) left (0 "." "800"" m" right ) {} # 
"        "=" 96" "." "0 J" "."  {} 
} } {}

The friction force and displacement are in opposite directions, so that =180=180 size 12{="180"} {}, and the work done by friction is





W

fr


 =






F

fr



d
cos



180


=



F

fr




d





 =







5.00 N






0.800 m





 =



4.00 J.











W

fr


 =






F

fr



d
cos



180


=



F

fr




d





 =







5.00 N






0.800 m





 =



4.00 J.





alignl { stack {
 size 12{W rSub { size 8{"fr"} } =F rSub { size 8{"fr"} } d"cos" left ("180" right )= - F rSub { size 8{"fr"} } d}  {} # 
"       "= -  left (5 "." "00 N" right ) left (0 "." "800"" m" right ) {} # 
 ital "       "=  - 4 "." "00" J "."  {} 
} } {}

So the amounts of work done by gravity, by the normal force, by the applied force, and by friction are, respectively,





W

gr


 =




0,







W

N


 =



0,





W

app



 =


96.0 J,




W

fr


 =







4.00 J.












W

gr


 =




0,







W

N


 =



0,





W

app



 =


96.0 J,




W

fr


 =







4.00 J.






alignl { stack {
 size 12{W rSub { size 8{"gr"} } =0,}  {} # 
W rSub { size 8{N} } =0, {} # 
W rSub { size 8{"app"} } ="96" "." 0" J," {} # 
W rSub { size 8{"fr"} } = - 4 "." "00"" J" "."  {} 
} } {}

The total work done as the sum of the work done by each force is then seen to be 
Wtotal=Wgr+WN+Wapp+Wfr=92.0 J.Wtotal=Wgr+WN+Wapp+Wfr=92.0 J. size 12{W rSub { size 8{"total"} } =W rSub { size 8{"gr"} } +W rSub { size 8{N} } +W rSub { size 8{"app"} } +W rSub { size 8{"fr"} } ="92" "." 0" J"} {}Discussion for (b) The calculated total work WtotalWtotal size 12{W rSub { size 8{"total"} } } {} as the sum of the work by each force agrees, as expected, with the work WnetWnet size 12{W rSub { size 8{"net"} } } {} done by the net force. The work done by a collection of forces acting on an object can be calculated by either approach.

Determining Speed from Work and Energy 
Find the speed of the package in [link] at the end of the push, using work and energy concepts.
Strategy 
Here the work-energy theorem can be used, because we have just calculated the net work, WnetWnet size 12{W rSub { size 8{"net"} } } {}, and the initial kinetic energy, 





1
2




m

v
0


2









1
2




m

v
0


2





 size 12{ {  {1}  over  {2} }  ital "mv" rSub { size 8{0}   rSup { size 8{2} } } } {}. These calculations allow us to find the final kinetic energy, 12mv212mv2 size 12{ {  {1}  over  {2} }  ital "mv" rSup { size 8{2} } } {}, and thus the final speed vv size 12{v} {}.Solution 
The work-energy theorem in equation form is 






W

net


=

1
2






mv

2






1
2





m

v
0


2


.










W

net


=

1
2






mv

2






1
2





m

v
0


2


.




 size 12{W rSub { size 8{"net"} } = {  {1}  over  {2} }  ital "mv" rSup { size 8{2} }  -  {  {1}  over  {2} }  ital "mv" rSub { size 8{0}   rSup { size 8{2} } }  "." } {}

Solving for 12mv212mv2 size 12{ {  {1}  over  {2} }  ital "mv" rSup { size 8{2} } } {} gives




1
2



mv


2


=


W



net




+

1
2





m

v
0


2


.









1
2



mv


2


=


W



net




+

1
2





m

v
0


2


.




 size 12{ {  {1}  over  {2} }   ital "mv""" lSup { size 8{2} }  =w rSub { size 8{ ital "net"} }  +  {  {1}  over  {2} }  ital "mv""" lSub { size 8{0} } "" lSup { size 8{2} }  "." } {}

Thus,
12mv2=92.0 J+3.75 J=95.75 J.12mv2=92.0 J+3.75 J=95.75 J. size 12{ {  {1}  over  {2} }  ital "mv" rSup { size 8{2} } ="92" "." 0`J+3 "." "75"`J="95" "." "75"`J} {}Solving for the final speed as requested and entering known values gives




 v
 =








2

(95.75 J)


m



=



191.5 kg



m

2




/s

2




30.0 kg








 =


2.53 m/s.









 v
 =








2

(95.75 J)


m



=



191.5 kg



m

2




/s

2




30.0 kg








 =


2.53 m/s.






Discussion 
Using work and energy, we not only arrive at an answer, we see that the final kinetic energy is the sum of the initial kinetic energy and the net work done on the package. This means that the work indeed adds to the energy of the package.
Work and Energy Can Reveal Distance, Too
How far does the package in [link] coast after the push, assuming friction remains constant? Use work and energy considerations.
Strategy 
We know that once the person stops pushing, friction will bring the package to rest. In terms of energy, friction does negative work until it has removed all of the packages kinetic energy. The work done by friction is the force of friction times the distance traveled times the cosine of the angle between the friction force and displacement; hence, this gives us a way of finding the distance traveled after the person stops pushing.Solution 
The normal force and force of gravity cancel in calculating the net force. The horizontal friction force is then the net force, and it acts opposite to the displacement, so =180=180. To reduce the kinetic energy of the package to zero, the work WfrWfr by friction must be minus the kinetic energy that the package started with plus what the package accumulated due to the pushing. Thus Wfr=95.75 JWfr=95.75 J. Furthermore, Wfr=fdcos= fdWfr=fdcos= fd, where dd is the distance it takes to stop. Thus,d=Wfrf=95.75 J5.00 N,d=Wfrf=95.75 J5.00 N, size 12{ { {d}} sup { ' }= -  {  {W rSub { size 8{"fr"} } }  over  {f} } = -  {  { - "95" "." "75"`J}  over  {5 "." "00 N"} } } {}and so
d=19.2 m.d=19.2 m. size 12{ { {d}} sup { ' }="19" "." 2" m"} {}Discussion 
This is a reasonable distance for a package to coast on a relatively friction-free conveyor system. Note that the work done by friction is negative (the force is in the opposite direction of motion), so it removes the kinetic energy.

Some of the examples in this section can be solved without considering energy, but at the expense of missing out on gaining insights about what work and energy are doing in this situation. On the whole, solutions involving energy are generally shorter and easier than those using kinematics and dynamics alone.
Section SummaryThe net work WnetWnet is the work done by the net force acting on an object.
Work done on an object transfers energy to the object.
The translational kinetic energy of an object of mass mm moving at speed vv is KE=12mv2KE=12mv2 size 12{"KE"= {  {1}  over  {2} }  ital "mv" rSup { size 8{2} } } {}.
The work-energy theorem states that the net work WnetWnet size 12{W rSub { size 8{"net"} } } {} on a system changes its kinetic energy, Wnet=12mv212


m

v
0


2

Wnet=12mv212


m

v
0


2

.
    
Conceptual Questions
The person in [link] does work on the lawn mower. Under what conditions would the mower gain energy? Under what conditions would it lose energy?





Work done on a system puts energy into it. Work done by a system removes energy from it. Give an example for each statement. 


When solving for speed in [link], we kept only the positive root. Why? 


Problems & Exercises
Compare the kinetic energy of a 20,000-kg truck moving at 110 km/h with that of an 80.0-kg astronaut in orbit moving at 27,500 km/h.





1
/
250








1
/
250




 size 12{1/"250"} {}




(a) How fast must a 3000-kg elephant move to have the same kinetic energy as a 65.0-kg sprinter running at 10.0 m/s? (b) Discuss how the larger energies needed for the movement of larger animals would relate to metabolic rates.


Confirm the value given for the kinetic energy of an aircraft carrier in [link]. You will need to look up the definition of a nautical mile (1 knot = 1 nautical mile/h).




1
.

1


10

10




J








1
.

1


10

10




J




 size 12{1 "." 1 times "10" rSup { size 8{"10"} } " J"} {}




(a) Calculate the force needed to bring a 950-kg car to rest from a speed of 90.0 km/h in a distance of 120 m (a fairly typical distance for a non-panic stop). (b) Suppose instead the car hits a concrete abutment at full speed and is brought to a stop in 2.00 m. Calculate the force exerted on the car and compare it with the force found in part (a).


A cars bumper is designed to withstand a 4.0-km/h (1.1-m/s) collision with an immovable object without damage to the body of the car. The bumper cushions the shock by absorbing the force over a distance. Calculate the magnitude of the average force on a bumper that collapses 0.200 m while bringing a 900-kg car to rest from an initial speed of 1.1 m/s.





2
.

8


10

3



 N








2
.

8


10

3



 N




 size 12{2 "." 8 times "10" rSup { size 8{3} } " N"} {}




Boxing gloves are padded to lessen the force of a blow. (a) Calculate the force exerted by a boxing glove on an opponents face, if the glove and face compress 7.50 cm during a blow in which the 7.00-kg arm and glove are brought to rest from an initial speed of 10.0 m/s. (b) Calculate the force exerted by an identical blow in the gory old days when no gloves were used and the knuckles and face would compress only 2.00 cm. (c) Discuss the magnitude of the force with glove on. Does it seem high enough to cause damage even though it is lower than the force with no glove?


Using energy considerations, calculate the average force a 60.0-kg sprinter exerts backward on the track to accelerate from 2.00 to 8.00 m/s in a distance of 25.0 m, if he encounters a headwind that exerts an average force of 30.0 N against him.

102 N



Glossary
net work work done by the net force, or vector sum of all the forces, acting on an object
work-energy theorem the result, based on Newtons laws, that the net work done on an object is equal to its change in kinetic energy
kinetic energy the energy an object has by reason of its motion, equal to 12mv212mv2 size 12{ {  {1}  over  {2} }  ital "mv" rSup { size 8{2} } } {} for the translational (i.e., non-rotational) motion of an object of mass mm size 12{m} {} moving at speed vv size 12{v} {}

