
Introduction
Introduction




The diversity of life on Earth is the result of evolution, a continuous process that is still occurring. (credit wolf: modification of work by Gary Kramer, USFWS; credit coral: modification of work by William Harrigan, NOAA; credit river: modification of work by Vojtch Dostl; credit protozoa: modification of work by Sharon Franklin, Stephen Ausmus, USDA ARS; credit fish modification of work by Christian Mehlfhrer; credit mushroom, bee: modification of work by Cory Zanker; credit tree: modification of work by Joseph Kranak)


All species of living organismsfrom the bacteria on our skin, to the trees in our yards, to the birds outsideevolved at some point from a different species. Although it may seem that living things today stay much the same from generation to generation, that is not the case: evolution is ongoing. Evolution is the process through which the characteristics of species change and through which new species arise.
The theory of evolution is the unifying theory of biology, meaning it is the framework within which biologists ask questions about the living world. Its power is that it provides direction for predictions about living things that are borne out in experiment after experiment. The Ukrainian-born American geneticist Theodosius Dobzhansky famously wrote that nothing makes sense in biology except in the light of evolution.1 He meant that the principle that all life has evolved and diversified from a common ancestor is the foundation from which we understand all other questions in biology. This chapter will explain some of the mechanisms for evolutionary change and the kinds of questions that biologists can and have answered using evolutionary theory.
Footnotes1 Theodosius Dobzhansky. Biology, Molecular and Organismic. American Zoologist 4, no. 4 (1964): 449.
