
The Process of Breathing
The Process of BreathingBy the end of this section, you will be able to:

Describe the mechanisms that drive breathing
Discuss how pressure, volume, and resistance are related
List the steps involved in pulmonary ventilation
Discuss the physical factors related to breathing
Discuss the meaning of respiratory volume and capacities
Define respiratory rate
Outline the mechanisms behind the control of breathing
Describe the respiratory centers of the medulla oblongata
Describe the respiratory centers of the pons
Discuss factors that can influence the respiratory rate

Pulmonary ventilation is the act of breathing, which can be described as the movement of air into and out of the lungs. The major mechanisms that drive pulmonary ventilation are atmospheric pressure (Patm); the air pressure within the alveoli, called alveolar pressure (Palv); and the pressure within the pleural cavity, called intrapleural pressure (Pip).Mechanisms of Breathing
The alveolar and intrapleural pressures are dependent on certain physical features of the lung. However, the ability to breatheto have air enter the lungs during inspiration and air leave the lungs during expirationis dependent on the air pressure of the atmosphere and the air pressure within the lungs.
Pressure Relationships
Inspiration (or inhalation) and expiration (or exhalation) are dependent on the differences in pressure between the atmosphere and the lungs. In a gas, pressure is a force created by the movement of gas molecules that are confined. For example, a certain number of gas molecules in a two-liter container has more room than the same number of gas molecules in a one-liter container ([link]). In this case, the force exerted by the movement of the gas molecules against the walls of the two-liter container is lower than the force exerted by the gas molecules in the one-liter container. Therefore, the pressure is lower in the two-liter container and higher in the one-liter container. At a constant temperature, changing the volume occupied by the gas changes the pressure, as does changing the number of gas molecules. Boyles law describes the relationship between volume and pressure in a gas at a constant temperature. Boyle discovered that the pressure of a gas is inversely proportional to its volume: If volume increases, pressure decreases. Likewise, if volume decreases, pressure increases. Pressure and volume are inversely related (P = k/V). Therefore, the pressure in the one-liter container (one-half the volume of the two-liter container) would be twice the pressure in the two-liter container. Boyles law is expressed by the following formula:



P
1


V
1

=
P
2


V
2




P
1


V
1

=
P
2


V
2


In this formula, P1 represents the initial pressure and V1 represents the initial volume, whereas the final pressure and volume are represented by P2 and V2, respectively. If the two- and one-liter containers were connected by a tube and the volume of one of the containers were changed, then the gases would move from higher pressure (lower volume) to lower pressure (higher volume).
Boyle's Law In a gas, pressure increases as volume decreases.


Pulmonary ventilation is dependent on three types of pressure: atmospheric, intra-alveolar, and interpleural. Atmospheric pressure is the amount of force that is exerted by gases in the air surrounding any given surface, such as the body. Atmospheric pressure can be expressed in terms of the unit atmosphere, abbreviated atm, or in millimeters of mercury (mm Hg). One atm is equal to 760 mm Hg, which is the atmospheric pressure at sea level. Typically, for respiration, other pressure values are discussed in relation to atmospheric pressure. Therefore, negative pressure is pressure lower than the atmospheric pressure, whereas positive pressure is pressure that it is greater than the atmospheric pressure. A pressure that is equal to the atmospheric pressure is expressed as zero.
Intra-alveolar pressure is the pressure of the air within the alveoli, which changes during the different phases of breathing ([link]). Because the alveoli are connected to the atmosphere via the tubing of the airways (similar to the two- and one-liter containers in the example above), the interpulmonary pressure of the alveoli always equalizes with the atmospheric pressure.
Intrapulmonary and Intrapleural Pressure Relationships Alveolar pressure changes during the different phases of the cycle. It equalizes at 760 mm Hg but does not remain at 760 mm Hg.


Intrapleural pressure is the pressure of the air within the pleural cavity, between the visceral and parietal pleurae. Similar to intra-alveolar pressure, intrapleural pressure also changes during the different phases of breathing. However, due to certain characteristics of the lungs, the intrapleural pressure is always lower than, or negative to, the intra-alveolar pressure (and therefore also to atmospheric pressure). Although it fluctuates during inspiration and expiration, intrapleural pressure remains approximately 4 mm Hg throughout the breathing cycle.
Competing forces within the thorax cause the formation of the negative intrapleural pressure. One of these forces relates to the elasticity of the lungs themselveselastic tissue pulls the lungs inward, away from the thoracic wall. Surface tension of alveolar fluid, which is mostly water, also creates an inward pull of the lung tissue. This inward tension from the lungs is countered by opposing forces from the pleural fluid and thoracic wall. Surface tension within the pleural cavity pulls the lungs outward. Too much or too little pleural fluid would hinder the creation of the negative intrapleural pressure; therefore, the level must be closely monitored by the mesothelial cells and drained by the lymphatic system. Since the parietal pleura is attached to the thoracic wall, the natural elasticity of the chest wall opposes the inward pull of the lungs. Ultimately, the outward pull is slightly greater than the inward pull, creating the 4 mm Hg intrapleural pressure relative to the intra-alveolar pressure. Transpulmonary pressure is the difference between the intrapleural and intra-alveolar pressures, and it determines the size of the lungs. A higher transpulmonary pressure corresponds to a larger lung.

Physical Factors Affecting Ventilation
In addition to the differences in pressures, breathing is also dependent upon the contraction and relaxation of muscle fibers of both the diaphragm and thorax. The lungs themselves are passive during breathing, meaning they are not involved in creating the movement that helps inspiration and expiration. This is because of the adhesive nature of the pleural fluid, which allows the lungs to be pulled outward when the thoracic wall moves during inspiration. The recoil of the thoracic wall during expiration causes compression of the lungs. Contraction and relaxation of the diaphragm and intercostals muscles (found between the ribs) cause most of the pressure changes that result in inspiration and expiration. These muscle movements and subsequent pressure changes cause air to either rush in or be forced out of the lungs.
Other characteristics of the lungs influence the effort that must be expended to ventilate. Resistance is a force that slows motion, in this case, the flow of gases. The size of the airway is the primary factor affecting resistance. A small tubular diameter forces air through a smaller space, causing more collisions of air molecules with the walls of the airways. The following formula helps to describe the relationship between airway resistance and pressure changes:


F
=

P
/
R


F
=

P
/
R
As noted earlier, there is surface tension within the alveoli caused by water present in the lining of the alveoli. This surface tension tends to inhibit expansion of the alveoli. However, pulmonary surfactant secreted by type II alveolar cells mixes with that water and helps reduce this surface tension. Without pulmonary surfactant, the alveoli would collapse during expiration.
Thoracic wall compliance is the ability of the thoracic wall to stretch while under pressure. This can also affect the effort expended in the process of breathing. In order for inspiration to occur, the thoracic cavity must expand. The expansion of the thoracic cavity directly influences the capacity of the lungs to expand. If the tissues of the thoracic wall are not very compliant, it will be difficult to expand the thorax to increase the size of the lungs.


Pulmonary Ventilation
The difference in pressures drives pulmonary ventilation because air flows down a pressure gradient, that is, air flows from an area of higher pressure to an area of lower pressure. Air flows into the lungs largely due to a difference in pressure; atmospheric pressure is greater than intra-alveolar pressure, and intra-alveolar pressure is greater than intrapleural pressure. Air flows out of the lungs during expiration based on the same principle; pressure within the lungs becomes greater than the atmospheric pressure.
Pulmonary ventilation comprises two major steps: inspiration and expiration. Inspiration is the process that causes air to enter the lungs, and expiration is the process that causes air to leave the lungs ([link]). A respiratory cycle is one sequence of inspiration and expiration. In general, two muscle groups are used during normal inspiration: the diaphragm and the external intercostal muscles. Additional muscles can be used if a bigger breath is required. When the diaphragm contracts, it moves inferiorly toward the abdominal cavity, creating a larger thoracic cavity and more space for the lungs. Contraction of the external intercostal muscles moves the ribs upward and outward, causing the rib cage to expand, which increases the volume of the thoracic cavity. Due to the adhesive force of the pleural fluid, the expansion of the thoracic cavity forces the lungs to stretch and expand as well. This increase in volume leads to a decrease in intra-alveolar pressure, creating a pressure lower than atmospheric pressure. As a result, a pressure gradient is created that drives air into the lungs.
Inspiration and Expiration Inspiration and expiration occur due to the expansion and contraction of the thoracic cavity, respectively.


The process of normal expiration is passive, meaning that energy is not required to push air out of the lungs. Instead, the elasticity of the lung tissue causes the lung to recoil, as the diaphragm and intercostal muscles relax following inspiration. In turn, the thoracic cavity and lungs decrease in volume, causing an increase in interpulmonary pressure. The interpulmonary pressure rises above atmospheric pressure, creating a pressure gradient that causes air to leave the lungs.
There are different types, or modes, of breathing that require a slightly different process to allow inspiration and expiration. Quiet breathing, also known as eupnea, is a mode of breathing that occurs at rest and does not require the cognitive thought of the individual. During quiet breathing, the diaphragm and external intercostals must contract.
A deep breath, called diaphragmatic breathing, requires the diaphragm to contract. As the diaphragm relaxes, air passively leaves the lungs. A shallow breath, called costal breathing, requires contraction of the intercostal muscles. As the intercostal muscles relax, air passively leaves the lungs.
In contrast, forced breathing, also known as hyperpnea, is a mode of breathing that can occur during exercise or actions that require the active manipulation of breathing, such as singing. During forced breathing, inspiration and expiration both occur due to muscle contractions. In addition to the contraction of the diaphragm and intercostal muscles, other accessory muscles must also contract. During forced inspiration, muscles of the neck, including the scalenes, contract and lift the thoracic wall, increasing lung volume. During forced expiration, accessory muscles of the abdomen, including the obliques, contract, forcing abdominal organs upward against the diaphragm. This helps to push the diaphragm further into the thorax, pushing more air out. In addition, accessory muscles (primarily the internal intercostals) help to compress the rib cage, which also reduces the volume of the thoracic cavity.

Respiratory Volumes and Capacities
Respiratory volume is the term used for various volumes of air moved by or associated with the lungs at a given point in the respiratory cycle. There are four major types of respiratory volumes: tidal, residual, inspiratory reserve, and expiratory reserve ([link]). Tidal volume (TV) is the amount of air that normally enters the lungs during quiet breathing, which is about 500 milliliters. Expiratory reserve volume (ERV) is the amount of air you can forcefully exhale past a normal tidal expiration, up to 1200 milliliters for men. Inspiratory reserve volume (IRV) is produced by a deep inhalation, past a tidal inspiration. This is the extra volume that can be brought into the lungs during a forced inspiration. Residual volume (RV) is the air left in the lungs if you exhale as much air as possible. The residual volume makes breathing easier by preventing the alveoli from collapsing. Respiratory volume is dependent on a variety of factors, and measuring the different types of respiratory volumes can provide important clues about a persons respiratory health ([link]).
Respiratory Volumes and Capacities These two graphs show (a) respiratory volumes and (b) the combination of volumes that results in respiratory capacity.


Pulmonary Function Testing 


Respiratory capacity is the combination of two or more selected volumes, which further describes the amount of air in the lungs during a given time. For example, total lung capacity (TLC) is the sum of all of the lung volumes (TV, ERV, IRV, and RV), which represents the total amount of air a person can hold in the lungs after a forceful inhalation. TLC is about 6000 mL air for men, and about 4200 mL for women. Vital capacity (VC) is the amount of air a person can move into or out of his or her lungs, and is the sum of all of the volumes except residual volume (TV, ERV, and IRV), which is between 4000 and 5000 milliliters. Inspiratory capacity (IC) is the maximum amount of air that can be inhaled past a normal tidal expiration, is the sum of the tidal volume and inspiratory reserve volume. On the other hand, the functional residual capacity (FRC) is the amount of air that remains in the lung after a normal tidal expiration; it is the sum of expiratory reserve volume and residual volume (see [link]).




Watch this video to learn more about lung volumes and spirometers. Explain how spirometry test results can be used to diagnose respiratory diseases or determine the effectiveness of disease treatment.
In addition to the air that creates respiratory volumes, the respiratory system also contains anatomical dead space, which is air that is present in the airway that never reaches the alveoli and therefore never participates in gas exchange. Alveolar dead space involves air found within alveoli that are unable to function, such as those affected by disease or abnormal blood flow. Total dead space is the anatomical dead space and alveolar dead space together, and represents all of the air in the respiratory system that is not being used in the gas exchange process.

Respiratory Rate and Control of Ventilation
Breathing usually occurs without thought, although at times you can consciously control it, such as when you swim under water, sing a song, or blow bubbles. The respiratory rate is the total number of breaths, or respiratory cycles, that occur each minute. Respiratory rate can be an important indicator of disease, as the rate may increase or decrease during an illness or in a disease condition. The respiratory rate is controlled by the respiratory center located within the medulla oblongata in the brain, which responds primarily to changes in carbon dioxide, oxygen, and pH levels in the blood.
The normal respiratory rate of a child decreases from birth to adolescence. A child under 1 year of age has a normal respiratory rate between 30 and 60 breaths per minute, but by the time a child is about 10 years old, the normal rate is closer to 18 to 30. By adolescence, the normal respiratory rate is similar to that of adults, 12 to 18 breaths per minute.
Ventilation Control Centers
The control of ventilation is a complex interplay of multiple regions in the brain that signal the muscles used in pulmonary ventilation to contract ([link]). The result is typically a rhythmic, consistent ventilation rate that provides the body with sufficient amounts of oxygen, while adequately removing carbon dioxide.


Summary of Ventilation Regulation


System component
Function



Medullary respiratory renter
Sets the basic rhythm of breathing


Ventral respiratory group (VRG)
Generates the breathing rhythm and integrates data coming into the medulla


Dorsal respiratory group (DRG)
Integrates input from the stretch receptors and the chemoreceptors in the periphery


Pontine respiratory group (PRG)
Influences and modifies the medulla oblongatas functions


Aortic body
Monitors blood PCO2, PO2, and pH


Carotid body
Monitors blood PCO2, PO2, and pH


Hypothalamus
Monitors emotional state and body temperature


Cortical areas of the brain
Control voluntary breathing


Proprioceptors
Send impulses regarding joint and muscle movements


Pulmonary irritant reflexes
Protect the respiratory zones of the system from foreign material


Inflation reflex
Protects the lungs from over-inflating


Neurons that innervate the muscles of the respiratory system are responsible for controlling and regulating pulmonary ventilation. The major brain centers involved in pulmonary ventilation are the medulla oblongata and the pontine respiratory group ([link]).
Respiratory Centers of the Brain 


The medulla oblongata contains the dorsal respiratory group (DRG) and the ventral respiratory group (VRG). The DRG is involved in maintaining a constant breathing rhythm by stimulating the diaphragm and intercostal muscles to contract, resulting in inspiration. When activity in the DRG ceases, it no longer stimulates the diaphragm and intercostals to contract, allowing them to relax, resulting in expiration. The VRG is involved in forced breathing, as the neurons in the VRG stimulate the accessory muscles involved in forced breathing to contract, resulting in forced inspiration. The VRG also stimulates the accessory muscles involved in forced expiration to contract.
The second respiratory center of the brain is located within the pons, called the pontine respiratory group, and consists of the apneustic and pneumotaxic centers. The apneustic center is a double cluster of neuronal cell bodies that stimulate neurons in the DRG, controlling the depth of inspiration, particularly for deep breathing. The pneumotaxic center is a network of neurons that inhibits the activity of neurons in the DRG, allowing relaxation after inspiration, and thus controlling the overall rate.

Factors That Affect the Rate and Depth of RespirationThe respiratory rate and the depth of inspiration are regulated by the medulla oblongata and pons; however, these regions of the brain do so in response to systemic stimuli. It is a dose-response, positive-feedback relationship in which the greater the stimulus, the greater the response. Thus, increasing stimuli results in forced breathing. Multiple systemic factors are involved in stimulating the brain to produce pulmonary ventilation.
The major factor that stimulates the medulla oblongata and pons to produce respiration is surprisingly not oxygen concentration, but rather the concentration of carbon dioxide in the blood. As you recall, carbon dioxide is a waste product of cellular respiration and can be toxic. Concentrations of chemicals are sensed by chemoreceptors. A central chemoreceptor is one of the specialized receptors that are located in the brain and brainstem, whereas a peripheral chemoreceptor is one of the specialized receptors located in the carotid arteries and aortic arch. Concentration changes in certain substances, such as carbon dioxide or hydrogen ions, stimulate these receptors, which in turn signal the respiration centers of the brain. In the case of carbon dioxide, as the concentration of CO2 in the blood increases, it readily diffuses across the blood-brain barrier, where it collects in the extracellular fluid. As will be explained in more detail later, increased carbon dioxide levels lead to increased levels of hydrogen ions, decreasing pH. The increase in hydrogen ions in the brain triggers the central chemoreceptors to stimulate the respiratory centers to initiate contraction of the diaphragm and intercostal muscles. As a result, the rate and depth of respiration increase, allowing more carbon dioxide to be expelled, which brings more air into and out of the lungs promoting a reduction in the blood levels of carbon dioxide, and therefore hydrogen ions, in the blood. In contrast, low levels of carbon dioxide in the blood cause low levels of hydrogen ions in the brain, leading to a decrease in the rate and depth of pulmonary ventilation, producing shallow, slow breathing.
Another factor involved in influencing the respiratory activity of the brain is systemic arterial concentrations of hydrogen ions. Increasing carbon dioxide levels can lead to increased H+ levels, as mentioned above, as well as other metabolic activities, such as lactic acid accumulation after strenuous exercise. Peripheral chemoreceptors of the aortic arch and carotid arteries sense arterial levels of hydrogen ions. When peripheral chemoreceptors sense decreasing, or more acidic, pH levels, they stimulate an increase in ventilation to remove carbon dioxide from the blood at a quicker rate. Removal of carbon dioxide from the blood helps to reduce hydrogen ions, thus increasing systemic pH.
Blood levels of oxygen are also important in influencing respiratory rate. The peripheral chemoreceptors are responsible for sensing large changes in blood oxygen levels. If blood oxygen levels become quite lowabout 60 mm Hg or lessthen peripheral chemoreceptors stimulate an increase in respiratory activity. The chemoreceptors are only able to sense dissolved oxygen molecules, not the oxygen that is bound to hemoglobin. As you recall, the majority of oxygen is bound by hemoglobin; when dissolved levels of oxygen drop, hemoglobin releases oxygen. Therefore, a large drop in oxygen levels is required to stimulate the chemoreceptors of the aortic arch and carotid arteries.
The hypothalamus and other brain regions associated with the limbic system also play roles in influencing the regulation of breathing by interacting with the respiratory centers. The hypothalamus and other regions associated with the limbic system are involved in regulating respiration in response to emotions, pain, and temperature. For example, an increase in body temperature causes an increase in respiratory rate. Feeling excited or the fight-or-flight response will also result in an increase in respiratory rate.
Disorders of the
Respiratory System: Sleep Apnea
Sleep apnea is a chronic disorder that can occur in children or adults, and is characterized by the cessation of breathing during sleep. These episodes may last for several seconds or several minutes, and may differ in the frequency with which they are experienced. Sleep apnea leads to poor sleep, which is reflected in the symptoms of fatigue, evening napping, irritability, memory problems, and morning headaches. In addition, many individuals with sleep apnea experience a dry throat in the morning after waking from sleep, which may be due to excessive snoring.
There are two types of sleep apnea: obstructive sleep apnea and central sleep apnea. Obstructive sleep apnea is caused by an obstruction of the airway during sleep, which can occur at different points in the airway, depending on the underlying cause of the obstruction. For example, the tongue and throat muscles of some individuals with obstructive sleep apnea may relax excessively, causing the muscles to push into the airway. Another example is obesity, which is a known risk factor for sleep apnea, as excess adipose tissue in the neck region can push the soft tissues towards the lumen of the airway, causing the trachea to narrow.
In central sleep apnea, the respiratory centers of the brain do not respond properly to rising carbon dioxide levels and therefore do not stimulate the contraction of the diaphragm and intercostal muscles regularly. As a result, inspiration does not occur and breathing stops for a short period. In some cases, the cause of central sleep apnea is unknown. However, some medical conditions, such as stroke and congestive heart failure, may cause damage to the pons or medulla oblongata. In addition, some pharmacologic agents, such as morphine, can affect the respiratory centers, causing a decrease in the respiratory rate. The symptoms of central sleep apnea are similar to those of obstructive sleep apnea.
A diagnosis of sleep apnea is usually done during a sleep study, where the patient is monitored in a sleep laboratory for several nights. The patients blood oxygen levels, heart rate, respiratory rate, and blood pressure are monitored, as are brain activity and the volume of air that is inhaled and exhaled. Treatment of sleep apnea commonly includes the use of a device called a continuous positive airway pressure (CPAP) machine during sleep. The CPAP machine has a mask that covers the nose, or the nose and mouth, and forces air into the airway at regular intervals. This pressurized air can help to gently force the airway to remain open, allowing more normal ventilation to occur. Other treatments include lifestyle changes to decrease weight, eliminate alcohol and other sleep apneapromoting drugs, and changes in sleep position. In addition to these treatments, patients with central sleep apnea may need supplemental oxygen during sleep.


Chapter Review
Pulmonary ventilation is the process of breathing, which is driven by pressure differences between the lungs and the atmosphere. Atmospheric pressure is the force exerted by gases present in the atmosphere. The force exerted by gases within the alveoli is called intra-alveolar (intrapulmonary) pressure, whereas the force exerted by gases in the pleural cavity is called intrapleural pressure. Typically, intrapleural pressure is lower, or negative to, intra-alveolar pressure. The difference in pressure between intrapleural and intra-alveolar pressures is called transpulmonary pressure. In addition, intra-alveolar pressure will equalize with the atmospheric pressure. Pressure is determined by the volume of the space occupied by a gas and is influenced by resistance. Air flows when a pressure gradient is created, from a space of higher pressure to a space of lower pressure. Boyles law describes the relationship between volume and pressure. A gas is at lower pressure in a larger volume because the gas molecules have more space to in which to move. The same quantity of gas in a smaller volume results in gas molecules crowding together, producing increased pressure.
Resistance is created by inelastic surfaces, as well as the diameter of the airways. Resistance reduces the flow of gases. The surface tension of the alveoli also influences pressure, as it opposes the expansion of the alveoli. However, pulmonary surfactant helps to reduce the surface tension so that the alveoli do not collapse during expiration. The ability of the lungs to stretch, called lung compliance, also plays a role in gas flow. The more the lungs can stretch, the greater the potential volume of the lungs. The greater the volume of the lungs, the lower the air pressure within the lungs.
Pulmonary ventilation consists of the process of inspiration (or inhalation), where air enters the lungs, and expiration (or exhalation), where air leaves the lungs. During inspiration, the diaphragm and external intercostal muscles contract, causing the rib cage to expand and move outward, and expanding the thoracic cavity and lung volume. This creates a lower pressure within the lung than that of the atmosphere, causing air to be drawn into the lungs. During expiration, the diaphragm and intercostals relax, causing the thorax and lungs to recoil. The air pressure within the lungs increases to above the pressure of the atmosphere, causing air to be forced out of the lungs. However, during forced exhalation, the internal intercostals and abdominal muscles may be involved in forcing air out of the lungs.
Respiratory volume describes the amount of air in a given space within the lungs, or which can be moved by the lung, and is dependent on a variety of factors. Tidal volume refers to the amount of air that enters the lungs during quiet breathing, whereas inspiratory reserve volume is the amount of air that enters the lungs when a person inhales past the tidal volume. Expiratory reserve volume is the extra amount of air that can leave with forceful expiration, following tidal expiration. Residual volume is the amount of air that is left in the lungs after expelling the expiratory reserve volume. Respiratory capacity is the combination of two or more volumes. Anatomical dead space refers to the air within the respiratory structures that never participates in gas exchange, because it does not reach functional alveoli. Respiratory rate is the number of breaths taken per minute, which may change during certain diseases or conditions.
Both respiratory rate and depth are controlled by the respiratory centers of the brain, which are stimulated by factors such as chemical and pH changes in the blood. These changes are sensed by central chemoreceptors, which are located in the brain, and peripheral chemoreceptors, which are located in the aortic arch and carotid arteries. A rise in carbon dioxide or a decline in oxygen levels in the blood stimulates an increase in respiratory rate and depth.

Interactive Link Questions

Watch this video to learn more about lung volumes and spirometers. Explain how spirometry test results can be used to diagnose respiratory diseases or determine the effectiveness of disease treatment.

Patients with respiratory ailments (such as asthma, emphysema, COPD, etc.) have issues with airway resistance and/or lung compliance. Both of these factors can interfere with the patients ability to move air effectively. A spirometry test can determine how much air the patient can move into and out of the lungs. If the air volumes are low, this can indicate that the patient has a respiratory disease or that the treatment regimen may need to be adjusted. If the numbers are normal, the patient does not have a significant respiratory disease or the treatment regimen is working as expected.


Review Questions

Which of the following processes does atmospheric pressure play a role in?

pulmonary ventilation
production of pulmonary surfactant
resistance
surface tension



A



A decrease in volume leads to a(n) ________ pressure.

decrease in
equalization of
increase in
zero



C



The pressure difference between the intra-alveolar and intrapleural pressures is called ________.

atmospheric pressure
pulmonary pressure
negative pressure
transpulmonary pressure



D



Gas flow decreases as ________ increases.

resistance
pressure
airway diameter
friction



A



Contraction of the external intercostal muscles causes which of the following to occur?

The diaphragm moves downward.
The rib cage is compressed.
The thoracic cavity volume decreases.
The ribs and sternum move upward.



D



Which of the following prevents the alveoli from collapsing?

residual volume
tidal volume
expiratory reserve volume
inspiratory reserve volume



A



Critical Thinking Questions
Describe what is meant by the term lung compliance.

Lung compliance refers to the ability of lung tissue to stretch under pressure, which is determined in part by the surface tension of the alveoli and the ability of the connective tissue to stretch. Lung compliance plays a role in determining how much the lungs can change in volume, which in turn helps to determine pressure and air movement.



Outline the steps involved in quiet breathing.

Quiet breathing occurs at rest and without active thought. During quiet breathing, the diaphragm and external intercostal muscles work at different extents, depending on the situation. For inspiration, the diaphragm contracts, causing the diaphragm to flatten and drop towards the abdominal cavity, helping to expand the thoracic cavity. The external intercostal muscles contract as well, causing the rib cage to expand, and the rib cage and sternum to move outward, also expanding the thoracic cavity. Expansion of the thoracic cavity also causes the lungs to expand, due to the adhesiveness of the pleural fluid. As a result, the pressure within the lungs drops below that of the atmosphere, causing air to rush into the lungs. In contrast, expiration is a passive process. As the diaphragm and intercostal muscles relax, the lungs and thoracic tissues recoil, and the volume of the lungs decreases. This causes the pressure within the lungs to increase above that of the atmosphere, causing air to leave the lungs.



What is respiratory rate and how is it controlled?

Respiratory rate is defined as the number of breaths taken per minute. Respiratory rate is controlled by the respiratory center, located in the medulla oblongata. Conscious thought can alter the normal respiratory rate through control by skeletal muscle, although one cannot consciously stop the rate altogether. A typical resting respiratory rate is about 14 breaths per minute.



Glossary
alveolar dead space  air space within alveoli that are unable to participate in gas exchange
anatomical dead space  air space present in the airway that never reaches the alveoli and therefore never participates in gas exchange
apneustic center  network of neurons within the pons that stimulate the neurons in the dorsal respiratory group; controls the depth of inspiration
atmospheric pressure  amount of force that is exerted by gases in the air surrounding any given surface
Boyles law  relationship between volume and pressure as described by the formula: P1V1 = P2V2
central chemoreceptor  one of the specialized receptors that are located in the brain that sense changes in hydrogen ion, oxygen, or carbon dioxide concentrations in the brain
dorsal respiratory group (DRG)  region of the medulla oblongata that stimulates the contraction of the diaphragm and intercostal muscles to induce inspiration
expiration (also, exhalation) process that causes the air to leave the lungs
expiratory reserve volume (ERV)  amount of air that can be forcefully exhaled after a normal tidal exhalation
forced breathing  (also, hyperpnea) mode of breathing that occurs during exercise or by active thought that requires muscle contraction for both inspiration and expiration
functional residual capacity (FRC)  sum of ERV and RV, which is the amount of air that remains in the lungs after a tidal expiration
inspiration (also, inhalation) process that causes air to enter the lungs
inspiratory capacity (IC)  sum of the TV and IRV, which is the amount of air that can maximally be inhaled past a tidal expiration
inspiratory reserve volume (IRV)  amount of air that enters the lungs due to deep inhalation past the tidal volume
intra-alveolar pressure  (intrapulmonary pressure) pressure of the air within the alveoli
intrapleural pressure  pressure of the air within the pleural cavity
peripheral chemoreceptor  one of the specialized receptors located in the aortic arch and carotid arteries that sense changes in pH, carbon dioxide, or oxygen blood levels
pneumotaxic center  network of neurons within the pons that inhibit the activity of the neurons in the dorsal respiratory group; controls rate of breathing
pulmonary ventilation  exchange of gases between the lungs and the atmosphere; breathing
quiet breathing  (also, eupnea) mode of breathing that occurs at rest and does not require the cognitive thought of the individual
residual volume (RV)  amount of air that remains in the lungs after maximum exhalation
respiratory cycle  one sequence of inspiration and expiration
respiratory rate  total number of breaths taken each minute
respiratory volume  varying amounts of air within the lung at a given time
thoracic wall compliance  ability of the thoracic wall to stretch while under pressure
tidal volume (TV)  amount of air that normally enters the lungs during quiet breathing
total dead space  sum of the anatomical dead space and alveolar dead space
total lung capacity (TLC)  total amount of air that can be held in the lungs; sum of TV, ERV, IRV, and RV
transpulmonary pressure  pressure difference between the intrapleural and intra-alveolar pressures
ventral respiratory group (VRG)  region of the medulla oblongata that stimulates the contraction of the accessory muscles involved in respiration to induce forced inspiration and expiration
vital capacity (VC)  sum of TV, ERV, and IRV, which is all the volumes that participate in gas exchange

