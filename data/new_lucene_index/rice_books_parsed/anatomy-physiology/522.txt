
Development of the Heart
Development of the HeartBy the end of this section, you will be able to:

Describe the embryological development of heart structures
Identify five regions of the fetal heart
Relate fetal heart structures to adult counterparts

The human heart is the first functional organ to develop. It begins beating and pumping blood around day 21 or 22, a mere three weeks after fertilization. This emphasizes the critical nature of the heart in distributing blood through the vessels and the vital exchange of nutrients, oxygen, and wastes both to and from the developing baby. The critical early development of the heart is reflected by the prominent heart bulge that appears on the anterior surface of the embryo.
The heart forms from an embryonic tissue called mesoderm around 18 to 19 days after fertilization. Mesoderm is one of the three primary germ layers that differentiates early in development that collectively gives rise to all subsequent tissues and organs. The heart begins to develop near the head of the embryo in a region known as the cardiogenic area. Following chemical signals called factors from the underlying endoderm (another of the three primary germ layers), the cardiogenic area begins to form two strands called the cardiogenic cords ([link]). As the cardiogenic cords develop, a lumen rapidly develops within them. At this point, they are referred to as endocardial tubes. The two tubes migrate together and fuse to form a single primitive heart tube. The primitive heart tube quickly forms five distinct regions. From head to tail, these include the truncus arteriosus, bulbus cordis, primitive ventricle, primitive atrium, and the sinus venosus. Initially, all venous blood flows into the sinus venosus, and contractions propel the blood from tail to head, or from the sinus venosus to the truncus arteriosus. This is a very different pattern from that of an adult.
Development of the Human Heart This diagram outlines the embryological development of the human heart during the first eight weeks and the subsequent formation of the four heart chambers.


The five regions of the primitive heart tube develop into recognizable structures in a fully developed heart. The truncus arteriosus will eventually divide and give rise to the ascending aorta and pulmonary trunk. The bulbus cordis develops into the right ventricle. The primitive ventricle forms the left ventricle. The primitive atrium becomes the anterior portions of both the right and left atria, and the two auricles. The sinus venosus develops into the posterior portion of the right atrium, the SA node, and the coronary sinus.
As the primitive heart tube elongates, it begins to fold within the pericardium, eventually forming an S shape, which places the chambers and major vessels into an alignment similar to the adult heart. This process occurs between days 23 and 28. The remainder of the heart development pattern includes development of septa and valves, and remodeling of the actual chambers. Partitioning of the atria and ventricles by the interatrial septum, interventricular septum, and atrioventricular septum is complete by the end of the fifth week, although the fetal blood shunts remain until birth or shortly after. The atrioventricular valves form between weeks five and eight, and the semilunar valves form between weeks five and nine.
Chapter Review
The heart is the first organ to form and become functional, emphasizing the importance of transport of material to and from the developing infant. It originates about day 18 or 19 from the mesoderm and begins beating and pumping blood about day 21 or 22. It forms from the cardiogenic region near the head and is visible as a prominent heart bulge on the surface of the embryo. Originally, it consists of a pair of strands called cardiogenic cords that quickly form a hollow lumen and are referred to as endocardial tubes. These then fuse into a single heart tube and differentiate into the truncus arteriosus, bulbus cordis, primitive ventricle, primitive atrium, and sinus venosus, starting about day 22. The primitive heart begins to form an S shape within the pericardium between days 23 and 28. The internal septa begin to form about day 28, separating the heart into the atria and ventricles, although the foramen ovale persists until shortly after birth. Between weeks five and eight, the atrioventricular valves form. The semilunar valves form between weeks five and nine.

Review Questions

The earliest organ to form and begin function within the developing human is the ________.

brain
stomach
lungs
heart



D



Of the three germ layers that give rise to all adult tissues and organs, which gives rise to the heart?

ectoderm
endoderm
mesoderm
placenta



C



The two tubes that eventually fuse to form the heart are referred to as the ________.

primitive heart tubes
endocardial tubes
cardiogenic region
cardiogenic tubes



D



Which primitive area of the heart will give rise to the right ventricle?

bulbus cordis
primitive ventricle
sinus venosus
truncus arteriosus



A



The pulmonary trunk and aorta are derived from which primitive heart structure?

bulbus cordis
primitive ventricle
sinus venosus
truncus arteriosus



D



Critical Thinking Questions

Why is it so important for the human heart to develop early and begin functioning within the developing embryo?

The human embryo is rapidly growing and has great demands for nutrients and oxygen, while producing waste products including carbon dioxide. All of these materials must be received from or delivered to the mother for processing. Without an efficient early circulatory system, this would be impossible.



Describe how the major pumping chambers, the ventricles, form within the developing heart.

After fusion of the two endocardial tubes into the single primitive heart, five regions quickly become visible. From the head, these are the truncus arteriosus, bulbus cordis, primitive ventricle, primitive atrium, and sinus venosus. Contractions propel the blood from the sinus venosus to the truncus arteriosus. About day 23, the heart begins to form an S-shaped structure within the pericardium. The bulbus cordis develops into the right ventricle, whereas the primitive ventricle becomes the left ventricle. The interventricular septum separating these begins to form about day 28. The atrioventricular valves form between weeks five to eight. At this point, the heart ventricles resemble the adult structure.



Glossary
bulbus cordis portion of the primitive heart tube that will eventually develop into the right ventricle
cardiogenic area area near the head of the embryo where the heart begins to develop 1819 days after fertilization
cardiogenic cords two strands of tissue that form within the cardiogenic area
endocardial tubes stage in which lumens form within the expanding cardiogenic cords, forming hollow structures
heart bulge prominent feature on the anterior surface of the heart, reflecting early cardiac development
mesoderm one of the three primary germ layers that differentiate early in embryonic development
primitive atrium portion of the primitive heart tube that eventually becomes the anterior portions of both the right and left atria, and the two auricles
primitive heart tube singular tubular structure that forms from the fusion of the two endocardial tubes
primitive ventricle portion of the primitive heart tube that eventually forms the left ventricle
sinus venosus develops into the posterior portion of the right atrium, the SA node, and the coronary sinus
truncus arteriosus portion of the primitive heart that will eventually divide and give rise to the ascending aorta and pulmonary trunk

