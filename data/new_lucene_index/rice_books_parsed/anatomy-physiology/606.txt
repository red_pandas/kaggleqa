
An Overview of the Endocrine System
An Overview of the Endocrine SystemBy the end of this section, you will be able to:

Distinguish the types of intercellular communication, their importance, mechanisms, and effects
Identify the major organs and tissues of the endocrine system and their location in the body

Communication is a process in which a sender transmits signals to one or more receivers to control and coordinate actions. In the human body, two major organ systems participate in relatively long distance communication: the nervous system and the endocrine system. Together, these two systems are primarily responsible for maintaining homeostasis in the body.
Neural and Endocrine Signaling
The nervous system uses two types of intercellular communicationelectrical and chemical signalingeither by the direct action of an electrical potential, or in the latter case, through the action of chemical neurotransmitters such as serotonin or norepinephrine. Neurotransmitters act locally and rapidly. When an electrical signal in the form of an action potential arrives at the synaptic terminal, they diffuse across the synaptic cleft (the gap between a sending neuron and a receiving neuron or muscle cell). Once the neurotransmitters interact (bind) with receptors on the receiving (post-synaptic) cell, the receptor stimulation is transduced into a response such as continued electrical signaling or modification of cellular response. The target cell responds within milliseconds of receiving the chemical message; this response then ceases very quickly once the neural signaling ends. In this way, neural communication enables body functions that involve quick, brief actions, such as movement, sensation, and cognition.In contrast, the endocrine system uses just one method of communication: chemical signaling. These signals are sent by the endocrine organs, which secrete chemicalsthe hormoneinto the extracellular fluid. Hormones are transported primarily via the bloodstream throughout the body, where they bind to receptors on target cells, inducing a characteristic response. As a result, endocrine signaling requires more time than neural signaling to prompt a response in target cells, though the precise amount of time varies with different hormones. For example, the hormones released when you are confronted with a dangerous or frightening situation, called the fight-or-flight response, occur by the release of adrenal hormonesepinephrine and norepinephrinewithin seconds. In contrast, it may take up to 48 hours for target cells to respond to certain reproductive hormones.




Visit this link to watch an animation of the events that occur when a hormone binds to a cell membrane receptor. What is the secondary messenger made by adenylyl cyclase during the activation of liver cells by epinephrine?

In addition, endocrine signaling is typically less specific than neural signaling. The same hormone may play a role in a variety of different physiological processes depending on the target cells involved. For example, the hormone oxytocin promotes uterine contractions in women in labor. It is also important in breastfeeding, and may be involved in the sexual response and in feelings of emotional attachment in both males and females.
In general, the nervous system involves quick responses to rapid changes in the external environment, and the endocrine system is usually slower actingtaking care of the internal environment of the body, maintaining homeostasis, and controlling reproduction ([link]). So how does the fight-or-flight response that was mentioned earlier happen so quickly if hormones are usually slower acting? It is because the two systems are connected. It is the fast action of the nervous system in response to the danger in the environment that stimulates the adrenal glands to secrete their hormones. As a result, the nervous system can cause rapid endocrine responses to keep up with sudden changes in both the external and internal environments when necessary.

Endocrine and Nervous Systems



Endocrine system
Nervous system



Signaling mechanism(s)
Chemical
Chemical/electrical


Primary chemical signal
Hormones
Neurotransmitters


Distance traveled
Long or short
Always short


Response time
Fast or slow
Always fast


Environment targeted
Internal
Internal and external



Structures of the Endocrine SystemThe endocrine system consists of cells, tissues, and organs that secrete hormones as a primary or secondary function. The endocrine gland is the major player in this system. The primary function of these ductless glands is to secrete their hormones directly into the surrounding fluid. The interstitial fluid and the blood vessels then transport the hormones throughout the body. The endocrine system includes the pituitary, thyroid, parathyroid, adrenal, and pineal glands ([link]). Some of these glands have both endocrine and non-endocrine functions. For example, the pancreas contains cells that function in digestion as well as cells that secrete the hormones insulin and glucagon, which regulate blood glucose levels. The hypothalamus, thymus, heart, kidneys, stomach, small intestine, liver, skin, female ovaries, and male testes are other organs that contain cells with endocrine function. Moreover, adipose tissue has long been known to produce hormones, and recent research has revealed that even bone tissue has endocrine functions.Endocrine System Endocrine glands and cells are located throughout the body and play an important role in homeostasis.


The ductless endocrine glands are not to be confused with the bodys exocrine system, whose glands release their secretions through ducts. Examples of exocrine glands include the sebaceous and sweat glands of the skin. As just noted, the pancreas also has an exocrine function: most of its cells secrete pancreatic juice through the pancreatic and accessory ducts to the lumen of the small intestine.
Other Types of Chemical Signaling
In endocrine signaling, hormones secreted into the extracellular fluid diffuse into the blood or lymph, and can then travel great distances throughout the body. In contrast, autocrine signaling takes place within the same cell. An autocrine (auto- = self) is a chemical that elicits a response in the same cell that secreted it. Interleukin-1, or IL-1, is a signaling molecule that plays an important role in inflammatory response. The cells that secrete IL-1 have receptors on their cell surface that bind these molecules, resulting in autocrine signaling.
Local intercellular communication is the province of the paracrine, also called a paracrine factor, which is a chemical that induces a response in neighboring cells. Although paracrines may enter the bloodstream, their concentration is generally too low to elicit a response from distant tissues. A familiar example to those with asthma is histamine, a paracrine that is released by immune cells in the bronchial tree. Histamine causes the smooth muscle cells of the bronchi to constrict, narrowing the airways. Another example is the neurotransmitters of the nervous system, which act only locally within the synaptic cleft.
Career Connections
Endocrinologist
Endocrinology is a specialty in the field of medicine that focuses on the treatment of endocrine system disorders. Endocrinologistsmedical doctors who specialize in this fieldare experts in treating diseases associated with hormonal systems, ranging from thyroid disease to diabetes mellitus. Endocrine surgeons treat endocrine disease through the removal, or resection, of the affected endocrine gland.
Patients who are referred to endocrinologists may have signs and symptoms or blood test results that suggest excessive or impaired functioning of an endocrine gland or endocrine cells. The endocrinologist may order additional blood tests to determine whether the patients hormonal levels are abnormal, or they may stimulate or suppress the function of the suspect endocrine gland and then have blood taken for analysis. Treatment varies according to the diagnosis. Some endocrine disorders, such as type 2 diabetes, may respond to lifestyle changes such as modest weight loss, adoption of a healthy diet, and regular physical activity. Other disorders may require medication, such as hormone replacement, and routine monitoring by the endocrinologist. These include disorders of the pituitary gland that can affect growth and disorders of the thyroid gland that can result in a variety of metabolic problems.
Some patients experience health problems as a result of the normal decline in hormones that can accompany aging. These patients can consult with an endocrinologist to weigh the risks and benefits of hormone replacement therapy intended to boost their natural levels of reproductive hormones.
In addition to treating patients, endocrinologists may be involved in research to improve the understanding of endocrine system disorders and develop new treatments for these diseases.

Chapter Review
The endocrine system consists of cells, tissues, and organs that secrete hormones critical to homeostasis. The body coordinates its functions through two major types of communication: neural and endocrine. Neural communication includes both electrical and chemical signaling between neurons and target cells. Endocrine communication involves chemical signaling via the release of hormones into the extracellular fluid. From there, hormones diffuse into the bloodstream and may travel to distant body regions, where they elicit a response in target cells. Endocrine glands are ductless glands that secrete hormones. Many organs of the body with other primary functionssuch as the heart, stomach, and kidneysalso have hormone-secreting cells.

Interactive Link Questions


Visit this link to watch an animation of the events that occur when a hormone binds to a cell membrane receptor. What is the secondary messenger made by adenylyl cyclase during the activation of liver cells by epinephrine?

cAMP



Review Questions

Endocrine glands ________.
secrete hormones that travel through a duct to the target organs
release neurotransmitters into the synaptic cleft
secrete chemical messengers that travel in the bloodstream
include sebaceous glands and sweat glands


C



Chemical signaling that affects neighboring cells is called ________.

autocrine
paracrine
endocrine
neuron



B



Critical Thinking Questions

Describe several main differences in the communication methods used by the endocrine system and the nervous system.

The endocrine system uses chemical signals called hormones to convey information from one part of the body to a distant part of the body. Hormones are released from the endocrine cell into the extracellular environment, but then travel in the bloodstream to target tissues. This communication and response can take seconds to days. In contrast, neurons transmit electrical signals along their axons. At the axon terminal, the electrical signal prompts the release of a chemical signal called a neurotransmitter that carries the message across the synaptic cleft to elicit a response in the neighboring cell. This method of communication is nearly instantaneous, of very brief duration, and is highly specific.



Compare and contrast endocrine and exocrine glands.
Endocrine glands are ductless. They release their secretion into the surrounding fluid, from which it enters the bloodstream or lymph to travel to distant cells. Moreover, the secretions of endocrine glands are hormones. Exocrine glands release their secretions through a duct that delivers the secretion to the target location. Moreover, the secretions of exocrine glands are not hormones, but compounds that have an immediate physiologic function. For example, pancreatic juice contains enzymes that help digest food.



True or false: Neurotransmitters are a special class of paracrines. Explain your answer.

True. Neurotransmitters can be classified as paracrines because, upon their release from a neurons axon terminals, they travel across a microscopically small cleft to exert their effect on a nearby neuron or muscle cell.



Glossary
autocrine chemical signal that elicits a response in the same cell that secreted it
endocrine gland tissue or organ that secretes hormones into the blood and lymph without ducts such that they may be transported to organs distant from the site of secretion
endocrine system cells, tissues, and organs that secrete hormones as a primary or secondary function and play an integral role in normal bodily processes
exocrine system cells, tissues, and organs that secrete substances directly to target tissues via glandular ducts
hormone secretion of an endocrine organ that travels via the bloodstream or lymphatics to induce a response in target cells or tissues in another part of the body
paracrine chemical signal that elicits a response in neighboring cells; also called paracrine factor

