
Types of Tissues
Types of TissuesBy the end of this section, you will be able to:

Identify the four main tissue types
Discuss the functions of each tissue type
Relate the structure of each tissue type to their function
Discuss the embryonic origin of tissue
Identify the three major germ layers
Identify the main types of tissue membranes

The term tissue is used to describe a group of cells found together in the body. The cells within a tissue share a common embryonic origin. Microscopic observation reveals that the cells in a tissue share morphological features and are arranged in an orderly pattern that achieves the tissues functions. From the evolutionary perspective, tissues appear in more complex organisms. For example, multicellular protists, ancient eukaryotes, do not have cells organized into tissues.
Although there are many types of cells in the human body, they are organized into four broad categories of tissues: epithelial, connective, muscle, and nervous. Each of these categories is characterized by specific functions that contribute to the overall health and maintenance of the body. A disruption of the structure is a sign of injury or disease. Such changes can be detected through histology, the microscopic study of tissue appearance, organization, and function.
The Four Types of Tissues
Epithelial tissue, also referred to as epithelium, refers to the sheets of cells that cover exterior surfaces of the body, lines internal cavities and passageways, and forms certain glands. Connective tissue, as its name implies, binds the cells and organs of the body together and functions in the protection, support, and integration of all parts of the body. Muscle tissue is excitable, responding to stimulation and contracting to provide movement, and occurs as three major types: skeletal (voluntary) muscle, smooth muscle, and cardiac muscle in the heart. Nervous tissue is also excitable, allowing the propagation of electrochemical signals in the form of nerve impulses that communicate between different regions of the body ([link]).
The next level of organization is the organ, where several types of tissues come together to form a working unit. Just as knowing the structure and function of cells helps you in your study of tissues, knowledge of tissues will help you understand how organs function. The epithelial and connective tissues are discussed in detail in this chapter. Muscle and nervous tissues will be discussed only briefly in this chapter.
Four Types of Tissue: BodyThe four types of tissues are exemplified in nervous tissue, stratified squamous epithelial tissue, cardiac muscle tissue, and connective tissue in small intestine.  Clockwise from nervous tissue, LM  872, LM  282, LM  460, LM  800. (Micrographs provided by the Regents of University of Michigan Medical School  2012)



Embryonic Origin of TissuesThe zygote, or fertilized egg, is a single cell formed by the fusion of an egg and sperm. After fertilization the zygote gives rise to rapid mitotic cycles, generating many cells to form the embryo. The first embryonic cells generated have the ability to differentiate into any type of cell in the body and, as such, are called totipotent, meaning each has the capacity to divide, differentiate, and develop into a new organism. As cell proliferation progresses, three major cell lineages are established within the embryo. Each of these lineages of embryonic cells forms the distinct germ layers from which all the tissues and organs of the human body eventually form. Each germ layer is identified by its relative position: ectoderm (ecto- = outer), mesoderm (meso- = middle), and endoderm (endo- = inner). [link] shows the types of tissues and organs associated with the each of the three germ layers. Note that epithelial tissue originates in all three layers, whereas nervous tissue derives primarily from the ectoderm and muscle tissue from mesoderm.
Embryonic Origin of Tissues and Major Organs






View this slideshow to learn more about stem cells. How do somatic stem cells differ from embryonic stem cells?
Tissue Membranes
A tissue membrane is a thin layer or sheet of cells that covers the outside of the body (for example, skin), the organs (for example, pericardium), internal passageways that lead to the exterior of the body (for example, abdominal mesenteries), and the lining of the moveable joint cavities. There are two basic types of tissue membranes: connective tissue and epithelial membranes ([link]).
Tissue Membranes The two broad categories of tissue membranes in the body are (1) connective tissue membranes, which include synovial membranes, and (2) epithelial membranes, which include mucous membranes, serous membranes, and the cutaneous membrane, in other words, the skin.


Connective Tissue Membranes
The connective tissue membrane is formed solely from connective tissue. These membranes encapsulate organs, such as the kidneys, and line our movable joints. A synovial membrane is a type of connective tissue membrane that lines the cavity of a freely movable joint. For example, synovial membranes surround the joints of the shoulder, elbow, and knee. Fibroblasts in the inner layer of the synovial membrane release hyaluronan into the joint cavity. The hyaluronan effectively traps available water to form the synovial fluid, a natural lubricant that enables the bones of a joint to move freely against one another without much friction. This synovial fluid readily exchanges water and nutrients with blood, as do all body fluids.

Epithelial Membranes
The epithelial membrane is composed of epithelium attached to a layer of connective tissue, for example, your skin. The mucous membrane is also a composite of connective and epithelial tissues. Sometimes called mucosae, these epithelial membranes line the body cavities and hollow passageways that open to the external environment, and include the digestive, respiratory, excretory, and reproductive tracts. Mucous, produced by the epithelial exocrine glands, covers the epithelial layer. The underlying connective tissue, called the lamina propria (literally own layer), help support the fragile epithelial layer.
A serous membrane is an epithelial membrane composed of mesodermally derived epithelium called the mesothelium that is supported by connective tissue. These membranes line the coelomic cavities of the body, that is, those cavities that do not open to the outside, and they cover the organs located within those cavities. They are essentially membranous bags, with mesothelium lining the inside and connective tissue on the outside. Serous fluid secreted by the cells of the thin squamous mesothelium lubricates the membrane and reduces abrasion and friction between organs. Serous membranes are identified according locations. Three serous membranes line the thoracic cavity; the two pleura that cover the lungs and the pericardium that covers the heart. A fourth, the peritoneum, is the serous membrane in the abdominal cavity that covers abdominal organs and forms double sheets of mesenteries that suspend many of the digestive organs.
The skin is an epithelial membrane also called the cutaneous membrane. It is a stratified squamous epithelial membrane resting on top of connective tissue. The apical surface of this membrane is exposed to the external environment and is covered with dead, keratinized cells that help protect the body from desiccation and pathogens.


Chapter Review
The human body contains more than 200 types of cells that can all be classified into four types of tissues: epithelial, connective, muscle, and nervous. Epithelial tissues act as coverings controlling the movement of materials across the surface. Connective tissue integrates the various parts of the body and provides support and protection to organs. Muscle tissue allows the body to move. Nervous tissues propagate information.
The study of the shape and arrangement of cells in tissue is called histology. All cells and tissues in the body derive from three germ layers in the embryo: the ectoderm, mesoderm, and endoderm.
Different types of tissues form membranes that enclose organs, provide a friction-free interaction between organs, and keep organs together. Synovial membranes are connective tissue membranes that protect and line the joints. Epithelial membranes are formed from epithelial tissue attached to a layer of connective tissue. There are three types of epithelial membranes: mucous, which contain glands; serous, which secrete fluid; and cutaneous which makes up the skin.

Interactive Link Questions

View this slideshow to learn more about stem cells. How do somatic stem cells differ from embryonic stem cells?

Most somatic stem cells give rise to only a few cell types.



Review Questions

Which of the following is not a type of tissue?

muscle
nervous
embryonic
epithelial



C



The process by which a less specialized cell matures into a more specialized cell is called ________.

differentiation
maturation
modification
specialization



A



Differentiated cells in a developing embryo derive from ________.

endothelium, mesothelium, and epithelium
ectoderm, mesoderm, and endoderm
connective tissue, epithelial tissue, and muscle tissue
epidermis, mesoderm, and endothelium



B



Which of the following lines the body cavities exposed to the external environment?

mesothelium
lamina propria
mesenteries
mucosa



D



Critical Thinking Questions

Identify the four types of tissue in the body, and describe the major functions of each tissue.

The four types of tissue in the body are epithelial, connective, muscle, and nervous. Epithelial tissue is made of layers of cells that cover the surfaces of the body that come into contact with the exterior world, line internal cavities, and form glands. Connective tissue binds the cells and organs of the body together and performs many functions, especially in the protection, support, and integration of the body. Muscle tissue, which responds to stimulation and contracts to provide movement, is divided into three major types: skeletal (voluntary) muscles, smooth muscles, and the cardiac muscle in the heart. Nervous tissue allows the body to receive signals and transmit information as electric impulses from one region of the body to another.



The zygote is described as totipotent because it ultimately gives rise to all the cells in your body including the highly specialized cells of your nervous system. Describe this transition, discussing the steps and processes that lead to these specialized cells.

The zygote divides into many cells. As these cells become specialized, they lose their ability to differentiate into all tissues. At first they form the three primary germ layers. Following the cells of the ectodermal germ layer, they too become more restricted in what they can form. Ultimately, some of these ectodermal cells become further restricted and differentiate in to nerve cells.



What is the function of synovial membranes?

Synovial membranes are a type of connective tissue membrane that supports mobility in joints. The membrane lines the joint cavity and contains fibroblasts that produce hyaluronan, which leads to the production of synovial fluid, a natural lubricant that enables the bones of a joint to move freely against one another.



Glossary
connective tissue type of tissue that serves to hold in place, connect, and integrate the bodys organs and systems
connective tissue membrane connective tissue that encapsulates organs and lines movable joints
cutaneous membrane skin; epithelial tissue made up of a stratified squamous epithelial cells that cover the outside of the body
ectoderm outermost embryonic germ layer from which the epidermis and the nervous tissue derive
endoderm innermost embryonic germ layer from which most of the digestive system and lower respiratory system derive
epithelial membrane epithelium attached to a layer of connective tissue
epithelial tissue type of tissue that serves primarily as a covering or lining of body parts, protecting the body; it also functions in absorption, transport, and secretion
histology microscopic study of tissue architecture, organization, and function
lamina propria areolar connective tissue underlying a mucous membrane
mesoderm middle embryonic germ layer from which connective tissue, muscle tissue, and some epithelial tissue derive
mucous membrane tissue membrane that is covered by protective mucous and lines tissue exposed to the outside environment
muscle tissue type of tissue that is capable of contracting and generating tension in response to stimulation; produces movement.
nervous tissue type of tissue that is capable of sending and receiving impulses through electrochemical signals.
serous membrane type of tissue membrane that lines body cavities and lubricates them with serous fluid
synovial membrane connective tissue membrane that lines the cavities of freely movable joints, producing synovial fluid for lubrication
tissue group of cells that are similar in form and perform related functions
tissue membrane thin layer or sheet of cells that covers the outside of the body, organs, and internal cavities
totipotent embryonic cells that have the ability to differentiate into any type of cell and organ in the body

