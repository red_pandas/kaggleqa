
Appendicular Muscles of the Pelvic Girdle and Lower Limbs
Appendicular Muscles of the Pelvic Girdle and Lower LimbsBy the end of this section, you will be able to:

Identify the appendicular muscles of the pelvic girdle and lower limb
Identify the movement and function of the pelvic girdle and lower limb

The appendicular muscles of the lower body position and stabilize the pelvic girdle, which serves as a foundation for the lower limbs. Comparatively, there is much more movement at the pectoral girdle than at the pelvic girdle. There is very little movement of the pelvic girdle because of its connection with the sacrum at the base of the axial skeleton. The pelvic girdle is less range of motion because it was designed to stabilize and support the body.
Muscles of the Thigh
What would happen if the pelvic girdle, which attaches the lower limbs to the torso, were capable of the same range of motion as the pectoral girdle? For one thing, walking would expend more energy if the heads of the femurs were not secured in the acetabula of the pelvis. The bodys center of gravity is in the area of the pelvis. If the center of gravity were not to remain fixed, standing up would be difficult as well. Therefore, what the leg muscles lack in range of motion and versatility, they make up for in size and power, facilitating the bodys stabilization, posture, and movement.
Gluteal Region Muscles That Move the Femur
Most muscles that insert on the femur (the thigh bone) and move it, originate on the pelvic girdle. The psoas major and iliacus make up the iliopsoas group. Some of the largest and most powerful muscles in the body are the gluteal muscles or gluteal group. The gluteus maximus is the largest; deep to the gluteus maximus is the gluteus medius, and deep to the gluteus medius is the gluteus minimus, the smallest of the trio ([link] and [link]).Hip and Thigh Muscles The large and powerful muscles of the hip that move the femur generally originate on the pelvic girdle and insert into the femur. The muscles that move the lower leg typically originate on the femur and insert into the bones of the knee joint. The anterior muscles of the femur extend the lower leg but also aid in flexing the thigh. The posterior muscles of the femur flex the lower leg but also aid in extending the thigh. A combination of gluteal and thigh muscles also adduct, abduct, and rotate the thigh and lower leg.


Gluteal Region Muscles That Move the Femur 

The tensor fascia latae is a thick, squarish muscle in the superior aspect of the lateral thigh. It acts as a synergist of the gluteus medius and iliopsoas in flexing and abducting the thigh. It also helps stabilize the lateral aspect of the knee by pulling on the iliotibial tract (band), making it taut. Deep to the gluteus maximus, the piriformis, obturator internus, obturator externus, superior gemellus, inferior gemellus, and quadratus femoris laterally rotate the femur at the hip.The adductor longus, adductor brevis, and adductor magnus can both medially and laterally rotate the thigh depending on the placement of the foot. The adductor longus flexes the thigh, whereas the adductor magnus extends it. The pectineus adducts and flexes the femur at the hip as well. The pectineus is located in the femoral triangle, which is formed at the junction between the hip and the leg and also includes the femoral nerve, the femoral artery, the femoral vein, and the deep inguinal lymph nodes.

Thigh Muscles That Move the Femur, Tibia, and Fibula
Deep fascia in the thigh separates it into medial, anterior, and posterior compartments (see [link] and [link]). The muscles in the medial compartment of the thigh are responsible for adducting the femur at the hip. Along with the adductor longus, adductor brevis, adductor magnus, and pectineus, the strap-like gracilis adducts the thigh in addition to flexing the leg at the knee.Thigh Muscles That Move the Femur, Tibia, and Fibula 


The muscles of the anterior compartment of the thigh flex the thigh and extend the leg. This compartment contains the quadriceps femoris group, which actually comprises four muscles that extend and stabilize the knee. The rectus femoris is on the anterior aspect of the thigh, the vastus lateralis is on the lateral aspect of the thigh, the vastus medialis is on the medial aspect of the thigh, and the vastus intermedius is between the vastus lateralis and vastus medialis and deep to the rectus femoris. The tendon common to all four is the quadriceps tendon (patellar tendon), which inserts into the patella and continues below it as the patellar ligament. The patellar ligament attaches to the tibial tuberosity. In addition to the quadriceps femoris, the sartorius is a band-like muscle that extends from the anterior superior iliac spine to the medial side of the proximal tibia. This versatile muscle flexes the leg at the knee and flexes, abducts, and laterally rotates the leg at the hip. This muscle allows us to sit cross-legged.The posterior compartment of the thigh includes muscles that flex the leg and extend the thigh. The three long muscles on the back of the knee are the hamstring group, which flexes the knee. These are the biceps femoris, semitendinosus, and semimembranosus. The tendons of these muscles form the popliteal fossa, the diamond-shaped space at the back of the knee.


Muscles That Move the Feet and Toes
Similar to the thigh muscles, the muscles of the leg are divided by deep fascia into compartments, although the leg has three: anterior, lateral, and posterior ([link] and [link]).Muscles of the Lower Leg The muscles of the anterior compartment of the lower leg are generally responsible for dorsiflexion, and the muscles of the posterior compartment of the lower leg are generally responsible for plantar flexion. The lateral and medial muscles in both compartments invert, evert, and rotate the foot.


Muscles That Move the Feet and Toes 


The muscles in the anterior compartment of the leg: the tibialis anterior, a long and thick muscle on the lateral surface of the tibia, the extensor hallucis longus, deep under it, and the extensor digitorum longus, lateral to it, all contribute to raising the front of the foot when they contract. The fibularis tertius, a small muscle that originates on the anterior surface of the fibula, is associated with the extensor digitorum longus and sometimes fused to it, but is not present in all people. Thick bands of connective tissue called the superior extensor retinaculum (transverse ligament of the ankle) and the inferior extensor retinaculum, hold the tendons of these muscles in place during dorsiflexion.
The lateral compartment of the leg includes two muscles: the fibularis longus (peroneus longus) and the fibularis brevis (peroneus brevis). The superficial muscles in the posterior compartment of the leg all insert onto the calcaneal tendon (Achilles tendon), a strong tendon that inserts into the calcaneal bone of the ankle. The muscles in this compartment are large and strong and keep humans upright. The most superficial and visible muscle of the calf is the gastrocnemius. Deep to the gastrocnemius is the wide, flat soleus. The plantaris runs obliquely between the two; some people may have two of these muscles, whereas no plantaris is observed in about seven percent of other cadaver dissections. The plantaris tendon is a desirable substitute for the fascia lata in hernia repair, tendon transplants, and repair of ligaments. There are four deep muscles in the posterior compartment of the leg as well: the popliteus, flexor digitorum longus, flexor hallucis longus, and tibialis posterior.
The foot also has intrinsic muscles, which originate and insert within it (similar to the intrinsic muscles of the hand). These muscles primarily provide support for the foot and its arch, and contribute to movements of the toes ([link] and [link]). The principal support for the longitudinal arch of the foot is a deep fascia called plantar aponeurosis, which runs from the calcaneus bone to the toes (inflammation of this tissue is the cause of plantar fasciitis, which can affect runners. The intrinsic muscles of the foot consist of two groups. The dorsal group includes only one muscle, the extensor digitorum brevis. The second group is the plantar group, which consists of four layers, starting with the most superficial.Intrinsic Muscles of the Foot The muscles along the dorsal side of the foot (a) generally extend the toes while the muscles of the plantar side of the foot (b, c, d) generally flex the toes. The plantar muscles exist in three layers, providing the foot the strength to counterbalance the weight of the body. In this diagram, these three layers are shown from a plantar view beginning with the bottom-most layer just under the plantar skin of the foot (b) and ending with the top-most layer (d) located just inferior to the foot and toe bones.


Intrinsic Muscles in the Foot 



Chapter Review
The pelvic girdle attaches the legs to the axial skeleton. The hip joint is where the pelvic girdle and the leg come together. The hip is joined to the pelvic girdle by many muscles. In the gluteal region, the psoas major and iliacus form the iliopsoas. The large and strong gluteus maximus, gluteus medius, and gluteus minimus extend and abduct the femur. Along with the gluteus maximus, the tensor fascia lata muscle forms the iliotibial tract. The lateral rotators of the femur at the hip are the piriformis, obturator internus, obturator externus, superior gemellus, inferior gemellus, and quadratus femoris. On the medial part of the thigh, the adductor longus, adductor brevis, and adductor magnus adduct the thigh and medially rotate it. The pectineus muscle adducts and flexes the femur at the hip.
The thigh muscles that move the femur, tibia, and fibula are divided into medial, anterior, and posterior compartments. The medial compartment includes the adductors, pectineus, and the gracilis. The anterior compartment comprises the quadriceps femoris, quadriceps tendon, patellar ligament, and the sartorius. The quadriceps femoris is made of four muscles: the rectus femoris, the vastus lateralis, the vastus medius, and the vastus intermedius, which together extend the knee. The posterior compartment of the thigh includes the hamstrings: the biceps femoris, semitendinosus, and the semimembranosus, which all flex the knee.
The muscles of the leg that move the foot and toes are divided into anterior, lateral, superficial- and deep-posterior compartments. The anterior compartment includes the tibialis anterior, the extensor hallucis longus, the extensor digitorum longus, and the fibularis (peroneus) tertius. The lateral compartment houses the fibularis (peroneus) longus and the fibularis (peroneus) brevis. The superficial posterior compartment has the gastrocnemius, soleus, and plantaris; and the deep posterior compartment has the popliteus, tibialis posterior, flexor digitorum longus, and flexor hallucis longus.

Review Questions

The large muscle group that attaches the leg to the pelvic girdle and produces extension of the hip joint is the ________ group.

gluteal
obturator
adductor
abductor



A



Which muscle produces movement that allows you to cross your legs?

the gluteus maximus
the piriformis
the gracilis
the sartorius



D



What is the largest muscle in the lower leg?

soleus
gastrocnemius
tibialis anterior
tibialis posterior



B



The vastus intermedius muscle is deep to which of the following muscles?

biceps femoris
rectus femoris
vastus medialis
vastus lateralis



B



Critical Thinking Questions

Which muscles form the hamstrings? How do they function together?

The biceps femoris, semimembranosus, and semitendinosus form the hamstrings. The hamstrings flex the leg at the knee joint.



Which muscles form the quadriceps? How do they function together?

The rectus femoris, vastus medialis, vastus lateralis, and vastus intermedius form the quadriceps. The quadriceps muscles extend the leg at the knee joint.



Glossary
adductor brevis muscle that adducts and medially rotates the thigh
adductor longus muscle that adducts, medially rotates, and flexes the thigh
adductor magnus muscle with an anterior fascicle that adducts, medially rotates and flexes the thigh, and a posterior fascicle that assists in thigh extension
anterior compartment of the leg region that includes muscles that dorsiflex the foot
anterior compartment of the thigh region that includes muscles that flex the thigh and extend the leg
biceps femoris hamstring muscle
calcaneal tendon (also, Achilles tendon) strong tendon that inserts into the calcaneal bone of the ankle
dorsal group region that includes the extensor digitorum brevis
extensor digitorum brevis muscle that extends the toes
extensor digitorum longus muscle that is lateral to the tibialis anterior
extensor hallucis longus muscle that is partly deep to the tibialis anterior and extensor digitorum longus
femoral triangle region formed at the junction between the hip and the leg and includes the pectineus, femoral nerve, femoral artery, femoral vein, and deep inguinal lymph nodes
fibularis brevis (also, peroneus brevis) muscle that plantar flexes the foot at the ankle and everts it at the intertarsal joints
fibularis longus (also, peroneus longus) muscle that plantar flexes the foot at the ankle and everts it at the intertarsal joints
fibularis tertius small muscle that is associated with the extensor digitorum longus
flexor digitorum longus muscle that flexes the four small toes
flexor hallucis longus muscle that flexes the big toe
gastrocnemius most superficial muscle of the calf
gluteal group muscle group that extends, flexes, rotates, adducts, and abducts the femur
gluteus maximus largest of the gluteus muscles that extends the femur
gluteus medius muscle deep to the gluteus maximus that abducts the femur at the hip
gluteus minimus smallest of the gluteal muscles and deep to the gluteus medius
gracilis muscle that adducts the thigh and flexes the leg at the knee
hamstring group three long muscles on the back of the leg
iliacus muscle that, along with the psoas major, makes up the iliopsoas
iliopsoas group muscle group consisting of iliacus and psoas major muscles, that flexes the thigh at the hip, rotates it laterally, and flexes the trunk of the body onto the hip
iliotibial tract muscle that inserts onto the tibia; made up of the gluteus maximus and connective tissues of the tensor fasciae latae
inferior extensor retinaculum cruciate ligament of the ankle
inferior gemellus muscle deep to the gluteus maximus on the lateral surface of the thigh that laterally rotates the femur at the hip
lateral compartment of the leg region that includes the fibularis (peroneus) longus and the fibularis (peroneus) brevis and their associated blood vessels and nerves
medial compartment of the thigh a region that includes the adductor longus, adductor brevis, adductor magnus, pectineus, gracilis, and their associated blood vessels and nerves
obturator externus muscle deep to the gluteus maximus on the lateral surface of the thigh that laterally rotates the femur at the hip
obturator internus muscle deep to the gluteus maximus on the lateral surface of the thigh that laterally rotates the femur at the hip
patellar ligament extension of the quadriceps tendon below the patella
pectineus muscle that abducts and flexes the femur at the hip
pelvic girdle hips, a foundation for the lower limb
piriformis muscle deep to the gluteus maximus on the lateral surface of the thigh that laterally rotates the femur at the hip
plantar aponeurosis muscle that supports the longitudinal arch of the foot
plantar group four-layered group of intrinsic foot muscles
plantaris muscle that runs obliquely between the gastrocnemius and the soleus
popliteal fossa diamond-shaped space at the back of the knee
popliteus muscle that flexes the leg at the knee and creates the floor of the popliteal fossa
posterior compartment of the leg region that includes the superficial gastrocnemius, soleus, and plantaris, and the deep popliteus, flexor digitorum longus, flexor hallucis longus, and tibialis posterior
posterior compartment of the thigh region that includes muscles that flex the leg and extend the thigh
psoas major muscle that, along with the iliacus, makes up the iliopsoas
quadratus femoris muscle deep to the gluteus maximus on the lateral surface of the thigh that laterally rotates the femur at the hip
quadriceps femoris group four muscles, that extend and stabilize the knee
quadriceps tendon (also, patellar tendon) tendon common to all four quadriceps muscles, inserts into the patella
rectus femoris quadricep muscle on the anterior aspect of the thigh
sartorius band-like muscle that flexes, abducts, and laterally rotates the leg at the hip
semimembranosus hamstring muscle
semitendinosus hamstring muscle
soleus wide, flat muscle deep to the gastrocnemius
superior extensor retinaculum transverse ligament of the ankle
superior gemellus muscle deep to the gluteus maximus on the lateral surface of the thigh that laterally rotates the femur at the hip
tensor fascia lata muscle that flexes and abducts the thigh
tibialis anterior muscle located on the lateral surface of the tibia
tibialis posterior muscle that plantar flexes and inverts the foot
vastus intermedius quadricep muscle that is between the vastus lateralis and vastus medialis and is deep to the rectus femoris
vastus lateralis quadricep muscle on the lateral aspect of the thigh
vastus medialis quadricep muscle on the medial aspect of the thigh

