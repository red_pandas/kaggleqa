
Diseases, Disorders, and Injuries of the Integumentary System
Diseases, Disorders, and Injuries of the Integumentary SystemBy the end of this section, you will be able to:

Describe several different diseases and disorders of the skin
Describe the effect of injury to the skin and the process of healing

The integumentary system is susceptible to a variety of diseases, disorders, and injuries. These range from annoying but relatively benign bacterial or fungal infections that are categorized as disorders, to skin cancer and severe burns, which can be fatal. In this section, you will learn several of the most common skin conditions.
Diseases
One of the most talked about diseases is skin cancer. Cancer is a broad term that describes diseases caused by abnormal cells in the body dividing uncontrollably. Most cancers are identified by the organ or tissue in which the cancer originates. One common form of cancer is skin cancer. The Skin Cancer Foundation reports that one in five Americans will experience some type of skin cancer in their lifetime. The degradation of the ozone layer in the atmosphere and the resulting increase in exposure to UV radiation has contributed to its rise. Overexposure to UV radiation damages DNA, which can lead to the formation of cancerous lesions. Although melanin offers some protection against DNA damage from the sun, often it is not enough. The fact that cancers can also occur on areas of the body that are normally not exposed to UV radiation suggests that there are additional factors that can lead to cancerous lesions.
In general, cancers result from an accumulation of DNA mutations. These mutations can result in cell populations that do not die when they should and uncontrolled cell proliferation that leads to tumors. Although many tumors are benign (harmless), some produce cells that can mobilize and establish tumors in other organs of the body; this process is referred to as metastasis. Cancers are characterized by their ability to metastasize.
Basal Cell Carcinoma
Basal cell carcinoma is a form of cancer that affects the mitotically active stem cells in the stratum basale of the epidermis. It is the most common of all cancers that occur in the United States and is frequently found on the head, neck, arms, and back, which are areas that are most susceptible to long-term sun exposure. Although UV rays are the main culprit, exposure to other agents, such as radiation and arsenic, can also lead to this type of cancer. Wounds on the skin due to open sores, tattoos, burns, etc. may be predisposing factors as well. Basal cell carcinomas start in the stratum basale and usually spread along this boundary. At some point, they begin to grow toward the surface and become an uneven patch, bump, growth, or scar on the skin surface ([link]). Like most cancers, basal cell carcinomas respond best to treatment when caught early. Treatment options include surgery, freezing (cryosurgery), and topical ointments (Mayo Clinic 2012).
Basal Cell Carcinoma Basal cell carcinoma can take several different forms. Similar to other forms of skin cancer, it is readily cured if caught early and treated. (credit: John Hendrix, MD)



Squamous Cell Carcinoma
Squamous cell carcinoma is a cancer that affects the keratinocytes of the stratum spinosum and presents as lesions commonly found on the scalp, ears, and hands ([link]). It is the second most common skin cancer. The American Cancer Society reports that two of 10 skin cancers are squamous cell carcinomas, and it is more aggressive than basal cell carcinoma. If not removed, these carcinomas can metastasize. Surgery and radiation are used to cure squamous cell carcinoma.
Squamous Cell Carcinoma Squamous cell carcinoma presents here as a lesion on an individuals nose. (credit: the National Cancer Institute)



Melanoma
A melanoma is a cancer characterized by the uncontrolled growth of melanocytes, the pigment-producing cells in the epidermis. Typically, a melanoma develops from a mole. It is the most fatal of all skin cancers, as it is highly metastatic and can be difficult to detect before it has spread to other organs. Melanomas usually appear as asymmetrical brown and black patches with uneven borders and a raised surface ([link]). Treatment typically involves surgical excision and immunotherapy.
Melanoma Melanomas typically present as large brown or black patches with uneven borders and a raised surface. (credit: the National Cancer Institute)


Doctors often give their patients the following ABCDE mnemonic to help with the diagnosis of early-stage melanoma. If you observe a mole on your body displaying these signs, consult a doctor.
Asymmetry  the two sides are not symmetrical
Borders  the edges are irregular in shape
Color  the color is varied shades of brown or black
Diameter  it is larger than 6 mm (0.24 in)
Evolving  its shape has changed
Some specialists cite the following additional signs for the most serious form, nodular melanoma:
Elevated  it is raised on the skin surface
Firm  it feels hard to the touch
Growing  it is getting larger


Skin Disorders
Two common skin disorders are eczema and acne. Eczema is an inflammatory condition and occurs in individuals of all ages. Acne involves the clogging of pores, which can lead to infection and inflammation, and is often seen in adolescents. Other disorders, not discussed here, include seborrheic dermatitis (on the scalp), psoriasis, cold sores, impetigo, scabies, hives, and warts.
Eczema
Eczema is an allergic reaction that manifests as dry, itchy patches of skin that resemble rashes ([link]). It may be accompanied by swelling of the skin, flaking, and in severe cases, bleeding. Many who suffer from eczema have antibodies against dust mites in their blood, but the link between eczema and allergy to dust mites has not been proven. Symptoms are usually managed with moisturizers, corticosteroid creams, and immunosuppressants.
Eczema Eczema is a common skin disorder that presents as a red, flaky rash. (credit: Jambula/Wikimedia Commons)



Acne
Acne is a skin disturbance that typically occurs on areas of the skin that are rich in sebaceous glands (face and back). It is most common along with the onset of puberty due to associated hormonal changes, but can also occur in infants and continue into adulthood. Hormones, such as androgens, stimulate the release of sebum. An overproduction and accumulation of sebum along with keratin can block hair follicles. This plug is initially white. The sebum, when oxidized by exposure to air, turns black. Acne results from infection by acne-causing bacteria (Propionibacterium and Staphylococcus), which can lead to redness and potential scarring due to the natural wound healing process ([link]).
Acne Acne is a result of over-productive sebaceous glands, which leads to formation of blackheads and inflammation of the skin.


Career Connection
Dermatologist
Have you ever had a skin rash that did not respond to over-the-counter creams, or a mole that you were concerned about? Dermatologists help patients with these types of problems and more, on a daily basis. Dermatologists are medical doctors who specialize in diagnosing and treating skin disorders. Like all medical doctors, dermatologists earn a medical degree and then complete several years of residency training. In addition, dermatologists may then participate in a dermatology fellowship or complete additional, specialized training in a dermatology practice. If practicing in the United States, dermatologists must pass the United States Medical Licensing Exam (USMLE), become licensed in their state of practice, and be certified by the American Board of Dermatology.
Most dermatologists work in a medical office or private-practice setting. They diagnose skin conditions and rashes, prescribe oral and topical medications to treat skin conditions, and may perform simple procedures, such as mole or wart removal. In addition, they may refer patients to an oncologist if skin cancer that has metastasized is suspected. Recently, cosmetic procedures have also become a prominent part of dermatology. Botox injections, laser treatments, and collagen and dermal filler injections are popular among patients, hoping to reduce the appearance of skin aging.
Dermatology is a competitive specialty in medicine. Limited openings in dermatology residency programs mean that many medical students compete for a few select spots. Dermatology is an appealing specialty to many prospective doctors, because unlike emergency room physicians or surgeons, dermatologists generally do not have to work excessive hours or be on-call weekends and holidays. Moreover, the popularity of cosmetic dermatology has made it a growing field with many lucrative opportunities. It is not unusual for dermatology clinics to market themselves exclusively as cosmetic dermatology centers, and for dermatologists to specialize exclusively in these procedures.
Consider visiting a dermatologist to talk about why he or she entered the field and what the field of dermatology is like. Visit this site for additional information.



Injuries
Because the skin is the part of our bodies that meets the world most directly, it is especially vulnerable to injury. Injuries include burns and wounds, as well as scars and calluses. They can be caused by sharp objects, heat, or excessive pressure or friction to the skin.
Skin injuries set off a healing process that occurs in several overlapping stages. The first step to repairing damaged skin is the formation of a blood clot that helps stop the flow of blood and scabs over with time. Many different types of cells are involved in wound repair, especially if the surface area that needs repair is extensive. Before the basal stem cells of the stratum basale can recreate the epidermis, fibroblasts mobilize and divide rapidly to repair the damaged tissue by collagen deposition, forming granulation tissue. Blood capillaries follow the fibroblasts and help increase blood circulation and oxygen supply to the area. Immune cells, such as macrophages, roam the area and engulf any foreign matter to reduce the chance of infection.
Burns
A burn results when the skin is damaged by intense heat, radiation, electricity, or chemicals. The damage results in the death of skin cells, which can lead to a massive loss of fluid. Dehydration, electrolyte imbalance, and renal and circulatory failure follow, which can be fatal. Burn patients are treated with intravenous fluids to offset dehydration, as well as intravenous nutrients that enable the body to repair tissues and replace lost proteins. Another serious threat to the lives of burn patients is infection. Burned skin is extremely susceptible to bacteria and other pathogens, due to the loss of protection by intact layers of skin.
Burns are sometimes measured in terms of the size of the total surface area affected. This is referred to as the rule of nines, which associates specific anatomical areas with a percentage that is a factor of nine ([link]). Burns are also classified by the degree of their severity. A first-degree burn is a superficial burn that affects only the epidermis. Although the skin may be painful and swollen, these burns typically heal on their own within a few days. Mild sunburn fits into the category of a first-degree burn. A second-degree burn goes deeper and affects both the epidermis and a portion of the dermis. These burns result in swelling and a painful blistering of the skin. It is important to keep the burn site clean and sterile to prevent infection. If this is done, the burn will heal within several weeks. A third-degree burn fully extends into the epidermis and dermis, destroying the tissue and affecting the nerve endings and sensory function. These are serious burns that may appear white, red, or black; they require medical attention and will heal slowly without it. A fourth-degree burn is even more severe, affecting the underlying muscle and bone. Oddly, third and fourth-degree burns are usually not as painful because the nerve endings themselves are damaged. Full-thickness burns cannot be repaired by the body, because the local tissues used for repair are damaged and require excision (debridement), or amputation in severe cases, followed by grafting of the skin from an unaffected part of the body, or from skin grown in tissue culture for grafting purposes.Calculating the Size of a Burn The size of a burn will guide decisions made about the need for specialized treatment. Specific parts of the body are associated with a percentage of body area.






Skin grafts are required when the damage from trauma or infection cannot be closed with sutures or staples. Watch this video to learn more about skin grafting procedures.


Scars and Keloids
Most cuts or wounds, with the exception of ones that only scratch the surface (the epidermis), lead to scar formation. A scar is collagen-rich skin formed after the process of wound healing that differs from normal skin. Scarring occurs in cases in which there is repair of skin damage, but the skin fails to regenerate the original skin structure. Fibroblasts generate scar tissue in the form of collagen, and the bulk of repair is due to the basket-weave pattern generated by collagen fibers and does not result in regeneration of the typical cellular structure of skin. Instead, the tissue is fibrous in nature and does not allow for the regeneration of accessory structures, such as hair follicles, sweat glands, or sebaceous glands.
Sometimes, there is an overproduction of scar tissue, because the process of collagen formation does not stop when the wound is healed; this results in the formation of a raised or hypertrophic scar called a keloid. In contrast, scars that result from acne and chickenpox have a sunken appearance and are called atrophic scars.
Scarring of skin after wound healing is a natural process and does not need to be treated further. Application of mineral oil and lotions may reduce the formation of scar tissue. However, modern cosmetic procedures, such as dermabrasion, laser treatments, and filler injections have been invented as remedies for severe scarring. All of these procedures try to reorganize the structure of the epidermis and underlying collagen tissue to make it look more natural.

Bedsores and Stretch Marks
Skin and its underlying tissue can be affected by excessive pressure. One example of this is called a bedsore. Bedsores, also called decubitis ulcers, are caused by constant, long-term, unrelieved pressure on certain body parts that are bony, reducing blood flow to the area and leading to necrosis (tissue death). Bedsores are most common in elderly patients who have debilitating conditions that cause them to be immobile. Most hospitals and long-term care facilities have the practice of turning the patients every few hours to prevent the incidence of bedsores. If left untreated by removal of necrotized tissue, bedsores can be fatal if they become infected.
The skin can also be affected by pressure associated with rapid growth. A stretch mark results when the dermis is stretched beyond its limits of elasticity, as the skin stretches to accommodate the excess pressure. Stretch marks usually accompany rapid weight gain during puberty and pregnancy. They initially have a reddish hue, but lighten over time. Other than for cosmetic reasons, treatment of stretch marks is not required. They occur most commonly over the hips and abdomen.

Calluses
When you wear shoes that do not fit well and are a constant source of abrasion on your toes, you tend to form a callus at the point of contact. This occurs because the basal stem cells in the stratum basale are triggered to divide more often to increase the thickness of the skin at the point of abrasion to protect the rest of the body from further damage. This is an example of a minor or local injury, and the skin manages to react and treat the problem independent of the rest of the body. Calluses can also form on your fingers if they are subject to constant mechanical stress, such as long periods of writing, playing string instruments, or video games. A corn is a specialized form of callus. Corns form from abrasions on the skin that result from an elliptical-type motion.


Chapter Review
Skin cancer is a result of damage to the DNA of skin cells, often due to excessive exposure to UV radiation. Basal cell carcinoma and squamous cell carcinoma are highly curable, and arise from cells in the stratum basale and stratum spinosum, respectively. Melanoma is the most dangerous form of skin cancer, affecting melanocytes, which can spread/metastasize to other organs. Burns are an injury to the skin that occur as a result of exposure to extreme heat, radiation, or chemicals. First-degree and second-degree burns usually heal quickly, but third-degree burns can be fatal because they penetrate the full thickness of the skin. Scars occur when there is repair of skin damage. Fibroblasts generate scar tissue in the form of collagen, which forms a basket-weave pattern that looks different from normal skin.
Bedsores and stretch marks are the result of excessive pressure on the skin and underlying tissue. Bedsores are characterized by necrosis of tissue due to immobility, whereas stretch marks result from rapid growth. Eczema is an allergic reaction that manifests as a rash, and acne results from clogged sebaceous glands. Eczema and acne are usually long-term skin conditions that may be treated successfully in mild cases. Calluses and corns are the result of abrasive pressure on the skin.
Review Questions

In general, skin cancers ________.

are easily treatable and not a major health concern
occur due to poor hygiene
can be reduced by limiting exposure to the sun
affect only the epidermis



C



Bedsores ________.

can be treated with topical moisturizers
can result from deep massages
are preventable by eliminating pressure points
are caused by dry skin



C



An individual has spent too much time sun bathing. Not only is his skin painful to touch, but small blisters have appeared in the affected area. This indicates that he has damaged which layers of his skin?

epidermis only
hypodermis only
epidermis and hypodermis
epidermis and dermis



D



After a skin injury, the body initiates a wound-healing response. The first step of this response is the formation of a blood clot to stop bleeding. Which of the following would be the next response?

increased production of melanin by melanocytes
increased production of connective tissue
an increase in Pacinian corpuscles around the wound
an increased activity in the stratum lucidum



B



Squamous cell carcinomas are the second most common of the skin cancers and are capable of metastasizing if not treated. This cancer affects which cells?

basal cells of the stratum basale
melanocytes of the stratum basale
keratinocytes of the stratum spinosum
Langerhans cells of the stratum lucidum



C



Critical Thinking Questions

Why do teenagers often experience acne?

Acne results from a blockage of sebaceous glands by sebum. The blockage causes blackheads to form, which are susceptible to infection. The infected tissue then becomes red and inflamed. Teenagers experience this at high rates because the sebaceous glands become active during puberty. Hormones that are especially active during puberty stimulate the release of sebum, leading in many cases to blockages.



Why do scars look different from surrounding skin?

Scars are made of collagen and do not have the cellular structure of normal skin. The tissue is fibrous and does not allow for the regeneration of accessory structures, such as hair follicles, and sweat or sebaceous glands.



References
American Cancer Society (US). Skin cancer: basal and squamous cell [Internet]. c2013 [cited 2012 Nov 1]. Available from: http://www.cancer.org/acs/groups/cid/documents/webcontent/003139-pdf.pdf.Lucile Packard Childrens Hospital at Stanford (US). Classification and treatment of burns [Internet]. Palo Alto (CA). c2012 [cited 2012 Nov 1]. Available from: http://www.lpch.org/diseasehealthinfo/healthlibrary/burns/classify.html.Mayo Clinic (US). Basal cell carcinoma [Internet]. Scottsdale (AZ); c2012 [cited 2012 Nov 1]. Available from: http://www.mayoclinic.com/health/basal-cell-carcinoma/ds00925/dsection=treatments-and-drugs.Beck, J. FYI: how much can a human body sweat before it runs out? Popular Science [Internet]. New York (NY); c2012 [cited 2012 Nov 1]. Available from:http://www.popsci.com/science/article/2011-01/fyi-how-much-can-human-body-sweat-it-runs-out.Skin Cancer Foundation (US). Skin cancer facts [Internet]. New York (NY); c2013 [cited 2012 Nov 1]. Available from: http://www.skincancer.org/skin-cancer-information/skin-cancer-facts#top.
Glossary
acne skin condition due to infected sebaceous glands
basal cell carcinoma cancer that originates from basal cells in the epidermis of the skin
bedsore sore on the skin that develops when regions of the body start necrotizing due to constant pressure and lack of blood supply; also called decubitis ulcers
callus thickened area of skin that arises due to constant abrasion
corn type of callus that is named for its shape and the elliptical motion of the abrasive force
eczema skin condition due to an allergic reaction, which resembles a rash
first-degree burn superficial burn that injures only the epidermis
fourth-degree burn burn in which full thickness of the skin and underlying muscle and bone is damaged
keloid type of scar that has layers raised above the skin surface
melanoma type of skin cancer that originates from the melanocytes of the skin
metastasis spread of cancer cells from a source to other parts of the body
scar collagen-rich skin formed after the process of wound healing that is different from normal skin
second-degree burn partial-thickness burn that injures the epidermis and a portion of the dermis
squamous cell carcinoma type of skin cancer that originates from the stratum spinosum of the epidermis
stretch mark mark formed on the skin due to a sudden growth spurt and expansion of the dermis beyond its elastic limits
third-degree burn burn that penetrates and destroys the full thickness of the skin (epidermis and dermis)

