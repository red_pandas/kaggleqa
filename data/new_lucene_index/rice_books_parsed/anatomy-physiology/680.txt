
Nervous Tissue
Nervous TissueBy the end of this section, you will be able to:

Describe the basic structure of a neuron
Identify the different types of neurons on the basis of polarity
List the glial cells of the CNS and describe their function
List the glial cells of the PNS and describe their function

Nervous tissue is composed of two types of cells, neurons and glial cells. Neurons are the primary type of cell that most anyone associates with the nervous system. They are responsible for the computation and communication that the nervous system provides. They are electrically active and release chemical signals to target cells. Glial cells, or glia, are known to play a supporting role for nervous tissue. Ongoing research pursues an expanded role that glial cells might play in signaling, but neurons are still considered the basis of this function. Neurons are important, but without glial support they would not be able to perform their function.
Neurons
Neurons are the cells considered to be the basis of nervous tissue. They are responsible for the electrical signals that communicate information about sensations, and that produce movements in response to those stimuli, along with inducing thought processes within the brain. An important part of the function of neurons is in their structure, or shape. The three-dimensional shape of these cells makes the immense numbers of connections within the nervous system possible.
Parts of a Neuron
As you learned in the first section, the main part of a neuron is the cell body, which is also known as the soma (soma = body). The cell body contains the nucleus and most of the major organelles. But what makes neurons special is that they have many extensions of their cell membranes, which are generally referred to as processes. Neurons are usually described as having one, and only one, axona fiber that emerges from the cell body and projects to target cells. That single axon can branch repeatedly to communicate with many target cells. It is the axon that propagates the nerve impulse, which is communicated to one or more cells. The other processes of the neuron are dendrites, which receive information from other neurons at specialized areas of contact called synapses. The dendrites are usually highly branched processes, providing locations for other neurons to communicate with the cell body. Information flows through a neuron from the dendrites, across the cell body, and down the axon. This gives the neuron a polaritymeaning that information flows in this one direction. [link] shows the relationship of these parts to one another.
Parts of a Neuron The major parts of the neuron are labeled on a multipolar neuron from the CNS.


Where the axon emerges from the cell body, there is a special region referred to as the axon hillock. This is a tapering of the cell body toward the axon fiber. Within the axon hillock, the cytoplasm changes to a solution of limited components called axoplasm. Because the axon hillock represents the beginning of the axon, it is also referred to as the initial segment.
Many axons are wrapped by an insulating substance called myelin, which is actually made from glial cells. Myelin acts as insulation much like the plastic or rubber that is used to insulate electrical wires. A key difference between myelin and the insulation on a wire is that there are gaps in the myelin covering of an axon. Each gap is called a node of Ranvier and is important to the way that electrical signals travel down the axon. The length of the axon between each gap, which is wrapped in myelin, is referred to as an axon segment. At the end of the axon is the axon terminal, where there are usually several branches extending toward the target cell, each of which ends in an enlargement called a synaptic end bulb. These bulbs are what make the connection with the target cell at the synapse.




Visit this site to learn about how nervous tissue is composed of neurons and glial cells. Neurons are dynamic cells with the ability to make a vast number of connections, to respond incredibly quickly to stimuli, and to initiate movements on the basis of those stimuli. They are the focus of intense research because failures in physiology can lead to devastating illnesses. Why are neurons only found in animals? Based on what this article says about neuron function, why wouldn't they be helpful for plants or microorganisms?


Types of Neurons
There are many neurons in the nervous systema number in the trillions. And there are many different types of neurons. They can be classified by many different criteria. The first way to classify them is by the number of processes attached to the cell body. Using the standard model of neurons, one of these processes is the axon, and the rest are dendrites. Because information flows through the neuron from dendrites or cell bodies toward the axon, these names are based on the neuron's polarity ([link]).
Neuron Classification by Shape Unipolar cells have one process that includes both the axon and dendrite. Bipolar cells have two processes, the axon and a dendrite. Multipolar cells have more than two processes, the axon and two or more dendrites.


Unipolar cells have only one process emerging from the cell. True unipolar cells are only found in invertebrate animals, so the unipolar cells in humans are more appropriately called pseudo-unipolar cells. Invertebrate unipolar cells do not have dendrites. Human unipolar cells have an axon that emerges from the cell body, but it splits so that the axon can extend along a very long distance. At one end of the axon are dendrites, and at the other end, the axon forms synaptic connections with a target. Unipolar cells are exclusively sensory neurons and have two unique characteristics. First, their dendrites are receiving sensory information, sometimes directly from the stimulus itself. Secondly, the cell bodies of unipolar neurons are always found in ganglia. Sensory reception is a peripheral function (those dendrites are in the periphery, perhaps in the skin) so the cell body is in the periphery, though closer to the CNS in a ganglion. The axon projects from the dendrite endings, past the cell body in a ganglion, and into the central nervous system.
Bipolar cells have two processes, which extend from each end of the cell body, opposite to each other. One is the axon and one the dendrite. Bipolar cells are not very common. They are found mainly in the olfactory epithelium (where smell stimuli are sensed), and as part of the retina.
Multipolar neurons are all of the neurons that are not unipolar or bipolar. They have one axon and two or more dendrites (usually many more). With the exception of the unipolar sensory ganglion cells, and the two specific bipolar cells mentioned above, all other neurons are multipolar. Some cutting edge research suggests that certain neurons in the CNS do not conform to the standard model of one, and only one axon. Some sources describe a fourth type of neuron, called an anaxonic neuron. The name suggests that it has no axon (an- = without), but this is not accurate. Anaxonic neurons are very small, and if you look through a microscope at the standard resolution used in histology (approximately 400X to 1000X total magnification), you will not be able to distinguish any process specifically as an axon or a dendrite. Any of those processes can function as an axon depending on the conditions at any given time. Nevertheless, even if they cannot be easily seen, and one specific process is definitively the axon, these neurons have multiple processes and are therefore multipolar.
Neurons can also be classified on the basis of where they are found, who found them, what they do, or even what chemicals they use to communicate with each other. Some neurons referred to in this section on the nervous system are named on the basis of those sorts of classifications ([link]). For example, a multipolar neuron that has a very important role to play in a part of the brain called the cerebellum is known as a Purkinje (commonly pronounced per-KIN-gee) cell. It is named after the anatomist who discovered it (Jan Evangilista Purkinje, 17871869).
Other Neuron Classifications Three examples of neurons that are classified on the basis of other criteria. (a) The pyramidal cell is a multipolar cell with a cell body that is shaped something like a pyramid. (b) The Purkinje cell in the cerebellum was named after the scientist who originally described it. (c) Olfactory neurons are named for the functional group with which they belong.




Glial Cells
Glial cells, or neuroglia or simply glia, are the other type of cell found in nervous tissue. They are considered to be supporting cells, and many functions are directed at helping neurons complete their function for communication. The name glia comes from the Greek word that means glue, and was coined by the German pathologist Rudolph Virchow, who wrote in 1856: This connective substance, which is in the brain, the spinal cord, and the special sense nerves, is a kind of glue (neuroglia) in which the nervous elements are planted. Today, research into nervous tissue has shown that there are many deeper roles that these cells play. And research may find much more about them in the future.
There are six types of glial cells. Four of them are found in the CNS and two are found in the PNS. [link] outlines some common characteristics and functions.


Glial Cell Types by Location and Basic Function


CNS glia
PNS glia
Basic function



Astrocyte
Satellite cell
Support


Oligodendrocyte
Schwann cell
Insulation, myelination


Microglia
-
Immune surveillance and phagocytosis


Ependymal cell
-
Creating CSF


Glial Cells of the CNS
One cell providing support to neurons of the CNS is the astrocyte, so named because it appears to be star-shaped under the microscope (astro- = star). Astrocytes have many processes extending from their main cell body (not axons or dendrites like neurons, just cell extensions). Those processes extend to interact with neurons, blood vessels, or the connective tissue covering the CNS that is called the pia mater ([link]). Generally, they are supporting cells for the neurons in the central nervous system. Some ways in which they support neurons in the central nervous system are by maintaining the concentration of chemicals in the extracellular space, removing excess signaling molecules, reacting to tissue damage, and contributing to the blood-brain barrier (BBB). The blood-brain barrier is a physiological barrier that keeps many substances that circulate in the rest of the body from getting into the central nervous system, restricting what can cross from circulating blood into the CNS. Nutrient molecules, such as glucose or amino acids, can pass through the BBB, but other molecules cannot. This actually causes problems with drug delivery to the CNS. Pharmaceutical companies are challenged to design drugs that can cross the BBB as well as have an effect on the nervous system.
Glial Cells of the CNS The CNS has astrocytes, oligodendrocytes, microglia, and ependymal cells that support the neurons of the CNS in several ways.


Like a few other parts of the body, the brain has a privileged blood supply. Very little can pass through by diffusion. Most substances that cross the wall of a blood vessel into the CNS must do so through an active transport process. Because of this, only specific types of molecules can enter the CNS. Glucosethe primary energy sourceis allowed, as are amino acids. Water and some other small particles, like gases and ions, can enter. But most everything else cannot, including white blood cells, which are one of the bodys main lines of defense. While this barrier protects the CNS from exposure to toxic or pathogenic substances, it also keeps out the cells that could protect the brain and spinal cord from disease and damage. The BBB also makes it harder for pharmaceuticals to be developed that can affect the nervous system. Aside from finding efficacious substances, the means of delivery is also crucial.
Also found in CNS tissue is the oligodendrocyte, sometimes called just oligo, which is the glial cell type that insulates axons in the CNS. The name means cell of a few branches (oligo- = few; dendro- = branches; -cyte = cell). There are a few processes that extend from the cell body. Each one reaches out and surrounds an axon to insulate it in myelin. One oligodendrocyte will provide the myelin for multiple axon segments, either for the same axon or for separate axons. The function of myelin will be discussed below.
Microglia are, as the name implies, smaller than most of the other glial cells. Ongoing research into these cells, although not entirely conclusive, suggests that they may originate as white blood cells, called macrophages, that become part of the CNS during early development. While their origin is not conclusively determined, their function is related to what macrophages do in the rest of the body. When macrophages encounter diseased or damaged cells in the rest of the body, they ingest and digest those cells or the pathogens that cause disease. Microglia are the cells in the CNS that can do this in normal, healthy tissue, and they are therefore also referred to as CNS-resident macrophages.
The ependymal cell is a glial cell that filters blood to make cerebrospinal fluid (CSF), the fluid that circulates through the CNS. Because of the privileged blood supply inherent in the BBB, the extracellular space in nervous tissue does not easily exchange components with the blood. Ependymal cells line each ventricle, one of four central cavities that are remnants of the hollow center of the neural tube formed during the embryonic development of the brain. The choroid plexus is a specialized structure in the ventricles where ependymal cells come in contact with blood vessels and filter and absorb components of the blood to produce cerebrospinal fluid. Because of this, ependymal cells can be considered a component of the BBB, or a place where the BBB breaks down. These glial cells appear similar to epithelial cells, making a single layer of cells with little intracellular space and tight connections between adjacent cells. They also have cilia on their apical surface to help move the CSF through the ventricular space. The relationship of these glial cells to the structure of the CNS is seen in [link].
Glial Cells of the PNS
One of the two types of glial cells found in the PNS is the satellite cell. Satellite cells are found in sensory and autonomic ganglia, where they surround the cell bodies of neurons. This accounts for the name, based on their appearance under the microscope. They provide support, performing similar functions in the periphery as astrocytes do in the CNSexcept, of course, for establishing the BBB.
The second type of glial cell is the Schwann cell, which insulate axons with myelin in the periphery. Schwann cells are different than oligodendrocytes, in that a Schwann cell wraps around a portion of only one axon segment and no others. Oligodendrocytes have processes that reach out to multiple axon segments, whereas the entire Schwann cell surrounds just one axon segment. The nucleus and cytoplasm of the Schwann cell are on the edge of the myelin sheath. The relationship of these two types of glial cells to ganglia and nerves in the PNS is seen in [link].
Glial Cells of the PNS The PNS has satellite cells and Schwann cells.



MyelinThe insulation for axons in the nervous system is provided by glial cells, oligodendrocytes in the CNS, and Schwann cells in the PNS. Whereas the manner in which either cell is associated with the axon segment, or segments, that it insulates is different, the means of myelinating an axon segment is mostly the same in the two situations. Myelin is a lipid-rich sheath that surrounds the axon and by doing so creates a myelin sheath that facilitates the transmission of electrical signals along the axon. The lipids are essentially the phospholipids of the glial cell membrane. Myelin, however, is more than just the membrane of the glial cell. It also includes important proteins that are integral to that membrane. Some of the proteins help to hold the layers of the glial cell membrane closely together.
The appearance of the myelin sheath can be thought of as similar to the pastry wrapped around a hot dog for pigs in a blanket or a similar food. The glial cell is wrapped around the axon several times with little to no cytoplasm between the glial cell layers. For oligodendrocytes, the rest of the cell is separate from the myelin sheath as a cell process extends back toward the cell body. A few other processes provide the same insulation for other axon segments in the area. For Schwann cells, the outermost layer of the cell membrane contains cytoplasm and the nucleus of the cell as a bulge on one side of the myelin sheath. During development, the glial cell is loosely or incompletely wrapped around the axon ([link]a). The edges of this loose enclosure extend toward each other, and one end tucks under the other. The inner edge wraps around the axon, creating several layers, and the other edge closes around the outside so that the axon is completely enclosed.




View the University of Michigan WebScope at http://virtualslides.med.umich.edu/Histology/EMsmallCharts/3%20Image%20Scope%20finals/054%20-%20Peripheral%20nerve_001.svs/view.apml?listview=1& to see an electron micrograph of a cross-section of a myelinated nerve fiber. The axon contains microtubules and neurofilaments that are bounded by a plasma membrane known as the axolemma. Outside the plasma membrane of the axon is the myelin sheath, which is composed of the tightly wrapped plasma membrane of a Schwann cell. What aspects of the cells in this image react with the stain to make them a deep, dark, black color, such as the multiple layers that are the myelin sheath?

Myelin sheaths can extend for one or two millimeters, depending on the diameter of the axon. Axon diameters can be as small as 1 to 20 micrometers. Because a micrometer is 1/1000 of a millimeter, this means that the length of a myelin sheath can be 1001000 times the diameter of the axon. [link], [link], and [link] show the myelin sheath surrounding an axon segment, but are not to scale. If the myelin sheath were drawn to scale, the neuron would have to be immensepossibly covering an entire wall of the room in which you are sitting.
The Process of Myelination Myelinating glia wrap several layers of cell membrane around the cell membrane of an axon segment. A single Schwann cell insulates a segment of a peripheral nerve, whereas in the CNS, an oligodendrocyte may provide insulation for a few separate axon segments. EM  1,460,000. (Micrograph provided by the Regents of University of Michigan Medical School  2012)


Disorders of the
Nervous Tissue
Several diseases can result from the demyelination of axons. The causes of these diseases are not the same; some have genetic causes, some are caused by pathogens, and others are the result of autoimmune disorders. Though the causes are varied, the results are largely similar. The myelin insulation of axons is compromised, making electrical signaling slower.
Multiple sclerosis (MS) is one such disease. It is an example of an autoimmune disease. The antibodies produced by lymphocytes (a type of white blood cell) mark myelin as something that should not be in the body. This causes inflammation and the destruction of the myelin in the central nervous system. As the insulation around the axons is destroyed by the disease, scarring becomes obvious. This is where the name of the disease comes from; sclerosis means hardening of tissue, which is what a scar is. Multiple scars are found in the white matter of the brain and spinal cord. The symptoms of MS include both somatic and autonomic deficits. Control of the musculature is compromised, as is control of organs such as the bladder.
Guillain-Barr (pronounced gee-YAN bah-RAY) syndrome is an example of a demyelinating disease of the peripheral nervous system. It is also the result of an autoimmune reaction, but the inflammation is in peripheral nerves. Sensory symptoms or motor deficits are common, and autonomic failures can lead to changes in the heart rhythm or a drop in blood pressure, especially when standing, which causes dizziness.


Chapter Review
Nervous tissue contains two major cell types, neurons and glial cells. Neurons are the cells responsible for communication through electrical signals. Glial cells are supporting cells, maintaining the environment around the neurons.
Neurons are polarized cells, based on the flow of electrical signals along their membrane. Signals are received at the dendrites, are passed along the cell body, and propagate along the axon towards the target, which may be another neuron, muscle tissue, or a gland. Many axons are insulated by a lipid-rich substance called myelin. Specific types of glial cells provide this insulation.
Several types of glial cells are found in the nervous system, and they can be categorized by the anatomical division in which they are found. In the CNS, astrocytes, oligodendrocytes, microglia, and ependymal cells are found. Astrocytes are important for maintaining the chemical environment around the neuron and are crucial for regulating the blood-brain barrier. Oligodendrocytes are the myelinating glia in the CNS. Microglia act as phagocytes and play a role in immune surveillance. Ependymal cells are responsible for filtering the blood to produce cerebrospinal fluid, which is a circulatory fluid that performs some of the functions of blood in the brain and spinal cord because of the BBB. In the PNS, satellite cells are supporting cells for the neurons, and Schwann cells insulate peripheral axons.

Interactive Link Questions

Visit this site to learn about how nervous tissue is composed of neurons and glial cells. The neurons are dynamic cells with the ability to make a vast number of connections and to respond incredibly quickly to stimuli and to initiate movements based on those stimuli. They are the focus of intense research as failures in physiology can lead to devastating illnesses. Why are neurons only found in animals? Based on what this article says about neuron function, why wouldnt they be helpful for plants or microorganisms?

Neurons enable thought, perception, and movement. Plants do not move, so they do not need this type of tissue. Microorganisms are too small to have a nervous system. Many are single-celled, and therefore have organelles for perception and movement.


View the University of Michigan WebScope at http://virtualslides.med.umich.edu/Histology/EMsmallCharts/3%20Image%20Scope%20finals/054%20-%20Peripheral%20nerve_001.svs/view.apml?listview=1& to see an electron micrograph of a cross-section of a myelinated nerve fiber. The axon contains microtubules and neurofilaments, bounded by a plasma membrane known as the axolemma. Outside the plasma membrane of the axon is the myelin sheath, which is composed of the tightly wrapped plasma membrane of a Schwann cell. What aspects of the cells in this image react with the stain that makes them the deep, dark, black color, such as the multiple layers that are the myelin sheath?
Lipid membranes, such as the cell membrane and organelle membranes.


Review Questions

What type of glial cell provides myelin for the axons in a tract?

oligodendrocyte
astrocyte
Schwann cell
satellite cell



A



Which part of a neuron contains the nucleus?

dendrite
soma
axon
synaptic end bulb



B



Which of the following substances is least able to cross the blood-brain barrier?

water
sodium ions
glucose
white blood cells



D



What type of glial cell is the resident macrophage behind the blood-brain barrier?

microglia
astrocyte
Schwann cell
satellite cell



A



What two types of macromolecules are the main components of myelin?

carbohydrates and lipids
proteins and nucleic acids
lipids and proteins
carbohydrates and nucleic acids



C



Critical Thinking Questions

Multiple sclerosis is a demyelinating disease affecting the central nervous system. What type of cell would be the most likely target of this disease? Why?

The disease would target oligodendrocytes. In the CNS, oligodendrocytes provide the myelin for axons.



Which type of neuron, based on its shape, is best suited for relaying information directly from one neuron to another? Explain why.

Bipolar cells, because they have one dendrite that receives input and one axon that provides output, would be a direct relay between two other cells.



Glossary
astrocyte glial cell type of the CNS that provides support for neurons and maintains the blood-brain barrier
axon hillock tapering of the neuron cell body that gives rise to the axon
axon segment single stretch of the axon insulated by myelin and bounded by nodes of Ranvier at either end (except for the first, which is after the initial segment, and the last, which is followed by the axon terminal)
axon terminal end of the axon, where there are usually several branches extending toward the target cell
axoplasm cytoplasm of an axon, which is different in composition than the cytoplasm of the neuronal cell body
bipolar shape of a neuron with two processes extending from the neuron cell bodythe axon and one dendrite
blood-brain barrier (BBB) physiological barrier between the circulatory system and the central nervous system that establishes a privileged blood supply, restricting the flow of substances into the CNS
cerebrospinal fluid (CSF) circulatory medium within the CNS that is produced by ependymal cells in the choroid plexus filtering the blood
choroid plexus specialized structure containing ependymal cells that line blood capillaries and filter blood to produce CSF in the four ventricles of the brain
ependymal cell glial cell type in the CNS responsible for producing cerebrospinal fluid
initial segment first part of the axon as it emerges from the axon hillock, where the electrical signals known as action potentials are generated
microglia glial cell type in the CNS that serves as the resident component of the immune system
multipolar shape of a neuron that has multiple processesthe axon and two or more dendrites
myelin sheath lipid-rich layer of insulation that surrounds an axon, formed by oligodendrocytes in the CNS and Schwann cells in the PNS; facilitates the transmission of electrical signals
node of Ranvier gap between two myelinated regions of an axon, allowing for strengthening of the electrical signal as it propagates down the axon
oligodendrocyte glial cell type in the CNS that provides the myelin insulation for axons in tracts
satellite cell glial cell type in the PNS that provides support for neurons in the ganglia
Schwann cell glial cell type in the PNS that provides the myelin insulation for axons in nerves
synapse narrow junction across which a chemical signal passes from neuron to the next, initiating a new electrical signal in the target cell
synaptic end bulb swelling at the end of an axon where neurotransmitter molecules are released onto a target cell across a synapse
unipolar shape of a neuron which has only one process that includes both the axon and dendrite
ventricle central cavity within the brain where CSF is produced and circulates

