
Functions of Human Life
Functions of Human LifeBy the end of this section, you will be able to:

Explain the importance of organization to the function of the human organism
Distinguish between metabolism, anabolism, and catabolism
Provide at least two examples of human responsiveness and human movement
Compare and contrast growth, differentiation, and reproduction

The different organ systems each have different functions and therefore unique roles to perform in physiology. These many functions can be summarized in terms of a few that we might consider definitive of human life: organization, metabolism, responsiveness, movement, development, and reproduction.
Organization
A human body consists of trillions of cells organized in a way that maintains distinct internal compartments. These compartments keep body cells separated from external environmental threats and keep the cells moist and nourished. They also separate internal body fluids from the countless microorganisms that grow on body surfaces, including the lining of certain tracts, or passageways. The intestinal tract, for example, is home to even more bacteria cells than the total of all human cells in the body, yet these bacteria are outside the body and cannot be allowed to circulate freely inside the body.
Cells, for example, have a cell membrane (also referred to as the plasma membrane) that keeps the intracellular environmentthe fluids and organellesseparate from the extracellular environment. Blood vessels keep blood inside a closed circulatory system, and nerves and muscles are wrapped in connective tissue sheaths that separate them from surrounding structures. In the chest and abdomen, a variety of internal membranes keep major organs such as the lungs, heart, and kidneys separate from others.The bodys largest organ system is the integumentary system, which includes the skin and its associated structures, such as hair and nails. The surface tissue of skin is a barrier that protects internal structures and fluids from potentially harmful microorganisms and other toxins.

MetabolismThe first law of thermodynamics holds that energy can neither be created nor destroyedit can only change form. Your basic function as an organism is to consume (ingest) energy and molecules in the foods you eat, convert some of it into fuel for movement, sustain your body functions, and build and maintain your body structures. There are two types of reactions that accomplish this: anabolism and catabolism.
Anabolism is the process whereby smaller, simpler molecules are combined into larger, more complex substances. Your body can assemble, by utilizing energy, the complex chemicals it needs by combining small molecules derived from the foods you eat
Catabolism is the process by which larger more complex substances are broken down into smaller simpler molecules. Catabolism releases energy. The complex molecules found in foods are broken down so the body can use their parts to assemble the structures and substances needed for life.
Taken together, these two processes are called metabolism. Metabolism is the sum of all anabolic and catabolic reactions that take place in the body ([link]). Both anabolism and catabolism occur simultaneously and continuously to keep you alive.
MetabolismAnabolic reactions are building reactions, and they consume energy. Catabolic reactions break materials down and release energy. Metabolism includes both anabolic and catabolic reactions.


Every cell in your body makes use of a chemical compound, adenosine triphosphate (ATP), to store and release energy. The cell stores energy in the synthesis (anabolism) of ATP, then moves the ATP molecules to the location where energy is needed to fuel cellular activities. Then the ATP is broken down (catabolism) and a controlled amount of energy is released, which is used by the cell to perform a particular job.



View this animation to learn more about metabolic processes. What kind of catabolism occurs in the heart?

Responsiveness
Responsiveness is the ability of an organism to adjust to changes in its internal and external environments. An example of responsiveness to external stimuli could include moving toward sources of food and water and away from perceived dangers. Changes in an organisms internal environment, such as increased body temperature, can cause the responses of sweating and the dilation of blood vessels in the skin in order to decrease body temperature, as shown by the runners in [link].

Movement
Human movement includes not only actions at the joints of the body, but also the motion of individual organs and even individual cells. As you read these words, red and white blood cells are moving throughout your body, muscle cells are contracting and relaxing to maintain your posture and to focus your vision, and glands are secreting chemicals to regulate body functions. Your body is coordinating the action of entire muscle groups to enable you to move air into and out of your lungs, to push blood throughout your body, and to propel the food you have eaten through your digestive tract. Consciously, of course, you contract your skeletal muscles to move the bones of your skeleton to get from one place to another (as the runners are doing in [link]), and to carry out all of the activities of your daily life.
Marathon RunnersRunners demonstrate two characteristics of living humansresponsiveness and movement. Anatomic structures and physiological processes allow runners to coordinate the action of muscle groups and sweat in response to rising internal body temperature. (credit: Phil Roeder/flickr)



Development, growth and reproductionDevelopment is all of the changes the body goes through in life. Development includes the processes of differentiation, growth, and renewal.
Growth is the increase in body size. Humans, like all multicellular organisms, grow by increasing the number of existing cells, increasing the amount of non-cellular material around cells (such as mineral deposits in bone), and, within very narrow limits, increasing the size of existing cells.
Reproduction is the formation of a new organism from parent organisms. In humans, reproduction is carried out by the male and female reproductive systems. Because death will come to all complex organisms, without reproduction, the line of organisms would end.

Chapter Review
Most processes that occur in the human body are not consciously controlled. They occur continuously to build, maintain, and sustain life. These processes include: organization, in terms of the maintenance of essential body boundaries; metabolism, including energy transfer via anabolic and catabolic reactions; responsiveness; movement; and growth, differentiation, reproduction, and renewal.

Interactive Link Questions

View this animation to learn more about metabolic processes. What kind of catabolism occurs in the heart?

Fatty acid catabolism.



Review Questions

Metabolism can be defined as the ________.

adjustment by an organism to external or internal changes
process whereby all unspecialized cells become specialized to perform distinct functions
process whereby new cells are formed to replace worn-out cells
sum of all chemical reactions in an organism



D



Adenosine triphosphate (ATP) is an important molecule because it ________.

is the result of catabolism
release energy in uncontrolled bursts
stores energy for use by body cells
All of the above



C



Cancer cells can be characterized as generic cells that perform no specialized body function. Thus cancer cells lack ________.

differentiation
reproduction
responsiveness
both reproduction and responsiveness



A



CRITICAL THINKING QUESTIONS

Explain why the smell of smoke when you are sitting at a campfire does not trigger alarm, but the smell of smoke in your residence hall does.

When you are sitting at a campfire, your sense of smell adapts to the smell of smoke. Only if that smell were to suddenly and dramatically intensify would you be likely to notice and respond. In contrast, the smell of even a trace of smoke would be new and highly unusual in your residence hall, and would be perceived as danger.



Identify three different ways that growth can occur in the human body.

Growth can occur by increasing the number of existing cells, increasing the size of existing cells, or increasing the amount of non-cellular material around cells.



Glossary
anabolism assembly of more complex molecules from simpler molecules
catabolism breaking down of more complex molecules into simpler molecules
development changes an organism goes through during its life
differentiation process by which unspecialized cells become specialized in structure and function
growth process of increasing in size
metabolism sum of all of the bodys chemical reactions
renewal process by which worn-out cells are replaced
reproduction process by which new organisms are generated
responsiveness ability of an organisms or a system to adjust to changes in conditions

