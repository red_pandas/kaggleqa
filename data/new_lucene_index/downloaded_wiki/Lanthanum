Lanthanum is a soft, ductile, silvery-white metallic chemical element with symbol La and atomic number 57. It tarnishes rapidly when exposed to air and is soft enough to be cut with a knife. It gave its name to the lanthanide series, a group of 15 similar elements between lanthanum and lutetium in the periodic table: it is also sometimes considered the first element of the 6th-period transition metals. As such, it almost always assumes the oxidation state +3. Lanthanum has no biological role and is not very toxic.
Lanthanum is usually found in combination with cerium and other rare earth elements, and it was first found by the Swedish chemist Carl Gustav Mosander in 1839 as an impurity in cerium nitrate  hence the name lanthanum, from the Greek  (lanthanein), meaning "to lie hidden". Although it is classified as a rare earth element, lanthanum is the 28th most abundant element in the Earth's crust, being just under three times as abundant as lead. In minerals such as monazite and bastnsite, lanthanum makes up over a quarter of the lanthanide content. It is extracted from these minerals using a complex multistage extraction process; due to the complexity of these processes, pure lanthanum metal was not isolated until 1923.
Lanthanum compounds have numerous applications as catalysts, additives in glass, carbon lighting for studio lighting and projection, ignition elements in lighters and torches, electron cathodes, scintillators, GTAW electrodes, and others. Lanthanum carbonate has been approved as a medicine for treating renal failure.


== Characteristics ==


=== Physical ===
Lanthanum has a hexagonal crystal structure at room temperature. At 310 C, lanthanum changes to a face-centered cubic structure. At 865 C, it changes to a body-centered cubic structure. Lanthanum is easily oxidized; a centimeter-sized sample will completely oxidize within a year. Therefore, it is used in elemental form only for research purposes. Single lanthanum atoms have been isolated by implanting them into fullerene molecules; if carbon nanotubes are filled with these lanthanum-encapsulated fullerenes and annealed, metallic nanochains of lanthanum are produced inside carbon nanotubes.


=== Chemical ===
Lanthanum exhibits two oxidation states, +3 and +2, the former being much more stable. For example, LaH3 is more stable than LaH2. Lanthanum burns readily at 150 C to form lanthanum(III) oxide:
4 La + 3 O2  2 La2O3
However, when exposed to moist air at room temperature, lanthanum oxide forms a hydrated oxide with a large volume increase. Lanthanum is quite electropositive, reacting slowly with cold water and quite quickly with hot water to form lanthanum hydroxide:
2 La (s) + 6 H2O (l)  2 La(OH)3 (aq) + 3 H2 (g)
Lanthanum metal reacts with all the halogens. The reaction is vigorous if conducted above 200 C:
2 La (s) + 3 F2 (g)  2 LaF3 (s)
2 La (s) + 3 Cl2 (g)  2 LaCl3 (s)
2 La (s) + 3 Br2 (g)  2 LaBr3 (s)
2 La (s) + 3 I2 (g)  2 LaI3 (s)
In dilute sulfuric acid, lanthanum readily forms solutions containing the La(III) ions, which exist as [La(OH2)9]3+ complexes:
2 La(s) + 3 H2SO4 (aq)  2 La3+(aq) + 3 SO2
4 (aq) + 3 H2 (g)
Lanthanum combines with nitrogen, carbon, sulfur, phosphorus, boron, selenium, silicon and arsenic at elevated temperatures, forming binary compounds. The electron configuration of the colourless La3+ ion is [Xe] 4f0.


=== Isotopes ===

Naturally occurring lanthanum is composed of one stable (139La) and one radioactive (138La) isotope, with 139La being the most abundant (99.91% natural abundance). 38 radioisotopes have been characterized  the most stable is 138La with a half-life of 1.051011 years, followed by 137La with a half-life of 60,000 years. Most other radioisotopes have half-lives of less than 24 hours, and the majority of these have half-lives less than 1 minute. This element also has three meta states. Lanthanum isotopes range in atomic weight from 117 u (117La) to 155 u (155La).


== History ==
The word lanthanum comes from the Greek  [lanthan] (lit. to lie hidden). Lanthanum was discovered in 1839 by Swedish chemist Carl Gustav Mosander, who partially decomposed a sample of cerium nitrate by roasting it in air and then treating the resulting oxide with dilute nitric acid. From the resulting solution, he isolated a new rare earth he called lantana. Lanthanum was isolated in relatively pure form in 1923.
Lanthanum is the most strongly basic of all the trivalent lanthanides, and it was this property that allowed Mosander to isolate and purify the salts of this element. Basicity separation as operated commercially involved the fractional precipitation of the weaker bases (such as didymium) from nitrate solution by the addition of magnesium oxide or dilute ammonia gas. Purified lanthanum remained in solution. (The basicity methods were only suitable for lanthanum purification; didymium could not be efficiently further separated in this manner.) The alternative technique of fractional crystallization was invented by Dmitri Mendeleev, in the form of the double ammonium nitrate tetrahydrate, which he used to separate the less-soluble lanthanum from the more-soluble didymium in the 1870s. This system was used commercially in lanthanum purification until the development of practical solvent extraction methods that started in the late 1950s. (A detailed process using the double ammonium nitrates to provide 99.99% pure lanthanum, neodymium concentrates and praseodymium concentrates is presented in Callow 1967, at a time when the process was just becoming obsolete.) As operated for lanthanum purification, the double ammonium nitrates were recrystallized from water. When later adapted by Carl Auer von Welsbach for the splitting of didymium, nitric acid was used as a solvent to lower the solubility of the system. Lanthanum is relatively easy to purify, since it has only one adjacent lanthanide, cerium, which itself is very readily removed due to its potential tetravalency.
The fractional crystallization purification of lanthanum as the double ammonium nitrate was sufficiently rapid and efficient, that lanthanum purified in this manner was not expensive. The Lindsay Chemical Division of American Potash and Chemical Corporation, for a while the largest producer of rare earths in the world, in a price list dated October 1, 1958 priced 99.9% lanthanum ammonium nitrate (oxide content of 29%) at $3.15 per pound, or $1.93 per pound in 50-pound quantities. The corresponding oxide (slightly purer at 99.99%) was priced at $11.70 or $7.15 per pound for the two quantity ranges. The price for their purest grade of oxide (99.997%) was $21.60 and $13.20, respectively.


== Occurrence ==

Although lanthanum belongs to the element group called rare earth metals, it is not rare at all. Lanthanum is available in relatively large quantities (32 ppm in Earths crust). "Rare earths" got their name because they were indeed rare as compared to the "common" earths such as lime or magnesia, and historically only a few deposits were known. Lanthanum is taken into consideration as a rare earth metal because the process to mine is difficult, time consuming and expensive.
Monazite (Ce, La, Th, Nd, Y)PO4, and bastnsite (Ce, La, Y)CO3F, are the principal ores in which lanthanum occurs, in percentages of up to 25 to 38 percent of the total lanthanide content. In general, there is more lanthanum in bastnsite than in monazite. Until 1949, bastnsite was a rare and obscure mineral, not even remotely contemplated as a potential commercial source for lanthanides. In that year, the large deposit at the Mountain Pass rare earth mine in California was discovered. This discovery alerted geologists to the existence of a new class of rare earth deposit, the rare-earth bearing carbonatite, other examples of which soon surfaced, particularly in Africa and China.


== Production ==

Lanthanum is most commonly obtained from monazite and bastnsite. The mineral mixtures are crushed and ground. Monazite, because of its magnetic properties, can be separated by repeated electromagnetic separation. After separation, it is treated with hot concentrated sulfuric acid to produce water-soluble sulfates of rare earths. The acidic filtrates are partially neutralized with sodium hydroxide to pH 3-4. Thorium precipitates out of solution as hydroxide and is removed. After that, the solution is treated with ammonium oxalate to convert rare earths to their insoluble oxalates. The oxalates are converted to oxides by annealing. The oxides are dissolved in nitric acid that excludes one of the main components, cerium, whose oxide is insoluble in HNO3. Lanthanum is separated as a double salt with ammonium nitrate by crystallization. This salt is relatively less soluble than other rare earth double salts and therefore stays in the residue.
The most efficient separation routine for lanthanum salt from the rare-earth salt solution is, however, ion exchange. In this process, rare-earth ions are adsorbed onto suitable ion-exchange resin by exchange with hydrogen, ammonium or cupric ions present in the resin. The rare earth ions are then selectively washed out by a suitable complexing agent, such as ammonium citrate or nitrilotriacetate. Lanthanum can also be separated from a solution of rare earth nitrates by liquid-liquid extraction with a suitable organic liquid, such as tributyl phosphate. Currently, the most widely used extractant for the purification of lanthanum and the other lanthanides is the 2-ethylhexyl ester of 2-ethylhexylphosphonic acid; this has better handling characteristics than the previously used bis-2-ethylhexyl phosphate.
Lanthanum metal is obtained from its oxide by heating it with ammonium chloride or fluoride and hydrofluoric acid at 300-400 C to produce the chloride or fluoride:
La2O3 + 6 NH4Cl  2 LaCl3 + 6 NH3 + 3 H2O
This is followed by reduction with alkali or alkaline earth metals in vacuum or argon atmosphere:
LaCl3 + 3 Li  La + 3 LiCl
Also, pure lanthanum can be produced by electrolysis of molten mixture of anhydrous LaCl3 and NaCl or KCl at elevated temperatures.


== Applications ==

The first historical application of lanthanum was in gas lantern mantles. Carl Auer von Welsbach used a mixture of 60% magnesium oxide, 20% lanthanum oxide, and 20% yttrium oxide, which he called Actinophor and patented in 1885. The original mantles gave a green-tinted light and were not very successful, and his first company, which established a factory in Atzgersdorf in 1887, failed in 1889.
Modern uses of lanthanum include:

One material used for anodic material of nickel-metal hydride batteries is La(Ni3.6Mn0.4Al0.3Co0.7). Due to high cost to extract the other lanthanides, a mischmetal with more than 50% of lanthanum is used instead of pure lanthanum. The compound is an intermetallic component of the AB5 type.
As most hybrid cars use nickel-metal hydride batteries, massive quantities of lanthanum are required for the production of hybrid automobiles. A typical hybrid automobile battery for a Toyota Prius requires 10 to 15 kg (2233 lb) of lanthanum. As engineers push the technology to increase fuel efficiency, twice that amount of lanthanum could be required per vehicle.
Hydrogen sponge alloys can contain lanthanum. These alloys are capable of storing up to 400 times their own volume of hydrogen gas in a reversible adsorption process. Heat energy is released every time they do so; therefore these alloys have possibilities in energy conservation systems.
Mischmetal, a pyrophoric alloy used in lighter flints, contains 25% to 45% lanthanum.
Lanthanum oxide and the boride are used in electronic vacuum tubes as hot cathode materials with strong emissivity of electrons. Crystals of LaB6 are used in high-brightness, extended-life, thermionic electron emission sources for electron microscopes and Hall-effect thrusters.
Lanthanum fluoride (LaF3) is an essential component of a heavy fluoride glass named ZBLAN. This glass has superior transmittance in the infrared range and is therefore used for fiber-optical communication systems.
Cerium-doped lanthanum bromide and lanthanum chloride are the recent inorganic scintillators, which have a combination of high light yield, best energy resolution, and fast response. Their high yield converts into superior energy resolution; moreover, the light output is very stable and quite high over a very wide range of temperatures, making it particularly attractive for high-temperature applications. These scintillators are already widely used commercially in detectors of neutrons or gamma rays.
Carbon arc lamps use a mixture of rare earth elements to improve the light quality. This application, especially by the motion picture industry for studio lighting and projection, consumed about 25% of the rare-earth compounds produced until the phase out of carbon arc lamps.
Lanthanum(III) oxide (La2O3) improves the alkali resistance of glass and is used in making special optical glasses, such as infrared-absorbing glass, as well as camera and telescope lenses, because of the high refractive index and low dispersion of rare-earth glasses. Lanthanum oxide is also used as a grain-growth additive during the liquid-phase sintering of silicon nitride and zirconium diboride.
Small amounts of lanthanum added to steel improves its malleability, resistance to impact, and ductility, whereas addition of lanthanum to molybdenum decreases its hardness and sensitivity to temperature variations.
Small amounts of lanthanum are present in many pool products to remove the phosphates that feed algae.
Lanthanum oxide additive to tungsten is used in gas tungsten arc welding electrodes, as a substitute for radioactive thorium.
Various compounds of lanthanum and other rare-earth elements (oxides, chlorides, etc.) are components of various catalysis, such as petroleum cracking catalysts.
Lanthanum-barium radiometric dating is used to estimate age of rocks and ores, though the technique has limited popularity.
Lanthanum carbonate was approved as a medication (Fosrenol, Shire Pharmaceuticals) to absorb excess phosphate in cases of end-stage renal failure.
Lanthanum fluoride is used in phosphor lamp coatings. Mixed with europium fluoride, it is also applied in the crystal membrane of fluoride ion-selective electrodes.
Like horseradish peroxidase, lanthanum is used as an electron-dense tracer in molecular biology.
Lanthanum modified bentonite (or phoslock) is used to remove phosphates from water in lake treatments.


== Biological role ==
Lanthanum has no known biological role. The element is very poorly absorbed after oral administration and when injected its elimination is very slow. Lanthanum carbonate (Fosrenol) was approved as a phosphate binder to absorb excess phosphate in cases of end stage renal disease.
While lanthanum has pharmacological effects on several receptors and ion channels, its specificity for the GABA receptor is unique among trivalent cations. Lanthanum acts at the same modulatory site on the GABA receptor as zinc- a known negative allosteric modulator. The lanthanum cation La3+ is a positive allosteric modulator at native and recombinant GABA receptors, increasing open channel time and decreasing desensitization in a subunit configuration dependent manner.


== Precautions ==
Lanthanum has a low to moderate level of toxicity and should be handled with care. The injection of lanthanum solutions produces hyperglycemia, low blood pressure, degeneration of the spleen and hepatic alterations. The application in carbon arc light led to the exposure of people to rare earth element oxides and fluorides, sometimes led to pneumoconiosis.


== See also ==


== References ==


== Further reading ==
The Industrial Chemistry of the Lanthanons, Yttrium, Thorium and Uranium, by R. J. Callow, Pergamon Press, 1967
Extractive Metallurgy of Rare Earths, by C. K. Gupta and N. Krishnamurthy, CRC Press, 2005
Nouveau Traite de Chimie Minerale, Vol. VII. Scandium, Yttrium, Elements des Terres Rares, Actinium, P. Pascal, Editor, Masson & Cie, 1959
Chemistry of the Lanthanons, by R. C. Vickery, Butterworths 1953


== External links ==
WebElements.com  Lanthanum