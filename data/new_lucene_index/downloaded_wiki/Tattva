Tattva is a Sanskrit word meaning 'thatness', 'principle', 'reality' or 'truth'. According to various Indian schools of philosophy, a tattva (or tattwa) is an element or aspect of reality. In some traditions, they are conceived as an aspect of deity. Although the number of tattvas varies depending on the philosophical school, together they are thought to form the basis of all our experience. The Samkhya philosophy uses a system of 25 tattvas, while Shaivism recognises 36 tattvas. In Buddhism, the equivalent is the list of dhammas which constitute reality.


== Jainism ==

Jain philosophy can be described in various ways, but the most acceptable tradition is to describe it in terms of the Tattvas or fundamentals. Without knowing them one cannot progress towards liberation. According to major Jain text, Tattvarthsutra, these are:
Jiva - Souls and living things
Ajiva - Non-living things
Asrava - Influx of karma
Bandha - The bondage of karma
Samvara - The stoppage of influx of karma
Nirjara - Shedding of karma
Moksha - Liberation or Salvation
Each one of these fundamental principles are discussed and explained by Jain Scholars in depth. There are two examples that can be used to explain the above principle intuitively.
A man rides a wooden boat to reach the other side of the river. Now the man is Jiva, the boat is ajiva. Now the boat has a leak and water flows in. That incoming of water is Asrava and accumulating there is Bandha. Now the man tries to save the boat by blocking the hole. That blockage is Samvara and throwing the water outside is Nirjara. Now the man crosses the river and reaches his destination, Moksha.
Consider a family living in a house. One day, they were enjoying a fresh cool breeze coming through their open doors and windows of the house. However, the weather suddenly changed to a terrible dust storm. The family, realizing the storm, closed the doors and windows. But, by the time they could close all the doors and windows some of the dust had been blown into the house. After closing the doors and the windows, they started clearing the dust that had come in to make the house clean again.
This simple scenario can be interpreted as follows:
Jivas are represented by the living people.
Ajiva is represented by the house.
Asrava is represented by the influx of dust.
Bandh is represented by the accumulation of dust in the house.
Samvar is represented by the closing of the doors and windows to stop the accumulation of dust.
Nirjara is represented by the cleaning up of already collected dust from the house.
Moksha is represented by the cleaned house, which is similar to the shedding off all karmic particles from the soul.


== Buddhism ==

In Buddhism the term "dhamma/dharma" is being used for the constitutional elements. Early Buddhist philosophy used several lists, such as namarupa and the five skandhas, to analyse reality. The Abhidhamma tradition elaborated on these lists, using over 100 terms to analyse reality.


== Samkhya ==

The Samkhya philosophy regards the Universe as consisting of two eternal realities: Purusha and Prakrti. It is therefore a strongly dualist philosophy. The Purusha is the centre of consciousness, whereas the Prakriti is the source of all material existence. The twenty-five tattva system of Samkhya concerns itself only with the tangible aspect of creation, theorizing that Prakriti is the source of the world of becoming. It is the first tattva and is seen as pure potentiality that evolves itself successively into twenty-four additional tattvas or principles.


== Theistic traditions ==


=== Kashmir Shaivism ===

In Kashmir Shaivite philosophy, the tattvas are inclusive of consciousness as well as material existence. The 36 tattvas of Shaivism are divided into three groups:
Shuddha tattvas
The first five tattvas are known as the shuddha or 'pure' tattvas. They are also known as the tattvas of universal experience.
Shuddha-ashuddha tattvas
The next seven tattvas (612) are known as the shuddha-ashuddha or 'pure-impure' tattvas. They are the tattvas of limited individual experience.
Ashuddha tattvas
The last twenty-four tattvas (1336) are known as the ashuddha or 'impure' tattvas. The first of these is prakriti and they include the tattvas of mental operation, sensible experience, and materiality.


=== Gaudiya Vaishnavism ===
Within Puranic literatures and general Vaishnava philosophy tattva is often used to denote certain categories or types of being or energies such as :
Krishna-tattva
The Supreme personality of Godhead. The causative factor of everything including other Tattva(s).
Vishnu-tattva
Any incarnation or expansion of Krishna.
Sakti-Tattva
The multifarious energies of the Lord Krishna. It includes his internal potency Yoga Maya and material prakrti
Jiva-tattva
The living souls (jivas).
Siva-tattva
Lord Siva (excluding the Rudra(s)) is not considered to be a jiva.
Mahat-tattva
The total material energy (prakrti).
In Gaudiya Vaishnava philosophy that there are a total of five primary tattvas described in terms of living beings, which are collectively known as the Pancha Tattva and described as follows:

"Spiritually there are no differences between these five tattvas, for on the transcendental platform everything is absolute. Yet there are also varieties in the spiritual world, and in order to taste these spiritual varieties one should distinguish between them".


== Tantra ==

In Hindu tantrism there are five tattvas creating global energy cycles of tattvic tides beginning at dawn with Akasha and ending with Prithvi:
Akasha (Spirit tattva)  symbolized by a black oval
Vayu (Air tattva)  symbolized by a blue circle
Tejas (Fire tattva)  symbolized by a red triangle
Apas (Water tattva)  symbolized by a white moon
Prithvi (Earth tattva)  symbolized by a yellow square
Each complete cycle lasts two hours. This system of five tattvas which each can be combined with another, was also adapted by the Golden Dawn (Tattva vision).


== Siddha Medicine ==

The siddha system of traditional medicine (Tamil:  , Citta maruttuvam ?) of ancient India was derived by Tamil Siddhas or the spiritual scientists of Tamil Nadu. According to this tradition, the human body is composed of 96 constituent principles or tattvas. Siddhas fundamental principles never differentiated man from the universe. According to them, Nature is man and man is nature and therefore both are essentially one. Man is said to the microcosm and the Universe is Macrocosm, because what exists in the Universe exists in man.


== Ayyavazhi ==

Tatvas are the 96 qualities or properties of human body according to Akilattirattu Ammanai, the religious book of Ayyavazhi.


== See also ==
Mahabhuta
Achintya Bheda Abheda
Tattva vision
Tat Tvam Asi
Tathat (Buddhism)


== References ==


== Sources ==


== External links ==
Uses of 'tattva' in Puranic and Gaudiya Vaishnava literature.
Articles on Absolute Truth in a Vedic paradigm.