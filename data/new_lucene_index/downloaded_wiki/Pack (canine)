Pack is a social group of conspecific canids. Not all species of canids form packs; for example, small canids like the red fox do not. Pack size and social behaviour within packs varies across species.


== Pack behavior in specific speciesEdit ==
African wild dogs (Lycaon pictus) live and hunt in packs. Males assist in raising the pups, and remain with their pack for life, while the females leave their birth pack at about the age of two and a half years old to join a pack with no females. Males outnumber the females in a pack, and usually only one female breeds, with all of the males. African wild dogs are not territorial, and they hunt cooperatively in their packs, running down large game and tearing it apart. They cooperate in caring for wounded and sick pack members as well as the young.
Gray wolves (Canis lupus) usually live in packs which consist of the adult parents and their offspring of perhaps the last 2 or 3 years. The adult parents are usually unrelated and other unrelated wolves may sometimes join the pack.
Black-backed jackals (Canis mesomelas) have a single long term mate, and usually hunt singly or in pairs. Both parents care for the young, and the parents and their current offspring are the pack. They will cooperate in larger packs to hunt large game.
The Ethiopian wolf (Canis simensis) has different social behavior from the gray wolf: pack members hunt alone for rodents, and come together mainly to defend their territory from other packs.


=== Domestic dogsEdit ===

Domestic dogs (Canis lupus familiaris): Domesticated dogs have had humans as part of dog social structure for at least 12,000 years, and human behaviour is not the same as wolf behaviour. Studies of dog behaviour include studies of dogs and their interactions with humans, and "dumped" or "road" dogs that were raised by humans and then left to fend for themselves (e.g. The Tuscany Dog Project).


=== Pack organization in other canine speciesEdit ===
Golden jackal#Social and territorial behaviours
Dhole#Social and territorial behaviours
Coyote#Behavior
Side-striped jackal#Social behavior and reproduction


== Dominance and the alpha wolfEdit ==
Dominance is a ubiquitous phenomenon in social animals. Animals which typically predominate over others are associated with the term alpha. Among pack-living wolves, alpha wolves are the genetic parents of most cubs in the pack. Such access to mating females creates strong selective pressure for intra-sex competition.
Wolves show deference to the alpha pair in their pack by allowing them to be the first to eat and, usually, the only pair to reproduce. Wolves use eye contact as an indicator of dominance or submission, but in order to establish a dominant position they often also show physical superiority through playing or fighting. The smaller and more nuclear a pack is, the status of alpha is less likely to be obtained through fighting, and young wolves instead leave the pack to find a mate and produce offspring of their own. Larger or less-nuclear packs may operate differently and possess more complex and flexible social structures.
In the case of other wild canids, the alpha male may not have exclusive access to the alpha female; moreover, other pack members may guard the maternity den used by the alpha female; as with the African wild dog, Lycaon pictus.
As dominant roles may be deemed normal among social species with extended parenting, it has been suggested that the additional term alpha is not required merely to describe dominance due to its ubiquity, but should be reserved for alpha males where they are the predominant pack progenitor. For instance, wolf biologist L. David Mech stated

"calling a wolf an alpha is usually no more appropriate than referring to a human parent or a doe deer as an alpha. Any parent is dominant to its young offspring, so alpha adds no information. Why not refer to an alpha female as the female parent, the breeding female, the matriarch, or simply the mother? Such a designation emphasizes not the animal's dominant status, which is trivial information, but its role as pack progenitor, which is critical information. The one use we may still want to reserve for alpha is in the relatively few large wolf packs comprised of multiple litters. ... In such cases the older breeders are probably dominant to the younger breeders and perhaps can more appropriately be called the alphas. ... The point here is not so much the terminology but what the terminology falsely implies: a rigid, force-based dominance hierarchy."


=== Use in dog trainingEdit ===
One of the most persistent theories in dog training literature is the idea of the alpha wolf, an individual gray wolf who uses body language and, when needed, physical force to maintain its dominance within the wolf pack. The idea was first reported in early wolf research . It was subsequently adopted by dog trainers. The term alpha was popularized as early as 1976 in the dog training book How to Be Your Dog's Best Friend (Monks of New Skete), which introduced the idea of the alpha roll, a technique for punishing unwanted dog behaviours. Psychologist and dog trainer Stanley Coren in the 2001 book How to Speak Dog says "you are the alpha dog...You must communicate that you are the pack leader and dominant".
Training techniques assumed to be wolf pack related such as scruff shaking, the alpha roll and recommendations to be alpha to your dog continue to be used and recommended by some dog training instructors. It has been suggested that the use of such techniques may have more to do with human psychology than with dog behaviour; "dominance hierarchies and dominance disputes and testing are a fundamental characteristic of all social groups... But perhaps only we humans learn to use punishment primarily to gain for ourselves the reward of being dominant. Many contemporary trainers would agree, advocating the use of rewards to teach commands and encourage good communication between owners and their pets. Some canine behaviourists suggest that kind, efficient training uses games to teach commands which can be utilised to benefit the owner's everyday life.


== See alsoEdit ==
Pack hunter
Dog behavior


== ReferencesEdit ==