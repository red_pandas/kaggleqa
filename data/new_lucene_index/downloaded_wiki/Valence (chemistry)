In chemistry, the valence or valency of an element is a measure of its combining power with other atoms when it forms chemical compounds or molecules. The concept of valence was developed in the second half of the 19th century and was successful in explaining the molecular structure of inorganic and organic compounds. The quest for the underlying causes of valence led to the modern theories of chemical bonding, including the cubical atom (1902), Lewis structures (1916), valence bond theory (1927), molecular orbitals (1928), valence shell electron pair repulsion theory (1958), and all of the advanced methods of quantum chemistry.


== Description ==
The combining power or affinity of an atom of an element was determined by the number of hydrogen atoms that it combined with. In methane, carbon has a valence of 4; in ammonia, nitrogen has a valence of 3; in water, oxygen has a valence of 2; and in hydrogen chloride, chlorine has a valence of 1. Chlorine, as it has a valence of one, can be substituted for hydrogen, so phosphorus has a valence of 5 in phosphorus pentachloride, PCl5. Valence diagrams of a compound represent the connectivity of the elements, with lines drawn between two elements, sometimes called bonds, representing a saturated valency for each element. Examples are:
Valence only describes connectivity; it does not describe the geometry of molecular compounds, or what are now known to be ionic compounds or giant covalent structures. A line between atoms does not represent a pair of electrons as it does in Lewis diagrams.


== Modern definitions ==
Valence is defined by the IUPAC as:
The maximum number of univalent atoms (originally hydrogen or chlorine atoms) that may combine with an atom of the element under consideration, or with a fragment, or for which an atom of this element can be substituted.
An alternative modern description is:
The number of hydrogen atoms that can combine with an element in a binary hydride or twice the number of oxygen atoms combining with an element in its oxide or oxides.
This definition differs from the IUPAC definition as an element can be said to have more than one valence.


== Historical development ==
The etymology of the word "valence" traces back to 1425, meaning "extract, preparation", from Latin valentia "strength, capacity", from the earlier valor "worth, value", and the chemical meaning referring to the "combining power of an element" is recorded from 1884, from German Valenz.

In 1789, William Higgins published views on what he called combinations of "ultimate" particles, which foreshadowed the concept of valency bonds. If, for example, according to Higgins, the force between the ultimate particle of oxygen and the ultimate particle of nitrogen were 6, then the strength of the force would be divided accordingly, and likewise for the other combinations of ultimate particles (see illustration).
The exact inception, however, of the theory of chemical valencies can be traced to an 1852 paper by Edward Frankland, in which he combined the older theories of free radicals and type theory with thoughts on chemical affinity to show that certain elements have the tendency to combine with other elements to form compounds containing 3, i.e., in the 3-atom groups (e.g., NO3, NH3, NI3, etc.) or 5, i.e., in the 5-atom groups (e.g., NO5, NH4O, PO5, etc.), equivalents of the attached elements. It is in this manner, according to Frankland, that their affinities are best satisfied. Following these examples and postulates, Frankland declares how obvious it is that
This combining power was afterwards called quantivalence or valency (and valence by American chemists). In 1857 August Kekul proposed fixed valences for many elements, such as 4 for carbon, and used them to propose structural formulas for many organic molecules, which are still accepted today.
Most 19th-century chemists defined the valence of an element as the number of its bonds without distinguishing different types of valence or of bond. However, in 1893 Alfred Werner described transition metal coordination complexes such as [Co(NH3)6]Cl3, in which he distinguished primary and secondary valences, corresponding to the modern concepts of oxidation state and coordination number respectively.
For main-group elements, in 1904 Richard Abegg considered positive and negative valences (maximal and minimal oxidation states), and proposed Abegg's rule to the effect that their difference is often 8.


=== Electrons and valence ===
The Rutherford model of the nuclear atom (1911) showed that the exterior of an atom is occupied by electrons, which suggests that electrons are responsible for the interaction of atoms and the formation of chemical bonds. In 1916, Gilbert N. Lewis explained valence and chemical bonding in terms of a tendency of (main-group) atoms to achieve a stable octet of 8 valence-shell electrons. According to Lewis, covalent bonding leads to octets by the sharing of electrons, and ionic bonding leads to octets by the transfer of electrons from one atom to the other. The term covalence is attributed to Irving Langmuir, who stated in 1919 that "the number of pairs of electrons which any given atom shares with the adjacent atoms is called the covalence of that atom". The prefix co- means "together", so that a co-valent bond means that the atoms share a valence. Subsequent to that, it is now more common to speak of covalent bonds rather than "valence", which has fallen out of use in higher-level work from the advances in the theory of chemical bonding, but it is still widely used in elementary studies, where it provides a heuristic introduction to the subject.
In the 1930s, Linus Pauling proposed that there are also polar covalent bonds, which are intermediate between covalent and ionic, and that the degree of ionic character depends on the difference of electronegativity of the two bonded atoms.
Pauling also considered hypervalent molecules, in which main-group elements have apparent valences greater than the maximal of 4 allowed by the octet rule. For example, in the sulfur hexafluoride molecule (SF6), Pauling considered that the sulfur forms 6 true two-electron bonds using so-called sp3d2 hybrid atomic orbitals, which combine one s, three p and two d orbitals. However more recently, quantum-mechanical calculations on this and similar molecules have shown that the role of d orbitals in the bonding is minimal, and that the SF6 molecule should be described as having 6 polar covalent (partly ionic) bonds made from only four orbitals on sulfur (one s and three p) in accordance with the octet rule, together with six orbitals on the fluorines. Similar calculations on transition-metal molecules show that the role of p orbitals is minor, so that one s and five d orbitals on the metal are sufficient to describe the bonding.


== Common valences ==
For elements in the main groups of the periodic table, the valence can vary between 1 and 7.
Many elements have a common valence related to their position in the periodic table, and nowadays this is rationalised by the octet rule. The Latin/Greek prefixes uni-/mono-, bi-/di-, ter-/tri-, quadri-/tetra-, quinque-/penta- are used to describe ions in the charge states 1, 2, 3, 4, 5 respectively. Polyvalence or multivalence refers to species that are not restricted to a specific number of valence bonds. Species with a single charge are univalent (monovalent). For example, the Cs+ cation is a univalent or monovalent cation, whereas the Ca2+ cation is a divalent cation, and the Fe3+ cation is a trivalent cation. Unlike Cs and Ca, Fe can also exist in other charge states, notably 2+ and 4+, and is thus known as a multivalent (polyvalent) ion.


== Valence versus oxidation state ==
Because of the ambiguity of the term valence, nowadays other notations are used in practice. Beside the system of oxidation numbers as used in Stock nomenclature for coordination compounds, and the lambda notation, as used in the IUPAC nomenclature of inorganic chemistry, "oxidation state" is a more clear indication of the electronic state of atoms in a molecule.
The "oxidation state" of an atom in a molecule gives the number of valence electrons it has gained or lost. In contrast to the valency number, the oxidation state can be positive (for an electropositive atom) or negative (for an electronegative atom).
Elements in a high oxidation state can have a valence higher than four. For example, in perchlorates, chlorine has seven valence bonds and ruthenium, in the +8 oxidation state in ruthenium tetroxide, has eight valence bonds.


=== Examples ===
(valencies according to the number of valence bonds definition and conform oxidation states)
* The univalent perchlorate ion (ClO4) has valence 1.
** Iron oxide appears in a crystal structure, so no typical molecule can be identified.
  In ferrous oxide, Fe has oxidation number II, in ferric oxide, oxidation number III.
Examples where valences and oxidation states differ due to bonds between identical atoms:
Valences may also be different from absolute values of oxidation states due to different polarity of bonds. For example, in dichloromethane, CH2Cl2, carbon has valence 4 but oxidation state 0.


== "Maximum number of bonds" definition ==
Frankland took the view that the valence (he used the term "atomicity") of an element was a single value that corresponded to the maximum value observed. The number of unused valencies on atoms of what are now called the p-block elements is generally even, and Frankland suggested that the unused valencies saturated one another. For example, nitrogen has a maximum valence of 5, in forming ammonia two valencies are left unattached; sulfur has a maximum valence of 6, in forming hydrogen sulphide four valencies are left unattached.
The International Union of Pure and Applied Chemistry (IUPAC) has made several attempts to arrive at an unambiguous definition of valence. The current version, adopted in 1994:
The maximum number of univalent atoms (originally hydrogen or chlorine atoms) that may combine with an atom of the element under consideration, or with a fragment, or for which an atom of this element can be substituted.
Hydrogen and chlorine were originally used as examples of univalent atoms, because of their nature to form only one single bond. Hydrogen has only one valence electron and can form only one bond with an atom that has an incomplete outer shell. Chlorine has seven valence electrons and can form only one bond with an atom that donates a valence electron to complete chlorine's outer shell. However, chlorine can also have oxidation states from +1 to +7 and can form more than one bond by donating valence electrons.
Although hydrogen has only one valence electron, it can form bonds with more than one atom. In the bifluoride ion ([HF
2]), for example, it forms a three-center four-electron bond with two fluoride atoms:
[ FH F  F HF ]
Another example is the Three-center two-electron bond in diborane (B2H6).


== Maximum valences of the elements ==
Maximum valences for the elements are based on the data from list of oxidation states of the elements.


== See also ==
Abegg's rule
Oxidation state
Valence electron


== References ==