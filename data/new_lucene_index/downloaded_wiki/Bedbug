Bed bugs, bed-bugs, or bedbugs are parasitic insects of the cimicid family that feed exclusively on blood. Cimex lectularius, the common bed bug, is the best known, as it prefers to feed on human blood. Other Cimex species specialize in other animals, e.g., bat bugs, such as Cimex pipistrelli (Europe), Cimex pilosellus (western US), and Cimex adjunctus (entire eastern US).
The name "bed bug" derives from the preferred habitat of Cimex lectularius: warm houses and especially near or inside beds and bedding or other sleep areas. Bed bugs are mainly active at night, but are not exclusively nocturnal. They usually feed on their hosts without being noticed.
A number of adverse health effects may result from bed bug bites, including skin rashes, psychological effects, and allergic symptoms. They are not known to transmit any pathogens as disease vectors. Certain signs and symptoms suggest the presence of bed bugs; finding the insects confirms the diagnosis.
Bed bugs have been known as human parasites for thousands of years. At a point in the early 1940s, they were mostly eradicated in the developed world, but have increased in prevalence since 1995, likely due to pesticide resistance and governmental bans on effective pesticides. Because infestation of human habitats has been on the increase, bed bug bites and related conditions have been on the rise as well.


== Infestation ==

Diagnosis of an infestation involves both finding bed bugs and the occurrence of compatible symptoms. Treatment involves the elimination of the insect (including its eggs) and measures to help with the symptoms until they resolve.
Bed bug bites or cimicosis may lead to a range of skin manifestations from no visible effects to prominent blisters. Effects include skin rashes, psychological effects, and allergic symptoms.
They can be infected by at least 28 human pathogens, but no study has clearly found that the insect can transmit the pathogen to a human being. They have been found with methicillin-resistant Staphylococcus aureus (MRSA) and with vancomycin-resistant Enterococcus faecium (VRE), but the significance of this is still unknown.
Investigations into potential transmission of HIV, MRSA, hepatitis B, hepatitis C, and hepatitis E have not shown that bed bugs can spread these diseases. However, it may be possible that arboviruses are transmissible.


== Description ==


=== Physical ===
Adult bed bugs are light brown to reddish-brown, flattened, oval-shaped, and have no hind wings. The front wings are vestigial and reduced to pad-like structures. Bed bugs have segmented abdomens with microscopic hairs that give them a banded appearance. Adults grow to 45 mm (0.160.20 in) long and 1.53 mm (0.0590.118 in) wide.
Newly hatched nymphs are translucent, lighter in color, and become browner as they moult and reach maturity. A bed bug nymph of any age that has just consumed a blood meal has a bright red, translucent abdomen, fading to brown over the next several hours, and to opaque black within two days as the insect digests its meal. Bed bugs may be mistaken for other insects, such as booklice, small cockroaches, or carpet beetles; however, when warm and active, their movements are more ant-like and, like most other true bugs, they emit a characteristic disagreeable odor when crushed.
Bed bugs use pheromones and kairomones to communicate regarding nesting locations, feeding, and reproduction.
The lifespan of bed bugs varies by species and is also dependent on feeding.
Bed bugs can survive a wide range of temperatures and atmospheric compositions. Below 16.1 C (61.0 F), adults enter semihibernation and can survive longer; they can survive for at least five days at 10 C (14 F), but die after 15 minutes of exposure to 32 C (26 F). Common commercial and residential freezers reach temperatures low enough to kill most life stages of bed bug, with 95% mortality after 3 days at 12 C (10 F). They show high desiccation tolerance, surviving low humidity and a 3540 C range even with loss of one-third of body weight; earlier life stages are more susceptible to drying out than later ones.
The thermal death point for C. lectularius is 45 C (113 F); all stages of life are killed by 7 minutes of exposure to 46 C (115 F). Bed bugs apparently cannot survive high concentrations of carbon dioxide for very long; exposure to nearly pure nitrogen atmospheres, however, appears to have relatively little effect even after 72 hours.


=== Feeding habits ===

Bed bugs are obligatory hematophagous (bloodsucking) insects. Most species feed on humans only when other prey are unavailable. They obtain all the additional moisture they need from water vapor in the surrounding air. Bed bugs are attracted to their hosts primarily by carbon dioxide, secondarily by warmth, and also by certain chemicals. Bedbugs prefer exposed skin, preferably the face, neck, and arms of a sleeping person.
Bedbugs have mouth parts that saw through the skin, and inject saliva with anticoagulants and painkillers. Sensitivity of humans varies from extreme allergic reaction to no reaction at all (about 20%). The bite usually produces a swelling with no red spot, but when many bugs feed on a small area, reddish spots may appear after the swelling subsides.
Although under certain cool conditions adult bed bugs can live for over a year without feeding, under typically warm conditions they try to feed at five- to ten-day intervals, and adults can survive for about five months without food. Younger instars cannot survive nearly as long, though even the vulnerable newly hatched first instars can survive for weeks without taking a blood meal.
At the 57th annual meeting of the Entomological Society of America in 2009, newer generations of pesticide-resistant bed bugs in Virginia were reported to survive only two months without feeding.
DNA from human blood meals can be recovered from bed bugs for up to 90 days, which mean they can be used for forensic purposes in identifying on whom the bed bugs have fed.


==== Feeding physiology ====

A bed bug pierces the skin of its host with a stylet fascicle, rostrum, or "beak". The rostrum is composed of the maxillae and mandibles, which have been modified into elongated shapes from a basic, ancestral style. The right and left maxillary stylets are connected at their midline and a section at the centerline forms a large food canal and a smaller salivary canal. The entire maxillary and mandibular bundle penetrates the skin.
The tips of the right and left maxillary stylets are not the same; the right is hook-like and curved, and the left is straight. The right and left mandibular stylets extend along the outer sides of their respective maxillary stylets and do not reach anywhere near the tip of the fused maxillary stylets. The stylets are retained in a groove in the labium, and during feeding, they are freed from the groove as the jointed labium is bent or folded out of the way; its tip never enters the wound.
The mandibular stylet tips have small teeth, and through alternately moving these stylets back and forth, the insect cuts a path through tissue for the maxillary bundle to reach an appropriately sized blood vessel. Pressure from the blood vessel itself fills the insect with blood in three to five minutes. The bug then withdraws the stylet bundle from the feeding position and retracts it back into the labial groove, folds the entire unit back under the head, and returns to its hiding place. It takes between five and ten minutes for a bed bug to become completely engorged with blood. In all, the insect may spend less than 20 minutes in physical contact with its host, and does not try to feed again until it has either completed a moult or, if an adult, has thoroughly digested the meal.


=== Reproduction ===

All bed bugs mate by traumatic insemination. Female bed bugs possess a reproductive tract that functions during oviposition, but the male does not use this tract for sperm insemination. Instead, the male pierces the female's abdomen with his hypodermic penis and ejaculates into the body cavity. In all bed bug species except Primicimex cavernis, sperm are injected into the mesospermalege, a component of the spermalege, a secondary genital structure that reduces the wounding and immunological costs of traumatic insemination. Injected sperm travel via the haemolymph (blood) to sperm storage structures called seminal conceptacles, with fertilisation eventually taking place at the ovaries.
Male bed bugs sometimes attempt to mate with other males and pierce their abdomens. This behaviour occurs because sexual attraction in bed bugs is based primarily on size, and males mount any freshly fed partner regardless of sex. The "bed bug alarm pheromone" consists of (E)-2-octenal and (E)-2-hexenal. It is released when a bed bug is disturbed, as during an attack by a predator. A 2009 study demonstrated the alarm pheromone is also released by male bed bugs to repel other males that attempt to mate with them.
Cimex lectularius and C. hemipterus mate with each other given the opportunity, but the eggs then produced are usually sterile. In a 1988 study, one of 479 eggs was fertile and resulted in a hybrid, Cimex hemipterus  lectularius.


==== Sperm protection ====
Cimex lectularius males have environmental microbes on their genitals. These microbes damage sperm cells, leaving them unable to fertilize female gametes. Due to these dangerous microbes, males have evolved antimicrobial ejaculate substances that prevent sperm damage. When the microbes contact sperm or the male genitals, the bed bug releases antimicrobial substances. Many species of these microbes live in the bodies of females after mating. The microbes can cause infections in the females. It has been suggested that females receive benefit from the ejaculate. Though the benefit is not direct, females are able to produce more eggs than optimum increasing the amount of the females' genes in the gene pool.


==== Sperm and seminal fluid allocation ====
In organisms, sexual selection extends past differential reproduction to affect sperm composition, sperm competition, and ejaculate size. Males of C. lectularius allocate 12% of their sperm and 19% of their seminal fluid per mating. Due to these findings, Reinhard et. al proposed that multiple mating is limited by seminal fluid and not sperm. After measuring ejaculate volume, mating rate and estimating sperm density, Reinhardt et al. showed that mating could be limited by seminal fluid. Despite these advances, the cost difference between ejaculate-dose dependence and mating frequency dependence have not been explored.


==== Egg production ====
Males fertilize females only by traumatic insemination into the structure called the ectospermalege (the organ of Berlese, however the organ of Ribaga (as it was first named) was first designated as an organ of stridulation. These two names are not descriptive, so other terminologies are used). On fertilization, the female's ovaries finish developing, which suggests that sperm plays a role other than fertilizing the egg. Fertilization also allows for egg production through the corpus allatum. Sperm remains viable in a female's spermathecae (a better term is conceptacle), a sperm-carrying sack, for a long period of time as long as body temperature is optimum. The female lays fertilized eggs until she depletes the sperm found in her conceptacle. After the depletion of sperm, she lays a few sterile eggs. The number of eggs a C. lectularius female produces does not depend on the sperm she harbors, but on the female's nutritional level.


==== Alarm pheromones ====
In C. lectularius, males sometimes mount other males because male sexual interest is directed at any recently fed individual regardless of their sex, but unfed females may also be mounted. Traumatic insemination is the only way for copulation to occur in bed bugs. Females have evolved the spermalege to protect themselves from wounding and infection. Because males lack this organ, traumatic insemination could leave them injured badly. For this reason, males have evolved alarm pheromones to signal their sex to other males. If a male C. lectularius mounts another male, the mounted male releases the pheromone signal and the male on top stops before insemination.
Females are capable of producing alarm pheromones to avoid multiple mating, but they generally do not do so. Two reasons are proposed as to why females do not release alarm pheromones to protect themselves. First, alarm pheromone production is costly. Due to egg production, females may refrain from spending additional energy on alarm pheromones. The second proposed reason is that releasing the alarm pheromone reduces the benefits associated with multiple mating. Benefits of multiple mating include material benefits, better quality nourishment or more nourishment, genetic benefits including increased fitness of offspring, and finally, the cost of resistance may be higher than the benefit of consentwhich appears the case in C. lectularius.


=== Life stages ===
Bed bugs have five immature nymph life stages and a final sexually mature adult stage. They shed their skins through ecdysis at each stage, discarding their outer exoskeleton, which is somewhat clear, empty exoskeletons of the bugs themselves. Bed bugs must molt six times before becoming fertile adults, and must consume at least one blood meal to complete each moult.
Each of the immature stages lasts about a week, depending on temperature and the availability of food, and the complete lifecycle can be completed in as little as two months (rather long compared to other ectoparasites). Fertilized females with enough food lay three to four eggs each day continually until the end of their lifespans (about nine months under warm conditions), possibly generating as many as 500 eggs in this time. Genetic analysis has shown that a single pregnant bed bug, possibly a single survivor of eradication, can be responsible for an entire infestation over a matter of weeks, rapidly producing generations of offspring.


=== Sexual dimorphism ===
Sexual dimorphism occurs in C. lectularius, with the females larger in size than the males on average. The abdomens of the sexes differ in that the males appear to have "pointed" abdomens, which are actually their copulatory organs, while females have more rounded abdomens. Since males are attracted to large body size, any bed bug with a recent blood meal can be seen as a potential mate. However, males will mount unfed, flat females on occasion. The female is able to curl her abdomen forward and underneath toward the head to not mate. Males are generally unable to discriminate between the sexes until after mounting, but before inseminating.


=== Host searching ===
C. lectularius only feeds every five to seven days, which suggests that it does not spend the majority of its life searching for a host. When a bed bug is starved, it leaves its shelter and searches for a host. If it successfully feeds, it returns to its shelter. If it does not feed, it continues to search for a host. After searchingregardless of whether or not it has eatenthe bed bug returns to the shelter to aggregate before the photophase (period of light during a day-night cycle). Reis argues that two reasons explain why C. lectularius would return to its shelter and aggregate after feeding. One is to find a mate and the other is to find shelter to avoid getting smashed after eating.


=== Aggregation and dispersal behavior ===
C. lectularius aggregates under all life stages and mating conditions. Bed bugs may choose to aggregate because of predation, resistance to desiccation, and more opportunities to find a mate. Airborne pheromones are responsible for aggregations. Another source of aggregation could be the recognition of other C. lectularius bugs through mechanoreceptors located on their antennae. Aggregations are formed and disbanded based on the associated cost and benefits. Females are more often found separate from the aggregation than males. Females are more likely to expand the population range and find new sites. Active female dispersal can account for treatment failures. Males, when found in areas with few females, abandon an aggregation to find a new mate. The males excrete an aggregation pheromone into the air that attracts virgin females and arrests other males.


== Detection ==

Bed bugs can exist singly, but tend to congregate once established. Though strictly parasitic, they spend only a tiny fraction of their lifecycles physically attached to hosts. Once a bed bug finishes feeding, it relocates to a place close to a known host, commonly in or near beds or couches in clusters of adults, juveniles, and eggswhich entomologists call harborage areas or simply harborages to which the insect returns after future feedings by following chemical trails. These places can vary greatly in format, including luggage, inside of vehicles, within furniture, amongst bedside cluttereven inside electrical sockets and nearby laptop computers. Bed bugs may also nest near animals that have nested within a dwelling, such as bats, birds, or rodents. They are also capable of surviving on domestic cats and dogs, though humans are the preferred host of C. lectularius.
Bed bugs can also be detected by their characteristic smell of rotting raspberries. Once you smell this scent, however, you can be assured of a massive infestation. Bed bug detection dogs are trained to pinpoint infestations, with a possible accuracy rate between 11% and 83%.


== Management ==

Eradication of bed bugs frequently requires a combination of nonpesticide approaches and the occasional use of pesticides.
Mechanical approaches, such as vacuuming up the insects and heat-treating or wrapping mattresses, are effective. A combination of heat and drying treatments is most effective. An hour at a temperature of 45 C (113 F) or over, or two hours at less than 17 C (1 F) kills them; a domestic clothes drier or steam kills bedbugs. Another study found 100% mortality rates for bed bugs exposed to temperatures greater than 50 C (122 F) for more than 2 minutes. Starving them is difficult as they can survive without eating for 100 to 300 days, depending on temperature. For public health reasons, individuals are encouraged to call a professional pest control service to eradicate bed bugs in a home, rather than attempting to do it themselves, particularly if they live in a multifamily building.
As of 2012, no truly effective pesticides were available. Pesticides that have historically been found effective include pyrethroids, dichlorvos, and malathion. Resistance to pesticides has increased significantly over time, and harm to health from their use is of concern. The carbamate insecticide propoxur is highly toxic to bed bugs, but it has potential toxicity to children exposed to it, and the US Environmental Protection Agency has been reluctant to approve it for indoor use. Boric acid, occasionally applied as a safe indoor insecticide, is not effective against bed bugs because they do not groom. The fungus Beauveria bassiana is being researched as of 2012 for its ability to control bed bugs. As bed bugs continue to adapt pesticide resistance, researchers have examined on the insect's genome to see how the adaptations develop and to look for potential vulnerabilities that can be exploited in the growth and development phases. 


== Predators ==
Natural enemies of bed bugs include the masked hunter insect (also known as "masked bed bug hunter"), cockroaches, ants, spiders (particularly Thanatus flavidus), mites, and centipedes (particularly the house centipede Scutigera coleoptrata). However, biological pest control is not considered practical for eliminating bed bugs from human dwellings.


== Epidemiology ==

Bed bugs occur around the world. Rates of infestations in developed countries, while decreasing from the 1930s to the 1980s, have increased dramatically since the 1980s. Previously, they were common in the developing world, but rare in the developed world. The increase in the developed world may have been caused by increased international travel, resistance to insecticides, and the use of new pest-control methods that do not affect bed bugs.
The fall in bed bug populations after the 1930s in the developed world is believed partly due to the use of DDT to kill cockroaches. The invention of the vacuum cleaner and simplification of furniture design may have also played a role. Others believe it might simply be the cyclical nature of the organism.
The exact causes of this resurgence remain unclear; it is variously ascribed to greater foreign travel, increased immigration from the developing world to the developed world, more frequent exchange of second-hand furnishings among homes, a greater focus on control of other pests, resulting in neglect of bed bug countermeasures, and increasing resistance to pesticides. Declines in household cockroach populations that have resulted from the use of insecticides effective against this major bed bug predator have aided the bed bugs' resurgence, as have bans on DDT and other potent pesticides.
The common bed bug (C. lectularius) is the species best adapted to human environments. It is found in temperate climates throughout the world. Other species include Cimex hemipterus, found in tropical regions, which also infests poultry and bats, and Leptocimex boueti, found in the tropics of West Africa and South America, which infests bats and humans. Cimex pilosellus and Cimex pipistrella primarily infest bats, while Haematosiphon inodora, a species of North America, primarily infests poultry.


== History ==

C. lectularius may have originated in the Middle East in caves inhabited by bats and humans.
Bed bugs were mentioned in ancient Greece as early as 400 BC, and were later mentioned by Aristotle. Pliny's Natural History, first published circa 77 AD in Rome, claimed bed bugs had medicinal value in treating ailments such as snake bites and ear infections. (Belief in the medicinal use of bed bugs persisted until at least the 18th century, when Guettard recommended their use in the treatment of hysteria.)
Bed bugs were first mentioned in Germany in the 11th century, in France in the 13th century, and in England in 1583, though they remained rare in England until 1670. Some in the 18th century believed bed bugs had been brought to London with supplies of wood to rebuild the city after the Great Fire of London (1666). Giovanni Antonio Scopoli noted their presence in Carniola (roughly equivalent to present-day Slovenia) in the 18th century.
Traditional methods of repelling and/or killing bed bugs include the use of plants, fungi, and insects (or their extracts), such as black pepper; black cohosh (Actaea racemosa); Pseudarthria hookeri; Laggera alata (Chinese yngmo co | ); Eucalyptus saligna oil; henna (Lawsonia inermis or camphire); "infused oil of Melolontha vulgaris" (presumably cockchafer); fly agaric (Amanita muscaria); Actaea spp. (e.g. black cohosh); tobacco; "heated oil of Terebinthina" (i.e. true turpentine); wild mint (Mentha arvensis); narrow-leaved pepperwort (Lepidium ruderale); Myrica spp. (e.g. bayberry); Robert geranium (Geranium robertianum); bugbane (Cimicifuga spp.); "herb and seeds of Cannabis"; "opulus" berries (possibly maple or European cranberrybush); masked hunter bugs (Reduvius personatus), "and many others".
In the mid-19th century, smoke from peat fires was recommended as an indoor domestic fumigant against bed bugs.
Dusts have been used to ward off insects from grain storage for centuries, including "plant ash, lime, dolomite, certain types of soil, and diatomaceous earth or Kieselguhr". Of these, diatomaceous earth in particular has seen a revival as a nontoxic (when in amorphous form) residual pesticide for bed bug abatement. While diatomaceous earth performed poorly, silica gel maybe effective.
Basket-work panels were put around beds and shaken out in the morning in the UK and in France in the 19th century. Scattering leaves of plants with microscopic hooked hairs around a bed at night, then sweeping them up in the morning and burning them, was a technique reportedly used in Southern Rhodesia and in the Balkans.
Bean leaves have been used historically to trap bedbugs in houses in Eastern Europe. The trichomes on the bean leaves capture the insects by impaling the feet (tarsi) of the insects. The leaves are then destroyed.


=== 20th century ===
Prior to the mid-20th century, bed bugs were very common. According to a report by the UK Ministry of Health, in 1933, all the houses in many areas had some degree of bed bug infestation. The increase in bed bug populations in the early 20th century has been attributed to the advent of electric heating, which allowed bed bugs to thrive year-round instead of only in warm weather.
Bed bugs were a serious problem at U.S. military bases during World War II. Initially, the problem was solved by fumigation, using Zyklon Discoids that released hydrogen cyanide gas, a rather dangerous procedure. Later, DDT was used to good effect as a safer alternative.
The decline of bed bug populations in the 20th century is often credited to potent pesticides that had not previously been widely available. Other contributing factors that are less frequently mentioned in news reports are increased public awareness and slum clearance programs that combined pesticide use with steam disinfection, relocation of slum dwellers to new housing, and in some cases also follow-up inspections for several months after relocated tenants moved into their new housing.


==== Resurgence ====
Bed bug infestations resurged since the 1980s for reasons that are not clear, but contributing factors may be complacency, increased resistance, bans on pesticides, and increased international travel. The U.S. National Pest Management Association reported a 71% increase in bed bug calls between 2000 and 2005. The number of reported incidents in New York City alone rose from 500 in 2004 to 10,000 in 2009. In 2013, Chicago was listed as the number 1 city in the United States with the worst bed bug infestation. As a result, the Chicago City Council passed a bed bug control ordinance to limit their spread. Additionally, bed bugs are reaching places in which they never established before, such as southern South America.
One recent theory about bed bug reappearance in the US is that they never truly disappeared, but may have been forced to alternative hosts. Consistent with this is the finding that bed bug DNA shows no evidence of an evolutionary bottleneck. Furthermore, investigators have found high populations of bed bugs at poultry facilities in Arkansas. Poultry workers at these facilities may be spreading bed bugs, unknowingly carrying them to their places of residence and elsewhere after leaving work.


== Society and culture ==
The saying "Good night, sleep tight, don't let the bed bugs bite" is common for parents to say to young children before they go to sleep. In Chhattisgarh, India, bed bugs have been used as a traditional medicine for epilepsy, piles, alopecia, and urinary disorders, but this practice has no scientific basis. Bed bug secretions can inhibit the growth of some bacteria and fungi; antibacterial components from the bed bug could be used against human pathogens, and be a source of pharmacologically active molecules as a resource for the discovery of new drugs.


=== Etymology ===
The word 'bug' and its earlier spelling 'bugge' originally meant bed bug. Many other creatures are called bugs, such as the ladybug (ladybird outside North America), the potatobug, or the informal use of the word for any insect, or even for microscopic germs, or diseases caused by these germs, but the earliest recorded use of the actual word 'bug' was to mean bed bug.
The term bed bug may also be spelled bedbug or bed-bug, though published sources consistently use the unhyphenated two-word name bed bug. They have been known by a variety of other names, including wall louse, mahogany flat, crimson rambler, chilly billies, heavy dragoon, chinche bug, and redcoat.


== References ==


== Further reading ==
Stephen Doggett. A Code of Practice for the Control of Bed Bugs in Australia. Draft 4th edition, ICPMR & AEPMA, Sydney Australia, September 2011. ISBN 1-74080-135-0."Bed Bug Home Page". Bedbug.org.au. 2005-10-14. Retrieved 2013-11-11. 


== External links ==
bed bug on the University of Florida/IFAS Featured Creatures Web site
Pollack, Richard; Alpert, Gary (2005). "Bedbugs: Biology and Management". Harvard School of Public Health. Archived from the original on 20 June 2010. Retrieved 2010-06-21. 
National Geographic segment on Bed bugs on YouTube
Bed Bug Fact Sheet highlights prevention tips as well as information on habits, habitat and health threats
Bed bugs  University of Sydney and Westmead Hospital Department of Medical Entomology
Understanding and Controlling Bed Bugs  National Pesticide Information Center
CISR: Center for Invasive Species Research More information on Bed Bugs, with lots of photos and video
EPA bedbugs information page