The sensory nervous system is a part of the nervous system responsible for processing sensory information. A sensory system consists of sensory receptors, neural pathways, and parts of the brain involved in sensory perception. Commonly recognized sensory systems are those for vision, auditory (hearing), somatic sensation (touch), gustatory (taste), olfaction (smell) and vestibular (balance/movement). In short, senses are transducers from the physical world to the realm of the mind where we interpret the information, creating our perception of the world around us.
The receptive field is the specific part of the world to which a receptor organ and receptor cells respond. For instance, the part of the world an eye can see, is its receptive field; the light that each rod or cone can see, is its receptive field. Receptive fields have been identified for the visual system, auditory system and somatosensory system, so far.


== Stimulus ==
Sensory systems code for four aspects of a stimulus; type (modality), intensity, location, and duration. Arrival time of a sound pulse and phase differences of continuous sound are used for localization of sound sources. Certain receptors are sensitive to certain types of stimuli (for example, different mechanoreceptors respond best to different kinds of touch stimuli, like sharp or blunt objects). Receptors send impulses in certain patterns to send information about the intensity of a stimulus (for example, how loud a sound is). The location of the receptor that is stimulated gives the brain information about the location of the stimulus (for example, stimulating a mechanoreceptor in a finger will send information to the brain about that finger). The duration of the stimulus (how long it lasts) is conveyed by firing patterns of receptors. These impulses are transmitted to the brain through afferent neurons.


== Senses and receptors ==
While there is debate among neurologists as to the specific number of senses due to differing definitions of what constitutes a sense, Gautama Buddha and Aristotle classified five traditional human senses which have become universally accepted: touch, taste, smell, sight, and hearing. Other senses that have been well-accepted in most mammals, including humans, include nociception, equilibrioception, kinaesthesia, and thermoception. Furthermore, some non-human animals have been shown to possess alternate senses, including magnetoception and electroreception.


=== Receptors ===

The initialization of sensation stems from the response of a specific receptor to a physical stimulus. The receptors which react to the stimulus and initiate the process of sensation are commonly characterized in four distinct categories: chemoreceptors, photoreceptors, mechanoreceptors, and thermoreceptors. All receptors receive distinct physical stimuli and transduce the signal into an electrical action potential. This action potential then travels along afferent neurons to specific brain regions where it is processed and interpreted.


=== Chemoreceptors ===

Chemoreceptors, or chemosensors, detect certain chemical stimuli and transduce that signal into an electrical action potential. There are two primary types of chemoreceptors:
Distance chemoreceptors are integral to receiving stimuli in the olfactory system through both olfactory receptor neurons and neurons in the vomeronasal organ.
Direct chemoreceptors include the taste buds in the gustatory system as well as receptors in the aortic bodies which detect changes in oxygen concentration.


=== Photoreceptors ===

Photoreceptors are capable of phototransduction, a process which converts light (electromagnetic radiation) into, among other types of energy, a membrane potential. There are three primary types of photoreceptors: Cones are photoreceptors which respond significantly to color. In humans the three different types of cones correspond with a primary response to short wavelength (blue), medium wavelength (green), and long wavelength (yellow/red). Rods are photoreceptors which are very sensitive to the intensity of light, allowing for vision in dim lighting. The concentrations and ratio of rods to cones is strongly correlated with whether an animal is diurnal or nocturnal. In humans rods outnumber cones by approximately 20:1, while in nocturnal animals, such as the tawny owl, the ratio is closer to 1000:1. Ganglion Cells reside in the adrenal medulla and retina where they are involved in the sympathetic response. Of the ~1.3 million ganglion cells present in the retina, 1-2% are believed to be photosensitive.


=== Mechanoreceptors ===

Mechanoreceptors are sensory receptors which respond to mechanical forces, such as pressure or distortion. While mechanoreceptors are present in hair cells and play an integral role in the vestibular and auditory system, the majority of mechanoreceptors are cutaneous and are grouped into four categories:
Slowly Adapting type 1 Receptors have small receptive fields and respond to static stimulation. These receptors are primarily used in the sensations of form and roughness.
Slowly Adapting type 2 Receptors have large receptive fields and respond to stretch. Similarly to type 1, they produce sustained responses to a continued stimuli.
Rapidly Adapting Receptors have small receptive fields and underlie the perception of slip.
Pacinian Receptors have large receptive fields and are the predominant receptors for high frequency vibration.


=== Thermoreceptors ===

Thermoreceptors are sensory receptors which respond to varying temperatures. While the mechanisms through which these receptors operate is unclear, recent discoveries have shown that mammals have at least two distinct types of thermoreceptors:
The End-Bulb of Krause, or bulboid corpuscle, detects temperatures above body temperature
Ruffinis end organ detects temperatures below body temperature


=== Nociceptors ===

Nociceptors respond to potentially damaging stimuli by sending signals to the spinal cord and brain. This process, called nociception, usually causes the perception of pain. They are found in internal organs as well as on the surface of the body. Nociceptors detect different kinds of damaging stimuli or actual damage. Those that only respond when tissues are damaged are known as "sleeping" or "silent" nociceptors.
Thermal nociceptors are activated by noxious heat or cold at various temperatures.
Mechanical nociceptors respond to excess pressure or mechanical deformation.
Chemical nociceptors respond to a wide variety of chemicals, some of which are signs of tissue damage. They are involved in the detection of some spices in food.


== Sensory cortex ==
All stimuli received by the receptors listed above are transduced to an action potential, which is carried along one or more afferent neurons towards a specific area of the brain. While the term sensory cortex is often used informally to refer to the somatosensory cortex, the term more accurately refers to the multiple areas of the brain at which senses are received to be processed. For the five traditional senses in humans, this includes the primary and secondary cortexes of the different senses: the somatosensory cortex, the visual cortex, the auditory cortex, the primary olfactory cortex, and the gustatory cortex.


=== Somatosensory cortex ===
Located in the parietal lobe, the somatosensory cortex is the primary receptive area for the sense of touch. This cortex is further divided into Brodmann areas 1, 2, and 3. Brodmann area 3 is considered the primary processing center of the somatosensory cortex as it receives significantly more input from the thalamus, has neurons highly responsive to somatosensory stimuli, and can evoke somatic sensations through electrical stimulation. Areas 1 and 2 receive most of their input from area 3.


=== Visual cortex ===
The visual cortex refers to the primary visual cortex, labeled V1 or Brodmann area 17, as well as the extrastriate visual cortical areas V2-V5. Located in the occipital lobe, V1 acts as the primary relay station for visual input, transmitting information to two primary pathways labeled the dorsal and ventral streams. The dorsal stream includes areas V2 and V5, and is used in interpreting visual where and how. The ventral stream includes areas V2 and V4, and is used in interpreting what. Increases in Task-negative activity are observed in the ventral attention network, after abrupt changes in sensory stimuli, at the onset and offset of task blocks, and at the end of a completed trial.


=== Auditory cortex ===
Located in the temporal lobe, the auditory cortex is the primary receptive area for sound information. The auditory cortex is composed of Brodmann areas 41 and 42, also known as the anterior transverse temporal area 41 and the posterior transverse temporal area 42, respectively. Both areas act similarly and are integral in receiving and processing the signals transmitted from auditory receptors.


=== Primary olfactory cortex ===
Located in the temporal lobe, the primary olfactory cortex is the primary receptive area for olfaction, or smell. Unique to the olfactory and gustatory systems, at least in mammals, is the implementation of both peripheral and central mechanisms of action. The peripheral mechanisms involve olfactory receptor neurons which transduce a chemical signal along the olfactory nerve, which terminates in the olfactory bulb. The chemo-receptors involved in olfactory nervous cascade involve using G-protein receptors to send their chemical signals down said cascade. The central mechanisms include the convergence of olfactory nerve axons into glomeruli in the olfactory bulb, where the signal is then transmitted to the anterior olfactory nucleus, the piriform cortex, the medial amygdala, and the entorhinal cortex, all of which make up the primary olfactory cortex.


=== Gustatory cortex ===
The gustatory cortex is the primary receptive area for taste, or gustation. The gustatory cortex consists of two primary structures: the anterior insula, located on the insular lobe, and the frontal operculum, located on the frontal lobe. Similarly to the olfactory cortex, the gustatory pathway operates through both peripheral and central mechanisms. Peripheral taste receptors, located on the tongue, soft palate, pharynx, and esophagus, transmit the received signal to primary sensory axons, where the signal is projected to the nucleus of the solitary tract in the medulla, or the gustatory nucleus of the solitary tract complex. The signal is then transmitted to the thalamus, which in turn projects the signal to several regions of the neocortex, including the gustatory cortex.


== Modality ==
A stimulus modality (sensory modality) is a type of physical phenomenon that can be sensed. Examples are temperature, taste, sound, and pressure. The type of sensory receptor activated by a stimulus plays the primary role in coding the stimulus modality.
In the memory-prediction framework, Jeff Hawkins mentions a correspondence between the six layers of the cerebral cortex and the six layers of the optic tract of the visual system. The visual cortex has areas labelled V1, V2, V3, V4, V5, MT, IT, etc. Thus Area V1 mentioned below, is meant to signify only one class of cells in the brain, for which there can be many other cells which are also engaged in vision.
Hawkins lays out a scheme for the analogous modalities of the sensory system. Note that there can be many types of senses, some not mentioned here. In particular, for humans, there will be cells which can be labelled as belonging to V1, V2 A1, A2, etc.:


=== V1 (vision) ===

Visual Area 1, or V1, is used for vision, via the visual system to the primary visual cortex.
The sense of sight or vision enables us to see our external environment. It provides the richest and most detailed source of sensory information. It tells us the things that are around us; gives their locations; and provides three-dimensional images of them (in color and motion). The stimulus that provides all this information is light (the part of the electromagnetic spectrum with wavelength in the range of 400 to 700nm). In mammals, the visual stimulus is detected by photoreceptors in specialized sense organ called eyes. Many have a pair of eyes located anteriorly in the head. They are like television camera producing continuous, ever-changing images of what they see. Eye: the Organ of Sight Each eye is a spherical organ known as the eyeball. It is located in a socket in the skull. Only a small portion of the front part of the eyeball can be seen. This part is protected by an upper and lower eyelid. Tear glands under the upper eyelid produce a saline tear fluid (made up of sodium chloride and hydrogen carbonate). We blink automatically every two to ten seconds. This action spreads tear fluid over the eye surfaces cleaning and keeping them moist. Tear fluid also contains a lysozyme which attacks germs. This fluid drains from the surface of the eyes into the nasal cavity via tear ducts. Each eyeball is attached to the eye sockets by six sets of muscles. These muscles enable the eye to move in many directions, thus widening the field of vision. Among these muscles is a single thicj nerve, the optic nerve, which connects the eye to the brain. Structure of the Eye The inside of the eyeball is a fluid-filled hollow structure. Functionally, it is most important structures are the lens and retina The wall of the eyeball consist of three layers:  The outermost sclera,  The middle choroid, and  The innermost retina. The sclera is a white tough layer of connective tissue which protects and maintains the shape of the eyeball. It bulges out in front to form the transparent cornea. At the back, it is perforated by the optic nerve. A thin transparent membrane, the conjunctiva, lines the inside of the eyelids and covers the cornea protectively. The choroid consist of black pigmented cells, and has a rich supply of blood capillaries. In front, it forms the muscular ciliary body and the iris. Suspensory ligaments from the ciliary body hold the lens in place. The lens is a transparent, biconvex structure which is flexible. The space in front of the lens is filled with water aqueous humor. The much bigger space behind is filled with jelly-like vitreous humor, which helps to maintain the spherical shape of the eyeball. The iris is the colored opaque disc of muscular tissue that lies in front of the lens. The hole in the center of the iris is called pupil. Light enters the eye through the pupil. The retina is the light-sensitive inside layer at the back of the eyeball. It gets it nourishment from the capillaries in the choroid. It contains numerous photoreceptors which are of two types:

The rods which are extremely sensitive to light and are responsible for black and white vision (and night vision); and

The cones which are responsible for color vision.
These sensory cells are partly embedded in the pigmented cells of the choroid. They synapse with bipolar neurons which synapse with sensory neurons that are part of the optic nerve


=== A1 (auditory - hearing) ===

Auditory Area 1, or A1, is for hearing, via the auditory system, the primary auditory cortex.


=== S1 (somatosensory - touch and proprioception) ===

Somatosensory Area 1, or S1, is for touch and proprioception in the somatosensory system. The somatosensory system feeds the Brodmann Areas 3, 1 and 2 of the primary somatosensory cortex. But there are also pathways for proprioception (via the cerebellum), and motor control (via Brodmann area 4). See also: S2 Secondary somatosensory cortex.


=== G1 (gustatory - taste) ===

Gustatory Area 1, or G1, is used for taste. Taste does not refer to the flavor of food, because the flavor of the food refers to the odor. The five qualities of taste include sourness, bitterness, sweetness, saltiness, and a recently discovered protein taste quality called umami.


=== O1 (olfactory - smell) ===

Olfactory Area 1, or O1, is used for smell. In contrast to vision and hearing, the olfactory bulbs are not cross-hemispheric; the right bulb connects to the right hemisphere and the left bulb connects to the left hemisphere.


== Human sensory system ==
The Human sensory system consists of the following sub-systems:
Visual system consists of the photoreceptor cells, optic nerve, and V1.
Auditory system
Somatosensory system consists of the receptors, transmitters (pathways) leading to S1, and S1 that experiences the sensations labelled as touch or pressure, temperature (warm or cold), pain (including itch and tickle), and the sensations of muscle movement and joint position including posture, movement, and facial expression (collectively also called proprioception).
Gustatory system
Olfactory system
Vestibular system


== Diseases ==

Amblyopia
Anacusis
Astigmatism (eye)
Cataract
Color blindness
Deafness
Hyperopia


== See also ==
Multisensory integration
Neural adaptation
Sensor
Sensory neuroscience


== References ==


== External links ==