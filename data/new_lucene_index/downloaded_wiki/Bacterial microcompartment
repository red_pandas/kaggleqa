Bacterial Microcompartments (BMCs) are organelles consisting of a protein shell that encloses enzymes and other proteins. BMCs are typically about 40-200 nanometers in diameter and are entirely made of proteins. The shell functions like a membrane, as it is selectively permeable. Other protein-based compartments found in bacteria and archaea include encapsulin nanocompartments and gas vesicles. Eukaryotes have also been observed to have proteinaceous organelles, such as the mysterious vault complex.


== DiscoveryEdit ==
See: discovery of carboxysomes
The first BMCs were observed in the 1950s in electron micrographs of cyanobacteria, and were later named carboxysomes after their role in carbon fixation was established. Until the 1990s, carboxysomes were thought to be an oddity confined to certain autotrophic bacteria. But then genes coding for proteins homologous to those of the carboxysome shell were identified in the pdu (propanediol utilization) and eut (ethanolamine utilization) operons. Subsequently, transmission electron micrographs of Salmonella cells grown on propanediol  or ethanolamine  showed the presence of polyhedral bodies similar to carboxysomes. The term metabolosome is used to refer to such catabolic BMCs (in contrast to the autotrophic carboxysome).
Although the carboxysome, propanediol utilizing (PDU), and ethanolamine utilizing (EUT) BMCs encapsulate different enzymes and therefore have different functions, the genes encoding for the shell proteins are very similar. Most of the genes (coding for the shell proteins and the encapsulated enzymes) from experimentally characterized BMCs are located near one another in distinct genetic loci or operons. There are currently over 20,000 bacterial genomes sequenced, and bioinformatics methods can be used to find all BMC shell genes and to look at what other genes are in the vicinity, producing a list of potential BMCs. Recently a comprehensive survey identified 23 different loci encoding up to 10 functionally distinct BMCs across 23 bacterial phyla.


== Bacterial Microcompartment shellsEdit ==


=== Protein families forming the shellEdit ===
The BMC shell appears icosahedral or quasi-icosahedral, and is formed by (pseudo)hexameric and pentameric protein subunits.
The three types of proteins (BMC-H, BMC-T and BMC-P) known to form the shell of BMCs. The encapsulated enzymes/proteins (shown in purple, red and turquoise) constitute a metabolic reaction sequence (Image credit: Seth Axen, Markus Sutter, Sarah Newnham, Clement Aussignargues and Cheryl Kerfeld)


== The BMC shell protein familyEdit ==

The major constituents of the BMC shell are proteins containing Pfam00936 domain(s). These proteins form oligomers that are hexagonal in shape and are thought to form the facets of the shell.


=== Single-domain proteins (BMC-H)Edit ===
The BMC-H proteins, which contain a single copy of the Pfam00936 domain, are the most abundant component of the facets of the shell. The crystal structures of a number of these proteins have been determined, showing that they assemble into cyclical hexamers, typically with a small pore in the center. This opening is proposed to be involved in the selective transport of the small metabolites across the shell.


=== Tandem-domain proteins (BMC-T)Edit ===
A subset of shell proteins are composed of tandem (fused) copies of the Pfam00936 domain (BMC-T proteins). The structurally characterized BMC-T proteins form trimers that are pseudohexameric in shape. Some BMC-T crystal structures show that the trimers can stack in a face-to-face fashion. In such structures, one pore from one trimer is in an open conformation, while the other is closed  suggesting that there may be an airlock-like mechanism that modulates the permeability of some BMC shells. Another subset of BMC-T proteins contain a [4Fe-4S] cluster, and may be involved in electron transport across the BMC shell.


=== The EutN/CcmL family (BMC-P)Edit ===
Twelve pentagonal units are necessary to cap the vertices of an icosahedral shell. Crystal structures of proteins from the EutN/CcmL family (Pfam03319) have been solved and they typically form pentamers (BMC-P). The importance of the BMC-P proteins in shell formation seems to vary among the different BMCs. It was shown that they are necessary for the formation of the shell of the PDU BMC as mutants in which the gene for the BMC-P protein was deleted cannot form shells, but not for the alpha-carboxysome: without BMC-P proteins, carboxysomes will still assemble and many are elongated; these mutant carboxysomes appear to be leaky.


== Relation to viral capsidsEdit ==
While the BMC shell is architecturally similar to many viral capsids, the shell proteins have not been found to have any structural or sequence homology to capsid proteins.


== Permeability of the shellEdit ==
It is well established that enzymes are packaged within the BMC shell and that some degree of metabolite and cofactor sequestration must occur. However, other metabolites and cofactors must also be allowed to cross the shell in order for BMCs to function. For example, in carboxysomes, ribulose-1,5-bisphosphate, bicarbonate, and phosphoglycerate must cross the shell, while carbon dioxide and oxygen diffusion is apparently limited. Similarly, for the PDU BMC, the shell must be permeable to propanediol, propanol, propionyl-phosphate, and potentially also vitamin B12, but it is clear that propionaldehyde is somehow sequestered to prevent cell damage. There is some evidence that ATP must also cross some BMC shells.
It has been proposed that the central pore formed in the hexagonal protein tiles of the shell are the conduits through which metabolites diffuse into the shell. For example, the pores in the carboxysome shell have an overall positive charge, which has been proposed to attract negatively charged substrates such as bicarbonate. For larger metabolites, a gating mechanism in some BMC-T proteins is apparent.
The presence of iron-sulfur clusters in some shell proteins, presumably in the central pore, has led to the suggestion that they can serve as a conduit through which electrons can be shuttled across the shell.


== TypesEdit ==
A recent comprehensive survey of microbial genome sequence data indicated up to ten different metabolic functions encapsulated by BMC shells. The majority are involved in either carbon fixation (carboxysomes) or aldehyde oxidation (metabolosomes).
Generalized function schematic for experimentally characterized BMCs. (A) Carboxysome. (B) Metabolosome. Reactions in gray are peripheral reactions to the core BMC chemistry. BMC shell protein oligomers are depicted on the left: blue, BMC-H; cyan, BMC-T; yellow, BMC-P. 3-PGA, 3-phosphoglycerate, and RuBP, ribulose 1,5-bisphosphate.


=== Carboxysomes: carbon fixationEdit ===

(A) Electron micrograph of Halothiobacillus neapolitanus cells, arrows denote carboxysomes. (B) Image of intact carboxysomes isolated from H. neapolitanus. Scale bars indicate 100 nm.
Carboxysomes encapsulate ribulose-1,5-bisphosphate carboxylase/oxygenase (RuBisCO) and carbonic anhydrase in carbon-fixing bacteria as part of a carbon concentrating mechanism. Bicarbonate is pumped into the cytosol and diffuses into the carboxysome, where carbonic anhydrase converts it to carbon dioxide, the substrate of RuBisCO. The carboxysome shell is thought to be only sparingly permeable to carbon dioxide, which results in an effective increase in carbon dioxide concentration around RuBisCO, thus enhancing carbon fixation. Mutants that lack genes coding for the carboxysome shell display a high carbon requiring phenotype due to the loss of the concentration of carbon dioxide, resulting in increased oxygen fixation by RuBisCO. The shells have also been proposed to restrict the diffusion of oxygen, thus preventing the oxygenase reaction, reducing wasteful photorespiration.
Electron micrograph of Synechococcus elongatus PCC 7942 cell showing the carboxysomes as polyhedral dark structures. Scale bar indicates 500 nm. (Image credit: Raul Gonzalez and Cheryl Kerfeld)


=== Metabolosomes: aldehyde oxidationEdit ===
In addition to the anabolic carboxysomes, several catabolic BMCs have been characterized that participate in the heterotrophic metabolism via short-chain aldehydes; they are collectively termed metabolosomes.
These BMCs share a common encapsulated chemistry driven by three core enzymes: aldehyde dehydrogenase, alcohol dehydrogenase, and phosphotransacylase. Because aldehydes can be toxic to cells and/or volatile, they are thought to be sequestered within the metabolosome. The aldehyde is initially fixed to coenzyme A by a NAD+-dependent aldehyde dehydrogenase, but these two cofactors must be recycled, as they apparently cannot cross the shell. These recycling reactions are catalyzed by an alcohol dehydrogenase (NAD+), and a phosphotransacetylase (coenzyme A), resulting in a phosphorylated acyl compound that can readily be a source of substrate-level phosphorylation or enter central metabolism, depending on if the organism is growing aerobically or anaerobically. It seems that most, if not all, metabolosomes utilize these core enzymes. Metabolosomes also encapsulate another enzyme that is specific to the initial substrate of the BMC, that generates the aldehyde; this is considered the signature enzyme of the BMC.


==== PDU BMCsEdit ====
Electron micrograph of Escherichia coli cell expressing the PDU BMC genes (left), and purified PDU BMCs from the same strain (right) (Image credit: Joshua Parsons, Steffanie Frank and Martin Warren)
Some bacteria can use 1,2-propanediol as a carbon source. They use a BMC to encapsulate several enzymes used in this pathway (Sampson and Bobik, 2008). The PDU BMC is typically encoded by a 21 gene locus. These genes are sufficient for assembly of the BMC since they can be transplanted from one type of bacterium to another, resulting in a functional metabolosome in the recipient. This is an example of bioengineering that likewise provides evidence in support of the selfish operon hypothesis. 1,2-propanediol is dehydrated to propionaldehyde by propanediol dehydratase, which requires vitamin B12 as a cofactor. Propionaldehyde causes DNA mutations and as a result is toxic to cells, possibly explaining why this compound is sequestered within a BMC. The end-products of the PDU BMC are propanol and propionyl-phosphate, which is then dephosphorylated to propionate, generating one ATP. Propanol and propionate can be used as substrates for growth.


==== EUT BMCsEdit ====
Ethanolamine utilization (EUT) BMCs are encoded in many diverse types of bacteria. Ethanolamine is cleaved to ammonia and acetaldehyde through the action of ethanolamine-ammonia lyase, which also requires vitamin B12 as a cofactor. Acetaldehyde is fairly volatile, and mutants deficient in the BMC shell have been observed to have a growth defect and release excess amounts of acetaldehyde. It has been proposed that sequestration of acetaldehyde in the metabolosome prevents its loss by volatility. The end-products of the EUT BMC are ethanol and acetyl-phosphate. Ethanol is likely a lost carbon source, but acetyl-phosphate can either generate ATP or be recycled to acetyl-CoA and enter the TCA cycle or several biosynthetic pathways.


==== Bifunctional PDU/EUT BMCsEdit ====
Some bacteria, especially those in the Listeria genus, encode a single locus in which genes for both PDU and EUT BMCs are present. It is not yet clear whether this is truly a chimeric BMC with a mixture of both sets of proteins, or if two separate BMCs are formed.


==== Glycyl radical enzyme-containing BMCs (GRM)Edit ====
Several different BMC loci have been identified that contain glycyl radical enzymes, which obtain the catalytic radical from the cleavage of s-adenosylcobalamin. One GRM locus in Clostridium phytofermentans has been shown to be involved in the fermentation of fucose and rhamnose, which are initially degraded to 1,2-propanediol under anaerobic conditions. The glycyl radical enzyme is proposed to dehydrate propanediol to propionaldehyde, which is then processed in a manner identical to the canonical PDU BMC.


==== Planctomycetes and Verrucomicrobia BMCs (PVM)Edit ====
Distinct lineages of Planctomycetes and Verrucomicrobia encode a BMC locus. The locus in Planctomyces limnophilus has been shown to be involved in the aerobic degradation of fucose and rhamnose. An aldolase is thought to generate lactaldehyde, which is then processed through the BMC, resulting in 1,2-propanediol and lactyl-phosphate.


==== Rhodococcus and Mycobacterium BMCs (RMM)Edit ====
Two types of BMC loci have been observed in members of the Rhodococcus and Mycobacterium genera, although their actual function has not been established. However, based on the characterized function of one of the genes present in the locus and the predicted functions of the other genes, it was proposed that these loci could be involved in the degradation of amino-2-propanol. The aldehyde generated in this predicted pathway would be the extremely toxic compound methylglyoxal; its sequestration within the BMC could protect the cell.


==== BMCs of Unknown Function (BUF)Edit ====
One type of BMC locus does not contain RuBisCO or any of the core metabolosome enzymes, and has been proposed to facilitate a third category of biochemical transformations (i.e. not carbon fixation or aldehyde oxidation). The presence of genes predicted to code for amidohydrolases and deaminases could indicate that this BMC is involved in the metabolism of nitrogenous compounds.


== Assembly of the BMCsEdit ==


=== CarboxysomesEdit ===
The assembly pathway for beta-carboxysomes has been identified, and begins with the protein CcmM nucleating RuBisCO. CcmM has two domains: an N-terminal gamma-carbonic anhydrase domain followed by a domain consisting of three to five repeats of RuBisCO small-subunit-like sequences. The C-terminal domain aggregates RuBisCO, likely by substituting for the actual RuBisCO small subunits in the L8-S8 holoenzyme, effectively cross-linking the RuBisCO in the cell into one large aggregate, termed the procarboxysome. The N-terminal domain of CcmM physically interacts with the N-terminal domain of the CcmN protein, which, in turn, recruits the hexagonal shell protein subunits via an encapsulation peptide on its C-terminus. Carboxysomes are then spatially aligned in the cyanobacterial cell via interaction with the bacterial cytoskeleton, ensuring their equal distribution into daughter cells.
Alpha-carboxysome assembly may be different than that of beta-carboxysomes, as they have no proteins homologous to CcmN or CcmM and no encapsulation peptides. Empty carboxysomes have been observed in electron micrographs. Some micrographs indicate that their assembly occurs as a simultaneous coalescence of enzymes and shell proteins as opposed to the seemingly stepwise fashion observed for beta-carboxysomes.


=== MetabolosomesEdit ===
Metabolosome assembly is likely similar to that of the beta-carboxysome, via an initial aggregation of the proteins to be encapsulated. The core proteins of many metabolosomes aggregate when expressed alone. Moreover, many encapsulated proteins contain terminal extensions that are strikingly similar to the C-terminal peptide of CcmN that recruits shell proteins. These encapsulation peptides are short (about 18 residues) and are predicted to form amphipathic alpha-helices. Some of these helices have been shown to mediate the encapsulation of native enzymes into BMCs, as well as heterologous proteins (such as GFP).


== Regulation (genetic)Edit ==
With the exception of carboxysomes, in all tested cases, BMCs are encoded in operons that are expressed only in the presence of their substrate.
PDU BMCs in Salmonella enterica are induced by the presence of propanediol or glycerol under anaerobic conditions, and only propanediol under aerobic conditions. This induction is mediated by the global regulator proteins Crp and ArcA (sensing cyclic AMP and anaerobic conditions respectively), and the regulatory protein PocR, which is the transcriptional activator for both the pdu and the cob loci (the operon necessary for the synthesis of vitamin B12, a required cofactor for propanediol dehydratase).
EUT BMCs in Salmonella enterica are induced via the regulatory protein EutR by the simultaneous presence of ethanolamine and vitamin B12, which can happen under aerobic or anaerobic conditions. Salmonella enterica can only produce endogenous vitamin B12 under anaerobic conditions, although it can import cyanobalamin and convert it to vitamin B12 under either aerobic or anaerobic conditions.
PVM BMCs in Planctomyces limnophilus are induced by the presence of fucose or rhamnose under aerobic conditions, but not by glucose. Similar results were obtained for the GRM BMC from Clostridium phytofermentans, for which both sugars induce the genes coding for the BMC as well as the ones coding for fucose and rhamnose dissimilatory enzymes.
In addition to characterized regulatory systems, bioinformatics surveys have indicated that there are potentially many other regulatory mechanisms, even within a functional type of BMC (e.g. PDU), including two-component regulatory systems.


== Relevance to global and human healthEdit ==
Carboxysomes are present in all cyanobacteria and many other photo- and chemoautotrophic bacteria. Cyanobacteria are globally significant drivers of carbon fixation, and since they require carboxysomes to do so in current atmospheric conditions, the carboxysome is a major component of global carbon dioxide fixation.
Several types of BMCs have been implicated in virulence of pathogens, such as Salmonella enterica and Listeria monocytogenes. BMC genes tend to be upregulated under virulence conditions, and mutating them leads to a virulence defect as judged by competition experiments.


== Biotechnological applicationsEdit ==
Several features of BMCs make them appealing for biotechnological applications. Because carboxysomes increase the efficiency of carbon fixation, much research effort has gone into introducing carboxysomes in plants in order to improve the carbon concentrating mechanism.
More generally, because BMC shell proteins self-assemble, empty shells can be formed, prompting efforts to engineer them to contain customized cargo. Discovery of the encapsulation peptide on the termini of some BMC-associated proteins provides a means to begin to engineer custom BMCs by fusing foreign proteins to this peptide and co-expressing this with shell proteins. For example, by adding this peptide to pyruvate decarboxylase and alcohol dehydrogenase, researchers have engineered an ethanol bioreactor. Finally, the pores present in the shell proteins control the permeability of the shell: these can be a target for bioengineering, as they can be modified to allow the crossing of selected substrates and products.


== See alsoEdit ==
Endomembrane system
Metabolic pathway
Substrate channeling
Encapsulins


== ReferencesEdit ==


== External linksEdit ==
Mysterious Bacterial Microcompartments Revealed By Biochemists
Not so simple after all. A renaissance of research into prokaryotic evolution and cell structure