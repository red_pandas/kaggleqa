Boiler feedwater is an essential part of boiler operations. The feed water is put in to the steam drum from a feed pump. In the steam drum the feed water is then turned into steam from the heat. After the feed water is used it is then dumped to the main condenser. From the condenser it is then pumped to the deaerated feed tank. From this tank it then goes back to the steam drum to complete its cycle. The feed water is never open to the atmosphere. This cycle is known as a closed system or Rankine cycle.


== History of feedwater treatment ==
During the early development of boilers, water treatment was not so much of an issue, as temperatures and pressures were so low that high amounts of scale and rust would not form to such a high amount, especially if the boiler was cleaned and/or blown down. It was general practice though, to install zinc plates and/or alkaline chemicals to reduce corrosion within the boiler. Many tests had been performed to try and determine the cause and possible protection from corrosion in boilers using distilled water, various chemicals, and sacrificial metals.  Silver nitrate can be added to feedwater samples in order to detect contamination by seawater. Use of lime for alkalinity control had been mentioned as early as 1900, and was used by the French and British Navies up until about 1935. In modern boilers though, treatment of boiler feedwater is extremely critical, as many problems can result from the use of untreated water in extreme pressure and temperature environments; this includes lower efficiency in terms of heat transfer, overheating, damage, and high costs of cleaning.


== Characteristics of boiler feedwater ==
Water absorbs more heat than any other substance at a given temperature change. This quality makes it an ideal raw material for boiler operations. Boilers are part of a closed system as compared to open systems in a gas turbine. The closed system that is used is the Rankine cycle. This means that the water is recirculated throughout the system and is never in contact with the atmosphere. The water is reused and needs to be treated to continue efficient operations. Boiler water must be treated in order to be proficient in producing steam. Boiler water is treated to prevent scaling, corrosion, foaming, and priming. Chemicals are put in to boiler water through the chemical feed tank to keep the water within chemical range. These chemicals are mostly oxygen scavengers and phosphates. The boiler water also has frequent blowdowns in order to keep the chloride content down. The boiler operations also include bottom blows in order to get rid of solids. Scale is precipitated impurities out of the water and then forms on heat transfer surfaces. This is a problem because scale does not transfer heat very well and causes the tubes to fail by getting too hot. Corrosion is caused by oxygen in the water. The oxygen causes the metal to oxidize which lowers the melting point of the metal. Foaming and priming is caused when the boiler water does not have the correct amount of chemicals and there are suspended solids in the water which carry over in the dry pipe. The dry pipe is where the steam and water mixture are separated.


== Boiler feedwater treatment ==
Boiler water treatment is used to control alkalinity, prevent scaling, correct pH, and to control conductivity. The boiler water needs to be alkaline and not acidic, so that it does not ruin the tubes. There can be too much conductivity in the feed water when there are too many dissolved solids. These correct treatments can be controlled by efficient operator and use of treatment chemicals. The main objectives to treat and condition boiler water is to exchange heat without scaling, protect against scaling, and produce high quality steam. The treatment of boiler water can be put in to two parts. These are internal treatment and external treatment. (Sendelbach, p. 131) The internal treatment is for boiler feed water and external treatment is for make-up feed water and the condensate part of the system. Internal treatment protects against feed water hardness by preventing precipitating of scale on the boiler tubes. This treatment also protects against concentrations of dissolved and suspended solids in the feed water without priming or foaming. These treatment chemicals also help with the alkalinity of the feed water making it more of a base to help protect against boiler corrosion. The correct alkalinity is protected by adding phosphates. These phosphates precipitate the solids to the bottom of the boiler drum. At the bottom of the boiler drum there is a bottom blow to remove these solids. These chemicals also include anti-scaling agents, oxygen scavengers, and anti-foaming agents. Sludge can also be treated by two approaches. These are by coagulation and dispersion. When there is a high amount of sludge content it is better to coagulate the sludge to form large particles in order to just use the bottom blow to remove them from the feed water. When there is a low amount of sludge content it is better to use dispersants because it disperses the sludge throughout the feed water so sludge does not form.


== Deaeration of feed water ==
Oxygen and Carbon Dioxide are removed from the feed water by deaeration. Deaeration can be accomplished by using deaerators heaters, vacuum deaerators, mechanical pumps, and steam-jet ejectors. In deaerating heaters steam sprays incoming feed water and carries away the dissolved gases. The deaerators also store hot feed water which is ready to be used in the boiler. This means of mechanical deaeration is also used with chemical oxygen scavenging agents to increase efficiency. (Sendelbach, p. 129) Deaerating heaters can be classified in to two groups. The two deaerating heaters are spray types and tray types. With tray type deaerating heaters the incoming water is sprayed into steam atmosphere in order to reach saturation temperature. When the saturation temperature is reached most of the oxygen and non-condensable gases are released. There are seals that prevent the recontamination of the water in the spray section. The water then falls to the storage tank below. The non-condensables and oxygen are then vented to the atmosphere. The components of the tray type deaerating heater are a shell, spray nozzles, direct contact vent condenser, tray stacks, and protective interchamber walls. The spray type deaerater is very similar to the tray type deaerater. The water is sprayed into a steam atmosphere and most of the oxygen and non-condensables are released to the steam. The water then falls to the steam scrubber where the slight pressure loss causes the water to flash a little bit which also helpswith the removal of oxygen and non-condensables. The water then overflows to the storage tank. The gases are then vented to the atmosphere. With vacuum deaeration a vacuum is applied to the system and water is then brought to its saturation temperature. The water is sprayed in to the tank just like the spray and tray deaeraters. The oxygen and non-condensables are vented to the atmosphere.(Sendelbach, p. 130)


== Conditioning ==

The feedwater must be specially treated to avoid problems in the boiler and downstream systems. Untreated boiler feed water can cause corrosion and fouling.


=== Boiler corrosion ===
Corrosive compounds, especially O2 and CO2 must be removed, usually by use of a deaerator. Residual amounts can be removed chemically, by use of oxygen scavengers. Additionally, feed water is typically alkalized to a pH of 9.0 or higher, to reduce oxidation and to support the formation of a stable layer of magnetite on the water-side surface of the boiler, protecting the material underneath from further corrosion. This is usually done by dosing alkaline agents into the feed water, such as sodium hydroxide (caustic soda) or ammonia. Corrosion in boilers is due to the presence of dissolved oxygen, dissolved carbon dioxide, or dissolved salts.


=== Fouling ===
Deposits reduce the heat transfer in the boiler, reduce the flow rate and eventually block boiler tubes. Any non-volatile salts and minerals that will remain when the feedwater is evaporated must be removed, because they will become concentrated in the liquid phase and require excessive "blow-down" (draining) to prevent the formation of solid precipitates. Even worse are minerals that form scale. Therefore, the make-up water added to replace any losses of feedwater must be demineralized/deionized water, unless a purge valve is used to remove dissolved minerals.


=== Caustic embrittlement ===


=== Priming and foaming ===


== Locomotive boilers ==
Steam locomotives usually do not have condensers so the feedwater is not recycled and water consumption is high. The use of deionized water would be prohibitively expensive so other types of water treatment are used. Chemicals employed typically include sodium carbonate, sodium bisulfite, tannin, phosphate and an anti-foaming agent.
Treatment systems have included:
Alfloc, developed by British Railways and Imperial Chemical Industries 
Traitement Integral Armand (TIA), developed by Louis Armand
Porta Treatment, developed by Livio Dante Porta 


== See also ==
Boiler feedwater pump
Evaporator
Helamin


== References ==

Shun'an, C. , Qing, Z. , & Zhixin, Z. (2008). A study of the influence of chloride ion concentration on the corrosion behavior of carbon steel in phosphate high-temperature boiler water chemistries. Anti-Corrosion Methods and Materials, 55(1), 15-19.
Sendelbach, M. (1988). Boiler-water treatment: Why, what and how. Chemical Engineering, 95(11), 127.
Characteristics of boiler feed water. (n.d.). Retrieved March 21, 2015, from http://www.lenntech.com/applications/process/boiler/boiler-feedwater-characteristics.htm


== External links ==
Boiler Feedwater System Configuration