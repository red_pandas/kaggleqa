Cestoda (Cestoidea) is a class of parasitic flatworms, of the phylum Platyhelminthes. Biologists informally refer to them as cestodes. The best-known species are commonly called tapeworms. All cestodes are parasitic and their life histories vary, but typically they live in the digestive tracts of vertebrates as adults, and often in the bodies of other species of animals as juveniles. Over a thousand species have been described, and all vertebrate species may be parasitised by at least one species of tapeworm.
Humans are subject to infection by several species of tapeworms if they eat undercooked meat such as pork (Taenia solium), beef (T. saginata), and fish (Diphyllobothrium spp.), or if they live in, or eat food prepared in, conditions of poor hygiene (Hymenolepis or Echinococcus species).
T. saginata, the beef tapeworm, can grow up to 20 m (65 ft); the largest species, the whale tapeworm Polygonoporus giganticus, can grow to over 30 m (100 ft). Species using small vertebrates as hosts, though, tend to be small. For example, vole and lemming tapeworms are only 13240 mm (0.519.45 in) in length, and those parasitizing shrews only 0.860 mm (0.0312.362 in).
Tapeworm parasites of vertebrates have a long history: recognizable clusters of cestode eggs, one with a developing larva, have been discovered in fossil feces (coprolites) of a shark dating to the mid- to late Permian, some 270 million years ago.


== AnatomyEdit ==


=== ScolexEdit ===

The worm's scolex ("head") attaches to the intestine of the definitive host. In some species, the scolex is dominated by bothria, or "sucking grooves" that function like suction cups. Other species have hooks and suckers that aid in attachment. Cyclophyllid cestodes can be identified by the presence of four suckers on their scolices.
While the scolex is often the most distinctive part of an adult tapeworm, it is often unnoticed in a clinical setting, as it is inside the host. Therefore, identifying eggs and proglottids in feces is the simplest way to diagnose an infection.


=== Body systemsEdit ===
The main nerve centre of a cestode is a cerebral ganglion in its scolex. Motor and sensory innervation depends on the number of nerves in and complexity of the scolex. Smaller nerves emanate from the ganglion to supply the general body muscular and sensory ending. The cirrus and vagina are innervated, and sensory endings around the genital pore are more plentiful than other areas. Sensory function includes both tactoreception (touch) and chemoreception (smell or taste). Some nerves are only temporary.


=== ProglottidsEdit ===

The body is composed of successive segments called proglottids. The sum of the proglottids is called a strobila, which is thin, and resembles a strip of tape. From this is derived the common name "tapeworm". Proglottids are continually produced by the neck region of the scolex, as long as the scolex is attached and alive. Like some other flatworms, cestodes use flame cells (protonephridia), located in the proglottids, for excretion. Mature proglottids are released from the tapeworm's end segment and leave the host in feces or migrate as independent motile proglottids. The proglottids farthest away from the scolex are the mature ones containing eggs. Mature proglottids are essentially bags of eggs, each of which is infective to the proper intermediate host.
The layout of proglottids comes in two forms: craspedote, meaning any given proglottid is overlapped by the previous proglottid, and acraspedote, indicating the proglottids are not overlapping.
Cestodes are unable to synthesise lipids and are entirely dependent on their host, although lipids are not used as an energy reserve, but for reproduction. Once anchored to the host's intestinal wall, the tapeworm absorbs nutrients through its skin as the food being digested by the host flows over and around it. Soon, it begins to grow a tail composed of a series of segments, with each segment containing an independent digestive system and reproductive tract. Older segments are pushed toward the tip of the tail as new segments are produced by the neckpiece. By the time a segment has reached the end of the worm's tail, only the reproductive tract is left. The segment then separates, carrying the tapeworm eggs out of the definitive host as what is basically a sack of eggs.


== Lifecycle of the tapewormsEdit ==

True tapeworms are exclusively hermaphrodites; they have both male and female reproductive systems in their bodies. The reproductive system includes one or more testes, cirri, vas deferens, and seminal vesicles as male organs, and a single lobed or unlobed ovary with the connecting oviduct and uterus as female organs. The common external opening for both male and female reproductive systems is known as the genital pore, which is situated at the surface opening of the cup-shaped atrium. Though they are sexually hermaphroditic, self-fertilization is a rare phenomenon. To permit hybridization, cross-fertilization between two individuals is often practiced for reproduction. During copulation, the cirri of one individual connect with those of the other through the genital pore, and then spermatozoa are exchanged.
The lifecycle of tapeworms is simple in the sense that no asexual phases occur as in other flatworms, but complicated in that at least one intermediate host is required as well as the definitive host. This lifecycle pattern has been a crucial criterion for assessing evolution among Platyhelminthes. Many tapeworms have a two-phase lifecycle with two types of hosts. The adult Taenia saginata lives in the gut of a primate such as a human, but more alarming is Taenia solium, which can form cysts in the human brain. Proglottids leave the body through the anus and fall onto the ground, where they may be eaten with grass by an animal such as a cow. If the tapeworm is compatible with the eating animal, this animal becomes an intermediate host. The juvenile form of the worm enters through the mouth, but then migrates and establishes as a cyst in the intermediate host's body tissues such as muscles, rather than the gut. This can cause more damage to the intermediate host than it does to its definitive host. The parasite completes its lifecycle when the intermediate host passes on the parasite to the definitive host. This is usually done by the definitive host eating a suitably infected intermediate host, e.g., a human eating raw or undercooked meat.


== Infection and treatmentEdit ==

Symptoms vary widely, as do treatment options, and these issues are discussed in detail in the individual articles on each worm. Praziquantel is an effective treatment for tapeworm infection, and is preferred over the older niclosamide. Cestodes can also be treated with certain kinds of antibiotics. While accidental tapeworm infections in developed countries are quite rare, some US dieters have risked intentional infection for the purpose of weight loss.
Certain medicines are used to remove it, such as praziquantel or albendazole. Physicians also give enema treatment to the patient to completely remove flatworms.


== TaxonomyEdit ==
The taxonomy of the Cestoda has been clarified with molecular data. The Gyrocotylidea are a sister group to all other Cestoda (Nephroposticophora): the Amphilinidea form the sister group to the Eucestoda. The Caryophyllidea are the sister group to Spathebothriidea and remaining Eucestoda. The Haplobothriidea are the sister group to Diphyllobothriidae. The Diphyllidea and Trypanorhyncha may be sister groups, but this is not definite.
At the more derived groups, the taxonomy appears to be:
Bothriocephalidea
Litobothriidea
Lecanicephalidea
Rhinebothriidea
Tetraphyllidea
Acanthobothrium, Proteocephalidea
Cyclophyllidea, Mesocestoididae, Nippotaeniidea, Tetrabothriidea

The Tetraphyllidea appear to be paraphyletic. The relations between Nippotaeniidea, Mesocestoididae, Tetrabothriidea, and Cyclophyllidea require further clarification.
The taxonomy of the Eucestoda has been also clarified. The current taxonomy is
Monogenea
Amphilinidea
Caryophyllidea
Spathebothriidea
Trypanorhyncha
Pseudophyllidea
Tetraphyllidea
Diphyllidea, Proteocephalidea
Nippotaeniidea
Cyclophyllidea, Tetrabothriidea

The Tetraphyllidea, Pseudophyllidea (because of the Diphyllobothriidae) and Cyclophyllidea (because of the Mesocestoididae) are paraphyletic.
The Taeniidae may be the most basal of the 12 orders of the Cyclophyllidea.
The Tetraphyllidea, Lecanicephalidea, Proteocephalidea, Nippotaeniidea, Tetrabothriidea, and Cyclophyllidea are considered to be the 'higher' tapeworms.
The 277 known species in the marine order Trypanorhyncha are placed in five superfamilies - Tentacularioidea, Gymnorhynchoidea, Otobothrioidea, Eutetrarhynchidae, and Lacistorhynchidae.


== Society and cultureEdit ==


=== In popular cultureEdit ===
Deliberately implanted tapeworms that take over their human hosts to form either voracious, zombie-like "sleepwalkers" or human-tapeworm chimerae are the basis of Mira Grant's science-fiction horror trilogy, Parasitology.


== ReferencesEdit ==


== Further readingEdit ==
Campbell, Reece, and Mitchell, Biology, 1999
Merck Manual of Medication' Information, Second Home Edition, Online Version, Tapeworm Infection 2005
Mayo Clinic Website on infectious diseases, Mayo Clinic - Tapeworm Infection, 2006
Medline Plus - Taeniasis (tapeworm infection)
University of South Carolina - School of Medicine - Cestodes (tapeworms)
 This article incorporates public domain material from websites or documents of the Centers for Disease Control and Prevention.
Medline Plus - Taeniasis (tapeworm infection)