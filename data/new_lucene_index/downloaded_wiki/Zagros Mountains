The Zagros Mountains (Persian:   , Kurdish:  ; iyayn Zagros, Lurish:    , Arabic:    Aramaic:  ,) form the largest mountain range in Iran, Iraq and Eastern Turkey. This mountain range has a total length of 1,500 km (932 mi). The Zagros mountain range begins in northwestern Iran and roughly corresponds to Iran's western border, and it spans the whole length of the western and southwestern Iranian plateau, ending at the Strait of Hormuz. The highest point in the Zagros Mountains is Dena. These mountains are regarded as sacred by the Kurds.


== GeologyEdit ==
The Zagros fold and thrust belt was formed by collision of two tectonic plates  the Iranian Plate and the Arabian Plate. This collision primarily happened during the Miocene and folded the entire rocks that had been deposited from the Carboniferous to the Miocene in the geosyncline in front of the Iranian Plate. The process of collision continues to the present and as the Arabian Plate is being pushed against the Iranian Plate, the Zagros Mountains and the Iranian Plateau are getting higher and higher. Recent GPS measurements in Iran have shown that this collision is still active and the resulting deformation is distributed non-uniformly in the country, mainly taken up in the major mountain belts like Alborz and Zagros. A relatively dense GPS network which covered the Zagros in the Iranian part also proves a high rate of deformation within the Zagros. The GPS results show that the current rate of shortening in SE Zagros is ~10 mm/yr and ~5mm/yr in the NW Zagros. The NS strike-slip Kazerun fault divides the Zagros into two distinct zones of deformation. The GPS results also show different shortening directions along the belt, i.e. normal shortening in the South-East and oblique shortening in the NW Zagros.
The sedimentary cover in the SE Zagros is deforming above a layer of rock salt (acting as a ductile decollement with a low basal friction) whereas in the NW Zagros the salt layer is missing or is very thin. This different basal friction partly made different topographies in either sides of Kazerun fault. Higher topography and narrower zone of deformation in the NW Zagros is observed whereas in the SE, deformation was spread more and wider zone of deformation with lower topography was formed. Stresses induced in the Earth's crust by the collision caused extensive folding of the preexisting layered sedimentary rocks. Subsequent erosion removed softer rocks, such as mudstone (rock formed by consolidated mud) and siltstone (a slightly coarser-grained mudstone) while leaving harder rocks, such as limestone (calcium-rich rock consisting of the remains of marine organisms) and dolomite (rocks similar to limestone containing calcium and magnesium). This differential erosion formed the linear ridges of the Zagros Mountains.
The depositional environment and tectonic history of the rocks were conducive to the formation and trapping of petroleum, and the Zagros region is an important part of Persian Gulf oil production.
Salt domes and salt glaciers are a common feature of the Zagros Mountains. Salt domes are an important target for oil exploration, as the impermeable salt frequently traps petroleum beneath other rock layers.


=== Type and age of rockEdit ===
The Zagros Mountains have a totally sedimentary origin and are made primarily of limestone. In the Elevated Zagros or the Higher Zagros, the Paleozoic rocks could be found mainly in the upper and higher sections of the peaks of the Zagros Mountains along the Zagros main fault. On the both sides of this fault, there are Mesozoic rocks, a combination of Triassic and Jurassic rocks that are surrounded by Cretaceous rocks on the both sides. The Folded Zagros (the mountains south of the Elevated Zagros and almost parallel to the main Zagros fault) is formed mainly of Tertiary rocks, with the Paleogene rocks south of the Cretaceous rocks and then the Neogene rocks south of the Paleogene rocks.
The mountains are divided into many parallel sub-ranges (up to 10, or 250 km wide), and orogenically have the same age as the Alps. Iran's main oilfields lie in the western central foothills of the Zagros mountain range. The southern ranges of the Fars Province have somewhat lower summits, reaching 4000 metres. They contain some limestone rocks showing abundant marine fossils.


=== ZonesEdit ===
Zagros Mountains follow a NW to SE pattern. A common way to divide this large area is considering two parts i.e. Northern Zagros and Southern Zagros. Northern Zagros includes Iranian provinces of West Azerbaijan, Kurdistan, Hamedan, Kermanshahan, Ilam, Lorestan, Isfahan, Khuzestan, Chaharmahal va Bakhtiari. Southern Zagros covers provinces of Kohgiluye va Buyerahmad, Fars, Bushehr and Hormozgan. Another description considers three parts: northern, middle and southern Zagros.
Geologically, Zagros Mountains consist of two major parts alongside: Elevated Zagros and Folded Zagros. Elevated Zagros forms the north eastern mountains and Folded Zagros stands in the south and west of the Elevated Zagros. Heading east, Elevated Zagros faces Inner Highlands of the Zagros Mountains also known as Sanandej - Sirjan Zone. Folded Zagros instead ends in the Persian Gulf in south Khuzestan plain in south west and Mesopotamia in west.


== Glaciation of the East ZargrosEdit ==
The mountains of the East-Zagros, the Kuh-i-Jupar (4135 m), Kuh-i-Lalezar (4374 m) and Kuh-i-Hezar (4469 m) do not currently have glaciers. Only at Zard Kuh and Dena some glaciers still survive. However, before the Last Glacial Period they had been glaciated to a depth in excess of 1900 meters, and during the Last Glacial Period to a depth in excess of 2160 meters. Evidence exists of a 20 km wide glacier fed along a 17 km long valley dropping approximately 1500 meters along its length on the north side of Kuh-i-Jupar with a thickness of 350-550m. Under precipitation conditions comparable to the current conditions, this size of glacier could be expected to form where the annual average temperature was between 10.5 and 11.2 C, but since conditions are expected to have been dryer during the period in which this glacier was formed, the temperature must have been lower.


== ClimateEdit ==
The Zagros Mountains contain several ecosystems. Prominent among them are the forest and forest steppe areas with a semi-arid climate. As defined by the World Wildlife Fund and used in their Wildfinder, the particular terrestrial ecoregion of the mid to high mountain area is Zagros Mountains forest steppe (PA0446). The annual precipitation ranges from 400 mm to 800 mm (16 to 30 inches) and falls mostly in the winter spring. The winters are severe, with low temperatures often below 25 C (-13 F). The region exemplifies the continental variation of the Mediterranean climate pattern, with a snowy, cold winter and mild rainy spring followed by a dry summer and autumn.


== Flora and faunaEdit ==
Although currently degraded through overgrazing and deforestation, the Zagros region is home to a rich and complex flora. Remnants of the originally widespread oak-dominated woodland can still be found, as can the park-like pistachio/almond steppelands. The ancestors of many familiar foods, including wheat, barley, lentil, almond, walnut, pistachio, apricot, plum, pomegranate and grape can be found growing wild throughout the mountains. Persian oak (Quercus brantii) (covering more than 50% of the Zagros forest area) is the most important tree species of the Zagros in Iran.
The Zagros are home to many threatened or endangered organisms, including the Zagros Mountains mouse-like hamster (Calomyscus bailwardi), the Basra reed-warbler (Acrocephalus griseldis) and the striped hyena (Hyena hyena). The Persian fallow deer (Dama dama mesopotamica), an ancient domesticate once thought extinct, was rediscovered in the late 20th century in Khuzestan province in the southern Zagros.


== HistoryEdit ==
Signs of early agriculture date back as far as 9000 BC to the foothills of the Zagros Mountains, in cities later named Anshan and Susa. Jarmo is one archaeological site in this area. Shanidar, where the ancient skeletal remains of Neanderthals have been found, is another.
Some of the earliest evidence of wine production has been discovered in the Zagros Mountains; both the settlements of Hajji Firuz Tepe and Godin Tepe have given evidence of wine storage dating between 3500 and 5400 BC.
During early ancient times, the Zagros was the home of peoples such as the Kassites, Guti, Assyrians, Elamites and Mitanni, who periodically invaded the Sumerian and/or Akkadian cities of Mesopotamia. The mountains create a geographic barrier between the flatlands of Mesopotamia, which is in Iraq, and the Iranian plateau. A small archive of clay tablets detailing the complex interactions of these groups in the early second millennium BC has been found at Tell Shemshara along the Little Zab. Tell Bazmusian, near Shemshara, was occupied between the sixth millennium BCE and the ninth century CE, although not continuously.


== See alsoEdit ==

Taurus Mountains
Alborz Mountains
Mount Elbrus
Silakhor Plain


== ReferencesEdit ==


== External linksEdit ==
Zagros, Photos from Iran, Livius.
The genus Dionysia
Iran, Timeline of Art History
Mesopotamia 9000500 B.C.
Major Peaks of the Zagros Mountains