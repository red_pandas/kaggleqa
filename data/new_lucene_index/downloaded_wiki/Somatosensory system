The somatosensory system, also known as somatic senses, touch or tactile perception, is a complex sensory system. It is considered one of the five traditional senses. It is made up of a number of different receptors, including thermoreceptors, photoreceptors, mechanoreceptors and chemoreceptors. It also comprises essential processing centres, or sensory modalities, such as proprioception, mechanoreception (touch), thermoception (temperature), and nociception (pain). The sensory receptors cover the skin and epithelial tissues, skeletal muscles, bones and joints, internal organs, and the cardiovascular system.
Somatic senses are sometimes referred to as somesthetic senses, with the understanding that somesthesis includes touch, proprioception and (depending on usage) also haptic perception.
Processing primarily occurs in the primary somatosensory area in the parietal lobe of the cerebral cortex: information is sent from the receptors via sensory nerves, through tracts in the spinal cord and finally into the brain.
The system works when activity in a sensory neuron is triggered by a specific stimulus such as pain, for instance. This signal then passes to the part of the brain attributed to that area on the bodythis allows the stimulus to be felt at the correct location. The mapping of the body surfaces in the brain is called a homunculus and plays a fundamental role in the creation of body image. This brain-surface ("cortical") map is not immutable, however. Dramatic shifts can occur in response to stroke or injury.


== StructureEdit ==
The somatosensory system is spread through all major parts of a mammal's body (and other vertebrates). It consists both of sensory receptors and sensory (afferent) neurons in the periphery (skin, muscle and organs for example), to deeper neurons within the central nervous system.


=== General somatosensory pathwayEdit ===
A somatosensory pathway will typically have three long neurons: primary, secondary and tertiary (or first, second, and third).
The first neuron always has its cell body in the dorsal root ganglion of the spinal nerve (if sensation is in parts of the head or neck not covered by the cervical nerves, it will be the trigeminal nerve ganglia or the ganglia of other sensory cranial nerves).
The second neuron has its cell body either in the spinal cord or in the brainstem. This neuron's ascending axons will cross (decussate) to the opposite side either in the spinal cord or in the brainstem. The axons of many of these neurons terminate in the thalamus (for example the ventral posterior nucleus, VPN), others terminate in the reticular system or the cerebellum.
In the case of touch and certain types of pain, the third neuron has its cell body in the VPN of the thalamus and ends in the postcentral gyrus of the parietal lobe.


=== PeripheryEdit ===
In the periphery, the somatosensory system detects various stimuli by sensory receptors, e.g. by mechanoreceptors for tactile sensation and nociceptors for pain sensation. The sensory information (touch, pain, temperature etc.,) is then conveyed to the central nervous system by afferent neurons. There are a number of different types of afferent neurons that vary in their size, structure and properties. Generally there is a correlation between the type of sensory modality detected and the type of afferent neuron involved. For example, slow, thin, unmyelinated neurons conduct pain whereas faster, thicker, myelinated neurons conduct casual touch.
The receptive field of a particular afferent neuron is given by the sensory receptors supplying it, and in turn from a particular region of the skin.


=== Spinal cordEdit ===
In the spinal cord, the somatosensory system  includes ascending pathways from the body to the brain. One major target is the postcentral gyrus in the cerebral cortex. This is the target for neurons of the dorsal column-medial lemniscus pathway and the ventral spinothalamic pathway. Note that many ascending somatosensory pathways include synapses in either the thalamus or the reticular formation before they reach the cortex. Other ascending pathways, particularly those involved with control of posture are projected to the cerebellum. These include the ventral and dorsal spinocerebellar tracts. Another important target for afferent somatosensory neurons which enter the spinal cord are those neurons involved with local segmental reflexes.


=== BrainEdit ===

The primary somatosensory area in the human cortex (also called primary somatic sensory cortex or SI) is located in the postcentral gyrus of the parietal lobe and makes up four distinct fields or regions known as Brodmann areas 3a, 3b, 1, and 2. The postcentral gyrus is the location of the primary somatosensory area, the main sensory receptive area for the sense of touch. Any individual neuron has its receptive field on the skin.
A relationship between the somatosensory cortical areas and their projection of the body was discovered by recording electrical activity in the human cortex after mechanosensory stimulation of different body parts during neurosurgical procedures. These data led to the construction of somatotopic maps in which a somatotopic arrangement was generated. Like other sensory areas, there is a map of sensory space called a homunculus at this location. For the primary somatosensory cortex, this is called the sensory homunculus. Areas of this part of the human brain map to certain areas of the body, dependent on the amount or importance of somatosensory input from that area. For example, there is a large area of cortex devoted to sensation in the hands, whereas the human back has a much smaller area. Somatosensory information involved with proprioception and posture also targets an entirely different part of the brain, the cerebellum.


== FunctionEdit ==

Initiation of somatosensation begins with activation of a physical "receptor". These somatosensory receptors tend to lie in organs, most notably in the skin, or muscle. The structure of these receptors is broadly similar in all cases, consisting of either a "free nerve ending" or a nerve ending embedded in a specialised capsule. They can be activated by movement (mechanoreceptor), pressure (mechanoreceptor), chemical (chemoreceptor) and/or temperature. Another activation is by vibrations generated as a finger scans across a surface. This is the means by which we can sense fine textures in which the spatial scale is less than 200 m. Such vibrations are around 250 Hz, which is the optimal frequency sensitivity of Pacinian corpuscles. In each case, the general principle of activation is similar; the stimulus causes depolarisation of the nerve ending and then an action potential is initiated. This action potential then (usually) travels inward towards the spinal cord.
Photoreceptors, similar to those found in the retina of the eye, detect potentially damaging ultraviolet radiation (ultraviolet A specifically), inducing increased production of melanin by melanocytes. This tanning potentially offers the skin rapid protection from DNA damage and sunburn caused by ultraviolet radiation (DNA damage caused by ultraviolet B). However, whether this offers protection is debatable, because the amount of melanin released by this process is modest in comparison to the amounts released in response to DNA damage caused by ultraviolet B radiation.


=== Fine touch and crude touchEdit ===
Fine touch (or discriminative touch) is a sensory modality which allows a subject to sense and localize touch. The form of touch where localization is not possible is known as crude touch. The posterior column-medial lemniscus pathway is the pathway responsible for the sending of fine touch information to the cerebral cortex of the brain.
Crude touch (or non-discriminative touch) is a sensory modality which allows the subject to sense that something has touched them, without being able to localize where they were touched (contrasting "fine touch"). Its fibres are carried in the spinothalamic tract, unlike the fine touch which is carried in the dorsal column.
As fine touch normally works in parallel to crude touch, a person will be able to localize touch until fibres carrying fine touch (Posterior column-medial lemniscus pathway) have been disrupted. Then the subject will feel the touch, but be unable to identify where they were touched.


=== Individual VariationEdit ===
A variety of studies have measured and investigated the causes for differences between individuals in the sense of fine touch. One well-studied area is passive tactile spatial acuity, the ability to resolve the fine spatial details of an object pressed against the stationary skin. A variety of methods have been used to measure passive tactile spatial acuity, perhaps the most rigorous being the grating orientation task. In this task subjects identify the orientation of a grooved surface presented in two different orientations, which can be applied manually or with automated equipment. Many studies have shown a decline in passive tactile spatial acuity with age; the reasons for this decline are unknown, but may include loss of tactile receptors during normal aging. Remarkably, index finger passive tactile spatial acuity is better among adults with smaller index fingertips; this effect of finger size has been shown to underlie the better passive tactile spatial acuity of women, on average, compared to men. The density of Meissner's corpuscles, a type of mechanoreceptor that detects low-frequency vibrations, is greater in smaller fingers; the same may hold for Merkel cells, which detect the static indentations important for fine spatial acuity. Among children of the same age, those with smaller fingers also tend to have better tactile acuity. Many studies have shown that passive tactile spatial acuity is enhanced among blind individuals compared to sighted individuals of the same age, possibly because of cross modal plasticity in the cerebral cortex of blind individuals. Perhaps also due to cortical plasticity, individuals who have been blind since birth reportedly consolidate tactile information more rapidly than sighted people.


== Clinical significanceEdit ==
A somatosensory deficiency may be caused by a peripheral neuropathy involving peripheral nerves of the somatosensory system.
This may present as numbness or paresthesia.
Certain types of seizures are associated with the somatosensory system. Cortical injury may lead to loss of thermal sensation or the ability to discriminate pain. An aura involving thermal and painful sensations is a phenomenon known to precede the onset of an epileptic seizure or focal seizure. Another type of seizure, called a sensory Jacksonian seizure involves an abnormal, localizable, cutaneous sensation but does not have apparent stimulus. This sensation may progress along a limb or to adjacent cutaneous body areas, reflecting abnormal neuronal firing in the postcentral gyrus where an epileptic discharge is propagated. These episodes in which patients are consciously aware during a seizure have been useful for identifying problems associated with the somatosensory cortex. Patients can describe the nature of the seizure and how they feel during it.
The absence of proprioception or two-point tactile discrimination on one side of the body suggests injury to the contralateral side of the primary somatosensory cortex. However, depending on the extent of the injury, damage can range in loss of proprioception of an individual limb or the entire body. A deficit known as cortical astereognosis of the receptive type describes an inability to make use of tactile sensory information for identifying objects placed in the hand. For example, if this type of injury effects the hand region in the primary somatosensory cortex for one cerebral hemisphere, a patient with closed eyes cannot perceive the position of the fingers on the contralateral hand and will not be able to identify objects such as keys or a cell phone if they are placed into that hand.
Evaluation of any suspected disease of the somatosensory system is included in a neurological examination of the peripheral nervous system


== Society and cultureEdit ==

Haptic technology can provide touch sensation in virtual and real environments. In the field of speech therapy, tactile feedback can be used to treat speech disorders.


== See alsoEdit ==
Allochiria
Cell signalling
Molecular cellular cognition
Haptic perception
Muscle spindle
Phantom limb
Secondary somatosensory cortex
Somatosensory Rehabilitation of Pain
Special senses
Supramarginal gyrus
Tactile illusion
Vibratese, method of communication through touch
Tactile imaging


== ReferencesEdit ==


== Further readingEdit ==
Boron, Walter F.; Boulpaep, Emile L. (2003). Medical Physiology. Saunders. pp. 352358. ISBN 0-7216-3256-4. 
Flanagan, J.R., Lederman, S.J. Neurobiology: Feeling bumps and holes, News and Views, Nature, 2001 Jul. 26;412(6845):389-91.
Hayward V, Astley OR, Cruz-Hernandez M, Grant D, Robles-De-La-Torre G. Haptic interfaces and devices. Sensor Review 24(1), pp. 1629 (2004).
Purves, Dale (2012). Neuroscience, Fifth Edition. Sunderland, MA: Sinauer Associates, Inc. pp. 202203. ISBN 978-0-87893-695-3. 
Robles-De-La-Torre G., Hayward V. Force Can Overcome Object Geometry In the perception of Shape Through Active Touch" Nature 412 (6845) 445-8 (2001).
Robles-De-La-Torre G. The Importance of the Sense of Touch in Virtual and Real Environments. IEEE Multimedia 13(3), Special issue on Haptic NO User Interfaces for Multimedia Systems, pp. 2430 (2006).
Grunwald, M. (Ed.) Human Haptic Perception - Basics and Applications. Boston/Basel/Berlin: Birkhuser, 2008, ISBN 978-3-7643-7611-6
Encyclopedia of Touch Scholarpedia Expert articles


== External linksEdit ==
'Somatosensory & Motor research' (Informa Healthcare)