Potassium arsenite (KAsO2) is an inorganic compound that exists in two forms, potassium meta-arsenite (KAsO2) and potassium ortho-arsenite (K3AsO3). It is composed of arsenite ions (AsO33 or AsO2) with arsenic always existing in the +3 oxidation state, and potassium existing in the +1 oxidation state. Like many other arsenic containing compounds, potassium arsenite is highly toxic and carcinogenic to humans. Potassium arsenite forms the basis of Fowlers solution, which was historically used as a medicinal tonic, but due to its toxic nature its use was discontinued. Potassium arsenite is still, however, used as a rodenticide.


== Structure ==
The two unique forms of potassium arsenite can be attributed to the different number of oxygen atoms. Potassium meta-arsenite (KAsO2) contains two oxygen atoms one of which is bonded to the arsenic atom via a double bond. Conversely, Potassium ortho-arsenite (K3AsO3) consists of three oxygen atoms all bound to the arsenic atom via single bonds. In each of these cases, arsenic exists in the +3 oxidation state and is known as arsenite, hence the single name referring to two different structures. Additionally, both the meta and ortho forms of potassium arsenite have identical properties.


== Properties ==
Potassium Arsenite is an inorganic salt that exists as an odorless white solid. It is largely soluble in water and only slightly soluble in alcohol. Solutions of potassium arsenite contain moderate concentrations of hydroxide, and are thus slightly basic. While potassium arsenite is noncombustible, heating it results in its decomposition and the formation of toxic fumes that include arsine, arsenic oxides, and potassium oxides. Potassium arsenite also reacts with acids to yield toxic arsine gas.


== Preparation ==
Aqueous potassium arsenite, more commonly known as Fowlers solution, can be prepared by heating arsenic trioxide (As2O3) with potassium hydroxide (KOH) in the presence of water. The reaction is shown below
As2O3 (aq) + 2 KOH (aq)  2 KAsO2 (aq) + H2O


== Uses ==
In the eighteenth century English physician Thomas Fowler utilized a potassium arsenite solution called Fowlers solution to remedy a number of conditions including anemia, rheumatism, psoriasis, eczema, dermatitis, asthma, cholera, and syphilis. Furthermore, in 1865 the potential uses of potassium arsenite expanded as Fowlers solution was used as the first chemotherapeutic agent to treat leukemia, however the chemotherapeutic effects were only temporary. Surprisingly enough, this specific use was inspired by potassium arsenites role in improving digestion and producing a smoother coat in horses. Potassium arsenite is also a key inorganic component of certain rodenticides, insecticides, and herbicides. Furthermore, its role as an insecticide also made it a great wood preservative; however, the solubility and toxicity made it a potential risk factor for the environment.


== Health Effects ==
The toxicity of potassium arsenite arises from arsenics high affinity for sulfhydryl groups. The formation of these arsenite-sulfur bonds impairs the functionality of certain enzymes such as glutathione reductase, glutathione peroxidases, thioredoxin reductase, and thioredoxin peroxidase. These enzymes are all closely affiliated with the defense of free radicals and the metabolism of pyruvate. Thus, exposure to potassium arsenite and other arsenite containing compounds results in the production of damaging oxygen free radicals and the arrest of cellular metabolism.
Additionally, arsenite containing compounds have also been labeled carcinogens. The carcinogenicity of potassium arsenite arises from its ability to inhibit DNA repair and methylation. This impairment of the cellular machinery can lead to cancer because the cells can no longer repair or arrest mutations and a tumor results. All of these conditions exhibit the hazardous nature of potassium arsenite and other arsenite containing compounds. This is evidenced by a LD50 of 14 mg/kg for rats and a TDL of 74 mg/kg for humans.


== Notes ==
^ a b c "NIOSH Pocket Guide to Chemical Hazards #0038". National Institute for Occupational Safety and Health (NIOSH). 
^ http://chem.sis.nlm.nih.gov/chemidplus/rn/10124-50-2
^ a b Lide, D. R. (1993). CRC Handbook Chemistry & Physics 74th Edition. ISBN 0-8493-0474-1. 
^ a b DM, J. (1993). "The History of the use of Arsenicals in Man". Journal of the Royal Society of Medicine 86 (5). 
^ Lander J.J., Stanley R.J., Sumner H.W., Boswell D.C., Aach R.D. (1975). "Angiosarcoma of the Liver associated with Fowler's Solution (Potassium Arsenite)". Gastroenterology 68 (6): 15821586. PMID 1169181. 
^ a b Potassium Arsenite. http://nj.gov/health/eoh/rtkweb/documents/fs/1557.pdf
^ a b U.S. Dept. of Health and Human Services, Public Health Service (October 2, 2014). "Arsenic and Inorganic Arsenic Compounds" (PDF). Report on Carcinogens, Thirteenth Edition. 
^ Caspari, Charles (1901). A Treatise on Pharmacy for Students and Pharmacists (2nd ed.). Philadelphia: Lea Brothers and Co. 
^ Tinwell, H.; Stephens, S. C.; Ashby, J. (1991). "Arsenite as the probable active species in the human carcinogenicity of arsenic: mouse micronucleus assays on Na and K arsenite, orpiment, and Fowler's solution" (PDF). Environmental Health Perspectives 95: 205210. doi:10.2307/3431125. PMC 1568403. PMID 1821373. 
^ Chen, Sai-Juan; Zhou, Guang-Biao; Zhang, Xiao-Wei; Mao, Jian-Hua; The , Hugues de; Chen, Zhu (16 June 2011). "From an old remedy to a magic bullet: molecular mechanisms underlying the therapeutic effects of arsenic in fighting leukemia" (PDF). Blood 117 (24). 
^ Xiong, Lei; Wang, Yinsheng (5 February 2010). "Quantitative Proteomic Analysis Reveals the Perturbation of Multiple Cellular Pathways in HL-60 Cells Induced by Arsenite Treatment". Journal of Proteome Research 9 (2): 11291137. doi:10.1021/pr9011359. 
^ "Potassium arsenite". TOXNET: Toxicology Data Network. Retrieved 6 December 2014.