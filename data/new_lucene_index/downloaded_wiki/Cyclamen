Cyclamen (US /saklmn/ SY-kl-men or UK /sklmn/ SIK-l-men) is a genus of 23 species of perennial flowering plants in the family Primulaceae. Cyclamen species are native to Europe and the Mediterranean Basin east to Iran, with one species in Somalia. They grow from tubers and are valued for their flowers with upswept petals and variably patterned leaves.
It was traditionally classified in the family Primulaceae, was reclassified in the family Myrsinaceae in 2000, and finally, in 2009 with the introduction of the APG III system, was returned to the subfamily Myrsinoideae within the family Primulaceae.


== Names ==
Cyclamen is Medieval Latin, from earlier Latin cyclamnos, from Ancient Greek , kyklmnos (also kyklms), probably from , kklos "circle", because of the round tuber. In English, the species of the genus are commonly called by the genus name.
In many languages, cyclamen species are colloquially called by a name like the English sowbread, because they are said to be eaten by pigs: pain de pourceau in French, pan porcino in Italian, varkensbrood in Dutch, "pigs' manj" in Japanese.


== Description ==
Cyclamens have a tuber, from which the flowers and roots grow. In most species, leaves come up in autumn, grow through the winter, and die in spring, then the plant goes dormant through the dry Mediterranean summer.


=== Tuber ===

The storage organ of the cyclamen is a round tuber that develops from the hypocotyl (the stem of a seedling). It is often mistakenly called a corm, but a corm (found in crocuses for example) has a papery tunic and a basal plate from which the roots grow. The storage organ of the cyclamen has no papery covering and, depending on the species, roots may grow out of any part. It is therefore properly classified as a tuber (somewhat like a potato). The tuber may produce roots from the top, sides, or bottom, depending on the species. Cyclamen persicum and Cyclamen coum root from the bottom; Cyclamen hederifolium roots from the top and sides. Cyclamen graecum has thick anchor roots on the bottom.
The shape of the tuber may be near spherical, as in Cyclamen coum or flattened, as in Cyclamen hederifolium. In some older specimens of Cyclamen purpurascens and Cyclamen rohlfsianum, growing points on the tuber become separated by shoulders of tissue, and the tuber becomes misshapen. In most other species, the tuber is round in old age.
Leaves and flowers sprout from growing points on the top of the tuber. Growing points that have lengthened and become like woody stems are known as floral trunks.
The size of the tuber varies depending on species. In Cyclamen hederifolium, older tubers commonly reach 24 cm (9.4 in) across, but in Cyclamen parviflorum, tubers do not grow larger than 2 cm (1 in) across.


=== Leaves ===

Leaves sprout from growing points or floral trunks on top of the tuber. Each leaf grows on its own stem. Leaf stems in early growth may be distinguished from flower stems by the direction their tips curl: tips of leaf stems curl upwards, while tips of flower stems curl downwards.
The shape of the leaves varies between the species, and even between different specimens of the same species. Cyclamen hederifolium and Cyclamen repandum usually have leaves shaped like ivy, with angles and lobes, Cyclamen coum has nearly round leaves, and Cyclamen persicum has heart-shaped leaves with a pointed tip. The leaf edge may be smooth, as in Cyclamen coum subsp. coum, or finely toothed, as in Cyclamen graecum.
The color of the upper side of leaves is variable, even within a species. Most species have leaves variegated in several shades of green and silver, either in an irregular pattern of blotches or an arrowhead or Christmas tree shape. In cultivation, cyclamens, especially species other than Cyclamen persicum, are selected as often or more often for striking or unusual leaf patterns than for their flowers.
The lower side of leaves is often shiny, and its color is from plain green to rich red or purple.
Most cyclamen species originate from the Mediterranean, where summers are hot and dry and winters are cool and wet, and are summer-dormant: their leaves sprout in the autumn, remain through the winter, and wither the next spring. Cyclamen purpurascens and Cyclamen colchicum, however, originate from cooler regions in mountains, and their leaves remain through the summer and wither only after the next year's leaves have developed.


=== Flowers ===
Flowering time may be any month of the year, depending on the species. Cyclamen hederifolium and Cyclamen purpurascens bloom in summer and autumn, Cyclamen persicum and coum bloom in winter, and Cyclamen repandum blooms in spring.
Each flower is on a stem coming from a growing point on the tuber. In all species, the stem is normally bent 150-180 at the tip, so that the nose of the flower faces downwards. Cyclamen hederifolium 'Stargazer' is an exception to this; its nose faces upwards. Flowers have 5 petals, bent outwards or up, sometimes twisted, and connected at the base into a cup, and five sepals behind the cup.
Petal shape varies depending on species, and sometimes within the same species. Cyclamen repandum has petals much longer than wide, Cyclamen coum has stubby, almost round petals, and Cyclamen hederifolium usually has petals with proportions between the two.
Petal color may be white, pink, or purple, often with darker color on the nose. Many species have a pink form and a white form, but a few have only one color, such as Cyclamen balearicum, which is always white.
The dark color on the flower nose varies in shape: Cyclamen persicum has a smooth band, Cyclamen hederifolium has a streaky V, and Cyclamen coum has an M-shaped splotch with two white or pink "eyes" beneath.

In some species, such as Cyclamen hederifolium, the petal edges at the nose are curved outwards into auricles (Latin for "little ears"). Most species, like Cyclamen persicum, have no auricles.
In most species, the style protrudes 13 mm out of the nose of the flower, but the stamens are inside the flower. In Cyclamen rohlfsianum, however, the cone of anthers sticks out prominently, about 23 mm (0.080.12 in) beyond the rim of the corolla, similar to shooting-stars (Dodecatheon).


=== Fruit ===
The flower stem coils or bends when the fruit begins to form. The stems of Cyclamen hederifolium and Cyclamen coum coil starting at the end, Cyclamen persicum arches downwards but does not curl, Cyclamen rohlfsianum coils starting near the tuber, and Cyclamen graecum coils both directions, starting at the middle.
The fruit is a round pod that opens by several flaps or teeth at maturity and contains numerous sticky seeds, brown at maturity. Natural seed dispersal is by ants (myrmecochory), which eat the sticky covering and then discard the seeds.


== Cultivation and uses ==

Cyclamen are commonly grown for their flowers, both outdoors and indoors in pots. Several species, particularly Cyclamen hederifolium, are hardy and can be grown outdoors in mild climates such as northwest Europe and the Pacific Northwest of North America.


=== Hardiness ===
Cyclamen species range from frost-hardy to frost-tender.
The most frost-hardy species, such as Cyclamen purpurascens, Cyclamen hederifolium, Cyclamen coum, and Cyclamen cilicium, tolerate temperatures down to 20 C (4 F). Cyclamen hederifolium has even survived prolonged freezing and temperatures down to 30 C (22 F).
Others, such as Cyclamen repandum, survive temperatures down to 14 C (7 F), but not prolonged freezing below this temperature.
Others, such as Cyclamen graecum, tolerate frost as low as 4 C (25 F) for a few hours.
Others, such as Cyclamen africanum, Cyclamen persicum, and Cyclamen rohlfsianum, only tolerate mild and brief frost.


=== Florist's cyclamen ===
The cyclamen commonly sold by florists is Cyclamen persicum, which is frost-tender. Selected cyclamen cultivars can have white, bright pink, red or purple flowers. While flowering, florists' cyclamens should be kept below 20 C (68 F), with the night time temperatures preferably between 6.5 C to 15 C (44 F to 59 F). Temperatures above 20 C (68 F) may induce the plant to go dormant.


== Ecology ==


=== Conservation ===
In many areas within the native range, cyclamen populations have been severely depleted by collection from the wild, often illegally, for the horticultural trade; some species are now endangered as a result. However, in a few areas, plant conservation charities have educated local people to control the harvest carefully at a sustainable level, including sowing seed for future crops, both sustaining the wild populations and producing a reliable long-term income. Many cyclamen are also propagated in nurseries without harm to the wild plants.


=== Caterpillar food source ===
Cyclamen species are eaten by the caterpillars of The Gothic moth.


=== Evolution ===
Cyclamen diversity in the Mediterranean has been studied extensively to understand how the species remain distinct (Debussche et al., 2000, 2002, 2003) and how they have reacted to the dramatic climate changes in the region. Certain climate change models suggest many species could become extinct in their current range within the next 50 years.


== Subdivisions ==


=== Subgenera and series ===


==== Subgenus Cyclamen ====


===== Series Cyclamen =====
Species in the Cyclamen hederifolium group have flowers with auricles, leaves usually with angles, and tubers with roots from the top and sides.
Cyclamen hederifolium
= Cyclamen neapolitanum
Cyclamen hederifolium var. hederifolium
Cyclamen hederifolium var. crassifolium

Cyclamen confusum
= Cyclamen hederifolium var. confusum
Cyclamen africanum


===== Series Purpurascens =====
Species in the Cyclamen purpurascens group have flowers with slight or no auricles and round or heart-shaped leaves.
Cyclamen purpurascens
= Cyclamen europaeum
Cyclamen colchicum


===== Series Persicum =====
Species in the Cyclamen persicum group have toothed leaves and flowers with or without auricles.
Cyclamen persicum
Cyclamen somalense 
Cyclamen graecum
Cyclamen rohlfsianum


==== Subgenus Gyrophoebe ====


===== Series Cilicium =====
Species of the Cyclamen cilicium group have small leaves sometimes scalloped or toothed and small tubers rooting from the bottom.
Cyclamen cilicium
Cyclamen intaminatum
Cyclamen mirabile


===== Series Pubipedia =====
Species in the Cyclamen coum group have flowers with short petals, round or oval leaves, and small tubers rooting from the center of the bottom.


==== Subgenus Psilanthum ====
Species of the Cyclamen repandum group have flowers with long, slender petals, leaves with scalloped edges, and tubers rooting from the center of the bottom.


==== Subgenus Corticata ====
Cyclamen cyprium
Cyclamen libanoticum


=== Species ===
Cyclamens bloom in different seasons, depending on the species.


==== Winter and spring ====


==== Summer and autumn ====


== References ==


=== Notes ===
^ cyclamen (noun). Oxford Advanced Learner's Dictionary. Oxford University Press. 2014.
^ "cyclamen". Oxford English Dictionary (3rd ed.). Oxford University Press. September 2005.  (Subscription or UK public library membership required.)
^ "cyclamen". Merriam-Webster Dictionary. 
^ Kllersj, Bergqvist & Anderberg 2000
^ Stevens 2012
^ cyclamnos. Charlton T. Lewis and Charles Short. A Latin Dictionary on Perseus Project.
^ , . Liddell, Henry George; Scott, Robert; A GreekEnglish Lexicon at the Perseus Project
^ Harper, Douglas. "cyclamen". Online Etymology Dictionary. 
^ Yesson & Culham 2006
^ Thulin & Warfa 1989


=== Sources ===
Debussche, Max; Debussche, Genevive; Grandjanny, Michel (2000). "Distribution of Cyclamen repandum Sibth. & Sm. subsp. repandum and ecology in Corsica and continental France". Acta Botanica Gallica 147 (2): 123142. doi:10.1080/12538078.2000.10515404. 
Debussche, Max; Thompson, John D (2002). "Morphological differentiation among closely related species with disjunct distributions: a case study of Mediterranean Cyclamen L. subgen. Psilanthum Schwarz (Primulaceae)". Botanical Journal of the Linnean Society 139 (2): 133144. doi:10.1046/j.1095-8339.2002.00054.x. 
Debussche, Max; Thompson, John D (2003). "Habitat differentiation between two closely related Mediterranean plant species, the endemic Cyclamen balearicum and the widespread C. repandum". Acta Oecologica-International Journal of Ecology 24 (1): 3545. doi:10.1016/s1146-609x(02)00006-1. 
Grey-Wilson, Christopher (1998). Cyclamen: a guide for gardeners, horticulturists, and botanists. Timber Press. 
Kllersj, Mari; Bergqvist, Gullevi; Anderberg, Arne A (2000). "Generic realignment in primuloid families of the Ericales s.l.: A phylogenetic analysis based on DNA sequences from three chloroplast genes and morphology". American Journal of Botany 87 (9): 13251341. doi:10.2307/2656725. PMID 10991903. 
Stevens, Peter F (July 2012). "Myrsinoideae". Angiosperm Phylogeny Website. 
Thulin, Mats; Warfa, Ahmed Mumin (1989). "Cyclamen (Primulaceae) in tropical Africa". Systematics and Evolution 166 (3-4): 249252. doi:10.1007/bf00935953. 
Yesson, Chris; Culham, Alastair (2006). "A phyloclimatic study of Cyclamen". BMC Evolutionary Biology 7 (72). doi:10.1186/1471-2148-6-72. PMC 1599755. PMID 16987413. 
Yesson, C; Toomey, NH; Culham, A (2008). "Cyclamen: Time, sea and speciation biogeography using a temporally calibrated phylogeny". Journal of Biogeography 36 (7): 12341252. doi:10.1111/j.1365-2699.2008.01971.x. 


== External links ==
The Cyclamen Society
Plants for a Future: Cyclamen
Cyclamen Q&A