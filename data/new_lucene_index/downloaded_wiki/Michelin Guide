Michelin Guides (French: Guide Michelin [id mi.l]) are a series of annual guide books published by the French company Michelin for more than a century. The term normally refers to the Michelin Red Guide, the oldest European hotel and restaurant reference guide, which awards Michelin stars for excellence to a select few establishments. The acquisition or loss of a star can have dramatic effects on the success of a restaurant. Michelin also publishes a series of general guides to countries.


== History ==
In 1900 the tyre manufacturers Andr Michelin and his brother douard published the first edition of a guide for French motorists. At the time there were fewer than 3,000 cars in France, and the Michelin guide was intended to boost the demand for cars, and thus for car tires. For the first edition of the Michelin Guide the brothers had nearly 35,000 copies printed. It was given away free of charge, and contained useful information for motorists, including maps, instructions for repairing and changing tires, and lists of car mechanics, hotels and petrol stations. In 1904 the brothers published a similar guide to Belgium.

Guides were introduced for Algeria and Tunisia (1907); the Alps and the Rhine, covering northern Italy, Switzerland, Bavaria and the Netherlands (1908); Germany, Spain and Portugal (1910); the British Isles (1911); and "The Countries of the Sun" ("Les Pays du Soleil") covering northern Africa, southern Italy and Corsica (1911). In 1909, the first English-language version of the Michelin guide to France was published.
During the First World War publication of the guide was suspended. After the war, revised editions of the guide continued to be given away until 1920. The company's website recounts the story that Andr Michelin, visiting a tire merchant, noticed copies of the guide being used to prop up a workbench. Based on the principle that "man only truly respects what he pays for," the brothers decided to charge a price for the guide. They also made several changes, notably: listing restaurants by specific categories; the debut of hotel listings (initially only for Paris); and the abandonment of advertisements in the guide. Recognizing the growing popularity of the restaurant section of the guide, the brothers recruited a team of inspectors to visit and review restaurants, who were always careful in maintaining anonymity.
In 1926, the guide began to award stars for fine dining establishments. Initially, there was only a single star awarded. Then, in 1931, the hierarchy of one, two, and three stars was introduced. Finally, In 1936, the criteria for the starred rankings were published:
: "A very good restaurant in its category" ("Une trs bonne table dans sa catgorie")
: "Excellent cooking, worth a detour" ("Table excellente, mrite un dtour")
: "Exceptional cuisine, worth a special journey" ("Une des meilleures tables, vaut le voyage").
In 1931 the cover of the guide was changed from blue to red, and has remained so in all subsequent editions. During the Second World War, publication was again suspended, but in 1944, at the request of the Allied Forces, the 1939 guide to France was specially reprinted for military use; its maps were judged the best and most up-to-date available to the invading armies. Publication of the annual guide resumed on 16 May 1945, a week after VE Day.
In the early post-war years the lingering effects of wartime shortages led Michelin to impose an upper limit of two stars; by 1950 the French edition listed 38 establishments judged to meet this standard. The first Michelin Guide to Italy was published in 1956. It awarded no stars in the first edition. In 1974, the first guide to Britain since 1931 was published. Twenty-five stars were awarded.
In November 2005 Michelin produced its first American guide, concentrating on New York, covering 500 restaurants in the city's five boroughs and 50 hotels (Manhattan only). In 2007 a Tokyo Michelin Guide was launched. In the same year the guide introduced a magazine, toile. In 2008 a Hong Kong and Macao volume was added to the list of Michelin Guides. The Michelin website in 2013 notes that the guide is published in 14 editions covering 23 countries and sold in nearly 90 countries.
In 2008 the German restaurateur Juliane Caspar was appointed editor-in-chief of the French edition of the guide. She had previously been responsible for the Michelin guides to Germany, Switzerland, and Austria. She became the first woman and first non-French national to occupy the French position. The German newspaper Die Welt commented on the appointment, "In view of the fact German cuisine is regarded as a lethal weapon in most parts of France, this decision is like Mercedes announcing that its new director of product development is a Martian."


== Methods and layout ==

Red Guides have historically listed many more restaurants than rival guides have done, relying on an extensive system of symbols to describe each establishment in as little as two lines. Reviews of starred restaurants also include two to three culinary specialities. Recently, short summaries (23 lines) have been added to enhance descriptions of many establishments. These summaries are written in the language of the country for which the guide is published (though the Spain and Portugal volume is in Spanish only) but the symbols are the same throughout all editions.


=== Stars ===

Michelin reviewers (commonly called "inspectors") are completely anonymous; they do not identify themselves, and their meals and expenses are paid for by the company founded by the Michelin brothers, never by a restaurant being reviewed. In 2009 The New Yorker said:

Michelin has gone to extraordinary lengths to maintain the anonymity of its inspectors. Many of the company's top executives have never met an inspector; inspectors themselves are advised not to disclose their line of work, even to their parents (who might be tempted to boast about it); and, in all the years that it has been putting out the guide, Michelin has refused to allow its inspectors to speak to journalists. The inspectors write reports that are distilled, in annual "stars meetings" at the guide's various national offices, into the ranking of three stars, two stars, or one staror no stars. (Establishments that Michelin deems unworthy of a visit are not included in the guide.)

The French chef Paul Bocuse, one of the pioneers of nouvelle cuisine in the 1960s, said, "Michelin is the only guide that counts." In France, each year, at the time the guide is published, it sparks a media frenzy which has been compared to that for annual Academy Awards for films. Media and others debate likely winners, speculation is rife, and TV and newspapers discuss which restaurant might lose, and who might gain, a Michelin star.
The Michelin Guide also awards Rising Stars, an indication that a restaurant has the potential to qualify for a star, or an additional star.


=== Bib Gourmand ===

Since 1955, the guide has also highlighted restaurants offering "exceptional good food at moderate prices," a feature now called "Bib Gourmand". They must offer menu items priced below a maximum determined by local economic standards. Bib (Bibendum) is the company's nickname for the Michelin Man, its corporate logo for over a century.


=== Non-restaurant food ===
With the blurring of lines between restaurants and other eateries, Michelin is adapting too. From 2014, it had a separate listing for gastropubs in Ireland. The 2016 Guide for Hong Kong and Macau introduced an overview of notable street food establishments.


=== Other ratings ===
All listed restaurants, regardless of their star- or Bib Gourmand-status, also receive a "fork and spoon" designation, as a subjective reflection of the overall comfort and quality of the restaurant. Rankings range from one to five: One fork and spoon represents a "comfortable restaurant" and five signifies a "luxurious restaurant". Forks and spoons coloured red designate a restaurant that is considered "pleasant" as well.
Restaurants, independently of their other ratings in the guide, can also receive a number of other symbols next to their listing.
Coins indicate restaurants that serve a menu for a certain price or less, depending on the local monetary standard. In 2010 France, 2011 US and Japan Red Guides, the maximum permitted "coin" prices were 19, $25, and 5000, respectively.
Interesting view or Magnificent view, designated by a black or red symbol, are given to restaurants offering those features.
Grapes, a sake set, or a cocktail glass indicate restaurants that offer, at minimum, a "somewhat interesting" selection of wines, sake, or cocktails, respectively.


== Green Guides ==
The Michelin Green Guides review and rate attractions other than restaurants. There is a Green Guide for France as a whole, and a more detailed one for each of ten regions within France. Other Green Guides cover many countries, regions, and cities outside France. Many Green Guides are published in several languages. They include background information and an alphabetical section describing points of interest. Like the Red Guides, they use a three-star system for recommending sights ranging from "worth a trip" to "worth a detour", and "interesting".


== Controversies ==


=== Allegations of lax inspection standards and bias ===
Pascal Rmy, a veteran France-based Michelin inspector, and also a former Gault Millau employee, wrote a tell-all book published in 2004 entitled L'Inspecteur se met  table (literally, "The Inspector Sits Down at the Table"; idiomatically, "The Inspector Spills the Beans", or "The Inspector Puts It All on the Table"). Rmy's employment was terminated in December 2003 when he informed Michelin of his plans to publish his book. He brought a court case for unfair dismissal, which was unsuccessful.
Rmy described the French Michelin inspector's life as lonely, underpaid drudgery, driving around France for weeks on end, dining alone, under intense pressure to file detailed reports on strict deadlines. He maintained that the guide had become lax in its standards. Though Michelin states that its inspectors visited all 4,000 reviewed restaurants in France every 18 months, and all starred restaurants several times a year, Rmy said only about one visit every 3 years was possible because there were only 11 inspectors in France when he was hired, rather than the 50 or more hinted by Michelin. That number, he said, had shrunk to five by the time he was fired in December 2003.
Rmy also accused the guide of favouritism. He alleged that Michelin treated famous and influential chefs, such as Paul Bocuse and Alain Ducasse, as "untouchable" and not subject to the same rigorous standards as lesser-known chefs. Michelin denied Rmy's charges, but refused to say how many inspectors it actually employed in France. In response to Rmy's statement that certain three-star chefs were sacrosanct, Michelin said, "There would be little sense in saying a restaurant was worth three stars if it weren't true, if for no other reason than that the customer would write and tell us."


==== Allegations of prejudice for French cuisine ====
Some non-French food critics have alleged that the rating system is biased in favour of French cuisine or French dining standards. In England The Guardian commented in 1997 that "some people maintain the guide's principal purpose is as a tool of Gallic cultural imperialism". When Michelin published its first New York City Red Guide in 2005 Steven Kurutz of The New York Times noted that Danny Meyer's Union Square Cafe, a restaurant rated highly by The New York Times, Zagat Survey, and other prominent guides, received a no star-rating from Michelin. (He did acknowledge that the restaurant received positive mention for its ambience, and that two other restaurants owned by Meyer received stars). Kurutz also claimed the guide appeared to favour restaurants that "emphasized formality and presentation" rather than a "casual approach to fine dining". He also claimed that over half of the restaurants that received one or two stars "could be considered French".


==== Allegations of leniency with stars for Japanese cuisine ====
In 2010 Michelin guides ranked Japan as the country with the most starred restaurants. This sparked questioning over whether these high ratings were merited for Japanese restaurants, or whether the Michelin guide was too generous in giving out stars to gain an acceptance with Japanese customers and to enable the parent tire-selling company to market itself in Japan. The Wall Street Journal reported in 2010 that some Japanese chefs were surprised at receiving a star, and were reluctant to accept one, because the publicity caused an unmanageable jump in booking, affecting their ability to serve their traditional customers without lowering their quality.


=== Unwanted stars ===
Some restaurateurs have asked Michelin to revoke a star, because they felt that it created undesirable customer expectations or pressure to spend more on service and dcor. Some cases:
Casa Julio (Fontanars dels Alforins, Spain): After receiving a star for a perfumed cuisine in 2009, the restaurant chef Julio Biosca felt the award was granted to dishes that he did not like and restricted his creativity, and tried to remove his star and in December 2013, discontinued his tasting menu. The removal took place in the 2015 guide.
Petersham Nurseries Caf (London): After receiving a star in 2011, founder and chef Skye Gyngell received complaints from customers expecting formal dining, leading to her attempt to remove the star, and subsequent retirement from the restaurant.
't Huis van Lede (Belgium): After receiving a star in 2014, chef Frederick Dhooge claimed he did not want his Michelin star or his points in the Gault-Millau restaurant guide because certain customers expected a spectacle of stars and points kitchen, who are not interested in simple food from a Michelin starred restaurant.


== References ==


=== Reflist ===


== Further reading ==


=== Published in the 20th century ===
Michelin Guide to the British Isles, London: Michelin Tyre Company, 1913  (+ List of excursions)
Amiens before and during the war, Clermont-Ferrand: Michelin and Cie, 1919, OCLC 887914 
Michelin Guide to the Battlefields of the World War, Milltown, N. J: Michelin, 1919 
Strasbourg (in French), Clermont-Ferrand: Michelin & Cie, 1919 
St. Quentin-Cambrai (in French), Clermont-Ferrand: Michelin & cie, 1921 


=== Published in the 21st century ===
Trois toiles au Michelin: Une histoire de la haute gastronomie franaise et europenne, by Jean-Franois Mesplde and Alain Ducasse, 2004. ISBN 2-7000-2468-0. Follows the 60-odd chefs who have been awarded three stars.
The Perfectionist: Life and Death in Haute Cuisine, by Rudolph Chelminski, 2006. ISBN 978-0-14-102193-5. The story of Bernard Loiseau.
From behind the wall: Danish Newspaper 'Berlingske' Employee 'Awards'


== External links ==
Michelin Red Guides
Ogushi's 3 Stars Restaurants List of France (Past & Now)