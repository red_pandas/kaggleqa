A surrogacy arrangement or surrogacy agreement is the carrying of a pregnancy for intended parents. There are two main types of surrogacy, gestational surrogacy (also known as host or full surrogacy) which was first achieved in April 1986 and traditional surrogacy (also known as partial, genetic, or straight surrogacy). In gestational surrogacy, the pregnancy results from the transfer of an embryo created by in vitro fertilization (IVF), in a manner so the resulting child is genetically unrelated to the surrogate. Gestational surrogates are also referred to as gestational carriers. In traditional surrogacy, the surrogate is impregnated naturally or artificially, but the resulting child is genetically related to the surrogate. In the United States, gestational surrogacy is more common than traditional surrogacy and is considered less legally complex.
Intended parents may seek a surrogacy arrangement when either pregnancy is medically impossible, pregnancy risks present an unacceptable danger to the mother's health or is a same sex couple's preferred method of procreation. Monetary compensation may or may not be involved in these arrangements. If the surrogate receives compensation beyond reimbursement of medical and other reasonable expenses, the arrangement is considered commercial surrogacy; otherwise, it is referred to as altruistic. The legality and costs of surrogacy vary widely between jurisdictions, sometimes resulting in interstate or international surrogacy arrangements.


== HistoryEdit ==
Having another woman bear a child for a couple to raise, usually with the male half of the couple as the genetic father, is referred to in antiquity. Babylonian law and custom allowed this practice, and infertile woman could use the practice to avoid a divorce, which would otherwise be inevitable.
Many developments in medicine, social customs, and legal proceedings worldwide paved the way for modern commercial surrogacy:
1930s   In the U.S., pharmaceutical companies Schering-Kahlbaum and Parke-Davis started the mass production of estrogen.
1944   Harvard Medical School professor John Rock broke ground by becoming the first person to fertilize human ova outside the uterus.
1953   Researchers successfully performed the first cryopreservation of sperm.
1971   The first commercial sperm bank opened in New York, which spurred the growth of this type of business into a highly profitable venture.
1978   Louise Brown, the first test-tube baby, was born in England. She was the product of the first successful IVF procedure.
1980   Michigan lawyer Noel Keane wrote the first surrogacy contract. He continued his work with surrogacy through his Infertility Center, through which he created the contract leading to the Baby M case.
1985   A woman carried the first successful gestational surrogate pregnancy.
1986   Melissa Stern, otherwise known as "Baby M," was born in the U.S. The surrogate and biological mother, Mary Beth Whitehead, refused to cede custody of Melissa to the couple with whom she made the surrogacy agreement. The courts of New Jersey found that Whitehead was the child's legal mother and declared contracts for surrogate motherhood illegal and invalid. However, the court found it in the best interest of the infant to award custody of Melissa to the child's biological father, William Stern, and his wife Elizabeth Stern, rather than to Whitehead, the surrogate mother.
1990   In California, gestational carrier Anna Johnson refused to give up the baby to intended parents Mark and Crispina Calvert. The couple sued her for custody (Calvert v. Johnson), and the court upheld their parental rights. In doing so, it legally defined the true mother as the woman who, according to the surrogacy agreement, intends to create and raise a child.
1994
Latin American fertility specialists convened in Chile to discuss assisted reproduction and its ethical and legal status.
The Chinese Ministry of Health banned gestational surrogacy because of the legal complications of defining true parenthood and possible refusal by surrogates to relinquish a baby.

2009   The Chinese government increased enforcement of the gestational-surrogacy ban, and Chinese women began coming forth with complaints of forced abortions.
Surrogacy has the potential for various kinds of clash between surrogate mothers and intended parents. For instance, the intended parents of the fetus may ask for an abortion when complications arise and the surrogate mother may oppose the abortion.


== Types of surrogacyEdit ==


=== Gestational surrogacy (GS)Edit ===
A surrogate is implanted with an embryo created by IVF. The resulting child is genetically unrelated to the surrogate. There are several sub-types of gestational surrogacy as noted below.


==== Gestational surrogacy with embryo from both intended parents (GS/IP)Edit ====
A surrogate is implanted with an embryo created by IVF, using intended father's sperm and intended mother's eggs.


==== Gestational surrogacy and egg donation (GS/ED)Edit ====
A surrogate is implanted with an embryo created by IVF, using intended father's sperm and a donor egg where the donor is not the surrogate. The resulting child is genetically related to intended father and genetically unrelated to the surrogate.


==== Gestational surrogacy and donor sperm (GS/DS)Edit ====
A surrogate is implanted with an embryo created by IVF, using intended mother's egg and donor sperm. The resulting child is genetically related to intended mother and genetically unrelated to the surrogate.


==== Gestational surrogacy and donor embryo (GS/DE)Edit ====
A donor embryo is implanted in a surrogate; such embryos may be available when others undergoing IVF have embryos left over, which they opt to donate to others. The resulting child is genetically unrelated to the intended parent(s) and genetically unrelated to the surrogate.


=== Traditional surrogacy (TS)Edit ===
This involves naturally or artificially inseminating a surrogate with intended father's sperm via IUI, IVF or home insemination. With this method, the resulting child is genetically related to intended father and genetically related to the surrogate.


==== Traditional surrogacy and donor sperm (TS/DS)Edit ====
A surrogate is artificially inseminated with donor sperm using ICI, IUI or IVF. An ICI insemination may be performed privately by the parties without the intervention of a doctor or physician. The resulting child is genetically unrelated to the intended parent(s) but is genetically related to the surrogate. In many jurisdictions, the 'commissioning parents' will need to go through an adoption process in order to have legal rights in respect to the resulting child. Many fertility centres which provide for surrogacy will assist the parties through this process.


== Legal issuesEdit ==

As of 2013, locations where a woman could legally be paid to carry another's child through IVF and embryo transfer included India, Georgia, Russia, Thailand, Ukraine and a few U.S. states.
The legal aspects of surrogacy in any particular jurisdiction tend to hinge on a few central questions:
Are surrogacy agreements enforceable, void or prohibited? Does it make a difference whether the surrogate mother is paid (commercial) or simply reimbursed for expenses (altruistic)?
What, if any, difference does it make whether the surrogacy is traditional or gestational?
Is there an alternative to post-birth adoption for the recognition of the intended parents as the legal parents, either before or after the birth?
Although laws differ widely from one jurisdiction to another, some generalizations are possible:
The historical legal assumption has been that the woman giving birth to a child is that child's legal mother, and the only way for another woman to be recognized as the mother is through adoption (usually requiring the birth mother's formal abandonment of parental rights).
Even in jurisdictions that do not recognize surrogacy arrangements, if the genetic parents and the birth mother proceed without any intervention from the government and have no changes of heart along the way, they will likely be able to achieve the effects of surrogacy by having the surrogate mother give birth and then give the child up for private adoption to the intended parents.
If the jurisdiction specifically prohibits surrogacy, however, and finds out about the arrangement, there may be financial and legal consequences for the parties involved. One jurisdiction (Quebec) prevented the genetic mother's adoption of the child even though that left the child with no legal mother.
Some jurisdictions specifically prohibit only commercial and not altruistic surrogacy. Even jurisdictions that do not prohibit surrogacy may rule that surrogacy contracts (commercial, altruistic, or both) are void. If the contract is either prohibited or void, then there is no recourse if one party to the agreement has a change of heart: If a surrogate changes her mind and decides to keep the child, the intended mother has no claim to the child even if it is her genetic offspring, and the couple cannot get back any money they may have paid or reimbursed to the surrogate; if the intended parents change their mind and do not want the child after all, the surrogate cannot get any reimbursement for expenses, or any promised payment, and she will be left with legal custody of the child.
Jurisdictions that permit surrogacy sometimes offer a way for the intended mother, especially if she is also the genetic mother, to be recognized as the legal mother without going through the process of abandonment and adoption.
Often this is via a birth order in which a court rules on the legal parentage of a child. These orders usually require the consent of all parties involved, sometimes including even the husband of a married gestational surrogate. Most jurisdictions provide for only a post-birth order, often out of an unwillingness to force the surrogate mother to give up parental rights if she changes her mind after the birth.
A few jurisdictions do provide for pre-birth orders, generally in only those cases when the surrogate mother is not genetically related to the expected child. Some jurisdictions impose other requirements in order to issue birth orders, for example, that the intended parents be heterosexual and married to one another. Jurisdictions that provide for pre-birth orders are also more likely to provide for some kind of enforcement of surrogacy contracts.


== Ethical issuesEdit ==
Ethical issues that have been raised with regards to surrogacy include:
To what extent should society be concerned about exploitation, commodification, and/or coercion when women are paid to be pregnant and deliver babies, especially in cases where there are large wealth and power differentials between intended parents and surrogates?
To what extent is it right for society to permit women to make contracts about the use of their bodies?
To what extent is it a woman's human right to make contracts regarding the use of her body?
Is contracting for surrogacy more like contracting for employment/labor, or more like contracting for prostitution, or more like contracting for slavery?
Which, if any, of these kinds of contracts should be enforceable?
Should the state be able to force a woman to carry out "specific performance" of her contract if that requires her to give birth to an embryo she would like to abort, or to abort an embryo she would like to carry to term?

What does motherhood mean?
What is the relationship between genetic motherhood, gestational motherhood, and social motherhood?
Is it possible to socially or legally conceive of multiple modes of motherhood and/or the recognition of multiple mothers?

Should a child born via surrogacy have the right to know the identity of any/all of the people involved in that child's conception and delivery?


== Religious issuesEdit ==

Different religions take different approaches to surrogacy, often related to their stances on assisted reproductive technology in general.


=== CatholicismEdit ===
Paragraph 2376 of the Catechism of the Catholic Church states that: "Techniques that entail the dissociation of husband and wife, by the intrusion of a person other than the couple (donation of sperm or ovum, surrogate uterus), are gravely immoral."


=== JudaismEdit ===
Jewish legal scholars debate this issue, some contend that that parenthood is determined by the woman giving birth while others opt to consider the genetic parents the legal parents, this is a hotly debated issue in recent years. More recently, Jewish religious establishments have accepted surrogacy only if it is full gestational surrogacy with both intended parents' gametes included and fertilization done via IVF.


== Psychological concernsEdit ==


=== SurrogateEdit ===
A study by the Family and Child Psychology Research Centre at City University London in 2002 concluded that surrogate mothers rarely had difficulty relinquishing rights to a surrogate child and that the intended mothers showed greater warmth to the child than mothers conceiving naturally.
Anthropological studies of surrogates have shown that surrogates engage in various distancing techniques throughout the surrogate pregnancy so as to ensure that they do not become emotionally attached to the baby. Many surrogates intentionally try to foster the development of emotional attachment between the intended mother and the surrogate child.
Surrogates are generally encouraged by the agency they go through to become emotionally detached from the fetus prior to giving birth.
Instead of the popular expectation that surrogates feel traumatized after relinquishment, an overwhelming majority describe feeling empowered by their surrogacy experience.
Although surrogate mothers generally report being satisfied with their experience as surrogates there are cases in which they are not. Unmet expectations are associated with dissatisfaction. Some women did not feel a certain level of closeness with the couple and others did not feel respected by the couple.
Some women experience emotional distress when participating as a surrogate mother. This could be due to a lack of therapy and emotional support through the surrogate process.
Some women have psychological reactions when being surrogate mothers. These include depression when surrendering the child, grief, and even refusal to release the child.
A 2011 study from the Centre for Family Research at the University of Cambridge found that surrogacy does not have a negative impact on the surrogate's own children.


=== ChildEdit ===
A recent study (involving 32 surrogacy, 32 egg donation, and 54 natural conception families) examined the impact of surrogacy on motherchild relationships and children's psychological adjustment at age seven. Researchers found no differences in negativity, maternal positivity, or child adjustment.


== Fertility tourismEdit ==

Fertility tourism for surrogacy is driven by legal regulations in the home country, or lower price abroad.


=== IndiaEdit ===

India is a main destination for surrogacy. Indian surrogates have been increasingly popular with intended parents in industrialized nations because of the relatively low cost. Indian clinics are at the same time becoming more competitive, not just in the pricing, but in the hiring and retention of Indian females as surrogates. Clinics charge patients between $10,000 and $28,000 for the complete package, including fertilization, the surrogate's fee, and delivery of the baby at a hospital. Including the costs of flight tickets, medical procedures and hotels, it comes to roughly a third of the price compared with going through the procedure in the UK.
Surrogacy in India is of low cost and the laws are flexible. In 2008, the Supreme Court of India in the Manji's case (Japanese Baby) has held that commercial surrogacy is permitted in India. That has again increased the international confidence in going in for surrogacy in India. But as of 2014, a surrogacy ban was placed on homosexual couples and single parents.
There is an upcoming Assisted Reproductive Technology Bill, aiming to regulate the surrogacy business. However, it is expected to increase the confidence in clinics by sorting out dubious practitioners, and in this way stimulate the practice.


=== Russian FederationEdit ===
Liberal legislation makes Russia attractive for "reproductive tourists" looking for techniques not available in their countries. Intended parents come there for oocyte donation, because of advanced age or marital status (single women and single men) and when surrogacy is considered. Gestational surrogacy, even commercial is absolutely legal in Russia, being available for practically all adults willing to be parents. Foreigners have the same rights as for assisted reproduction as Russian citizens. Within three days after the birth the commissioning parents obtain a Russian birth certificate with both their names on it. Genetic relation to the child (in case of donation) does not matter. On August 4, 2010, a Moscow court ruled that a single man who applied for gestational surrogacy (using donor eggs) could be registered as the only parent of his son, becoming the first man in Russia to defend his right to become a father through a court procedure. The surrogate mother's name was not listed on the birth certificate; the father was listed as the only parent.


=== UkraineEdit ===

Surrogacy is completely legal in Ukraine. However, only healthy mothers who have had children before can become surrogates. Surrogates in Ukraine have zero parental rights over the child, as stated on Article 123 of the Family Code of Ukraine. Thus, a surrogate cannot refuse to hand the baby over in case she changes her mind after birth. Only married couples can legally go through gestational surrogacy in Ukraine.


=== United StatesEdit ===
The United States is sought as a location for surrogate mothers by some couples seeking a green card in the U.S., since the resulting child can get birthright citizenship in the United States, and can thereby apply for green cards for the parents when the child turns 21 years of age. However, this is not the main reason. People come to the US for surrogacy procedures, including to enjoy a better quality of medical technology and care, as well as the high level of legal protections afforded through some US state courts to surrogacy contracts as compared to other countries. Increasingly, homosexual couples who face restrictions using IVF and surrogacy procedures in their home countries travel to US states where it is legal.


== See alsoEdit ==


== ReferencesEdit ==


== Further readingEdit ==
Teman, Elly (March, 2010). "Birthing a Mother: The Surrogate Body and the Pregnant Self". Berkeley: University of California Press.
Siegel-Itzkovich, Judy (April 3, 2010). "Womb to Let". The Jerusalem Post.
Li, Shan (February 18, 2012). "Chinese Couples Come to U.S. to Have Children Through Surrogacy". Los Angeles Times.


== External linksEdit ==
"Surrogacy", Better Health Channel, State Government of Victoria, Australia