Apathy (also called perfunctoriness) is a lack of feeling, emotion, interest, and concern. Apathy is a state of indifference, or the suppression of emotions such as concern, excitement, motivation, and/or passion. An apathetic individual has an absence of interest in or concern about emotional, social, spiritual, philosophical and/or physical life and the world.
The apathetic may lack a sense of purpose or meaning in their life. An apathetic person may also exhibit insensibility or sluggishness. In positive psychology, apathy is described as a result of the individual feeling they do not possess the level of skill required to confront a challenge (i.e. "Flow"). It may also be a result of perceiving no challenge at all (e.g. the challenge is irrelevant to them, or conversely, they have learned helplessness). Apathy may be a sign of more specific mental problems such as schizophrenia or dementia. However, apathy is something that all people face in some capacity. It is a natural response to disappointment, dejection, and stress. As a response, apathy is a way to forget about these negative feelings. This type of common apathy is usually only felt in the short-term and when it becomes a long-term or even lifelong state is when deeper social and psychological issues are most likely present.
Apathy should be distinguished from reduced affect, which refers to reduced emotional expression but not necessarily reduced emotion.


== EtymologyEdit ==
Although the word apathy was first used in 1594 and is derived from the Greek  (apatheia), from  (apaths, "without feeling" from a- ("without, not") and pathos ("emotion")), it is important not to confuse the two terms. Also meaning "absence of passion," "apathy" or "insensibility" in Greek, the term apatheia was used by the Stoics to signify a (desirable) state of indifference towards events and things which lie outside one's control (that is, according to their philosophy, all things exterior, one being only responsible for his representations and judgments). In contrast to apathy, apatheia is considered a virtue, especially in Orthodox monasticism. In the Philokalia the word dispassion is used for apatheia, so as not to confuse it with apathy.


== History and other viewsEdit ==
Christians have historically condemned apathy as a deficiency of love and devotion to God and His works. This interpretation of apathy is also referred to as Sloth and is listed among the Seven Deadly Sins. Clemens Alexandrinus used the term to draw to Christianity philosophers who aspired after virtue.
The modern concept of apathy became more well-known after World War I, when it was one of the various forms of "shell shock". Soldiers who lived in the trenches amidst the bombing and machine gun fire, and who saw the battlefields strewn with dead and maimed comrades, developed a sense of disconnected numbness and indifference to normal social interaction when they returned from combat.
In 1950, US novelist John Dos Passos wrote: "Apathy is one of the characteristic responses of any living organism when it is subjected to stimuli too intense or too complicated to cope with. The cure for apathy is comprehension."
Douglas Hofstadter suggests that, recognizing that the human brain's "ego" is nothing but a construct, no emotion is necessary. Since the realization of the future of an expanding universe, apathy is the only intelligent response. It is in contrast to the contented feeling of self-satisfaction of complacency, driven by the illusion of the "ego".


=== TechnologyEdit ===
Apathy is a normal way for humans to cope with stress. Being able to "shrug off" disappointments is considered an important step in moving people forward and driving them to try other activities and achieve new goals. Coping seems to be one of the most important aspects of getting over a tragedy and an apathetic reaction may be expected. With the addition of the handheld device and the screen between people, apathy has also become a common occurrence on the net as users observe others being bullied, slandered, threatened or even sent horrific pictures. The bystander effect grows to an apathetic level as people lose interest in caring for others who are not in their circle, even going so far as the join in the torment as well.


=== Social originEdit ===
There may be other things contributing to a person's apathy. Activist David Meslin argues that people often care, and that apathy is often the result of social systems actively obstructing engagement and involvement. He describes various obstacles that prevent people from knowing how or why they might get involved in something. Meslin focuses on design choices that unintentionally or intentionally exclude people. These include: capitalistic media systems that have no provisions for ideas that are not immediately (monetarily) profitable, government and political media (e.g. notices) that make it difficult for potentially interested individuals to find relevant information, and media portrayals of heroes as "chosen" by outside forces rather than self-motivated. He moves that we redefine social apathy to think of it, not as a population that is stupid or lazy, but as a result of poorly designed systems that fail to invite others to participate.
Apathy has been socially viewed as worse than things such as hate or anger. Not caring whatsoever, in the eyes of some, is even worse than having distaste for something. Author Leo Buscaglia is quoted as saying "I have a very strong feeling that the opposite of love is not hate-it's apathy. It's not giving a damn." Helen Keller claimed that apathy is the "worst of them all" when it comes to the various evils of the world. French social commentator and political thinker Charles de Montesquieu stated that "the tyranny of a prince in an oligarchy is not so dangerous to the public welfare as the apathy of a citizen in the democracy." As can be seen by these quotes and various others, the social implications of apathy are great. Many people believe that not caring at all can be worse for society than individuals who are overpowering or hateful.


=== In the school systemEdit ===
Apathy in students, especially those in high school, is a growing phenomenon. Apathy in schools is most easily recognized by students being unmotivated or, quite commonly, being motivated by outside factors. For example, when asked about their motivation for doing well in school, fifty percent of students cited outside sources such as "college acceptance" or "good grades". On the contrary, only fourteen percent cited "gaining an understanding of content knowledge or learning subject material" as their motivation to do well in school. As a result of these outside sources, and not a genuine desire for knowledge, students often do the minimum amount of work necessary to get by in their classes. This then leads to average grades and test grades but no real grasping of knowledge. Many students cited that "assignments/content was irrelevant or meaningless" and that this was the cause of their apathetic attitudes toward their schooling. These apathetic attitudes lead to teacher and parent frustration. Other causes of apathy in students include situations within their home life, media influences, peer influences, and school struggles and failures. Some of the signs for apathetic students include declining grades, skipping classes, routine illness, and behavioral changes both in school and at home. This is accurate to about 76% of students in high school.


=== BystanderEdit ===
Also known as the bystander effect, bystander apathy occurs when, during an emergency, those standing by do nothing to help but instead stand by and watch. Sometimes this can be caused by one bystander observing other bystanders and imitating their behavior. If other people are not acting in a way that makes the situation seem like an emergency that needs attention, often other bystanders will act in the same way. The diffusion to responsibility can also be to blame for bystander apathy. The more people that are around in emergency situations, the more likely individuals are to think that someone else will help so they do not need to. This theory was popularized by social psychologists in response to the 1964 Kitty Genovese murder. The murder took place in New York and the victim, Genovese, was stabbed to death as bystanders reportedly stood by and did nothing to stop the situation or even call the police. Latane and Darley are the two psychologists who did research on this theory. They performed different experiments that placed people into situations where they had the opportunity to intervene or do nothing. The individuals in the experiment were either by themselves, with a stranger(s), with a friend, or with a confederate. The experiments ultimately led them to the conclusion that there are many social and situational factors that are behind whether a person will react in an emergency situation or simply remain apathetic to what is occurring.


=== CommunicationEdit ===
Apathy is one psychological barrier to communication. An apathetic listener creates a communication barrier by not caring or paying attention to what they are being told. An apathetic speaker, on the other hand, tends to not relate information well and, in their lack of interest, may leave out key pieces of information that need to be communicated. Within groups, an apathetic communicator can be detrimental. Their lack of interest and/or passion can inhibit the other group members in what they are trying to accomplish. Within interpersonal communication, an apathetic listener can make the other feel that they are not cared for or about. Overall, apathy is a dangerous barrier to successful communication. Apathetic listeners and speakers are individuals that have no care for what they are trying to communicate, or what is being communicated to them.


== MeasuringEdit ==


=== Apathy Evaluation ScaleEdit ===
Developed by Robert Marin in 1991, the Apathy Evaluation Scale (AES) is a way of measuring as related to brain-related pathology. Centered around evaluation, the scale can either be self-informed, or other-informed. The three versions of the test include self, informant such as a family member, and clinician. The scale is based around questionnaires that ask about topics including interest, motivation, socialization, and how the individual spends their time. The individual or informant answers on a scale of "not at all", "slightly", "somewhat" or "a lot". Each item on the evaluation is created with positive or negative syntax and deals with cognition, behavior, and emotion. Each item is then scored and, based on the score, the individual's level of apathy can be evaluated.


== Medical aspectsEdit ==


=== DepressionEdit ===

Mental health journalist and author John McManamy argues that although psychiatrists do not explicitly deal with the condition of apathy, it is a psychological problem for some depressed people, in which they get a sense that "nothing matters", the "lack of will to go on and the inability to care about the consequences". He describes depressed people who "...cannot seem to make [themselves] do anything," who "can't complete anything," and who do not "feel any excitement about seeing loved ones." He acknowledges that the Diagnostic and Statistical Manual of Mental Disorders does not discuss apathy.
In a Journal of Neuropsychiatry and Clinical Neurosciences article from 1991, Robert Marin, MD, claimed that apathy occurs due to brain damage or neuropsychiatric illnesses such as Alzheimers, dementia, Parkinson's, or Huntingtons, or else an event such as a stroke. Marin argues that apathy should be regarded as a syndrome or illness.
A review article by Robert van Reekum, MD, et al. from the University of Toronto in the Journal of Neuropsychiatry (2005) claimed that an obvious relationship between depression and apathy exists in some populations.


=== Alzheimer'sEdit ===
Apathy affects nearly 50-70 percent of individuals with Alzheimer's Disease. It is a neuropsychiatric symptom of Alzheimer's associated with functional impairment. There are two main causes of apathy that have been recognized in patients with Alzheimer's. First, dysfunction in frontal systems of the brain is thought to be an important neurobiological basis for the onset of apathy. Second, the neuropathological changes associated with dementia, and therefore Alzheimer's, may also result in apathy. While there is a great lack in large-scale clinical trials in the effort of finding specific treatment for apathy, Cholinesterase inhibitors used with individuals who have Alzheimer's have been seen to have positive responses from apathetic symptoms.


=== AnxietyEdit ===
While apathy and anxiety may appear to be separate, and different, states of being, there are many ways that severe anxiety can cause apathy. First, the emotional fatigue that so often accompanies severe anxiety leads to one's emotions being worn out, thus leading to apathy. Second, the low serotonin levels associated with anxiety often lead to less passion and interest in the activities in one's life which can be seen as apathy. Third, negative thinking and distractions associated with anxiety can ultimately lead to a decrease in one's overall happiness which can then lead to an apathetic outlook about one's life. Finally, the difficulty enjoying activities that individuals with anxiety often face can lead to them doing these activities much less often and can give them a sense of apathy about their lives. Even behavioral apathy may be found in individuals with anxiety in the form of them not wanting to make efforts to treat their anxiety.


=== OtherEdit ===
Often, apathy has been felt after witnessing horrific acts, such as the killing or maiming of people during a war. It is also known to be a distinct psychiatric syndrome that is associated with many conditions, some of which are: CADASIL syndrome, depression, Alzheimer's disease, Chagas' disease, Creutzfeldt-Jakob disease, dementia (and dementias such as Alzheimer's disease, vascular dementia, and frontotemporal dementia), Korsakoff's syndrome, excessive vitamin D; hypothyroidism; hyperthyroidism; general fatigue; Huntington's disease; Pick's disease; progressive supranuclear palsy (PSP); schizophrenia; schizoid personality disorder; bipolar disorder;, Autism, Asperger's Syndrome, and others. Some medications and the heavy use of drugs such as opiates or GABA-ergic drugs may bring apathy as a side effect.


== See alsoEdit ==
Callous and unemotional traits
Detachment (philosophy)
Reduced affect display


== NotesEdit ==


== ReferencesEdit ==
 This article incorporates text from a publication now in the public domain: Chambers, Ephraim, ed. (1728). "article name needed". Cyclopdia, or an Universal Dictionary of Arts and Sciences (first ed.). James and John Knapton, et al. 


== External linksEdit ==
 Quotations related to Apathy at Wikiquote
The Roots of Apathy  Essay By David O. Solmitz
Apathy  McMan's Depression and Bipolar Web, by John McManamy