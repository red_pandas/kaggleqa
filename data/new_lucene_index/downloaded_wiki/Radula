The radula (plural radulae or radulas) is an anatomical structure that is used by molluscs for feeding, sometimes compared rather inaccurately to a tongue. It is a minutely toothed, chitinous ribbon, which is typically used for scraping or cutting food before the food enters the esophagus. The radula is unique to the molluscs, and is found in every class of mollusc except the bivalves.
Within the gastropods, the radula is used in feeding by both herbivorous and carnivorous snails and slugs. The arrangement of teeth (also known as denticles) on the radula ribbon varies considerably from one group to another.
In most of the more ancient lineages of gastropods, the radula is used to graze, by scraping diatoms and other microscopic algae off rock surfaces and other substrates.
Predatory marine snails such as the Naticidae use the radula plus an acidic secretion to bore through the shell of other molluscs. Other predatory marine snails, such as the Conidae, use a specialized radula tooth as a poisoned harpoon. Predatory pulmonate land slugs, such as the ghost slug, use elongated razor-sharp teeth on the radula to seize and devour earthworms. Predatory cephalopods, such as squid, use the radula for cutting prey.
The introduction of the term "radula" is usually attributed to Alexander von Middendorff in 1848.


== Components ==
A typical radula comprises a number of bilaterally-symmetrical self-similar rows of teeth rooted in a radular membrane. Some species have teeth that bend with the membrane as it moves over the odontophore, whereas in other species, the teeth are firmly rooted in place, and the entire radular structure moves as one entity.


=== Radular membrane ===
The elastic, delicate radular membrane may be a single tongue, or may split into two (bipartite).


=== Hyaline shield ===
See Hyaline shield for more details.


=== Odontophore ===
The odontophore is the tongue of flesh underlying the radular membrane, and controls the organ's protrusion and return. It can be likened to a pulley wheel over which the radular 'string' is pulled.


=== Flexibility ===
The radular teeth can generally bend in a sideways direction. In the patellogastropods, though, the teeth lost this ability and became fixed.


=== Teeth ===
The radula comprises multiple, identical (or near enough) rows of teeth; often, each tooth in a row (along with its symmetric partner) will have a unique morphology.
Each tooth can be divided into three sections: a base, a shaft, and a cusp. In radulae that just sweep, rather than rasp, the underlying substrate, the shaft and cusp are often continuous and cannot be differentiated.
The teeth often tesselate with their neighbours, and this interlocking serves to make it more difficult to remove them from the radular ribbon.


== Radula formulae ==

The number, shape, and specialized arrangement of molluscan teeth in each transverse row is consistent on a radula, and the different patterns can be used as a diagnostic characteristic to identify the species in many cases.
Each row of radular teeth consists of
One central or median tooth (or rachidian tooth, rachis tooth)
On each side: one or more lateral teeth
And then beyond that: one or more marginal teeth.
This arrangement is expressed in a radular tooth formula, with the following abbreviations :
R : designates the central tooth or the rachis tooth (in case of lack of central tooth : the zero sign 0)
the lateral teeth on each side are expressed by a specific number or D, in case the outer lateral tooth is dominant.
the marginal teeth are designated by a specific number or, in case they are in a very large numbers, the infinity symbol 

This can be expressed in a typical formula such as:
3 + D + 2 + R + 2 + D + 3
This formula means: across the radula there are 3 marginal teeth, 1 dominant lateral tooth, 2 lateral teeth, and one central tooth.
Another formula for describing radulae omits the use of letters and simply gives a sequence of numbers in the order marginal-lateral-rachidian-lateral-marginal, thus:
1-1-1-1-1
This particular formula, which is common to the scaphopods, means one marginal tooth, one lateral tooth, one rachidian tooth, one lateral tooth, and one marginal tooth across the ribbon.


== Morphology ==
The morphology of the radula is related to diet. However, it is not fixed per species; some molluscs can adapt the form of their radular teeth according to which food sources are abundant.
Pointed teeth are best suited to grazing on algal tissue, whereas blunt teeth are preferable if feeding habits entail scraping epiphytes from surfaces.


== Use ==

The radula is used in two main ways: either as a rake, generally to comb up microscopic, filamentous algae from a surface; or as a rasp, to feed directly on a plant. The rhipidoglossan (see below) and, to a lesser extent, the taenigloissan radula types are suited to less strenuous modes of feeding, brushing up smaller algae or feeding on soft forms; molluscs with such radulae are rarely able to feed on leathery or coralline algae. On the other hand, the docoglossan gastropod radula allows a very similar diet to the polyplacophora, feeding primarily on these resistant algae, although microalgae are also consumed by species with these radular types.
The sacoglossans (sea slugs) form an interesting anomaly in that their radula comprises a single row; they feed by sucking on cell contents, rather than rasping at tissue, and most species feed on a single genus or species of alga. Here, the shape of the radular teeth has a close match with the food substrate on which they are used. Triangular teeth are suited to diets of calcified algae, and are also present in radulae used to graze on Caulerpa; in both these cases the cell walls are predominantly composed of xylan. Sabot-shaped teeth  rods with a groove along one side  are associated with diets of crossed-fibrillar cellulose-walled algae, such as the Siphonocladaceae and Cladophorales, whereas blade-shaped teeth are more generalist.


== Early molluscs ==
The first bona fide radula dates to the Early Cambrian, although trace fossils from the earlier Ediacaran have been suggested to have been made by the radula of the organism Kimberella.
A so-called radula from the early Cambrian was discovered in 1974, this one preserved with fragments of the mineral ilmenite suspended in a quartz matrix, and showing similarities to the radula of the modern cephalopod Sepia. However, this was since re-interpreted as Salterella [/Volborthella?].
Based on the bipartite nature of the radular dentition pattern in solenogasters, larval gastropods and larval polyplacophora, it has been postulated that the ancestral mollusc bore a bipartite radula (although the radular membrane may not have been bipartite).


== In chitons ==
Each row of the polyplacophoran radula has two mineralized teeth used to abrade the substrate, and two longer teeth that sweep up any debris. The other 13 teeth on each row do not appear to be involved in feeding.
The teeth of Chaetopleura apiculata comprise fibres surrounded by magnetite, sodium and magnesium.


== In gastropods ==


=== Anatomy and method of functioning ===
The mouth of the gastropods is located below the anterior part of the mollusc. It opens into a pocket-like buccal cavity, containing the radula sac, an evaginated pocket in the posterior wall of this cavity.
The radula apparatus consists of two parts :
the cartilaginous base (the odontophore), with the odontophore protractor muscle, the radula protractor muscle and the radula retractor muscle.
the radula itself, with its longitudinal rows of chitinous and recurved teeth, the cuticula.
The odontophore is movable and protrusible, and the radula itself is movable over the odontophore. Through this action the radula teeth are being erected. The tip of the odontophore then scrapes the surface, while the teeth cut and scoop up the food and convey the particles through the esophagus to the digestive tract.
In a flexoglossate radula (the primitive condition), the teeth flex outwards to the sides as they round the tip of the odontophore, before flexing back inwards. In the derived stereoglossate condition, the teeth do not flex.
These actions continually wear down the frontal teeth. New teeth are continuously formed at the posterior end of the buccal cavity in the radula sac. They are slowly brought forward to the tip by a slow forward movement of the ribbon, to be replaced in their turn when they are worn out.
Teeth production is rapid (some species produce up to five rows per day). The radular teeth are produced by odontoblasts, cells in the radula sac.
The number of teeth present depends on the species of mollusc and may number more than 100,000. Large numbers of teeth in a row (actually v-shaped on the ribbon in many species) is presumed to be a more primitive condition, but this may not always be true.
The greatest number of teeth per row is found in Pleurotomaria (deep water gastropods in an ancient lineage) which has over 200 teeth per row (Hyman, 1967).
The shape and arrangement of the radular teeth is an adaptation to the feeding regimen of the species.
The teeth of the radula are lubricated by the mucus of the salivary gland, just above the radula. Food particles are trapped into this sticky mucus, smoothing the progress of food into the oesophagus.
Certain gastropods use their radula teeth to hunt other gastropods and bivalve molluscs, scraping away the soft parts for ingestion. Cone shells have a single radula tooth, that can be thrust like a harpoon into its prey, releasing a neurotoxin.


==== The seven basic types ====
The docoglossan or stereoglossan radula: in each row there is one usually small central tooth, flanked by 1-3 laterals (with the outer one dominant) and a few (3 at the most) hooked marginals. The central tooth may even be absent. The teeth are fixed in a stiff position on the radular ribbon. This is the most primitive radula type, and we could assume it represents the plesiomorphic condition i.e., the primitive character state, that is taken from an ancestor without change, such as would be possessed by the earliest molluscs (Eogastropoda, also Polyplacophora; limpet families Patellidae, Lottiidae, Lepetidae). The radula operates like a chain of 'shovels', and the rigid structure operates like a rasp, scraping at hardened macroalgae. Accordingly, docoglossan radulae are often hardened by biomineralization. Spaces between the teeth make the radula ill-suited to collecting microalgae.Formula: 3 + D + 2 + R + 2 + D + 3
Or: 3 + D + 2 + 0 + 2 + D + 3

Rhipidoglossan radula: a large central and symmetrical tooth, flanked on each side by several (usually five) lateral teeth and numerous closely packed flabellate marginals, called uncini (typical examples: Vetigastropoda, Neritomorpha). This already marks an improvement over the simple docoglossan state. These radulae generally operate like 'brooms', brushing up loose microalgae.Formula:  + 5 + R + 5 + 
In case of a dominant lateral tooth:  + D + 4 + R + 4 + D + 

Hystrichoglossan radula: each row with lamellate and hooked lateral teeth and hundreds of uniform marginal teeth that are tufted at their ends (typical example : Pleurotomariidae).
The radula formula of, for example, Pleurotomaria (Entemnotrochus) rumphii is : . 14. 27. 1. 27. 14. 

Taenioglossan radula: seven teeth in each row: one middle tooth, flanked on each side by one lateral and two marginal teeth (characteristic of the majority of the Caenogastropoda). These operate like 'rakes', scraping algae and gathering the resultant detritus.Formula : 2 + 1 + R + 1 + 2

Ptenoglossan radula: rows with no central tooth but a series of several uniform, pointed marginal teeth (typical example : Epitonioidea).
Formula : n + 0 + n

Stenoglossan or rachiglossan radula: each row has one central tooth and one lateral tooth on each side (or no lateral teeth in some cases) (most Neogastropoda).
Formula : 1 + R + 1
Or : 0 + R + 0

Toxoglossan radula: The middle teeth are very small or completely absent. Each row has only two teeth of which only one is in use at a time. These grooved teeth are very long and pointed, with venom channels (neurotoxins) and barbs, and are not firmly fixed to the basal plate. The teeth can therefore be individually transferred to the proboscis and ejected like a harpoon into the prey (typical example : Conoidea).
formula : 1 + 0 + 1

These radula types show the evolution in the gastropods from herbivorous to carnivorous feeding patterns. Scraping algae requires many teeth, as is found in the first three types.
Carnivorous gastropods generally need fewer teeth, especially laterals and marginals. The ptenoglossan radula is situated between the two extremes and is typical for those gastropods which are adapted to a life as parasites on polyps.


=== Gastropods with no radula ===
The streptaxid Careoradula perelegans is the only known terrestrial gastropod which has no radula.
Some marine gastropods lack a radula. For example, all species of sea slugs in the family Tethydidae have no radula, and a clade of dorids (the Porostomata) as well as all species of the genus Clathromangelia (family Clathurellidae) likewise lack the organ. The radula has been lost a number of times in the Opisthobrancha.


== In cephalopods ==

Most cephalopods possess a radula as well as a horny chitinous beak, although it is reduced in octopuses and absent in Spirula.
The cephalopod radula rarely fossilizes: it has been found in around one in five ammonite genera, and is rarer still in non-ammonoid forms. Indeed, it is known from only three non-ammonoid taxa in the Palaeozoic era: Michelinoceras, Paleocadmus, and an unnamed species from the Soom Shale.


== In solenogasters ==
The solenogaster radula is akin to that of other molluscs, with regularly spaced rows of teeth produced at one end and shed at the other. The teeth within each row are similar in shape, and get larger in size towards the outer extreme. A number of teeth occur on each row; this number is usually constant but prone to small variations from row to row; indeed, it increases over time, with teeth being added to the middle of rows by addition or by the division of existing teeth. A number of radular formulae are exhibited by this class: 1:0:1 is most common, followed by 0:1:0 and n:0:n.


== In caudofoveates ==
The radula of the caudofoveate Falcidens is unlike the conchiferan radula. It has a reduced form, comprising just a single row of teeth. On each side of the apparatus, two teeth appear at the front; behind these, the third teeth fuse to form a mineralized axial plate. Bars occur posterior to this, behind which a sheath encircles the apparatus. The rear of the apparatus consists of a large plate, the 'radular cone'. The unusual form of the radula is accompanied by an unusual purpose: rather than rasping substrates, Falcidens uses its teeth as pincers to grasp prey items.


== See also ==
hypostome


== References ==


== Further reading ==
Molluscan buccal structures and radula
Barker, G. M (2001). The biology of terrestrial molluscs. ISBN 978-0-85199-318-8. 
A Comparison of the feeding behaviour and the functional morphology of radula structure in Nudibranchs
Hickman Carole, S (1980). "Gastropod Radulae and the Assessment of Form in Evolutionary Paleontology". Paleobiology 6 (3): 276294. doi:10.2307/2400346. 
Caron, J.-B.; Scheltema, A.; Schander, C.; Rudkin, D. (2006). "A soft-bodied mollusc with radula from the Middle Cambrian Burgess Shale" (PDF). Nature 442 (7099): 159163. Bibcode:2006Natur.442..159C. doi:10.1038/nature04894. PMID 16838013. 
Amlie H. Scheltema, Klaus Kerth, Alan M. Kuzirian (April 2006). "Original molluscan radula: Comparisons among Aplacophora, Polyplacophora, Gastropoda, and the Cambrian fossil Wiwaxia corrugata". Journal of Morphology 257 (2): 219245. doi:10.1002/jmor.10121. PMID 12833382. 
Katsuno, S.; Sasaki, T. (2008). "Comparative Histology of Radula-Supporting Structures in Gastropoda". Malacologia 50 (1-2): 1356. doi:10.4002/0076-2997-50.1.13. 
H. A. Lowenstam, Goethite in Radular Teeth of Recent Marine Gastropods; Science, New Series, Vol. 137, No. 3526, 27 July 1968, p. 279-280


== External links ==