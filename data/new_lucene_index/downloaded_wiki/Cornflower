Centaurea cyanus, commonly known as cornflower, bachelor's button, bluebottle, boutonniere flower, hurtsickle or cyani flower, is an annual flowering plant in the family Asteraceae, native to Europe. "Cornflower" is also used for chicory, and a few other Centaurea species; to distinguish C. cyanus from these it is sometimes called common cornflower. It may also be referred to as basketflower, though the term also refers to the Plectocephalus group of Centaurea, which is probably a distinct genus.
It is an annual plant growing to 16-35 inches tall, with grey-green branched stems. The leaves are lanceolate, 14 cm long. The flowers are most commonly an intense blue colour, produced in flowerheads (capitula) 1.53 cm diameter, with a ring of a few large, spreading ray florets surrounding a central cluster of disc florets. The blue pigment is protocyanin, which in roses is red.
In the past it often grew as a weed in cornfields (in the broad sense of the word "corn", referring to grains, such as wheat, barley, rye, or oats), hence its name. It is now endangered in its native habitat by agricultural intensification, particularly over-use of herbicides, destroying its habitat; in the United Kingdom it has declined from 264 sites to just 3 sites in the last 50 years. In reaction to this, the conservation charity Plantlife named it as one of 101 species it would actively work to bring 'Back from the Brink'. It is also, however, through introduction as an ornamental plant in gardens and a seed contaminant in crop seeds, now naturalised in many other parts of the world, including North America and parts of Australia.


== Cultivation ==

It is grown as an ornamental plant in gardens, where several cultivars have been selected with varying pastel colours, including pink and purple. Centaurea is also grown for the cutflower industry in Canada for use by florists. The most common colour variety for this use is a doubled blue variety such as 'Blue Boy' or 'Blue Diadem'. White, pink, lavender and black (actually a very dark maroon) are also used but less commonly. It is also occasionally used as a culinary ornament. Cornflowers have been used and prized historically for their blue pigment. Cornflowers are often used as an ingredient in some tea blends and herbal teas, and is famous in the Lady Grey blend of Twinings. A relative, Centaurea montana, is a perennial plant which is also cultivated as a garden plant. Cornflowers germinate quickly after planting.
Light requirements: full sun. Water requirements: high-average water daily. Soil pH requirements: neutral (6.6-7.5) to mildly alkaline (7.6-7.8).
It flowers all summer.
The cornflower is considered a beneficial weed, and its edible flower can be used to add colour to salads.


== Distribution ==
In Ireland, in Co.Clare (H9)Centaurea cynus is recorded in arable fields as very rare and almost extinct. while in the north-east of Ireland it was abundant before 1930s.


== Folklore and symbolism ==

In folklore, cornflowers were worn by young men in love; if the flower faded too quickly, it was taken as a sign that the man's love was not returned.
In herbalism, a decoction of cornflower is effective in treating conjunctivitis, and as a wash for tired eyes.
The blue cornflower has been the national flower of Estonia since 1968 and symbolizes daily bread to Estonians. It is also the symbol of the Estonian political party, People's Union, the Finnish political party, National Coalition Party, and the Swedish political party, Liberal People's Party, and has since the dawn of the 20th century been a symbol for social liberalism there. It is the official flower of the Swedish province of stergtland and the school flower of Winchester College and also of Dulwich College where it is said to have been the favourite flower of the founder, Edward Alleyn.
The blue cornflower was one of the national symbols of Germany. This is partly due to the story that when Queen Louise of Prussia was fleeing Berlin and pursued by Napoleon's forces, she hid her children in a field of cornflowers and kept them quiet by weaving wreaths for them from the flowers. The flower thus became identified with Prussia, not least because it was the same color as the Prussian military uniform. After the unification of Germany in 1871, it went on to became a symbol of the country as a whole. For this reason, in Austria the blue cornflower is a political symbol for pan-German and rightist ideas. Members of the Freedom Party wore it at the opening of the Austrian parliament in 2006.
It was also the favourite flower of Louise's son Kaiser Wilhelm I. Because of its ties to royalty, authors such as Theodor Fontane have used it symbolically, often sarcastically, to comment on the social and political climate of the time.
The cornflower is also often seen as an inspiration for the German Romantic symbol of the Blue Flower.
Due to its traditional association with Germany, the cornflower has been made the official symbol of the annual German-American Steuben Parade.
In France the Bleuet de France is the symbol of the 11th November 1918 armistice and, as such, a common symbol for veterans (especially the now defunct poilus of World War I), similar to the Remembrance poppies worn in the United Kingdom and in Canada.
The cornflower is also the symbol for motor neurone disease and amyotrophic lateral sclerosis.
Cornflowers are sometimes worn by Old Harrovians.
A Cornflower design (Blue Cornflower) was used by Corning Glass Works for the initial release of Corning Ware Pyroceram Cookware. Its popularity in the United States, Canada, United Kingdom and Australia was so high that it became the symbol of Corning Glass Works. The production of Blue Cornflower Corning Ware (known as Pyrosil Ware & Pyroflam in Europe) spanned over 30 years, with initial production from 1958 to 1988 and a second short run from 1993-1994 marking the 35th anniversary of their flagship consumer product. Corning Ware is no longer produced by Corning Glass Works, as the Consumer Products Division was sold to Borden, Inc./World Kitchen LLC. in 1998. Production of Pyroceram was discontinued in the United States and Canada by World Kitchen in 2000 in lieu of stoneware products, however, the popularity of the Blue Cornflower Corning Ware pattern (as well as a growing market for Pyroceram cookware) pushed World Kitchen, LLC. into a re-introduce Pyroceram Cookware imported from France in 2009 with the familiar Cornflower pattern.


== Trivia ==
It was the favorite flower of John F. Kennedy and was worn by his son, John F. Kennedy, Jr. at his wedding in tribute to his father.
Cornflowers were also used in the funeral wreath made for Pharaoh Tutankhamun.
It could be occasionally found on the banks of the river Main in Frankfurt.


== In paintings ==


== See also ==
Cornflower blue


== References ==


== External links ==
Flora Europaea: Centaurea cyanus
UK Biodiversity Action Plan: Centaurea cyanus
Briefing sheet on Centaurea cyanus from page [2]