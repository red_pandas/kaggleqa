A barrel, cask, or tun is a hollow cylindrical container, traditionally made of wooden staves bound by wooden or metal hoops. Traditionally, the barrel was a standard size of measure referring to a set capacity or weight of a given commodity. For example, in the UK a barrel of beer refers to a quantity of 36 imperial gallons (160 L; 43 US gal). Wine was shipped in barrels of 119 litres (31 US gal; 26 imp gal).
Modern wooden barrels for wine-making are either made of French common oak (Quercus robur) and white oak (Quercus petraea) or from American white oak (Quercus alba) and have typically these standard sizes: "Bordeaux type" 225 litres (59 US gal; 49 imp gal), "Burgundy type" 228 litres (60 US gal; 50 imp gal) and "Cognac type" 300 litres (79 US gal; 66 imp gal). Modern barrels and casks can also be made of aluminum, stainless steel, and different types of plastic, such as HDPE.
Someone who makes barrels is called a "barrel maker" or cooper. Barrels are only one type of cooperage. Other types include, but are not limited to: buckets, tubs, butter churns, hogsheads, firkins, kegs, kilderkins, tierces, rundlets, puncheons, pipes, tuns, butts, pins, and breakers.
Barrels have a variety of uses, including storage of liquids such as water and oil, fermenting wine, arrack, and sake, and maturing beverages such as wine, cognac, armagnac, sherry, port, whiskey, and beer.


== Uses today ==


=== Beverage maturing ===

An "aging barrel" is used to age wine; distilled spirits such as whiskey, brandy, or rum; beer; tabasco sauce; or (in smaller sizes) traditional balsamic vinegar. When a wine or spirit ages in a barrel, small amounts of oxygen are introduced as the barrel lets some air in (compare to microoxygenation where oxygen is deliberately added). Oxygen enters a barrel when water or alcohol is lost due to evaporation, a portion known as the "angels' share". In an environment with 100% relative humidity, very little water evaporates and so most of the loss is alcohol, a useful trick if one has a wine with very high proof. Most beverages are topped up from other barrels to prevent significant oxidation, although others such as vin jaune are not.
Beverages aged in wooden barrels take on some of the compounds in the barrel, such as vanillin and wood tannins. The presence of these compounds depends on many factors, including the place of origin, how the staves were cut and dried, and the degree of "toast" applied during manufacture. After roughly three years, most of a barrel's flavor compounds have been leached out and it is well on its way to becoming "neutral". Barrels used for aging are typically made of French or American oak, but chestnut and redwood are also used. Some Asian beverages (e.g., Japanese sake) use Japanese cedar, which imparts an unusual, minty-piney flavor. In Peru and Chile, a grape distillate named pisco is either aged in oak or in earthenware.


==== Wine ====
Some wines are fermented "on barrel", as opposed to in a neutral container such as a steel or wine-grade HDPE (high density polyethylene). Wine can also be fermented in large wooden tanks, whichwhen open to the atmosphereare called "open-tops". Other wooden cooperage for storing wine or spirits range from smaller barriques to huge casks, with either elliptical or round heads.
The tastes yielded by French and American species of oak are slightly different, with French oak being subtler, while American oak gives stronger aromas. To retain the desired measure of oak influence, a winery will replace a certain percentage of its barrels every year, although this can vary from 5 to 100%. Some winemakers use "200% new oak", where the wine is put into new oak barrels twice during the aging process. Bulk wines are sometimes flavored by soaking oak chips in them instead of being aged in a barrel.


==== Whiskey ====
NOTE: This section includes both the U.S. spelling, whiskey, and the U.K.  Canadian spelling, whisky.
Laws in several jurisdictions require that whiskey be aged in wooden barrels. The law in the United States requires that "straight whiskey" (with the exception of corn whiskey) must be stored for at least two years in new, charred oak containers. Other forms of whiskey aged in used barrels cannot be called "straight".
International laws require any whisky bearing the label "Scotch" to be distilled in Scotland and matured for a minimum of three years and one day in oak casks.
By Canadian law, Canadian whiskies must "be aged in small wood for not less than three years", and "small wood" is defined as a wood barrel not exceeding 700 L capacity.
Since the U.S. law requires the use of new barrels, which is not typically considered necessary elsewhere, whisky made elsewhere is usually aged in used barrels that previously contained U.S. whiskey (usually Bourbon whiskey). The typical U.S. Bourbon barrel is 53 US gallons (200 L) in size, which is thus the de facto standard whisky barrel size worldwide. Some distillers transfer their whiskey into different barrels to "finish" or add qualities to the final product. These finishing barrels frequently aged a different spirit (such as rum) or wine. Other distillers, particularly those producing Scotch, often disassemble 5 used bourbon barrels and reassemble them into 4 casks with different barrel ends for aging Scotch, creating a type of cask referred to as a Hogshead.


==== Sherry ====
Sherry is stored in 600-litre casks made of North American oak, which is slightly more porous than French or Spanish oak. The casks, or butts, are filled five-sixths full, leaving "the space of two fists" empty at the top to allow flor to develop on top of the wine.


==== Brandy ====
Maturing is very important for a good brandy, which is typically aged in oak casks. The wood used for those barrels is selected because of its ability to transfer certain aromas to the spirit. Cognac is aged only in oak casks made from wood from the Tronais and more often from the Limousin forests.


==== Balsamic vinegar ====
Traditional balsamic vinegar is aged in a series of wooden barrels.


==== Tabasco sauce ====
Since its invention in 1868, the pepper mash used to make tabasco sauce is aged for three years in previously used oak whiskey barrels. 


==== Beer ====
Beers are sometimes aged in barrels which were previously used for maturing wines or spirits. Cask ale is aged in the barrel (usually steel) for a short time before serving. Extensive barrel aging is required of many sour beers.


==== Angels' share ====
"Angels' share" is a term for the portion (share) of a wine or distilled spirit's volume that is lost to evaporation during aging in oak barrels. In low humidity conditions, the loss to evaporation may be primarily water. However, in higher humidities, more alcohol than water will evaporate, therefore reducing the alcoholic strength of the product. In humid climates, this loss of ethanol is associated with the growth of a darkly colored fungus, the angels' share fungus, Baudoinia compniacensis, on the exterior surfaces of buildings, trees, and other vegetation, and almost anything else that happens to be nearby. 


=== Water storage ===
Water barrels are often used to collect the rainwater from dwellings (so that it may be used for irrigation or other purposes). This usage, known as rainwater harvesting, requires (besides a large rainwater barrel or water butt) adequate (waterproof) roof-covering and an adequate rain pipe.


=== Oil storage ===

The standard barrel of crude oil or other petroleum product (abbreviated bbl) is 42 US gallons (34.972315754 imp gal; 158.987294928 L). This measurement originated in the early Pennsylvania oil fields, and permitted both British and American merchants to refer to the same unit, based on the old English wine measure, the tierce.
Earlier, another size of whiskey barrel was the most common size; this was the 40 US gallons (33.3 imp gal; 151.4 L) barrel for proof spirits, which was of the same volume as 5 US bushels. However, by 1866, the oil barrel was standardized at 42 US gallons.
Oil has not been shipped in barrels since the introduction of oil tankers, but the 42-US-gallon size is still used as a unit for measurement, pricing, and in tax and regulatory codes. Each barrel is refined into about 19.74 US gallons (16.44 imp gal; 74.7 L) of gasoline, the rest becoming other products such as jet fuel and heating oil, using fractional distillation.


== Shape and construction ==

Barrels often have a convex shape, bulging at the middle. This bulge facilitates rolling a well-built wooden barrel on its side and change directions with little friction, compared to a cylinder. It also helps to distribute stress evenly in the material by making the container more curved. Barrels have reinforced edges to enable safe displacement by rolling them at an angle (in addition of rolling on their sides as described).

Casks used for ale or beer have shives and keystones in their openings. Before serving the beer, a spile is hammered into the shive and a tap into the keystone.
The rings holding a wooden barrel together, called hoops, are generally made of galvanized iron, though historically were made from flexible bits of wood called withies. While wooden hoops could require barrels to be "fully hooped", with hoops stacked tightly together along the entire top and bottom third of a barrel, iron-hooped barrels only require a few hoops on each end.
The "head hoop" or "chime hoop" is the hoop nearest the extremes of a barrel, the chime being the beveled edge and the head being the flat circular top or bottom of the barrel. The "bilge hoops" are those nearest the bilge, or bulging center, while the "quarter hoop" is located between the chime and bilge hoops.
The stopper used to seal the hole in a barrel is called the bung and mostly made of silicone.


== Sizes ==

A barrel is one of several units of volume, with dry barrels, fluid barrels (UK beer barrel, US beer barrel), oil barrel, etc. The volume of some barrel units is double others, with various volumes in the range of about 100200 litres (2244 imp gal; 2653 US gal).


=== English wine casks ===
Pre-1824 definitions continued to be used in the US, the wine gallon of 231 cubic inches being the standard gallon for liquids (the corn gallon of 268.8 cubic inches for solids). In Britain, the wine gallon was replaced by the imperial gallon. The tierce later became the petrol barrel. The tun was originally 256 gallons, which explains from where the quarter, 8 bushels or 64 (wine) gallons, comes.


=== Brewery casks ===
Although it is common to refer to draught beer containers of any size as barrels, in the UK this is strictly correct only if the container holds 36 imperial gallons. The terms "keg" and "cask" refer to containers of any size, the distinction being that kegs are used for beers intended to be served using external gas cylinders. Cask ales undergo part of their fermentation process in their containers, called casks.
Casks are available in several sizes, and it is common to refer to "a firkin" or "a kil" (kilderkin) instead of a cask.
The modern US beer barrel is 31 US gallons (117.34777 L), half a gallon less than the traditional wine barrel. (26 U.S.C. 5051)


=== Dry goods ===
Barrels are also used as a unit of measurement for dry goods (dry groceries), such as flour or produce. Traditionally, a barrel is 196 pounds (89 kg) of flour (wheat or rye), with other substances such as pork subject to more local variation. In modern times, produce barrels for all dry goods, excepting cranberries, contain 7,056 cubic inches, about 115.627 L.


== See also ==


== References ==


== External links ==
Origin of "over a barrel"
 Chisholm, Hugh, ed. (1911). "Barrel". Encyclopdia Britannica (11th ed.). Cambridge University Press. 
Barrel Basics