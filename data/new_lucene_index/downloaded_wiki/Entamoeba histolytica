Entamoeba histolytica is an anaerobic parasitic protozoan, part of the genus Entamoeba. Predominantly infecting humans and other primates, E. histolytica is estimated to infect about 50 million people worldwide. Previously, it was thought that 10% of the world population was infected, but these figures predate the recognition that at least 90% of these infections were due to a second species, E. dispar. Mammals such as dogs and cats can become infected transiently, but are not thought to contribute significantly to transmission.
The word histolytic literally means "tissue destroyer".


== TransmissionEdit ==
The active (trophozoite) stage exists only in the host and in fresh loose feces; cysts survive outside the host in water, in soils, and on foods, especially under moist conditions on the latter. The cysts are readily killed by heat and by freezing temperatures, and survive for only a few months outside of the host. When cysts are swallowed they cause infections by excysting (releasing the trophozoite stage) in the digestive tract. The pathogenic nature of E. histolytica was first reported by Lsch in 1875, but it was not given its Latin name until Fritz Schaudinn described it in 1903. E. histolytica, as its name suggests (histolytic = tissue destroying), is pathogenic; infection can be asymptomatic or can lead to amoebic dysentery or amoebic liver abscess. Symptoms can include fulminating dysentery, bloody diarrhea, weight loss, fatigue, abdominal pain, and amoeboma. The amoeba can actually 'bore' into the intestinal wall, causing lesions and intestinal symptoms, and it may reach the blood stream. From there, it can reach different vital organs of the human body, usually the liver, but sometimes the lungs, brain, spleen, etc. A common outcome of this invasion of tissues is a liver abscess, which can be fatal if untreated. Ingested red blood cells are sometimes seen in the amoeba cell cytoplasm.


== GenomeEdit ==
The E. histolytica genome was sequenced, assembled, and automatically annotated in 2005. The genome was reassembled and reannotated in 2010. The 20 million basepair genome assembly contains 8,160 predicted genes; known and novel transposable elements have been mapped and characterized, functional assignments have been revised and updated, and additional information has been incorporated, including metabolic pathways, Gene Ontology assignments, curation of transporters, and generation of gene families. The major group of transposable elements in E. histolytica are non-LTR retrotransposons. These have been divided in three families called EhLINEs and EhSINEs (EhLINE1,2,3 and EhSINE1,2,3). EhLINE1 encode an endonuclease (EN) protein (in addition to reverse transcriptase and nucleotide-binding ORF1), which have similarity with bacterial restriction endonuclease. This similarity with bacterial protein indicates that transposable elements have been acquired from prokaryotes by horizontal gene transfer in this protozoan parasite.


== PathologyEdit ==
In the vast majority of cases, infection is asymptomatic and the carrier is unaware they are infected. However, in an estimated 10% of cases E. histolytica causes disease. Once the trophozoites are excysted they colonize the large bowel, remaining on the surface of the mucus layer and feeding on bacteria and food particles. Occasionally, and in response to unknown stimuli, trophozoites move through the mucus layer where they come in contact with the epithelial cell layer and start the pathological process. E. histolytica has a lectin that binds to galactose and N-acetylgalactosamine sugars on the surface of the epithelial cells, The lectin normally is used to bind bacteria for ingestion. The parasite has several enzymes such as pore forming proteins, lipases, and cysteine proteases, which are normally used to digest bacteria in food vacuoles but which can cause lysis of the epithelial cells by inducing cellular necrosis and apoptosis when the trophozoite comes in contact with them and binds via the lectin. The trophozoites will then ingest these dead cells. This damage to the epithelial cell layer attracts human immune cells and these in turn can be lysed by the trophozoite, which releases the immune cell's own lytic enzymes into the surrounding tissue, creating a type of chain reaction and leading to tissue destruction. This destruction manifests itself in the form of an 'ulcer' in the tissue, typically described as flask shaped because of its appearance in transverse section. this tissue destruction can also involve blood vessels leading to bloody diarrhea, amebic dysentery. Occasionally, trophozoites enter the bloodstream where they are transported typically to the liver via the portal system. In the liver a similar pathological sequence ensues, leading to amebic liver abscesses. The trophozoites can end up in other organs also, sometimes via the bloodstream, sometimes via liver abscess rupture or fistulas. In all locations similar pathology can occur.


== Pathogen interactionEdit ==
E. histolytica may modulate the virulence of certain human viruses and is itself a host for its own viruses.
For example, AIDS accentuates the damage and pathogenicity of E. histolytica. On the other hand, cells infected with HIV are often consumed by E. histolytica. Infective HIV remains viable within the amoeba, although fortunately there has been no proof of human reinfection from amoeba carrying this virus.
A burst of research on viruses of E. histolytica stems from a series of papers published by Diamond et al. from 1972 to 1979. In 1972, they hypothesized two separate polyhedral and filamentous viral strains within E. histolytica that caused cell lysis. Perhaps the most novel observation was that two kinds of viral strains existed, and that within one type of amoeba (strain HB-301) the polyhedral strain had no detrimental effect but led to cell lysis in another (strain HK-9). Although Mattern et al. attempted to explore the possibility that these protozoal viruses could function like bacteriophages, they found no significant changes in Entamoeba histolytica virulence when infected by viruses. However, no newer published research has been conducted on this species since.


== DiagnosisEdit ==
It can be diagnosed by stool samples, but it is important to note that certain other species are impossible to distinguish by microscopy alone. Trophozoites may be seen in a fresh fecal smear and cysts in an ordinary stool sample. ELISA or RIA can also be used.


== TreatmentEdit ==
There are many kinds of effective drugs. This is just a short overview of a few of the different methods of treatments.
Intestinal infection: Usually nitroimidazole derivatives are used because they are highly effective against the trophozoite form of the amoeba. Since they have little effect on amoeba cysts, usually this treatment is followed by an agent (such as paromomycin or diloxanide furoate) that acts on the organism in the lumen.
Liver abscess: In addition to targeting organisms in solid tissue, primarily with drugs like metronidazole and chloroquine, treatment of liver abscess must include agents that act in the lumen of the intestine (as in the preceding paragraph) to avoid re-invasion. Surgical drainage is usually not necessary except when rupture is imminent.
Asymptomatic patients: For asymptomatic patients (otherwise known as carriers, with no symptoms), non endemic areas should be treated by paromomycin, and other treatments include diloxanide furoate and iodoquinol. There have been problems with the use of iodoquinol and iodochlorhydroxyquin, so their use is not recommended. Diloxanide furoate can also be used by mildly symptomatic persons who are just passing cysts.


== MeiosisEdit ==
In sexually reproducing eukaryotes, homologous recombination (HR) ordinarily occurs during meiosis. The meiosis-specific recombinase, Dmc1, is required for efficient meiotic HR, and Dmc1 is expressed in E. histolytica. The purified Dmc1 from E. histolytica forms presynaptic filaments and catalyzes ATP-dependent homologous DNA pairing and DNA strand exchange over at least several thousand base pairs. The DNA pairing and strand exchange reactions are enhanced by the eukaryotic meiosis-specific recombination accessory factor (heterodimer) Hop2-Mnd1. These processes are central to meiotic recombination, suggesting that E. histolytica undergoes meiosis.
Several other genes involved in both mitotic and meiotic HR are also present in E. histolytica. HR is enhanced under stressful growth conditions (serum starvation) concomitant with the up-regulation of HR-related genes. Also, UV irradiation induces DNA damage in E. histolytica trophozoites and activates the recombinational DNA repair pathway. In particular, expression of the Rad51 recombinase is increased about 15-fold by UV treatment.


== See alsoEdit ==
List of parasites (human)


== ReferencesEdit ==


== External linksEdit ==
Entamoeba histolytica image library
Entamoeba histolytica - Centers for Disease Control and Prevention
CDC DPDx Parasitology Diagnostic Web Site
LSHTM Entamoeba Homepage
Entamoeba Genome Resource - AmoebaDB
Entamoeba histolytica article from the US Food and Drug Administration's Bad Bug Book