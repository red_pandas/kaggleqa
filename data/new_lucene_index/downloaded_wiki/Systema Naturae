Systema Naturae (sometimes written Systema Natur with the ligature ) was one of the major works of the Swedish botanist, zoologist and physician Carolus Linnaeus (17071778) and introduced the Linnaean taxonomy. Although the system, now known as binomial nomenclature, was partially developed by the Bauhin brothers, Gaspard and Johann, 200 years earlier, Linnaeus was first to use it consistently throughout his book. The first edition was published in 1735. The full title of the 10th edition (1758), which was the most important one, was Systema natur per regna tria natur, secundum classes, ordines, genera, species, cum characteribus, differentiis, synonymis, locis or translated: "System of nature through the three kingdoms of nature, according to classes, orders, genera and species, with characters, differences, synonyms, places".
The tenth edition of this book (1758) is considered the starting point of zoological nomenclature. In 17661768 Linnaeus published the much enhanced 12th edition, the last under his authorship. Another again enhanced work in the same style and entitled "Systema Naturae" was published by Johann Friedrich Gmelin between 1788 and 1793. Since at least the early 1900s zoologists commonly recognized this as the last edition belonging to this series. It was also officially regarded by the International Commission on Zoological Nomenclature in Opinion 296 (26 Oct 1954) as the 13th edition of Systema Naturae.


== OverviewEdit ==
Linnaeus (later known as "Carl von Linn", after his ennoblement in 1761) published the first edition of Systema Naturae in the year 1735, during his stay in the Netherlands. As was customary for the scientific literature of its day, the book was published in Latin. In it, he outlined his ideas for the hierarchical classification of the natural world, dividing it into the animal kingdom (regnum animale), the plant kingdom (regnum vegetabile), and the "mineral kingdom" (regnum lapideum).
Linnaeus's Systema Naturae lists only about 10,000 species of organisms, of which about 6,000 are plants and 4,236 are animals. According to the historian of botany William T. Stearn, "Even in 1753 he believed that the number of species of plants in the whole world would hardly reach 10,000; in his whole career he named about 7,700 species of flowering plants."
Linnaeus did not suppose that his classification of the plant kingdom in the book was natural, reflecting the logic of God's creation. His sexual system, where species with the same number of stamens were treated in the same group, was convenient but in his view artificial. Linnaeus believed in God's creation, and that there were no deeper relationships to be expressed. He is frequently quoted to have said. "God created, Linnaeus organized". The classification of animals was more natural. For instance, humans were for the first time placed together with other primates, as Anthropomorpha.
In view of the popularity of the work, Linnaeus kept publishing new and ever-expanding editions, growing from eleven very large pages in the first edition (1735) to 2,400 pages in the 12th edition (17661768). Also, as the work progressed, he made changes: in the first edition, whales were classified as fishes, following the work of Linnaeus' friend and "father of ichthyology" Peter Artedi; in the 10th edition, published in 1758, whales were moved into the mammal class. In this same edition, he introduced two part names (see binomen) for animal species, something he had done for plant species (see binary name) in the 1753 publication of Species Plantarum. The system eventually developed into modern Linnaean taxonomy, a hierarchically organized biological classification.
After the decline in Linnaeus' health in the early 1770s, publication of editions of Systema Naturae went in two different directions. Another Swedish scientist, Johan Andreas Murray issued the Regnum Vegetabile section separately in 1774 as the Systema Vegetabilium, rather confusingly labelled the 13th edition. Meanwhile, a 13th edition of the entire Systema appeared in parts between 1788-1793. It was as the Systema Vegetabilium, that Linnaeus' work became widely known in England following translation from the Latin by the Lichfield Botanical Society, as A System of Vegetables (17831785).


== TaxonomyEdit ==
In his Imperium Natur, Linnaeus established three kingdoms, namely Regnum Animale, Regnum Vegetabile and Regnum Lapideum. This approach, the Animal, Vegetable and Mineral Kingdoms, survives until today in the popular mind, notably in the form of parlour games: "Is it animal, vegetable or mineral?". The classification was based on five levels: kingdom, class, order, genus, and species. While species and genus was seen as God-given (or "natural"), the three higher levels were seen by Linnaeus as constructs. The concept behind the set ranks being applied to all groups was to make a system that was easy to remember and navigate, a task in which he must be said to have succeeded.

Linnaeus's work had a huge impact on science; it was indispensable as a foundation for biological nomenclature, now regulated by the Nomenclature Codes. Two of his works, the first edition of the Species Plantarum (1753) for plants and the 10th edition of the Systema Natur (1758), are accepted among the starting points of nomenclature. Most of his names for species and genera were published at very early dates, thus take priority over those of other, later works. In zoology there is one exception, which is a monograph on Swedish spiders, Svenska Spindlar, published by Carl Clerck in 1757, so the names established there take priority over the Linnean names. However, his impact on science was not because of the value of his taxonomy. His talent for attracting skillful young students and sending them abroad to collect made his work far more influential than that of his contemporaries. At the close of the 18th century, his system had effectively become the standard system for biological classification.


=== The Animal KingdomEdit ===
Only in the Animal Kingdom is the higher taxonomy of Linnaeus still more or less recognizable and some of these names are still in use, but usually not quite for the same groups as used by Linnaeus. He divided the Animal Kingdom into six classes; in the tenth edition (1758), these were:
Mammalia comprised the mammals. In the first edition, whales and the West Indian manatee were classified among the fishes.
Aves comprised the birds. Linnaeus was the first to remove bats from the birds and classify them under mammals.
Amphibia comprised amphibians, reptiles, and assorted fishes that are not of Osteichthyes.
Pisces comprised the bony fishes. These included the spiny-finned fishes (Perciformes) as a separate order.
Insecta comprised all arthropods. Crustaceans, arachnids and myriapods were included as the order "Aptera".
Vermes comprised the remaining invertebrates, roughly divided into "worms", molluscs, and hard-shelled organisms like echinoderms.


=== The Plant KingdomEdit ===
His orders and classes of plants, according to his Systema Sexuale, were never intended to represent natural groups (as opposed to his ordines naturales in his Philosophia Botanica) but only for use in identification. They were used in that sense well into the 19th century.

The Linnaean classes for plants, in the Sexual System, were:


=== The Mineral KingdomsEdit ===
Linnaeus's taxonomy of minerals has long since fallen out of use. In the 10th edition, 1758, of the Systema Natur, the Linnaean classes were:
Classis 1. Petr (rocks)
Classis 2. Miner (minerals and ores)
Classis 3. Fossilia (fossils and aggregates)


== EditionsEdit ==
The Gmelin thirteenth (decima tertia) edition of 1788-1793, is likely to be confused with another thirteenth edition prepared by Johan Andreas Murray in 1774, and then subsequently in further editions, all under a revised name of Systema Vegetabilium.
The dates of publication for Gmelin's edition were the following:
Part 1: pp. [112], 1500 (25 July 1788)
Part 2: pp. 5011032 (20 April 1789)
Part 3: pp. 10331516 (20 November 1789)
Part 4: pp. 15172224 (21 May 1790)
Part 5: pp. 22253020 (6 December 1790)
Part 6: pp. 30213910 (14 May 1791)
Part 7: pp. 39114120 (2 July 1792)


== See alsoEdit ==
10th edition of Systema Naturae
12th edition of Systema Naturae
Supplementum Plantarum
Animalia Paradoxa


== ReferencesEdit ==


== BibliographyEdit ==


== External linksEdit ==
Linn online