A pipe is a tubular section or hollow cylinder, usually but not necessarily of circular cross-section, used mainly to convey substances which can flow  liquids and gases (fluids), slurries, powders and masses of small solids. It can also be used for structural applications; hollow pipe is far stiffer per unit weight than solid members.
In common usage the words pipe and tube are usually interchangeable, but in industry and engineering, the terms are uniquely defined. Depending on the applicable standard to which it is manufactured, pipe is generally specified by a nominal diameter with a constant outside diameter (OD) and a schedule that defines the thickness. Tube is most often specified by the OD and wall thickness, but may be specified by any two of OD, inside diameter (ID), and wall thickness. Pipe is generally manufactured to one of several international and national industrial standards. While similar standards exist for specific industry application tubing, tube is often made to custom sizes and a broader range of diameters and tolerances. Many industrial and government standards exist for the production of pipe and tubing. The term "tube" is also commonly applied to non-cylindrical sections, i.e., square or rectangular tubing. In general, "pipe" is the more common term in most of the world, whereas "tube" is more widely used in the United States.
Both "pipe" and "tube" imply a level of rigidity and permanence, whereas a hose (or hosepipe) is usually portable and flexible. Pipe assemblies are almost always constructed with the use of fittings such as elbows, tees, and so on, while tube may be formed or bent into custom configurations. For materials that are inflexible, cannot be formed, or where construction is governed by codes or standards, tube assemblies are also constructed with the use of tube fittings.


== Uses ==

Plumbing
Tap water
Pipelines transporting gas or liquid over long distances
Scaffolding
Structural steel
As components in mechanical systems such as:
Rollers in conveyor belts
Compactors (e.g.: steam rollers)
Bearing casing

Casing for concrete pilings used in construction projects
High temperature or pressure manufacturing processes
The petroleum industry:
Oil well casing
Oil refinery equipment

Delivery of fluids, either gaseous or liquid, in a process plant from one point to another point in the process
Delivery of bulk solids, in a food or process plant from one point to another point in the process
The construction of high pressure storage vessels (note that large pressure vessels are constructed from plate, not pipe owing to their wall thickness and size).


== Manufacture ==

There are three processes for metallic pipe manufacture. Centrifugal casting of hot alloyed metal is one of the most prominent process. Ductile iron pipes are generally manufactured in such a fashion. Seamless (SMLS) pipe is formed by drawing a solid billet over a piercing rod to create the hollow shell. As the manufacturing process does not include any welding, seamless pipes are perceived to be stronger and more reliable. Historically seamless pipe was regarded as withstanding pressure better than other types, and was often more easily available than welded pipe.
Advances since the 1970s in materials, process control and non-destructive testing allow correctly specified welded pipe to replace seamless in many applications. Welded (also Electric Resistance Welded ("ERW"), and Electric Fusion Welded ("EFW")) pipe is formed by rolling plate and welding the seam. The weld flash can be removed from the outside or inside surfaces using a scarfing blade. The weld zone can also be heat treated to make the seam less visible. Welded pipe often has tighter dimensional tolerances than seamless, and can be cheaper if manufactured in the same quantities.
There are a number of processes that may be used to produce ERW pipes. Each of these processes leads to coalescence or merging of steel components into pipes. Electric current is passed through the surfaces that have to be welded together; as the components being welded together resist the electric current, heat is generated which forms the weld. Pools of molten metal are formed where the two surfaces are being connected as strong electric current is passed through the metal; these pools of molten metal form the weld that binds the two connected components.
ERW pipes are manufactured from the longitudinal welding of steel. The welding process for ERW pipes is continuous as opposed to welding of distinct sections at intervals. ERW process uses steel coil as feedstock.
The High Frequency Induction Technology (HFI) welding process is used for manufacturing ERW pipes. In this process the current to weld the pipe is applied by means of an induction coil around the tube. HFI is generally considered to be technically superior to ordinary ERW when manufacturing pipes for critical applications, such as for usage in the energy sector In addition to other uses in line pipe applications, as well as for casing and tubing.
Large-diameter pipe (25 centimetres (10 in) or greater) may be ERW, EFW or Submerged Arc Welded ("SAW") pipe. There are two technologies that can be used to manufacture steel pipes of sizes larger than the steel pipes that can be produced by seamless and ERW processes. The two types of pipes produced through these technologies are Longitudinal submerged arc welded (LSAW) and Spiral submerged arc welded (SSAW) pipes. LSAW are made by bending and welding wide steel plates and most commonly used in oil and gas industry applications. Due to their high cost, LSAW pipes are seldom used in lower value non-energy applications such as water pipelines. SSAW pipes are produced by spiral (helicoidal) welding of steel coil and have a cost advantage over LSAW pipes as the process uses coils rather than steel plates. As such, in applications where spiral-weld is acceptable, SSAW pipes may be preferred over LSAW pipes. Both LSAW pipes and SSAW pipes compete against ERW pipes and seamless pipes in the diameter ranges of 16-24.
Tubing for flow, either metal or plastic, is generally extruded.


== Materials ==

Pipe is made in many materials including ceramic, glass, fiberglass, many metals, concrete and plastic. In the past, wood and lead (Latin plumbum, from which comes the word 'plumbing') were commonly used.
Typically metallic piping is made of steel or iron, such as unfinished, black (lacquer) steel, carbon steel, stainless steel or galvanized steel, brass, and ductile iron. Iron based piping is subject to corrosion in highly oxygenated water stream. Aluminum pipe or tubing may be utilized where iron is incompatible with the service fluid or where weight is a concern; aluminum is also used for heat transfer tubing such as in refrigerant systems. Copper tubing is popular for domestic water (potable) plumbing systems; copper may be used where heat transfer is desirable (i.e. radiators or heat exchangers). Inconel, chrome moly, and titanium steel alloys are used in high temperature and pressure piping in process and power facilities. When specifying alloys for new processes, the known issues of creep and sensitization effect must be taken into account.
Lead piping is still found in old domestic and other water distribution systems, but it is no longer permitted for new potable water piping installations due to its toxicity. Many building codes now require that lead piping in residential or institutional installations be replaced with non-toxic piping or that the tubes' interiors be treated with phosphoric acid. According to a senior researcher and lead expert with the Canadian Environmental Law Association, "...there is no safe level of lead [for human exposure]". In 1991 the US EPA issued the Lead and Copper Rule, it is a federal regulation which limits the concentration of lead and copper allowed in public drinking water, as well as the permissible amount of pipe corrosion occurring due to the water itself.
Plastic tubing is widely used for its light weight, chemical resistance, non-corrosive properties, and ease of making connections. Plastic materials include polyvinyl chloride (PVC), chlorinated polyvinyl chloride (CPVC), fibre reinforced plastic (FRP), reinforced polymer mortar (RPMP), polypropylene (PP), polyethylene (PE), cross-linked high-density polyethylene (PEX), polybutylene (PB), and acrylonitrile butadiene styrene (ABS), for example. In many countries, PVC pipes account for most pipe materials used in buried municipal applications for drinking water distribution and wastewater mains. Market researchers are forecasting total global revenues of more than US$80 billion in 2019.  In Europe, market value will amount to approx. 12.7 billion in 2020 
Pipe may be made from concrete or ceramic, usually for low-pressure applications such as gravity flow or drainage. Pipes for sewage are still predominantly made from concrete or vitrified clay. Reinforced concrete can be used for large-diameter concrete pipes. This pipe material can be used in many types of construction, and is often used in the gravity-flow transport of storm water. Usually such pipe will have a receiving bell or a stepped fitting, with various sealing methods applied at installation.


=== Traceability and positive material identification (PMI) ===
When the alloys for piping are forged, metallurgical tests are performed to determine material composition by % of each chemical element in the piping, and the results are recorded in a Material Test Report (MTR). These tests can be used to prove that the alloy conforms to various specifications (e.g. 316 SS). The tests are stamped by the mill's QA/QC department and can be used to trace the material back to the mill by future users, such as piping and fitting manufacturers. Maintaining the traceability between the alloy material and associated MTR is an important quality assurance issue. QA often requires the heat number to be written on the pipe. Precautions must also be taken to prevent the introduction of counterfeit materials. As a backup to etching/labeling of the material identification on the pipe, positive material identification (PMI) is performed using a handheld device; the device scans the pipe material using an emitted electromagnetic wave (x-ray fluorescence/XRF) and receives a reply that is spectrographically analyzed.


== Sizes ==

Pipe sizes can be confusing because the terminology may relate to historical dimensions. For example, a half-inch iron pipe does not have any dimension that is a half inch. Initially, a half inch pipe did have an inner diameter of 0.5 inches (13 mm)but it also had thick walls. As technology improved, thinner walls became possible, but the outside diameter stayed the same so it could mate with existing older pipe, increasing the inner diameter beyond half an inch. The history of copper pipe is similar. In the 1930s, the pipe was designated by its internal diameter and a 116-inch (1.6 mm) wall thickness. Consequently, a 1-inch (25 mm) copper pipe had a 1 18-inch (28.58 mm) outside diameter. The outside diameter was the important dimension for mating with fittings. The wall thickness on modern copper is usually thinner than 116 inch (1.6 mm), so the internal diameter is only "nominal" rather than a controlling dimension. Newer pipe technologies sometimes adopted a sizing system as its own. PVC pipe uses the Nominal Pipe Size.
Pipe sizes are specified by a number of national and international standards, including API 5L, ANSI/ASME B36.10M and B36.19M in the US, BS 1600 and BS EN 10255 in the United Kingdom and Europe.
There are two common methods for designating pipe outside diameter (OD). The North American method is called NPS ("Nominal Pipe Size") and is based on inches (also frequently referred to as NB ("Nominal Bore")). The European version is called DN ("Diametre Nominal" / "Nominal Diameter") and is based on millimetres. Designating the outside diameter allows pipes of the same size to be fit together no matter what the wall thickness.
For pipe sizes less than NPS 14 inch (DN 350), both methods give a nominal value for the OD that is rounded off and is not the same as the actual OD. For example, NPS 2 inch and DN 50 are the same pipe, but the actual OD is 2.375 inches or 60.33 millimetres. The only way to obtain the actual OD is to look it up in a reference table.
For pipe sizes of NPS 14 inch (DN 350) and greater the NPS size is the actual diameter in inches and the DN size is equal to NPS times 25 (not 25.4) rounded to a convenient multiple of 50. For example, NPS 14 has an OD of 14 inches or 355.60 millimetres, and is equivalent to DN 350.
Since the outside diameter is fixed for a given pipe size, the inside diameter will vary depending on the wall thickness of the pipe. For example, 2" Schedule 80 pipe has thicker walls and therefore a smaller inside diameter than 2" Schedule 40 pipe.
Steel pipe has been produced for about 150 years. The pipe sizes that are in use today in PVC and galvanized were originally designed years ago for steel pipe. The number system, like Sch 40, 80, 160, were set long ago and seem a little odd. For example, Sch 20 pipe is even thinner than Sch 40, but same OD. And while these pipes are based on old steel pipe sizes, there is other pipe, like cpvc for heated water, that uses pipe sizes, inside and out, based on old copper pipe size standards instead of steel.
Many different standards exist for pipe sizes, and their prevalence varies depending on industry and geographical area. The pipe size designation generally includes two numbers; one that indicates the outside (OD) or nominal diameter, and the other that indicates the wall thickness. In the early twentieth century, American pipe was sized by inside diameter. This practice was abandoned to improve compatibility with pipe fittings that must usually fit the OD of the pipe, but it has had a lasting impact on modern standards around the world.
In North America and the UK, pressure piping is usually specified by Nominal Pipe Size (NPS) and schedule (SCH). Pipe sizes are documented by a number of standards, including API 5L, ANSI/ASME B36.10M (Table 1) in the US, and BS 1600 and BS 1387 in the United Kingdom. Typically the pipe wall thickness is the controlled variable, and the Inside Diameter (I.D.) is allowed to vary. The pipe wall thickness has a variance of approximately 12.5 percent.
In the rest of Europe pressure piping uses the same pipe IDs and wall thicknesses as Nominal Pipe Size, but labels them with a metric Diameter Nominal (DN) instead of the imperial NPS. For NPS larger than 14, the DN is equal to the NPS multiplied by 25. (Not 25.4) This is documented by EN 10255 (formerly DIN 2448 and BS 1387) and ISO 65, and it is often called DIN or ISO pipe.
Japan has its own set of standard pipe sizes, often called JIS pipe.
The Iron pipe size (IPS) is an older system still used by some manufacturers and legacy drawings and equipment. The IPS number is the same as the NPS number, but the schedules were limited to Standard Wall (STD), Extra Strong (XS), and Double Extra Strong (XXS). STD is identical to SCH 40 for NPS 1/8 to NPS 10, inclusive, and indicates .375" wall thickness for NPS 12 and larger. XS is identical to SCH 80 for NPS 1/8 to NPS 8, inclusive, and indicates .500" wall thickness for NPS 8 and larger. Different definitions exist for XXS, however it is never the same as SCH 160. XXS is in fact thicker than SCH 160 for NPS 1/8" to 6" inclusive, whereas SCH 160 is thicker than XXS for NPS 8" and larger.
Another old system is the Ductile Iron Pipe Size (DIPS), which generally has larger ODs than IPS.
Copper plumbing tube for residential plumbing follows an entirely different size system in America, often called Copper Tube Size (CTS); see domestic water system. Its nominal size is neither the inside nor outside diameter. Plastic tubing, such as PVC and CPVC, for plumbing applications also has different sizing standards.
Agricultural applications use PIP sizes, which stands for Plastic Irrigation Pipe. PIP comes in pressure ratings of 22 psi (150 kPa), 50 psi (340 kPa), 80 psi (550 kPa), 100 psi (690 kPa), and 125 psi (860 kPa) and is generally available in diameters of 6", 8", 10", 12", 15", 18", 21", and 24".


== Standards ==
The manufacture and installation of pressure piping is tightly regulated by the ASME "B31" code series such as B31.1 or B31.3 which have their basis in the ASME Boiler and Pressure Vessel Code (BPVC). This code has the force of law in Canada and the USA. Europe and the rest of the world has an equivalent system of codes. Pressure piping is generally pipe that must carry pressures greater than 10 to 25 atmospheres, although definitions vary. To ensure safe operation of the system, the manufacture, storage, welding, testing, etc. of pressure piping must meet stringent quality standards.
Manufacturing standards for pipes commonly require a test of chemical composition and a series of mechanical strength tests for each heat of pipe. A heat of pipe is all forged from the same cast ingot, and therefore had the same chemical composition. Mechanical tests may be associated to a lot of pipe, which would be all from the same heat and have been through the same heat treatment processes. The manufacturer performs these tests and reports the composition in a mill traceability report and the mechanical tests in a material test report, both of which are referred to by the acronym MTR. Material with these associated test reports is called traceable. For critical applications, third party verification of these tests may be required; in this case an independent lab will produce a certified material test report(CMTR), and the material will be called certified.
Some widely used pipe standards or piping classes are:
The API range - now ISO 3183. E.g.: API 5L Grade B - now ISO L245 where the number indicates yield strength in MPa
ASME SA106 Grade B (Seamless carbon steel pipe for high temperature service)
ASTM A312 (Seamless and welded austenitic stainless steel pipe)
ASTM C76 (Concrete Pipe)
ASTM D3033/3034 (PVC Pipe)
ASTM D2239 (Polyethylene Pipe)
ISO 14692 (Petroleum and natural gas industries. Glass-reinforced plastics (GRP) piping. Qualification and manufacture)
ASTM A36 (Carbon steel pipe for structural or low pressure use)
ASTM A795 (Steel pipe specifically for fire sprinkler systems)
API 5L was changed in the second half of 2008 to edition 44 from edition 43 to make it identical to ISO 3183. It is important to note that the change has created the requirement that sour service, ERW pipe, pass a hydrogen induced cracking (HIC) test per NACE TM0284 in order to be used for sour service.
ACPA [American Concrete Pipe Association]
AWWA [American Water Works Association]
AWWA M45
ASTM A252 [Standard Specification for Welded and Seamless Steel Pipe Piles]
See this site for more specification summaries.


== Installation ==

Pipe installation is often more expensive than the material and a variety of specialized tools, techniques, and parts have been developed to assist this. Pipe is usually delivered to a customer or jobsite as either "sticks" or lengths of pipe (typically 20 feet, called single random length) or they are prefabricated with elbows, tees and valves into a prefabricated pipe spool [A pipe spool is a piece of pre-assembled pipe and fittings, usually prepared in a shop so that installation on the construction site can be more efficient.]. Typically, pipe smaller than 2.0 inch are not pre-fabricated. The pipe spools are usually tagged with a bar code and the ends are capped (plastic) for protection. The pipe and pipe spools are delivered to a warehouse on a large commercial/industrial job and they may be held indoors or in a gridded laydown yard. The pipe or pipe spool is retrieved, staged, rigged, and then lifted into place. On large process jobs the lift is made using cranes and hoist and other material lifts. They are typically temporarily supported in the steel structure using beam clamps, straps, and small hoists until the Pipe Supports are attached or otherwise secured.
An example of a tool used for installation for a small plumbing pipe (threaded ends) is the pipe wrench. Small pipe is typically not heavy and can be lifted into place by the installation craft laborer. However, during a plant outage or shutdown, the small (small bore) pipe may also be pre-fabricated to expedite installation during the outage. After the pipe is installed it will be tested for leaks. Before testing it may need to be cleaned by blowing air or steam or flushing with a liquid.


=== Pipe supports ===
Pipes are usually either supported from below or hung from above (but may also be supported from the side), using devices called pipe supports. Supports may be as simple as a pipe "shoe" which is akin to a half of an I-beam welded to the bottom of the pipe; they may be "hung" using a clevis, or with trapeze type of devices called pipe hangers. Pipe supports of any kind may incorporate springs, snubbers, dampers, or combinations of these devices to compensate for thermal expansion, or to provide vibration isolation, shock control, or reduced vibration excitation of the pipe due to earthquake motion. Some dampers are simply fluid dashpots, but other dampers may be active hydraulic devices that have sophisticated systems that act to dampen peak displacements due to externally imposed vibrations or mechanical shocks. The undesired motions may be process derived (such as in a fluidized bed reactor) or from a natural phenomenon such as an earthquake (design basis event or DBE).
Pipe hanger assembles are usually attached with pipe clamps. Possible exposure to high temperatures and heavy loads should be included when specifying which clamps are needed.


=== Joining ===

Pipes are commonly joined by welding, using threaded pipe and fittings; sealing the connection with a pipe thread compound, Polytetrafluoroethylene (PTFE) Thread seal tape, oakum, or PTFE string, or by using a mechanical coupling. Process piping is usually joined by welding using a TIG or MIG process. The most common process pipe joint is the butt weld. The ends of pipe to be welded must have a certain weld preparation called an End Weld Prep (EWP) which is typically at an angle of 37.5 degrees to accommodate the filler weld metal. The most common pipe thread in North America is the National Pipe Thread (NPT) or the Dryseal (NPTF) version. Other pipe threads include the British standard pipe thread (BSPT), the garden hose thread (GHT), and the fire hose coupling (NST).
Copper pipes are typically joined by soldering, brazing, compression fittings, flaring, or crimping. Plastic pipes may be joined by solvent welding, heat fusion, or elastomeric sealing.
If frequent disconnection will be required, gasketed pipe flanges or union fittings provide better reliability than threads. Some thin-walled pipes of ductile material, such as the smaller copper or flexible plastic water pipes found in homes for ice makers and humidifiers, for example, may be joined with compression fittings.
Underground pipe typically uses a "push-on" gasket style of pipe that compresses a gasket into a space formed between the two adjoining pieces. Push-on joints are available on most types of pipe. A pipe joint lubricant must be used in the assembly of the pipe. Under buried conditions, gasket-joint pipes allow for lateral movement due to soil shifting as well as expansion/contraction due to temperature differentials. Plastic MDPE and HDPE gas and water pipes are also often joined with Electrofusion fittings.
Large above ground pipe typically uses a flanged joint, which is generally available in ductile iron pipe and some others. It is a gasket style where the flanges of the adjoining pipes are bolted together, compressing the gasket into a space between the pipe.
Mechanical grooved couplings or Victaulic joints are also frequently used for frequent disassembly & assembly. Developed in the 1920s, these mechanical grooved couplings can operate up to 1,200psi working pressures and available in materials to match the pipe grade. Another type of mechanical coupling is a Swagelok brand fitting; this type of compression fitting is typically used on small tubing under 3/4 inch in diameter.
When pipes join in chambers where other components are needed for the management of the network (valves, gauges...), dismantling joints are generally used, in order to make mounting/dismounting easier.


=== Fittings and valves ===

Fittings are also used to split or join a number of pipes together, and for other purposes. A broad variety of standardized pipe fittings are available; they are generally broken down into either a tee, an elbow, a branch, a reducer/enlarger, or a wye. Valves control fluid flow and regulate pressure. The piping and plumbing fittings and valves articles discuss them further.


== Cleaning ==

The inside of pipes can be cleaned with a tube cleaning process, if they are contaminated with debris or fouling. This depends on the process that the pipe will be used for and the cleanliness needed for the process. In some cases the pipes are cleaned using a displacement device formally known as a Pipeline Inspection Gauge or "pig"; alternately the pipes or tubes may be chemically flushed using specialized solutions that are pumped through. In some cases, where care has been taken in the manufacture, storage, and installation of pipe and tubing, the lines are blown clean with compressed air or nitrogen.


== Other uses ==

Pipe is widely used in the fabrication of handrails, guardrails, and railings.


== See also ==


== References ==


=== Bibliography ===


== External links ==
A history of Pipe - With dimensions
Critical fluid handling with tubes
Quick calculator to determine standard pipe dimensions For Carbon Steel and Stainless Steel pipes as per ANSI.