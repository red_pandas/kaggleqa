SkyOS (Sky Operating System) was a prototype commercial, proprietary, graphical desktop operating system written for the x86 computer architecture. As of January 30, 2009 development was halted with no plans to resume its development. In August 2013 Robert Szeleney announced the release of a public beta on the SkyOS website. This allows public users to download a Live CD of the SkyOS operating system, for testing and to optionally install the system.


== HistoryEdit ==

SkyOS is the result of over ten years of work by Robert Szeleney and volunteers.
A young man at the university, Szeleney and several friends began the "Sky Operating System" as an experiment in OS design. As the years progressed and the other founding members of SkyOS became distant from the project, Szeleney continued work on the operating system in his spare time.
Szeleney's full-time job was in automation programming, and he already had a fair understanding of operating system design. But he continued to use SkyOS as a learning device, releasing four versions under an open source license.
The project reached the height of its popularity in 2004, when Szeleney decided to close the source of SkyOS and begin work on the fifth version. At the beginning of the year, Szeleney started a paid beta program to fund development and distribution of the OS, expecting to have very few (if any) interested subscribers. At the same time, members of the community hosted a contest to determine the look of the GUI, which drew significant attention to the project. So much attention, in fact, that by the end of that same year, the SkyOS community had tripled in size, and Szeleney had hundreds of active beta testers downloading and testing his twice-monthly releases.
SkyOS underwent many changes after this surge in popularity. Because of significant differences at the source level, Szeleney stopped thinking of what was under development as the fifth version of his operating system, and the name "SkyOS 5.0" was rebranded to simply "SkyOS". A more professional demeanor was taken throughout the project, and Szeleney even considered incorporating under the name "Djinnworks". The SkyOS community has since shrunk significantly from its peak in 2004.
As of January 30, 2009, development on SkyOS was halted. The developer was deciding on which course of action to take regarding future development:
Open source SkyOS
Make SkyOS available for free
Specialize on a yet-to-be-defined niche
Stop SkyOS development
There is also an experimental attempt to use NetBSD or Linux to solve the driver issues.
As of September 8, 2013, SkyOS is freely available for downloads, for testing.


=== Beta releasesEdit ===


== TechnologyEdit ==


=== KernelEdit ===
SkyOS user space lies on top of a custom, modularized, preemptive-multitasking kernel with support for common features like process and thread isolation, memory management/paging, kernel debugging, low-level locking primitives, and real-time PIC/APIC timers. It is a monolithic kernel, with drivers that are dynamically loaded into kernel space via an extension API.
There are some notable features that distinguish the SkyOS kernel from others. These include:
Kernel-mode VESA support, allowing for graphical display immediately upon power-up
Architecture abstraction layer, allowing SkyOS to be easily ported to other architectures
Advanced CPU support, including Multi-Core/SMP/HyperThreading and all the major x86 extensions
Contrary to a popular misconception, the kernel is not derived from Linux or any of the various BSDs. It was written entirely by Szeleney over the course of several years, and uses no outside code.


=== SkyGIEdit ===
The design of the SkyGI API is loosely based around the concepts of the Qt and Swing windowing toolkits. A core principle of SkyGI is the "view." Every GUI object is derived from the base "view" object, and, as such, all have similar properties and behave in similar ways.
SkyGI has built in support for internationalization and localization, allowing third party developers to easily create multilingual applications that are based around easy to write 'translation' files. The framework also facilitates accessibility by building in support for advanced keyboard navigation.
Native controls can be themed by placing a 'theme' file, syntactically similar to the MSStyles format, in a directory corresponding to the location of the executable.


==== ComponentsEdit ====
The SkyOS GUI closely follows the WIMP desktop metaphor used in all modern GUIs. However, there are several major components of the SkyOS GUI that are distinct from other operating systems:
The Panel is the functional equivalent to the task bar in Windows or the dock in Mac OS X. It is used to launch programs and switch between application windows, as well as to display information to the user. Plug-ins allow developers to extend the panel's functionality. Existing plug-ins, for example, have added query-based fast application launching, and have modified the day/night indicator to instead show localized weather information.
The Notifier is a built-in user interface element for alerting the user to any number of things, including application crashes and hardware additions/removals. Application programmers may use the notifier for their own purposes via the Desktop Communication Service.
The Viewer is the native file browser on SkyOS. It features multiple view types, thumbnails, and metadata-based display of information, as well as "search as you type" browsing and advanced queries. The Viewer is one of the first file system browsers to support tabbed viewing.


=== SkyFSEdit ===

SkyFS is a fork of the OpenBFS filesystem. It supports the following notable features:
64-bit data structures
Journaling
Metadata support, allowing for, among other things, ACLs
Metadata indexing and querying
POSIX file permissions
Few changes have been made to the filesystem since the fork. The Magic ID has been changed to differentiate between SkyFS partitions and BFS partitions, and space is now reserved at the beginning of each partition for a bootloader. Otherwise, the on-disk layout of the filesystems are identical.
SkyOS can also be run from the following filesystems:
FAT32/FAT16/FAT12
ISO 9660


==== Fast searchingEdit ====
With the help of the SQL-based Index Feeder, SkyFS supports fast metadata and full-text searching similar in concept to WinFS and Spotlight. This allows for instantaneous system-wide searches as well as "search as you type" file browsing.
The concept of virtual folders, which is commonly implemented alongside a query-based search framework, exists in SkyOS as the ability to save queries in the Viewer for later execution.
Here is an example Viewer query that would display all .txt documents that were modified today:
name = *.txt && last_modified >= %today%
To perform a search, it is not necessary to understand the syntax of the above query. Queries can be built using a GUI, or simpler keyword-based searching can be (and is by default) used.


== DevelopmentEdit ==
While there are several well-developed APIs as well as exceptional POSIX compatibility, writing and releasing applications for SkyOS can be difficult due to minor binary incompatibilities between betas. Developers that wish to offer an application for SkyOS must keep up with the changes being made to the APIs, and are usually required to modify and recompile their applications upon the release of each new beta. There are no plans to finalize any of the APIs before SkyOS is publicly released, as Szeleney wishes to reserve the ability to further improve them as development progresses.
SkyOS executables are compiled into the ELF format using the GNU Compiler Collection. Commonly, developers cross compile their applications for use on SkyOS. However, as GCC has been ported and is actively being maintained, it is also possible to develop applications inside of a running SkyOS installation. Most notably, Scribis, a multi-protocol instant messenger, was developed by a volunteer exclusively under a running SkyOS environment.
Developers who wish to distribute their applications via an official channel may set up a Software Store repository online. The SkyOS Software Store is a GUI-based software distribution system that implements a custom package format and simple repository protocol.
The evolution of SkyOS itself happens rather sporadically, with little documented long-term planning. This has often caused concern in the community, but has allowed for a development pace much faster than democratically-steered projects.


=== Native APIsEdit ===
C++ is the only programming language officially supported for native development on SkyOS. Perl and Python have been ported, but the virtual machines have no bindings to any of the native SkyOS APIs. .NET applications that are command-line based can be run through Mono, but again, as there are no bindings to SkyGI, Windows Forms based GUI applications can not be run.
The Desktop Communication Service is an object-oriented inter-process communication framework used throughout SkyOS. It allows command-line and API based communication between both kernel and user space processes in SkyOS.
In this messaging model, human-readable messages are sent to 'interfaces' that are represented by a string of identifiers.
For example, sending this message to "Notify.Media.Player.Control" would cause the SkyOS media player to advance to the next song:

MessageType STRING "Next Song"

SkyOS emits messages in response to hundreds of events, including device attachments, successful software installs, battery level changes, and new weather data, all available to any application or driver that subscribes to the appropriate interface.
The Integrated Streaming System (ISS) is a set of C++ APIs designed to facilitate media interaction, similar to the concept of a sound server in Linux. The API is abstracted so that media playback is completely independent of codec.
Basic functionality can be achieved in less than ten lines of code in situations where default behavior is acceptable to the programmer. However, through a very extended API, all relevant parts of the audio-visual processing pipeline can be controlled.

"For example, you can create two audio streams, one stereo, one 5.1 DolbyDigital, connecting an echo filter to the second, associating the 5.1 stream with a SB Audigy live and the stereo stream with a basic stereo soundcard, and apply various software digital filters to individual streams."

 Robert Szeleney 


=== Porting applicationsEdit ===
Most command-line applications that were written to be compiled with the GNU Toolchain can be ported to SkyOS with little or no modification. Several large applications, including Apache, GCC, Samba, CUPS, and Bash have been ported using the Toolchain.
The following applications have been ported using native APIs and are currently being maintained on SkyOS:
Mozilla Firefox
Mozilla Thunderbird
Blender
Pixel
Nvu
SDL
The ports of GTK and several GTK-based applications have become unusable with recent API changes. It is not yet known if in the future they will be brought back up to speed.


== ReceptionEdit ==


=== Third party supportEdit ===
Due to the limited popularity of the operating system, SkyOS has very few third party developers. This means that many common hardware devices are not supported by SkyOS, and are not likely to be without significant money and effort on Szeleney's part.
Less importantly, this means that a limited variety of applications are available for SkyOS. While some of the basic needs such as web browsing and e-mail are covered, others are not. These needs include:
A full office suite
An advanced media manager
Popular commercial video games
To help stimulate third party development, SkyOS employs a "code ransom" system where people may donate money to projects that they wish to see completed. Developers who complete these projects will then receive any money that has been donated to the project. If possible, Szeleney hopes to complete any projects not claimed by the code ransom before SkyOS leaves beta status.


=== Security and stabilityEdit ===
Although the APIs for working with user and file permissions are well-developed, none of the applied permissions are actually obeyed by the system. While SkyOS is under development, this will remain the intended behavior.
The network stack used in SkyOS is a custom design which is largely untested in a production environment. No known attempts have been made to penetrate a live system from the internet, meaning that the vulnerability of SkyOS is unknown.
The "passworded folders" feature is implemented at the filesystem level, so no SkyOS applications can use the native APIs to access files without the correct password having been entered by the user. However, files in a passworded folder are stored on-disk in a way identical to un-passworded files, so a clever programmer could write a userspace SkyFS driver that simply ignores the password and reads the directory at whim.
The stability of SkyOS varies depending on hardware. Its ability to operate on a variety of systems has improved throughout its development. However, no comprehensive hardware compatibility list is kept due to its erratic development.


=== Involvement with free softwareEdit ===
As a former open source project, SkyOS has been the subject of many debates involving free software.
Often, SkyOS is accused of violating the GPL. That is, since developing an operating system is thought to be an exceedingly complex task, the conclusion is drawn by some that Szeleney must have stolen code from free software in order for him to have made the progress that he has. In fact, the opposite is implied by Szeleney's public dealings with open source developers. Even when not required under license, Szeleney has consistently published changes made to open source works used in SkyOS.
Some beta testers fear that their investment will be lost if Szeleney becomes unable or unwilling to continue the development of SkyOS. This fear is commonly used as one of the more well-substantiated arguments for open sourcing SkyOS. Following the cessation of active development, Szeleney has officially released the last build of SkyOS to the general public, which is available for download.


== See alsoEdit ==
List of operating systems


== ReferencesEdit ==


== External linksEdit ==
SkyOS.orgThe SkyOS Homepage
SkyOS.atThe SkyOS Homepage (Alternative Address)
TechIMOSkyOS Developers Interview
SlashdotWalking Through SkyOS 5.0 Beta
SlashdotThunderbird and Firefox Ported to SkyOS
OSNews.comSkyOS, The 7th Beta and Robert Szeleney
[1]