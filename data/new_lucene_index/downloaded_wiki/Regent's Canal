Regent's Canal is a canal across an area just north of central London, England. It provides a link from the Paddington Arm of the Grand Union Canal, just north-west of Paddington Basin in the west, to the Limehouse Basin and the River Thames in east London. The canal is 13.8 kilometres (8.6 miles) long.


== HistoryEdit ==
First proposed by Thomas Homer in 1802 as a link from the Paddington arm of the then Grand Junction Canal (opened in 1801) with the River Thames at Limehouse, the Regent's Canal was built during the early 19th century after an Act of Parliament was passed in 1812. Noted architect and town planner John Nash was a director of the company; in 1811 he had produced a masterplan for the Prince Regent to redevelop a large area of central north London  as a result, the Regents Canal was included in the scheme, running for part of its distance along the northern edge of Regent's Park.

As with many Nash projects, the detailed design was passed to one of his assistants, in this case James Morgan, who was appointed chief engineer of the canal company. Work began on 14 October 1812. The first section from Paddington to Camden Town, opened in 1816 and included a 251-metre (274 yd) long tunnel under Maida Hill east of an area now known as 'Little Venice', and a much shorter tunnel, just 48 metres (52 yd) long, under Lisson Grove. The Camden to Limehouse section, including the 886-metre (969 yd) long Islington tunnel and the Regent's Canal Dock (used to transfer cargo from seafaring vessels to canal barges  today known as Limehouse Basin), opened four years later on 1 August 1820. Various intermediate basins were also constructed (e.g.: Cumberland Basin to the east of Regent's Park, Battlebridge Basin (close to King's Cross, London) and City Road Basin). Many other basins such as Wenlock Basin, Kingsland Basin, St. Pancras Stone and Coal Basin, and one in front of the Great Northern Railway's Granary were also built, and some of these survive.
The City Road Basin, the nearest to the City of London, soon eclipsed the Paddington Basin in the amount of goods carried, principally coal and building materials. These were goods that were being shipped locally, in contrast to the canal's original purpose of transshipping imports to the Midlands. The opening of the London and Birmingham Railway in 1838 actually increased the tonnage of coal carried by the canal. However, by the early twentieth century, with the Midland trade lost to the railways, and more deliveries made by road, the canal had fallen into a long decline.


=== Railway projectsEdit ===
There were a number of abortive projects to convert the route of the canal into a railway. In September 1845 a special general assembly of the proprietors approved the sale of the canal at the price of one million pounds to a group of businessmen  who had formed the Regent's Canal Railway Company for the purpose. The advertisement for the company explained:

The vast importance of this undertaking, whereby a junction will be effected between all existing and projected railways north of the Thames, combined with the advantage of a General City Terminus, is too obvious to require comment. By the proposed railway, passengers and goods will be brought into the heart of the City at a great saving of time and expense, and facilities will be afforded for the more expeditious transmission of the mails to most parts of the kingdom.

The railway company subsequently failed, but in 1846 the directors of the canal went about trying to obtain an Act of Parliament to allow them to build a railway along its banks. The scheme was abandoned in the face of vigorous opposition, especially from the government who objected to the idea of a railway passing through Regent's Park. In 1859, two further schemes to convert the canal into a railway were proposed. One, from a company called the Central London Railway and Dock Company, was accepted by the directors, but once again the railway company failed. In 1860 the Regent's Canal Company proposed a railway track alongside the canal from Kings Cross to Limehouse, but funds could not be raised. Further schemes over the next twenty years also came to nothing.
In 1883, after some years of negotiation, the canal was sold to a company called the Regent's Canal and City Docks Railway Company. at a cost of 1,170,585. The company altered its name to the North Metropolitan Railway and Canal Company in 1892, but no railway was ever built; instead it raised money for dock and canal improvement and eventually, in 1904, became the Regent's Canal and Dock Company.


=== Twentieth centuryEdit ===
In 1927, the Regent's Canal Company bought the Grand Junction Canal and the Warwick Canals, the merged entity coming into force on 1 January 1929 as the Grand Union Canal Company. A new carrying subsidiary was formed, the Grand Union Canal Carrying Co, with a fleet of 186 pairs of new narrow boats. A vigorous expansion policy was combined with a successful drive for new traffic much of which was carried on the Regent's Canal. Iron and steel for Birmingham, imported via Regent's Canal Dock, was won from the railways by offering a quicker and cheaper service. Other traffic commodities included grain, raw materials for HP sauce, leather waste, last blocks, cresylic acid, zinc ashes, and even cheese. The decline of the 1920s had been reversed, and tonnage rose from 8999 tons in 1931 to 168,638 tons in 1941. In August 1938 the Cumberland Basin was dammed off and drained and in the next two years it was formally abandoned. The Regent's Canal was nationalised in 1948. By this time, the canal's importance for commercial traffic was dwindling, and by the late 1960s commercial vessels had almost ceased to operate, the lorry taking over the traffic not already lost to the railway in the 19th century, and closure of the Regent's Canal Dock to shipping in 1969 was the last nail in the coffin.


=== New usesEdit ===
A new purpose was found for the canal route in 1979, when the Central Electricity Generating Board (CEGB) installed underground cables in a trough below the towpath between St John's Wood and City Road. These 400 kV cables now form part of the National Grid, supplying electrical power to London. Pumped canal water is circulated as a coolant for the high-voltage cables.

The canal is frequently used today for pleasure cruising; a regular waterbus service operates between Maida Vale and Camden, running hourly during the summer months.
Due to the increase in cycle commuting since the 2005 London Bombings and increasing environmental awareness, the canal's towpath has become a busy cycle route for commuters. National Cycle Route 1 includes the stretch along the canal towpath from Limehouse Basin to Mile End. British Waterways has carried out several studies into the effects of sharing the towpath between cyclists and pedestrians, all of which have concluded that despite the limited width there are relatively few problems.
The Code of Conduct for shared use sets out the behaviour expected of pedestrians and cyclists.


== GeographyEdit ==

The Regent's Canal forms a junction with the old Grand Junction Canal at Little Venice, a short distance north of Paddington Basin. After passing through the Maida Hill and Lisson Grove tunnels, the canal curves round the northern edge of Regent's Park and bisects London Zoo. It continues through Camden Town and King's Cross Central, also known as the Railway Lands. It performs a sharp bend at Camley Street Natural Park, following Goods Way where it flows behind both St Pancras railway station and King's Cross railway station. The canal opens out into Battlebridge Basin, originally known as Horsfall Basin, home of the London Canal Museum, and where on the northwest corner the new Kings Place development is currently taking shape. Continuing eastwards beyond the Islington tunnel and passing many notable landmarks including the Rosemary Branch Theatre, it then forms the southern end of Broadway Market and then meets the Hertford Union Canal by Victoria Park, East London before Twig Folly Bridge at Roman Road on the Bethnal Green/Mile End Palmers Road junction in Bethnal Green and Mile End junction near Twig Folly Bridge in Roman Road after which it turns south towards the Limehouse Basin, where today it also meets the Limehouse Cut. At this point the canal ends as it joins the River Thames.


== Maximum craft dimensionsEdit ==
On the Regent's Canal the maximum length is 21.95 metres (72.0 ft), with a beam of 4.27 metres (14.0 ft) and a headroom of 2.79 metres (9 ft 2 in). The navigational depth is, on average 1.15 m (3 ft 6 in).


== CultureEdit ==
In 2012, playwright Rob Inglis was awarded a 16,000 Arts Council grant to write Regent's Canal, a Folk Opera, a musical that celebrates the 200th anniversary of the digging of the canal. It played in a number of locations around London in 2012.
The London Canal Museum occupies Carlo Gatti's Ice House at Kings Cross, London, 12-13 New Wharf Road.


== See alsoEdit ==
St Pancras Cruising Club
Camden Lock
Kingsland Basin
List of canal basins in Great Britain
List of canal aqueducts in Great Britain
List of canal locks in Great Britain
List of canal tunnels in Great Britain


== ReferencesEdit ==


== Further readingEdit ==
Alan Faulkner  The Regent's Canal: London's Hidden Waterway (2005) ISBN 1-870002-59-8
Alan Faulkner  The George and the Mary: A Brief History of the Grand Union Canal carrying Company Ltd (1973)


== External linksEdit ==
Canalplan AC Gazetteer: Grand Union Canal (Regent's Canal)
Plan of Grand Union Canal (Regent's Canal) with links to places of interest near each lock, basin, and bridge.
Photographs of the Regents Canal
Regent's Canal interactive map
More background information and history of the Regent's Arm
Regent's Canal, Camden markets, history and pictures