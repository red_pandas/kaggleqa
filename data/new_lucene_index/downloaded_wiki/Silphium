Silphium (also known as silphion, laserwort, or laser) was a plant that was used in classical antiquity as a seasoning and as a medicine. It was the essential item of trade from the ancient North African city of Cyrene, and was so critical to the Cyrenian economy that most of their coins bore a picture of the plant. The valuable product was the plant's resin (laser, laserpicium, or lasarpicium).
Silphium was an important species in prehistory, as evidenced by the Egyptians and Knossos Minoans developing a specific glyph to represent the silphium plant. It was used widely by most ancient Mediterranean cultures; the Romans considered it "worth its weight in denarii" (silver coins). Legend said that it was a gift from the god Apollo.
The exact identity of silphium is unclear. It is commonly believed to be a now-extinct plant of the genus Ferula, perhaps a variety of "giant fennel". The still-extant plant Ferula tingitana has been suggested as another possibility. Another plant, asafoetida, was used as a cheaper substitute for silphium, and had similar enough qualities that Romans, including the geographer Strabo, used the same word to describe both.


== Identity and extinctionEdit ==
The identity of silphium is highly debated. It is generally considered to belong to the genus Ferula, probably as an extinct species (although the currently extant plants Ferula tingitana, Ferula narthex, and Thapsia garganica have historically been suggested as possible identities). K. Parejko, writing on its possible extinction, concludes that "because we cannot even accurately identify the plant we cannot know for certain whether it is extinct."
The cause of silphium's supposed extinction is not entirely known. The plant grew along a narrow coastal area, about 125 by 35 miles (201 by 56 km), in Cyrenaica (in present-day Libya). Much of the speculation about the cause of its extinction rests on a sudden demand for animals that grazed on the plant, for some supposed effect on the quality of the meat. Overgrazing combined with overharvesting may have led to its extinction. Demand for its contraceptive use was reported to have led to its extinction in the 3rd or 2nd century BCE. The climate of the Maghreb has been drying over the millennia, and desertification may also have been a factor. Another theory is that when Roman provincial governors took over power from Greek colonists, they over-farmed silphium and rendered the soil unable to yield the type that was said to be of such medicinal value. Theophrastus wrote in Enquiry into Plants that the type of ferula specifically referred to as "silphium" was odd in that it could not be cultivated. He reports inconsistencies in the information he received about this, however. Pliny reported that the last known stalk of silphium found in Cyrenaica was given to the Emperor Nero "as a curiosity".


== Medicinal usesEdit ==
Many medical uses were ascribed to the plant. It was said that it could be used to treat cough, sore throat, fever, indigestion, aches and pains, warts, and all kinds of maladies. Hippocrates wrote:

When the gut protrudes and will not remain in its place, scrape the finest and most compact silphium into small pieces and apply as a cataplasm.

It has been speculated that the plant may also have functioned as a contraceptive, based partly on Pliny's statement that it could be used "to promote the menstrual discharge". Many species in the parsley family have estrogenic properties, and some, such as wild carrot, are reputed abortifacients (chemicals that terminate a pregnancy). Given this, it is quite possible that the plant was pharmacologically active in the prevention or termination of pregnancy.


== Culinary usesEdit ==
Silphium was used in Greco-Roman cooking, notably in recipes by Apicius.
Long after its extinction, silphium continued to be mentioned in lists of aromatics copied one from another, until it makes perhaps its last appearance in the list of spices that the Carolingian cook should have at hand  Brevis pimentorum que in domo esse debeant ("A short list of condiments that should be in the home")  by a certain "Vinidarius", whose excerpts of Apicius survive in one 8th-century uncial manuscript. Vinidarius's dates may not be much earlier.


== Connection with the heart symbolEdit ==

There has been some speculation about the connection between silphium and the traditional heart shape (). Silver coins from Cyrene of the 65th century BCE bear a similar design, sometimes accompanied by a silphium plant and understood to represent its seed or seed pod.
Contemporary writings help tie silphium to sexuality and love. Silphium appears in Pausanias' Description of Greece in a story of the Dioscuri staying at a house belonging to Phormion, a Spartan, "For it so happened that his maiden daughter was living in it. By the next day this maiden and all her girlish apparel had disappeared, and in the room were found images of the Dioscuri, a table, and silphium upon it." Silphium as Laserpicium makes an appearance in a poem (Catullus 7) of Catullus to his lover Lesbia (though others have suggested that the reference here is instead to silphium's use as a treatment for mental illness, tying it to the 'madness' of love).


== HeraldryEdit ==

In the Italian military heraldry, Il silfio doro reciso di Cirenaica (Silphium of Cyrenaica, smoothly cut and printed in gold; in blazon: silphium couped or of Cyrenaica) was the symbol granted to the units that fought in the campaigns in North Africa during World War II.


== Modern referencesEdit ==
A fruitless search for silphium by Romans in the reign of the emperor Vespasian is a sub-plot of the crime novel Two for the Lions by Lindsey Davis.
In HBO's Rome, Gaia acquires the abortion-inducing herb called silphium, which she administers surreptitiously to Eirene in her tea in "A Necessary Fiction". Eirene miscarries, and then dies (apparently of blood loss).
In the Starz series Spartacus, character Ilythia (played by Viva Bianca) is shown with a vial of silphium, wishing to terminate her pregnancy. Lucretia (Lucy Lawless) urges her to wait, saying that she had given the substance to a number of her house servants, warning that Ilythia will become sick, weak, and will bleed for a few days from the effects of the plant.
In the Doc Savage story magazine series, the September 1934 issue, "Fear Cay" features the plant. In it, silphium is claimed to be the true source of power of the mysterious Fountain of Youth. Throughout the story, it is referred to as the "Mysterious weed". At the end of the story, Doc Savage theorizes that the plant has no longevity properties and that a recipient of its benefits, Dan Thunden, a spry 131-year-old, is long lived due to his isolated living conditions.


== ReferencesEdit ==
Dalby, Andrew (2002). Dangerous Tastes: The Story of Spices. University of California Press. ISBN 0-520-23674-2. 
Herodotus. The Histories. II:161, 181, III:131, IV:15065, 20005.
Pausanias. Description of Greece 3.16.13
Pliny the Elder. Natural History. XIX:15 and XXII:10006.
Tatman, John. "Silphium: Ancient wonder drug?". Jencek's Ancient Coins & Antiquities. Retrieved 2007-02-05. 
Theophrastus. Enquiry into plants and minor works on odours and weather signs, with an English translation by Sir Arthur Hort, bart (1916). Volume 1 (Books IV) and Volume 2 (Books VIIX) Volume 2 includes the index, which lists silphium (Greek ) on page 476, column 2, 2nd entry.


== NotesEdit ==


== Further readingEdit ==
Buttrey, T. V. (1997). "Part I: The Coins from the Sanctuary of Demeter and Persephone". In D. White (ed.) (ed.). Extramural Sanctuary of Demeter and Persephone at Cyrene Libya, Final Reports: Vol. VI. Philadelphia. pp. 166. 
Fisher, Nick (1996). "Laser-Quests: unnoticed allusions to contraception in a poet and a princeps?". Classics Ireland ( Scholar search) 3: 7397. doi:10.2307/25528292. JSTOR 25528292.  
Gemmill, Chalmers L. (JulyAugust 1966). "Silphium". Bulletin of the History of Medicine 40 (4): 295313. PMID 5912906. 
Koerper, Henry; A. L. Kolls (AprilJune 1999). "The Silphium Motif Adorning Ancient Libyan Coinage: Marketing a Medicinal Plant". Economic Botany 53 (2): 133143. doi:10.1007/BF02866492. 
Riddle, John M. (1997). Eve's Herbs: A History of Contraception and Abortion in the West. Cambridge: Harvard University Press. pp. 4446. ISBN 0-674-27024-X. 
Riddle, John M.; J. Worth Estes; Josiah C. Russell (MarchApril 1994). "Birth Control in the Ancient World". Archaeology 47 (2): 2935. 
Tameanko, M. (April 1992). "The Silphium Plant: Wonder Drug of the Ancient World Depicted on Coins". Celator 6 (4): 2628. 
Tatman, J. L. (October 2000). "Silphium, Silver and Strife: A History of Kyrenaika and Its Coinage". Celator 14 (10): 624. 
Wright, W. S. (February 2001). "Silphium Rediscovered". Celator 15 (2): 2324. 
William Turner, A New Herball (1551, 1562, 1568)


== External linksEdit ==
Contraception In Ancient Times: Use of Morning-After Pill by David W. Tschanz
Silphion at Gernot Katzer's Spice Pages
The Secret of the Heart