In physics, natural units are physical units of measurement based only on universal physical constants. For example, the elementary charge e is a natural unit of electric charge, and the speed of light c is a natural unit of speed. A purely natural system of units has all of its units defined in this way, and usually such that the numerical values of the selected physical constants in terms of these units are exactly 1. These constants are then typically omitted from mathematical expressions of physical laws, and while this has the apparent advantage of simplicity, it may entail a loss of clarity due to the loss of information for dimensional analysis.


== Introduction ==
Natural units are intended to elegantly simplify particular algebraic expressions appearing in the laws of physics or to normalize some chosen physical quantities that are properties of universal elementary particles and are reasonably believed to be constant. However, there is a choice of which quantities to set to unity in a natural system of units, and quantities which are set to unity in one system may take a different value or even be assumed to vary in another natural unit system.
Natural units are "natural" because the origin of their definition comes only from properties of nature and not from any human construct. Planck units are often, without qualification, called "natural units", although they constitute only one of several systems of natural units, albeit the best known such system. Planck units (up to a simple multiplier for each unit) might be considered one of the most "natural" systems in that the set of units is not based on properties of any prototype, object, or particle but are solely derived from the properties of free space.
As with other systems of units, the base units of a set of natural units will include definitions and values for length, mass, time, temperature, and electric charge (in lieu of electric current). Some physicists do not recognize temperature as a fundamental physical quantity, since it expresses the energy per degree of freedom of a particle, which can be expressed in terms of energy (or mass, length, and time). Virtually every system of natural units normalizes Boltzmann's constant kB to 1, which can be thought of as simply a way of defining the unit temperature.
In the SI unit system, electric charge is a separate fundamental dimension of physical quantity, but in natural unit systems charge is expressed in terms of the mechanical units of mass, length, and time, similarly to cgs. There are two common ways to relate charge to mass, length, and time: In LorentzHeaviside units (also called "rationalized"), Coulomb's law is F = q1q2/4r2, and in Gaussian units (also called "non-rationalized"), Coulomb's law is F = q1q2/r2. Both possibilities are incorporated into different natural unit systems.


== Notation and use ==
Natural units are most commonly used by setting the units to one. For example, many natural unit systems include the equation c = 1 in the unit-system definition, where c is the speed of light. If a velocity v is half the speed of light, then as v = c/2 and c = 1, hence v = 1/2. The equation v = 1/2 means "the velocity v has the value one-half when measured in Planck units", or "the velocity v is one-half the Planck unit of velocity".
The equation c = 1 can be plugged in anywhere else. For example, Einstein's equation E = mc2 can be rewritten in Planck units as E = m. This equation means "The energy of a particle, measured in Planck units of energy, equals the mass of the particle, measured in Planck units of mass."


=== Advantages and disadvantages ===
Compared to SI or other unit systems, natural units have both advantages and disadvantages:
Simplified equations: By setting constants to 1, equations containing those constants appear more compact and in some cases may be simpler to understand. For example, the special relativity equation E2 = p2c2 + m2c4 appears somewhat complicated, but the natural units version, E2 = p2 + m2, appears simpler.
Physical interpretation: Natural unit systems automatically subsume dimensional analysis. For example, in Planck units, the units are defined by properties of quantum mechanics and gravity. Not coincidentally, the Planck unit of length is approximately the distance at which quantum gravity effects become important. Likewise, atomic units are based on the mass and charge of an electron, and not coincidentally the atomic unit of length is the Bohr radius describing the orbit of the electron in a hydrogen atom.
No prototypes: A prototype is a physical object that defines a unit, such as the International Prototype Kilogram, a physical cylinder of metal whose mass is by definition exactly one kilogram. A prototype definition always has imperfect reproducibility between different places and between different times, and it is an advantage of natural unit systems that they use no prototypes. (They share this advantage with other non-natural unit systems, such as conventional electrical units.)
Less precise measurements: SI units are designed to be used in precision measurements. For example, the second is defined by an atomic transition frequency in cesium atoms, because this transition frequency can be precisely reproduced with atomic clock technology. Natural unit systems are generally not based on quantities that can be precisely reproduced in a lab. Therefore, in order to retain the same degree of precision, the fundamental constants used still have to be measured in a laboratory in terms of physical objects that can be directly observed. If this is not possible, then a quantity expressed in natural units can be less precise than the same quantity expressed in SI units. For example, Planck units use the gravitational constant G, which is measurable in a laboratory only to four significant digits.


== Choosing constants to normalize ==
Out of the many physical constants, the designer of a system of natural unit systems must choose a few of these constants to normalize (set equal to 1). It is not possible to normalize just any set of constants. For example, the mass of a proton and the mass of an electron cannot both be normalized: if the mass of an electron is defined to be 1, then the mass of a proton has to be approximately 1836. In a less trivial example, the fine-structure constant,   1/137, cannot be set to 1, at least not independently, because it is a dimensionless number defined in terms of other quantities, some of which one may want to set to unity as well. The fine-structure constant is related to other fundamental constants through  = kee2/c, where ke is the Coulomb constant, e is the elementary charge,  is the reduced Planck constant, and c is the speed of light.


== Electromagnetism units ==

In SI units, electric charge is expressed in coulombs, a separate unit which is additional to the "mechanical" units (mass, length, time), even though the traditional definition of the ampere refers to some of these other units. In natural unit systems, however, electric charge has units of [mass]12 [length]32 [time]1.
There are two main natural unit systems for electromagnetism:
LorentzHeaviside units (classified as a rationalized system of electromagnetism units).
Gaussian units (classified as a non-rationalized system of electromagnetism units).
Of these, LorentzHeaviside is somewhat more common, mainly because Maxwell's equations are simpler in Lorentz-Heaviside units than they are in Gaussian units.
In the two unit systems, the elementary charge e satisfies:
e = 4c (LorentzHeaviside),
e = c (Gaussian)
where  is the reduced Planck constant, c is the speed of light, and   1/137 is the fine-structure constant.
In a natural unit system where c = 1, LorentzHeaviside units can be derived from SI units by setting 0 = 0 = 1. Gaussian units can be derived from SI units by a more complicated set of transformations, such as multiplying all electric fields by (40)12, multiplying all magnetic susceptibilities by 4, and so on.


== Systems of natural units ==


=== Planck units ===

Planck units are defined by
c =  = G = ke = kB = 1,
where c is the speed of light,  is the reduced Planck constant, G is the gravitational constant, ke is the Coulomb constant, and kB is the Boltzmann constant.
Planck units are a system of natural units that is not defined in terms of properties of any prototype, physical object, or even elementary particle. They only refer to the basic structure of the laws of physics: c and G are part of the structure of spacetime in general relativity, and  captures the relationship between energy and frequency which is at the foundation of quantum mechanics. This makes Planck units particularly useful and common in theories of quantum gravity, including string theory.
Planck units may be considered "more natural" even than other natural unit systems discussed below, as Planck units are not based on any arbitrarily chosen prototype object or particle. For example, some other systems use the mass of an electron as a parameter to be normalized. But the electron is just one of 16 known massive elementary particles, all with different masses, and there is no compelling reason, within fundamental physics, to emphasize the electron mass over some other elementary particle's mass.


=== Stoney units ===

Stoney units are defined by:
c = G = ke = e = kB = 1,
where c is the speed of light, G is the gravitational constant, ke is the Coulomb constant, e is the elementary charge, and kB is the Boltzmann constant.
George Johnstone Stoney was the first physicist to introduce the concept of natural units. He presented the idea in a lecture entitled "On the Physical Units of Nature" delivered to the British Association in 1874. Stoney units differ from Planck units by fixing the elementary charge at 1, instead of Planck's constant (only discovered after Stoney's proposal).
Stoney units are rarely used in modern physics for calculations, but they are of historical interest.


=== Atomic units ===

There are two types of atomic units, closely related.
Hartree atomic units:
e = me =  = ke = kB = 1
c = 1/
Rydberg atomic units:
e/2 = 2me =  = ke = kB = 1
c = 2/
Coulomb's constant is generally expressed as
ke = 1/40.
These units are designed to simplify atomic and molecular physics and chemistry, especially the hydrogen atom, and are widely used in these fields. The Hartree units were first proposed by Douglas Hartree, and are more common than the Rydberg units.
The units are designed especially to characterize the behavior of an electron in the ground state of a hydrogen atom. For example, using the Hartree convention, in the Bohr model of the hydrogen atom, an electron in the ground state has orbital velocity = 1, orbital radius = 1, angular momentum = 1, ionization energy = 1/2, etc.
The unit of energy is called the Hartree energy in the Hartree system and the Rydberg energy in the Rydberg system. They differ by a factor of 2. The speed of light is relatively large in atomic units (137 in Hartree or 274 in Rydberg), which comes from the fact that an electron in hydrogen tends to move much slower than the speed of light. The gravitational constant is extremely small in atomic units (around 1045), which comes from the fact that the gravitational force between two electrons is far weaker than the Coulomb force. The unit length, lA, is the Bohr radius, a0.
The values of c and e shown above imply that e = c, as in Gaussian units, not LorentzHeaviside units. However, hybrids of the Gaussian and LorentzHeaviside units are sometimes used, leading to inconsistent conventions for magnetism-related units.


=== Quantum chromodynamics (QCD) units ===
c = mp =  = kB = 1
The electron mass is replaced with that of the proton. Strong units are "convenient for work in QCD and nuclear physics, where quantum mechanics and relativity are omnipresent and the proton is an object of central interest".


=== "Natural units" (particle physics and cosmology) ===
In particle physics and cosmology, the phrase "natural units" generally means:
 = c = kB = 1.
where  is the reduced Planck constant, c is the speed of light, and kB is the Boltzmann constant.
Both Planck units and QCD units are this type of Natural units. Like the other systems, the electromagnetism units can be based on either LorentzHeaviside units or Gaussian units. The unit of charge is different in each.
Finally, one more unit is needed to construct a usable system of units that includes energy and mass. Most commonly, electron-volt (eV) is used, despite the fact that this is not a "natural" unit in the sense discussed above  it is defined by a natural property, the elementary charge, and the anthropogenic unit of electric potential, the volt. (The SI prefixed multiples of eV are used as well: keV, MeV, GeV, etc.)
With the addition of eV (or any other auxiliary unit with the proper dimension), any quantity can be expressed. For example, a distance of 1.0 cm can be expressed in terms of eV, in natural units, as:
1.0 cm = 1.0 cm/c  51000 eV1


=== Geometrized units ===

c = G = 1
The geometrized unit system, used in general relativity, is not a completely defined system. In this system, the base physical units are chosen so that the speed of light and the gravitational constant are set equal to unity. Other units may be treated however desired. Planck units and Stoney units are examples of geometrized unit systems.


=== Summary table ===
where:
 is the fine-structure constant, (e/qPlanck)2  0.007297,
G is the gravitational coupling constant, (me/mPlanck)2  69551752000000000001.7521045,


== See also ==


== Notes and references ==


== External links ==
The NIST website (National Institute of Standards and Technology) is a convenient source of data on the commonly recognized constants.
K.A. Tomilin: NATURAL SYSTEMS OF UNITS; To the Centenary Anniversary of the Planck System A comparative overview/tutorial of various systems of natural units having historical use.
Pedagogic Aides to Quantum Field Theory Click on the link for Chap. 2 to find an extensive, simplified introduction to natural units.