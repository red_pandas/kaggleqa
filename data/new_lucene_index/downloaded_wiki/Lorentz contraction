Length contraction is the phenomenon of a decrease in length of an object as measured by an observer which is traveling at any non-zero velocity relative to the object. This contraction (more formally called Lorentz contraction or LorentzFitzGerald contraction after Hendrik Lorentz and George FitzGerald) is usually only noticeable at a substantial fraction of the speed of light. Length contraction is only in the direction parallel to the direction in which the observed body is travelling. This effect is negligible at everyday speeds, and can be ignored for all regular purposes. Only at greater speeds does it become relevant. At a speed of 13,400,000 m/s (30 million mph, 0.0447c) contracted length is 99.9% of the length at rest; at a speed of 42,300,000 m/s (95 million mph, 0.141c), the length is still 99%. As the magnitude of the velocity approaches the speed of light, the effect becomes dominant, as can be seen from the formula:

where
L0 is the proper length (the length of the object in its rest frame),
L is the length observed by an observer in relative motion with respect to the object,
v is the relative velocity between the observer and the moving object,
c is the speed of light,
and the Lorentz factor, (v), is defined as
.
In this equation it is assumed that the object is parallel with its line of movement. For the observer in relative movement, the length of the object is measured by subtracting the simultaneously measured distances of both ends of the object. For more general conversions, see the Lorentz transformations. An observer at rest viewing an object travelling very close to the speed of light would observe the length of the object in the direction of motion as very near zero.


== History ==

Length contraction was postulated by George FitzGerald (1889) and Hendrik Antoon Lorentz (1892) to explain the negative outcome of the Michelson-Morley experiment and to rescue the hypothesis of the stationary aether (LorentzFitzGerald contraction hypothesis). Although both FitzGerald and Lorentz alluded to the fact that electrostatic fields in motion were deformed ("Heaviside-Ellipsoid" after Oliver Heaviside, who derived this deformation from electromagnetic theory in 1888), it was considered an ad hoc hypothesis, because at this time there was no sufficient reason to assume that intermolecular forces behave the same way as electromagnetic ones. In 1897 Joseph Larmor developed a model in which all forces are considered to be of electromagnetic origin, and length contraction appeared to be a direct consequence of this model. Yet it was shown by Henri Poincar (1905) that electromagnetic forces alone cannot explain the electron's stability. So he had to introduce another ad hoc hypothesis: non-electric binding forces (Poincar stresses) that ensure the electron's stability, give a dynamical explanation for length contraction, and thus hide the motion of the stationary aether.
Eventually, Albert Einstein (1905) was the first to completely remove the ad hoc character from the contraction hypothesis, by demonstrating that this contraction did not require motion through a supposed aether, but could be explained using special relativity, which changed our notions of space, time, and simultaneity. Einstein's view was further elaborated by Hermann Minkowski, who demonstrated the geometrical interpretation of all relativistic effects by introducing his concept of four-dimensional spacetime.


== Basis in relativity ==

First it is necessary to carefully consider the methods for measuring the lengths of resting and moving objects. Here, "object" simply means a distance with endpoints that are always mutually at rest, i.e., that are at rest in the same inertial frame of reference. If the relative velocity between an observer (or his measuring instruments) and the observed object is zero, then the proper length  of the object can simply be determined by directly superposing a measuring rod. However, if the relative velocity > 0, then one can proceed as follows:
The observer installs a row of clocks that either are synchronized a) by exchanging light signals according to the Poincar-Einstein synchronization, or b) by "slow clock transport", that is, one clock is transported along the row of clocks in the limit of vanishing transport velocity. Now, when the synchronization process is finished, the object is moved along the clock row and every clock stores the exact time when the left or the right end of the object passes by. After that, the observer only has to look after the position of a clock A that stored the time when the left end of the object was passing by, and a clock B at which the right end of the object was passing by at the same time. It's clear that distance AB is equal to length  of the moving object. Using this method, the definition of simultaneity is crucial for measuring the length of moving objects.
Another method is to use a clock indicating its proper time , which is traveling from one endpoint of the rod to the other in time  as measured by clocks in the rod's rest frame. The length of the rod can be computed by multiplying its travel time by its velocity, thus  in the rod's rest frame or  in the clock's rest frame.
In Newtonian mechanics, simultaneity and time duration are absolute and therefore both methods lead to the equality of  and . Yet in relativity theory the constancy of light velocity in all inertial frames in connection with relativity of simultaneity and time dilation destroys this equality. In the first method an observer in one frame claims to have measured the object's endpoints simultaneously, but the observers in all other inertial frames will argue that the object's endpoints were not measured simultaneously. In the second method, times  and  are not equal due to time dilation, resulting in different lengths too.
The deviation between the measurements in all inertial frames is given by the formulas for Lorentz transformation and time dilation (see Derivation). It turns out, that the proper length remains unchanged and always denotes the greatest length of an object, yet the length of the same object as measured in another inertial frame is shorter than the proper length. This contraction only occurs in the line of motion, and can be represented by the following relation (where  is the relative velocity and  the speed of light)


== Symmetry ==

The principle of relativity (according to which the laws of nature must assume the same form in all inertial reference frames) requires that length contraction is symmetrical: If a rod rests in inertial frame S, it has its proper length in S and its length is contracted in S'. However, if a rod rests in S', it has its proper length in S' and its length is contracted in S. This can be vividly illustrated using symmetric Minkowski diagrams (or Loedel diagrams), because the Lorentz transformation geometrically corresponds to a rotation in four-dimensional spacetime.
First image: If a rod at rest in S' is given, then its endpoints are located upon the ct' axis and the axis parallel to it. In this frame the simultaneous (parallel to the axis of x') positions of the endpoints are O and B, thus the proper length is given by OB. But in S the simultaneous (parallel to the axis of x) positions are O and A, thus the contracted length is given by OA.
On the other hand, if another rod is at rest in S, then its endpoints are located upon the ct axis and the axis parallel to it. In this frame the simultaneous (parallel to the axis of x) positions of the endpoints are O and D, thus the proper length is given by OD. But in S' the simultaneous (parallel to the axis of x') positions are O and C, thus the contracted length is given by OC.
Second image: A train at rest in S and a station at rest in S' with relative velocity of  are given. In S a rod with proper length  is located, so its contracted length  in S' is given by:

Then the rod will be thrown out of the train in S and will come to rest at the station in S'. Its length has to be measured again according to the methods given above, and now the proper length  will be measured in S' (the rod has become larger in that system), while in S the rod is in motion and therefore its length is contracted (the rod has become smaller in that system):


== Experimental verifications ==

Any observer co-moving with the observed object cannot measure the object's contraction, because he can judge himself and the object as at rest in the same inertial frame in accordance with the principle of relativity (as it was demonstrated by the Trouton-Rankine experiment). So Length contraction cannot be measured in the object's rest frame, but only in a frame in which the observed object is in motion. In addition, even in such a non-co-moving frame, direct experimental confirmations of Length contraction are hard to achieve, because at the current state of technology, objects of considerable extension cannot be accelerated to relativistic speeds. And the only objects traveling with the speed required are atomic particles, yet whose spatial extensions are too small to allow a direct measurement of contraction.
However, there are indirect confirmations of this effect in a non-co-moving frame:
It was the negative result of a famous experiment, that required the introduction of length contraction: the Michelson-Morley experiment (and later also the KennedyThorndike experiment). In special relativity its explanation is as follows: In its rest frame the interferometer can be regarded as at rest in accordance with the relativity principle, so the propagation time of light is the same in all directions. Although in a frame in which the interferometer is in motion, the transverse beam must traverse a longer, diagonal path with respect to the non-moving frame thus making its travel time longer, the factor by which the longitudinal beam would be delayed by taking times L/(c-v) & L/(c+v) for the forward and reverse trips respectively is even longer. Therefore, in the longitudinal direction the interferometer is supposed to be contracted, in order to restore the equality of both travel times in accordance with the negative experimental result(s). Thus the two-way speed of light remains constant and the round trip propagation time along perpendicular arms of the interferometer is independent of its motion & orientation.

The range of action of muons at high velocities is much higher than that of slower ones. The atmosphere has its proper length in the Earth frame, while the increased muon range is explained by their longer lifetimes due to time dilation (see Time dilation of moving particles). However, in the muon frame their lifetime is unchanged but the atmosphere is contracted so that even their small range is sufficient to reach the surface of earth.
Heavy ions that are spherical when at rest should assume the form of "pancakes" or flat disks when traveling nearly at the speed of light. And in fact, the results obtained from particle collisions can only be explained when the increased nucleon density due to length contraction is considered.
The ionization ability of electrically charged particles with large relative velocities is higher than expected. In pre-relativistic physics the ability should decrease at high velocities, because the time in which ionizing particles in motion can interact with the electrons of other atoms or molecules is diminished. Though in relativity, the higher-than-expected ionization ability can be explained by length contraction of the Coulomb field in frames in which the ionizing particles are moving, which increases their electrical field strength normal to the line of motion.
In free-electron lasers, relativistic electrons were injected into an undulator, so that synchrotron radiation is generated. In the proper frame of the electrons, the undulator is contracted which leads to an increased radiation frequency. Additionally, to find out the frequency as measured in the laboratory frame, one has to apply the relativistic Doppler effect. So, only with the aid of length contraction and the relativistic Doppler effect, the extremely small wavelength of undulator radiation can be explained.


== Reality of length contraction ==

In 1911 Vladimir Variak asserted that length contraction is "real" according to Lorentz, while it is "apparent or subjective" according to Einstein. Einstein replied:

The author unjustifiably stated a difference of Lorentz's view and that of mine concerning the physical facts. The question as to whether length contraction really exists or not is misleading. It doesn't "really" exist, in so far as it doesn't exist for a comoving observer; though it "really" exists, i.e. in such a way that it could be demonstrated in principle by physical means by a non-comoving observer.

Einstein also argued in that paper, that length contraction is not simply the product of arbitrary definitions concerning the way clock regulations and length measurements are performed. He presented the following thought experiment: Let A'B' and A"B" be the endpoints of two rods of the same proper length. Let them move in opposite directions at the same speed with respect to a resting coordinate x-axis. Endpoints A'A" meet at point A*, and B'B" meet at point B*, both points being marked on that axis. Einstein pointed out that length A*B* is shorter than A'B' or A"B", which can also be demonstrated by one of the rods when brought to rest with respect to that axis.


== Paradoxes ==
Due to superficial application of the contraction formula some paradoxes can occur. For examples see the Ladder paradox or Bell's spaceship paradox. However, those paradoxes can simply be solved by a correct application of relativity of simultaneity. Another famous paradox is the Ehrenfest paradox, which proves that the concept of rigid bodies is not compatible with relativity, reducing the applicability of Born rigidity, and showing that for a co-rotating observer the geometry is in fact non-euclidean.


== Visual effects ==

Length contraction refers to measurements of position made at simultaneous times according to a coordinate system. This could suggest that if one could take a picture of a fast moving object, that the image would show the object contracted in the direction of motion. However, such visual effects are completely different measurements, as such a photograph is taken from a distance, while length contraction can only directly be measured at the exact location of the object's endpoints. It was shown by several authors such as Roger Penrose and James Terrell that moving objects generally do not appear length contracted on a photograph. For instance, for a small angular diameter, a moving sphere remains circular and is rotated. This kind of visual rotation effect is called Penrose-Terrell rotation.


== Derivation ==


=== Lorentz transformation ===
Length contraction can be derived from the Lorentz transformation in several ways:


==== Moving length is known ====
In an inertial reference frame S,  and  shall denote the endpoints of an object in motion in this frame. There, its length  was measured according to the above convention by determining the simultaneous positions of its endpoints at . Now, the proper length of this object in S' shall be calculated by using the Lorentz transformation. Transforming the time coordinates from S into S' results in different times, but this is not problematic, as the object is at rest in S' where it does not matter when the endpoints are measured. Therefore, the transformation of the spatial coordinates suffices, which gives:

Since , and by setting  and , the proper length in S' is given by

with respect to which the measured length in S is contracted by

According to the relativity principle, objects that are at rest in S have to be contracted in S' as well. By exchanging the above signs and primes symmetrically, it follows:

Thus the contracted length as measured in S' is given by:


==== Proper length is known ====
Conversely, if the object rests in S and its proper length is known, the simultaneity of the measurements at the object's endpoints has to be considered in another frame S', as the object constantly changes its position there. Therefore, both spatial and temporal coordinates must be transformed:

With  and  this results in non-simultaneous differences:

In order to obtain the simultaneous positions of both endpoints, the distance traveled by the second endpoint with  during  must be subtracted from :

So the moving length in S' is contracted. Likewise, the preceding calculation gives a symmetric result for an object at rest in S':
.


=== Time dilation ===
Length contraction can also be derived from time dilation, according to which the rate of a single "moving" clock (indicating its proper time ) is lower with respect to two synchronized "resting" clocks (indicating ). Time dilation was experimentally confirmed multiple times, and is represented by the relation:
.
Suppose a rod of proper length  at rest in  and a clock at rest in  are moving along each other. The respective travel times of the clock between the rod's endpoints are given by  in  and  in , thus  and . By inserting the time dilation formula, the ratio between those lengths is:
.
Therefore, the length measured in  is given by
.
So the effect that the moving clock indicates a lower travel time in  due to time dilation, is interpreted in  as due to length contraction of the moving rod. Likewise, if the clock were at rest in  and the rod in , the above procedure would give
.


=== Geometrical considerations ===

Additional geometrical considerations show, that length contraction can be regarded as a trigonometric phenomenon, with analogy to parallel slices through a cuboid before and after a rotation in E3 (see left half figure at the right). This is the Euclidean analog of boosting a cuboid in E1,2. In the latter case, however, we can interpret the boosted cuboid as the world slab of a moving plate.
Image: Left: a rotated cuboid in three-dimensional euclidean space E3. The cross section is longer in the direction of the rotation than it was before the rotation. Right: the world slab of a moving thin plate in Minkowski spacetime (with one spatial dimension suppressed) E1,2, which is a boosted cuboid. The cross section is thinner in the direction of the boost than it was before the boost. In both cases, the transverse directions are unaffected and the three planes meeting at each corner of the cuboids are mutually orthogonal (in the sense of E1,2 at right, and in the sense of E3 at left).
In special relativity, Poincar transformations are a class of affine transformations which can be characterized as the transformations between alternative Cartesian coordinate charts on Minkowski spacetime corresponding to alternative states of inertial motion (and different choices of an origin). Lorentz transformations are Poincar transformations which are linear transformations (preserve the origin). Lorentz transformations play the same role in Minkowski geometry (the Lorentz group forms the isotropy group of the self-isometries of the spacetime) which are played by rotations in euclidean geometry. Indeed, special relativity largely comes down to studying a kind of noneuclidean trigonometry in Minkowski spacetime, as suggested by the following table:


== References ==


== External links ==
Physics FAQ: Can You See the LorentzFitzgerald Contraction? Or: Penrose-Terrell Rotation; The Barn and the Pole