Ephedrine is a medication used to prevent low blood pressure during spinal anesthesia. It has also be used for asthma, narcolepsy, and obesity but is not the preferred treatment. It can be taken by mouth or by injection into a muscle, vein, or just under the skin. Onset with intravenous use is fast, while injection into a muscle can take 20 minutes, and by mouth can take an hour for effect. When given by injection it lasts about an hour and when taken by mouth it can last up to four hours.
Common side effects include, trouble sleeping, anxiety, headache, hallucinations, high blood pressure, fast heart rate, loss of appetite, and inability to urinate. Serious side effects include stroke, heart attack, and abuse. While likely okay in pregnancy its use in this population is poorly studied. Use during breastfeeding is not recommended. Ephedrine works by turning on alpha adrenergic receptors and beta adrenergic receptors.
Ephedrine was first isolated in 1885. It is on the WHO Model List of Essential Medicines, the most important medications needed in a basic health system. It is available as a generic medication. The wholesale cost is about 0.69 to 1.35 USD per dose. In the United States it is not very expensive. It can normally be found in plants of the Ephedra type. Dietary supplements that contain ephedrine are illegal in the United States. An exception is when used in traditional Chinese medicine.


== Medical use ==

Both ephedrine and pseudoephedrine increase blood pressure and act as bronchodilators, with pseudoephedrine having considerably less effect. 


=== Weight loss ===
Ephedrine promotes modest short-term weight loss, specifically fat loss, but its long-term effects are unknown. In mice ephedrine is known to stimulate thermogenesis in the brown adipose tissue, but because adult humans have only small amounts of brown fat, thermogenesis is assumed to take place mostly in the skeletal muscle. Ephedrine also decreases gastric emptying. Methylxanthines such as caffeine and theophylline have a synergistic effect with ephedrine with respect to weight loss. This led to creation and marketing of compound products.


== Recreational use ==

As a phenethylamine, ephedrine has a similar chemical structure to amphetamines and is a methamphetamine analogue having the methamphetamine structure with a hydroxyl group at the  position. Because of ephedrine's structural similarity to methamphetamine, it can be used to create methamphetamine using chemical reduction in which ephedrine's hydroxyl group is removed; this has made ephedrine a highly sought-after chemical precursor in the illicit manufacture of methamphetamine. The most popular method for reducing ephedrine to methamphetamine is similar to the Birch reduction, in that it uses anhydrous ammonia and lithium metal in the reaction. The second-most popular method uses red phosphorus, iodine, and ephedrine in the reaction.
Through oxidation, ephedrine can be easily synthesized into methcathinone. Ephedrine is listed as a table-I precursor under the United Nations Convention Against Illicit Traffic in Narcotic Drugs and Psychotropic Substances.
One of them, known as the ECA stack, contains caffeine and aspirin besides ephedrine, and is a popular supplement taken by body builders to cut down body fat before a competition. It increases performance in athletes, and has a synergistic effect with caffeine.


=== Detection of use ===
Ephedrine may be quantified in blood, plasma, or urine to monitor possible abuse by athletes, confirm a diagnosis of poisoning, or assist in a medicolegal death investigation. Many commercial immunoassay screening tests directed at the amphetamines cross-react appreciably with ephedrine, but chromatographic techniques can easily distinguish ephedrine from other phenethylamine derivatives. Blood or plasma ephedrine concentrations are typically in the 20-200 g/l range in persons taking the drug therapeutically, 300-3000 g/l in abusers or poisoned patients and 320 mg/l in cases of acute fatal overdosage. The current WADA limit for ephedrine in an athlete's urine is 10 g/ml.


== Contraindications ==
Ephedrine should not be used in conjunction with certain antidepressants, namely norepinephrine-dopamine reuptake inhibitors (NDRIs), as this increases the risk of the above symptoms due to excessive serum levels of norepinephrine.
Bupropion is an example of an antidepressant with an amphetamine-like structure similar to ephedrine, and it is an NDRI. Its action bears more resemblance to amphetamine than to fluoxetine in that its primary mode of therapeutic action involves norepinephrine and to a lesser degree dopamine, but it also releases some serotonin from presynaptic clefts. It should not be used with ephedrine, as it may increase the likelihood of the above side effects.
Ephedrine should be used with caution in patients with inadequate fluid replacement, impaired adrenal function, hypoxia, hypercapnia, acidosis, hypertension, hyperthyroidism, prostatic hypertrophy, diabetes mellitus, cardiovascular disease, during delivery if maternal blood pressure is >130/80 mmHg, and lactation.
Contraindications for the use of ephedrine include: closed-angle glaucoma, phaeochromocytoma, asymmetric septal hypertrophy (idiopathic hypertrophic subaortic stenosis), concomitant or recent (previous 14 days) monoamine oxidase inhibitor (MAOI) therapy, general anaesthesia with halogenated hydrocarbons (particularly halothane), tachyarrhythmias or ventricular fibrillation, or hypersensitivity to ephedrine or other stimulants.
Ephedrine should not be used at any time during pregnancy unless specifically indicated by a qualified physician and only when other options are unavailable.


== Adverse effects ==
Ephedrine is a potentially dangerous natural compound; as of 2004 the US Food and Drug Administration had received over 18,000 reports of adverse effects in people using it.
Adverse drug reactions (ADRs) are more common with systemic administration (e.g. injection or oral administration) compared to topical administration (e.g. nasal instillations). ADRs associated with ephedrine therapy include:
Cardiovascular: tachycardia, cardiac arrhythmias, angina pectoris, vasoconstriction with hypertension
Dermatological: flushing, sweating, acne vulgaris
Gastrointestinal: nausea
Genitourinary: decreased urination due to vasoconstriction of renal arteries, difficulty urinating is not uncommon, as alpha-agonists such as ephedrine constrict the internal urethral sphincter, mimicking the effects of sympathetic nervous system stimulation
Nervous system: restlessness, confusion, insomnia, mild euphoria, mania/hallucinations (rare except in previously existing psychiatric conditions), delusions, formication (may be possible, but lacks documented evidence) paranoia, hostility, panic, agitation
Respiratory: dyspnea, pulmonary edema
Miscellaneous: dizziness, headache, tremor, hyperglycemic reactions, dry mouth
The neurotoxicity of l-ephedrine is disputed. 


== Other uses ==
In chemical synthesis, ephedrine is used in bulk quantities as a chiral auxiliary group. 

In saquinavir synthesis, the half-acid is resolved as its salt with l-ephedrine.


== Chemistry and nomenclature ==
Ephedrine is a sympathomimetic amine and substituted amphetamine. It is similar in molecular structure to phenylpropanolamine, methamphetamine, and epinephrine (adrenaline). Chemically, it is an alkaloid with a phenethylamine skeleton found in various plants in the genus Ephedra (family Ephedraceae). It works mainly by increasing the activity of norepinephrine (noradrenaline) on adrenergic receptors. It is most usually marketed as the hydrochloride or sulfate salt.
Ephedrine exhibits optical isomerism and has two chiral centres, giving rise to four stereoisomers. By convention, the pair of enantiomers with the stereochemistry (1R,2S and 1S,2R) is designated ephedrine, while the pair of enantiomers with the stereochemistry (1R,2R and 1S,2S) is called pseudoephedrine. Ephedrine is a substituted amphetamine and a structural methamphetamine analogue. It differs from methamphetamine only by the presence of a hydroxyl group (OH).
The isomer which is marketed is ()-(1R,2S)-ephedrine.
Ephedrine hydrochloride has a melting point of 187188 C.
In the outdated d/l system (+)-ephedrine is also referred to as l-ephedrine and ()-ephedrine as d-ephedrine (in the Fisher projection, then the phenyl ring is drawn at bottom).
Often, the d/l system (with small caps) and the d/l system (with lower-case) are confused. The result is that the levorotary l-ephedrine is wrongly named l-ephedrine and the dextrorotary d-pseudoephedrine (the diastereomer) wrongly d-pseudoephedrine.
The IUPAC names of the two enantiomers are (1R,2S)- respectively (1S,2R)-2-methylamino-1-phenylpropan-1-ol. A synonym is erythro-ephedrine.


== Sources ==


=== Agricultural ===
Ephedrine is obtained from the plant Ephedra sinica and other members of the Ephedra genus. Raw materials for the manufacture of ephedrine and traditional Chinese medicines are produced in China on a large scale. As of 2007, companies produced for export US$13 million worth of ephedrine from 30,000 tons of ephedra annually, 10 times the amount used in traditional Chinese medicine.


=== Synthetic ===
Most of the l-ephedrine produced today for official medical use is made synthetically as the extraction and isolation process from E. sinica is tedious and no longer cost effective.


=== Biosynthetic ===
Ephedrine was long thought to come from modifying the amino acid L-Phenylalanine. L-Phenylalanine would be decarboxylated and subsequently attacked with -aminoacetophenone. Methylation of this product would then produce ephedrine. This pathway has since been disproven. A new pathway proposed suggests that phenylalanine first forms cinnamoyl-CoA via the enzymes phenylalanine ammonia-lyase and acyl CoA ligase. The cinnamoyl-CoA is then reacted with a hydratase to attach the alcohol functional group. The product is then reacted with a retro-aldolase, forming benzaldehyde. Benzaldehyde reacts with pyruvic acid to attach a 2 carbon unit. This product then undergoes transamination and methylation to form ephedrine and its stereoisomer, pseudoephedrine.


== Mechanism of action ==
Ephedrine, a sympathomimetic amine, acts on part of the sympathetic nervous system (SNS). The principal mechanism of action relies on its indirect stimulation of the adrenergic receptor system by increasing the activity of norepinephrine at the postsynaptic - and -receptors. The presence of direct interactions with -receptors is unlikely, but still controversial. L-Ephedrine, and particularly its stereoisomer norpseudoephedrine (which is also present in Catha edulis) has indirect sympathomimetic effects and due to its ability to cross the blood-brain barrier, it is a CNS stimulant similar to amphetamines, but less pronounced, as it releases noradrenaline and dopamine in the substantia nigra.
The presence of an N-methyl group decreases binding affinities at -receptors, compared with norephedrine. Ephedrine, though, binds better than N-methylephedrine, which has an additional methyl group at the N-atom. Also the steric orientation of the hydroxyl group is important for receptor binding and functional activity.
Compounds with decreasing -receptor affinity


== History ==
Ephedrine in its natural form, known as m hung () in traditional Chinese medicine, has been documented in China since the Han dynasty (206 BC  220 AD) as an antiasthmatic and stimulant. In 1885, the chemical synthesis of ephedrine was first accomplished by Japanese organic chemist Nagai Nagayoshi based on his research on traditional Japanese and Chinese herbal medicines. The industrial manufacture of ephedrine in China began in the 1920s, when Merck began marketing and selling the drug as ephetonin. Ephedrine exports between China and the West grew from 4 to 216 tonnes between 1926 and 1928.
In traditional Chinese medicine, m hung has been used as a treatment for asthma and bronchitis for centuries.


== Legality ==


=== Canada ===
In January 2002, Health Canada issued a voluntary recall of all ephedrine products containing more than 8 mg per dose, all combinations of ephedrine with other stimulants such as caffeine, and all ephedrine products marketed for weight-loss or bodybuilding indications, citing a serious risk to health. Ephedrine is still sold as an oral nasal decongestant in 8 mg pills, OTC.


=== United States ===
In 1997, the FDA proposed a regulation on ephedra (the herb from which ephedrine is obtained), which limited an ephedra dose to 8 mg (of active ephedrine) with no more than 24 mg per day. This proposed rule was withdrawn, in part, in 2000 because of "concerns regarding the agency's basis for proposing a certain dietary ingredient level and a duration of use limit for these products." In 2004, the FDA created a ban on ephedrine alkaloids marketed for reasons other than asthma, colds, allergies, other disease, or traditional Asian use. On April 14, 2005, the U.S. District Court for the District of Utah ruled the FDA did not have proper evidence that low dosages of ephedrine alkaloids are actually unsafe, but on August 17, 2006, the U.S. Court of Appeals for the Tenth Circuit in Denver upheld the FDA's final rule declaring all dietary supplements containing ephedrine alkaloids adulterated, and therefore illegal for marketing in the United States. Furthermore, ephedrine is banned by the NCAA, MLB, NFL, and PGA. Ephedrine is, however, still legal in many applications outside of dietary supplements. Purchasing is currently limited and monitored, with specifics varying from state to state.
The House passed the Combat Methamphetamine Epidemic Act of 2005 as an amendment to the renewal of the USA PATRIOT Act. Signed into law by President George W. Bush on March 6, 2006, the act amended the US Code (21 USC 830) concerning the sale of ephedrine-containing products. The federal statute included these requirements for merchants who sell these products:
A retrievable record of all purchases identifying the name and address of each party to be kept for two years
Required verification of proof of identity of all purchasers
Required protection and disclosure methods in the collection of personal information
Reports to the Attorney General of any suspicious payments or disappearances of the regulated products
Non-liquid dose form of regulated product may only be sold in unit-dose blister packs
Regulated products are to be sold behind the counter or in a locked cabinet in such a way as to restrict access
Daily sales of regulated products not to exceed 3.6 g without regard to the number of transactions
Monthly sales not to exceed 9 g of pseudoephedrine base in regulated products
The law gives similar regulations to mail-order purchases, except the monthly sales limit is only 7.5 g.
As a pure herb or tea, m hung, containing ephedrine, is still sold legally in the USA. The law restricts/prohibits its being sold as a dietary supplement (pill) or as an ingredient/additive to other products, like diet pills.


=== Australia ===
All Ephedra spp and Ephedrine itself are considered schedule 4 substances under the Poisons Standard (October 2015). A schedule 4 drug is considered a Prescription Only Medicine, or Prescription Animal Remedy  Substances, the use or supply of which should be by or on the order of persons permitted by State or Territory legislation to prescribe and should be available from a pharmacist on prescription under the Poisons Standard (October 2015).


=== South Africa ===
In South Africa, ephedrine was moved to schedule 6 on 27 May 2008, which makes the substance legal to possess but available by prescription only.


=== Germany ===
Ephedrine was freely available in pharmacies in Germany until 2001. Afterwards, access was restricted since it was mostly bought for unindicated uses. Similarly, ephedra can only be bought with a prescription. Since April 2006, all products, including plant parts, that contain ephedrine are only available with a prescription.


== See also ==
Amphetamine
Halostachine
Synephrine
Oxyfedrine
Metaraminol


== References ==