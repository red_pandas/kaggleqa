In introductory chemistry, the reactivity series or activity series is an empirical, calculated and a structurally analytical progression of a series of metals, according to their "reactivity" from highest to lowest. It is used to summarize information about the reactions of metals with acids and water, double displacement reactions and the extraction of metals from their ores.


== Table ==
Going from bottom to top the metals:
increase in reactivity;
lose electrons more readily to form positive ions;
corrode or tarnish more readily;
require more energy (and different methods) to be separated from their ores;
become stronger reducing agents (electron donors).


== Defining reactions ==
There is no unique and fully consistent way to define the reactivity series, but it is common to use the three types of reaction listed below, many of which can be performed in a high-school laboratory (at least as demonstrations).


=== Reaction with water and acids ===
The most reactive metals, such as sodium, will react with cold water to produce hydrogen and the metal hydroxide:
2 Na (s) + 2 H2O (l) 2 NaOH (aq) + H2 (g)
Metals in the middle of the reactivity series, such as iron, will react with acids such as sulfuric acid (but not water at normal temperatures) to give hydrogen and a metal salt, such as iron(II) sulfate:
Fe (s) + H2SO4 (l)  FeSO4 (aq) + H2 (g)
There is some ambiguity at the borderlines between the groups. Magnesium, aluminium and zinc can react with water, but the reaction is usually very slow unless the metal samples are specially prepared to remove the surface layer of oxide which protects the rest of the metal. Copper and silver will react with nitric acid; but because nitric acid is an oxidizing acid, the oxidizing agent is not the H+ ion as in normal acids, but the NO3 ion.


=== Single displacement reactions ===
An iron nail placed in a solution of copper sulphate will quickly change colour as metallic copper is deposited and the iron is converted into iron(II) sulfate:
Fe (s) + CuSO4 (aq)  Cu (s) + FeSO4 (aq)
In general, a metal can displace any of the metals which are lower in the reactivity series: the higher metal reduces the ions of the lower metal. This is used in the thermite reaction for preparing small quantities of metallic iron, and in the Kroll process for preparing titanium (Ti comes at about the same level as Al in the reactivity series). For example, aluminium will reduce iron(III) oxide to iron, becoming aluminium oxide in the process:
Al (s) + Fe2O3 (s)  Fe (s) + Al2O3 (s)
Similarly, magnesium can be used to extract titanium from titanium tetrachloride, forming magnesium chloride in the process:
2 Mg (s) + TiCl4 (l)  Ti (s) + 2 MgCl2 (s)
However, other factors can come into play, such as in the preparation of metallic potassium by the reduction of potassium chloride with sodium at 850 C. Although sodium is lower than potassium in the reactivity series, the reaction can proceed because potassium is more volatile, and is distilled off from the mixture.
Na (g) + KCl (l)  K (g) + NaCl (l)


== Comparison with standard electrode potentials ==
The reactivity series is sometimes quoted in the strict reverse order of standard electrode potentials, when it is also known as the "electrochemical series":
Li > K > Sr > Ca > Na > Mg > Al > Mn > Zn > Cr(+3) > Fe > Cd > Co > Ni > Sn > Pb > H > Cu > Ag > Hg > Pd > Ir > Pt > Au
The positions of lithium and sodium are changed on such a series; gold and platinum are also inverted, although this has little practical significance as both metals are highly unreactive.
Standard electrode potentials offer a quantitative measure of the power of a reducing agent, rather than the qualitative considerations of other reactivity series. However, they are only valid for standard conditions: in particular, they only apply to reactions in aqueous solution. Even with this proviso, the electrode potentials of lithium and sodium  and hence their positions in the electrochemical series  appear anomalous. The order of reactivity, as shown by the vigour of the reaction with water or the speed at which the metal surface tarnishes in air, appears to be
potassium > sodium > lithium > alkaline earth metals,
the same as the reverse order of the (gas-phase) ionization energies. This is borne out by the extraction of metallic lithium by the electrolysis of a eutectic mixture of lithium chloride and potassium chloride: lithium metal is formed at the cathode, not potassium.


== See also ==
Reactivity (chemistry), which discusses the inconsistent way that the term 'reactivity' is used in chemistry.


== References ==


== External links ==
Science Line Chemistry