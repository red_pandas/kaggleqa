The Strait of Hormuz /hrmuz/ Persian:   Tangeh-ye Hormoz  listen , Arabic:   Maq Hurmuz) is a strait between the Gulf of Oman and the Persian Gulf. It is the only sea passage from the Persian Gulf to the open ocean and is one of the world's most strategically important choke points. On the north coast is Iran, and on the south coast is the United Arab Emirates and Musandam, an exclave of Oman. At its narrowest, the strait is 29 nautical miles (54 km) wide.


== EtymologyEdit ==
The opening to the Persian Gulf was described, but not given a name, in the Periplus of the Erythraean Sea, a 1st-century mariner's guide:

"At the upper end of these Calaei islands is a range of mountains called Calon, and there follows not far beyond, the mouth of the Persian Gulf, where there is much diving for the pearl-mussel. To the left of the straits are great mountains called Asabon and to the right there rises in full view another round and high mountain called Semiramis; between them the passage across the strait is about six hundred stadia; beyond which that very great and broad sea, the Persian Gulf, reaches far into the interior. At the upper end of this gulf there is a market-town designated by law called Apologus, situated near Charaex Spasini and the River Euphrates."

In the 10th to 17th centuries AD, the Kingdom of Ormus, which seems to have given the strait its name, was located here. Scholars, historians and linguists derive the name "Ormuz" from the local Persian word  Hur-mogh meaning date palm. In the local dialects of Hurmoz and Minab this strait is still called Hurmogh and has the aforementioned meaning. The resemblance of this word with the name of the Persian god  Hormoz (a variant of Ahura Mazda) has resulted in the popular belief that these words are related.


== NavigationEdit ==
To reduce the risk of collision, ships moving through the Strait follow a Traffic Separation Scheme (TSS): inbound ships use one lane, outbound ships another, each lane being two miles wide. The lanes are separated by a two-mile-wide "median".
To traverse the Strait, ships pass through the territorial waters of Iran and Oman under the transit passage provisions of the United Nations Convention on the Law of the Sea. Although not all countries have ratified the convention, most countries, including the U.S., accept these customary navigation rules as codified in the Convention.
Oman has a radar site Link Quality Indicator (LQI) to monitor the TSS in the Strait of Hormuz. This site is on a small island on the peak of Musandam Peninsula.


== Traffic statisticsEdit ==
According to the U.S. Energy Information Administration, in the year 2011, an average of 14 tankers per day passed out of the Persian Gulf through the Strait carrying 17 million barrels (2,700,000 m3) of crude oil. This was said to represent 35% of the world's seaborne oil shipments and 20% of oil traded worldwide. The report stated that more than 85% of these crude oil exports went to Asian markets, with Japan, India, South Korea and China the largest destinations.
A 2007 report from the Center for Strategic and International Studies also stated that 17 million barrels passed out of the Gulf daily, but that oil flows through the Strait accounted for roughly 40% of all world-traded oil.


== EventsEdit ==


=== Operation Praying MantisEdit ===

On 18 April 1988, the U.S. Navy waged a one-day battle against Iranian forces in and around the strait. The battle, dubbed Operation Praying Mantis by the U.S., was launched in retaliation for the USS Samuel B. Roberts striking a mine laid in the channel by Iran on 14 April. U.S. forces sank one frigate, one gunboat, and up to six armed speedboats, as well as seriously damaging a second frigate.


=== The downing of Iran Air 655Edit ===

On 3 July 1988, 290 people were killed when an Iran Air Airbus A300 passenger jet was shot down over the strait by the United States Navy guided missile cruiser USS Vincennes when it was mistakenly identified as an attacking warplane.


=== Collision between USS Newport News and tanker MogamigawaEdit ===
On 8 January 2007, the nuclear submarine USS Newport News, traveling submerged, struck MV Mogamigawa, a 300,000-ton Japanese-flagged very large crude tanker, south of the strait. There were no injuries, and no oil leaked from the tanker.


=== Tensions in 2008Edit ===


==== 2008 US-Iranian naval disputeEdit ====

A series of naval stand-offs between Iranian speedboats and U.S. warships in the Strait of Hormuz occurred in December 2007 and January 2008. U.S. officials accused Iran of harassing and provoking their naval vessels, but Iranian officials denied the allegations. On 14 January 2008, U.S. Navy officials appeared to contradict the Pentagon version of the 16th of January event, in which the Pentagon had reported that U.S. vessels had almost fired on approaching Iranian boats. The Navy's regional commander, Vice Admiral Kevin Cosgriff, said the Iranians had "neither anti-ship missiles nor torpedoes" and he "wouldn't characterize the posture of the US 5th Fleet as afraid of these small boats".


==== Iranian defence policyEdit ====
On 29 June 2008, the commander of Iran's Revolutionary Guard, Mohammad Ali Jafari, said that if either Israel or the United States attacked Iran, it would seal off the Strait of Hormuz to wreak havoc in the oil markets. This followed more ambiguous threats from Iran's oil minister and other government officials that an attack on Iran would result in turmoil in the world's oil supply.
Vice Admiral Kevin Cosgriff, commander of the U.S. 5th Fleet stationed in Bahrain across the Persian Gulf from Iran, warned that such Iranian action by would be considered an act of war, and the U.S. would not allow Iran to hold hostage nearly a third of the world's oil supply.
On 8 July 2008, Ali Shirazi, a mid-level clerical aide to Iran's Supreme Leader Ayatollah Ali Khamenei, was quoted by the student news agency ISNA as telling the Revolutionary Guards, "The Zionist regime is pressuring White House officials to attack Iran. If they commit such a stupidity, Tel Aviv and U.S. shipping in the Persian Gulf will be Iran's first targets and they will be burned."


==== Naval activity in 2008Edit ====
In the last week of July 2008, in the Operation Brimstone, dozens of U.S. and foreign naval ships came to undergo joint exercises for possible military activity in the shallow waters off the coast of Iran.
As of 11 August 2008, more than 40 U.S. and allied ships reportedly were en route to the Strait of Hormuz. One U.S. carrier battle group from Japan would complement the two which are already in the Persian Gulf, for a total of five battle groups, not including the submarines.


=== Collision between USS Hartford and USS New OrleansEdit ===

On 20 March 2009, United States Navy Los Angeles-class submarine USS Hartford collided with the San Antonio-class amphibious transport dock USS New Orleans in the strait. The collision, which slightly injured 15 sailors aboard the Hartford, ruptured a fuel tank aboard the New Orleans, spilling 25,000 US gallons (95 m3) of marine diesel fuel.


=== U.S. Iran tensions in 20112012Edit ===

On 27 December 2011, Iranian Vice President Mohammad-Reza Rahimi threatened to cut off oil supply from the Strait of Hormuz should economic sanctions limit, or cut off, Iranian oil exports. A U.S. Fifth Fleet spokeswoman said in response that the Fleet was "always ready to counter malevolent actions", whilst Admiral Habibollah Sayyari of the Iranian navy claimed that cutting off oil shipments would be "easy". Despite an initial 2% rise in oil prices, oil markets ultimately did not react significantly to the Iranian threat, with oil analyst Thorbjoern Bak Jensen of Global Risk Management concluding that "they cannot stop the flow for a longer period due to the amount of U.S. hardware in the area".

On 3 January 2012, Iran threatened to take action if the U.S. Navy moves an aircraft carrier back into the Persian Gulf. Iranian Army chief Ataollah Salehi said the United States had moved an aircraft carrier out of the Gulf because of Iran's naval exercises, and Iran would take action if the ship returned. "Iran will not repeat its warning...the enemy's carrier has been moved to the Gulf of Oman because of our drill. I recommend and emphasize to the American carrier not to return to the Persian Gulf", he said.
The U.S. Navy spokesman Commander Bill Speaks quickly responded that deployment of U.S. military assets would continue as has been the custom stating: "The U.S. Navy operates under international maritime conventions to maintain a constant state of high vigilance in order to ensure the continued, safe flow of maritime traffic in waterways critical to global commerce."
While earlier statements from Iran had little effect on global oil markets, coupled with the new sanctions, these comments from Iran are driving crude futures higher, up over 4%. Pressure on prices reflect a combination of uncertainty driven further by China's recent response  reducing oil January 2012 purchases from Iran by 50% compared to those made in 2011.
The U.S. led sanctions may be "beginning to bite" as Iranian currency has recently lost some 12% of its value. Further pressure on Iranian currency was added by French Foreign Minister Alain Jupp who was quoted as calling for more "strict sanctions" and urged EU countries to follow the US in freezing Iranian central bank assets and imposing an embargo on oil exports.
On 7 January 2012, the British government announced that it would be sending the Type 45 destroyer HMS Daring to the Persian Gulf. Daring, which is the lead ship of her class is one of the "most advanced warships" in the world, and will undertake its first mission in the Persian Gulf. The British Government however have said that this move has been long-planned, as Daring will replace another Armilla patrol frigate.
On 9 January 2012, Iranian Defense Minister Ahmad Vahidi denied that Iran had ever claimed that it would close the Strait of Hormuz, saying that "the Islamic Republic of Iran is the most important provider of security in the strait... if one threatens the security of the Persian Gulf, then all are threatened."
The Iranian Foreign Ministry confirmed on 16 January 2012 that it has received a letter from the United States concerning the Strait of Hormuz, "via three different channels." Authorities were considering whether to reply, although the contents of the letter were not divulged. The United States had previously announced its intention to warn Iran that closing the Strait of Hormuz is a "red line" that would provoke an American response. Gen. Martin E. Dempsey, the chairman of the Joint Chiefs of Staff, said this past weekend that the United States would "take action and re-open the strait, which could be accomplished only by military means, including minesweepers, warship escorts and potentially airstrikes. Defense Secretary Leon E. Panetta told troops in Texas that the United States would not tolerate Iran's closing of the strait. Nevertheless, Iran continued to discuss the impact of shutting the Strait on world oil markets, saying that any disruption of supply would cause a shock to markets that "no country" could manage.
By 23 January, a flotilla had been established by countries opposing Iran's threats to close the Hormuz Strait. These ships operated in the Persian Gulf and Arabian Sea off the coast of Iran. The flotilla included three American aircraft carriers (the USS Carl Vinson, the USS Enterprise and USS Abraham Lincoln) and three destroyers (USS Momsen, USS Sterett, USS Halsey), seven British warships, including the destroyer HMS Daring and a number of Type 23 frigates (HMS Westminster, HMS Argyll, HMS Somerset and HMS St Albans), and a French warship, the frigate La Motte-Picquet .
On 24 January, tensions rose further after the European Union imposed sanctions on Iranian oil. A senior member of Iran's parliament said that the Islamic Republic would close the entry point to the Gulf if new sanctions block its oil exports. "If any disruption happens regarding the sale of Iranian oil, the Strait of Hormuz will definitely be closed," Mohammad Kossari, deputy head of parliament's foreign affairs and national security committee, told the semi-official Fars News Agency.


=== 2015 seizure of MV Maersk TigrisEdit ===

On April 28, 2015, IRGCN patrol boats contacted the Marshall Islands-flagged container ship Maersk Tigris, which was westbound through the strait, and directed the ship to proceed further into Iranian territorial waters, according to a spokesman for the U.S. Defense Department. When the ship's master declined, one of the Iranian craft fired shots across the bridge of the Maersk Tigris. The master complied and proceeded into Iranian waters near Larak Island. The U.S. Navy sent aircraft and a destroyer, USS Farragut, to monitor the situation.
Maersk says they have agreed to pay an Iranian company $163,000 over a dispute about 10 container boxes transported to Dubai in 2005. The court ruling allegedly ordered a fine of $3.6 million.


== Ability of Iran to hinder shippingEdit ==

Millennium Challenge 2002 was a major war game exercise conducted by the United States armed forces in 2002. According to a 2012 article in The Christian Science Monitor, it simulated an attempt by Iran to close the strait. The assumptions and results were controversial.
A 2008 article in International Security contended that Iran could seal off or impede traffic in the Strait for a month, and an attempt by the U.S. to reopen it would be likely to escalate the conflict. In a later issue, however, the journal published a response which questioned some key assumptions and suggested a much shorter timeline for re-opening.
In December 2011, Iran's navy began a ten-day exercise in international waters along the strait. The Iranian Navy Commander, Rear Admiral Habibollah Sayyari, stated that the strait would not be closed during the exercise; Iranian forces could easily accomplish that but such a decision must be made at a political level.
Captain John Kirby, a Pentagon spokesman, was quoted in a December 2011 Reuters article: "Efforts to increase tension in that part of the world are unhelpful and counter-productive. For our part, we are comfortable that we have in the region sufficient capabilities to honor our commitments to our friends and partners, as well as the international community." In the same article, Suzanne Maloney, an Iran expert at the Brookings Institution, said, "The expectation is that the U.S. military could address any Iranian threat relatively quickly."
General Martin Dempsey, Chairman of the Joint Chiefs of Staff, said in January 2012 that Iran "has invested in capabilities that could, in fact, for a period of time block the Strait of Hormuz." He also stated, "We've invested in capabilities to ensure that if that happens, we can defeat that."


== Alternative shipping routesEdit ==
In June 2012, Saudi Arabia reopened the Iraq Pipeline through Saudi Arabia (IPSA), which was confiscated from Iraq in 2001 and travels from Iraq across Saudi Arabia to a Red Sea port. It will have a capacity of 1.65 million barrels per day.
In July 2012, the UAE began using a new pipeline from the Habshan fields in Abu Dhabi to the Fujairah oil terminal on the Gulf of Oman, effectively bypassing the Strait of Hormuz. It was constructed by China and will have a maximum capacity of around 2 million barrels per day, over three-fourths of the UAE's 2012 production rate. The UAE is also increasing Fujairah's storage and off-loading capacities.
In a July 2012 Foreign Policy article, Gal Luft compared Iran and the Strait of Hormuz to the Ottoman Empire and the Dardanelles, a choke point for shipments of Russian grain a century ago. He indicated that tensions involving the Strait of Hormuz are leading those currently dependent on shipments from the Gulf to find alternative shipping capabilities. He stated that Saudi Arabia was considering building new pipelines to Oman and Yemen, and that Iraq might revive the disused Iraq-Syria pipeline to ship crude to the Mediterranean. Luft stated that reducing Hormuz traffic "presents the West with a new opportunity to augment its current Iran containment strategy."


== See alsoEdit ==
Abu Musa island
Bandar Lengeh
Hormozgn Province
Kingdom of Hormuz


== NotesEdit ==


== ReferencesEdit ==
Mohammed Kookherdi (1997) Kookherd, an Islamic civil at Mehran river, third edition: Dubai
[Atlas Gitashenasi Ostanhai Iran] (Gitashenasi Province Atlas of Iran)


== Further readingEdit ==
Wise, Harold Lee (2007). Inside the Danger Zone: The U.S. Military in the Persian Gulf 1987-88. Annapolis: Naval Institute Press. ISBN 1-59114-970-3. 
Diba, Bahman Aghai (2011). Is Iran legally permitted to close Strait of Hormuz to countries that impose sanctions against Iran's oil?. Cupertino, California: Payvand Iranian-American Website. 
Waehlisch, Martin. The Iran-United States Dispute, the Strait of Hormuz, and International Law. The Yale Journal of International Law Online, Vol. 37 (Spring 2012), pp. 23-34. 


== External linksEdit ==
"Strait of Hormuz" by the Robert Strauss Center: background on political, economic, business, technical, and military issues
"Strait of Hormuz": links to various resources, including antique maps.
"Abu Musa Island" by the Federation of American Scientists
1580-pixel-wide excerpt from "Strait of Hormuz  U.K. Admiralty Chart 2888"
"How Great a Concern? Iranian Threats to Close the Strait of Hormuz": Briefly describes offense/defense balance in the Strait and links to articles in the journal, International Security; offers a map of the Strait and surrounding region
"Transit Passage Rights in the Strait of Hormuz and Irans Threats to Block the Passage of Oil Tankers": The American Society of International Law
Videos
Politics of Strait of Hormuz: PressTV (2012)