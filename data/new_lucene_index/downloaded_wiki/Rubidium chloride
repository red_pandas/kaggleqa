Rubidium chloride is the chemical compound with the formula RbCl. This alkali metal halide is composed of rubidium and chlorine, and finds diverse uses ranging from electrochemistry to molecular biology.


== Structure ==
In its gas phase, RbCl is diatomic with a bond length estimated at 2.7868 . This distance increases to 3.285  for cubic RbCl, reflecting the higher coordination number of the ions in the solid phase.
Depending on conditions, solid RbCl exists in one of three arrangements or polymorphs as determined with holographic imaging:


=== Sodium chloride (octahedral 6:6) ===
The NaCl polymorph is most common. A cubic close-packed arrangement of chloride anions with rubidium cations filling the octahedral holes describes this polymorph. Both ions are six-coordinate in this arrangement. This polymorph's lattice energy is only 3.2 kJ/mol less than the following structure's.


=== Caesium chloride (cubic 8:8) ===
At high temperature and pressure, RbCl adopts the CsCl structure (NaCl and KCl undergo the same structural change at high pressures). Here, the chloride ions form a simple cubic arrangement with chloride anions occupying the vertices of a cube surrounding a central Rb+. This is RbCl's densest packing motif. Because a cube has eight vertices, both ions' coordination numbers equal eight. This is RbCl's highest possible coordination number. Therefore, according to the radius ratio rule, cations in this polymorph will reach their largest apparent radius because the anion-cation distances are greatest.


=== Sphalerite (tetrahedral 4:4) ===
The sphalerite polymorph of rubidium chloride is extremely rare, resulting in few structural studies. The lattice energy, however, for this formation is predicted to nearly 40.0 kJ/mol smaller than those of the preceding structures.


== Synthesis ==
The most common preparation of pure rubidium chloride involves the reaction of its hydroxide with hydrochloric acid, followed by recrystallization:
RbOH(aq) + HCl(aq)  RbCl(aq) + H2O(l)
Because RbCl is hygroscopic, it must be protected from atmospheric moisture, e.g. using a desiccator. RbCl is primarily used in laboratories. Therefore, numerous suppliers (see below) produce it in smaller quantities as needed. It is offered in a variety of forms for chemical and biomedical research.


== Reactions ==
Rubidium chloride reacts with sulfuric acid to rubidium hydrogen sulfate.


== Uses ==
Rubidium chloride has been shown to modify coupling between circadian oscillators via reduced photaic input to the suprachiasmatic nuclei. The outcome is a more equalized circadian rhythm, even for stressed organisms.
RbCl is an excellent non-invasive biomarker. The compound dissolves well in water and readily be taken up by organisms. Once broken in the body, Rb+ replaces K+ in tissues because they are from the same chemical group. An example of this is the use of a radioactive isotope to evaluate perfusion of heart muscle.
RbCl transformation for competent cells is arguably the compound's most abundant use. Cells treated with a hypotonic solution containing RbCl expand. As a result, the expulsion of membrane proteins allows negatively charged DNA to bind.
RbCl has been used as an antidepressant in Europe under the trade name Rubinorm in doses ranging from 180 to 720 mg. Every 18 mg of RbCl is approximately equivalent to 1 Banana equivalent dose due to the large fraction (27.8%) of naturally occurring isotope Rubidium-87. It increases dopamine and norepinephrine levels and has stimulating effects, hence it is useful for anergic and apathetic depressives.


== References ==