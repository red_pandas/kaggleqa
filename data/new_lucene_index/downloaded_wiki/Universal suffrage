Universal suffrage (also universal adult suffrage, general suffrage or common suffrage) consists of the extension of the right to vote to adult citizens (or subjects), though it may also mean extending that right to minors (Demeny voting) and non-citizens. Although suffrage has two necessary components, the right to vote and opportunities to vote, the term universal suffrage is associated only with the right to vote and ignores the frequency that an incumbent government consults the electorate. Where universal suffrage exists, the right to vote is not restricted by race, sex, belief, wealth, or social status.
Historically universal suffrage initially referred to adult male suffrage. The First French Republic was the first nation that adopted universal male suffrage in 1792; it was the first national system that abolished all property requirements as a prerequisite for allowing men to register and vote. Greece recognized full male suffrage in 1830 and France and Switzerland have continuously done so since the 1848 Revolution (for resident male citizens). Upon independence in the 19th century, several Latin American countries and Liberia in Africa initially extented suffrage to all adult males, but subsequently restricted it based on property requirements. The German Empire implemented full male suffrage in 1871. The United States theoretically adopted full male suffrage with the Fifteenth Amendment to the United States Constitution in 1870, but this was not practically implemented in the South until the Voting Rights Act of 1965.
In 1893 New Zealand became the first nation in the world (bar the short-lived 18th century Corsican Republic) to grant universal, male and female adult suffrage. In most countries, full universal suffrage followed about a generation after full male suffrage. Notable exceptions in Europe were France, where women could not vote until 1944, Greece (1952), and Switzerland (1971 in federal elections and 1990 in all cantonal elections). It is worth noting that countries that took a long time to adopt women's suffrage were often actually pioneers in granting universal male suffrage.
In the first modern democracies, governments restricted the vote to those with property and wealth, which almost always meant a minority of the male population. In some jurisdictions, other restrictions existed, such as requiring voters to practice a given religion. In all modern democracies, the number of people who could vote has increased progressively with time. In the 19th century in Europe, Great Britain and North America, there were movements advocating "universal [male] suffrage". The democratic movement of the late 19th century, unifying liberals and social democrats, particularly in northern Europe, used the slogan Equal and Common Suffrage.
The concept of universal suffrage requires the right to vote to be granted to all its residents. All countries, however, do not allow certain categories of citizens to vote. All countries have a minimum age, usually coinciding with the age of majority, and several countries impose felony disenfranchisement and disfranchisement based on resident status and citizenship. Saudi Arabia was the last major country that did not allow women to vote, but admitted women both to voting and candidacy in the 2015 municipal elections.


== Expanding suffrageEdit ==

The first movements in the Western world toward universal suffrage occurred in the early 19th century, and focused on removing property requirements for voting. In the United States following the American Civil War, slaves were freed and granted rights of citizens, including suffrage for adult males (although states established restrictions later in the century). In the late 19th and early 20th centuries, the focus of the universal suffrage movement was the extension of the right to vote to women.
Several European nations that had enacted universal suffrage had their normal legal process, or their status as an independent nation, interrupted during and after the First World War.
Many societies in the past have denied people the right to vote on the basis of race or ethnicity, related to discriminatory ideas about citizenship. For example, in apartheid-era South Africa, non-white people could not vote in national elections until the first multi-party elections in 1994.
Although in the United States African Americans were granted the rights of citizens, including suffrage, by constitutional amendments following the American Civil War, later in the century white Democrats had regained control in all states of the former Confederacy in the American South. From 1890 to 1910 they passed new constitutions, laws or constitutional amendments establishing barriers to voter registration and, later, voting, that essentially disfranchised most African Americans. They mounted legal challenges, but did not fully regain the ability to exercise their rights until after passage in the mid-1960s of the Voting Rights Act, which provided federal protection and enforcement. This was a result of their activism in the Civil Rights Movement.


== DisfranchisementEdit ==

All US states, with the exceptions of Maine and Vermont, disfranchise some felons from voting depending on their current incarceration, parole or probation status; a number of US states permanently disfranchise some felons, even after their release from prison. Many states within the U.S. previously disfranchised paupers, persons who either paid no direct taxes, or received public assistance.
Nations have differing degrees of legal recognition of non-resident citizens: non-resident Danes cannot vote after two years; non-resident Italians may vote for representatives at-large in the Italian parliament; British citizens cannot vote for their national parliament unless they have lived in the UK within the last fifteen years. A few nations also restrict those who are part of the military or police forces, e.g. Kuwait.
Many democratic countries, for example the United Kingdom and France, have had colonies with citizens living outside of the mother country and have generally not been entitled to vote for the national legislature. A peculiarly complex case is that of Algeria under the Third French Republic: Algeria was legally an integral part of France, but citizenship was restricted (as in other French colonies proper) by legal status, not by race or ethnicity. Any Muslim Algerian could become a French citizen by choosing to live like one. As this required the person to resign jurisdiction under Islamic law in favour of French civic law, very few did. Among Muslims, such a change was considered apostasy from Islam, which was the dominant religion in Algeria. Colonists in America declared Independence from Great Britain citing "no taxation without representation" as one of their main grievances. However, the newly established country did not extend voting rights in the United States beyond white male property owners (about 6% of the population), and did not grant its overseas citizens the right to vote in elections either, until the passage of the Uniformed and Overseas Citizens Absentee Voting Act in 1986.
Citizens of an EU Member State are allowed to vote in EU parliamentary elections, as well as some local elections. For example, a British person living in Graz, Austria, would be entitled to vote for the European Parliament as a resident of the "electoral district" of Austria, and to vote in Graz municipal elections. He would, however, not be entitled to vote in Austrian (federal) elections, or Styrian (state) elections. Similarly, all locally resident EU citizens in the UK are allowed to vote for representatives of the local council, and those resident in Scotland, Wales and Northern Ireland may vote for the devolved parliaments or assemblies. But, only British, Irish and Commonwealth citizens are allowed to vote for the British House of Commons.
In the West Bank, Palestinians are not Israeli citizens and therefore cannot vote in Israeli elections. Different areas of the West Bank are under varying levels of Israeli control. settlers (and their offspring) to Area C retain their citizenship, and can continue to vote.


== Dates by countryEdit ==
States have granted and revoked universal suffrage at various times. This list can be organised in three ways:
Universal There are no distinctions between voters over a certain age in any part of its territories due to gender, literacy, wealth, social status, language, religion, race, or ethnicity.
Male is for all males over a certain age irrespective of literacy, wealth, or social status.
Female is for all genders over a certain age irrespective of literacy, wealth, or social status.
Ethnicity is for all eligible voters over a certain age irrespective of language, religion, race, or ethnicity.
Note: The table can be sorted alphabetically or chronologically using the  icons.


== Women's suffrageEdit ==

The first women's suffrage was granted in Sweden-Finland, where it was in effect during the age of liberty from 1718 until 1772.
The first women's suffrage was granted in Corsica in 1755 and lasted until 1769.
Women's suffrage (with the same property qualifications as for men) was next granted in New Jersey in 1776 (the word "inhabitants" was used instead of "men" in the 1776 Constitution) and rescinded in 1807.
The Pitcairn Islands granted restricted women's suffrage in 1838. Various other countries and states granted restricted women's suffrage in the later half of the nineteenth century, starting with South Australia in 1861.
The first unrestricted women's suffrage in a major country was granted in New Zealand in 1893. The women's suffrage bill was adopted mere weeks before the general election of 1893. Mori men had been granted suffrage in 1867, white men in 1879. The Freedom in the World index lists New Zealand as the only free country in the world in 1893.
South Australia first granted women suffrage and allowed them to stand for parliament in 1894.
The autonomous Grand Principality of Finland, a decade before becoming the republic of Finland, was the first European country to introduce female suffrage in 1906. It was also the second country to allow female candidates and the first to actually elect female MP's in 1907.
In 1931, the Second Spanish Republic allowed women the right of passive suffrage with three women being elected. During the discussion to extend their right to active suffrage, the Radical Socialist Victoria Kent confronted the Radical Clara Campoamor. Kent argued that Spanish women were not yet prepared to vote and, since they were too influenced by the Catholic Church they would vote for right-wing candidates. Campoamor however pleaded for women's rights regardless of political orientation. Her point finally prevailed and, in the election of 1933, the political right won with the vote of citizens of any sex over 23. Both Campoamor and Kent lost their seats.


== Youth suffrage, children's suffrage and suffrage in schoolEdit ==
Main article: Youth suffrage
Democratic schools practice and support universal suffrage in school, which allows a vote to every member of the school, including students and staff. Such schools hold that this feature is essential for students to be ready to move into society at large.


== See alsoEdit ==
Suffragette
Demeny voting
Voting age
Equality before the law
List of suffragists and suffragettes
List of women's rights activists
Timeline of women's suffrage


== ReferencesEdit ==


== External linksEdit ==
Limited suffrage in England prior to the 1832 reforms
Finnish centennial celebration
"Have you heard the news?", a pamphlet published by an anonymous English freeman in 1835
An address to the middle and working classes engaged in trade and manufactures throughout the empire on the necessity of union at the present crisis (1842) by Richard Gardner