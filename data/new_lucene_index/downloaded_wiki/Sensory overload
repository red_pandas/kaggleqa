Sensory overload occurs when one or more of the body's senses experiences over-stimulation from the environment. There are many environmental elements that impact an individual. Examples of these elements are urbanization, crowding, noise, mass media, technology, and the explosive growth of information. Sensory overload is commonly associated with sensory processing disorder. Like its opposite sensory deprivation, it has been used as a means of torture.


== Symptoms ==
There are a wide variety of symptoms that have been found to be associated with sensory overload. These symptoms can occur in both children and adults. Some of these symptoms are:
Irritability
"Shuts down", or refuses to participate in activities and/or interact with others
Avoids touching or being touched
Gets overexcited
Covers eyes around bright lights
Makes poor eye contact
Covers ears to close out sounds or voices
Complains about noises that do not affect others
Having difficulty focusing on an activity
Constantly changing activities, never completing a task
Irritation caused by shoes, socks, tags, or different textures
Over-sensitivity to touch, movement, sights, and/or sounds
Has trouble with social interactions
Extremely high or extremely low activity levels
Muscle tension
Fidgeting and restlessness
Angry outbursts
Sleeplessness/fatigue
Difficulty concentrating


== Causes ==
Sensory overload can result from the overstimulation of any of the senses.
Hearing: Loud noise or sound from multiple sources, such as several people talking at once or a loud fire alarm.
Sight: Bright lights, strobing lights, or environments with lots of movement such as crowds or frequent scene changes on television.
Smell and taste: Strong aromas or spicy foods.
Touch: Tactile sensations such as being touched by another person or the feel of cloth on skin.


== As component of other disorders ==
Sensory overload has been found to be associated with other disorders such as:
Fibromyalgia (FM)
Chronic fatigue syndrome (CFS)
Posttraumatic stress disorder (PTSD)
Autistic spectrum disorders
Generalized anxiety disorder (GAD)
Schizophrenia (see also sensory gating)
Synesthesia
Sensory processing disorder


== Treatments ==
There are many different ways to treat sensory overload. One way to reduce this tension is to participate in occupational therapy; however, there are many ways for people with symptoms to reduce it themselves. Being able to identify one's own triggers of sensory overload can help reduce, eliminate, or avoid them. Most often the quickest way to ease sensory overload symptoms is to remove oneself from the situation. Deep pressure against the skin combined with proprioceptive input that stimulates the receptors in the joints and ligaments often calms the nervous system. Reducing sensory input such as eliminating distressing sounds and lowering the lights can help. Calming, focusing music works for some. If a quick break does not relieve the problem, an extended rest is advised. People with sensory processing issues may benefit from a sensory diet of activities and accommodations designed to prevent sensory overload and retrain the brain to process sensory input more typically. It is important in situations of sensory overload to calm oneself and return to a normal level.


== Prevention ==
There are two different methods to prevent sensory overload: avoidance and setting limits. The process of avoidance involves creating a more quiet and orderly environment. This includes keeping the noise to a minimum and reducing the sense of clutter. To prevent sensory overload, it is important to rest before big events and focus your attention and energy on one thing at a time. Setting limits involves restricting the amount of time spent on various activities and selecting settings to carefully avoid crowds and noise. One may also limit interactions with specific people to help prevent sensory overload.


== Sensory overload and marketing ==
It can be difficult to distinguish and understand information when experiencing sensory overload. Even such meaningless stimuli such as white noise or flickering lights may induce sensory overload. Sensory overload is common among consumers as many corporations compete with each other especially when advertising. Advertisers will use the best colours, words, sounds, textures, designs and much more to get the attention of a customer. This can influence the consumer, as he or she will be drawn to a product that is more attention grabbing. However policy makers and advertisers must be aware that too much information or attention grabbing products can cause sensory overload.


=== Implications of public policy ===
Implications of a public policy in regards to information overload have two main assumptions. The assumptions the policymakers have are, first, to assume that consumers have a great deal of processing capacity and a great deal of time to process information. Secondly consumers can always absorb the information without serious concern about how much information has been presented. As researchers have pointed out, policy makers should better understand the difference between the process and availability of information. This will help decrease the possibility of information overload. In some cases the time to process such information in a commercial can be 6 out of 30 seconds. This can lead consumers confused and overloaded with such fast paced information thrown at them. To understand how consumers process information three factors much be analyzed. Factors such as the amount of information given, the source of corrective information and the way in which it is all presented to the consumer. However it should be noted that different types of media have different processing demands. An optimal outcome for policy makers to influence advertisers to try is to present information through a T.V. commercial stating simple facts about a product and then encourage the audience to check out their website for more details. Therefore their quick processing time of a commercial was not overload with information thus saving the consumer from sensory overload.


=== Implications for the consumers ===
Consumers today must learn to learn to cope with overloading and an abundance of information. Through the radio, billboards, television, newspapers and much more. Information is everywhere and being thrown at consumers from every angle and direction. Therefore Naresh K. Malhorta, the author of Information and Sensory Overload presents the following guidelines. First, consumers must try and limit the intake of external information and sensory inputs to avoid sensory overload. This can be done by tuning out irrelevant information presented by the media and marketers to get the attention of the consumer. Second, record important information externally rather than mentally. Information can be easily forgotten mentally once the individual becomes overloaded by their sense. Thus it is recommended for a consumer to write down important information rather than store it mentally. Third, when examining a product, do not overload their senses by examining more than five products at a time. This will lead to confusion, frustration and possibly leading to the correct choice according to the marketers. Fourth, process information where there is less irrelevant information around. This will eliminate external information and sensory distractions such as white noise an other information presented in an environment. Finally, it is important to make consuming a pleasant and relaxed experience. This will help diminish the stress, overwhelming feeling and experience of sensory overload.


=== Human senses and marketing ===
Using the human sense in marketing can help create a personal unique experience for the customer. Most commonly, marketers will take advantage of the humans four out of five senses to increase the amount of information being presented to the customer.
The sense of smell can affect a persons evaluation of products. Researchers have suggested that good or bad feelings can be generated by smells and usually associated with upbringing, emotion, learning and even culture. Therefore the sense of smell is a tool to attract an individuals attention to a certain product.
The sense of hearing is a powerful sense. The ears are capable of picking up all sorts of information and can contribute to peoples feelings. Marketers will play music to help create and control feelings within a consumer. This music can help great a positive and comfortable association with the brand.
The sense of touch helps determine the quality of a product. Thus companies of certain products will make the sense of touch available to its consumer. Touch here or feel me, signs encourage costumers to interact with the product. Allowing companies customers to touch a product will help add information from their senses.
The sense of sight is the most focused on sense by marketers. Researchers have found that when consumers see warm colours such as orange or red their blood pressure and heart rate increase. The opposite effect occurs when customers see cool colours such as green or blue. Therefore fast-food corporations will more than often use the colours red to symbolize fast and speedy for advertising and marketing purposes.


== Case histories ==
Not many studies have been done on sensory overload, but one example of a sensory overload study was reported by Lipowski (1975) as part of his research review on the topic that discussed the work done by Japanese researchers at Tohoku University. The Tohoku researchers exposed their subjects to intense visual and auditory stimuli presented randomly in a condition of confinement ranging in duration from 3 to 5 hours. Subjects showed heightened and sustained arousal as well as mood changes such as aggression, anxiety, and sadness. These results have helped open the door to further research on sensory overload.


== History ==
Sociologist Georg Simmel contributed to the description of sensory overload in the early 1900s. Writer of The Metropolis and Mental Life, Simmel writes about an urban scenario of constantly appearing stimuli that trigger the brains senses. He writes about a barrier that must protect the individual from this constant stimulation in order to keep one sane. In short, Simmel concludes with stating that the urban life, full of its stimulations at different scenarios, provides excitement to our nervous system. The downside is that too much exposure of this sensory overload depletes the bodys energy reservoirs. Lacking the appropriate energy to react at new situations can form the bland mentality of an individual. A persons mentality can be detrimental with a high degree of exposure of sensory overload. The raw reaction to new stimuli will be different when a persons sensory experiences are overloaded (from past stimuli), compared to when they are not overloaded, the experience will be more pure.


== See also ==
Sensory processing disorder
Sensory adaptation
Sensory deprivation
Sensory gating
Highly sensitive person


== References ==