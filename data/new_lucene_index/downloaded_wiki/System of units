A system of measurement is a collection of units of measurement and rules relating them to each other. Systems of measurement have historically been important, regulated and defined for the purposes of science and commerce. Examples of systems of measurement in modern use include the metric system, the imperial system, and United States customary units.


== History ==

Although we might suggest that the Egyptians had discovered the art of measurement, it is only with the Greeks that the science of measurement begins to appear. The Greek's knowledge of geometry, and their early experimentation with weights and measures, soon began to place their measurement system on a more scientific basis. By comparison, Roman science, which came later, was not as advanced...

The French Revolution gave rise to acceptance of the metric system, and this has spread around the world, replacing most customary units of measure. In most systems, length (distance), weight, and time are base quantities; or as has been now accepted as better in science, the substitution of mass for weight, as a better more, basic parameter. Some systems have changed to recognize the improved relationship, notably the 1824 legal changes to the imperial system.
Later science developments showed that either electric charge or electric current may be added to complete a minimum set of base quantities by which all other metrological units may be defined. (However, electrical units are not necessary for a minimum set. Gaussian units, for example, have only length, mass, and time as base quantities.) Other quantities, such as power, speed, etc. are derived from the base set; for example, speed is distance per unit time. Historically a wide range of units was used for the same quantity, in several cultural settings, length was measured in inches, feet, yards, fathoms, rods, chains, furlongs, miles, nautical miles, stadia, leagues, with conversion factors which were not powers of ten. yes were they necessarily the same units (or equal units) between different members of similar cultural backgrounds. It must be understood by the modern reader that historically, measurement systems were perfectly adequate within their own cultural milieu, and the understanding that a better, more universal system (based on more rationale and base units) only gradually spread with the maturation and appreciation of the rigor characteristic of Newtonian physics. Moreover, changing a measurement system has real fiscal and cultural costs as well as the advantages that accrue from replacing one measuring system with a better one.
Once the analysis tools within that field were appreciated and came into widespread use in the emerging sciences, especially in the applied sciences like civil and mechanical engineering, pressure built up for conversion to a common basis of measurement. As people increasingly appreciated these needs and the difficulties of converting between numerous national customary systems became more widely recognised there was an obvious justification for an international effort to standardise measurements. The French Revolutionary spirit took the first significant and radical step down that road.
In antiquity, systems of measurement were defined locally, the different units were defined independently according to the length of a king's thumb or the size of his foot, the length of stride, the length of arm or per custom like the weight of water in a keg of specific size, perhaps itself defined in hands and knuckles. The unifying characteristic is that there was some definition based on some standard, however egocentric or amusing it may now seem viewed with eyes used to modern precision. Eventually cubits and strides gave way under need and demand from merchants and evolved to customary units.
In the metric system and other recent systems, a single basic unit is used for each base quantity. Often secondary units (multiples and submultiples) are used which convert to the basic units by multiplying by powers of ten, i.e., by simply moving the decimal point. Thus the basic metric unit of length is the metre; a distance of 1.234 m is 1234.0 millimetres, or 0.001234 kilometres.


=== Current practice ===

Metrication is complete or nearly complete in almost all countries. US customary units are heavily used in the United States and to some degree Liberia. Traditional Burmese units of measurement are used in Burma. U.S. units are used in limited contexts in Canada due to a high degree of trade; additionally there is considerable use of Imperial weights and measures, despite de jure Canadian conversion to metric.
A number of other jurisdictions have laws mandating or permitting other systems of measurement in some or all contexts, such as the United Kingdom  where for example its road signage legislation only allows distance signs displaying imperial units (miles or yards)  or Hong Kong.
In the United States, metric units are used almost universally in science, widely in the military, and partially in industry, but customary units predominate in household use. At retail stores, the liter is a commonly used unit for volume, especially on bottles of beverages, and milligrams are used to denominate the amounts of medications, rather than grains. Also, other standardized measuring systems other than metric are still in universal international use, such as nautical miles and knots in international aviation and shipping.


== Metric system ==

Metric systems of units have evolved since the adoption of the first well-defined system in France in 1795. During this evolution the use of these systems has spread throughout the world, first to non-English-speaking countries, and then to English speaking countries.
Multiples and submultiples of metric units are related by powers of ten and their names are formed with prefixes. This relationship is compatible with the decimal system of numbers and it contributes greatly to the convenience of metric units.
In the early metric system there were two base units, the metre for length and the gram for mass. The other units of length and mass, and all units of area, volume, and derived units such as density were derived from these two base units.
Mesures usuelles (French for customary measurements) were a system of measurement introduced to act as a compromise between the metric system and traditional measurements. It was used in France from 1812 to 1839.
A number of variations on the metric system have been in use. These include gravitational systems, the centimetregramsecond systems (cgs) useful in science, the metretonnesecond system (mts) once used in the USSR and the metrekilogramsecond system (mks).
The current international standard metric system is the International System of Units (Systme international d'units or SI) It is an mks system based on the metre, kilogram and second as well as the kelvin, ampere, candela, and mole.
The SI includes two classes of units which are defined and agreed internationally. The first of these classes are the seven SI base units for length, mass, time, temperature, electric current, luminous intensity and amount of substance. The second of these are the SI derived units. These derived units are defined in terms of the seven base units. All other quantities (e.g. work, force, power) are expressed in terms of SI derived units.


== Imperial and US customary units ==

Both imperial units and US customary units derive from earlier English units. Imperial units were mostly used in the British Commonwealth and the former British Empire but in all Commonwealth countries they have been largely supplanted by the metric system. They are still used for some applications in the United Kingdom but have been mostly replaced by the metric system in commercial, scientific, and industrial applications. US customary units, however, are still the main system of measurement in the United States. While some steps towards metrication have been made (mainly in the late 1960s and early 1970s), the customary units have a strong hold due to the vast industrial infrastructure and commercial development.
While imperial and US customary systems are closely related, there are a number of differences between them. Units of length and area (the inch, foot, yard, mile etc.) are identical except for surveying purposes. The Avoirdupois units of mass and weight differ for units larger than a pound (lb.). The imperial system uses a stone of 14 lb., a long hundredweight of 112 lb. and a long ton of 2240 lb. The stone is not used in the US and the hundredweights and tons are short being 100 lb. and 2000 lb. respectively.
Where these systems most notably differ is in their units of volume. A US fluid ounce (fl oz) c. 29.6 millilitres (ml) is slightly larger than the imperial fluid ounce (28.4 ml). However, as there are 16 US fl oz to a US pint and 20 imp fl oz per imperial pint, these imperial pint is about 20% larger. The same is true of quarts, gallons, etc. Six US gallons are a little less than five imperial gallons.
The Avoirdupois system served as the general system of mass and weight. In addition to this there are the Troy and the Apothecaries' systems. Troy weight was customarily used for precious metals, black powder and gemstones. The troy ounce is the only unit of the system in current use; it is used for precious metals. Although the troy ounce is larger than its Avoirdupois equivalent, the pound is smaller. The obsolete troy pound was divided into twelve ounces opposed to the sixteen ounces per pound of the Avoirdupois system. The Apothecaries' system; traditionally used in pharmacology, now replaced by the metric system; shares the same pound and ounce as the troy system but with different further subdivisions.


== Natural units ==
Natural units are physical units of measurement defined in terms of universal physical constants in such a manner that some chosen physical constants take on the numerical value of one when expressed in terms of a particular set of natural units. Natural units are natural because the origin of their definition comes only from properties of nature and not from any human construct. Various systems of natural units are possible. Below are listed some examples.
Geometric unit systems are useful in relativistic physics. In these systems the base physical units are chosen so that the speed of light and the gravitational constant are set equal to unity.
Planck units are a form of geometric units obtained by also setting Boltzmann's constant, the Coulomb force constant and the reduced Planck constant to unity. They might be considered unique in that they are based only on properties of free space rather than any prototype, object or particle.
Stoney units are similar to Planck units but set the elementary charge to unity and allow Planck's constant to float.
"Schrdinger" units are also similar to Planck units and set the elementary charge to unity too but allow the speed of light to float.
Atomic units (au) are a convenient system of units of measurement used in atomic physics, particularly for describing the properties of electrons. The atomic units have been chosen such that the fundamental electron properties are all equal to one atomic unit. They are similar to "Schrdinger" units but set the electron mass to unity and allow the gravitational constant to float. The unit energy in this system is the total energy of the electron in the Bohr atom and called the Hartree energy. The unit length is the Bohr radius.
Electronic units are similar to Stoney units but set the electron mass to unity and allow the gravitational constant to float. They are also similar to Atomic units but set the speed of light to unity and allow Planck's constant to float.
Quantum electrodynamical units are similar to the electronic system of units except that the proton mass is normalised rather than the electron mass.


== Non-standard units ==
Non-standard measurement units, sometimes found in books, newspapers etc., include:


=== Area ===
The American football field, which has a playing area 100 yards (91.4 m) long by 160 feet (48.8 m) wide. This is often used by the American public media for the sizes of large buildings or parks: easily walkable but non-trivial distances. Note that it is used both as a unit of length (100 yd or 91.4 m, the length of the playing field excluding goal areas) and as a unit of area (57,600 sq ft or 5,350 m2), about 1.32 acres (0.53 ha).
British media also frequently uses the football pitch for equivalent purposes, although soccer pitches are not of a fixed size, but instead can vary within defined limits (100130 yd or 91.4118.9 m long, and 50100 yd or 45.791.4 m wide, giving an area of 5,000 to 13,000 sq yd or 4,181 to 10,870 m2). However the UEFA Champions League field must be exactly 105 by 68 m (114.83 by 74.37 yd) giving an area of 7,140 m2 (0.714 ha) or 8,539 sq yd (1.764 acres). Example: HSS vessels are aluminium catamarans about the size of a football pitch... - Belfast Telegraph 23 June 2007


=== Energy ===
A ton of TNT equivalent, and its multiples the kiloton, the megaton, and the gigaton. Often used in stating the power of very energetic events such as explosions and volcanic events and earthquakes and asteroid impacts. A gram of TNT as a unit of energy has been defined as 1000 thermochemical calories (1,000 cal or 4,184 J).
The atom bomb dropped on Hiroshima. Its force is often used in the public media and popular books as a unit of energy. (Its yield was roughly 13 kilotons, or 60 TJ.)
One stick of dynamite


== Units of currency ==
A unit of measurement that applies to money is called a unit of account in economics and unit of measure in accounting. This is normally a currency issued by a country or a fraction thereof; for instance, the US dollar and US cent (1100 of a dollar), or the euro and euro cent.
ISO 4217 is the international standard describing three letter codes (also known as the currency code) to define the names of currencies established by the International Organization for Standardization (IOS).


== Historical systems of measurement ==

Throughout history, many official systems of measurement have been used. While no longer in official use, some of these customary systems are occasionally used in day-to-day life, for instance in cooking.


=== Africa ===


=== Asia ===


=== Europe ===


=== North America ===


=== Oceania ===


=== South America ===


== See also ==


=== Conversion tables ===
Conversion of units


== Notes and references ==


== Bibliography ==
Tavernor, Robert (2007), Smoot's Ear: The Measure of Humanity, ISBN 0-300-12492-9


== External links ==
CLDR - Unicode localization of currency, date, time, numbers
A Dictionary of Units of Measurement
Old units of measure
Measures from Antiquity and the Bible Antiquity and the Bible at the Wayback Machine (archived May 10, 2008)
Reasonover's Land Measures A Reference to Spanish and French land measures (and their English equivalents with conversion tables) used in North America
The Unified Code for Units of Measure