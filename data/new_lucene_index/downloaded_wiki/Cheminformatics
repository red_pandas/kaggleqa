Cheminformatics (also known as chemoinformatics, chemioinformatics and chemical informatics) is the use of computer and informational techniques applied to a range of problems in the field of chemistry. These in silico techniques are used in, for example, pharmaceutical companies in the process of drug discovery. These methods can also be used in chemical and allied industries in various other forms.


== History ==
The term chemoinformatics was defined by F.K. Brown  in 1998:

Chemoinformatics is the mixing of those information resources to transform data into information and information into knowledge for the intended purpose of making better decisions faster in the area of drug lead identification and optimization.

Since then, both spellings have been used, and some have evolved to be established as Cheminformatics, while European Academia settled in 2006 for Chemoinformatics. The recent establishment of the Journal of Cheminformatics is a strong push towards the shorter variant.


== Basics ==
Cheminformatics combines the scientific working fields of chemistry, computer science and information science for example in the areas of topology, chemical graph theory, information retrieval and data mining in the chemical space. Cheminformatics can also be applied to data analysis for various industries like paper and pulp, dyes and such allied industries.


== Applications ==


=== Storage and retrieval ===

The primary application of cheminformatics is in the storage, indexing and search of information relating to compounds. The efficient search of such stored information includes topics that are dealt with in computer science as data mining, information retrieval, information extraction and machine learning. Related research topics include:
Unstructured data
Information retrieval
Information extraction

Structured Data Mining and mining of Structured data
Database mining
Graph mining
Molecule mining
Sequence mining
Tree mining

Digital libraries


==== File formats ====

The in silico representation of chemical structures uses specialized formats such as the XML-based Chemical Markup Language or SMILES. These representations are often used for storage in large chemical databases. While some formats are suited for visual representations in 2 or 3 dimensions, others are more suited for studying physical interactions, modeling and docking studies.


=== Virtual libraries ===
Chemical data can pertain to real or virtual molecules. Virtual libraries of compounds may be generated in various ways to explore chemical space and hypothesize novel compounds with desired properties.
Virtual libraries of classes of compounds (drugs, natural products, diversity-oriented synthetic products) were recently generated using the FOG (fragment optimized growth) algorithm.  This was done by using cheminformatic tools to train transition probabilities of a Markov chain on authentic classes of compounds, and then using the Markov chain to generate novel compounds that were similar to the training database.


=== Virtual screening ===

In contrast to high-throughput screening, virtual screening involves computationally screening in silico libraries of compounds, by means of various methods such as docking, to identify members likely to possess desired properties such as biological activity against a given target. In some cases, combinatorial chemistry is used in the development of the library to increase the efficiency in mining the chemical space. More commonly, a diverse library of small molecules or natural products is screened.


=== Quantitative structure-activity relationship (QSAR) ===

This is the calculation of quantitative structure-activity relationship and quantitative structure property relationship values, used to predict the activity of compounds from their structures. In this context there is also a strong relationship to Chemometrics. Chemical expert systems are also relevant, since they represent parts of chemical knowledge as an in silico representation. There is a relatively new concept of Matched molecular pair analysis or Prediction driven MMPA which is coupled with QSAR model in order to identify activity cliff.


== See also ==


== References ==
^ F.K. Brown (1998). "Chapter 35. Chemoinformatics: What is it and How does it Impact Drug Discovery". Annual Reports in Med. Chem. Annual Reports in Medicinal Chemistry 33: 375. doi:10.1016/S0065-7743(08)61100-8. ISBN 978-0-12-040533-6. 
^ Brown, Frank (2005). "Editorial Opinion: Chemoinformatics  a ten year update". Current Opinion in Drug Discovery & Development 8 (3): 296302. 
^ Cheminformatics or Chemoinformatics ?
^ Obernai Declaration
^ Gasteiger J.(Editor), Engel T.(Editor): Chemoinformatics : A Textbook. John Wiley & Sons, 2004, ISBN 3-527-30681-1
^ A.R. Leach, V.J. Gillet: An Introduction to Chemoinformatics. Springer, 2003, ISBN 1-4020-1347-7
^ Alexandre Varnek and Igor Baskin (2011). "Chemoinformatics as a Theoretical Chemistry Discipline". Molecular Informatics 30 (1): 2032. doi:10.1002/minf.201000100. 
^ Barry A. Bunin (Author), Brian Siesel (Author), Guillermo Morales (Author), Jrgen Bajorath (Author): Chemoinformatics: Theory, Practice, & Products. Springer, 2006, ISBN 978-1402050008
^ Kutchukian, Peter; Lou, David; Shakhnovich, Eugene (2009). "FOG: Fragment Optimized Growth Algorithm for the de Novo Generation of Molecules occupying Druglike Chemical". Journal of Chemical Information and Modeling 49 (7): 16301642. doi:10.1021/ci9000458. PMID 19527020. 
^ http://www.jcheminf.com/content/6/1/48/abstract


== External links ==
Indiana Cheminformatics Education Portal
Journal of Cheminformatics
OEChem Cheminformatics Programming Toolkit
The Blue Obelisk Movement
The eCheminfo Network and Community of Practice
Cheminformatics courses at Indiana University
ChemXSeer search engine and tool at Penn State
Cheminformatics at Rensselaer Polytechnic Institute
Collaborative Drug Discovery CDD Vault
The Chemical Structure Association Trust (see also CSA Trust).
Comprehensive cheminformatics link list and data set repository
A cheminformatics glossary
Chemoinformatics initiatives at NCL Pune, India
International Conference on Chemoinformatics at NCL,Pune
Crowd Computing for Chemoinformatics at Vinod Scaria Lab , India
Cheminformatics Crowd Computing for Tuberculosis Drug Discovery (3C4TB) Project Page
Famous Cheminformatics quotations
The Cheminformatics and QSAR Society
UK-QSAR and ChemoInformatics Group
Education and Research at the University of Hamburg
Cheminformatics research at the Unilever Centre for Molecular Informatics, Cambridge, UK
YACHS Yet Another CHemistry Summarizer, Laboratoire Informatique d'Avignon LIA, France
Cheminformatics research at NovaMechanics Cyprus
Weblink-Cheminformatics SW and DB
Cheminformatics studies from Unilever Centre for Molecular Informatics to OpenEye
International Journal of Chemoinformatics and Chemical Engineering