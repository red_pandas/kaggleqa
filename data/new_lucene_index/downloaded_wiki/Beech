Beech (Fagus) is a genus of deciduous trees in the family Fagaceae, native to temperate Europe, Asia and North America. Recent classification systems of the genus recognize ten to thirteen species in two distinct subgenera, Engleriana and Fagus. The Engleriana subgenus is found only in East Asia, and is notably distinct from the Fagus subgenus in that these beeches are low-branching trees, often made up of several major trunks with yellowish bark. Further differentiating characteristics include the whitish bloom on the underside of the leaves, the visible tertiary leaf veins, and a long, smooth cupule-peduncle. Fagus japonica, Fagus engleriana, and the species F. okamotoi, proposed by the bontanist Chung-Fu Shen in 1992, comprise this subgenus. The better known Fagus subgenus beeches are high-branching with tall, stout trunks and smooth silver-grey bark. This group includes Fagus sylvatica, Fagus grandifolia, Fagus crenata, Fagus lucida, Fagus longipetiolata, and Fagus hayatae. The classification of the European beech, Fagus sylvatica is complex, with a variety of different names proposed for different species and subspecies within this region (for example Fagus taurica, Fagus orientalis, and Fagus moesica). Research suggests that beeches in Eurasia differentiated fairly late in evolutionary history, during the Miocene. The populations in this area represent a range of often overlapping morphotypes, though genetic analysis does not clearly support separate species.
Within its family, the Fagaceae, recent research has suggested that Fagus is the evolutionarily most basal group. The southern beeches (Nothofagus genus) previously thought closely related to beeches, are now treated as members of a separate family, Nothofagaceae. They are found in Australia, New Zealand, New Guinea, New Caledonia, Argentina and Chile (principally Patagonia and Tierra del Fuego).
The European beech (Fagus sylvatica) is the most commonly cultivated, although there are few important differences between species aside from detail elements such as leaf shape. The leaves of beech trees are entire or sparsely toothed, from 515 cm long and 410 cm broad. Beeches are monoecious, bearing both male and female flowers on the same plant. The small flowers are unisexual, the female flowers borne in pairs, the male flowers wind-pollinating catkins. They are produced in spring shortly after the new leaves appear. The bark is smooth and light grey. The fruit is a small, sharply threeangled nut 1015 mm long, borne singly or in pairs in soft-spined husks 1.52.5 cm long, known as cupules. The husk can have a variety of spine- to scale-like appendages, the character of which is, in addition to leaf shape, one of the primary ways beeches are differentiated. The nuts are edible, though bitter (though not nearly as bitter as acorns) with a high tannin content, and are called beechnuts or beechmast.
The name of the tree (Latin fagus, whence the species name; cognate with English "beech") is of Indo-European origin, and played an important role in early debates on the geographical origins of the Indo-European people. Greek  is from the same root, but the word was transferred to the oak tree (e.g. Iliad 16.767) as a result of the absence of beech trees in Greece.


== Habitat ==
Beech grows on a wide range of soil types, acidic or basic, provided they are not waterlogged. The tree canopy casts dense shade, and carpets the ground thickly with leaf litter.
In North America, they often form beech-maple climax forests by partnering with the sugar maple.
The beech blight aphid (Grylloprociphilus imbricator) is a common pest of American beech trees. Beeches are also used as food plants by some species of Lepidoptera (see list of Lepidoptera that feed on beeches).
Beech bark is extremely thin and scars easily. Since the beech tree has such delicate bark, carvings, such as lovers' initials and other forms of graffiti, remain because the tree is unable to heal itself.
Beech bark disease is a fungal infection that attacks the American beech through damage caused by scale insects. Infection can lead to the death of the tree .


== Distribution ==


=== Britain and Ireland ===

Beech was a late entrant to Great Britain after the last glaciation, and may have been restricted to basic soils in the south of England. Some suggest that it was introduced by Neolithic tribes who planted the trees for their edible nuts. The beech is classified as a native in the south of England and as a non-native in the north where it is often removed from 'native' woods. Large areas of the Chilterns are covered with beech woods, which are habitat to the common bluebell and other flora. The Cwm Clydach National Nature Reserve in southeast Wales was designated for its beech woodlands which are believed to be on the western edge of their natural range in this steep limestone gorge.
Beech is not native to Ireland; however, it was widely planted from the 18th century, and can become a problem shading out the native woodland understory. The Friends of the Irish Environment say that the best policy is to remove young, naturally regenerating beech while retaining veteran specimens with biodiversity value.
There is a campaign by Friends of the Rusland Beeches and South Lakeland Friends of the Earth launched in 2007 to reclassify the beech as native in Cumbria. The campaign is backed by Tim Farron MP who tabled a motion on 3 December 2007 regarding the status of beech in Cumbria.
Today, beech is widely planted for hedging and in deciduous woodlands, and mature, regenerating stands occur throughout mainland Britain below about 650 m. The tallest and longest hedge in the world (according to Guinness World Records) is the Meikleour Beech Hedge in Meikleour, Perth and Kinross, Scotland.


=== Continental Europe ===

The common European beech (Fagus sylvatica) grows naturally in Denmark and southern Norway and Sweden up to about the 57th  59th northern latitude. The most northern known naturally growing (not planted) beech trees are found in a few very small forests around the city of Bergen on the west coast of Norway with the North Sea nearby. Near the city of Larvik is the largest naturally occurring beech forest in Norway. Planted beeches are grown much farther north along the Norwegian coast.
There is some research suggesting that early agriculture patterns supported the spread of beech in continental Europe. Research has linked the establishment of beech stands in Scandinavia and Germany with cultivation and fire disturbance, i.e. early agricultural practices. Other areas which have a long history of cultivation, Bulgaria for example, do not exhibit this pattern, so it is as yet unclear how much human activity has influenced the spread of beech trees.

As a naturally growing forest tree, it marks the important border between the European deciduous forest zone and the northern pine forest zone. This border is important for both wildlife and fauna and is a sharp line along the Swedish western coast, which gets broader toward the south. In Denmark and in Scania, at the southernmost peak of the Scandinavian peninsula, south-west of the natural Spruce boundary, it's the most common forest tree. In Norway, the beech migration was very recent, and the species has not reached its distribution potential. Thus, the occurrence of oak in Norway is used as an indicator of the border between the temperate deciduous forest and the boreal spruce  pine forest.
Fagus Sylvatica is one of the most common hardwood trees in north central Europe, in France alone comprising about 15% of all non-conifers.


=== North America ===

The American beech (Fagus grandifolia) occurs across much of the eastern United States and southeastern Canada, with a disjunct population in Mexico. It is the only Fagus species in the Western Hemisphere. Prior to the Pleistocene Ice Age, it is believed to have spanned the entire width of the continent from the Atlantic Ocean to the Pacific, but now is confined to east of the Great Plains. F. grandifolia tolerates hotter climates than its European cousin, but is not planted much as an ornamental due to slower growth and less resistance to urban pollution. It most commonly occurs as an overstory component in the northern part of its range with sugar maple, transitioning to other forest types further south such as beech-magnolia. American beech is rarely encountered in developed areas unless as a remnant of a forest that was cut down for land development.


=== Asia ===

East Asia is home to five species of Fagus, only one of which (F. crenata) is occasionally planted in Western countries. Smaller than F. sylvatica and F. grandifolia, Japanese beech is one of the most common hardwoods in its native range.


== Uses ==
Beech wood is an excellent firewood, easily split and burning for many hours with bright but calm flames. Slats of Beech wood are washed in caustic soda to leach out any flavor or aroma characteristics and are spread around the bottom of fermentation tanks for Budweiser beer. This provides a complex surface on which the yeast can settle, so that it doesn't pile up too deep, thus preventing yeast autolysis (essentially death of the yeast cells) which would contribute off-flavors to the beer. Beech logs are burned to dry the malts used in some German smoked beers, giving the beers their typical flavour. Beech is also used to smoke Westphalian ham, various sausages, and some cheeses.
Some drums are made from beech, which has a tone between those of maple and birch, the two most popular drum woods.
The textile modal is a kind of rayon often made wholly from the reconstituted cellulose of pulped beech wood.
The European species Fagus sylvatica yields a utility timber that is tough but dimensionally unstable. It weighs about 720 kg per cubic metre and is widely used for furniture framing and carcass construction, flooring and engineering purposes, in plywood and in household items like plates, but rarely as a decorative wood. The timber can be used to build chalets, houses and log cabins.
Beech wood is used for the stocks of military rifles when traditionally preferred woods such as walnut are scarce or unavailable or as a lower-cost alternative.
The fruit of the beech tree is known as beechnuts or mast and is found in small burrs that drop from the tree in autumn. It is small, roughly triangular and edible, with a bitter, astringent taste. They have a high enough fat content that they can be pressed for edible oil. Fresh from the tree, beech leaves are a fine salad vegetable, as sweet as a mild cabbage though much softer in texture. The young leaves can be steeped in gin for several weeks, the liquor strained off and sweetened to give a light green/yellow liqueur called beechleaf noyau.
Beech wood tablets were a common writing material in Germanic societies before the development of paper. The Old English bc and Old Norse bk both have the primary sense of beech but also a secondary sense of book, and it is from bc that the modern word derives. In modern German, the word for book is Buch, with Buche meaning beech tree. In modern Dutch, the word for book is boek, with beuk meaning beech tree. In Swedish, these words are the same, bok meaning both beech tree and book. Similarly, in Russian, the word for beech is  (buk), while that for letter (as in a letter of the alphabet) is  (bukva).
The pigment bistre was made from beech wood soot.


=== As an ornamental ===
The beech most commonly grown as an ornamental tree is the European beech (Fagus sylvatica), widely cultivated in North America as well as its native Europe. Many varieties are in cultivation, notably the weeping beech F. sylvatica 'Pendula', several varieties of copper or purple beech, the fern-leaved beech F. sylvatica 'Asplenifolia', and the tricolour beech F. sylvatica 'roseomarginata'. The strikingly columnar Dawyck beech (F. sylvatica 'Dawyck') occurs in green, gold and purple forms, named after Dawyck Botanic Garden in the Scottish Borders, one of the four garden sites of the Royal Botanic Garden Edinburgh. Europe is also home to the lesser-known Oriental beech (F. orientalis) and Crimean beech (F. taurica).


== See also ==
Beech bark disease
Beech blight aphid
English Lowlands beech forests
Nothofagus - the Southern beeches
Primeval Beech Forests of the Carpathians and the Ancient Beech Forests of Germany
Sylvatica ecology cycle 'Sylvatic' means 'occurring in or affecting wild animals'


== References ==


== External links ==
"WCSP". World Checklist of Selected Plant Families  Fagus. 
Flora of China  Fagus
Flora of North America  Fagus
Eichhorn, Markus (October 2010). "The Beech Tree". Test Tube. Brady Haran for the University of Nottingham.