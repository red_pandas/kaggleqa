Baird's tapir (Tapirus bairdii), also known as the Central American tapir, is a species of tapir native to Mexico, Central America and northwestern South America. It is one of four Latin American species of tapir.


== NamesEdit ==
Baird's tapir is named for the American naturalist Spencer Fullerton Baird, who traveled to Mexico in 1843 and observed the animals. However, the species was first documented by another American naturalist, W. T. White. The tapir is the largest land mammal in Central America.
Like the other Latin American tapirs (the mountain tapir and the South American tapir), the Baird's tapir is commonly called danta by people in all areas. In the regions around Oaxaca and Veracruz, it is referred to as the anteburro. Panamanians, and Colombians call it macho de monte, and in Belize, where the Baird's tapir is the national animal, it is known as the mountain cow.
In Mexico, it is called tzemen in Tzeltal; in Lacandon, it is called cash-i-tzimin, meaning "jungle horse" and in Tojolab'al it is called niguanchan, meaning "big animal". In Panama, the Kunas people call the Baird's tapir moli in their colloquial language (Tule kaya), oloalikinyalilele, oloswikinyaliler, or oloalikinyappi in their political language (Sakla kaya), and ekwirmakka or ekwilamakkatola in their spiritual language (Suar mimmi kaya).


== DescriptionEdit ==

Baird's tapir has a distinctive cream-colored marking on its face and throat and a dark spot on each cheek, behind and below the eye. The rest of its hair is dark brown or grayish-brown. This tapir is the largest of the three American species and the largest native land mammal in both Central and South America. Baird's tapirs average 2 m (6.6 ft) in length but can range between 1.8 and 2.5 m (5.9 and 8.2 ft), not counting a stubby, vestigal tail of 513 cm (2.05.1 in), and 73120 cm (2.403.94 ft) in height. Body mass in adults can range from 150 to 400 kilograms (330 to 880 lb). Like the other species of tapirs, they have small, stubby tails and long, flexible proboscises. They have four toes on each front foot and three toes on each back foot.


== LifecycleEdit ==
The gestation period is about 400 days, after which one offspring is born. Multiple births are extremely rare. The babies, as with all species of tapir, have reddish-brown hair with white spots and stripes, a camouflage which affords them excellent protection in the dappled light of the forest. This pattern eventually fades into the adult coloration.
For the first week of their lives, infant Baird's tapirs are hidden in secluded locations while their mothers forage for food and return periodically to nurse them. Later, the young follow their mothers on feeding expeditions. At three weeks of age, the young are able to swim. Weaning occurs after one year, and sexual maturity is usually reached six to 12 months later. Baird's tapirs can live for over 30 years.


== BehaviorEdit ==

Baird's tapir may be active at all hours, but is primarily nocturnal. It forages for leaves and fallen fruit, using well-worn tapir paths which zig-zag through the thick undergrowth of the forest. The animal usually stays close to water and enjoys swimming and wading  on especially hot days, individuals will rest in a watering hole for hours with only their heads above water.
It generally leads a solitary life, though feeding groups are not uncommon and individuals, especially those of different ages (young with their mothers, juveniles with adults) are often observed together. The animals communicate with one another through shrill whistles and squeaks.
Adults can be potentially dangerous to humans and should not be approached if spotted in the wild. The animal is most likely to follow or chase a human for a bit, though they have been known to charge and gore humans on rare occasions.


== Predation and vulnerabilityEdit ==

According to the IUCN, Baird's tapir is in danger of extinction, and in 1996 it was officially classified as "Vulnerable". There are two main contributing factors in the decline of the species which are poaching and habitat loss. Though in many areas the animal is only hunted by a few humans, any loss of life is a serious blow to the tapir population, especially because their reproductive rate is so slow.
In Mexico, Belize, Guatemala, Costa Rica, and Panama, hunting of the Baird's tapirs is illegal, but the laws protecting them are often unenforced. Furthermore, restrictions against hunting do not address the problem of deforestation. Therefore, many conservationists focus on environmental education and sustainable forestry to try to save the Baird's tapir and other rainforest species from extinction.
Attacks on humans are rare and normally in self-defense. In 2006, Carlos Manuel Rodrguez Echandi, the former Costa Rican Minister of Environment and Energy, was attacked and injured by a tapir after he followed it off the trail.
An adult Baird's tapir, being such a massive mammal, has very few natural predators. Only large adult American crocodiles (4 metres or 13 feet or more) and adult jaguars are capable of preying on tapirs, although even in these cases the outcomes are unpredictable and often in the tapir's favor (as is evident on multiple tapirs documented in Corcovado National Park with large claw marks covering their hides).


== ReferencesEdit ==


== External linksEdit ==
ARKive  images and movies of the Bairds Tapir (Tapirus bairdii)
Tapir Specialist Group  Bairds Tapir