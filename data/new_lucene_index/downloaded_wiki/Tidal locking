Tidal locking (also called gravitational locking or captured rotation) occurs when the gravitational gradient makes one hemisphere of a revolving astronomical body constantly face the partner body. This effect is known as synchronous rotation. A tidally locked body takes just as long to rotate around its own axis as it does to revolve around its partner. For example, the same side of the Moon always faces the Earth. Usually, only the satellite is tidally locked to the larger body. However, if both the mass difference between the two bodies and the distance between them are relatively small, each may be tidally locked to the other; this is the case for Pluto and Charon. This effect is employed to stabilize some artificial satellites.


== Mechanism ==
The change in rotation rate necessary to tidally lock a body B to a larger body A is caused by the torque applied by A's gravity on bulges it has induced on B by tidal forces.
The gravity of body A produces a tidal force on B that distorts its gravitational equilibrium shape slightly so that it becomes elongated along the axis oriented toward A, and conversely, is slightly reduced in dimension in directions orthogonal to this axis. These distortions are known as tidal bulges. When B is not yet tidally locked, the bulges travel over its surface, with one of the two "high" tidal bulges traveling close to the point where body A is overhead. For large astronomical bodies that are nearly spherical due to self-gravitation, the tidal distortion produces a slightly prolate spheroid, i.e. an axially symmetric ellipsoid that is elongated along its major axis. Smaller bodies also experience distortion, but this distortion is less regular.
The material of B exerts resistance to this periodic reshaping caused by the tidal force. In effect, some time is required to reshape B to the gravitational equilibrium shape, by which time the forming bulges have already been carried some distance away from the AB axis by B's rotation. Seen from a vantage point in space, the points of maximum bulge extension are displaced from the axis oriented toward A. If B's rotation period is shorter than its orbital period, the bulges are carried forward of the axis oriented toward A in the direction of rotation, whereas if B's rotation period is longer the bulges lag behind instead.

Because the bulges are now displaced from the AB axis, A's gravitational pull on the mass in them exerts a torque on B. The torque on the A-facing bulge acts to bring B's rotation in line with its orbital period, whereas the "back" bulge, which faces away from A, acts in the opposite sense. However, the bulge on the A-facing side is closer to A than the back bulge by a distance of approximately B's diameter, and so experiences a slightly stronger gravitational force and torque. The net resulting torque from both bulges, then, is always in the direction that acts to synchronize B's rotation with its orbital period, leading eventually to tidal locking.


=== Orbital changes ===

The angular momentum of the whole AB system is conserved in this process, so that when B slows down and loses rotational angular momentum, its orbital angular momentum is boosted by a similar amount (there are also some smaller effects on A's rotation). This results in a raising of B's orbit about A in tandem with its rotational slowdown. For the other case where B starts off rotating too slowly, tidal locking both speeds up its rotation, and lowers its orbit.


=== Locking of the larger body ===

The tidal locking effect is also experienced by the larger body A, but at a slower rate because B's gravitational effect is weaker due to B's smaller size. For example, Earth's rotation is gradually slowing down because of the Moon, by an amount that becomes noticeable over geological time as revealed in the fossil record. The process is still happening and has significantly slowed down the rotation of Earth over time. Current estimations are that this (together with the tidal influence of the Sun) has helped lengthen the Earth day from about 6 hours to the current 24 hours. Currently, atomic clocks show that Earth's day lengthens by about 15 microseconds every year. Given enough time, this would create a mutual tidal locking between Earth and the Moon, where the length of a day has increased and the length of a lunar month has shortened until the two are the same. However, Earth is not expected to become tidally locked to the Moon before the Sun becomes a red giant and engulfs Earth and the Moon.
For bodies of similar size the effect may be of comparable size for both, and both may become tidally locked to each other on a much shorter timescale. The dwarf planet Pluto and its satellite Charon are good examples of this. They have already reached a state where Charon is only visible from one hemisphere of Pluto and vice versa. Pluto is roughly the same age as the planets in the Solar System.


=== Rotationorbit resonance ===
Finally, in some cases where the orbit is eccentric and the tidal effect is relatively weak, the smaller body may end up in a so-called spin-orbit resonance, rather than being tidally locked. Here, the ratio of the rotation period of a body to its own orbital period is some simple fraction different from 1:1. A well known case is the rotation of Mercury, which is locked to its own orbit around the Sun in a 3:2 resonance.
Many exoplanets (especially the close-in ones) are expected to be in spinorbit resonances higher than 1:1.


== Occurrence ==


=== Moons ===

Most significant moons in the Solar System are tidally locked with their primaries, because they orbit very closely and tidal force increases rapidly (as a cubic function) with decreasing distance. Notable exceptions are the irregular outer satellites of the gas giants, which orbit much farther away than the large well-known moons. Iapetus is tidally locked to Saturn.
Pluto and Charon are an extreme example of a tidal lock. Charon is a relatively large moon in comparison to its primary and also has a very close orbit. This results in Pluto and Charon being mutually tidally locked. Pluto's other moons are not tidally locked; Styx, Nix, Kerberos, and Hydra all rotate chaotically due to the influence of Charon.
The tidal locking situation for asteroid moons is largely unknown, but closely orbiting binaries are expected to be tidally locked, as well as contact binaries.


==== The Moon ====

The Moon's rotation and orbital periods are tidally locked with each other, so no matter when the Moon is observed from Earth the same hemisphere of the Moon is always seen. The far side of the Moon was not seen until 1959, when photographs of most of the far side were transmitted from the Soviet spacecraft Luna 3.
Despite the Moon's rotational and orbital periods being exactly locked, about 59% of the Moon's total surface may be seen with repeated observations from Earth due to the phenomena of libration and parallax. Librations are primarily caused by the Moon's varying orbital speed due to the eccentricity of its orbit: this allows up to about 6 more along its perimeter to be seen from Earth. Parallax is a geometric effect: at the surface of Earth we are offset from the line through the centers of Earth and Moon, and because of this we can observe a bit (about 1) more around the side of the Moon when it is on our local horizon.


=== Planets ===
It was thought for some time that Mercury was tidally locked with the Sun. This was because whenever Mercury was best placed for observation, the same side faced inward. Radar observations in 1965 demonstrated instead that Mercury has a 3:2 spinorbit resonance, rotating three times for every two revolutions around the Sun, which results in the same positioning at those observation points. The eccentricity of Mercury's orbit makes this 3:2 resonance stable.
Venus's 583.92-day interval between successive close approaches to Earth is equal to 5.001444 Venusian solar days, making approximately the same face visible from Earth at each close approach. Whether this relationship arose by chance or is the result of some kind of tidal locking with Earth is unknown.
A planet that is tidally locked to its star has one side that is in perpetual starlight (i.e. "sunlight") and another that is in perpetual darkness.


=== Stars ===
Close binary stars throughout the universe are expected to be tidally locked with each other, and extrasolar planets that have been found to orbit their primaries extremely closely are also thought to be tidally locked to them. An unusual example, confirmed by MOST, is Tau Botis, a star tidally locked by a planet. The tidal locking is almost certainly mutual.


== Timescale ==
An estimate of the time for a body to become tidally locked can be obtained using the following formula:

where
 is the initial spin rate (radians per second)
 is the semi-major axis of the motion of the satellite around the planet (given by average of perigee and apogee distances)
  is the moment of inertia of the satellite.
 is the dissipation function of the satellite.
 is the gravitational constant
 is the mass of the planet
 is the mass of the satellite
 is the tidal Love number of the satellite
 is the mean radius of the satellite.
 and  are generally very poorly known except for the Moon, which has . For a really rough estimate it is common to take  (perhaps conservatively, giving overestimated locking times), and

where
 is the density of the satellite
 is the surface gravity of the satellite
 is the rigidity of the satellite. This can be roughly taken as 31010 Nm2 for rocky objects and 4109 Nm2 for icy ones.
Even knowing the size and density of the satellite leaves many parameters that must be estimated (especially w, Q, and ), so that any calculated locking times obtained are expected to be inaccurate, even to factors of ten. Further, during the tidal locking phase the semi-major axis  may have been significantly different from that observed nowadays due to subsequent tidal acceleration, and the locking time is extremely sensitive to this value.
Because the uncertainty is so high, the above formulas can be simplified to give a somewhat less cumbersome one. By assuming that the satellite is spherical, , and it is sensible to guess one revolution every 12 hours in the initial non-locked state (most asteroids have rotational periods between about 2 hours and about 2 days)

with masses in kilograms, distances in meters, and  in newtons per meter squared;  can be roughly taken as 31010 Nm2 for rocky objects and 4109 Nm2 for icy ones.
There is an extremely strong dependence on semi-major axis .
For the locking of a primary body to its satellite as in the case of Pluto, the satellite and primary body parameters can be interchanged.
One conclusion is that, other things being equal (such as  and ), a large moon will lock faster than a smaller moon at the same orbital distance from the planet because  grows as the cube of the satellite radius,. A possible example of this is in the Saturn system, where Hyperion is not tidally locked, whereas the larger Iapetus, which orbits at a greater distance, is. However, this is not clear cut because Hyperion also experiences strong driving from the nearby Titan, which forces its rotation to be chaotic.
The above formulae for the timescale of locking may be off by orders of magnitude, because they ignore the frequency dependence of .


== List of known tidally locked bodies ==


=== Solar System ===
Locked to the Earth
Moon
Locked to Mars
Phobos
Deimos
Locked to Jupiter

Locked to Saturn

Locked to Uranus

Locked to Neptune
Proteus
Triton
Locked to Pluto
Charon (Pluto is itself locked to Charon)


=== Extra-solar ===
Tau Botis is known to be locked to the close-orbiting giant planet Tau Botis b.


== Bodies likely to be locked ==


=== Solar System ===
Based on comparison between the likely time needed to lock a body to its primary, and the time it has been in its present orbit (comparable with the age of the Solar System for most planetary moons), a number of moons are thought to be locked. However their rotations are not known or not known enough. These are:
Probably locked to Saturn

Probably locked to Uranus

Probably locked to Neptune


=== Extrasolar ===
Alpha Centauri Bb may be tidally locked to its parent star Alpha Centauri.
Gliese 581 c, Gliese 581 g, Gliese 581 b, and Gliese 581 e may be tidally locked to their parent star Gliese 581. Gliese 581 d is almost certainly locked either into the 2:1 or the 3:2 spinorbit resonance with the same star.


== See also ==
Tidal acceleration
Gravity-gradient stabilization
Orbital resonance
Synchronous orbit


== References ==