The chemical elements can be broadly divided into metals, metalloids and nonmetals according to their shared physical and chemical properties. All metals have a shiny appearance (at least when freshly polished); are good conductors of heat and electricity; form alloys with other metals; and have at least one basic oxide. Metalloids are metallic-looking brittle solids that are either semiconductors or exist in semiconducting forms, and have amphoteric or weakly acidic oxides. Typical nonmetals have a dull, coloured or colourless appearance; are brittle when solid; are poor conductors of heat and electricity; and have acidic oxides. Most or some elements in each category share a range of other properties; a few elements have properties that are either anomalous given their category, or otherwise extraordinary.


== Shared properties ==


=== Metals ===

Metals appear lustrous (beneath any patina); form mixtures (alloys) when combined with other metals; tend to lose or share electrons when they react with other substances; and each forms at least one predominately basic oxide.
Most metals are silvery looking, high density, relatively soft and easily deformed solids with good electrical and thermal conductivity, closely packed structures, low ionisation energies and electronegativities, and are found naturally in combined states.
Some metals appear coloured (Cu, Cs, Au), have low densities (e.g. Be, Al) or very high melting points, are liquids at or near room temperature, are brittle (e.g. Os, Bi), not easily machined (e.g. Ti, Re), or are noble (hard to oxidise) or have nonmetallic structures (Mn and Ga are structurally analogous to, respectively, white P and I).
Metals comprise the large majority of the elements, and can be subdivided into several different categories. From left to right in the periodic table, these categories include the highly reactive alkali metals; the less reactive alkaline earth metals, lanthanides and radioactive actinides; the archetypal transition metals, and the physically and chemically weak post-transition metals. Specialized subcategories such as the refractory metals and the noble metals also exist.


=== Metalloids ===

Metalloids are metallic looking brittle solids; tend to share electrons when they react with other substances; have weakly acidic or amphoteric oxides; and are usually found naturally in combined states.
Most are semiconductors, and moderate thermal conductors, and have structures that are more open than those of most metals.
Some metalloids (As, Sb) conduct electricity like metals.
The metalloids, as the smallest major category of elements, are not subdivided further.


=== Nonmetals ===

Nonmetals have open structures (unless solidified from gaseous or liquid forms); tend to gain or share electrons when they react with other substances; and do not form distinctly basic oxides.
Most are gases at room temperature; have relatively low densities; are poor electrical and thermal conductors; have relatively high ionisation energies and electronegativities; form acidic oxides; and are found naturally in uncombined states in large amounts.
Some nonmetals (C, black P, S and Se) are brittle solids at room temperature (although each of these also have malleable, pliable or ductile allotropes).
From left to right in the periodic table, the nonmetals can be subdivided into the polyatomic nonmetals which, being nearest to the metalloids, show some incipient metallic character; the diatomic nonmetals, which are essentially nonmetallic; and the monatomic noble gases, which are almost completely inert.


== Comparison of properties ==

The characteristic properties of metals and nonmetals are quite distinct, as shown in the table below. Metalloids, straddling the metal-nonmetal border, are mostly distinct from either, but in a few properties resemble one or the other, as shown in the shading of the metalloid column below and summarized in the small table at the top of this section.
Authors differ in where they divide metals from nonmetals and in whether they recognize an intermediate metalloid category. Some authors count metalloids as nonmetals with weakly nonmetallic properties. Others count some of the metalloids as post-transition metals with weakly metallic properties.


== Anomalous properties ==

Within each category, elements can be found with one or two properties very different from the expected norm, or that are otherwise notable.


=== Metals ===
Sodium, potassium, rubidium, caesium, barium, platinum, gold

The common notions that "alkali metal ions (group 1A) always have a +1 charge" and that "transition elements do not form anions" are textbook errors. The synthesis of a crystalline salt of the sodium anion Na was reported in 1974. Since then further compounds ("alkalides") containing anions of all other alkali metals except Li and Fr, as well as that of Ba, have been prepared. In 1943, Sommer reported the preparation of the yellow transparent compound CsAu. This was subsequently shown to consist of caesium cations (Cs+) and auride anions (Au) although it was some years before this conclusion was accepted. Several other aurides (KAu, RbAu) have since been synthesized, as well as the red transparent compound Cs2Pt which was found to contain Cs+ and Pt2 ions.

Manganese

Well-behaved metals have crystal structures featuring unit cells with up to four atoms. Manganese has a complex crystal structure with a 58-atom unit cell, effectively four different atomic radii, and four different coordination numbers (10, 11, 12 and 16). It has been described as resembling "a quaternary intermetallic compound with four Mn atom types bonding as if they were different elements." The half-filled 3d shell of manganese appears to be the cause of the complexity. This confers a large magnetic moment on each atom. Below 727 C, a unit cell of 58 spatially diverse atoms represents the energetically lowest way of achieving a zero net magnetic moment. The crystal structure of manganese makes it a hard and brittle metal, with low electrical and thermal conductivity. At higher temperatures "greater lattice vibrations nullify magnetic effects" and manganese adopts less complex structures.

Iron, cobalt, nickel, gadolinium, dysprosium

The only elements strongly attracted to magnets are iron, cobalt, and nickel at room temperature, gadolinium just below, and dysprosium at ultra cold temperatures (below 185 C).

Iridium

The only element encountered with an oxidation state of +9 is iridium, in the [IrO4]+ cation. Other than this, the highest known oxidation state is +8, in Ru, Xe, Os, Ir, Pu, Cm, and Hs.

Gold

The malleability of gold is extraordinary: a fist sized lump can be hammered and separated into one million paper back sized sheets, each 10 nm thick, 1600 times thinner than regular kitchen aluminum foil (0.016 mm thick).

Mercury
Bricks and bowling balls will float on the surface of mercury thanks to it having a density 13.5 times that of water. Equally, a solid mercury bowling ball would weigh around 50 pounds and, if it could be kept cold enough, would float on the surface of liquid gold.
The only metal having an ionisation energy higher than some nonmetals (sulfur and selenium) is mercury.
Mercury and its compounds have a reputation for toxicity but on a scale of 1 to 10, dimethylmercury ((CH3)2Hg) (abbr. DMM), a volatile colourless liquid, has been described as a 15. It is so dangerous that scientists have been encouraged to use less toxic mercury compounds wherever possible. In 1997, Karen Wetterhahn, a professor of chemistry specialising in toxic metal exposure, died of mercury poisoning ten months after a few drops of DMM landed on her "protective" latex gloves. Although Wetterhahn had been following the then published procedures for handling this compound, it passed through her gloves and skin within seconds. It is now known that DMM is exceptionally permeable to (ordinary) gloves, skin and tissues. And its toxicity is such that less than one-tenth of a ml applied to the skin will be seriously toxic.
Lead

The expression, to "go down like a lead balloon" is anchored in the common view of lead as a dense, heavy metalbeing nearly as dense as mercury. However, it is possible to construct a balloon made of lead foil, filled with a helium and air mixture, which will float and be buoyant enough to carry a small load.

Bismuth

Bismuth has the longest half life of any naturally occurring element; its only primordial isotope, bismuth-209, was found in 2003 to be slightly radioactive, decaying via alpha decay with a half life more than a billion times the estimated age of the universe. Prior to this discovery, Bi-209 was thought to be the heaviest naturally occurring stable isotope; this distinction now belongs to lead.

Uranium

The only element with a naturally occurring isotope capable of undergoing nuclear fission is uranium. The capacity of U-235 to undergo fission was first suggested (and ignored) in 1934, and subsequently discovered in 1938.

Plutonium

It is a commonly held belief that metals reduce their electrical conductivity when heated. Plutonium increases its electrical conductivity when heated in the temperature range of around 175 to +125 C.


=== Metalloids ===
Boron

Boron is the only element with a partially disordered structure in its most thermodynamically stable crystalline form.

Boron, antimony

These elements are record holders within the field of superacid chemistry. For seven decades, fluorosulfonic acid HSO3F and trifluoromethanesulfonic acid CF3SO3H were the strongest known acids that could be isolated as single compounds. Both are about a thousand times times more acidic than pure sulfuric acid. In 2004, a boron compound broke this record by a thousand fold with the synthesis of carborane acid H(CHB11Cl11). Another metalloid, antimony, features in the strongest known acid, a mixture 10 billion times stronger than carborane acid. This is fluoroantimonic acid H2F[SbF6], a mixture of antimony pentafluoride SbF5 and hydrofluoric acid HF.

Silicon
The thermal conductivity of silicon is better than that of most metals.
A sponge-like porous form of silicon (p-Si) is typically prepared by the electrochemical etching of silicon wafers in a hydrofluoric acid solution. Flakes of p-Si sometimes appear red; it has a band gap of 1.972.1 eV. The many tiny pores in porous silicon give it an enormous internal surface area, up to 1,000 m2/cm3. When exposed to an oxidant, especially a liquid oxidant, the high surface-area to volume ratio of p-Si creates a very efficient burn, accompanied by nano-explosions, and sometimes by ball-lightning-like plasmoids with, for example, a diameter of 0.10.8 m, a velocity of up to 0.5 m/s and a lifetime of up to 1s. The first ever spectrographic analysis of a ball lightning event (in 2012) revealed the presence of silicon, iron and calcium, these elements also being present in the soil.
Arsenic

Metals are said to be fusible, resulting in some confusion in old chemistry as to whether arsenic was a true metal, or a nonmetal, or something in between. It sublimes rather than melts at standard atmospheric pressure, like the nonmetals carbon and red phosphorus.

Antimony

A high-energy explosive form of antimony was first obtained in 1858. It is prepared by the electrolysis of any of the heavier antimony trihalides (SbCl3, SbBr3, SbI3) in a hydrochloric acid solution at low temperature. It comprises amorphous antimony with some occluded antimony trihalide (720% in the case of the trichloride). When scratched, struck, powdered or heated quickly to 200 C, it "flares up, emits sparks and is converted explosively into the lower-energy, crystalline grey antimony."


=== Nonmetals ===
Hydrogen
Water (H2O), a well known oxide of hydrogen, is a spectacular anomaly. Extrapolating from the heavier hydrogen chalcogenides, namely hydrogen sulfide H2S, hydrogen selenide H2Se, and hydrogen telluride H2Te, water should be "a foul-smelling, poisonous, inflammable gascondensing to a nasty liquid [at] around 100 C". Instead, due to hydrogen bonding, water is "stable, potable, odorless, benign, andindispensable to life".
Less well known of the oxides of hydrogen is the trioxide, H2O3. Berthelot proposed the existence of this oxide in 1880 but his suggestion was soon forgotten as there was no way of testing it using the technology of the time. Hydrogen trioxide was prepared in 1994 by replacing the oxygen used in the industrial process for making hydrogen peroxide, with ozone. The yield is about 40 per cent, at 78 C; above around 40 C it decomposes into water and oxygen. Derivatives of hydrogen trioxide, such as F3COOOCF3 ("bis(trifluoromethyl) trioxide") are known; these are metastable at room temperature. Mendeleev went a step further, in 1895, and proposed the existence of hydrogen tetroxide HOOOOH as a transient intermediate in the decomposition of hydrogen peroxide; this was prepared and characterised in 1974, using a matrix isolation technique. Alkali metal ozonide salts of the unknown hydrogen ozonide (HO3) are also known; these have the formula MO3.
Helium
At temperatures below 0.3 and 0.8 K respectively, helium-3 and helium-4 each have a negative enthalpy of fusion. This means that, at the appropriate constant pressures, these substances freeze with the addition of heat.
Until 1999 helium was thought to be too small to form a cage clathratea compound in which a guest atom or molecule is encapsulated in a cage formed by a host moleculeat atmospheric pressure. In that year the synthesis of microgram quantities of He@C20H20 represented the first such helium clathrate and (what was described as) the world's smallest helium balloon.
Carbon
Graphite is the most electrically conductive nonmetal, better than some metals.
Diamond is the best natural conductor of heat; it even feels cold to the touch. Its thermal conductivity (2,200 W/mK) is five times greater than the most conductive metal (Ag at 429); 300 times higher than the least conductive metal (Pu at 6.74); and nearly 4,000 times that of water (0.58) and 100,000 times that of air (0.0224). This high thermal conductivity is used by jewelers and gemologists to separate diamonds from imitations.
Graphene aerogel, produced in 2012 by freeze-drying a solution of carbon nanotubes and graphite oxide sheets and chemically removing oxygen, is seven times lighter than air, and ten per cent lighter than helium. It is the lightest solid known (0.16 mg/cm3), conductive and elastic.
Phosphorus

The least stable and most reactive form of phosphorus is the white allotrope. It is a hazardous, highly flammable and toxic substance, spontaneously igniting in air and producing phosphoric acid residue. It is therefore normally stored under water. White phosphorus is also the most common, industrially important, and easily reproducible allotrope, and for these reasons is regarded as the standard state of phosphorus. The most stable form is the black allotrope, which is a metallic looking, brittle and relatively non-reactive semiconductor (unlike the white allotrope, which has a white or yellowish appearance, is pliable, highly reactive and a semiconductor). When assessing periodicity in the physical properties of the elements it needs to be borne in mind that the quoted properties of phosphorus tend to be those of its least stable form rather than, as is the case with all other elements, the most stable form.

Iodine

The mildest of the halogens, iodine is the active ingredient in tincture of iodine, a disinfectant. This can be found in household medicine cabinets or emergency survival kits. Tincture of iodine will rapidly dissolve gold, a task ordinarily requiring the use of aqua regia (a highly corrosive mixture of nitric and hydrochloric acids).


== Notes ==


== Citations ==


== References ==