In organic chemistry and biochemistry, a substituent is an atom or group of atoms substituted in place of a hydrogen atom on the parent chain of a hydrocarbon, becoming a moiety of the resultant new molecule. The terms substituent, side chain, group, branch, or pendant group are used almost interchangeably to describe branches from a parent structure, though certain distinctions are made in the context of polymer chemistry. In polymers, side chains extend from a backbone structure. In proteins, side chains are attached to the alpha carbon atoms of the amino acid backbone.
The suffix yl is used when naming organic compounds that contain a single bond replacing one hydrogen; -ylidene and -ylidyne are used with double bonds and triple bonds, respectively. In addition, when naming hydrocarbons that contain a substituent, positional numbers are used to indicate which carbon atom the substituent attaches to when such information is needed to distinguish between isomers. The polar effect exerted by a substituent is a combination of the inductive effect and the mesomeric effect. Additional steric effects result from the volume occupied by a substituent.
The phrases most-substituted and least-substituted are frequently used to describe molecules and predict their products. In this terminology, methane is used as a reference of comparison. Using methane as a reference, for each hydrogen atom that is replaced or "substituted" by something else, the molecule can be said to be more highly substituted. For example:
Markovnikov's rule predicts that the hydrogen adds to the carbon of the alkene functional group that has the greater number of hydrogen atoms (fewer alkyl substituents).
Zaitsev's rule predicts that the major reaction product is the alkene with the more highly substituted (more stable) double bond.


== Nomenclature ==
The suffix -yl is used in organic chemistry to form names of radicals, either separate or chemically bonded parts of molecules. It can be traced back to the old name of methanol, "methylene" (coined from Greek words methy = "wine" and hl = "wood"), which became shortened to "methyl" in compound names. Several reforms of chemical nomenclature eventually generalized the use of the suffix to other organic substituents.
The use of the suffix is determined by the number of hydrogen atoms that the substituent replaces on a parent compound (and also, usually, on the substituent). According to 1993 IUPAC guidelines:
-yl means that one hydrogen is replaced.
-ylidene means that two hydrogens are replaced by a double bond between parent and substituent.
-ylidyne means that three hydrogens are replaced by a triple bond between parent and substituent.
The suffix -ylidine (with "ine" instead of "yne" or "ene") is encountered sporadically, and appears to be a variant spelling of "-ylidene". It is not mentioned in IUPAC guidelines.
For multiple bonds of the same type, which link the substituent to the parent group, the prefixes di, tri, tetra, etc.are used: -diyl (two single bonds), -triyl (three single bonds), -tetrayl (four single bonds), -diylidene (two double bonds)
For multiple bonds of different bond types, multiple suffixes are added: -ylylidene (one single and one double), -ylylidyne (one single and one triple), -diylylidene (two single and one double)
The parent compound name can be altered in two ways.
For many common compounds the substituent is linked at one end (the 1 position), which is therefore not explicitly numbered in the formula.The substituent name is modified by stripping ane (see Alkane) and adding the appropriate suffix.This is "recommended only for saturated acyclic and monocyclic hydrocarbon substituent groups and for the mononuclear parent hydrides of silicon, germanium, tin, lead, and boron". Thus, if there is a carboxylic acid called "X-ic acid", an alcohol ending "X-anol" (or "X-yl alcohol"), or an alkane called "X-ane", then "X-yl" typically denotes the same carbon chain lacking these groups but modified by attachment to some other parent molecule.
The more general method omits only the terminal "e" of the substituent name, but requires explicit numbering of each yl prefix, even at position 1 (except for -ylidyne, which as a triple bond must terminate the substituent carbon chain). Pentan-1-yl is an example of a name by this method, and is synonymous with Pentyl from the previous guideline.
Note that some popular terms such as "vinyl" (when used to mean "polyvinyl") represent only a portion of the full chemical name.
The suffix "-yl" arose by extracting it from the word "methyl".


== Methane substituents ==
According to the above rules, a carbon atom in a molecule, considered as a substituent, has the following names depending on the number of hydrogens bound to it, and the type of bonds formed with the remainder of the molecule:


== Structures ==
In a chemical structural formula, an organic substituent such as methyl, ethyl, or aryl can be written as R (or R1, R2, etc.) This is a generic placeholder, the R derived from radical or rest, which may replace any portion of the formula as the author finds convenient. The first to use this symbol was Charles Frdric Gerhardt in 1844.
The symbol X is often used to denote electronegative substituents such as the halides.


== Statistical distribution ==
One cheminformatics study identified 849,574 unique substituents up to 12 non-hydrogen atoms large and containing only carbon, hydrogen, nitrogen, oxygen, sulfur, phosphorus, selenium, and the halogens in a set of 3,043,941 molecules. Fifty common substituents are found in only 1% of this set, and 438 in 0.1%. 64% of the substituents are unique to just one molecule. The top 5 consists of the phenyl, chlorine, methoxy, hydroxyl, and ethyl substituent. The total number of organic substituents in organic chemistry is estimated at 3.1 million, creating a total of 6.71023 molecules. An infinite number of substituents can be obtained simply by increasing carbon chain length. For instance, the substituents methyl (-CH3) and pentyl (-C5H11).


== See also ==
Functional groups are a subset of substituents


== References ==