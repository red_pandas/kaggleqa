Tiktaalik /tktlk/ is a monospecific genus of extinct sarcopterygian (lobe-finned fish) from the late Devonian period, about 375 Ma (million years) ago, having many features akin to those of tetrapods (four-legged animals).
Tiktaalik has a possibility of being a representative of the evolutionary transition from fish to amphibians. It is an example from several lines of ancient sarcopterygian fish developing adaptations to the oxygen-poor shallow-water habitats of its time, environmental conditions which are thought to have led to the evolution of tetrapods.
It and similar animals may possibly be the common ancestors of the broad swath of all terrestrial fauna: amphibians, reptiles, birds, and mammals. The first well-preserved Tiktaalik fossils were found in 2004 on Ellesmere Island in Nunavut, Canada.


== Etymology ==
The name Tiktaalik is an Inuktitut word meaning "burbot", a freshwater fish related to true cod. The "fishapod" genus received this name after a suggestion by Inuit elders of Canada's Nunavut Territory, where the fossil was discovered. The specific name roseae cryptically honours an anonymous donor. Taking a detailed look at the internal head skeleton of Tiktaalik roseae, in the October 16, 2008, issue of Nature, researchers show how Tiktaalik was gaining structures that could allow it to support itself on solid ground and breathe air, a key intermediate step in the transformation of the skull that accompanied the shift to life on land by our distant ancestors.


== Description ==

Tiktaalik provides insights on the features of the extinct closest relatives of the tetrapods. Unlike many previous, more fishlike transitional fossils, the "fins" of Tiktaalik have basic wrist bones and simple rays reminiscent of fingers. The homology of distal elements is uncertain; there have been suggestions that they are homologous to digits, although this is incompatible with the digital arch developmental model because digits are supposed to be postaxial structures, and only three of the (reconstructed) eight rays of Tiktaalik are postaxial.
However, the proximal series can be directly compared to the ulnare and intermedium of tetrapods. The fin was clearly weight bearing, being attached to a massive shoulder with expanded scapular and coracoid elements and attached to the body armor, large muscular scars on the ventral surface of the humerus, and highly mobile distal joints. The bones of the forefins show large muscle facets, suggesting that the fin was both muscular and had the ability to flex like a wrist joint. These wrist-like features would have helped anchor the creature to the bottom in fast moving current.

Also notable are the spiracles on the top of the head, which suggest the creature had primitive lungs as well as gills. This attribute would have been useful in shallow water, where higher water temperature would lower oxygen content. This development may have led to the evolution of a more robust ribcage, a key evolutionary trait of land-living creatures. The more robust ribcage of Tiktaalik would have helped support the animals body any time it ventured outside a fully aquatic habitat. Tiktaalik also lacked a characteristic that most fishes havebony plates in the gill area that restrict lateral head movement. This makes Tiktaalik the earliest known fish to have a neck, with the pectoral girdle separate from the skull. This would give the creature more freedom in hunting prey either on land or in the shallows.
Tiktaalik is sometimes compared to gars (esp. Atractosteus spatula, the alligator gar) of the Lepisosteidae family, with whom it shares a number of characteristics:
diamond-shaped scale patterns common to the Crossopterygii class (in both species scales are rhombic, overlapping and tuberculated);
teeth structured in two rows;
both internal and external nostrils;
tubular and streamlined body;
absence of anterior dorsal fin;
broad, dorsoventrally compressed skull;
paired frontal bones;
marginal nares;
subterminal mouth;
lung-like organ.


== Paleobiology ==

Tiktaalik generally had the characteristics of a lobe-finned fish, but with front fins featuring arm-like skeletal structures more akin to those of a crocodile, including a shoulder, elbow, and wrist. The fossil discovered in 2004 did not include the rear fins and tail. It had rows of sharp teeth of a predator fish, and its neck could move independently of its body, which is not common in other fish (Tarrasius, Mandageria, placoderms, and extant seahorses being some exceptions; see also Lepidogalaxias and Channallabes apus). The animal had a flat skull resembling a crocodile's; eyes on top of its head, suggesting that it spent a lot of time looking up; a neck and ribs similar to those of tetrapods, with the ribs being used to support its body and aid in breathing via lungs; well developed jaws suitable for catching prey; and a small gill slit called a spiracle that, in more derived animals, became an ear.
The fossils were found in the "Fram Formation", deposits of meandering stream systems near the Devonian equator, suggesting a benthic animal that lived on the bottom of shallow waters and perhaps even out of the water for short periods, with a skeleton indicating that it could support its body under the force of gravity whether in very shallow water or on land. At that period, for the first time, deciduous plants were flourishing and annually shedding leaves into the water, attracting small prey into warm oxygen-poor shallows that were difficult for larger fish to swim in. The discoverers said that in all likelihood, Tiktaalik flexed its proto-limbs primarily on the floor of streams and may have pulled itself onto the shore for brief periods. In 2014, the discovery of the animal's pelvic girdle was announced; it was strongly built, indicating the animal could have used them for moving in shallow water and across mudflats. Neil Shubin and Ted Daeschler, the leaders of the team, have been searching Ellesmere Island for fossils since 2000

We're making the hypothesis that this animal was specialized for living in shallow stream systems, perhaps swampy habitats, perhaps even to some of the ponds. And maybe occasionally, using its very specialized fins, for moving up overland. And that's what is particularly important here. The animal is developing features which will eventually allow animals to exploit land.


== Classification and evolution ==

Tiktaalik roseae is the only species classified under the genus. Tiktaalik lived approximately 375 million years ago. Paleontologists suggest that it is representative of the transition between non-tetrapod vertebrates (fish) such as Panderichthys, known from fossils 380 million years old, and early tetrapods such as Acanthostega and Ichthyostega, known from fossils about 365 million years old. Its mixture of primitive fish and derived tetrapod characteristics led one of its discoverers, Neil Shubin, to characterize Tiktaalik as a "fishapod".
Tiktaalik is a transitional fossil; it is to tetrapods what Aurornis is to birds, troodonts and dromaeosaurids. While it may be that neither is ancestor to any living animal, they serve as evidence that intermediates between very different types of vertebrates did once exist. The mixture of both fish and tetrapod characteristics found in Tiktaalik include these traits:
Fish
fish gills
fish scales
fish fins

"Fishapod"
half-fish, half-tetrapod limb bones and joints, including a functional wrist joint and radiating, fish-like fins instead of toes
half-fish, half-tetrapod ear region

Tetrapod
tetrapod rib bones
tetrapod mobile neck with separate pectoral girdle
tetrapod lungs


== Phylogenetic position ==


=== 2006 - 2010 ===
The phylogenetic analysis by Daeschler et al. placed Tiktaalik as a sister taxon to Elpistostege and directly below Panderichthys preceded by Eusthenopteron. Tiktaalik was thus inserted above Acanthostega and Ichthyostega as a transitional form  and a true "missing link".
Such order of the phylogenetic tree was initially adopted by other experts, most notably by Per Ahlberg and Jennifer Clack. However, it was questioned in a 2008 paper by Boisvert at al. who noted that Panderichthys, due to its more derived distal portion, might be closer to tetrapods than Tiktaalik or even that it was convergent with tetrapods. Ahlberg, co-author of the study, considered the possibility of Tiktaalik's fin having been "an evolutionary return to a more primitive form."


=== 2010 - now ===
In January 2010, a group of paleontologists (including Ahlberg) published a paper accompanied by extensive supplementary material (discussed also in a Nature documentary) which showed that first tetrapods appeared long before Tiktaalik and other elpistostegids. Their conclusions were based on numerous trackways (esp. Muz. PGI 1728.II.16) and individual footprints (esp. Muz. PGI 1728.II.1) discovered at the Zachemie quarry in the Holy Cross Mountains (Poland). A tetrapod origin of those tracks was established based on:
distinct digits and limb morphology;
trackways reflecting quadrupedal gait and diagonal walk;
no body or tail drag marks;
very wide stride in relation to body length (much beyond that of Tiktaalik or any other fish);
various size footprints with some unusually big (up to 26 cm wide) indicating body lengths of over 2.5 m.
Track-bearing layers were assigned to the lower-middle Eifelian based on conodont index fossil samples (costatus Zone) and "previous biostratigraphic data obtained from the underlying and overlying strata" with subsequent studies confirming this dating.

Both Titaalik's discoverers were skeptical about the Zachelmie trackways. Edward Daeschler said that trace evidence was not enough for him to modify the theory of tetrapod evolution, while Neil Shubin argued that Tiktaalik could have produced very similar footprints (in a later study Shubin expressed a significantly modified opinion that some of the Zachelmie footprints, those which lacked digits, may have been made by walking fish). However, Ahlberg insisted that those tracks could not have possibly been formed either by natural processes or by transitional species such as Tiktaalik or Panderichthys. Instead, the authors of the publication suggested ichthyostegalians as trackmakers, based on available pes morphology of those animals. However, a 2012 study indicated that Zachelmie trackmakers were even more advanced than Ichthyostega in terms of quadrupedalism. Grzegorz Niedwiedzki's reconstruction of one of the trackmakers was identical to that of Tulerpeton.
Prof. Narkiewicz, co-author of the article on the Zachelmie trackways, claimed that the Polish "discovery has disproved the theory that elpistostegids were the ancestors of tetrapods", a notion partially shared by Philippe Janvier. There have been a number of new hypotheses suggested as to a possible origin and phylogenetic position of the elpistostegids (including Tiktaalik):
their phylogenetic position remains unchanged and the footprints found in the Holy Cross Mountains are attributed to tetrapods but as a result there are at least six long ghost lineages separating Zachelmie trackmakers from various elpistostegalian and ichthyostegalian species;
they were "late-surviving relics rather than direct transitional forms";
they were "an evolutionary dead-end";
they were a result of convergent or parallel evolution so that apomorphies and striking anatomical similarities found in both digited tetrapods and elpistostegalians evolved at least twice.
It should be noted that convergency is considered responsible for uniquely tetrapod features found also in other non-elpistostegalian fish from the period like Sauripterus (finger-like jointed distal radial bones) or Tarrasius (tetrapod-like spine with 5 axial regions).
Estimates published after the discovery of Zachelmie tracks suggested that digited tetrapods may have appeared as early as 427.4 Ma ago and questioned attempts to read absolute timing of evolutionary events in early tetrapod evolution from stratigraphy.
Until more data becomes available, the phylogenetic position of Tiktaalik and other elpistostegids remains uncertain.


== Discovery ==

In 2004, three fossilized Tiktaalik skeletons were discovered in rock formed from late Devonian river sediments on Ellesmere Island, Nunavut, in northern Canada. Estimated ages reported at 375 Ma ago, 379 Ma ago, and 383 Ma ago. At the time of the species' existence, Ellesmere Island was part of the continent Laurentia (modern eastern North America and Greenland), which was centered on the equator and had a warm climate. When discovered, one of the skulls was found sticking out of a cliff. Upon further inspection, the fossil was found to be in excellent condition for a 375-million-year-old specimen.
The discovery, made by Edward B. Daeschler of the Academy of Natural Sciences, Neil H. Shubin from the University of Chicago, and Harvard University Professor Farish A. Jenkins, Jr, was published in the April 6, 2006, issue of Nature and quickly recognized as a transitional form. Jennifer A. Clack, a Cambridge University expert on tetrapod evolution, said of Tiktaalik, "It's one of those things you can point to and say, 'I told you this would exist,' and there it is."

After five years of digging on Ellesmere Island, in the far north of Nunavut, they hit pay dirt: a collection of several fish so beautifully preserved that their skeletons were still intact. As Shubin's team studied the species they saw to their excitement that it was exactly the missing intermediate they were looking for. 'We found something that really split the difference right down the middle,' says Daeschler.


== See also ==

Walking fish
Crocodilefish (disambiguation)
Amphibious fish
Spotted handfish
Other lobe-finned fish found in fossils from the Devonian period:
Coelacanth
Eusthenopteron
Gogonasus
Ichthyostega
Panderichthys


== References ==


== External links ==
University of Chicago website dedicated to the discovery
Interview with Neil Shubin on The Inoculated Mind, February 12, 2008.
Interview with Neil Shubin on Tech Nation where he discusses the discovery of the Tiktaalik, February 14, 2008.
Lecture (presentation) by Neil Shubin about the discovery of Tiktaalik on YouTube
Fishapod stars in music video, YouTube. Accessed on December 27, 2008.
Finding Tiktaalik: Interview with Neil Shubin, Royal Institution video, February 2013