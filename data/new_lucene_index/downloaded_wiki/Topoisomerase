Topoisomerases are enzymes that regulate the overwinding or underwinding of DNA. The winding problem of DNA arises due to the intertwined nature of its double-helical structure. During DNA replication and transcription, DNA becomes overwound ahead of a replication fork. If left unabated, this tension would eventually stop the ability of RNA & DNA polymerase involved in these processes to continue down the DNA strand.
In order to prevent and correct these types of topological problems caused by the double helix, topoisomerases bind to either single-stranded or double-stranded DNA and cut the phosphate backbone of the DNA. This intermediate break allows the DNA to be untangled or unwound, and, at the end of these processes, the DNA backbone is resealed again. Since the overall chemical composition and connectivity of the DNA do not change, the tangled and untangled DNAs are chemical isomers, differing only in their global topology, thus their name. Topoisomerases are isomerase enzymes that act on the topology of DNA.
Bacterial topoisomerase and human topoisomerase proceed via the same mechanism for replication and transcription.


== DiscoveryEdit ==
James C. Wang was the first to discover a topoisomerase when he identified E. coli topoisomerase I. Topo EC-codes are as follows: type I, EC 5.99.1.2; type II: EC 5.99.1.3. His discovery was made in the 1970s.


== FunctionEdit ==
The double-helical configuration that DNA strands naturally reside, makes them difficult to separate and yet they must be separated by helicase enzymes, if other enzymes are to transcribe the sequences that encode proteins, or if chromosomes are to be replicated. In so-called circular DNA, in which double-helical DNA is bent around and joined in a circle, the two strands are topologically linked, or knotted. Otherwise identical loops of DNA, having different numbers of twists, are topoisomers, and cannot be interconverted by any process that does not involve the breaking of DNA strands. Topoisomerases catalyze and guide the unknotting or unkinking of DNA by creating transient breaks in the DNA using a conserved Tyrosine as the catalytic residue.
The insertion of (viral) DNA into chromosomes and other forms of recombination can also require the action of topoisomerases.


== Clinical significanceEdit ==
See also topoisomerase inhibitor
Many drugs operate through interference with the topoisomerases [1]. The broad-spectrum fluoroquinolone antibiotics act by disrupting the function of bacterial type II topoisomerases. These small molecule inhibitors act as efficient anti-bacterial agents by hijacking the natural ability of topoisomerase to create breaks in chromosomal DNA.
Some chemotherapy drugs called topoisomerase inhibitors work by interfering with mammalian-type eukaryotic topoisomerases in cancer cells. This induces breaks in the DNA that ultimately lead to programmed cell death (apoptosis). This DNA-damaging effect, outside of its potential curative properties, may lead to secondary neoplasms in the patient.
Topoisomerase I is the antigen recognized by Anti Scl-70 antibodies in scleroderma.


== Topological problemsEdit ==
There are three main types of topology: supercoiling, knotting, and catenation. Outside of the essential processes of replication or transcription, DNA must be kept as compact as possible, and these three states help this cause. However, when transcription or replication occurs, DNA must be free, and these states seriously hinder the processes. In addition, during replication, the newly replicated duplex of DNA and the original duplex of DNA become intertwined and must be completely separated in order to ensure genomic integrity as a cell divides. As a transcription bubble proceeds, DNA ahead of the transcription fork becomes overwound, or positively supercoiled, while DNA behind the transcription bubble becomes underwound, or negatively supercoiled. As replication occurs, DNA ahead of the replication bubble becomes positively supercoiled, while DNA behind the replication fork becomes entangled forming precatenanes. One of the most essential topological problems occurs at the very end of replication, when daughter chromosomes must be fully disentangled before mitosis occurs. Topoisomerase IIA plays an essential role in resolving these topological problems.


== ClassesEdit ==
Topoisomerases can fix these topological problems and are separated into two types depending on the number of strands cut in one round of action: Both these classes of enzyme utilize a conserved tyrosine. However these enzymes are structurally and mechanistically different. For a video of this process see: http://www.youtube.com/watch?v=EYGrElVyHnU&feature=related.
A type I topoisomerase cuts one strand of a DNA double helix, relaxation occurs, and then the cut strand is reannealed. Cutting one strand allows the part of the molecule on one side of the cut to rotate around the uncut strand, thereby reducing stress from too much or too little twist in the helix. Such stress is introduced when the DNA strand is "supercoiled" or uncoiled to or from higher orders of coiling. Type I topoisomerases are subdivided into two subclasses: type IA topoisomerases, which share many structural and mechanistic features with the type II topoisomerases, and type IB topoisomerases, which utilize a controlled rotary mechanism. Examples of type IA topoisomerases include topo I and topo III. In the past, type IB topoisomerases were referred to as eukaryotic topo I, but IB topoisomerases are present in all three domains of life. Like type II topoisomerases, type IA topoisomerases form a covalent intermediate with the 5' end of DNA, whereas the IB topoisomerases form a covalent intermediate with the 3' end of DNA. Recently, a type IC topoisomerase has been identified, called topo V. While it is structurally unique from type IA and IB topoisomerases, it shares a similar mechanism with type IB topoisomerase.
A type II topoisomerase cuts both strands of one DNA double helix, passes another unbroken DNA helix through it, and then reanneals the cut strands. This class is also split into two subclasses: type IIA and type IIB topoisomerases, which possess similar structure and mechanisms. Examples of type IIA topoisomerases include eukaryotic topo II, E. coli gyrase, and E. coli topo IV. Examples of type IIB topoisomerase include topo VI. Type II topisomerases utilize ATP hydrolysis.
Both type I and type II topoisomerases change the linking number (L) of DNA. Type IA topoisomerases change the linking number by one, type IB and type IC topoisomerases change the linking number by any integer, whereas type IIA and type IIB topoisomerases change the linking number by two.


== See alsoEdit ==
DNA topology
Supercoil
TOP1
Type II topoisomerase


== ReferencesEdit ==

Pommier, Yves (May 28, 2010). "DNA topoisomerases and their poisoning by anticancer and antibacterial drugs". Chemistry & Biology. Retrieved May 28, 2010. 


== Further readingEdit ==
James C. Wang (2009) Untangling the Double Helix. DNA Entanglement and the Action of the DNA Topoisomerases, Cold Spring Harbor Laboratory Press, Cold Spring Harbor, NY, 2009. 245 pp. ISBN 978-0-87969-879-9


== External linksEdit ==
DNA Topoisomerases at the US National Library of Medicine Medical Subject Headings (MeSH)