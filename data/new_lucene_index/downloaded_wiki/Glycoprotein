Glycoproteins are proteins that "contain" oligosaccharide chains (glycans) covalently attached to polypeptide side-chains. The carbohydrate is attached to the protein in a cotranslational or posttranslational modification. This process is known as glycosylation. Secreted extracellular proteins are often glycosylated. In proteins that have segments extending extracellularly, the extracellular segments are also glycosylated. Glycoproteins are often important integral membrane proteins, where they play a role in cellcell interactions. Glycoproteins are also formed in the cytosol, but their functions and the pathways producing these modifications in this compartment are less understood.


== Types of glycosylation ==
There are several types of glycosylation, although the first two are the most common.
In N-glycosylation, sugars are attached to nitrogen, typically on the amide side-chain of asparagine.
In O-glycosylation, sugars are attached to oxygen, typically on serine or threonine but also on non-canonical amino acids such as hydroxylysine & hydroxyproline.
In P-glycosylation, sugars are attached to phosphorus on a phosphoserine.
In C-glycosylation, sugars are attached directly to carbon, such as in the addition of mannose to tryptophan.
In glypiation, a GPI glycolipid is attached to the C-terminus of a polypeptide, serving as a membrane anchor.


== Monosaccharides ==

Monosaccharides commonly found in eukaryotic glycoproteins include:
The sugar group(s) can assist in protein folding or improve proteins' stability.


== Examples ==
One example of glycoproteins found in the body is mucins, which are secreted in the mucus of the respiratory and digestive tracts. The sugars when attached to mucins give them considerable water-holding capacity and also make them resistant to proteolysis by digestive enzymes.
Glycoproteins are important for white blood cell recognition, especially in mammals. Examples of glycoproteins in the immune system are:
molecules such as antibodies (immunoglobulins), which interact directly with antigens.
molecules of the major histocompatibility complex (or MHC), which are expressed on the surface of cells and interact with T cells as part of the adaptive immune response.
sialyl Lewis X antigen on the surface of leukocytes.
H antigen of the ABO blood compatibility antigens. Other examples of glycoproteins include:
glycoprotein IIb/IIIa, an integrin found on platelets that is required for normal platelet aggregation and adherence to the endothelium.
components of the zona pellucida, which surrounds the oocyte, and is important for sperm-egg interaction.
structural glycoproteins, which occur in connective tissue. These help bind together the fibers, cells, and ground substance of connective tissue. They may also help components of the tissue bind to inorganic substances, such as calcium in bone.
Glycoprotein-41 (gp41) and glycoprotein-120 (gp120) are HIV viral coat proteins.
Soluble glycoproteins often show a high viscosity, for example, in egg white and blood plasma.
Miraculin, is a glycoprotein extracted from Synsepalum dulcificum a berry which alters human tongue receptors to recognize sour foods as sweet.
Variable surface glycoproteins allow the sleeping sickness Trypanosoma parasite to escape the immune response of the host.


== Hormones ==
Hormones that are glycoproteins include:
Follicle-stimulating hormone
Luteinizing hormone
Thyroid-stimulating hormone
Human chorionic gonadotropin
Alpha-fetoprotein
Erythropoietin (EPO)


== Functions ==


== Analysis ==
A variety of methods used in detection, purification, and structural analysis of glycoproteins are


== See also ==


== References ==


== External links ==
Glycan Recognizing Proteins
Structure of Glycoprotein and Carbohydrate Chain  Home Page for Learning Environmental Chemistry
Biochemistry 5thE 11.3. Carbohydrates Can Be Attached to Proteins to Form Glycoproteins
Carbohydrate Chemistry and Glycobiology: A Web Tour SPECIAL WeB SUPPLEMENT Science 23 March 2001 Vol 291, Issue 5512, Pages 22632502
Glycoproteins at the US National Library of Medicine Medical Subject Headings (MeSH)
Emanual Maverakis et. al. "Glycans in the immune system and The Altered Glycan Theory of Autoimmunity" (PDF). 
Biological Importance of the glycosylation of a protein