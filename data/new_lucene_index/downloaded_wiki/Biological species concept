The species problem is a mixture of difficult related questions that often come up when biologists define the word "species". There are a wide range of approaches to defining how we identify species and how species function in nature; each approach is known as a species concept. The number and types of species concepts which exist are constantly changing, but there are at least 26 recognized species concepts. Biological reality means that a definition that works well for some organisms (e.g., birds) will be useless for others (e.g., bacteria) particularly due to concerns regarding if the species reproduces asexually or sexually. The scientific study of the species problem has been called microtaxonomy.
One common, but sometimes difficult, question is how best to decide which species an organism belongs to, because reproductively isolated groups may not be readily recognizable, and cryptic species may be present.
There is a continuum from total reproductive isolation to panmixis. Populations can move forward or backwards along the interbreeding continuum, at any point meeting or failing the criteria for different species concepts. Thus, it is difficult to define reproductive isolation. Some separately evolving groups may continue to interbreed to some extent, or begin interbreeding again after a period of isolation, and it can be a difficult matter to discover whether this hybridization affects the long-term genetic make-up of the groups.
Many of the debates on species touch on philosophical issues, such as nominalism and realism, as well as on issues of language and cognition.
The current meaning of the phrase "species problem" is quite different from what Charles Darwin and others meant by it during the 19th and early 20th centuries. For Darwin, the species problem was the question of how new species arose: speciation.


== Confusion on the meaning of "Species" ==
Species is one of several ranks in the hierarchical system of scientific classification, called taxonomic ranks.
Even though it is not disputed that species is a taxonomic rank, this does not prevent disagreements when particular species are discussed. In the case of the Baltimore oriole (Icterus galbula) and Bullock's oriole (I. bullockii), two similar species of birds have sometimes in the past been considered to be one single species, the northern oriole (I. galbula). Currently, biologists agree that these are actually two separate species, but in the past this was not the case.
Disagreements surround what the best criteria are for identifying new species. In 1942, Ernst Mayr wrote that, because biologists have different ways of identifying species, they actually have different species concepts. Mayr listed five different species concepts, and since then many more have been added. The question of which species concept is best has occupied many printed pages and many hours of discussion.
The debates are philosophical in nature. One common disagreement is over whether a species should be defined by the characteristics that biologists use to identify the species, or whether a species is an evolving entity in nature. Every named species has been formally described as a type of organism with particular defining characteristics. These defining traits are used to identify which species an organism belongs to. For many species, all of the individuals that fit the defining criteria also make up a single evolving unit, but it might not be known whether that is the case. These two different ways of thinking about species, as a category or as an evolving population, may be quite different from each other.


== History ==


=== Before Darwin ===
The idea that one organism reproduces by giving birth to a similar organism, or producing seeds that grow to a similar organism, goes back to the earliest days of farming. While people tended to think of this as a relatively stable process, many thought that change was possible. The term species was just used as a term for a sort or kind of organism, until in 1686 John Ray introduced the biological concept that species were distinguished by always producing the same species, and this was fixed and permanent, though considerable variation was possible within a species. Carolus Linnaeus (17071778) formalized the taxonomic rank of species, and devised the two part naming system of binomial nomenclature that we use today. However, this did not prevent disagreements on the best way to identify species.
The history of definitions of the term "species" reveal that the seeds of the modern species debate were alive and growing long before Darwin.

"The traditional view, which was developed by Cain, Mayr and Hull in the mid-twentieth century, claims that until the Origin of species by Charles Darwin both philosophy and biology considered species as invariable natural kinds with essential features. This essentialism story was adopted by many authors, but questioned from the beginning by a minority  when Aristotle and the early naturalists wrote about the essences of species, they meant essential functions, not essential properties. Richards pointed out [Richard A. Richards, The Species Problem: A Philosophical Analysis, Cambridge University Press, 2010] that Linnaeus saw species as eternally fixed in his very first publication from 1735, but only a few years later he discovered hybridization as a modus for speciation.


=== From Darwin to Mayr ===
Charles Darwin's famous book On the Origin of Species (1859) offered an explanation as to how species changed over time (evolution). Although Darwin did not provide details on how one species splits into two, he viewed speciation as a gradual process. If Darwin was correct, then, when new incipient species are forming, there must be a period of time when they are not yet distinct enough to be recognized as species. Darwin's theory suggested that there was often not going to be an objective fact of the matter, on whether there were one or two species.
Darwin's book triggered a crisis of uncertainty for some biologists over the objectivity of species, and some came to wonder whether individual species could be objectively real  i.e. have an existence that is independent of the human observer.
In the 1920s and 1930s, Mendel's theory of inheritance and Darwin's theory of evolution by natural selection were joined in what was called the modern evolutionary synthesis. This conjunction of theories also had a large impact on how biologists think about species. Edward Poulton anticipated many ideas on species that today are well accepted, and that were later more fully developed by Theodosius Dobzhansky and Ernst Mayr, two of the architects of the modern synthesis. Dobzhansky's 1937 book articulated the genetic processes that occur when incipient species are beginning to diverge. In particular, Dobzhansky described the critical role, for the formation of new species, of the evolution of reproductive isolation.


=== Biological species concept ===
Ernst Mayr's 1942 book was a turning point for the species problem. In it, he wrote about how different investigators approach species identification, and he characterized these different approaches as different species concepts. He also argued strongly for what came to be called a Biological Species Concept (BSC), which is that a species consists of populations of organisms that can reproduce with one another and that are reproductively isolated from other such populations.
Mayr was not the first to define "species" on the basis of reproductive compatibility, as Mayr makes clear in his book on the history of biology. For example, Mayr discusses how Buffon proposed this kind of definition of "species" in 1753.
Theodosius Dobzhansky was a close contemporary of Mayr and the author of a classic book about the evolutionary origins of reproductive barriers between species, which was published a few years before Mayr's. Many biologists credit Dobzhansky and Mayr jointly for emphasizing the need to consider reproductive isolation when studying species and speciation.
Mayr was persuasive in many respects and from 1942 until his death in 2005, both he and the Biological Species Concept played a central role in nearly all debates on the species problem. For many, the Biological Species Concept was a useful theoretical idea because it leads to a focus on the evolutionary origins of barriers to reproduction between species. But the BSC has been criticized for not being very useful for deciding when to identify new species. It is also true that there are many cases where members of different species will hybridize and produce fertile offspring when they are under confined conditions, such as in zoos. One fairly extreme example is that lions and tigers will hybridize in captivity, and at least some of the offspring have been reported to be fertile. Mayr's response to cases like these is that the reproductive barriers that are important for species are the ones that occur in the wild. But even so, it is also the case that there are many cases of different species that are known to hybridize and produce fertile offspring in nature.
After Mayr's 1942 book, many more species concepts were introduced. Some, such as the Phylogenetic Species Concept (PSC), were designed to be more useful than the BSC for actually deciding when a new species should be described. However, not all of the new species concepts were about identifying species, and some concepts were mostly conceptual or philosophical.
About two dozen species concepts have been identified or proposed since Mayr's 1942 book, and many articles and several books have been written on the species problem. At some point, it became common for articles to profess to "solve" or "dissolve" the species problem.
Some have argued that the species problem is too multidimensional to be "solved" by one definition of species or one species concept. Since the 1990s, articles have appeared that make the case that species concepts, particularly those that specify how species should be identified, have not been very helpful in resolving the species problem.
Although Mayr promoted the biological species concept for use in systematics, the concept has been criticized as not being useful for those who do research in systematics. Some systematists have criticized the BSC as not being operational. However, for many others, the BSC is the preferred description of species. For example, many geneticists who work on the process of species formation prefer the BSC because it emphasizes the role of barriers to reproduction between species.
It has also been argued that the BSC, based on reproductive isolation, is not only a useful preferred description of species, but is also a natural consequence of the effect of sexual reproduction on the dynamics of natural selection. (Also see speciation.)


== Philosophical aspects ==


=== Realism ===
Realism is the philosophical position species are real mind-independent entities, natural kinds. Mayr, a proponent of realism attempted to demonstrate species exist as natural, extra-mental categories. He showed for example that the New Guinean tribesman classify 136 species of birds, which Western ornithologists came to independently recognize:

"I have always thought that there is no more devastating refutation of the nominalistic claims than the above mentioned fact that primitive natives in New Guinea, with a Stone Age culture, recognize as species exactly the same entities of nature as western taxonomists. If species were something purely arbitrary, it would be totally improbable for representatives of two drastically different cultures to arrive at the identical species delimitations."

Mayr's argument however has been criticized:

"The fact that independently observing humans see much the same species in nature does not show that species are real rather than nominal categories. The most it shows is that all human brains are wired up with a similar perceptual cluster statistic (Ridley, 1993). On this view we [humans] might have been wired differently and different species might now be wired differently from us, so that no one wiring can be said to be true or veridical."

Another position of realism, is that natural kinds are demarcated by the world itself by having a unique property that is shared by all the members of a species, and none outside the group. In other words, a natural kind possesses an essential or intrinsic feature (essence) that is self-individuating and non-arbitrary. This notion has been heavily criticized as essentialist, but modern realists have argued while biological natural kinds have essences  these need not be fixed and are prone to change through speciation. According to Mayr (1957) reproductive isolation or interbreeding "supplies an objective yardstick, a completely non-arbitrary criterion and "describing a presence or absence relationship makes this species concept non-arbitrary". The BSC defines species as "groups of actually or potentially interbreeding natural populations, which are reproductively isolated from other such groups". From this perspective, each species is based on a property (reproductive isolation) that is shared by all the organisms in the species that objectively distinguishes them.


=== Nominalism ===
Some philosophical variants of Nominalism propose that species are just names that people have assigned to groups of creatures but where the lines between species get drawn does not reflect any fundamental underlying biological cut-off point. In this view, the kinds of things that people have given names to, do not reflect any underlying reality. It then follows that species do not exist outside the mind, because species are just named abstractions. If species are not real, then it would not be sensible to talk about "the origin of a species" or the "evolution of a species". As recently at least as the 1950s, some authors adopted this view and wrote of species as not being real.
A counterpoint to the nominalist views in regard to species, was raised by Michael Ghiselin who argued that an individual species is not a type, but rather an actual individual, an actual entity. This idea comes from thinking of a species as an evolving dynamic population. If viewed as an entity, a species would exist regardless of whether or not people have observed it and whether or not it has been given a name.


=== Pragmatism ===
A popular alternative view, pragmatism, espoused by philosophers such as Philip Kitcher and John Dupre states while species do not exist in the sense of natural kinds, they are conceptually real and exist for convenience and for practical applications. For example, regardless of which definition of species one uses, one can still quantitatively compare species diversity across regions or decades, as long as the definition is held constant within a study. This has practical importance in advancing biodiversity science and environmental science.


=== Language and the role of human investigators ===
The nominalist critique of the view that kinds of things exist, raises for consideration the role that humans play in the species problem. For example, Haldane suggested that species are just mental abstractions.
Several authors have noted the similarity between "species", as a word of ambiguous meaning, and points made by Wittgenstein on family resemblance concepts and the indeterminacy of language.
Jody Hey described the species problem as a result of two conflicting motivations by biologists:
to categorize and identify organisms;
to understand the evolutionary processes that give rise to species.
Under the first view, species appear to us as typical natural kinds, but when biologists turn to understand species evolutionarily they are revealed as changeable and without sharp boundaries. Hey argued that it is unrealistic to expect that one definition of "species" is going to serve the need for categorization and still reflect the changeable realities of evolving species.


=== Pluralism and monism ===
Usually, it is assumed that biologists approach the species problem with the idea that it would be useful to develop one common viewpoint of species  one single common conception of what species are and of how they should be identified. It is thought that, if such a monistic description of species could be developed and agreed upon, then the species problem would be solved.
In contrast, some authors have argued for pluralism, claiming that biologists cannot have just one shared concept of species, and that they should accept multiple, seemingly incompatible ideas about species.
David Hull argued that pluralist proposals were unlikely to actually solve the species problem.


== Quotations on the species problem ==
"No term is more difficult to define than "species," and on no point are zoologists more divided than as to what should be understood by this word." Nicholson (1872, p. 20).
"Of late, the futility of attempts to find a universally valid criterion for distinguishing species has come to be fairly generally, if reluctantly, recognized" Dobzhansky (1937, p. 310).
"The concept of a species is a concession to our linguistic habits and neurological mechanisms" Haldane (1956).
"The species problem is the long-standing failure of biologists to agree on how we should identify species and how we should define the word 'species'." Hey (2001).
"First, the species problem is not primarily an empirical one, but it is rather fraught with philosophical questions that require  but cannot be settled by  empirical evidence." Pigliucci (2003).
"An important aspect of any species definition whether in neontology or palaeontology is that any statement that particular individuals (or fragmentary specimens) belong to a certain species is an hypothesis (not a fact)" Bonde (1977).
"We show that although discrete phenotypic clusters exist in most [plant] genera (> 80%), the correspondence of taxonomic species to these clusters is poor (< 60%) and no different between plants and animals. ... Contrary to conventional wisdom, plant species are more likely than animal species to represent reproductively independent lineages." Rieseberg et al. (2006).


== See also ==
Evolutionarily Significant Unit (ESU)
Ring species
Species
Speciation


== References ==


== External links ==
A catalogue of species conceptions
A Reduction of "Species" Resolves the Species Problem
Curiosities of Biological Nomenclature