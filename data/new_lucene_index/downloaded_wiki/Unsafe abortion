An unsafe abortion is the termination of a pregnancy by people lacking the necessary skills, or in an environment lacking minimal medical standards, or both. For example, an unsafe abortion may refer to an extremely dangerous life-threatening procedure that is self-induced in unhygienic conditions, or it may refer to a much safer abortion performed by a medical practitioner who does not provide appropriate post-abortion attention.
Unsafe abortion is a significant cause of maternal mortality and morbidity in the world. Most unsafe abortions occur where abortion is illegal, or in developing countries where affordable well-trained medical practitioners are not readily available, or where modern contraceptives are unavailable. About one in eight pregnancy-related deaths worldwide is associated with unsafe abortion.


== Overview ==
The World Health Organization (WHO) published an estimate that in 2003 approximately 42 million pregnancies were voluntarily terminated, of which 20 million were unsafe. According to WHO and Guttmacher, approximately 68,000 women die annually as a result of complications of unsafe abortion; and between two million and seven million women each year survive unsafe abortion but sustain long-term damage or disease (incomplete abortion, infection (sepsis), haemorrhage, and injury to the internal organs, such as puncturing or tearing of the uterus). They also concluded abortion is safer in countries where it's legal, but dangerous in countries where it's outlawed and performed clandestinely. The WHO reports that in developed regions, nearly all abortions (92%) are safe, whereas in developing countries, more than half (55%) are unsafe. According to WHO statistics, the risk rate for unsafe abortion is 1/270; according to other sources, unsafe abortion is responsible for one in eight maternal deaths. Worldwide, 48% of all induced abortions are unsafe. The British Medical Bulletin reported in 2003 that 70,000 women a year die from unsafe abortion. Incidence of such abortions may be difficult to measure because they can be reported variously as miscarriage, "induced miscarriage", "menstrual regulation", "mini-abortion", and "regulation of a delayed/suspended menstruation".
An article pre-printed by the World Health Organization called safe, legal abortion a "fundamental right of women, irrespective of where they live" and unsafe abortion a "silent pandemic". The article states "ending the silent pandemic of unsafe abortion is an urgent public-health and human-rights imperative." It also states "access to safe abortion improves womens health, and vice versa, as documented in Romania during the regime of President Nicolae Ceauescu" and "legalisation of abortion on request is a necessary but insufficient step toward improving womens health" citing that in some countries, such as India where abortion has been legal for decades, access to competent care remains restricted because of other barriers. WHOs Global Strategy on Reproductive Health, adopted by the World Health Assembly in May 2004, noted: "As a preventable cause of maternal mortality and morbidity, unsafe abortion must be dealt with as part of the MDG on improving maternal health and other international development goals and targets." The WHO's Development and Research Training in Human Reproduction (HRP), whose research concerns people's sexual and reproductive health and lives, has an overall strategy to combat unsafe abortion that comprises four interrelated activities:
to collate, synthesize and generate scientifically sound evidence on unsafe abortion prevalence and practices;
to develop improved technologies and implement interventions to make abortion safer;
to translate evidence into norms, tools and guidelines;
and to assist in the development of programmes and policies that reduce unsafe abortion and improve access to safe abortion and highquality postabortion care
A 2007 study published in the The Lancet found that, although the global rate of abortion declined from 45.6 million in 1995 to 41.6 million in 2003, unsafe procedures still accounted for 48% of all abortions performed in 2003. It also concluded that, while the overall incidence of abortion in both developed and developing countries is approximately equal, unsafe abortion occurs more often in less-developed nations.
Anti-abortion critics contend that the results of The Lancet study are flawed, as there are no accurate statistics about abortion from countries in the developing world. In a 2005 report, the WHO itself states, "More than a third of the 204 countries or areas examined did not report the number of deaths by sex even once for the period 1995 to 2003. About half did not report deaths by cause, sex and age at least once in the same period. Moreover, from 1975 to 2003 there has been limited progress in the reporting of deaths and their causes."


== Conflating illegal and unsafe abortion ==
Unsafe abortions often occur where abortion is illegal. However, the prevalence of unsafe abortion may also be determined by other factors, such as whether it occurs in a developing country that has a low level of competent medical care, and whether modern contraceptives are available.
Unsafe abortions sometimes occur where abortion is legal, and safe abortions sometimes occur where abortion is illegal. Legalization is not always followed by elimination of unsafe abortion. Affordable safe services may be unavailable despite legality, and conversely women may be able to afford medically competent services despite illegality.
When abortion is illegal, that generally contributes to the prevalence of unsafe abortion, but it is not the only contributor. In addition, a lack of access to safe and effective contraception contributes to unsafe abortion. It has been estimated that the incidence of unsafe abortion could be reduced by as much as 73% without any change in abortion laws if modern family planning and maternal health services were readily available globally.
Illegality of abortion contributes to maternal mortality, but that contribution is not as great as it once was, due to medical advances including penicillin and the birth control pill.


== Incidence by continent ==
* Excluding Japan
** Excluding Australia and New Zealand
Source: WHO 2006


=== Incidents in the U.S. after 1973 ===
In 2005, the Detroit News reported that a 16-year-old boy beat his pregnant, under-age girlfriend with a bat at her request to abort a fetus. The young couple lived in Michigan, where parental consent is required to receive an abortion. In Indiana, where there are also parental consent laws, a young woman by the name of Becky Bell died from an unsafe abortion rather than discuss her pregnancy and wish for an abortion with her parents.
In 2011, Kermit Gosnell, a licensed doctor who provided abortion services in the American state of Pennsylvania, was indicted by a grand jury on murder charges after a woman died in his clinic. The grand jury found that the conditions in Dr. Gosnell's clinic were not only unsanitary and that Dr. Gosnell staffed his clinic with unlicensed individuals, he had also commonly conducted the lesser known practice of severing the spinal cords of newly born babies.


== Methods ==
Methods of unsafe abortion include:
Trying to break the amniotic sac inside the womb with a sharp object or wire (for example an unbent wire clothes hanger or knitting needle). This method can cause infection or injury to internal organs (for example perforating the uterus or intestines), resulting in death. The uterus softens during pregnancy and is very easy to pierce, so one traditional method was to use a large feather.
Pumping toxic mixtures, such as chili peppers and chemicals like alum, Lysol, permanganate, or plant poison into the body of the woman. This method can cause the woman to go into toxic shock and die.
Inducing an abortion without medical supervision by self-administering abortifacient over-the-counter drugs or drugs obtained illegally or by using drugs not indicated for abortion but known to result in miscarriage or uterine contraction. Drugs that cause uterine contractions include oxytocin (synthetic forms are Pitocin and Syntocinon), prostaglandins, and ergot alkaloids. Risks include uterine rupture, irregular heartbeat, a rise in blood pressure (hypertension), a drop in blood pressure (hypotension), anemia requiring transfusion, cardiovascular problems, pulmonary edema, and death, as well as intense bronchospasms in women with asthma.


== Health risks ==
Unsafe abortion is a major cause of injury and death among women worldwide. Although data are imprecise, it is estimated that approximately 20 million unsafe abortions are performed annually, with 97% taking place in developing countries. Unsafe abortion is believed to result in approximately 69,000 deaths and millions of injuries annually. The legal status of abortion is believed to play a major role in the frequency of unsafe abortion. For example, the 1996 legalization of abortion in South Africa had an immediate positive impact on the frequency of abortion-related complications, with abortion-related deaths dropping by more than 90%. Groups such as the World Health Organization have advocated a public-health approach to addressing unsafe abortion, emphasizing the legalization of abortion, the training of medical personnel, and ensuring access to reproductive-health services.


== Illegal abortion ==
An illegal abortion may be called a "back-alley", "backstreet", or "back-yard" abortion.
The wire coat hanger method was a popularly known illegal abortion procedure, although they were not the norm. In fact, Mary Calderone, former medical director of Planned Parenthood, said, in a 1960 printing of the American Journal of Public Health:
"Abortion is no longer a dangerous procedure. This applies not just to therapeutic abortions as performed in hospitals but also to so-called illegal abortions as done by physician. In 1957 there were only 260 deaths in the whole country attributed to abortions of any kind, second, and even more important, the conference [on abortion sponsored by Planned Parenthood] estimated that 90 percent of all illegal abortions are presently being done by physicians. Whatever trouble arises usually arises from self-induced abortions, which comprise approximately 8 percent, or with the very small percentage that go to some kind of non-medical abortionist. Abortion, whether therapeutic or illegal, is in the main no longer dangerous, because it is being done well by physicians."
Herbal abortions (when done illegally) can also be described as an unsafe abortions because they are not induced in a medical facility.


== See also ==
Reproductive health
Reproductive rights
Gerri Santoro


== References ==


== External links ==
World Health Organization, index for Sexual and reproductive health
Preventing Unsafe Abortion and its Consequences: Priorities for Research and Action, New York: Guttmacher Institute, 2006
My Back-Alley Abortion, via BeliefNet