Acceleration, in physics, is the rate of change of velocity of an object. An object's acceleration is the net result of any and all forces acting on the object, as described by Newton's Second Law. The SI unit for acceleration is metre per second squared (m s-2). Accelerations are vector quantities (they have magnitude and direction) and add according to the parallelogram law. As a vector, the calculated net force is equal to the product of the object's mass (a scalar quantity) and its acceleration.
For example, when a car starts from a standstill (zero relative velocity) and travels in a straight line at increasing speeds, it is accelerating in the direction of travel. If the car turns, there is an acceleration toward the new direction. In this example, we can call the forward acceleration of the car a "linear acceleration", which passengers in the car might experience as a force pushing them back into their seats. When changing direction, we might call this "non-linear acceleration", which passengers might experience as a sideways force. If the speed of the car decreases, this is an acceleration in the opposite direction from the direction of the vehicle, sometimes called deceleration. Passengers may experience deceleration as a force lifting them forwards. Mathematically, there is no separate formula for deceleration: both are changes in velocity. Each of these accelerations (linear, non-linear, deceleration) might be felt by passengers until their velocity (speed and direction) matches that of the car.


== Definition and properties ==


=== Average acceleration ===

An object's average acceleration over a period of time is its change in velocity  divided by the duration of the period . Mathematically,


=== Instantaneous acceleration ===

Instantaneous acceleration, meanwhile, is the limit of the average acceleration over an infinitesimal interval of time. In the terms of calculus, instantaneous acceleration is the derivative of the velocity vector with respect to time:

(Here and elsewhere, if motion is in a straight line, vector quantities can be substituted by scalars in the equations.)
It can be seen that the integral of the acceleration function a(t) is the velocity function v(t); that is, the area under the curve of an acceleration vs. time (a vs. t) graph corresponds to velocity.

As acceleration is defined as the derivative of velocity, v, with respect to time t and velocity is defined as the derivative of position, x, with respect to time, acceleration can be thought of as the second derivative of x with respect to t:


=== Units ===
Acceleration has the dimensions of velocity (L/T) divided by time, i.e. L.T-2. The SI unit of acceleration is the metre per second squared (m s-2); or "metre per second per second", as the velocity in metres per second changes by the acceleration value, every second.


=== Other forms ===
An object moving in a circular motionsuch as a satellite orbiting the Earthis accelerating due to the change of direction of motion, although its speed may be constant. In this case it is said to be undergoing centripetal (directed towards the center) acceleration.
Proper acceleration, the acceleration of a body relative to a free-fall condition, is measured by an instrument called an accelerometer.
In classical mechanics, for a body with constant mass, the (vector) acceleration of the body's center of mass is proportional to the net force vector (i.e. sum of all forces) acting on it (Newton's second law):

where F is the net force acting on the body, m is the mass of the body, and a is the center-of-mass acceleration. As speeds approach the speed of light, relativistic effects become increasingly large.


== Tangential and centripetal acceleration ==

The velocity of a particle moving on a curved path as a function of time can be written as:

with v(t) equal to the speed of travel along the path, and

a unit vector tangent to the path pointing in the direction of motion at the chosen moment in time. Taking into account both the changing speed v(t) and the changing direction of ut, the acceleration of a particle moving on a curved path can be written using the chain rule of differentiation for the product of two functions of time as:

where un is the unit (inward) normal vector to the particle's trajectory (also called the principal normal), and r is its instantaneous radius of curvature based upon the osculating circle at time t. These components are called the tangential acceleration and the normal or radial acceleration (or centripetal acceleration in circular motion, see also circular motion and centripetal force).
Geometrical analysis of three-dimensional space curves, which explains tangent, (principal) normal and binormal, is described by the FrenetSerret formulas.


== Special cases ==


=== Uniform acceleration ===

Uniform or constant acceleration is a type of motion in which the velocity of an object changes by an equal amount in every equal time period.
A frequently cited example of uniform acceleration is that of an object in free fall in a uniform gravitational field. The acceleration of a falling body in the absence of resistances to motion is dependent only on the gravitational field strength g (also called acceleration due to gravity). By Newton's Second Law the force, F, acting on a body is given by:

Because of the simple analytic properties of the case of constant acceleration, there are simple formulas relating the displacement, initial and time-dependent velocities, and acceleration to the time elapsed:

where
 is the elapsed time,
 is the initial displacement from the origin,
 is the displacement at time ,
 is the initial velocity,
 is the velocity at time , and
 is the uniform rate of acceleration.
In particular, the motion can be resolved into two orthogonal parts, one of constant velocity and the other according to the above equations. As Galileo showed, the net result is parabolic motion, which describes, e. g., the trajectory of a projectile in a vacuum near the surface of Earth.


=== Circular motion ===

Uniform circular motion, that is constant speed along a circular path, is an example of a body experiencing acceleration resulting in velocity of a constant magnitude but change of direction. In this case, because the direction of the object's motion is constantly changing, being tangential to the circle, the object's linear velocity vector also changes, but its speed does not. This acceleration is a radial acceleration since it is always directed toward the centre of the circle and takes the magnitude:

where  is the object's linear speed along the circular path. Equivalently, the radial acceleration vector () may be calculated from the object's angular velocity :

where  is a vector directed from the centre of the circle and equal in magnitude to the radius. The negative shows that the acceleration vector is directed towards the centre of the circle (opposite to the radius).
The acceleration and the net force acting on a body in uniform circular motion are directed toward the centre of the circle; that is, it is centripetal. Whereas the so-called 'centrifugal force' appearing to act outward on the body is really a pseudo force experienced in the frame of reference of the body in circular motion, due to the body's linear momentum at a tangent to the circle.
With nonuniform circular motion, i.e., the speed along the curved path changes, a transverse acceleration is produced equal to the rate of change of the angular speed around the circle times the radius of the circle. That is,

The transverse (or tangential) acceleration is directed at right angles to the radius vector and takes the sign of the angular acceleration ().


== Relation to relativity ==


=== Special relativity ===

The special theory of relativity describes the behavior of objects traveling relative to other objects at speeds approaching that of light in a vacuum. Newtonian mechanics is exactly revealed to be an approximation to reality, valid to great accuracy at lower speeds. As the relevant speeds increase toward the speed of light, acceleration no longer follows classical equations.
As speeds approach that of light, the acceleration produced by a given force decreases, becoming infinitesimally small as light speed is approached; an object with mass can approach this speed asymptotically, but never reach it.


=== General relativity ===

Unless the state of motion of an object is known, it is totally impossible to distinguish whether an observed force is due to gravity or to accelerationgravity and inertial acceleration have identical effects. Albert Einstein called this the principle of equivalence, and said that only observers who feel no force at allincluding the force of gravityare justified in concluding that they are not accelerating.


== Conversions ==


== See also ==


== References ==


== External links ==
Acceleration Calculator Simple acceleration unit converter
Measurespeed.com - Acceleration Calculator Based on starting & ending speed and time elapsed.