An occultation is an event that occurs when one object is hidden by another object that passes between it and the observer. The word is used in astronomy (see below). It can also refer to any situation wherein an object in the foreground blocks from view (occults) an object in the background. In this general sense, occultation applies to the visual scene observed from low-flying aircraft (or computer-generated imagery) wherein foreground objects obscure distant objects dynamically, as the scene changes over time.


== Occultations by the MoonEdit ==

The term occultation is most frequently used to describe those relatively frequent occasions when the Moon passes in front of a star during the course of its orbital motion around the Earth. Since the Moon, with an angular speed with respect to the stars of 0.55 arcsec/s or 2.7 rad/s, has a very thin atmosphere and stars have an angular diameter of at most 0.057 arcseconds or 0.28 rad, a star that is occulted by the Moon will disappear or reappear in 0.1 seconds or less on the Moon's edge, or limb. Events that take place on the Moon's dark limb are of particular interest to observers, because the lack of glare allows these occultations to more easily be observed and timed.
The Moon's orbit is inclined to the ecliptic (see orbit of the Moon), and any stars with an ecliptic latitude of less than about 6.5 degrees may be occulted by it. There are three first magnitude stars that are sufficiently close to the ecliptic that they may be occulted by the Moon and by planets  Regulus, Spica and Antares. Occultations of Aldebaran are presently only possible by the Moon, because the planets pass Aldebaran to the north. Neither planetary nor lunar occultations of Pollux are currently possible. However, in the far future, occultations of Pollux will be possible, as they were in the far past. Some deep-sky objects, such as the Pleiades, can also be occulted by the Moon.

Within a few kilometres of the edge of an occultation's predicted path, referred to as its northern or southern limit, an observer may see the star intermittently disappearing and reappearing as the irregular limb of the Moon moves past the star, creating what is known as a Grazing lunar occultation. From an observational and scientific standpoint, these "grazes" are the most dynamic and interesting of lunar occultations.
The accurate timing of lunar occultations is performed regularly by (primarily amateur) astronomers. Lunar occultations timed to an accuracy of a few tenths of a second have various scientific uses, particularly in refining our knowledge of lunar topography. Photoelectric analysis of lunar occultations have also discovered some stars to be very close visual or spectroscopic binaries. Some angular diameters of stars have been measured by timing of lunar occultations, which is useful for determining effective temperatures of those stars. Early radio astronomers found occultations of radio sources by the Moon valuable for determining their exact positions, because the long wavelength of radio waves limited the resolution available through direct observation. This was crucial for the unambiguous identification of the radio source 3C 273 with the optical quasar and its jet, and a fundamental prerequisite for Maarten Schmidt's discovery of the cosmological nature of quasars.
Several times during the year, someone on Earth can usually observe the Moon occulting a planet. Since planets, unlike stars, have significant angular sizes, lunar occultations of planets will create a narrow zone on Earth from which a partial occultation of the planet will occur. An observer located within that narrow zone could observe the planet's disk partly blocked by the slowly moving moon. The same mechanic can be seen with the Sun, where observers on Earth will view it as a Solar Eclipse. Therefore, a Total Solar Eclipse is effectively the same event as the Moon occulting the Sun.


== Occultation by planetsEdit ==

Stars may also be occulted by planets. In 1959, Venus occulted Regulus. Uranus's rings were first discovered when that planet occulted a star in 1977. On 3 July 1989, Saturn passed in front of the 5th magnitude star 28 Sagittarii. Pluto occulted stars in 1988, 2002, and 2006, allowing its tenuous atmosphere to be studied via atmospheric limb sounding.


=== Mutual planetary occultations and transitsEdit ===
It is also possible for one planet to occult another planet. However, these mutual occultations of planets are extremely rare. The last such event occurred on 3 January 1818 and the next will occur on 22 November 2065, in both cases involving the same two planetsVenus and Jupiter. Technically speaking, when the foreground planet is smaller in apparent size than the background planet, the event should be called a "mutual planetary transit". When the foreground planet is larger in apparent size than the background planet, the event should be called a "mutual planetary occultation". (See Transit for a list of past and future events).
Twice during the orbital cycles of Jupiter and Saturn, the equatorial (and satellite) planes of those planets are aligned with Earth's orbital plane, resulting in a series of mutual occultations and eclipses between the moons of these giant planets. These orbital alignments have also occurred artificially when unmanned spacecraft have traversed these planetary systems, resulting in photographs such as the one shown here. The terms "eclipse," "occultation" and "transit" are also used to describe these events. A satellite of Jupiter (for example) may be eclipsed (i.e. made dimmer because it moves into Jupiter's shadow), occulted (i.e. hidden from view because Jupiter lies on our line of sight), or may transit (i.e. pass in front of) Jupiter's disk.


==== Historical observationsEdit ====
An occultation of Mars by Venus on 13 October 1590 was observed by the German astronomer Michael Maestlin at Heidelberg. The 1737 event (see list below) was observed by John Bevis at Greenwich Observatory  it is the only detailed account of a mutual planetary occultation. A transit of Mars across Jupiter on 12 September 1170 was observed by the monk Gervase at Canterbury, and by Chinese astronomers.


==== Future eventsEdit ====
The next time a mutual planetary transit or occultation will happen (as seen from Earth) will be on 22 November 2065 at about 12:43 UTC, when Venus near superior conjunction (with an angular diameter of 10.6") will transit in front of Jupiter (with an angular diameter of 30.9"); however, this will take place only 8 west of the Sun, and will therefore not be visible to the unaided/unprotected eye. When the nearer object has a larger angular diameter than the farther object, thus covering it completely, the event is not a transit but an occultation. Before transiting Jupiter, Venus will occult Jupiter's moon Ganymede at around 11:24 UTC as seen from some southernmost parts of Earth. Parallax will cause actual observed times to vary by a few minutes, depending on the precise location of the observer.


==== List of mutual planetary occultations and transitsEdit ====
There are only 18 mutual planetary transits and occultations as seen from Earth between 1700 and 2200. Note the long break of events between 1818 and 2065.
19 September 1702  Jupiter occults Neptune
20 July 1705  Mercury transits Jupiter
14 July 1708  Mercury occults Uranus

4 October 1708  Mercury transits Jupiter
28 May 1737  Venus occults Mercury
29 August 1771  Venus transits Saturn
21 July 1793  Mercury occults Uranus
9 December 1808  Mercury transits Saturn
3 January 1818  Venus transits Jupiter
22 November 2065  Venus transits Jupiter
15 July 2067  Mercury occults Neptune
11 August 2079  Mercury occults Mars
27 October 2088  Mercury transits Jupiter
7 April 2094  Mercury transits Jupiter
21 August 2104  Venus occults Neptune
14 September 2123  Venus transits Jupiter
29 July 2126  Mercury occults Mars
3 December 2133  Venus occults Mercury
This table is another compilation of occultations and transits of bright stars and planets by solar planets. These events are not visible everywhere the occulting body and the occulted body are above the skyline. Some events are barely visible, because they take place in close proximity to the Sun.


== Occultations by asteroidsEdit ==

An asteroid occultation occurs when an asteroid (also known as a minor planet) passes in front of a star (occults a star), temporarily blocking its light (as seen from Earth). Several events occur nearly every day over the world. From any particular place such events occur almost every night, although most require a telescope to see. Professionals and amateurs around the globe are collaborating over the Internet to exchange their observation for a joint analysis.
Asteroid occultations are useful for measuring the size and position of asteroids much more precisely than can be done by any other means. A cross-sectional profile of the shape of an asteroid can even be determined if a number of observers at different, nearby, locations observe the occultation. For example, on 12 March 2009 there were 8 minor planet occulations, including 85 Io, 247 Eukrate, 1585 Union, 201 Penelope, 70 Panopaea, 980 Anacostia, 2448 Sholokhov, 1746 Brouwer, and 191 Kolga. Any one of these would be expected to occult at a time and place on the globe, at a certain magnitude, and with a certain star.
For example, according to the 1998 European Asteroidal Occultation Results from Euraster, 39 Laetitia was observed by over 38 observatories in one occultation on 3 March 1998, which resulted in many chords being determined.
Regulus was occulted by the asteroid 163 Erigone in the early morning of March 20, 2014 as first predicted by A. Vitagliano in 2004. This was the brightest asteroid occultation ever predicted to occur over a populated area. As the main belt asteroid passed in front of the star its 67-mile-wide (100 km) shadow swept across Nassau and Suffolk counties, all of New York City and the Hudson River Valley, with the center of the shadow path following a line roughly connecting New York City, White Plains, Newburgh, Oneonta, Rome and Pulaski before crossing into Canada near Belleville and North Bay, Ontario. Bad weather obscured the occultation.
Occultations have produced outlines of many asteroids. Some notable ones include:
Occultations have also been used to estimate the diameter of trans-Neptunian objects such as (55636) 2002 TX300, 28978 Ixion, and 20000 Varuna. Preliminary results of a 6 November 2010 occultation by the dwarf planet Eris of a magnitude 17 star in the constellation of Cetus placed an upper limit on Eris's diameter of 2320 km, making it almost the same size as Pluto. Due to their slower movement through the night sky, occultations by TNOs are far less common than by asteroids in the main-belt.


== Double occultationsEdit ==
It is possible that the Moon or another celestial body can occult multiple celestial bodies at the same time. Such events are extremely rare and can be seen only from a small part of the world. The last event of such type was on 23 April 1998 when the Moon occulted Venus and Jupiter simultaneously for observers on Ascension Island.


== Occulting satellitesEdit ==
The Big Occulting Steerable Satellite (BOSS) was a proposed satellite that would work in conjunction with a telescope to detect planets around distant stars. The satellite consists of a large, very lightweight sheet, and a set of maneuvering thrusters and navigation systems. It would maneuver to a position along the line of sight between the telescope and a nearby star. The satellite would thereby block the radiation from the star, permitting the orbiting planets to be observed.
The proposed satellite would have a dimension of 70 m  70 m, a mass of about 600 kg, and maneuver by means of an ion drive engine in combination with using the sheet as a light sail. Positioned at a distance of 100,000 km from the telescope, it would block more than 99.998% of the starlight.
There are two possible configurations of this satellite. The first would work with a space telescope, most likely positioned near the Earth's L2 Lagrangian point. The second would place the satellite in a highly elliptical orbit about the Earth, and work in conjunction with a ground telescope. At the apogee of the orbit, the satellite would remain relatively stationary with respect to the ground, allowing longer exposure times.
An updated version of this design is called the Starshade, which uses a sunflower-shaped coronagraph disc. A comparable proposal was also made for a satellite to occult bright X-ray sources, called an X-ray Occulting Steerable Satellite or XOSS.


== See alsoEdit ==


== ReferencesEdit ==


== Further readingEdit ==
Meeus, Jean (1995). Astronomical Tables of the Sun, Moon and Planets. Richmond, Virginia: Willmann-Bell, Inc. ISBN 0-943396-45-X. 
(German) Marco Peuschel  Astronomische Tabellen fr den Mond von 2007 bis 2016, Mondphasen, Apsiden, Knotendurchgnge, Maximale und minimale Deklinationswerte und Sternbedeckungen sowie ausfhrliche Ephemeriden fr jeden Tag des Jahres, inkl. Mondauf-und Untergnge und physische Daten.


== External linksEdit ==
Occultation Studies at Williams College
International Occultation Timing Association (IOTA)
International Occultation Timing Association (IOTA), newer web site
Occultations of planets by other planets
Big Occulting Steerable Satellite
Moon, planets, asteroid occultations
Mutual occultations (and conjunctions) of planets
Society for Popular Astronomy  Occultation Section
SPACE.com: Discovery Hints at a Quadrillion Space Rocks Beyond Neptune (Sara Goudarzi) 15 August 2006 06:13 am ET
Scientists Use Novel Technique To Watch Occultations By Planets
Mutual Planetary Transits  Fifteen millennium catalog: 5 000 BC  10 000 AD / Occultation of planets by Moon 2009 - 2014
asteroidoccultation.com/ Asteroid Occultation Updates
Occultations by asteroids (Good visual aid of an occultation of a star by Phoebe)
Society for Popular Astronomy (Asteroidal Occultations for the UK)
Occultations of HIP and UCAC2 stars downto 15m by large TNO in 20042014
Combining asteroid models derived by lightcurve inversion with asteroidal occultation silhouettes (arXiv : 21 April 2011)
Lunar Occultation Workbench, a free software package for computing lunar occultations
Occult, another free software package for computing occultations
Asteroidal transits on planets from 2008-2020 (Jean Meeus using Solex 9 / 4 April 2007)
2012 : Lunar Occultations of Planets, Minor Planets and Bright Stars (The Astronomical Almanac)
Occultations of bright stars by planets between 0 and 4000