In chemistry, a suspension is a heterogeneous mixture containing solid particles that are sufficiently large for sedimentation. Usually they must be larger than one micrometer. The internal phase (solid) is dispersed throughout the external phase (fluid) through mechanical agitation, with the use of certain excipients or suspending agents. Unlike colloids, suspensions will eventually settle. An example of a suspension would be sand in water. The suspended particles are visible under a microscope and will settle over time if left undisturbed. This distinguishes a suspension from a colloid, in which the suspended particles are smaller and do not settle. Colloids and suspensions are different from solutions, in which the dissolved substance (solute) does not exist as a solid, and solvent and solute are homogeneously mixed.
A suspension of liquid droplets or fine solid particles in a gas is called an aerosol or particulate. In the atmosphere these consist of fine dust and soot particles, sea salt, biogenic and volcanogenic sulfates, nitrates, and cloud droplets.
Suspensions are classified on the basis of the dispersed phase and the dispersion medium, where the former is essentially solid while the latter may either be a solid, a liquid, or a gas.
In modern chemical process industries, high shear mixing technology has been used to create many novel suspensions.
Suspensions are unstable from the thermodynamic point of view; however, they can be kinetically stable over a large period of time, which determines their shelf life. This time span needs to be measured to ensure the best product quality to the final consumer. Dispersion stability refers to the ability of a dispersion to resist change in its properties over time. D.J. McClements.


== Technique monitoring physical stability ==
Multiple light scattering coupled with vertical scanning is the most widely used technique to monitor the dispersion state of a product, hence identifying and quantifying destabilization phenomena. It works on concentrated dispersions without dilution. When light is sent through the sample, it is back scattered by the particles. The backscattering intensity is directly proportional to the size and volume fraction of the dispersed phase. Therefore, local changes in concentration (sedimentation) and global changes in size (flocculation, aggregation) are detected and monitored.


== Accelerating methods for shelf life prediction ==
The kinetic process of destabilisation can be rather long (up to several months or even years for some products) and it is often required for the formulator to use further accelerating methods in order to reach reasonable development time for new product design. Thermal methods are the most commonly used and consists in increasing temperature to accelerate destabilisation (below critical temperatures of phase inversion or chemical degradation). Temperature affects not only the viscosity, but also interfacial tension in the case of non-ionic surfactants or more generally interactions forces inside the system. Storing a dispersion at high temperatures enables simulation of real life conditions for a product (e.g. tube of sunscreen cream in a car in the summer), but also to accelerate destabilisation processes up to 200 times.
Mechanical acceleration including vibration, centrifugation and agitation are sometimes used. They subject the product to different forces that pushes the particles / droplets against one another, hence helping in the film drainage. However, some emulsions would never coalesce in normal gravity, while they do under artificial gravity. Moreover, segregation of different populations of particles have been highlighted when using centrifugation and vibration.
Common examples of suspensions include:
Mud or muddy water: where soil, clay, or silt particles are suspended in water
Flour suspended in water


== See also ==
Sol
Emulsion
Turbidity
Settleable solids
Sediment transport
Solution
Mixture
Tyndall effect


== References ==