A rover (or sometimes planetary rover) is a space exploration vehicle designed to move across the surface of a planet or other celestial body. Some rovers have been designed to transport members of a human spaceflight crew; others have been partially or fully autonomous robots. Rovers usually arrive at the planetary surface on a lander-style spacecraft.


== Comparison with space probes of other typesEdit ==

Rovers have several advantages over stationary landers: they examine more territory and they can be directed to interesting features. If they are solar powered, they can place themselves in sunny positions to weather winter months. They can also advance the knowledge of how to perform very remote robotic vehicle control which is necessarily semi-autonomous due to the finite speed of light.
Their advantages over orbiting spacecraft are that they can make observations to a microscopic level and can conduct physical experimentation. Disadvantages of rovers compared to orbiters are the higher chance of failure, due to landing and other risks, and that they are limited to a small area around a landing site which itself is only approximately anticipated.


== FeaturesEdit ==
Rovers arrive on spacecraft and are used in conditions very distinct from those on the Earth, which makes some demands on their design.


=== ReliabilityEdit ===
Rovers have to withstand high levels of acceleration, high and low temperatures, pressure, dust, corrosion, cosmic rays, remaining functional without repair for a needed period of time.


=== CompactnessEdit ===
Rovers are usually packed for placing in a spacecraft, because it has limited capacity, and has to be deployed. They are also attached to a spacecraft, so devices for removing these connections are installed.


=== AutonomyEdit ===
Rovers which land on celestial bodies far from the Earth, such as the Mars Exploration Rovers, cannot be remotely controlled in real-time since the speed at which radio signals travel is far too slow for real time or near-real time communication. For example, sending a signal from Mars to Earth takes between 3 and 21 minutes. These rovers are thus capable of operating autonomously with little assistance from ground control as far as navigation and data acquisition are concerned, although they still require human input for identifying promising targets in the distance to which to drive, and determining how to position itself to maximize solar energy. Giving a rover some rudimentary visual identification capabilities to make simple distinctions can allow engineers to speed up the reconnaissance.


== HistoryEdit ==


=== Lunokhod 0 (No.201)Edit ===
The Soviet rover was intended to be the first roving remote-controlled robot on the Moon, but crashed during a failed start of the launcher 19 February 1969.


=== Lunokhod 1Edit ===

The Lunokhod 1 rover landed on the Moon in November 1970. It was the first roving remote-controlled robot to land on any celestial body. The Soviet Union launched Lunokhod 1 aboard the Luna 17 spacecraft on November 10, 1970, and it entered lunar orbit on November 15. The spacecraft soft-landed in the Sea of Rains region on November 17. The lander had dual ramps from which Lunokhod 1 could descend to the lunar surface, which it did at 06:28 UT. From November 17, 1970 to November 22, 1970 the rover drove 197 m, and during 10 communication sessions returned 14 close up pictures of the Moon and 12 panoramic views. It also analyzed the lunar soil. The last successful communications session with Lunokhod 1 was on September 14, 1971. Having worked for 11 months, Lunokhod 1 held the durability record for space rovers for more than 30 years, until a new record was set by the Mars Exploration Rovers.


=== Apollo Lunar Roving VehicleEdit ===

NASA included Lunar Roving Vehicles in three Apollo missions: Apollo 15 (which landed on the Moon July 30, 1971), Apollo 16 (which landed April 21, 1972) and Apollo 17 (which landed December 11, 1972).


=== Lunokhod 2Edit ===

The Lunokhod 2 was the second of two unmanned lunar rovers landed on the Moon by the Soviet Union as part of the Lunokhod program. The rover became operational on the Moon on January 16, 1973. It was the second roving remote-controlled robot to land on any celestial body. The Soviet Union launched Lunokhod 2 aboard the Luna 21 spacecraft on January 8, 1973, and it entered lunar orbit on January 12, 1973. The spacecraft soft-landed in the eastern edge of the Mare Serenitatis region on January 15, 1973. Lunokhod 2 descended from the lander's dual ramps to the lunar surface at 01:14 UT on January 16, 1973. Lunokhod 2 operated for about 4 months, covered 39 km (24 mi) of terrain, including hilly upland areas and rilles, and sent back 86 panoramic images and over 80,000 TV pictures. Based on wheel rotations Lunokhod 2 was thought to have covered 37 km (23 mi) but Russian scientists at the Moscow State University of Geodesy and Cartography (MIIGAiK) have revised that to an estimated distance of about 42.142.2 km (26.226.2 mi) based on Lunar Reconnaissance Orbiter (LRO) images of the lunar surface. Subsequent discussions with their American counterparts ended with an agreed-upon final distance of 39 km (24 mi), which has stuck since.


=== Prop-M RoverEdit ===

The Soviet Mars 2 and Mars 3 landers had a small 4.5 kg Mars rover on board, which would have moved across the surface on skis while connected to the lander with a 15-meter umbilical. Two small metal rods were used for autonomous obstacle avoidance, as radio signals from Earth would have taken too long to drive the rovers using remote control. The rover was planned to be placed on the surface after landing by a manipulator arm and to move in the field of view of the television cameras and stop to make measurements every 1.5 meters. The traces of movement in the Martian soil would also have been recorded to determine material properties. Because of the demise of both the landers, the rover was not deployed.


=== Lunokhod 3Edit ===
The Soviet rover was intended to be the third roving remote-controlled robot on the Moon in 1977. The mission was canceled due to lack of launcher availability and funding, although the rover was built.


=== MarsokhodEdit ===
The Marsokhod was a heavy Soviet rover (hybrid, with both controls telecommand and automatic) aimed at Mars, part of the Mars 4NM and scheduled to be released (after 1973 according to the plans of 1970) launched by a N1 rocket that never arrived to fly successfully.


=== SojournerEdit ===

The Mars Pathfinder mission included Sojourner, the first rover to successfully reach another planet. NASA, the space agency of the United States, launched Mars Pathfinder on 1996-12-04; it landed on Mars in a region called Chryse Planitia on 1997-07-04. From its landing until the final data transmission on 1997-09-27, Mars Pathfinder returned 16,500 images from the lander and 550 images from Sojourner, as well as data from more than 15 chemical analyses of rocks and soil and extensive data on winds and other weather factors.


=== Beagle 2 Planetary Undersurface ToolEdit ===
Beagle 2 was designed to explore Mars with a small "mole" (Planetary Undersurface Tool, or PLUTO), to be deployed by the arm. PLUTO had a compressed spring mechanism designed to enable it to move across the surface at a rate of 20 mm per second and to burrow into the ground and collect a subsurface sample in a cavity in its tip. Beagle 2 failed while attempting to land on Mars in 2003.


=== Mars Exploration Rover A SpiritEdit ===

Spirit is a robotic rover on Mars, active from 2004 to 2010. It was one of two rovers of NASA's ongoing Mars Exploration Rover Mission. It landed successfully on Mars at 04:35 Ground UTC on January 4, 2004, three weeks before its twin, Opportunity (MER-B), landed on the other side of the planet. Its name was chosen through a NASA-sponsored student essay competition. The rover became stuck in late 2009, and its last communication with Earth was sent on March 22, 2010.


== Active rover missionsEdit ==


=== Active rover locations in contextEdit ===


=== Mars Exploration Rover B OpportunityEdit ===

Opportunity is a robotic rover on the planet Mars, active since 2004. It is the remaining rover in NASA's ongoing Mars Exploration Rover Mission. Launched from Earth on July 7, 2003, it landed on the Martian Meridiani Planum on January 25, 2004 at 05:05 Ground UTC (about 13:15 local time), three weeks after its twin Spirit (MER-A) touched down on the other side of the planet. On July 28, 2014, NASA announced that Opportunity, after having traveled over 40 km (25 mi) on the planet Mars, has set a new "off-world" record as the rover having driven the greatest distance, surpassing the previous record held by the Soviet Union's Lunokhod 2 rover that had traveled 39 km (24 mi). (related image)


=== Mars Science Laboratory Rover "Curiosity"Edit ===

On 26 November 2011, NASA's Mars Science Laboratory mission was successfully launched for Mars. The mission successfully landed the robotic "Curiosity" rover on the surface of Mars in August 2012, whereupon the rover is currently helping to determine whether Mars could ever have supported life, and search for evidence of past or present life on Mars.


=== Chang'e 3Edit ===

Chang'e 3 is a Chinese Moon mission that includes a robotic lunar rover. Launched in 2013, it is China's first lunar rover, part of the second phase of the Chinese Lunar Exploration Program undertaken by China National Space Administration (CNSA).


== Planned rover missionsEdit ==


=== Chandrayaan 2Edit ===

The Chandrayaan-II mission is a joint venture between India and Russia, consisting of a lunar orbiter and a lunar lander. An opportunity was given to students to design this rover. 150 students gave their designs but only 6 were selected. They gave a demonstration in NRSA and are going to ISRO.The Russian designed rover weighs 50 kg, will have six wheels and will run on solar power. It will land near one of the poles and will operate for a year, roving up to 150 km at a maximum speed of 360 m/h. The proposed launch date is 2015.


=== ExoMars RoverEdit ===

The European Space Agency (ESA) is currently designing and carrying out early prototyping and testing of the ExoMars rover which is scheduled for launch in 2018.


=== Mars 2020 rover missionEdit ===

The Mars 2020 rover mission is a Mars planetary rovermission concept under study by NASA with a possible launch in 2020. It is intended to investigate an astrobiologically relevant ancient environment on Mars, investigate its surface geological processes and history, including the assessment of its past habitability and potential for preservation of biosignatures within accessible geological materials.


=== Future lunar missionsEdit ===
As of 2009, NASA had developed a series of plans for future moon missions which called for rovers that have a far longer range than the Apollo rovers.


== See alsoEdit ==
Google Lunar X Prize
Lander (spacecraft)
LORAX (robot)
Lunar rover
Mars rover (Manned)
Tank on the Moon


== ReferencesEdit ==