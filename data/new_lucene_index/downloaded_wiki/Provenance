Provenance (from the French provenir, "to come from"), is the chronology of the ownership, custody or location of a historical object. The term was originally mostly used in relation to works of art, but is now used in similar senses in a wide range of fields, including archaeology, paleontology, archives, manuscripts, printed books, and science and computing. The primary purpose of tracing the provenance of an object or entity is normally to provide contextual and circumstantial evidence for its original production or discovery, by establishing, as far as practicable, its later history, especially the sequences of its formal ownership, custody, and places of storage. The practice has a particular value in helping authenticate objects. Comparative techniques, expert opinions, and the results of scientific tests may also be used to these ends, but establishing provenance is essentially a matter of documentation.
It has been argued that in archaeology (North American archaeology and anthropological archaeology throughout the world), when the US spelling provenience is used it has a related but subtly different sense to provenance. Archaeological researchers use provenience to refer to the three-dimensional location or find spot of an artifact or feature within an archaeological site, whereas provenance covers an object's complete documented history. Ideally, in modern excavations, the provenience or find spot is recorded (even videoed) with great precision, but in older cases only the general site or approximate area may be known, especially when an artifact was found outside a professional excavation and its specific position not recorded. Any given antiquity may therefore have both a provenience (where it was found) and a provenance (where it has been since it was found). In some cases, especially where there is an inscription, the provenance may include a history that predates its burial in the ground, as well as those relating to its history after rediscovery.


== Works of art and antiquesEdit ==
The provenance of works of fine art, antiques and antiquities is of great importance, especially to their owner. There are a number of reasons why painting provenance is important, which mostly also apply to other types of fine art. A good provenance increases the value of a painting, and establishing provenance may help confirm the date, artist and, especially for portraits, the subject of a painting. It may confirm whether a painting is genuinely of the period it seems to date from. The provenance of paintings can help resolve ownership disputes. For example, provenance between 1933 and 1945 can determine whether a painting was looted by the Nazis. Many galleries are putting a great deal of effort into researching the provenance of paintings in their collections for which there is no firm provenance during that period. Documented evidence of provenance for an object can help to establish that it has not been altered and is not a forgery, a reproduction, stolen or looted art. Provenance helps assign the work to a known artist, and a documented history can be of use in helping to prove ownership. An example of a detailed provenance is given in the Arnolfini portrait.
The quality of provenance of an important work of art can make a considerable difference to its selling price in the market; this is affected by the degree of certainty of the provenance, the status of past owners as collectors, and in many cases by the strength of evidence that an object has not been illegally excavated or exported from another country. The provenance of a work of art may vary greatly in length, depending on context or the amount that is known, from a single name to an entry in a scholarly catalogue some thousands of words long.
An expert certification can mean the difference between an object having no value and being worth a fortune. Certifications themselves may be open to question. Jacques van Meegeren forged the work of his father Han van Meegeren (who in his turn had forged the work of Vermeer). Jacques sometimes produced a certificate with his forgeries stating that a work was created by his father. See Jacques van Meegeren.
John Drewe was able to pass off as genuine paintings, a large number of forgeries that would have easily been recognised as such by scientific examination. He established an impressive (but false) provenance and because of this galleries and dealers accepted the paintings as genuine. He created this false provenance by forging letters and other documents, including false entries in earlier exhibition catalogues.
Sometimes provenance can be as simple as a photograph of the item with its original owner. Simple yet definitive documentation such as that can increase its value by an order of magnitude, but only if the owner was of high renown. Many items that were sold at auction have gone far past their estimates because of a photograph showing that item with a famous person. Some examples include antiques owned by politicians, musicians, artists, actors, etc.


=== Researching the provenance of paintingsEdit ===

The objective of provenance research is to produce a complete list of owners (together, where possible, with the supporting documentary proof) from when the painting was commissioned or in the artist's studio through to the present time. In practice, there are likely to be gaps in the list and documents that are missing or lost. The documented provenance should also list when the painting has been part of an exhibition and a bibliography of when it has been discussed (or illustrated) in print.
Where the research is proceeding backwards, to discover the previous provenance of a painting whose current ownership and location is known, it is important to record the physical details of the painting  style, subject, signature, materials, dimensions, frame, etc. The titles of paintings and the attribution to a particular artist may change over time. The size of the work and its description can be used to identify earlier references to the painting. The back of a painting can contain significant provenance information. There may be exhibition marks, dealer stamps, gallery labels and other indications of previous ownership. There may also be shipping labels. In the BBC TV programme Fake or Fortune? the provenance of the painting Bords de la Seine  Argenteuil was investigated using a gallery sticker and shipping label on the back. Early provenance can sometimes be indicated by a cartellino added to a painting in a collection. However these can be forged and can become faded or be painted over.
Auction records are an important resource to assist in researching the provenance of paintings.
The Witt Library houses a collection of cuttings from auction catalogs which enables the researcher to identify occasions when a picture has been sold.
The Heinz Library at the National Portrait Gallery, London maintains a similar collection, but restricted to portraits.
The National Art Library at the Victoria and Albert Museum has a collection of UK sales catalogues.
The University of York is establishing a web site with on-line resources for investigating art history in the period 16601735. This includes diaries, sales catalogues, bills, correspondence and inventories.
The Getty Research Institute in Los Angeles has a Project for the Study of Collecting and Provenance (PSCP) which includes an on-line database, still being compiled, of auction and other records relating to painting provenance.
The Frick Art Reference Library in New York has an extensive collection of auction and exhibition catalogues.
The Netherlands Institute for Art History (RKD) has a number of databases related to artists from the Netherlands.
If a painting has been in private hands for an extended period and on display in a stately home, it may be recorded in an inventory  for example, the Lumley inventory. The painting may also have been noticed by a visitor who subsequently wrote about it. It may have been mentioned in a will or a diary. Where the painting has been bought from a dealer, or changed hands in a private transaction, there may be a bill of sale or sales receipt that provides evidence of provenance. Where the artist is known, there may be a catalogue raisonn listing all the artist's known works and their location at the time of writing. A database of catalogues raisonns is available at the International Foundation for Art Research. Historic photos of the painting may be discussed and illustrated in a more general work on the artist, period or genre. Similarly, a photograph of a painting may show inscriptions (or a signature) that subsequently became lost as a result of overzealous restoration. Conversely, a photograph may show that an inscription was not visible at an earlier date. One of the disputed aspects of the "Rice" portrait of Jane Austen concerns apparent inscriptions identifying artist and sitter.


== WinesEdit ==
In transactions of old wine with the potential of improving with age, the issue of provenance has a large bearing on the assessment of the contents of a bottle, both in terms of quality and the risk of wine fraud. A documented history of wine cellar conditions is valuable in estimating the quality of an older vintage due to the fragile nature of wine.
Recent technology developments have aided collectors in assessing the temperature and humidity history or the wine which are two key components in establishing perfect provenance. For example, there are devices available that rest inside the wood case and can be read through the wood by waving a smartphone equipped with a simple app. These devices track the conditions the case has been exposed to for the duration of the battery life, which can be as long as 15 years, and sends a graph and high/low readings to the smartphone user. This takes the trust issue out of the hands of the owner and gives it to a third party for verification.


== ArchivesEdit ==
Provenance is a fundamental principle of archival science, referring to the individuals, groups, or organizations that originally created or received the items in a collection, and to the items' subsequent chain of custody. According to archival theory and the principle of provenance, records which originate from a common source (or fonds) should be kept together  either physically, or, where that is not practicable, intellectually in the way in which they are catalogued and arranged in finding aids  in accordance with what is sometimes termed the principle of archival integrity or respect des fonds. Conversely, records of different provenance should be separated. In archival practice, proof of provenance is provided by the operation of control systems that document the history of records kept in archives, including details of amendments made to them. The authority of an archival document or set of documents of which the provenance is uncertain (because of gaps in the recorded chain of custody) will be considered to be severely compromised.
The principles of archival provenance were developed in the 19th century by both French and Prussian archivists, and gained widespread acceptance on the basis of their formulation in the Manual for the arrangement and description of archives by Dutch state archivists Samuel Muller, J. A. Feith, and R. Fruin, published in the Netherlands in 1898 (often referred to as the "Dutch Manual").
Provenance is also the title of the journal published by the Society of Georgia Archivists.


== BooksEdit ==
In the case of books, the study of provenance refers to the study of the ownership of individual copies of books. It is usually extended to include study of the circumstances in which individual copies of books have changed ownership, and of evidence left in books that shows how readers interacted with them.
Provenance studies may shed light on the books themselves, providing evidence of the role particular titles have played in social, intellectual and literary history. Such studies may also add to our knowledge of particular owners of books. For instance, looking at the books owned by a writer may help to show which works influenced him or her.
Many provenance studies are historically focused, and concentrated on books owned by writers, politicians and public figures. The recent ownership of books is studied, however, as is evidence of how ordinary or anonymous readers have interacted with books.
Provenance can be studied both by examining the books themselves (for instance looking at inscriptions, marginalia, bookplates, book rhymes, and bindings) and by reference to external sources of information such as auction catalogues.


== ScienceEdit ==


=== ArchaeologyEdit ===
Evidence of provenance can be of importance in archaeology. Fakes are not unknown and finds are sometimes removed from the context in which they were found without documentation, reducing their value to the world of learning. Even when apparently discovered in-situ archaeological finds are treated with caution. The provenance of a find may not be properly represented by the context in which it was found. Artifacts can be moved far from their place of origin by mechanisms that include looting, collecting, theft or trade, and further research is often required to establish the true provenance of a find.


=== Data provenanceEdit ===
Scientific research is generally held to be of good provenance when it is documented in detail sufficient to allow reproducibility. Scientific workflows assist scientists and programmers with tracking their data through all transformations, analyses, and interpretations. Data sets are reliable when the process used to create them are reproducible and analyzable for defects. Current initiatives to effectively manage, share, and reuse ecological data are indicative of the increasing importance of data provenance. Examples of these initiatives are National Science Foundation Datanet projects, DataONE and Data Conservancy, as well as the U.S. Global Change Research Program.


=== Computer ScienceEdit ===
Within computer science, informatics uses the term 'provenance' to mean the lineage of data, as per data provenance, with research in the last decade extending the conceptual model of causality and relation to include processes that act on data and agents that are responsible for those processes. See, for example, the proceedings of the International Provenance Annotation Workshop (IPAW) and Theory and Practice of Provenance (TaPP). Semantic web standards bodies, such as the World Wide Web Consortium, have recently (2014) ratified a standard data model for provenance representation known as PROV which draws from many of the better known provenance representation systems that preceded it, such as the Proof Markup Language and the Open Provenance Model.


=== PaleontologyEdit ===
In paleontology it is recognised that fossils can also move from their primary context and are sometimes found, apparently in-situ, in deposits to which they do not belong, moved by, for example, the erosion of nearby but different outcrops. Most museums make strenuous efforts to record how the works in their collections were acquired and these records are often of use in helping to establish provenance.


=== PetrologyEdit ===

In the geologic use of the term, provenance instead refers to the origin or source area of particles within a rock, most commonly in sedimentary rocks. It does not refer to the circumstances of the collection of the rock. The provenance of sandstone, in particular, can be evaluated by determining the proportion of quartz, feldspar, and lithic fragments (see diagram).


=== Seed provenanceEdit ===
Seed provenance refers to the specified area in which plants that produced seed are located or were derived. Local provenancing is a position maintained by ecologists that suggests that seeds should be planted of local provenance only. However, this view suffers from the adaptationist program  a view that populations are universally locally adapted. It is maintained that local seed is best adapted to local conditions (local adaptation) and outbreeding depression will be avoided. Evolutionary biologists suggest that strict adherence to provenance collecting is not a wise decision because:
Local adaptation is not as common as assumed.
Background population maladaptation can be driven by natural processes.
Human actions of habitat fragmentation drive maladaptation up and adaptive potential down.
Natural selection is changing rapidly due to climate change. and habitat fragmentation
Population fragments are unlikely to divergence by natural selection since fragmentation (<500 years). This leads to a low risk of outbreeding depression.


== Computers and lawEdit ==
The term provenance is used when ascertaining the source of goods such as computer hardware to assess if they are genuine or counterfeit. Chain of custody is an equivalent term used in law, especially for evidence in criminal or commercial cases.
Software provenance encompasses the origin of software and its licensing terms. For example, when incorporating a free, open source or proprietary software component in an application, one may wish to understand its provenance to ensure that licensing requirements are fulfilled and that other software characteristics can be understood.
Data provenance covers the provenance of computerized data. There are two main aspects of data provenance: ownership of the data and data usage. Ownership will tell the user who is responsible for the source of the data, ideally including information on the originator of the data. Data usage gives details regarding how the data has been used and modified and often includes information on how to cite the data source or sources. Data provenance is of particular concern with electronic data, as data sets are often modified and copied without proper citation or acknowledgement of the originating data set. Databases make it easy to select specific information from data sets and merge this data with other data sources without any documentation of how the data was obtained or how it was modified from the original data set or sets.
Secure Provenance refers to providing integrity and confidentiality guarantees to provenance information. In other words, secure provenance means to ensure that history cannot be rewritten, and users can specify who else can look into their actions on the object.
A simple method of ensuring data provenance in computing is to mark a file as read only. This allows the user to view the contents of the file, but not edit or otherwise modify it. Read only can also in some cases prevent the user from accidentally or intentionally deleting the file.


== See alsoEdit ==
Dating methodology (archaeology)
Post excavation
Records Management
Traceability
Arnolfini Portrait  fairly full example of the provenance of a painting
Annunciation (van Eyck, Washington) - another example
Fake or Fortune?  a TV series on BBC One in the UK


== ReferencesEdit ==


== BibliographyEdit ==
Provenance in book studies


== External linksEdit ==
The National Gallery of Art Washington gives brief provenances for most featured works
EU Provenance Project - a technology project that sought to support the electronic certification of data provenance
the difference between provenience and provenance
DataONE
Data Conservancy
W3C Provenance Working Group
W3C Provenance Outreach Information
W3C PROV Implementations: Preliminary Analysis (by Khalid Belhajjame), 2013.