Wind-chill or windchill, (popularly wind chill factor) is the perceived decrease in air temperature felt by the body on exposed skin due to the flow of air.
Wind chill numbers are always lower than the air temperature for values where the formula is valid. When the apparent temperature is higher than the air temperature, the heat index is used instead.


== Explanation ==
A surface loses heat through conduction, convection, and radiation. The rate of convection depends on the difference in temperature between the surface and its surroundings. As convection from a warm surface heats the air around it, an insulating boundary layer of warm air forms against the surface. Moving air disrupts this boundary layer, or epiclimate, allowing for cooler air to replace the warm air against the surface. The faster the wind speed, the more readily the surface cools.
The effect of wind chill is to increase the rate of heat loss and reduce any warmer objects to the ambient temperature more quickly. Dry air cannot, however, reduce the temperature of these objects below the ambient temperature, no matter how great the wind velocity. For most biological organisms, the physiological response is to generate more heat in order to maintain a surface temperature in an acceptable range. The attempt to maintain a given surface temperature in an environment of faster heat loss results in both the perception of lower temperatures and an actual greater heat loss. In other words, the air 'feels' colder than it is because of the chilling effect of the wind on the skin. In extreme conditions this will increase the risk of adverse effects such as frostbite.


== Alternative approaches ==
Many formulas exist for wind chill because, unlike temperature, there is no universally agreed standard for what the term should mean. All the formulas attempt to provide the ability to qualitatively predict the effect of wind on the temperature humans perceive. Within different countries weather services use a standard for their country or region. U.S. and Canadian weather services use a model accepted by the National Weather Service. That model has evolved over time.
The first wind chill formulas and tables were developed by Paul Allman Siple and Charles F. Passel working in the Antarctic before the Second World War, and were made available by the National Weather Service by the 1970s. It was based on the cooling rate of a small plastic bottle as its contents turned to ice while suspended in the wind on the expedition hut roof, at the same level as the anemometer. The so-called Windchill Index provided a pretty good indication of the severity of the weather. In the 1960s, wind chill began to be reported as a wind chill equivalent temperature (WCET), which is theoretically less useful. The author of this change is unknown, but it was not Siple and Passel as is generally believed. At first, it was defined as the temperature at which the windchill index would be the same in the complete absence of wind. This led to equivalent temperatures that were obviously exaggerations of the severity of the weather. Charles Eagan realized that people are rarely still and that even when it was calm, there was some air movement. He redefined the absence of wind to be an air speed of 1.8 metres per second (4.0 mph), which was about as low a wind speed as a cup anemometer could measure. This led to more realistic (warmer-sounding) values of equivalent temperature.


=== Original model ===
Equivalent temperature was not universally used in North America until the 21st century. Until the 1970s, the coldest parts of Canada reported the original Wind Chill Index, a three or four digit number with units of kilocalories/hour per square metre. Each individual calibrated the scale of numbers personally, through experience. The chart also provided general guidance to comfort and hazard through threshold values of the index, such as 1400, which was the threshold for frostbite.
The original formula for the index was:
Where:
 = Wind chill index, kcal/m2/h
 = Wind velocity, m/s
 = Air temperature, C


=== North American and United Kingdom wind chill index ===
In November 2001 Canada, U.S. and U.K. implemented a new wind chill index developed by scientists and medical experts on the Joint Action Group for Temperature Indices (JAG/TI). It is determined by iterating a model of skin temperature under various wind speeds and temperatures using standard engineering correlations of wind speed and heat transfer rate. Heat transfer was calculated for a bare face in wind, facing the wind, while walking into it at 1.4 metres per second (3.1 mph). The model corrects the officially measured wind speed to the wind speed at face height, assuming the person is in an open field. The results of this model may be approximated, to within one degree, from the following formula:
The standard Wind Chill formula for Environment Canada is:

where  is the wind chill index, based on the Celsius temperature scale,  is the air temperature in degrees Celsius (C), and  is the wind speed at 10 metres (standard anemometer height), in kilometres per hour (km/h).
The equivalent formula in US customary units is:

where  is the wind chill index, based on the Fahrenheit scale,  is the air temperature, measured in F, and  is the wind speed, in mph.
Windchill temperature is defined only for temperatures at or below 10 C (50 F) and wind speeds above 4.8 kilometres per hour (3.0 mph).
As the air temperature falls, the chilling effect of any wind that is present increases. For example, a 16 km/h (9.9 mph) wind will lower the apparent temperature by a wider margin at an air temperature of 20 C (4 F), than a wind of the same speed would if the air temperature were 10 C (14 F).
The method for calculating wind chill has been controversial because experts disagree on whether it should be based on whole body cooling either while naked or while wearing appropriate clothing, or if it should be based instead on local cooling of the most exposed skin, such as the face. The internal thermal resistance is also a point of contention. It varies widely from person to person. Had the average value for the subjects been used, calculated WCET's would be a few degrees more severe.
The 2001 WCET is a steady state calculation (except for the time to frostbite estimates). There are significant time-dependent aspects to wind chill because cooling is most rapid at the start of any exposure, when the skin is still warm.
The exposure to wind depends on the surroundings and wind speeds can vary widely depending on exposure and obstructions to wind flow.
The 2007 McMillan Coefficient worked to simplify wind chill. This formula calculates wind chill by subtracting the wind speed in mph from the Fahrenheit temperature.
The 2012 Breedlove Coefficient proposed a different method to calculate the wind chill. The new formula subtracts the temperature in Fahrenheit from the wind speed rather than subtracting the wind speed in mph from the Fahrenheit temperature.


=== Australian Apparent Temperature ===
The Australian Bureau of Meteorology uses a different formula for cooler temperatures.
The formula is:

Where:
 Dry bulb temperature (C)
 Water vapour pressure (hPa)
 Wind speed (m/s) at an elevation of 10 meters
The vapour pressure can be calculated from the temperature and relative humidity using the equation:

Where:
 Dry bulb temperature (C)
 Relative humidity [%]
 represents the exponential function
The Australian formula includes the important factor of humidity and is somewhat more involved than the simpler North American model. However humidity can be a significant factor. The North American formula is designed mainly on the basis that it is expected to be applied at low temperatures, when humidity levels are also low, and also for much colder temperatures (as low as 50 F or 46 C). As these are qualitative models this is not necessarily a major failing.
A more exhaustive model (which factors in wind speed) was developed for the US Navy stationed in Parris Island in South Carolina. It was developed with a consideration for heat stroke due to the high humidity of the island during summer months. It utilized three specialized thermometers. This research is what the Australian Apparent Temperature formula was derived from.


== See also ==
Cold wave
Heat index
Humidex
Wind chill warning


== References ==


== External links ==
National Center for Atmospheric Research Table of wind chill temperatures in Celsius and Fahrenheit
Wind chill calculator
National Weather Service Wind Chill Temperature Index Table of wind chill temperatures in Fahrenheit with frostbite times
National Science Digital Library - wind chill Temperature
Environment Canada - Wind Chill
Weather Images Wind Chill Chart and Introduction
An Introduction to Wind Chill A lesson plan on wind chill
A Cold Wind Blowing An article on wind chill and tables in SI units
Wind Chill and Humidex Criticism about the use of wind chill and humidex
An article about wind chill from The Canadian Encyclopedia
Gorman, James (February 10, 2004). "Beyond Brrr: The Elusive Science of Cold". The New York Times.