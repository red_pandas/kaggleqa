A fish hatchery is a "place for artificial breeding, hatching and rearing through the early life stages of animals, finfish and shellfish in particular". Hatcheries produce larval and juvenile fish (and shellfish and crustaceans) primarily to support the aquaculture industry where they are transferred to on-growing systems i.e. fish farms to reach harvest size. Some species that are commonly raised in hatcheries include Pacific oysters, shrimp, Indian prawns, salmon, tilapia and scallops. The value of global aquaculture production is estimated to be US$98.4 billion in 2008 with China significantly dominating the market, however the value of aquaculture hatchery and nursery production has yet to be estimated. Additional hatchery production for small-scale domestic uses, which is particularly prevalent in South-East Asia or for conservation programmes, has also yet to be quantified.
There is much interest in supplementing exploited stocks of fish by releasing juveniles that may be wild caught and reared in nurseries before transplanting, or produced solely within a hatchery. Culture of finfish larvae has been utilised extensively in the United States in stock enhancement efforts to replenish natural populations. The U.S. Fish and Wildlife Service have established a National Fish Hatchery System to support the conservation of native fish species.


== PurposeEdit ==

Hatcheries produce larval and juvenile fish and shellfish for transferral to aquaculture facilities where they are on-grown to reach harvest size. Hatchery production confers three main benefits to the industry;1. Out of season production
Consistent supply of fish from aquaculture facilities is an important market requirement. Broodstock conditioning can extend the natural spawning season and thus the supply of juveniles to farms. Supply can be further guaranteed by sourcing from hatcheries in the opposite hemisphere i.e. with opposite seasons.2. Genetic improvement
Genetic modification is conducted in some hatcheries to improve the quality and yield of farmed species. Artificial fertilisation facilitates selective breeding programs which aim to improve production characteristics such as growth rate, disease resistance, survival, colour, increased fecundity and/or lower age of maturation. Genetic improvement can be mediated by selective breeding, via hybridization, or other genetic manipulation techniques.3. Reduce dependence on wild-caught juveniles
In 2008 aquaculture accounted for 46% of total food fish supply, around 115 million tonnes. Although wild caught juveniles are still utilised in the industry, concerns over sustainability of extracting juveniles, and the variable timing and magnitude of natural spawning events, make hatchery production an attractive alternative to support the growing demands of aquaculture.


== Production stepsEdit ==


=== BroodstockEdit ===
Broodstock conditioning is the process of bringing adults into spawning condition by promoting the development of gonads. Broodstock conditioning can also extend spawning beyond natural spawning periods, or for production of species reared outside their natural geographic range with different environmental conditions. Some hatcheries collect wild adults and then bring them in for conditioning whilst others maintain a permanent breeding stock. Conditioning is achieved by holding broodstock in flow-through tanks at optimal conditions for light, temperature, salinity, flow rate and food availability (optimal levels are species specific). Another important aspect of broodstock conditioning is ensuring the production of high quality eggs to improve growth and survival of larvae by optimising the health and welfare of broodstock individuals. Egg quality is often determined by the nutritional condition of the mother. High levels of lipid reserves in particular are required to improve larval survival rates.


=== SpawningEdit ===
Natural spawning can occur in hatcheries during the regular spawning season however where more control over spawning time is required spawning of mature animals can be induced by a variety of methods. Some of the more common methods are:Manual stripping : For shellfish, gonads are generally removed and gametes are extracted or washed free. Fish can be manually stripped of eggs and sperm by stroking the anaesthetised fish under the pectoral fins towards the anus causing gametes to freely flow out.Environmental manipulation: Thermal shock, where cool water is alternated with warmer water in flow-through tanks can induce spawning. Alternatively, if environmental cues that stimulate natural spawning are known, these can be mimicked in the tank e.g. changing salinity to simulate migratory behaviour. Many individuals can be induced to spawn this way, however this increases the likelihood of uncontrolled fertilisation occurring.Chemical injection: A number of chemicals can be used to induce spawning with various hormones being the most commonly used.


=== FertilisationEdit ===
Prior to fertilisation, eggs can be gently washed to remove wastes and bacteria that may contaminate cultures. Promoting cross-fertilisation between a large number of individuals is necessary to retain genetic diversity in hatchery produced stock. Batches of eggs are kept separate, fertilised with sperm obtained from several males and allowed to stand for an hour or two before samples are analyzed under a microscope to ensure high rates of fertilisation and to estimate numbers to be transferred to larval rearing tanks.


=== LarvaeEdit ===
Rearing larvae through the early life stages is conducted in nurseries which are generally closely associated with hatcheries for fish culture whilst it is common for shellfish nurseries to exist separately. Nursery culture of larvae to rear juveniles of a size suitable for transferral to on-growing facilities can be performed in a variety of different systems which may be entirely land-based, or larvae may be later transferred to sea-based rearing systems which reduce the need to supply feed. Juvenile survival is dependent on very high quality water conditions. Feeding is an important component of the rearing process. Although many species are able to grow on maternal reserves alone (lecithotrophy), most commercially produced species require feeding to optimise survival, growth, yield and juvenile quality. Nutritional requirements are species specific and also vary with larval stage. Carnivorous fish are commonly fed with live prey; rotifers are usually offered to early larvae due to their small size, progressing to larger Artemia nauplii or zooplankton. The production of live feed on-site or buying-in is one of the biggest costs for hatchery facilities as it is a labour-intensive process. The development of artificial feeds is targeted to reduce the costs involved in live feed production and increase the consistency of nutrition, however decreased growth and survival has been found with these alternatives.


=== Settlement of shellfishEdit ===
The hatchery production of shellfish also involves a crucial settling phase where free-swimming larvae settle out of the water onto a substrate and undergo metamorphosis if suitable conditions are found. Once metamorphosis has taken place the juveniles are generally known as spat, it is this phase which is then transported to on-growing facilities. Settlement behaviour is governed by a range of cues including substrate type, water flow, temperature, and the presence of chemical cues indicating the presence of adults, or a food source etc. Hatchery facilities therefore need to understand these cues to induce settlement and also be able to substitute artificial substrates to allow for easy handling and transportation with minimal mortality.


== Hatchery designEdit ==

Hatchery designs are highly flexible and are tailored to the requirements of site, species produced, geographic location, funding and personal preferences. Many hatchery facilities are small and coupled to larger on-growing operations, whilst others may produce juveniles solely for sale. Very small-scale hatcheries are often utilized in subsistence farming to supply families or communities particularly in south-east Asia. A small-scale hatchery unit consists of larval rearing tanks, filters, live food production tanks and a flow through water supply. A generalized commercial scale hatchery would contain a broodstock holding and spawning area, feed culture facility, larval culture area, juvenile culture area, pump facilities, laboratory, quarantine area, and offices and bathrooms.


=== ExpenseEdit ===
Labour is generally the largest cost in hatchery production making up more that 50% of total costs. Hatcheries are a business and thus economic viability and scale of production are vital considerations. The cost of production for stock-enhancement programmes is further complicated by the difficulty of assessing the benefits to wild populations from restocking activities.


== IssuesEdit ==


=== GeneticEdit ===
Hatchery facilities present three main problems in the field of genetics. The first is that maintenance of a small number of broodstock can cause inbreeding and potentially lead to inbreeding depression thus affecting the success of the facility. Secondly, hatchery reared juveniles, even from a fairly large broodstock, can have greatly reduced genetic diversity compared to wild populations (the situation is comparable to the founder effect). Such fish that escape from farms or are released for restocking purposes may adversely affect wild population genetics and viability. This is of particular concern where escaped fish have been actively bred or are otherwise genetically modified. The third key issue is that genetic modification of food items is highly undesirable for many people. See: Genetically modified food controversies


=== Fish farmsEdit ===
Other arguments that surround fish farms such as the supplementation of feed from wild caught species, the prevalence of disease, fish welfare issues and potential effects on the environment are also issues for hatchery facilities. For more information see: Aquaculture, Mariculture, Fish farming


== See alsoEdit ==
List of harvested aquatic animals by weight
Mount Whitney Fish Hatchery
Raceway (aquaculture)


== ReferencesEdit ==


== External linksEdit ==
 Ernest Ingersoll (1920). "Bass, Culture of". Encyclopedia Americana.