The tusk shells or tooth shells, often referred to by the more-technical term scaphopods /skfpd/ (Greek, "boat-footed"), are members of a class of shelled marine mollusc with worldwide distribution, and are the only class of exclusively infaunal marine molluscs. Shells of species within this class range from about 0.5 to 15 cm in length. Members of the order Dentaliida tend to be significantly larger than those of the order Gadilida.
These molluscs live in soft substrates offshore (usually not intertidally). Because of this subtidal habitat and the small size of most species, many beachcombers are unfamiliar with them; their shells are not as common or as easily visible in the beach drift as the shells of sea snails and clams.
Molecular data suggest that the scaphopods are a sister group to the cephalopods, although higher-level molluscan phylogeny remains somewhat unresolved.


== Orientation ==
The morphological shape of the scaphopod body makes it difficult to orient it satisfactorily. As a result, researchers have often disagreed as to which direction is anterior/ posterior and which is ventral/ dorsal. According to Shimek and Steiner, "[t]he apex of the shell and mantle are anatomically dorsal, and the large aperture is ventral and anterior. Consequently, the concave side of the shell and viscera are anatomically dorsal. The convex side has to be divided into anteriorly ventral and dorsally posterior portions, with the anus as the demarcation. Functionally, as in cephalopods, the large aperture with the foot is anterior, the apical area posterior, the concave side dorsal and the convex side ventral."


== Anatomy ==
The shells of the members of the Gadilida are usually glassy-smooth in addition to being quite narrow and with a reduced aperture. This along with other structures of their anatomy allows them to move with surprising speed through loose sediment to escape potential bottom-dwelling predators. The Dentalids, on the other hand, tend to have strongly ribbed and often rather rough shells. When they sense vibrations anywhere around them, their defensive response is to freeze. This makes them harder to detect by animals such as ratfish which can sense the electrical signals given off by the most minute muscle movement.
The mantle of a scaphopod is entirely within the shell. The foot extends from the larger end of the shell, and is used to burrow through the substrate. The scaphopod positions itself head down in the substrate, with the apical end of the shell (at the rear of the animal's body) projecting upward. This end seldom appears above the level of the substrate, however, as doing so exposes the animal to numerous predators. Most adult scaphopods live their lives entirely buried within the substrate.
Water enters the mantle cavity through the apical aperture, and is wafted along the body surface by cilia. There are no gills; the entire surface of the mantle cavity absorbs oxygen from the water. Unlike most other molluscs, there is no continuous flow of water with a separate exhalant stream. Instead, deoxygenated water is expelled rapidly back through the apical aperture through muscular action once every ten to twelve minutes.
A number of minute tentacles around the foot, called captacula, sift through the sediment and latch onto bits of food, which they then convey to the mouth. The mouth has a grinding radula that breaks the bit into smaller pieces for digestion. The radulae and cartilaginous oral bolsters of the Gadilidae are structured like zippers where the teeth actively crush the prey by opening and closing on it repeatedly, while the radulae and bolsters of the Dentaliidae work rachet-like to pull the prey into the esophagus, sometimes whole. The massive radula of the scaphopods is the largest such organ relative to body size of any mollusc (among whom, except for the bivalves, the presence of a which is a defining characteristic). The remainder of the digestive system consists of a digestive diverticulum, esophagus, stomach, and intestine. A digestive gland secretes enzymes into the stomach, but, unlike some other molluscs, does not digest the food directly itself. The anus opens on the ventral/ underside of the animal, roughly in the middle of the mantle cavity.
The scaphopod vascular system is rudimentary, lacking both heart and blood vessels; the blood is held in sinuses throughout the body cavity, and is pumped through the body by the rhythmic action of the foot. Metabolic waste is excreted through a pair of nephridia close to the anus. The tusk shells appear to be the only extant molluscs which completely lack the otherwise standard molluscan reno-pericardial apertures. Furthermore, they also appear to be the only molluscs with openings that directly connect the hemocoel with the surrounding water (through two "water pores" located near the nephridial openings). These openings may serve to allow the animal to relieve internal pressure by ejecting body fluid (blood) during moments of extreme muscular contraction of the foot.
The nervous system is generally similar to that of gastropods. One pair each of cerebral and pleural ganglia lie close to the oesophagus, and effectively form the animal's brain. A separate set of pedal ganglia lie in the foot, and a pair of visceral ganglia are set further back in the body. Scaphopods have no eyes, or other distinct sensory organs.


== Reproduction and development ==
Scaphopods have separate sexes, and external fertilisation. They have a single gonad occupying much of the posterior part of the body, and shed their gametes into the water through the nephridium.
Once fertilised, the eggs hatch into a free-living trochophore larva, which develops into a veliger larva that more closely resembles the adult, but lacks the extreme elongation of the adult body.


== Ecology ==
Tusk shells live in seafloor sediment, feeding on microscopic organisms; some supplement their diet of zooplankton with vegetable matter.


== Classification ==
The group is composed of a two subtaxa, the Dentaliida (which may be paraphyletic) and the monophyletic Gadilida. The differences between the two orders is subtle and hinges on size and on details of the radula, shell, and foot. Specifically, the Dentaliids are the physically larger of the two families, and possess a shell that tapers uniformly from anterior (widest) to posterior (narrowest); they also have a foot which consists of one central and two lateral lobes and which bends into the shell when retracted. The Gadilids, on the other hand, are much smaller, have a shell whose widest portion is slightly posterior to its aperture, and have a foot which is disk-like and fringed with tentacles which inverts into itself when retracted (in this state resembling a pucker rather than a disk).
According to the World Register of Marine Species :
Dentaliida da Costa, 1776
family Anulidentaliidae Chistikov, 1975 -- 3 genera
family Calliodentaliidae -- 1 genus
family Dentaliidae Children, 1834 -- 14 genera
family Fustiariidae Steiner, 1991 -- 1 genus
family Gadilinidae Chistikov, 1975 -- 2 genera
family Laevidentaliidae Palmer, 1974 -- 1 genus
family Omniglyptidae Chistikov, 1975 -- 1 genus
family Rhabdidae Chistikov, 1975 -- 1 genus

Gadilida Starobogatov, 1974
sub-order Entalimorpha Steiner, 1992
family Entalinidae Chistikov, 1979 -- 9 genera

sub-order Gadilimorpha Steiner, 1992
family Gadilidae Stoliczka, 1868 -- 8 genera
family Pulsellidae Scarabino in Boss, 1982 -- 3 genera
family Wemersoniellidae Scarabino, 1986 -- 2 genera


== Fossil record ==
There is a good fossil record of scaphopods from the Mississippian onwards, making them the youngest molluscan class; but the origin of the group remains contentious.
The Ordovician Rhytiodentalium kentuckyensis has been interpreted as an early antecedent of the scaphopods, implying an evolutionary succession from ribeirioid rostroconch molluscs such as Pinnocaris. However, a competing hypothesis suggests a Devonian/Carboniferous origin from a non-mineralized ancestor, or from a more derived, Devonian, conocardioid rostroconch.
As such they were the most recent of all molluscan classes to evolve. They are most closely related to the extinct molluscan class Rostroconchia.


== Human use ==
The shells of Dentalium hexagonum, a scaphopod mollusc, were strung on thread and used by the natives of the Pacific Northwest as shell money. Dentalium shells were also used to make belts and headdresses by the Natufian culture of the Middle East, and are a possible indicator of early social stratification.
Shells of the species Dentalium pretiosum were used as money.


== References ==


== Further reading ==
For a comprehensive overview, see Reynolds, P. D. (2002). "The scaphopoda". Molluscan Radiation - Lesser-known Branches. Advances in Marine Biology 42. pp. 137236. doi:10.1016/S0065-2881(02)42014-7. ISBN 9780120261420. PMID 12094723. .