The olfactory system is the sensory system used for olfaction, or the sense of smell. Most mammals and reptiles have two distinct parts to their olfactory system: a main olfactory system and an accessory olfactory system. The main olfactory system detects volatile, airborne substances, while the accessory olfactory system senses fluid-phase stimuli. Behavioral evidence indicates that most often, the stimuli detected by the accessory olfactory system are pheromones.
The olfactory system is often spoken of along with the gustatory system as the chemosensory senses because both transduce chemical signals into perception.


== FunctionEdit ==
The mechanism of the olfactory system can be divided into a peripheral one, sensing an external stimulus and encoding it as an electric signal in neurons, and a central one, where all signals are integrated and processed in the central nervous system.


=== PeripheralEdit ===

In mammals, the main olfactory system detects odorants that are inhaled through the nose, where they contact the main olfactory epithelium, which contains various olfactory receptors. These olfactory receptors are membrane proteins of bipolar olfactory receptor neurons in the olfactory epithelium. Rather than binding specific ligands like most receptors, olfactory receptors display affinity for a range of odor molecules. Olfactory neurons transduce receptor activation into electrical signals in neurons. The signals travel along the olfactory nerve. The olfactory nerve, similar to the optic nerve, is not part of the peripheral nervous system but is defined as a part of the brain. This nerve terminates in the olfactory bulb, which also belongs to the central nervous system. The complex set of olfactory receptors on different olfactory neurons can distinguish a new odor from the background environmental odors and determine the concentration of the odor.


=== CentralEdit ===
Axons from the olfactory sensory neurons converge in the olfactory bulb to form clusters called glomeruli (singular glomerulus). Inside the glomerulus, the axons contact the dendrites of mitral cells and several other types of cells. Mitral cells send their axons to a number of brain areas, including the anterior olfactory nucleus, piriform cortex, the medial amygdala, the entorhinal cortex, and the olfactory tubercle.
The piriform cortex is probably the area most closely associated with identifying the odor. The medial amygdala is involved in social functions such as mating and the recognition of animals of the same species. The entorhinal cortex is associated with memory, e.g. to pair odors with proper memories. The exact functions of these higher areas are a matter of scientific research and debate.
In the central nervous system, odors are represented as patterns of neural activity. These representations may be encoded by space (a pattern of activated neurons across a given olfactory region corresponds to the odor), time (a pattern of action potentials by multiple neurons corresponds to the odor) or a combination of the two. Scientists debate whether the odor code is primarily temporal or spatial.


== Olfactory dysfunctionsEdit ==
Destruction to olfactory bulb, tract, and primary cortex (brodmann area 34) results in ipsilateral anosmia; also, irritative lesion of the uncus results in olfactory hallucinations.
Damage to the olfactory system can occur by traumatic brain injury, cancer, infection, inhalation of toxic fumes, or neurodegenerative diseases such as Parkinson's disease and Alzheimer's disease. These conditions can cause anosmia. In contrast, recent finding suggested the molecular aspects of olfactory dysfunction can be recognized as a hallmark of amyloidogenesis-related diseases and there may even be a causal link through the disruption of multivalent metal ion transport and storage. Doctors can detect damage to the olfactory system by presenting the patient with odors via a scratch and sniff card or by having the patient close their eyes and try to identify commonly available odors like coffee or peppermint candy. Doctors must exclude other diseases that inhibit or eliminate 'the sense of smell' such as chronic colds or sinusitus before making the diagnosis that there is permanent damage to the olfactory system.
Olfactory problems can be divided into different types based on their malfunction. The olfactory dysfunction can be total (anosmia), incomplete (partial anosmia, hyposmia, or microsmia), distorted (dysosmia), or can be characterized by spontaneous sensations like phantosmia. An inability to recognize odors despite a normally functioning olfactory system is termed olfactory agnosia. Hyperosmia is a rare condition typified by an abnormally heightened sense of smell. Like vision and hearing, the olfactory problems can be bilateral or unilateral meaning if a person has anosmia on the right side of the nose but not the left, it is a unilateral right anosmia. On the other hand, if it is on both sides of the nose it is called bilateral anosmia or total anosmia.


=== Causes of olfactory dysfunctionEdit ===
The common causes of olfactory dysfunction: advanced age, viral infections, exposure to toxic chemicals, head trauma, and neurodegenerative diseases.


==== AgeEdit ====
Age is the strongest reason for olfactory decline in healthy adults, having even greater impact than does cigarette smoking. Age-related changes in smell function often go unnoticed and smell ability is rarely tested clinically unlike hearing and vision. 2% of people under 65 years of age have chronic smelling problems. This increases greatly between people of ages 65 and 80 with about half experiencing significant problems smelling. Then for adults over 80, the numbers rise to almost 75%. The basis for age-related changes in smell function include closure of the cribriform plate, and cumulative damage to the olfactory receptors from repeated viral and other insults throughout life.


==== Viral infectionsEdit ====
The most common cause of permanent hyposmia and anosmia are upper respiratory infections. Such dysfunctions show no change over time and can sometimes reflect damage not only to the olfactory epithelium, but also to the central olfactory structures as a result of viral invasions into the brain. Among these virus-related disorders are the common cold, hepatitis, influenza and influenza-like illness, as well as herpes. Most viral infections are unrecognizable because they are so mild or entirely asymptomatic.


==== Exposure to toxic chemicalsEdit ====
Chronic exposure to some airborne toxins such as herbicides, pesticides, solvents, and heavy metals (cadmium, chromium, nickel, and manganese), can alter the ability to smell. These agents not only damage the olfactory epithelium, but they are likely to enter the brain via the olfactory mucosa.


==== Head traumaEdit ====
Trauma-related olfactory dysfunction depends on the severity of the trauma and whether strong acceleration/deceleration of the head occurred. Occipital and side impact causes more damage to the olfactory system than frontal impact.


==== Neurodegenerative diseasesEdit ====
Neurologists have observed that olfactory dysfunction is a cardinal feature of several neurodegenerative diseases such as Alzheimer's disease and Parkinson's disease. Most of these patients are unaware of an olfactory deficit until after testing where 85% to 90% of early-stage patients showed decrease activity in central odor processing structures.
Other neurodegenerative diseases that affect olfactory dysfunction include Huntington's disease, multi-infarct dementia, amyotrophic lateral sclerosis, and schizophrenia. These diseases have more moderate effects on the olfactory system than Alzheimer's or Parkinson's diseases. Furthermore, progressive supranuclear palsy and parkinsonism are associated with only minor olfactory problems. These findings have led to the suggestion that olfactory testing may help in the diagnosis of several different neurodegenerative diseases.
Neurodegenerative diseases with well-established genetic determinants are also associated with olfactory dysfunction. Such dysfunction, for example, is found in patients with familial Parkinson's disease and those with Down syndrome. Further studies have concluded that the olfactory loss may be associated with intellectual disability, rather than any Alzheimer's disease-like pathology.
Huntington's disease is also associated with problems in odor identification, detection, discrimination, and memory. The problem is prevalent once the phenotypic elements of the disorder appear, although it is unknown how far in advance the olfactory loss precedes the phenotypic expression.


== HistoryEdit ==
Linda B. Buck and Richard Axel won the 2004 Nobel Prize in Physiology or Medicine for their work on the olfactory system.


== See alsoEdit ==
Olfactory transduction
Anosmia (loss of smell)
Sinusitis


== ReferencesEdit ==


== External linksEdit ==
 Media related to Olfactory system at Wikimedia Commons