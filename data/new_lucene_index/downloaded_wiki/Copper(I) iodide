Copper(I) iodide is the inorganic compound with the formula CuI. It is also known as cuprous iodide. It is useful in a variety of applications ranging from organic synthesis to cloud seeding.
Pure copper(I) iodide is white, but samples are often tan or even, when found in nature as rare mineral marshite, reddish brown, but such colour is due to the presence of impurities. It is common for samples of iodide-containing compounds to become discolored due to the facile aerobic oxidation of the iodide anion to molecular iodine.


== Structure ==
Copper(I) iodide, like most "binary" (containing only two elements) metal halides, is an inorganic polymer. It has a rich phase diagram, meaning that it exists in several crystalline forms. It adopts a zinc blende structure below 390 C (-CuI), a wurtzite structure between 390 and 440 C (-CuI), and a rock salt structure above 440 C (-CuI). The ions are tetrahedrally coordinated when in the zinc blende or the wurtzite structure, with a Cu-I distance of 2.338 . Copper(I) bromide and copper(I) chloride also transform from the zinc blende structure to the wurtzite structure at 405 and 435 C, respectively. Therefore, the longer the copper  halide bond length, the lower the temperature needs to be to change the structure from the zinc blende structure to the wurtzite structure. The interatomic distances in copper(I) bromide and copper(I) chloride are 2.173 and 2.051 , respectively.


== Preparation ==
Copper(I) iodide can be prepared by heating iodine and copper in concentrated hydriodic acid, HI. In the laboratory however, copper(I) iodide is prepared by simply mixing an aqueous solution of sodium or potassium iodide and a soluble copper(II) salt such copper sulfate.
Cu2+ + 2I  CuI2
The CuI2 immediately decomposes to iodine and insoluble copper(I) iodide, releasing I2.
2 CuI2  2 CuI + I2
This reaction has been employed as a means of assaying copper(II) samples, since the evolved I2 can be analyzed by redox titration. The reaction in itself may look rather odd, as using the rule of thumb for a proceeding redox reaction, Eooxidator  Eoreductor > 0, this reaction fails. The quantity is below zero, so the reaction should not proceed. But the equilibrium constant for the reaction is 1.38*1013. By using fairly moderate concentrates of 0.1 mol/L for both iodide and Cu2+, the concentration of Cu+ is calculated as 3*107. As a consequence, the product of the concentrations is far in excess of the solubility product, so copper(I)iodide precipitates. The process of precipitation lowers the copper(I) concentration, providing an entropic driving force according to Le Chatelier's principle, and allowing the redox reaction to proceed.
CuI is poorly soluble in water (0.00042 g/L at 25 C), but it dissolves in the presence of NaI or KI to give the linear anion [CuI2]. Dilution of such solutions with water reprecipitates CuI. This dissolutionprecipitation process is employed to purify CuI, affording colorless samples.


== Uses ==
CuI has several uses:
CuI is used as a reagent in organic synthesis. In combination with 1,2- or 1,3 diamine ligands, CuI catalyzes the conversion of aryl, heteroaryl, and vinyl bromides into the corresponding iodides. NaI is the typical iodide source and dioxane is a typical solvent (see aromatic Finkelstein reaction). Aryl halides are used to form carboncarbon and carbonheteroatom bonds in process such as the Heck, Stille, Suzuki, Sonogashira and Ullmann type coupling reactions. Aryl iodides, however, are more reactive than the corresponding aryl bromides or aryl chlorides. 2-Bromo-1-octen-3-ol and 1-nonyne are coupled when combined with dichlorobis(triphenylphosphine)palladium(II), CuI, and diethylamine to form 7-methylene-8-hexadecyn-6-ol.
CuI is used in cloud seeding, altering the amount or type of precipitation of a cloud, or their structure by dispersing substances into the atmosphere which increase water's ability to form droplets or crystals. CuI provides a sphere for moisture in the cloud to condense around, causing precipitation to increase and cloud density to decrease.
The structural properties of CuI allow CuI to stabilize heat in nylon in commercial and residential carpet industries, automotive engine accessories, and other markets where durability and weight are a factor.
CuI is used as a source of dietary iodine in table salt and animal feed.
CuI is used in the detection of mercury. Upon contact with mercury vapors, the originally white compound changes color to form copper tetraiodomercurate, which has a brown color.


== References ==
^ Lide, David R., ed. (2006). CRC Handbook of Chemistry and Physics (87th ed.). Boca Raton, FL: CRC Press. ISBN 0-8493-0487-3. 
^ Skoog West Holler Crouch. Fundamentals of Inorganic Chemistry. Brooks/Cole, 2004, pp. A-6 ISBN 978-0-03-035523-3
^ Sigma-Aldrich Co., Copper(I) iodide. Retrieved on 2014-09-09.
^ a b c "NIOSH Pocket Guide to Chemical Hazards #0150". National Institute for Occupational Safety and Health (NIOSH). 
^ a b Kauffman, G. B.; Fang, L. Y. (1983). "Purification of Copper(I) Iodide". Inorg. Synth. Inorganic Syntheses 22: 101103. doi:10.1002/9780470132531.ch20. ISBN 978-0-470-13253-1. 
^ Wells, A. F. Structural Inorganic Chemistry Oxford University Press, Oxford, (1984). 5th ed., p. 410 and 444.
^ Holleman, A. F.; Wiberg, E. "Inorganic Chemistry" Academic Press: San Diego, 2001. ISBN 0-12-352651-5.
^ The value depends on the specific half-reaction for iodine. The value itself is calculated by using the formula: Kredox=10^{(nox*nred/0.0591)*(Eooxidator  Eoreductor)} which in itself is easily derived from the Nernst equations for the specific half reactions. Using Eoox=EoCu2+/Cu+ = 0.15; nox = 1 for copper; Eored=EoI/I2 = 0.52; nred = 2 for iodine
^ Klapars, A.; Buchwald, S. L. (2002). "Copper-Catalyzed Halogen Exchange in Aryl Halides: an Aromatic Finkelstein Reaction". J. Am. Chem. Soc. 124 (50): 14845. doi:10.1021/ja028865v. PMID 12475315. 
^ Marshall, J. A.; Sehon, C. A. "Isomerization of -Alkynyl Allylic Alcohols to Furans Catalyzed by Silver Nitrate on Silica Gel: 2-Pentyl-3-methyl-5-heptylfuran". Org. Synth. 76: 263. 
^ a b H. W. Richardson "Copper Compounds" in Ullmann's Encyclopedia of Industrial Chemistry 2005, Wiley-VCH, Weinheim. doi:10.1002/14356007.a07 567


== Sources ==
Macintyre, J. Dictionary of Inorganic Compounds. Chapman and Hall, London, (1992). Vol. 3, p. 3103.


== External links ==
Chemicalland properties database
National Pollutant Inventory  Copper and compounds fact sheet