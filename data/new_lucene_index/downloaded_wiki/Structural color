Structural coloration is the production of colour by microscopically structured surfaces fine enough to interfere with visible light, sometimes in combination with pigments. For example, peacock tail feathers are pigmented brown, but their microscopic structure makes them also reflect blue, turquoise, and green light, and they are often iridescent.
Structural coloration was first observed by English scientists Robert Hooke and Isaac Newton, and its principle  wave interference  explained by Thomas Young a century later. Young correctly described iridescence as the result of interference between reflections from two (or more) surfaces of thin films, combined with refraction as light enters and leaves such films. The geometry then determines that at certain angles, the light reflected from both surfaces adds (interferes constructively), while at other angles, the light subtracts. Different colours therefore appear at different angles.
In animals such as on the feathers of birds and the scales of butterflies, interference is created by a range of photonic mechanisms, including diffraction gratings, selective mirrors, photonic crystals, crystal fibres, matrices of nanochannels and proteins that can vary their configuration. Some cuts of meat also show structural coloration due to the exposure of the periodic arrangement of the muscular fibres. Many of these photonic mechanisms correspond to elaborate structures visible by electron microscopy. In plants, brilliant colours are produced by structures within cells. The most brilliant blue coloration known in any living tissue is found in the marble berries of Pollia condensata, where a spiral structure of cellulose fibrils produces Bragg's law scattering of light.
Structural coloration has potential for industrial, commercial and military application, with biomimetic surfaces that could provide brilliant colours, adaptive camouflage, efficient optical switches and low-reflectance glass.


== History ==

In his 1665 book Micrographia, Robert Hooke described the "fantastical" (structural, not pigment) colours of the peacock's feathers:
"The parts of the Feathers of this glorious Bird appear, through the Microscope, no less gaudy then do the whole Feathers; for, as to the naked eye 'tis evident that the stem or quill of each Feather in the tail sends out multitudes of Lateral branches,  so each of those threads in the Microscope appears a large long body, consisting of a multitude of bright reflecting parts.
 their upper sides seem to me to consist of a multitude of thin plated bodies, which are exceeding thin, and lie very close together, and thereby, like mother of Pearl shells, do not onely reflect a very brisk light, but tinge that light in a most curious manner; and by means of various positions, in respect of the light, they reflect back now one colour, and then another, and those most vividly. Now, that these colours are onely fantastical ones, that is, such as arise immediately from the refractions of the light, I found by this, that water wetting these colour'd parts, destroy'd their colours, which seem'd to proceed from the alteration of the reflection and refraction."
In his 1704 book Opticks, Isaac Newton described the mechanism of the colours (other than the brown pigment) of peacock tail feathers. Newton noted that
"The finely colour'd Feathers of some Birds, and particularly those of Peacocks Tails, do, in the very same part of the Feather, appear of several Colours in several Positions of the Eye, after the very same manner that thin Plates were found to do in the 7th and 19th Observations, and therefore their Colours arise from the thinness of the transparent parts of the Feathers; that is, from the slenderness of the very fine Hairs, or Capillamenta, which grow out of the sides of the grosser lateral Branches or Fibres of those Feathers."

Thomas Young (17731829) extended Newton's particle theory of light by showing that light could also behave as a wave. He showed in 1803 that light could diffract from sharp edges or slits, creating interference patterns.
In his 1892 book Animal Coloration, Frank Evers Beddard (18581925) acknowledged the existence of structural colours:
"The colours of animals are due either solely to the presence of definite pigments in the skin, or  beneath the skin; or they are partly caused by optical effects due to the scattering, diffraction or unequal refraction of the light rays. Colours of the latter kind are often spoken of as structural colours; they are caused by the structure of the coloured surfaces. The metallic lustre of the feathers of many birds, such as the humming birds, is due to the presence of excessively fine striae upon the surface of the feathers."
But Beddard then largely dismissed structural coloration, firstly as subservient to pigments: "in every case the [structural] colour needs for its display a background of dark pigment;" and then by asserting its rarity: "By far the commonest source of colour in invertebrate animals is the presence in the skin of definite pigments ...", though he does later admit that the Cape golden mole has "structural peculiarities" in its hair that "give rise to brilliant colours".


== Principles ==


=== Structure not pigment ===

Structural coloration is caused by interference effects rather than by pigments. Colours are produced when a material is scored with fine parallel lines, formed of one or more parallel thin layers, or otherwise composed of microstructures on the scale of the colour's wavelength.
Structural coloration is responsible for the blues and greens of the feathers of many birds (the bee-eater, kingfisher and roller, for example), as well as many butterfly wings and beetle wing-cases (elytra). These are often iridescent, as in peacock feathers and nacreous shells such as of pearl oysters (Pteriidae) and Nautilus. This is because the reflected colour depends on the viewing angle, which in turn governs the apparent spacing of the structures responsible. Structural colours can be combined with pigment colours: peacock feathers are pigmented brown with melanin.


=== Principle of iridescence ===

Iridescence, as explained by Thomas Young in 1803, is created when extremely thin films reflect part of the light falling on them from their top surfaces. The rest of the light goes through the films, and a further part of it is reflected from their bottom surfaces. The two sets of reflected waves travel back upwards in the same direction. But since the bottom-reflected waves travelled a little further  controlled by the thickness and refractive index of the film, and the angle at which the light fell  the two sets of waves are out of phase. When the waves are one or more whole wavelength apart  in other words at certain specific angles, they add (interfere constructively), giving a strong reflection. At other angles and phase differences, they can subtract, giving weak reflections. The thin film therefore selectively reflects just one wavelength  a pure colour  at any given angle, but other wavelengths  different colours  at different angles. So, as a thin-film structure like a butterfly's wing or bird's feather moves, it seems to change colour.


== Mechanisms ==


=== Fixed structures ===

A number of fixed structures can create structural colours, by mechanisms including diffraction gratings, selective mirrors, photonic crystals, crystal fibres and deformed matrices. Structures can be far more elaborate than a single thin film: films can be stacked up to give strong iridescence, to combine two colours, or to balance out the inevitable change of colour with angle to give a more diffuse, less iridescent effect. Each mechanism offers a specific solution to the problem of creating a bright colour or combination of colours visible from different directions.

A diffraction grating constructed of layers of chitin and air gives rise to the iridescent colours of various butterfly wing scales as well as to the tail feathers of birds such as the peacock. Hooke and Newton were correct in their claim that the peacock's colours are created by interference, but the structures responsible, being close to the wavelength of light in scale (see micrographs), were smaller than the striated structures they could see with their light microscopes. Another way to produce a diffraction grating is with tree-shaped arrays of chitin, as in the wing scales of some of the brilliantly coloured tropical Morpho butterflies (see drawing). Yet another variant exists in Parotia lawesii, Lawes's parotia, a bird of paradise. The barbules of the feathers of its brightly coloured breast patch are V-shaped, creating thin-film microstructures that strongly reflect two different colours, bright blue-green and orange-yellow. When the bird moves the colour switches sharply between these two colours, rather than drifting iridescently. During courtship, the male bird systematically makes small movements to attract females, so the structures must have evolved through sexual selection.
Photonic crystals can be formed in different ways. In Parides sesostris, the emerald-patched cattleheart butterfly, photonic crystals are formed of arrays of nano-sized holes in the chitin of the wing scales. The holes have a diameter of about 150 nanometres and are about the same distance apart. The holes are arranged regularly in small patches; neighbouring patches contain arrays with differing orientations. The result is that these emerald-patched cattleheart scales reflect green light evenly at different angles instead of being iridescent. In Lamprocyphus augustus, a weevil from Brazil, the chitin exoskeleton is covered in iridescent green oval scales. These contain diamond-based crystal lattices oriented in all directions to give a brilliant green coloration that hardly varies with angle. The scales are effectively divided into pixels about a metre wide. Each such pixel is a single crystal and reflects light in a direction different from its neighbours.

Selective mirrors to create interference effects are formed of micron-sized bowl-shaped pits lined with multiple layers of chitin in the wing scales of Papilio palinurus, the emerald swallowtail butterfly. These act as highly selective mirrors for two wavelengths of light. Yellow light is reflected directly from the centres of the pits; blue light is reflected twice by the sides of the pits. The combination appears green, but can be seen as an array of yellow spots surrounded by blue circles under a microscope.
Crystal fibres, formed of hexagonal arrays of hollow nanofibres, create the bright iridescent colours of the bristles of Aphrodita, the sea mouse, a non-wormlike genus of marine annelids. The colours are aposematic, warning predators not to attack. The chitin walls of the hollow bristles form a hexagonal honeycomb-shaped photonic crystal; the hexagonal holes are 0.51 metre apart. The structure behaves optically as if it consisted of a stack of 88 diffraction gratings, making Aphrodita one of the most iridescent of marine organisms.

Deformed matrices, consisting of randomly oriented nanochannels in a spongelike keratin matrix, create the diffuse non-iridescent blue colour of Ara ararauna, the blue-and-yellow macaw. Since the reflections are not all arranged in the same direction, the colours, while still magnificent, do not vary much with angle, so they are not iridescent.

Spiral coils, formed of helicoidally stacked cellulose microfibrils, create Bragg reflection in the "marble berries" of the African herb Pollia condensata, resulting in the most intense blue coloration known in nature. The berry's surface has four layers of cells with thick walls, containing spirals of transparent cellulose spaced so as to allow constructive interference with blue light. Below these cells is a layer two or three cells thick containing dark brown tannins. Pollia produces a stronger colour than the wings of Morpho butterflies, and is one of the first instances of structural coloration known from any plant. Each cell has its own thickness of stacked fibres, making it reflect a different colour from its neighbours, and producing a pixellated or pointillist effect with different blues speckled with brilliant green, purple and red dots. The fibres in any one cell are either left-handed or right-handed, so each cell circularly polarizes the light it reflects in one direction or the other. Pollia is the first organism known to show such random polarization of light, which, nevertheless does not have a visual function, as the seed-eating birds that visit this plant species are not able to perceive polarised light. Spiral microstructures are also found in scarab beetles where they produce iridescent colours.

Surface gratings, consisting on ordered surface features due exposure of ordered muscle cells on cuts of meat. The structural coloration on meat cuts appears only after the ordered pattern of muscle fibrils is exposed and light is diffracted by the proteins in the fibrils. The coloration or wavelength of the diffracted light depends on the angle of observation and can be enhanced by covering the meat with translucent foils. Roughening the surface or removing water content by drying causes the structure to collapse, thus, the structural coloration to disappear.


=== Variable structures ===
Some animals including cephalopods like squid are able to vary their colours rapidly for both camouflage and signalling. The mechanisms include reversible proteins which can be switched between two configurations. The configuration of reflectin proteins in chromatophore cells in the skin of the Loligo pealeii squid is controlled by electric charge. When charge is absent, the proteins stack together tightly, forming a thin, more reflective layer; when charge is present, the molecules stack more loosely, forming a thicker layer. Since chromatophores contain multiple reflectin layers, the switch changes the layer spacing and hence the colour of light that is reflected.


== Examples ==


== In technology ==

Structural coloration could be exploited industrially and commercially, and research that could lead to such applications is under way. A direct parallel would be to create active or adaptive military camouflage fabrics that vary their colours and patterns to match their environments, just as chameleons and cephalopods do. The ability to vary reflectivity to different wavelengths of light could also lead to efficient optical switches that could function like transistors, enabling engineers to make fast optical computers and routers.
The surface of the compound eye of the housefly is densely packed with microscopic projections that have the effect of reducing reflection and hence increasing transmission of incident light. Similarly, the eyes of some moths have antireflective surfaces, again using arrays of pillars smaller than the wavelength of light. "Moth-eye" nanostructures could be used to create low-reflectance glass for windows, solar cells, display devices, and military stealth technologies. Antireflective biomimetic surfaces using the "moth-eye" principle can be manufactured by first creating a mask by lithography with gold nanoparticles, and then performing reactive-ion etching.


== See also ==
Animal coloration
Camouflage
Patterns in nature
Iridescence


== Bibliography ==


=== Pioneering books ===
Beddard, Frank Evers (1892). Animal Coloration, An Account of the Principal Facts and Theories Relating to the Colours and Markings of Animals. Swan Sonnenschein, London.
--- 2nd Edition, 1895.
Hooke, Robert (1665). Micrographia, John Martyn and James Allestry, London.
Newton, Isaac (1704). Opticks, William Innys, London.


=== Research ===
Fox, D.L. (1992). Animal Biochromes and Animal Structural Colours. University of California Press.
Johnsen, S. (2011). The Optics of Life: A Biologist's Guide to Light in Nature. Princeton University Press.
Kolle, M. (2011). Photonic Structures Inspired by Nature . Springer.


=== General books ===
Brebbia, C.A. (2011). Colour in Art, Design and Nature. WIT Press.
Lee, D.W. (2008). Nature's Palette: The Science of Plant Color. University of Chicago Press.


== Notes ==


== References ==


== External links ==
National Geographic News: Peacock Plumage Secrets Uncovered
Iridescent plumage in satin bowerbirds: Doucet et al, 2005
Causes of Color: Peacock feathers