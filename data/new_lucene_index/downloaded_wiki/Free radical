In chemistry, a radical (more precisely, a free radical) is an atom, molecule, or ion that has unpaired valence electrons. With some exceptions, these unpaired electrons make free radicals highly chemically reactive towards other substances, or even towards themselves: their molecules will often spontaneously dimerize or polymerize if they come in contact with each other. Most radicals are reasonably stable only at very low concentrations in inert media or in a vacuum.
A notable example of a free radical is the hydroxyl radical (HO), a molecule that has one unpaired electron on the oxygen atom. Two other examples are triplet oxygen and triplet carbene (:CH
2) which have two unpaired electrons. In contrast, the hydroxyl anion (HO) is not a radical, since the unpaired electron is resolved by the addition of an electron; singlet oxygen and singlet carbene are not radicals as the two electrons are paired.
Free radicals may be created in a number of ways, including synthesis with very dilute or rarefied reagents, reactions at very low temperatures, or breakup of larger molecules. The latter can be affected by any process that puts enough energy into the parent molecule, such as ionizing radiation, heat, electrical discharges, electrolysis, and chemical reactions. Indeed, radicals are intermediate stages in many chemical reactions.
Free radicals play an important role in combustion, atmospheric chemistry, polymerization, plasma chemistry, biochemistry, and many other chemical processes. In living organisms, the free radicals superoxide and nitric oxide and their reaction products regulate many processes, such as control of vascular tone and thus blood pressure. They also play a key role in the intermediary metabolism of various biological compounds. Such radicals can even be messengers in a process dubbed redox signaling. A radical may be trapped within a solvent cage or be otherwise bound.
Until late in the 20th century the word "radical" was used in chemistry to indicate any connected group of atoms, such as a methyl group or a carboxyl, whether it was part of a larger molecule or a molecule on its own. The qualifier "free" was then needed to specify the unbound case. Following recent nomenclature revisions, a part of a larger molecule is now called a functional group or substituent, and "radical" now implies "free". However, the old nomenclature may still occur in the literature.


== HistoryEdit ==

The first organic free radical identified was triphenylmethyl radical. This species was discovered by Moses Gomberg in 1900 at the University of Michigan USA.
The term radical was already in use when radical theory was developed. Louis-Bernard Guyton de Morveau introduced the phrase "radical" in 1785 and the phrase was employed by Antoine Lavoisier in 1789 in his Trait lmentaire de Chimie. A radical was identified as the root base of certain acids (The Latin word "radix" meaning "root"). Historically, the term radical in radical theory was also used for bound parts of the molecule, especially when they remain unchanged in reactions. These are now called functional groups. For example, methyl alcohol was described as consisting of a methyl "radical" and a hydroxyl "radical". Neither are radicals in the modern chemical sense, as they are permanently bound to each other, and have no unpaired, reactive electrons; however, they can be observed as radicals in mass spectrometry when broken apart by irradiation with energetic electrons.


== Depiction in chemical reactionsEdit ==
In chemical equations, free radicals are frequently denoted by a dot placed immediately to the right of the atomic symbol or molecular formula as follows:

Chlorine gas can be broken down by ultraviolet light to form atomic chlorine radicals.
Radical reaction mechanisms use single-headed arrows to depict the movement of single electrons:

The homolytic cleavage of the breaking bond is drawn with a 'fish-hook' arrow to distinguish from the usual movement of two electrons depicted by a standard curly arrow. It should be noted that the second electron of the breaking bond also moves to pair up with the attacking radical electron; this is not explicitly indicated in this case.
Free radicals also take part in radical addition and radical substitution as reactive intermediates. Chain reactions involving free radicals can usually be divided into three distinct processes. These are initiation, propagation, and termination.
Initiation reactions are those that result in a net increase in the number of free radicals. They may involve the formation of free radicals from stable species as in Reaction 1 above or they may involve reactions of free radicals with stable species to form more free radicals.
Propagation reactions are those reactions involving free radicals in which the total number of free radicals remains the same.
Termination reactions are those reactions resulting in a net decrease in the number of free radicals. Typically two free radicals combine to form a more stable species, for example: 2Cl Cl2


== FormationEdit ==
The formation of radicals may involve the breaking of covalent bonds by homolysis, a process that requires significant amounts of energy. Such energies are known as homolytic bond dissociation energies, usually abbreviated as "H". Splitting H2 into 2H, for example, requires a H of +435 kJmol-1, while splitting Cl2 into 2Cl requires a H of +243 kJmol-1.
The bond energy between two covalently-bonded atoms is affected by the structure of the molecule as a whole, not just the identity of the two atoms. Likewise, radicals requiring more energy to form are less stable than those requiring less energy. An additional barrier can be the selection rule. Propagation, however, is very exothermic.
Radical formation through homolytic bond cleavage most often happens between two atoms of similar electronegativity; in organic chemistry, this is often between the OO bond in peroxide species or between ON bonds. Radicals may also be formed by single-electron oxidation or reduction of an atom or molecule: an example is the production of superoxide by the electron transport chain. Early studies in organometallic chemistry  especially F. A. Paneth and K. Hahnfeld's studies of tetra-alkyl lead species during the 1930s  supported the heterolytic fission of bonds and a radical-based mechanism. Although radical ions do exist, most species are electrically neutral.


== Persistence and stabilityEdit ==

Although radicals are generally short-lived due to their reactivity, there are long-lived radicals. These are categorized as follows:
Stable radicals
The prime example of a stable radical is molecular dioxygen (O2). Another common example is nitric oxide (NO). Organic radicals can be long lived if they occur in a conjugated  system, such as the radical derived from -tocopherol (vitamin E). There are also hundreds of examples of thiazyl radicals, which show low reactivity and remarkable thermodynamic stability with only a very limited extent of  resonance stabilization.
Persistent radicals
Persistent radical compounds are those whose longevity is due to steric crowding around the radical center, which makes it physically difficult for the radical to react with another molecule. Examples of these include Gomberg's triphenylmethyl radical, Fremy's salt (Potassium nitrosodisulfonate, (KSO3)2NO), nitroxides, (general formula R2NO) such as TEMPO, TEMPOL, nitronyl nitroxides, and azephenylenyls and radicals derived from PTM (perchlorophenylmethyl radical) and TTM (tris(2,4,6-trichlorophenyl)methyl radical). Persistent radicals are generated in great quantity during combustion, and "may be responsible for the oxidative stress resulting in cardiopulmonary disease and probably cancer that has been attributed to exposure to airborne fine particles."
Diradicals
Diradicals are molecules containing two radical centers. Multiple radical centers can exist in a molecule. Atmospheric oxygen naturally exists as a diradical in its ground state as triplet oxygen. The low reactivity of atmospheric oxygen is due to its diradical state. Non-radical states of dioxygen are actually less stable than the diradical. The relative stability of the oxygen diradical is primarily due to the spin-forbidden nature of the triplet-singlet transition required for it to grab electrons, i.e., "oxidize". The diradical state of oxygen also results in its paramagnetic character, which is demonstrated by its attraction to an external magnet.


== ReactivityEdit ==

Radical alkyl intermediates are stabilized by similar physical processes to carbocations: as a general rule, the more substituted the radical center is, the more stable it is. This directs their reactions. Thus, formation of a tertiary radical (R3C) is favored over secondary (R2HC), which is favored over primary (RH2C). Likewise, radicals next to functional groups such as carbonyl, nitrile, and ether are more stable than tertiary alkyl radicals.
Radicals attack double bonds. However, unlike similar ions, such radical reactions are not as much directed by electrostatic interactions. For example, the reactivity of nucleophilic ions with ,-unsaturated compounds (C=CC=O) is directed by the electron-withdrawing effect of the oxygen, resulting in a partial positive charge on the carbonyl carbon. There are two reactions that are observed in the ionic case: the carbonyl is attacked in a direct addition to carbonyl, or the vinyl is attacked in conjugate addition, and in either case, the charge on the nucleophile is taken by the oxygen. Radicals add rapidly to the double bond, and the resulting -radical carbonyl is relatively stable; it can couple with another molecule or be oxidized. Nonetheless, the electrophilic/neutrophilic character of radicals has been shown in a variety of instances. One example is the alternating tendency of the copolymerization of maleic anhydride (electrophilic) and styrene (slightly nucleophilic).
In intramolecular reactions, precise control can be achieved despite the extreme reactivity of radicals. In general, radicals attack the closest reactive site the most readily. Therefore, when there is a choice, a preference for five-membered rings is observed: four-membered rings are too strained, and collisions with carbons six or more atoms away in the chain are infrequent.
Triplet carbenes and nitrenes, which are diradicals, have distinctive chemistry.


== CombustionEdit ==

A familiar free-radical reaction is combustion. The oxygen molecule is a stable diradical, best represented by O-O. Because spins of the electrons are parallel, this molecule is stable. While the ground state of oxygen is this unreactive spin-unpaired (triplet) diradical, an extremely reactive spin-paired (singlet) state is available. For combustion to occur, the energy barrier between these must be overcome. This barrier can be overcome by heat, requiring high temperatures. The triplet-singlet transition is also "forbidden". This presents an additional barrier to the reaction. It also means molecular oxygen is relatively unreactive at room temperature except in the presence of a catalytic heavy atom such as iron or copper.
Combustion consists of various radical chain reactions that the singlet radical can initiate. The flammability of a given material strongly depends on the concentration of free radicals that must be obtained before initiation and propagation reactions dominate leading to combustion of the material. Once the combustible material has been consumed, termination reactions again dominate and the flame dies out. As indicated, promotion of propagation or termination reactions alters flammability. For example, because lead itself deactivates free radicals in the gasoline-air mixture, tetraethyl lead was once commonly added to gasoline. This prevents the combustion from initiating in an uncontrolled manner or in unburnt residues (engine knocking) or premature ignition (preignition).
When a hydrocarbon is burned, a large number of different oxygen radicals are involved. Initially, hydroperoxyl radical (HOO) are formed. These then react further to give organic hydroperoxides that break up into hydroxyl radicals (HO).


== PolymerizationEdit ==
In addition to combustion, many polymerization reactions involve free radicals. As a result, many plastics, enamels, and other polymers are formed through radical polymerization. For instance, drying oils and alkyd paints harden due to radical crosslinking by oxygen from the atmosphere.
Recent advances in radical polymerization methods, known as living radical polymerization, include:
Reversible addition-fragmentation chain transfer (RAFT)
Atom transfer radical polymerization (ATRP)
Nitroxide mediated polymerization (NMP)
These methods produce polymers with a much narrower distribution of molecular weights.


== Atmospheric radicalsEdit ==

The most common radical in the lower atmosphere is molecular dioxygen. Photodissociation of source molecules produces other free radicals. In the lower atmosphere, the most important examples of free radical production are the photodissociation of nitrogen dioxide to give an oxygen atom and nitric oxide (see eq. 1 below), which plays a key role in smog formationand the photodissociation of ozone to give the excited oxygen atom O(1D) (see eq. 2 below). The net and return reactions are also shown (eq. 3 and 4, respectively).

In the upper atmosphere, a particularly important source of radicals is the photodissociation of normally unreactive chlorofluorocarbons (CFCs) by solar ultraviolet radiation, or by reactions with other stratospheric constituents (see eq. 1 below). These reactions give off the chlorine radical, Cl, which reacts with ozone in a catalytic chain reaction ending in Ozone depletion and regeneration of the chlorine radical, allowing it to reparticipate in the reaction (see eq. 24 below). Such reactions are believed to be the primary cause of depletion of the ozone layer (the net result is shown in eq. 5 below), and this is why the use of chlorofluorocarbons as refrigerants has been restricted.


== In biologyEdit ==

Free radicals play an important role in a number of biological processes. Many of these are necessary for life, such as the intracellular killing of bacteria by phagocytic cells such as granulocytes and macrophages. Researchers have also implicated free radicals in certain cell signalling processes, known as redox signaling. Some of these signaling molecules involve the free radical-induce peroxidation of tissue stores of polyunsaturated fatty acids such as linoleic acid, arachidonic acid, and docosahexaenoic acid. For example, free radical attack of linoleic acid produces a series of 13-Hydroxyoctadecadienoic acids and 9-Hydroxyoctadecadienoic acids which may act to regulate localized tissue inflammatory and/or healing responses, pain perception, and the proliferation of malignant cells. Free radical attacks on arachidonic acid and docosahexaenoic acid produce a similar but broader array of signaling products.
The two most important oxygen-centered free radicals are superoxide and hydroxyl radical. They derive from molecular oxygen under reducing conditions. However, because of their reactivity, these same free radicals can participate in unwanted side reactions resulting in cell damage. Excessive amounts of these free radicals can lead to cell injury and death, which may contribute to many diseases such as cancer, stroke, myocardial infarction, diabetes and major disorders. Many forms of cancer are thought to be the result of reactions between free radicals and DNA, potentially resulting in mutations that can adversely affect the cell cycle and potentially lead to malignancy. Some of the symptoms of aging such as atherosclerosis are also attributed to free-radical induced oxidation of cholesterol to 7-ketocholesterol. In addition free radicals contribute to alcohol-induced liver damage, perhaps more than alcohol itself. Free radicals produced by cigarette smoke are implicated in inactivation of alpha 1-antitrypsin in the lung. This process promotes the development of emphysema.
Free radicals may also be involved in Parkinson's disease, senile and drug-induced deafness, schizophrenia, and Alzheimer's. The classic free-radical syndrome, the iron-storage disease hemochromatosis, is typically associated with a constellation of free-radical-related symptoms including movement disorder, psychosis, skin pigmentary melanin abnormalities, deafness, arthritis, and diabetes mellitus. The free-radical theory of aging proposes that free radicals underlie the aging process itself. Similarly, the process of mitohormesis suggests that repeated exposure to free radicals may extend life span.
Because free radicals are necessary for life, the body has a number of mechanisms to minimize free-radical-induced damage and to repair damage that occurs, such as the enzymes superoxide dismutase, catalase, glutathione peroxidase and glutathione reductase. In addition, antioxidants play a key role in these defense mechanisms. These are often the three vitamins, vitamin A, vitamin C and vitamin E and polyphenol antioxidants. Furthermore, there is good evidence indicating that bilirubin and uric acid can act as antioxidants to help neutralize certain free radicals. Bilirubin comes from the breakdown of red blood cells' contents, while uric acid is a breakdown product of purines. Too much bilirubin, though, can lead to jaundice, which could eventually damage the central nervous system, while too much uric acid causes gout.


=== Reactive oxygen speciesEdit ===

Reactive oxygen species or ROS are species such as superoxide, hydrogen peroxide, and hydroxyl radical, commonly associated with cell damage. ROS form as a natural by-product of the normal metabolism of oxygen and have important roles in cell signaling.
Oxybenzone has been found to form free radicals in sunlight, and therefore may be associated with cell damage as well. This only occurred when it was combined with other ingredients commonly found in sunscreens, like titanium oxide and octyl methoxycinnamate.
ROS attack the polyunsaturated fatty acid, linoleic acid, to form a series of 13-Hydroxyoctadecadienoic acid and 9-Hydroxyoctadecadienoic acid products that serve as signaling molecules that may trigger responses that counter the tissue injury which caused their formation. ROS attacks other polyunsaturated fatty acids, e.g. arachidonic acid and docosahexaenoic acid, to produce a similar series of signaling products.


== Loose definition of radicalsEdit ==
In most fields of chemistry, the historical definition of radicals contends that the molecules have nonzero spin. However, in fields including spectroscopy, chemical reaction, and astrochemistry, the definition is slightly different. Gerhard Herzberg, who won the Nobel prize for his research into the electron structure and geometry of radicals, suggested a looser definition of free radicals: "any transient (chemically unstable) species (atom, molecule, or ion)". The main point of his suggestion is that there are many chemically unstable molecules that have zero spin, such as C2, C3, CH2 and so on. This definition is more convenient for discussions of transient chemical processes and astrochemistry; therefore researchers in these fields prefer to use this loose definition.


== DiagnosticsEdit ==
Free radical diagnostic techniques include:
Electron spin resonance
A widely used technique for studying free radicals, and other paramagnetic species, is electron spin resonance spectroscopy (ESR). This is alternately referred to as "electron paramagnetic resonance" (EPR) spectroscopy. It is conceptually related to nuclear magnetic resonance, though electrons resonate with higher-frequency fields at a given fixed magnetic field than do most nuclei.
Nuclear magnetic resonance using a phenomenon called CIDNP
Chemical labelling
Chemical labelling by quenching with free radicals, e.g. with nitric oxide (NO) or DPPH (2,2-diphenyl-1-picrylhydrazyl), followed by spectroscopic methods like X-ray photoelectron spectroscopy (XPS) or absorption spectroscopy, respectively.
Use of free radical markers
Stable, specific or non-specific derivates of physiological substances can be measured e.g. lipid peroxidation products (isoprostanes, TBARS), amino acid oxidation products (meta-tyrosine, ortho-tyrosine, hydroxy-Leu, dityrosine etc.), peptide oxidation products (oxidized glutathione  GSSG)
2,2'-Azobis(2-amidinopropane) dihydrochloride (AAPH) is a chemical compound used to study the chemistry of the oxidation of drugs. It is a free radical-generating azo compound. It is gaining prominence as a model oxidant in small molecule and protein therapeutics for its ability to initiate oxidation reactions via both nucleophilic and free radical mechanisms.
Indirect method
Measurement of the decrease in the amount of antioxidants (e.g. TAS, reduced glutathione  GSH)
Trapping agents
Using a chemical species that reacts with free radicals to form a stable product that can then be readily measured (Hydroxyl radical and salicylic acid)


== See alsoEdit ==
-yl
Electron pair
Globally Harmonized System of Classification and Labelling of Chemicals
HofmannLffler reaction


== ReferencesEdit ==


== External linksEdit ==
Cannabinoids as antioxidants and neuroprotectants- The United States of America - the Department of Health and Human Services (Washington, DC)