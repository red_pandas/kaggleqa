An atoll (/tl/, /tl/, /tol/, /tl/, /tl/ or /tol/), sometimes called a coral atoll, is a ring-shaped coral reef including a coral rim that encircles a lagoon partially or completely. There may be coral islands/cays on the rim.  The coral of the atoll often sits atop the rim of an extinct seamount or volcano which has eroded or subsided partially beneath the water. The lagoon forms over the volcanic crater or caldera while the higher rim remains above water or at shallow depths that permit the coral to grow and form the reefs. For the atoll to persist, continued erosion or subsidence must be at a rate slow enough to permit reef growth upwards and outwards to replace the lost height.


== Usage ==
The word atoll comes from the Dhivehi (an Indo-Aryan language spoken on the Maldive Islands) word atholhu (Dhivehi: , [tu]), meaning an administrative subdivision.OED Its first recorded use in English was in 1625 as atollon  Charles Darwin recognized its indigenous origin and coined, in his The Structure and Distribution of Coral Reefs, the definition of atolls as "circular groups of coral islets" that is synonymous with "lagoon-island".
More modern definitions of atoll describe them as "annular reefs enclosing a lagoon in which there are no promontories other than reefs and islets composed of reef detritus" or "in an exclusively morphological sense, [as] a ring-shaped ribbon reef enclosing a lagoon".


== Distribution and size ==

The distribution of atolls around the globe is instructive: most of the world's atolls are in the Pacific Ocean (with concentrations in the Tuamotu Islands, Caroline Islands, Marshall Islands, Coral Sea Islands, and the island groups of Kiribati, Tuvalu and Tokelau) and Indian Ocean (the Atolls of the Maldives, the Lakshadweep Islands, the Chagos Archipelago and the Outer Islands of the Seychelles). The Atlantic Ocean has no large groups of atolls, other than eight atolls east of Nicaragua that belong to the Colombian department of San Andres and Providencia in the Caribbean.
Reef-building corals will thrive only in warm tropical and subtropical waters of oceans and seas, and therefore atolls are only found in the tropics and subtropics. The northernmost atoll of the world is Kure Atoll at 2824' N, along with other atolls of the Northwestern Hawaiian Islands. The southernmost atolls of the world are Elizabeth Reef at 2958' S, and nearby Middleton Reef at 2929' S, in the Tasman Sea, both of which are part of the Coral Sea Islands Territory. The next southerly atoll is Ducie Island in the Pitcairn Islands Group, at 2440' S. Bermuda is sometimes claimed as the "northernmost atoll" at a latitude of 3224' N. At this latitude coral reefs would not develop without the warming waters of the Gulf Stream. However, Bermuda is termed a pseudo-atoll because its general form, while resembling that of an atoll, has a very different mode of formation. While there is no atoll directly on the equator, the closest atoll to the Equator is Aranuka of Kiribati, with its southern tip just 12 km north of the equator.
The largest atolls by total area (lagoon plus reef and dry land) are listed below:
Saya de Malha Bank, Western Indian Ocean (35000 km) (without separate North Bank), submerged, least depth 7 m,
Lansdowne Bank, west of New Caledonia (21000 km), submerged, least depth 3.7 m 
Great Chagos Bank (12642 km, land area only 4.5 km)
Reed Bank, Spratly Islands (8866 km), submerged, least depth 9 m
Macclesfield Bank, South China Sea (6448 km), submerged, least depth 9.2 m
North Bank (Ritchie Bank, north of Saya de Malha Bank) (5800 km), submerged, least depth <10 m
Rosalind Bank, Caribbean (4500 km), submerged, least depth 7.3 m
Boduthiladhunmathi (Thiladhunmathi-Miladhunmadulu) Atoll, Maldives, (two names, but a single atoll structure) (3850 km, land area 51 km)
Chesterfield Islands, New Caledonia (3500 km, land area <10 km)
Huvadhu Atoll, Maldives (3152 km, land area 38.5 km)
Truk Lagoon, Chuuk (3130 km)
Sabalana Islands, Indonesia (2694 km)
Nukuoro atoll, Federated States of Micronesia, lagoon, is 40 km, land area of 1.7 km, divided among more than 40 islets that lie on the northern, eastern and southern sides of the lagoon
Lihou Reef, Coral Sea (2529 km, land area 1 km)
Bassas de Pedro (2474.33 km), submerged, least depth 16.4 m
Ardasier Bank, Spratly Islands (2347 km), cay on the south side?
Kwajalein, Marshall Islands (2304 km, land area 16.4 km)
Diamond Islets Bank, Coral Sea (2282 km, land area <1 km)
Namonuito Atoll, Chuuk (2267 km, land area 4.4 km)
Ari Atoll, Maldives (2252 km, land area 69 km)
Maro Reef, Northwestern Hawaiian Islands, 1934 km
Rangiroa, Tuamotu Islands (1762 km, land area 79 km)
Kolhumadulhu Atoll, Maldives (1617 km, land area 79 km)
Kaafu Atoll (North Mal Atoll), Maldives (1565 km, land area 69 km)
Ontong Java, Solomon Islands (1500 km, land area 12 km)
In most cases, the land area of an atoll is very small in comparison to the total area. Atoll islands are low lying, with their elevations less than 5 meters (9). Measured by total area, Lifou (1146 km) is the largest raised coral atoll of the world, followed by Rennell Island (660 km). More sources however list as the largest atoll in the world in terms of land area Kiritimati, which is also a raised coral atoll (321.37 km land area; according to other sources even 575 km), 160 km main lagoon, 168 km other lagoons (according to other sources 319 km total lagoon size). The remains of an ancient atoll as a hill in a limestone area is called a reef knoll. The second largest atoll by dry land area is Aldabra with 155 km. The largest atoll in terms of island numbers is Huvadhu Atoll in the south of the Maldives with 255 islands.


== Formation ==

In 1842, Charles Darwin explained the creation of coral atolls in the southern Pacific Ocean based upon observations made during a five-year voyage aboard the HMS Beagle from 1831 to 1836. Accepted as basically correct, his explanation involved considering that several tropical island typesfrom high volcanic island, through barrier reef island, to atollrepresented a sequence of gradual subsidence of what started as an oceanic volcano. He reasoned that a fringing coral reef surrounding a volcanic island in the tropical sea will grow upwards as the island subsides (sinks), becoming an "almost atoll", or barrier reef island, as typified by an island such as Aitutaki in the Cook Islands, Bora Bora and others in the Society Islands. The fringing reef becomes a barrier reef for the reason that the outer part of the reef maintains itself near sea level through biotic growth, while the inner part of the reef falls behind, becoming a lagoon because conditions are less favorable for the coral and calcareous algae responsible for most reef growth. In time, subsidence carries the old volcano below the ocean surface and the barrier reef remains. At this point, the island has become an atoll.
Atolls are the product of the growth of tropical marine organisms, and so these islands are only found in warm tropical waters. Volcanic islands located beyond the warm water temperature requirements of hermatypic (reef-building) organisms become seamounts as they subside and are eroded away at the surface. An island that is located where the ocean water temperatures are just sufficiently warm for upward reef growth to keep pace with the rate of subsidence is said to be at the Darwin Point. Islands in colder, more polar regions evolve towards seamounts or guyots; warmer, more equatorial islands evolve towards atolls, for example Kure Atoll.

Reginald Aldworth Daly offered a somewhat different explanation for atoll formation: islands worn away by erosion, by ocean waves and streams, during the last glacial stand of the sea of some 900 feet (270 m) below present sea level developed as coral islands (atolls), or barrier reefs on a platform surrounding a volcanic island not completely worn away, as sea level gradually rose from melting of the glaciers. Discovery of the great depth of the volcanic remnant beneath many atolls such as at Midway Atoll favors the Darwin explanation, although there can be little doubt that fluctuating sea level has had considerable influence on atolls and other reefs.
Coral atolls are also an important place where dolomitization of calcite occurs. At certain depths water is undersaturated in calcium carbonate but saturated in dolomite. Convection created by tides and sea currents enhance this change. Hydrothermal currents created by volcanoes under the atoll may also play an important role.


== Investigation by the Royal Society of London into the formation of coral reefs ==
In 1896, 1897 and 1898, the Royal Society of London carried out drilling on Funafuti atoll in Tuvalu for the purpose of investigating the formation of coral reefs to determine whether traces of shallow water organisms could be found at depth in the coral of Pacific atolls. This investigation followed the work on the structure and distribution of coral reefs conducted by Charles Darwin in the Pacific.
The first expedition in 1896 was led by Professor William Johnson Sollas of the University of Oxford. The geologists included Walter George Woolnough and Edgeworth David of the University of Sydney. Professor Edgeworth David led the expedition in 1897. The third expedition in 1898 was led by Alfred Edmund Finckh.


== United States national monuments ==

On January 6, 2009, U.S. President George W. Bush announced that several remote Pacific islands under U.S. jurisdiction were now national monuments, protecting coral reefs.


== See also ==

Low island


== References ==
^ pronunciation in old video on youtube, Bikini Atoll video
^ Dictionary.com
^ Blake, Gerald Henry, ed. (1994). [Google books World Boundary Series] . 5 Maritime Boundaries. Routledge. ISBN 978-0-415-08835-0. Retrieved 12 February 2013. 
^ Piotr Migon (2010). Geomorphological Landscapes of the World. Springer. p. 349. ISBN 978-90-481-3055-9. Retrieved 12 February 2013. 
^ Erickson, Jon (2003), Marine Geology, Facts On File, pp. 1268, ISBN 0-8160-4874-6 
^ Darwin, Charles R (1842). [Darwin Online The structure and distribution of coral reefs. Being the first part of the geology of the voyage of the Beagle, under the command of Capt. Fitzroy, R.N. during the years 1832 to 1836] . London: Smith Elder and Co. 
^ McNeil (1954, p. 396).
^ Fairbridge (1950, p. 341).
^ "Archipilago de Los Roques" (in Spanish). Caracas, Venezuela: Instituto Nacional de Parques (INPARQUES). 2007. Retrieved 27 February 2013.  
^ Atoll Area, Depth and Rainfall (2001) spreadsheet from The Geological Society of America. Retrieved d.d. October 1, 2009.
^ Lansdowne Bank "21 000"  Meine Bibliothek  Google Bcher
^ http://conserveonline.org/workspaces/pacific.island.countries.publications/fsm/fsmblueprint
^ Misinformation about Islands
^ David, Cara (Caroline Martha) (1899). Funafuti or Three Months On A Coral Atoll: an unscientific account of a scientific expedition. London: John Murray. ASIN B00A1YMOFO. ISBN 978-1151256164. 
^ Finckh, Dr. Alfred Edmund (11 September 1934). "TO THE EDITOR OF THE HERALD.". The Sydney Morning Herald (NSW: National Library of Australia). p. 6. Retrieved 20 June 2012. 
^ Cantrell, Carol (1996). "Finckh, Alfred Edmund (18661961)". Australian Dictionary of Biography, National Centre of Biography, Australian National University. Retrieved 23 December 2012.  
^ Rodgers, K A; Cantrell, Carol. "Alfred Edmund Finckh 18661961: Leader of the 1898 Coral Reef Boring Expedition to Funafuti" (PDF). Historical Records of Australian Science. pp. 393403. doi:10.1071/HR9890740393. Retrieved 23 December 2012. 
^ "Presidential Proclamation 8336" (PDF). The White House. 6 January 2009. 
^ "Weekly Compilation of Presidential Documents" (PDF). 12 January 2009. 

Dobbs, David. 2005. Reef Madness : Charles Darwin, Alexander Agassiz, and the Meaning of Coral. Pantheon. ISBN 0-375-42161-0
Fairbridge, R. W. 1950. Recent and Pleistocene coral reefs of Australia. J. Geol., 58(4): 330401.
McNeil, F. S. 1954. Organic reefs and banks and associated detrital sediments. Amer. J. Sci., 252(7): 385401.


== External links ==
Nukuoro Atoll Nukuoro
Kapingamarangi Atoll Kapingamarangi
Formation of Bermuda reefs
Darwin's Volcano  a short video discussing Darwin and Agassiz' coral reef formation debate