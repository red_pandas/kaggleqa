Tropical cyclogenesis is the development and strengthening of a tropical cyclone in the atmosphere. The mechanisms through which tropical cyclogenesis occurs are distinctly different from those through which mid-latitude cyclogenesis occurs. Tropical cyclogenesis involves the development of a warm-core cyclone, due to significant convection in a favorable atmospheric environment. There are six main requirements for tropical cyclogenesis: sufficiently warm sea surface temperatures, atmospheric instability, high humidity in the lower to middle levels of the troposphere, enough Coriolis force to develop a low pressure center, a preexisting low level focus or disturbance, and low vertical wind shear.
Tropical cyclones tend to develop during the summer, but have been noted in nearly every month in most basins. Climate cycles such as ENSO and the MaddenJulian oscillation modulate the timing and frequency of tropical cyclone development. There is a limit on tropical cyclone intensity which is strongly related to the water temperatures along its path. An average of 86 tropical cyclones of tropical storm intensity form annually worldwide. Of those, 47 reach hurricane/typhoon strength, and 20 become intense tropical cyclones (at least Category 3 intensity on the SaffirSimpson Hurricane Scale).


== Requirements for tropical cyclone formation ==

There are six main requirements for tropical cyclogenesis: sufficiently warm sea surface temperatures, atmospheric instability, high humidity in the lower to middle levels of the troposphere, enough Coriolis force to sustain a low pressure center, a preexisting low level focus or disturbance, and low vertical wind shear. While these conditions are necessary for tropical cyclone formation, they do not guarantee that a tropical cyclone will form.


=== Warm waters, instability, and mid-level moisture ===

Normally, an ocean temperature of 26.5 C (79.7 F) spanning through at least a 50-metre depth is considered the minimum to maintain the special mesocyclone that is the tropical cyclone. These warm waters are needed to maintain the warm core that fuels tropical systems. This value is well above 16.1 C (60.9 F), the global average surface temperature of the oceans. However, this requirement can be considered only a general baseline because it assumes that the ambient atmospheric environment surrounding an area of disturbed weather presents average conditions.
Tropical cyclones are known to form even when normal conditions are not met. For example, cooler air temperatures at a higher altitude (e.g., at the 500 hPa level, or 5.9 km) can lead to tropical cyclogenesis at lower water temperatures, as a certain lapse rate is required to force the atmosphere to be unstable enough for convection. In a moist atmosphere, this lapse rate is 6.5 C/km, while in an atmosphere with less than 100% relative humidity, the required lapse rate is 9.8 C/km.
At the 500 hPa level, the air temperature averages 7 C (18 F) within the tropics, but air in the tropics is normally dry at this level, giving the air room to wet-bulb, or cool as it moistens, to a more favorable temperature that can then support convection. A wetbulb temperature at 500 hPa in a tropical atmosphere of 13.2 C is required to initiate convection if the water temperature is 26.5 C, and this temperature requirement increases or decreases proportionally by 1 C in the sea surface temperature for each 1 C change at 500 hpa. Under a cold cyclone, 500 hPa temperatures can fall as low as 30 C, which can initiate convection even in the driest atmospheres. This also explains why moisture in the mid-levels of the troposphere, roughly at the 500 hPa level, is normally a requirement for development. However, when dry air is found at the same height, temperatures at 500 hPa need to be even colder as dry atmospheres require a greater lapse rate for instability than moist atmospheres. At heights near the tropopause, the 30-year average temperature (as measured in the period encompassing 1961 through 1990) was 77 C (132 F). A recent example of a tropical cyclone that maintained itself over cooler waters was Epsilon of the 2005 Atlantic hurricane season.


==== Role of Maximum Potential Intensity (MPI) ====
Kerry Emanuel created a mathematical model around 1988 to compute the upper limit of tropical cyclone intensity based on sea surface temperature and atmospheric profiles from the latest global model runs. Emanuel's model is called the maximum potential intensity, or MPI. Maps created from this equation show regions where tropical storm and hurricane formation is possible, based upon the thermodynamics of the atmosphere at the time of the last model run (either 0000 or 1200 UTC). This does not take into account vertical wind shear.


=== Coriolis force ===

A minimum distance of 500 km (310 mi) from the equator is normally needed for tropical cyclogenesis. The Coriolis force imparts rotation on the flow and arises as winds begin to flow in toward the lower pressure created by the pre-existing disturbance. In areas with a very small or non-existent Coriolis force (e.g. near the Equator), the only significant atmospheric forces in play are the pressure gradient force (the pressure difference that causes winds to blow from high to low pressure) and a smaller friction force; these two alone would not cause the large-scale rotation required for tropical cyclogenesis. The existence of a significant Coriolis force allows the developing vortex to achieve gradient wind balance. This is a balance condition found in mature tropical cyclones that allows latent heat to concentrate near the storm core; this results in the maintenance or intensification of the vortex if other development factors are neutral.


=== Low level disturbance ===
Whether it be a depression in the intertropical covergence zone (ITCZ), a tropical wave, a broad surface front, or an outflow boundary, a low level feature with sufficient vorticity and convergence is required to begin tropical cyclogenesis. Even with perfect upper level conditions and the required atmospheric instability, the lack of a surface focus will prevent the development of organized convection and a surface low. Tropical cyclones can form when smaller circulations within the Intertropical Convergence Zone merge.


=== Weak vertical wind shear ===

Vertical wind shear of less than 10 m/s (20 kt, 22 mph) between the surface and the tropopause is favored for tropical cyclone development. A weaker vertical shear makes the storm grow faster vertically into the air, which helps the storm develop and become stronger. If the vertical shear is too strong, the storm cannot rise to its full potential and its energy becomes spread out over too large of an area for the storm to strengthen. Strong wind shear can "blow" the tropical cyclone apart, as it displaces the mid-level warm core from the surface circulation and dries out the mid-levels of the troposphere, halting development. In smaller systems, the development of a significant mesoscale convective complex in a sheared environment can send out a large enough outflow boundary to destroy the surface cyclone. Moderate wind shear can lead to the initial development of the convective complex and surface low similar to the mid-latitudes, but it must relax to allow tropical cyclogenesis to continue.


==== Favorable trough interactions ====
Limited vertical wind shear can be positive for tropical cyclone formation. When an upper-level trough or upper-level low is roughly the same scale as the tropical disturbance, the system can be steered by the upper level system into an area with better diffluence aloft, which can cause further development. Weaker upper cyclones are better candidates for a favorable interaction. There is evidence that weakly sheared tropical cyclones initially develop more rapidly than non-sheared tropical cyclones, although this comes at the cost of a peak in intensity with much weaker wind speeds and higher minimum pressure. This process is also known as baroclinic initiation of a tropical cyclone. Trailing upper cyclones and upper troughs can cause additional outflow channels and aid in the intensification process. It should be noted that developing tropical disturbances can help create or deepen upper troughs or upper lows in their wake due to the outflow jet emanating from the developing tropical disturbance/cyclone.
There are cases where large, mid-latitude troughs can help with tropical cyclogenesis when an upper-level jet stream passes to the northwest of the developing system, which will aid divergence aloft and inflow at the surface, spinning up the cyclone. This type of interaction is more often associated with disturbances already in the process of recurvature.


== Times of formation ==

Worldwide, tropical cyclone activity peaks in late summer when water temperatures are warmest. Each basin, however, has its own seasonal patterns. On a worldwide scale, May is the least active month, while September is the most active.
In the North Atlantic, a distinct hurricane season occurs from June 1 through November 30, sharply peaking from late August through October. The statistical peak of the North Atlantic hurricane season is September 10. The Northeast Pacific has a broader period of activity, but in a similar time frame to the Atlantic. The Northwest Pacific sees tropical cyclones year-round, with a minimum in February and a peak in early September. In the North Indian basin, storms are most common from April to December, with peaks in May and November.
In the Southern Hemisphere, tropical cyclone activity generally begins in early November and generally ends on April 30. Southern Hemisphere activity peaks in mid-February to early March. Virtually all the Southern Hemisphere activity is seen from the southern African coast eastward, toward South America. Tropical cyclones are rare events across the south Atlantic ocean and the southeastern Pacific ocean.


== Unusual areas of formation ==


=== Middle latitudes ===
Areas farther than 30 degrees from the equator (except in the vicinity of a warm current) are not normally conducive to tropical cyclone formation or strengthening, and areas more than 40 degrees from the equator are often very hostile to such development. The primary limiting factor is water temperatures, although higher shear at increasing latitudes is also a factor. These areas are sometimes frequented by cyclones moving poleward from tropical latitudes. On rare occasions, such as in 2004, 1988, and 1975, storms may form or strengthen in this region. Typically, tropical cyclones will undergo extratropical transition after recurving polewards, and typically become fully extratropical after reaching 4550 of latitude. The majority of extratropical cyclones tend to restrengthen after completing the transition period.


=== Near the Equator ===

Areas within approximately ten degrees latitude of the equator do not experience a significant Coriolis Force, a vital ingredient in tropical cyclone formation. However, recently a few tropical cyclones have been observed forming within five degrees of the equator.


=== South Atlantic ===

A combination of wind shear and a lack of tropical disturbances from the Intertropical Convergence Zone (ITCZ) makes it very difficult for the South Atlantic to support tropical activity. Over four tropical cyclones have been observed here such as a weak tropical storm in 1991 off the coast of Africa near Angola, Cyclone Catarina (sometimes also referred to as Aldona), which made landfall in Brazil in 2004 at Category 2 strength, and a smaller storm in January 2004, east of Salvador, Brazil. The January storm is thought to have reached tropical storm intensity based on scatterometer wind measurements.


=== Mediterranean and Black Seas ===

Storms that appear similar to tropical cyclones in structure sometimes occur in the Mediterranean basin. Examples of these "Mediterranean tropical cyclones" formed in September 1947, September 1969, September 1973, August 1976, January 1982, September 1983, December 1984, December 1985, October 1994, January 1995, October 1996, September 1997, December 2005, September 2006, and November 2011. However, there is debate on whether these storms were tropical in nature.
The Black Sea has, on occasion, produced or fueled storms that begin cyclonic rotation, and that appear to be similar to cyclones seen in the Mediterranean.


=== Elsewhere ===
Tropical cyclogenesis is rare in the southeastern Pacific Ocean due to the cold sea-surface temperatures generated by the Humboldt Current and to unfavourable wind shear; there are no records of a tropical cyclone hitting western South America. But in mid-2015, a rare subtropical cyclone was identified in early May relatively close to Chile. This system was unofficially dubbed Katie by researchers.
Vortices have been reported off the coast of Morocco in the past. However, it is debatable if they are truly tropical in character.
Tropical activity is also extremely rare in the Great Lakes. However, a storm system that appeared similar to a subtropical or tropical cyclone formed in 1996 on Lake Huron. It formed an eye-like structure in its center, and it may have briefly been a subtropical or tropical cyclone.


== Influence of large-scale climate cycles ==


=== Influence of ENSO ===

Warm waters during the El Nio-Southern Oscillation lower the potential of tropical cyclone formation primarily in the Atlantic Basin and around Australia. Because tropical cyclones in the northeastern Pacific and north Atlantic basins are both generated in large part by tropical waves from the same wave train, decreased tropical cyclone activity in the north Atlantic translates to increased tropical cyclone activity in the Eastern North Pacific.
In the Northwestern Pacific, El Nio shifts the formation of tropical cyclones eastward. During El Nio episodes, tropical cyclones tend to form in the eastern part of the basin, between 150E and the International Date Line (IDL). Coupled with an increase in activity in the North-Central Pacific (IDL to 140W) and the South-Central Pacific (east of 160E), there is a net increase in tropical cyclone development near the International Date Line on both sides of the equator. While there is no linear relationship between the strength of an El Nio and tropical cyclone formation in the Northwestern Pacific, typhoons forming during El Nio years tend to have a longer duration and higher intensities. Tropical cyclogenesis in the Northwestern Pacific is suppressed west of 150E in the year following an El Nio event.


=== Influence of the MJO ===

In general, westerly wind increases associated with the MaddenJulian oscillation lead to increased tropical cyclogenesis in all basins. As the oscillation propagates from west to east, it leads to an eastward march in tropical cyclogenesis with time during that hemisphere's summer season. There is an inverse relationship between tropical cyclone activity in the western Pacific basin and the north Atlantic basin, however. When one basin is active, the other is normally quiet, and vice versa. The main reason for this appears to be the phase of the MaddenJulian oscillation, or MJO, which is normally in opposite modes between the two basins at any given time.


=== Influence of equatorial Rossby waves ===

Research has shown that trapped equatorial Rossby wave packets can increase the likelihood of tropical cyclogenesis in the Pacific Ocean, as they increase the low-level westerly winds within that region, which then leads to greater low-level vorticity. The individual waves can move at approximately 1.8 m/s (4 mph) each, though the group tends to remain stationary.


== Seasonal forecasts ==
Since 1984, Colorado State University has been issuing seasonal tropical cyclone forecasts for the north Atlantic basin, with results that are better than climatology. The university has found several statistical relationships for this basin that appear to allow long range prediction of the number of tropical cyclones. Since then, numerous others have followed in the university's steps, with some organizations issuing seasonal forecasts for the northwest Pacific and the Australian region. The predictors are related to regional oscillations in the global climate system: the Walker circulation which is related to the El Nio-Southern Oscillation; the North Atlantic oscillation or NAO; the Arctic oscillation or AO; and the Pacific North American pattern or PNA.


== See also ==
Tropical cyclone forecasting
Rossby wave
Tropical cyclone
Tropical wave
Intertropical convergence zone
Monsoon trough
Invest (meteorology)


== References ==


== External links ==
Current AO conditions
Current ENSO conditions
Current MJO conditions
Current NAO conditions
Current PNA conditions
Maximum Potential Intensity
Maximum Potential Intensity Maps Worldwide
Tropical Cyclone Heat Potential