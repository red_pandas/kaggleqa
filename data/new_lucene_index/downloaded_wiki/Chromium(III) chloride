Chromium(III) chloride (also called chromic chloride) describes any of several compounds of with the formula CrCl3(H2O)x, where x can be 0, 5, and 6. The anhydrous compound with the formula CrCl3 is a violet solid. The most common form of the trichloride is the dark green "hexahydrate", CrCl3.6H2O. Chromium chloride finds uses as catalysts and as precursors to dyes for wool.


== Structure ==
Anhydrous chromium(III) chloride adopts the YCl3 structure, with Cr3+ occupying two thirds of the octahedral interstices in alternating layers of a pseudo-cubic close packed lattice of Cl ions. The absence of cations in alternate layers leads to weak bonding between adjacent layers. For this reason, crystals of CrCl3 cleave easily along the planes between layers, which results in the flaky (micaceous) appearance of samples of chromium(III) chloride.


=== Chromium(III) chloride hydrates ===
Chromium(III) chlorides display the somewhat unusual property of existing in a number of distinct chemical forms (isomers), which differ in terms of the number of chloride anions that are coordinated to Cr(III) and the water of crystallization. The different forms exist both as solids, and in aqueous solutions. Several members are known of the series of [CrCl3n(H2O)n]z+. The main hexahydrate can be more precisely described as [CrCl2(H2O)4]Cl2H2O. It consists of the cation trans-[CrCl2(H2O)4]+ and additional molecules of water and a chloride anion in the lattice. Two other hydrates are known, pale green [CrCl(H2O)5]Cl2H2O and violet [Cr(H2O)6]Cl3. Similar behaviour occurs with other chromium(III) compounds.


== Preparation ==
Anhydrous chromium(III) chloride may be prepared by chlorination of chromium metal directly, or indirectly by carbothermic chlorination of chromium(III) oxide at 650800 C
Cr2O3 + 3 C + 3 Cl2  2 CrCl3 + 3 CO
It may also be prepared by treating the hexahydrate with thionyl chloride:
CrCl36H2O + 6 SOCl2  CrCl3 + 6 SO2 + 12 HCl
The hydrated chlorides are prepared by treatment of chromate with hydrochloric acid and methanol. In laboratory the hydrates are usually prepared by dissolving the chromium metal or chromium(III) oxide in hydrochloric acid.


== Reactions ==
Slow reaction rates are common with chromium(III) complexes. The low reactivity of the d3 Cr3+ ion can be explained using crystal field theory. One way of opening CrCl3 up to substitution in solution is to reduce even a trace amount to CrCl2, for example using zinc in hydrochloric acid. This chromium(II) compound undergoes substitution easily, and it can exchange electrons with CrCl3 via a chloride bridge, allowing all of the CrCl3 to react quickly.
With the presence of some chromium(II), however, solid CrCl3 dissolves rapidly in water. Similarly, ligand substitution reactions of solutions of [CrCl2(H2O)4]+ are accelerated by chromium(II) catalysts.
With molten alkali metal chlorides such as potassium chloride, CrCl3 gives salts of the type M3CrCl6 and K3Cr2Cl9, which is also octahedral but where the two chromiums are linked via three chloride bridges.


=== Complexes with organic ligands ===
CrCl3 is a Lewis acid, classified as "hard" according to the Hard-Soft Acid-Base theory. It forms a variety of adducts of the type [CrCl3L3]z, where L is a Lewis base. For example, it reacts with pyridine (C
5H
5N) to form an adduct:
CrCl3 + 3 C5H5N  CrCl3(C5H5N)3
Treatment with trimethylsilylchloride in THF gives the anhydrous THF complex:
CrCl3.(H2O)6 + 12 (CH3)3SiCl + 3 THF  CrCl3(THF)3 + 6 ((CH3)3Si)2O + 12 HCl


=== Precursor to organochromium complexes ===
Chromium(III) chloride is used as the precursor to many organochromium compounds, for example bis(benzene)chromium, an analogue of ferrocene:

Phosphine complexes derived from CrCl3 catalyse the trimerization of ethylene to 1-hexene.


=== Use in organic synthesis ===
One niche use of CrCl3 in organic synthesis is for the in situ preparation of chromium(II) chloride, a reagent for the reduction of alkyl halides and for the synthesis of (E)-alkenyl halides. The reaction is usually performed using two moles of CrCl3 per mole of lithium aluminium hydride, although if aqueous acidic conditions are appropriate zinc and hydrochloric acid may be sufficient.

Chromium(III) chloride has also been used as a Lewis acid in organic reactions, for example to catalyse the nitroso Diels-Alder reaction.


=== Dyestuffs ===
A number of chromium-containing dyes are used commercially for wool. Typical dyes are triarylmethanes consisting of ortho-hydroxylbenzoic acid derivatives.


== Precautions ==
Although trivalent chromium is far less poisonous than hexavalent, chromium salts are generally considered toxic.


== References ==


== Further reading ==
Handbook of Chemistry and Physics, 71st edition, CRC Press, Ann Arbor, Michigan, 1990.
The Merck Index, 7th edition, Merck & Co, Rahway, New Jersey, USA, 1960.
J. March, Advanced Organic Chemistry, 4th ed., p. 723, Wiley, New York, 1992.
K. Takai, in Handbook of Reagents for Organic Synthesis, Volume 1: Reagents, Auxiliaries and Catalysts for C-C Bond Formation, (R. M. Coates, S. E. Denmark, eds.), pp. 206211, Wiley, New York, 1999.


== External links ==
International Chemical Safety Card 1316 (anhydr. CrCl3)
International Chemical Safety Card 1532 (CrCl36H2O)
National Pollutant Inventory  Chromium (III) compounds fact sheet
NIOSH Pocket Guide to Chemical Hazards
IARC Monograph "Chromium and Chromium compounds"