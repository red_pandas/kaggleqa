A complex system is a system that exhibits some (and possibly all) of the following characteristics:
feedback loops;
some degree of spontaneous order;
robustness of the order;
emergent organization;
numerosity;
hierarchical organization.
A complex system can be also viewed as a system composed on many components which may interact with each other. In many cases it is useful to represent such a system as a network where the nodes represent the components and the links their interactions.
Examples of complex systems are Earth's global climate, the human brain, social organization, an ecosystem, a living cell, and ultimately the entire universe.


== History ==
Although it is arguable that humans have been studying complex systems for thousands of years, the modern scientific study of complex systems is relatively young in comparison to conventional fields of science with simple system assumptions, such as physics and chemistry. The history of the scientific study of these systems follows several different research trends.
In the area of mathematics, arguably the largest contribution to the study of complex systems was the discovery of chaos in deterministic systems, a feature of certain dynamical systems that is strongly related to nonlinearity. The study of neural networks was also integral in advancing the mathematics needed to study complex systems.
The notion of self-organizing systems is tied up to work in nonequilibrium thermodynamics, including that pioneered by chemist and Nobel laureate Ilya Prigogine in his study of dissipative structures.


== Types of complex systems ==


=== Nonlinear systems ===
The behaviour of non-linear systems is not subject to the principle of superposition while that of linear systems is subject to superposition. Thus, a complex nonlinear system is one whose behaviour cannot be expressed as a sum of the behaviour of its parts (or of their multiples).


==== Chaotic systems ====
For a dynamical system to be classified as chaotic, it must have the following properties:

it must be sensitive to initial conditions,
it must be topologically mixing, and
its periodic orbits must be dense.
Sensitivity to initial conditions means that each point in such a system is arbitrarily closely approximated by other points with significantly different future trajectories. Thus, an arbitrarily small perturbation of the current trajectory may lead to significantly different future behavior.


=== Complex adaptive systems ===
Complex adaptive systems (CAS) are special cases of complex systems. They are complex in that they are diverse and made up of multiple interconnected elements and adaptive in that they have the capacity to change and learn from experience. Examples of complex adaptive systems include the stock market, social insect and ant colonies, the biosphere and the ecosystem, the brain and the immune system, the cell and the developing embryo, manufacturing businesses and any human social group-based endeavor in a cultural and social system such as political parties or communities. This includes some large-scale online systems, such as collaborative tagging or social bookmarking systems.


== Topics on complex systems ==


=== Features of complex systems ===
Complex systems may have the following features:
Cascading Failures
Due to the strong coupling between components in complex systems, a failure in one or more components can lead to cascading failures which may have catastrophic consequences on the functioning of the system.
Complex systems may be open
Complex systems are usually open systems  that is, they exist in a thermodynamic gradient and dissipate energy. In other words, complex systems are frequently far from energetic equilibrium: but despite this flux, there may be pattern stability, see synergetics.
Complex systems may have a memory
The history of a complex system may be important. Because complex systems are dynamical systems they change over time, and prior states may have an influence on present states. More formally, complex systems often exhibit hysteresis.
Complex systems may be nested
The components of a complex system may themselves be complex systems. For example, an economy is made up of organisations, which are made up of people, which are made up of cells - all of which are complex systems.
Dynamic network of multiplicity
As well as coupling rules, the dynamic network of a complex system is important. Small-world or scale-free networks which have many local interactions and a smaller number of inter-area connections are often employed. Natural complex systems often exhibit such topologies. In the human cortex for example, we see dense local connectivity and a few very long axon projections between regions inside the cortex and to other brain regions.Recovery and failures of complex systems. A complex system can evolve into spontaneous recovery and failure and have hysteresis behavior.
May produce emergent phenomena
Complex systems may exhibit behaviors that are emergent, which is to say that while the results may be sufficiently determined by the activity of the systems' basic constituents, they may have properties that can only be studied at a higher level. For example, the termites in a mound have physiology, biochemistry and biological development that are at one level of analysis, but their social behavior and mound building is a property that emerges from the collection of termites and needs to be analysed at a different level.
Relationships are non-linear
In practical terms, this means a small perturbation may cause a large effect (see butterfly effect), a proportional effect, or even no effect at all. In linear systems, effect is always directly proportional to cause. See nonlinearity.
Relationships contain feedback loops
Both negative (damping) and positive (amplifying) feedback are always found in complex systems. The effects of an element's behaviour are fed back to in such a way that the element itself is altered.


== See also ==


== References ==


== Further reading ==
Paolo Sibani & Henrik Jeldtoft Jensen (2013). Stochastic Dynamics of Complex Systems, ISBN 978-1-84816-993-7, World Scientific and Imperial College Press.
Chu, Dominique (2011). Complexity: Against Systems. Theory in Biosciences, Springer Verlag. [1]
Rocha, Luis M. (1999). "Complex Systems Modeling: Using Metaphors From Nature in Simulation and Scientific Models". BITS: Computer and Communications News. Computing, Information, and Communications Division. Los Alamos National Laboratory. November 1999
Ignazio Licata & Ammar Sakaji (eds) (2008). Physics of Emergence and Organization, ISBN 978-981-277-994-6, World Scientific and Imperial College Press.
Alfred Hbler, Cory Stephenson, Dave Lyon, Ryan Swindeman (2011). Fabrication and programming of large physically evolving networks Complexity, 16(5), pp. 78
De Toni, Alberto and Comello, Luca (2011). Journey into Complexity. Udine: Lulu. ISBN 978-1-4452-6078-5. 


== External links ==
Introduction to complex systems-short course by Shlomo Havlin
Complex systems in scholarpedia.
(European) Complex Systems Society
(Australian) Complex systems research network.
Complex Systems Modeling based on Luis M. Rocha, 1999.
CRM Complex systems research group
The Center for Complex Systems Research, Univ. of Illinois at Urbana-Champaign
FuturICT - Exploring and Managing our Future