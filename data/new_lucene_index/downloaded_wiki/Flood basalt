A flood basalt is the result of a giant volcanic eruption or series of eruptions that coats large stretches of land or the ocean floor with basalt lava. Flood basalt provinces such as the Deccan Traps of India are often called traps, which derives from the characteristic stairstep geomorphology of many associated landscapes. Eleven distinct flood basalt episodes occurred in the past 250 million years, resulting in large volcanic provinces, creating plateaus and mountain ranges on Earth. Large igneous provinces have been connected to five mass extinction events, and may be associated with bolide impacts.


== Formation ==
The formation and effects of a flood basalt depend on a range of factors, such as continental configuration, latitude, volume, rate, duration of eruption, style and setting (continental vs. oceanic), the preexisting climate state, and the biota resilience to change.
One proposed explanation for flood basalts is that they are caused by the combination of continental rifting and its associated decompression melting, in conjunction with a mantle plume also undergoing decompression melting, producing vast quantities of a tholeiitic basaltic magma. These have a very low viscosity, which is why they 'flood' rather than form taller volcanoes. Another explanation is that they result from the release, over a short time period, of melt that has accumulated in the mantle over a long time period.
The Deccan Traps of central India, the Siberian Traps, and the Columbia River Plateau of western North America are three regions covered by prehistoric flood basalts. The Mesoproterozoic Mackenzie Large Igneous Province in Canada contains the Coppermine River flood basalts related to the Muskox layered intrusion. The maria on the Moon are additional, even more extensive, flood basalts. Flood basalts on the ocean floor produce oceanic plateaus.
The surface covered by one eruption can vary from around 200,000 km (Karoo) to 1,500,000 km (Siberian Traps). The thickness can vary from 2000 metres (Deccan Traps) to 12,000 m (Lake Superior). These are smaller than the original volumes due to erosion.


== Petrography ==

Flood basalts have tholeiite and olivine compositions (according to the classification of Yoder and Tilley). The composition of the basalts from the Paran is fairly typical of that of flood basalts; it contains phenocrysts occupying around 25% of the volume of rock, trapped in volcanic glass. These phenocrysts are pyroxenes (augite and pigeonite), plagioclases, opaque crystals such as titanium rich magnetite or ilmenite, and occasionally some olivine. Sometimes more differentiated volcanic products such as andesites, dacites and rhyodacites have been observed, but only in small quantities at the top of former magma chambers.


== Structures ==
Subaerial flood basalts can be of two kinds :
with a smooth or twisted surface : very compact surface; vesicles (gas bubbles) are rare. Degassing was easy (magma maintained at a high temperature and more fluid in a chamber of a size such that confining pressures did not confine gases to the melt before expulsion). Such lava flows may form underground rivers; when degassing fractures and conduits are present, very large flows may reach the surface.
with a chaotic surface : the basalt flood is very rich in bubbles of gas, with an irregular, fragmental surface. Degassing was difficult (less fluid magma expelled from a rift with no chance of progressive expansion in a hot chamber; the degassing took place closer to the surface where the flow forms a crust which cracks under the pressure of the gases in the flow itself and during more rapid cooling).
In the Massif Central in Auvergne, France, there is a good example of chaotic lava flow, produced by eruptions from Puy de la Vache and Puy de Lassolas.


== Geochemistry ==

Geochemical analysis of the major oxides reveals a composition close to that of mid-ocean ridge basalts (MORB) but also close to that of ocean island basalts (OIB). These are in fact tholeiites with a silicon dioxide percentage close to 50%.
Two kinds of basaltic flood basalts can be distinguished :
those poor in P2O5 and in TiO2, called low phosphorus and titanium
those rich in P2O5 and in TiO2, called high phosphorus and titanium
The isotopic ratios 87Sr/86Sr and 206Pb/204Pb are different from that observed in general, which shows that the basalt flood magma was contaminated as it passed through the continental crust. It is this contamination that explains the difference between the two kinds of basalt mentioned above. The low phosphorus and titanium type has an excess of elements from the crust such as potassium and strontium.
The content in incompatible elements of flood basalts is lower than that of ocean island basalts, but higher than that of mid-ocean ridge basalts.


== Other occurrences ==
Basalt floods on the planet Venus are larger than those on Earth.


== List of flood basalts ==

Representative continental flood basalts (also known as traps) and oceanic plateaus, together forming a listing of large igneous provinces:
Chilcotin Group (south-central British Columbia, Canada)
High Arctic Large Igneous Province
Columbia-Snake River flood basalts (see Columbia River Basalt Group)
Ethiopian and Yemen traps in the Ethiopian Highlands
Viluy traps
Pre-Devonian traps
North Atlantic Volcanic Province
Emeishan Traps (western China)
Deccan Traps (India) 66 million years ago (end of Cretaceous period)
Caribbean large igneous province
Mackenzie Large Igneous Province
Kerguelen Plateau
Ontong JavaManihikiHikurangi Plateau
Paran and Etendeka traps (Brazil-Namibia)
Karoo and Ferrar provinces (South Africa-Antarctica)
Central Atlantic Magmatic Province
Siberian Traps (Russia) 251 million years ago (end of Permian)


== See also ==
Large igneous province
Oceanic plateau
Supervolcano
Volcanic plateau
List of flood basalt provinces


== References ==

White, R.S.; McKenzie, D.P. (1989). "Magmatism at rift zones: The generation of volcanic continental margins and flood basalts". J. Geophys. Res. 94: 76857729. Bibcode:1989JGR....94.7685W. doi:10.1029/jb094ib06p07685.