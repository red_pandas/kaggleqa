The Johnstown Flood (locally, the Great Flood of 1889) occurred on May 31, 1889, after the catastrophic failure of the South Fork Dam on the Little Conemaugh River 14 miles (23 km) upstream of the town of Johnstown, Pennsylvania. The dam broke after several days of extremely heavy rainfall, unleashing 20 million tons of water (18 million cubic meters) from the reservoir known as Lake Conemaugh. With a volumetric flow rate that temporarily equalled that of the Mississippi River, the flood killed 2,209 people and caused US$17 million of damage (about $450 million in 2015 dollars).
The American Red Cross, led by Clara Barton and with 50 volunteers, undertook a major disaster relief effort. Support for victims came from all over the United States and 18 foreign countries. After the flood, survivors suffered a series of legal defeats in their attempts to recover damages from the dam's owners. Public indignation at that failure prompted the development in American law changing a fault-based regime to strict liability.


== HistoryEdit ==
The village of Johnstown was founded in 1800 by the Swiss immigrant Joseph Johns (anglicized from "Schantz") where the Stony Creek and Little Conemaugh rivers joined to form the Conemaugh River. It began to prosper with the building of the Pennsylvania Main Line Canal in 1836 and the construction in the 1850s of the Pennsylvania Railroad and the Cambria Iron Works. By 1889, Johnstown's industries had attracted numerous Welsh and German immigrants. With a population of 30,000, it was a growing industrial community known for the quality of its steel.
The high, steep hills of the narrow Conemaugh Valley and the Allegheny Mountains range to the east kept development close to the riverfront areas. The valley had large amounts of runoff from rain and snowfall. The area surrounding Johnstown is prone to flooding due to its location on the rivers, whose upstream watersheds include an extensive drainage basin of the Allegheny plateau. Adding to these factors, slag from the iron furnaces of the steel mills was dumped along the river to create more land for building. Developers' artificial narrowing of the riverbed to maximize early industries left the city even more flood-prone. The Conemaugh River immediately downstream of Johnstown is hemmed in by steep mountainsides for about 10 miles (16 km). Today, a plaque at the scenic overlook on Pennsylvania Route 56 about 4 miles (6 km) outside Johnstown cites this gorge as the deepest river gap in the United States east of the Rocky Mountains.


=== South Fork Dam and Lake ConemaughEdit ===

High above the city, the Commonwealth of Pennsylvania built the South Fork Dam between 1838 and 1853, as part of a cross-state canal system, the Main Line of Public Works. Johnstown was the eastern terminus of the Western Division Canal, supplied with water by Lake Conemaugh, the reservoir behind the dam. As railroads superseded canal barge transport, the Commonwealth abandoned the canal and sold it to the Pennsylvania Railroad. The dam and lake were part of the purchase, and the railroad sold them to private interests.
Henry Clay Frick led a group of speculators, including Benjamin Ruff, from Pittsburgh to purchase the abandoned reservoir, modify it, and convert it into a private resort lake for their wealthy associates. Many were connected through business and social links to Carnegie Steel. Development included lowering the dam to make its top wide enough to hold a road, and putting a fish screen in the spillway (the screen also trapped debris). These alterations are thought to have increased the vulnerability of the dam. Moreover, a system of relief pipes and valves, a feature of the original dam, previously sold off for scrap, was not replaced, so the club had no way of lowering the water level in the lake in case of an emergency. The members built cottages and a clubhouse to create the South Fork Fishing and Hunting Club, an exclusive and private mountain retreat. Membership grew to include more than 50 wealthy Pittsburgh steel, coal, and railroad industrialists.

Lake Conemaugh at the club's site was 450 feet (140 m) in elevation above Johnstown. The lake was about 2 miles (3.2 km) long, about 1 mile (1.6 km) wide, and 60 feet (18 m) deep near the dam. The lake had a perimeter of 7 miles (11 km) and held 20 million tons of water.
The dam was 72 feet (22 m) high and 931 feet (284 m) long. After 1881, when the club opened, the dam frequently sprang leaks. It was patched, mostly with mud and straw. Additionally, a previous owner had removed and sold for scrap the three cast iron discharge pipes that had allowed a controlled release of water. There had been some speculation as to the dam's integrity, and concerns had been raised by the head of the Cambria Iron Works downstream in Johnstown.


== Events of the floodEdit ==
On May 28, 1889, a storm formed over Nebraska and Kansas and headed east. When the storm struck the Johnstown-South Fork area two days later, it was the worst downpour that had ever been recorded in that part of the country. The U.S. Army Signal Corps estimated that 6 to 10 inches (150 to 250 mm) of rain fell in 24 hours over the region. During the night, small creeks became roaring torrents, ripping out trees and debris. Telegraph lines were downed and rail lines were washed away. Before daybreak, the Conemaugh River that ran through Johnstown was about to overwhelm its banks.

On the morning of May 31, in a farmhouse on a hill just above the South Fork Dam, Elias Unger, president of the South Fork Fishing and Hunting Club, awoke to the sight of Lake Conemaugh swollen after a night-long heavy rainfall. Unger ran outside in the still-pouring rain to assess the situation and saw that the water was nearly cresting the dam. He quickly assembled a group of men to save the face of the dam by trying to unclog the spillway; it was blocked by the broken fish trap and debris caused by the swollen waterline. Other men tried digging another spillway at the other end of the dam to relieve the pressure, without success. Most remained on top of the dam, some plowing earth to raise it, while others tried to pile mud and rock on the face to save the eroding wall.
John Parke, an engineer for the South Fork Club, briefly considered cutting through the dam's end, where the pressure would be less, but decided against it. Twice, under orders from Unger, Parke rode on horseback to the nearby town of South Fork to the telegraph office to send warnings to Johnstown explaining the critical nature of the eroding dam. But the warnings were not passed to the authorities in town, as there had been many false alarms in the past of the South Fork Dam not holding against flooding. Unger, Parke, and the rest of the men continued working until exhausted to save the face of the dam; they abandoned their efforts at around 1:30 p.m., fearing that their efforts were futile and the dam was at risk of imminent collapse. Unger ordered all of his men to fall back to high ground on both sides of the dam where they could do nothing but wait. During the day in Johnstown, the situation worsened as water rose to as high as 10 feet (3.0 m) in the streets, trapping some people in their houses.
At around 3:10 p.m., the South Fork Dam collapsed, freeing the 20 million tons of Lake Conemaugh to cascade down the Little Conemaugh River. It took about 40 minutes for the entire lake to drain of the water. The first town to be hit by the flood was South Fork. The town was on high ground, and most of the people escaped by running up the nearby hills when they saw the dam spill over. Some 20 to 30 houses were destroyed or washed away, and four people were killed.
On its way downstream toward Johnstown, 14 miles away, the crest picked up debris, such as trees, houses, and animals. At the Conemaugh Viaduct, a 78-foot (24 m) high railroad bridge, the flood temporarily was stopped when debris jammed against the stone bridge's arch. But within seven minutes, the viaduct collapsed, allowing the flood to resume its course. Because of this, the surging river gained renewed hydraulic head, resulting in a stronger wave hitting Johnstown than otherwise would have been expected. The small town of Mineral Point, one mile (1.6 km) below the Conemaugh Viaduct, was hit with this renewed force. About 30 families lived on the village's single street. After the flood, only bare rock remained. About 16 people were killed. In 2009, studies showed that the volume of the flood through the narrow valley temporarily equalled the flow of the Mississippi River.

"The deluge released by the dam's collapse carried more than 12,000 cubic meters of debris-filled water each second. Flow rates in the Mississippi River typically vary between 7,000 and 20,000 cubic meters per second."

The village of East Conemaugh was next. One witness on high ground near the town described the water as almost obscured by debris, resembling "a huge hill rolling over and over". From his locomotive, the engineer John Hess heard the rumbling of the approaching flood. Throwing his locomotive into reverse, Hess raced backward toward East Conemaugh, the whistle blowing constantly. His warning saved many people who reached high ground. When the flood hit, it picked up the locomotive and tossed it aside; Hess himself survived, but at least 50 people died, including about 25 passengers stranded on trains in the town.
Before hitting the main part of Johnstown, the flood surge hit the Cambria Iron Works at the town of Woodvale, sweeping up railroad cars and barbed wire in its moil. Of Woodvale's 1,100 residents, 314 died in the flood. Boilers exploded when the flood hit the Gautier Wire Works, causing black smoke seen by the Johnstown residents. Miles of its barbed wire became entangled in the debris in the flood waters.
Some 57 minutes after the South Fork Dam collapsed, the flood hit Johnstown. The residents were caught by surprise as the wall of water and debris bore down, traveling at 40 miles per hour (64 km/h) and reaching a height of 60 feet (18 m) in places. Some, realizing the danger, tried to escape by running towards high ground but most people were hit by the surging floodwater. Many people were crushed by pieces of debris, and others became caught in barbed wire from the wire factory upstream. Those who reached attics, or managed to stay afloat on pieces of floating debris, waited hours for help to arrive.

At Johnstown, the Stone Bridge, which was a substantial arched structure, carried the Pennsylvania Railroad across the Conemaugh River. The debris carried by the flood formed a temporary dam at the bridge, resulting in the flood surge rolling upstream along the Stoney Creek River. Eventually, gravity caused the surge to return to the dam, causing a second wave to hit the city, but from a different direction. Some people who had been washed downstream became trapped in an inferno as the debris piled up against the Stone Bridge caught fire; at least 80 people died there. The fire at the Stone Bridge burned for three days. After floodwaters receded, the pile of debris at the bridge was seen to cover 30 acres (12 ha), and reached 70 feet (21 m) in height. It took workers three months to remove the mass of debris, largely because it was bound by the steel wire from the ironworks. Dynamite was eventually used to clear it. Still standing and in use as a railroad bridge, the Stone Bridge is a landmark associated with survival and recovery from the flood. In 2008, it was restored in a project including new lighting as part of commemorative activities related to the flood.


== AftermathEdit ==


=== Immediately afterwardEdit ===

The total death toll was 2,209, making the disaster the largest loss of civilian life in the United States at the time (perhaps with exception of the Peshtigo Fire). It was later surpassed by fatalities in the 1900 Galveston hurricane and the September 11, 2001 terrorist attacks. Some historians believe the 1928 Okeechobee hurricane and 1906 San Francisco earthquake killed more people in the U.S. than did the Johnstown Flood, but the official death toll was lower.
Ninety-nine entire families died in the flood, including 396 children. One hundred twenty-four women and 198 men were widowed, 98 children were orphaned. One-third of the dead, 777 people, were never identified; their remains were buried in the "Plot of the Unknown" in Grandview Cemetery in Westmont.
It was the worst flood to hit the U.S. in the 19th century. Sixteen hundred homes were destroyed, $17 million in property damage levied, and 4 square miles (10 km2) of downtown Johnstown were completely destroyed. Clean-up operations continued for years. Although Cambria Iron and Steel's facilities were heavily damaged, they returned to full production within a year and a half.
Working seven days and nights, workmen replaced the huge stone railroad viaduct, which had been nearly destroyed by the flood. The Pennsylvania Railroad restored service to Pittsburgh, 55 miles (89 km) away, by June 2. Food, clothing, medicine, and other provisions began arriving by rail. Morticians traveled by railroad. Johnstown's first call for help requested coffins and undertakers. The demolition expert "Dynamite Bill" Flinn and his 900-man crew cleared the wreckage at the Stone Bridge. They carted off debris, distributed food, and erected temporary housing. At its peak, the army of relief workers totaled about 7,000.
One of the first outsiders to arrive was Clara Barton, nurse, founder and president of the American Red Cross. Barton arrived on June 5, 1889, to lead the group's first major disaster relief effort; she did not leave for more than 5 months. She and many other volunteers worked tirelessly. Donations for the relief effort came from all over the United States and overseas. $3,742,818.78 was collected for the Johnstown relief effort from within the U.S. and 18 foreign countries, including Russia, Turkey, France, Great Britain, Australia, and Germany.
Frank Shomo, the last known survivor of the 1889 flood, died March 20, 1997, at the age of 108.


=== Subsequent floodsEdit ===
Floods have continued to be a concern for Johnstown, which had major flooding in 1894, 1907, and 1924. The biggest flood of the first half of the 20th century was the St. Patrick's Day Flood of March 1936. It also reached Pittsburgh, where it was known as the Great Pittsburgh Flood of 1936. Following the 1936 flood, the U.S. Army Corps of Engineers dredged the river within the city and built concrete river walls, creating a channel nearly 20 feet deep. Upon completion, the Corps proclaimed Johnstown "flood free."
The new river walls withstood Hurricane Agnes in 1972, but on the night of July 19, 1977, a severe thunderstorm dropped 11 inches of rain in eight hours on the watershed above the city and the rivers began to rise. By dawn, the city was under water that reached as high as 8 feet (2.4 m). Seven counties were declared a disaster area, suffering $200 million in property damage, and 78 people died. Forty were killed by the Laurel Run Dam failure. Another 50,000 were rendered homeless as a result of this "100-year flood". Markers on a corner of City Hall at 401 Main Street show the height of the crests of the 1889, 1936, and 1977 floods.


=== Court case and recoveryEdit ===
In the years following the disaster, some people blamed the members of the South Fork Fishing and Hunting Club for their modifications to the dam and failure to maintain it properly. The club had bought and redesigned the dam to turn the area into a vacation retreat in the mountains. They were accused of failing to maintain the dam properly, so that it was unable to contain the additional water of the unusually heavy rainfall.
Popular feeling ran high and was best summed up in the concluding lines of Isaac Reed's poem:

The club was successfully defended by the firm of Knox and Reed (now Reed Smith LLP), whose partners Philander Knox and James Hay Reed were both Club members. The Club was never held legally responsible for the disaster. The court held the dam break to have been an Act of God, and granted the survivors no legal compensation.
Individual members of the club, millionaires in their day, contributed to the recovery. Along with about half of the club members, Henry Clay Frick donated thousands of dollars to the relief effort in Johnstown. After the flood, Andrew Carnegie, already known as an industrialist and philanthropist, built the town a new library.


=== Effect on the development of American lawEdit ===
Survivors were unable to recover damages in court because of the club's lack of resources. First, the wealthy club owners had designed the club's financial structure to keep their personal assets separated from it and, secondly, it was difficult for any suit to prove that any particular owner had behaved negligently. Though the former reason was probably more central to the failure of survivors' suits against the club, the latter received coverage and extensive criticism in the national press.
As a result of this criticism, in the 1890s, state courts around the country adopted Rylands v. Fletcher, a British common-law precedent which had formerly been largely ignored in the United States. State courts' adoption of Rylands, which held that a non-negligent defendant could be held liable for damage caused by the unnatural use of land, foreshadowed the legal system's 20th-century acceptance of strict liability.


== Legacy and popular cultureEdit ==
At Point Park in Johnstown, at the confluence of the Stonycreek and Little Conemaugh rivers, an "eternal flame" burns in memory of the flood victims.
The Carnegie Library is now owned by the Johnstown Historical Society, which has adapted it for use as The Flood Museum.
Portions of the Stone Bridge have been made part of the Johnstown Flood National Memorial, established in 1969 and managed by the National Park Service.
The flood has been the subject or setting for numerous histories, novels, and other works as well.


=== Science fictionEdit ===
The Star Trek: The Original Series novel Rough Trails (2006) (third part of the Star Trek: New Earth mini-series) by L.A. Graf recreates the Johnstown Flood set on another planet.
Peg Kehret's fantasy novel, The Flood Disaster, features two students assigned a project on the flood who travel back in time.
Murray Leinster's fantasy novel The Time Tunnel (1967) features two time travelers who were unable to warn the Johnstown population of the coming disaster.
Catherine Marshall's novel Julie, features a teenage girl living in a small Pennsylvania town below an earthen dam in the 1930s; its events parallel the Johnstown Flood.
Paul Mark Tag's science fiction novel Prophecy features the flood.
Donald Keith's science fiction serial Mutiny in the Time Machine was published in Boy's Life magazine beginning in Dec 1962. It involved a Boy Scout troop discovering a time machine and travelling to Johnstown just prior to the flood.


=== Film and televisionEdit ===
The Johnstown Flood (1926) is an American silent epic film directed by Irving Cummings. A print is held at George Eastman House.
The Johnstown Flood (1946 film), a 1946 animated film. Mighty Mouse uses time-reversal power to undo the flood and prevent the dam from breaking in the first place. One of a series of cartoons where he stops disasters that actually happened.
The Johnstown Flood (1989 film), a 1989 short documentary film which won the Best Documentary Academy Award in 1990.
"Bloody Battles" episode of The Men Who Built America


=== TheaterEdit ===
"A True History of the Johnstown Flood" by Rebecca Gilman
By the early twentieth century, entertainers developed an exhibition portraying the flood, using moving scenery, light effects, and a live narrator. It was featured as a main attraction at the Stockholm Exhibition of 1909, where it was seen by 100,000 and presented as "our time's greatest electromechanical spectacle". The stage was 82 feet (25 m) wide, and the show employed a total of 13 stagehands.


=== MusicEdit ===
Highway Patrolman, a track from Bruce Springsteen's 1982 album Nebraska mentions a song titled "Night of the Johnstown Flood."
Mother Country, written by singer/songwriter John Stewart in 1969, contains the lyrics "What ever happened to those faces in the old photographs / I mean, the little boys....... /  Boys? . . . . . Hell they were men /  Who stood knee deep in the Johnstown mud /  In the time of that terrible flood /  And they listened to the water, that awful noise /  And then they put away the dreams that belonged to little boys."


=== LiteratureEdit ===
There is literature about the flood. Poems include:
William McGonagall (March 1825  29 September 1902) wrote "The Pennsylvania Disaster" (Lane, F.W. The Elements Rage (David & Charles 1966)) about the flood.
"By the Conemaugh", a poem by Florence Earle Coates
Short stories include:
Brian Booker's "A Drowning Accident", in One Story (Issue #57, May 30, 2005), was largely based on the Johnstown Flood of 1889.
Caitln R. Kiernan featured the flood in her "To This Water (Johnstown, Pennsylvania, 1889)", in her collected Tales of Pain and Wonder (1994).
Books about the flood in a historical context include:
Willis Fletcher Johnson wrote in 1889 a book called History of the Johnstown Flood (published by Edgewood Publishing Co.), likely the first book account of the flood.
Gertrude Quinn Slattery, who survived the flood as a six-year-old girl, published a memoir entitled Johnstown and Its Flood (1936).
Historian and author David McCullough's first book was The Johnstown Flood (1968), published by Simon & Schuster.
Fictional novels include:
Rudyard Kipling noted the flood in his novel, Captains Courageous (1897), as the disaster that destroyed the family of the minor character "Pennsylvania Pratt."
Marden A. Dahlstedt wrote the young adult novel, The Terrible Wave (1972), featuring a young girl as the main character, the book is inspired by the memoir of Gertrude Quinn (Slattery) who was six years old at the time of the flood.
John Jakes featured the flood in his novel, The Americans (1979), set in 1890 and the final book in the series of The Kent Family Chronicles.
Rosalyn Alsobrook wrote Emerald Storm (1985), a mass market historical romance set in Johnstown. The characters Patricia and Cole try to reunite with each other and loved ones after the flood.
Kathleen Cambor wrote the historical novel In Sunlight, In a Beautiful Garden (2001), based on events of the flood. The book was a New York Times Notable Book of the Year.
Richard A. Gregory wrote The Bosses Club, The conspiracy that caused the Johnstown Flood, destroying the iron and steel capital of America (2011), a historical novel that proposes a theory of the involvement of Andrew Carnegie and other wealthy American industrialists in the Johnstown Flood, told through the lives of two survivors.
Judith Redline Coopey wrote Waterproof: A Novel of the Johnstown Flood (2012), a story of Pamela Gwynedd McCrae from 18891939 through flashbacks.
Kathleen Danielczyk wrote "Summer of Gold and Water" (2013) which tells story of life at the lake, the flood and a coming together of the classes.
Colleen Coble wrote "The Wedding Quilt Bride" (2001) which tells the story of a romance between a member of the club's granddaughter and a man brought in to see if the dam was really in trouble. It follows him trying to convince the people of the danger and then the flood.
Michael Stephan Oates wrote the historical fiction novel "Wade in the Water" (2014), a coming of age tale set against the backdrop of the Johnstown flood.
Jeanette Watts's "Wealth and Privilege" (2014) portrays the Fishing and Hunting Club at its heyday, and then the main characters scramble for their lives in the Flood at the novel's climax.


== See alsoEdit ==
St. Francis Dam disaster
Vajont Dam disaster


== ReferencesEdit ==


== BibliographyEdit ==
 This article incorporates text from a publication now in the public domain: Chisholm, Hugh, ed. (1911). Encyclopdia Britannica (11th ed.). Cambridge University Press. 
Coleman, N.M., C. Davis Todd, et al. 2009. "Johnstown flood of 1889  destruction and rebirth" (Presentation 76-9). Geological Society of America meeting. Oct. 18-21. Portland, Ore.
Davis T., C., et al. 2009. "A determination of peak discharge rate and water volume from the 1889 Johnstown flood" (Presentation 76-10). Geological Society of America meeting. Oct. 18-21. Portland, Ore.
Johnson, Willis Fletcher. History of the Johnstown Flood (1889). [2]
McCullough, David. The Johnstown Flood (1968); ISBN 0-671-20714-8
O'Connor, R. Johnstown  The Day The Dam Broke (1957).


== External linksEdit ==
"Johnstown Flood Memorial", National Park Service
Johnstown Flood Tax
Johnstown Flood Museum  Johnstown Area Heritage Association
A Valley of Death Three Rivers Tribune (Three Rivers Michigan) #45 Vol. XI June 7, 1889
"The Johnstown Flood", Greater Johnstown/Cambria County Convention & Visitors Bureau
Perils of a Restless Planet: Scientific Perspectives on Natural Disasters by Ernest Zebrowsky