Chlorine trifluoride is an interhalogen compound with the formula ClF3. This colourless, poisonous, corrosive, and extremely reactive gas condenses to a pale-greenish yellow liquid, the form in which it is most often sold (pressurized at room temperature). The compound is primarily of interest as a component in rocket fuels, in plasmaless cleaning and etching operations in the semiconductor industry, in nuclear reactor fuel processing, and other industrial operations.


== Preparation, structure, and properties ==
It was first reported in 1930 by Ruff and Krug who prepared it by fluorination of chlorine; this also produced ClF and the mixture was separated by distillation.
3 F2 + Cl2  2 ClF3
ClF3 is approximately T-shaped, with one short bond (1.598 ) and two long bonds (1.698 ). This structure agrees with the prediction of VSEPR theory, which predicts lone pairs of electrons as occupying two equatorial positions of a hypothetic trigonal bipyramid. The elongated Cl-F axial bonds are consistent with hypervalent bonding.
Pure ClF3 is stable to 180 C in quartz vessels; above this temperature it decomposes by a free radical mechanism to the elements.


== Reactions ==
Reaction with several metals give chlorides and fluorides; phosphorus yields phosphorus trichloride (PCl3) and phosphorus pentafluoride (PF5); and sulfur yields sulfur dichloride (SCl2) and sulfur tetrafluoride (SF4). ClF3 also reacts explosively with water, in which it oxidizes water to give oxygen or in controlled quantities, oxygen difluoride (OF2), as well as hydrogen fluoride and hydrogen chloride. Metal oxides will react to form metal halides and oxygen or oxygen difluoride.
ClF3 + 2H2O  3HF + HCl + O2
ClF3 + H2O  HF + HCl + OF2
The main use of ClF3 is to produce uranium hexafluoride, UF6, as part of nuclear fuel processing and reprocessing, by the fluorination of uranium metal:
U + 3 ClF3  UF6 + 3 ClF
Dissociates under the scheme:
ClF3  ClF + F2


== Hazards ==
ClF3 is a very strong oxidizing and fluorinating agent. It is extremely reactive with most inorganic and organic materials, including glass and teflon, and will initiate the combustion of many otherwise non-flammable materials without any ignition source. These reactions are often violent, and in some cases explosive. Vessels made from steel, copper or nickel resist the attack of the material due to formation of a thin layer of insoluble metal fluoride, but molybdenum, tungsten and titanium form volatile fluorides and are consequently unsuitable. Any equipment that comes into contact with chlorine trifluoride must be scrupulously cleaned and then passivated, because any contamination left may burn through the passivation layer faster than it can re-form.
The power to surpass the oxidizing ability of oxygen leads to extreme corrosivity against oxide-containing materials often thought as incombustible. Chlorine trifluoride and gases like it have been reported to ignite sand, asbestos, and other highly fire-retardant materials. In an industrial accident, a spill of 900 kg of chlorine trifluoride burned through 30 cm of concrete and 90 cm of gravel beneath. Fire control/suppression is incapable of suppressing this oxidation, so the surrounding area must simply be kept cool until the reaction ceases. The compound reacts violently with water-based suppressors, and oxidizes in the absence of atmospheric oxygen, rendering atmosphere-displacement suppressors such as CO2 and halon completely ineffective. It ignites glass on contact.
Exposure of larger amounts of chlorine trifluoride, as a liquid or as a gas, ignites tissue. The hydrolysis reaction with water is violent and exposure results in a thermal burn. The products of hydrolysis are mainly hydrofluoric acid and hydrochloric acid, usually released as steam or vapor due to the highly exothermic nature of the reaction. Hydrofluoric acid is corrosive to human tissue, is absorbed through skin, selectively attacks bone, interferes with nerve function, and causes often-fatal fluorine poisoning. Hydrochloric acid is secondary in its danger to living organisms, but is several times more corrosive to most inorganic materials than hydrofluoric acid.


== Uses ==


=== Military applications ===
Under the code name N-Stoff ("substance N"), chlorine trifluoride was investigated for military applications by the Kaiser Wilhelm Institute in Nazi Germany from slightly before the start of World War II. Tests were made against mock-ups of the Maginot Line fortifications, and it was found to be an effective combined incendiary weapon and poison gas. From 1938, construction commenced on a partly bunkered, partly subterranean 31.76 km2 munitions factory, the Falkenhagen industrial complex, which was intended to produce 50 tonnes of N-stoff per month, plus sarin. However, by the time it was captured by the advancing Red Army in 1945, the factory had produced only about 30 to 50 tonnes, at a cost of over 100 German Reichsmark per kilograma. N-Stoff was never used in war.


=== Semiconductor industry ===
In the semiconductor industry, chlorine trifluoride is used to clean chemical vapour deposition chambers. It has the advantage that it can be used to remove semiconductor material from the chamber walls without having to dismantle the chamber. Unlike most of the alternative chemicals used in this role, it does not need to be activated by the use of plasma since the heat of the chamber is enough to make it decompose and react with the semiconductor material.


=== Rocket propellant ===
Chlorine trifluoride has been investigated as a high-performance storable oxidizer in rocket propellant systems. Handling concerns, however, prevented its use. John Drury Clark summarized the difficulties:

"It is, of course, extremely toxic, but that's the least of the problem. It is hypergolic with every known fuel, and so rapidly hypergolic that no ignition delay has ever been measured. It is also hypergolic with such things as cloth, wood, and test engineers, not to mention asbestos, sand, and water  with which it reacts explosively. It can be kept in some of the ordinary structural metals  steel, copper, aluminum, etc.  because of the formation of a thin film of insoluble metal fluoride which protects the bulk of the metal, just as the invisible coat of oxide on aluminum keeps it from burning up in the atmosphere. If, however, this coat is melted or scrubbed off, and has no chance to reform, the operator is confronted with the problem of coping with a metal-fluorine fire. For dealing with this situation, I have always recommended a good pair of running shoes."


== References ==

Notes

^a Using data from Economic History Services and The Inflation Calculator, we can calculate that 100 Reichsmark in 1941 is approximately equivalent to US$540 in 2006. Reichsmark exchange rate values from 1942 to 1944 are fragmentary.


== External links ==
National Pollutant Inventory - Fluoride and compounds fact sheet
NIST Standard Reference Database
CDC - NIOSH Pocket Guide to Chemical Hazards - Chlorine Trifluoride
WebElements
Sand Won't Save You This Time, blog post by Derek Lowe on the hazards of handling ClF3


== See also ==
Dioxygen difluoride
Chlorine fluoride