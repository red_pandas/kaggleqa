Vibration isolation is the process of isolating an object, such as a piece of equipment, from the source of vibrations.


== Passive isolationEdit ==
"Passive vibration isolation" refers to vibration isolation or mitigation of vibrations by passive techniques such as rubber pads or mechanical springs, as opposed to "active vibration isolation" or "electronic force cancellation" employing electric power, sensors, actuators, and control systems.
Passive vibration isolation is a vast subject, since there are many types of passive vibration isolators used for many different applications. A few of these applications are for industrial equipment such as pumps, motors, HVAC systems, or washing machines; isolation of civil engineering structures from earthquakes (base isolation), sensitive laboratory equipment, valuable statuary, and high-end audio.
A basic understanding of how passive isolation works, the more common types of passive isolators, and the main factors that influence the selection of passive isolators:


=== Common passive isolation systemsEdit ===
Pneumatic or air isolators
These are bladders or canisters of compressed air. A source of compressed air is required to maintain them. Air springs are rubber bladders which provide damping at the same time as isolation and are used in large trucks. Some pneumatic isolators can attain low resonant frequencies and are used for isolating large industrial equipment. Air tables consist of a working surface or optical surface mounted on air legs. These tables provide enough isolation for laboratory instrument under some conditions. Air systems may leak under vacuum conditions. The air container can interfere with isolation of low-amplitude vibration.
Mechanical springs and spring-dampers
These are heavy-duty isolators used for building systems and industry. Sometimes they serve as mounts for a concrete block, which provides further isolation.
Pads or sheets of flexible materials such as elastomers, rubber, cork, dense foam and laminate materials.
Elastomer pads, dense closed cell foams and laminate materials are often used under heavy machinery, under common household items, in vehicles and even under higher performing audio systems.
Molded and bonded rubber and elastomeric isolators and mounts
These are often used as machinery mounts or in vehicles. They absorb shock and attenuate some vibration.
Negative-stiffness isolators
Negative-stiffness isolators are less common than other types and have generally been developed for high-level research applications such as gravity wave detection. Lee, Goverdovskiy, and Temnikov (2007) proposed a negative-stiffness system for isolating vehicle seats.
The focus on negative 'stiffness isolators has been on developing systems with very low resonant frequencies (below 1 Hz), so that low frequencies can be adequately isolated, which is critical for sensitive instrumentation. In addition, all higher frequencies are also isolated. Negative stiffness systems can be made with low stiction, so that they are effective in isolating low-amplitude vibrations.
Negative-stiffness mechanisms are purely mechanical and typically involve the configuration and loading of components such as beams or inverted pendulums. Greater loading of the negative-stiffness mechanism, within the range of its operability, decreases the natural frequency.
Wire rope isolators
These isolators are durable and can withstand extreme environments. They are often used in military applications.
Bungee cord isolators and tennis balls
Bungee cords can be used as a cheap isolation system which may be effective enough for some applications. The item to be isolated is suspended from the bungee cords. This is difficult to implement without a danger of the isolated item falling. Tennis balls cut in half have been used under washing machines and other items with some success.
Base isolators for seismic isolation of buildings, bridges, etc.
Base isolators made of layers of neoprene and steel with a low horizontal stiffness are used to lower the natural frequency of the building. Some other base isolators are designed to slide, preventing the transfer of energy from the ground to the building.
Tuned mass dampers
Tuned mass dampers reduce the effects of harmonic vibration in buildings or other structures. A relatively small mass is attached in such a way that it can dampen out a very narrow band of vibration of the structure.


=== How passive isolation worksEdit ===
A passive isolation system, such as a shock mount, in general contains mass, spring, and damping elements and moves as a harmonic oscillator. The mass and spring stiffness dictate a natural frequency of the system. Damping causes energy dissipation and has a secondary effect on natural frequency.

Every object on a flexible support has a fundamental natural frequency. When vibration is applied, energy is transferred most efficiently at the natural frequency, somewhat efficiently below the natural frequency, and with increasing inefficiency (decreasing efficiency) above the natural frequency. This can be seen in the transmissibility curve, which is a plot of transmissibility vs. frequency.
Here is an example of a transmissibility curve. Transmissibility is the ratio of vibration of the isolated surface to that of the source. Vibrations are never completely eliminated, but they can be greatly reduced. The curve below shows the typical performance of a passive, negative-stiffness isolation system with a natural frequency of 0.5 Hz. The general shape of the curve is typical for passive systems. Below the natural frequency, transmissibility hovers near 1. A value of 1 means that vibration is going through the system without being amplified or reduced. At the resonant frequency, energy is transmitted efficiently, and the incoming vibration is amplified. Damping in the system limits the level of amplification. Above the resonant frequency, little energy can be transmitted, and the curve rolls off to a low value. A passive isolator can be seen as a mechanical low-pass filter for vibrations.

In general, for any given frequency above the natural frequency, an isolator with a lower natural frequency will show greater isolation than one with a higher natural frequency. The best isolation system for a given situation depends on the frequency, direction, and magnitude of vibrations present and the desired level of attenuation of those frequencies.
All mechanical systems in the real world contain some amount of damping. Damping dissipates energy in the system, which reduces the vibration level which is transmitted at the natural frequency. The fluid in automotive shock absorbers is a kind of damper, as is the inherent damping in elastomeric (rubber) engine mounts.
Damping is used in passive isolators to reduce the amount of amplification at the natural frequency. However, increasing damping tends to reduce isolation at the higher frequencies. As damping is increased, transmissibility roll-off decreases. This can be seen in the chart below.

Passive isolation operates in both directions, isolating the payload from vibrations originating in the support, and also isolating the support from vibrations originating in the payload. Large machines such as washers, pumps, and generators, which would cause vibrations in the building or room, are often isolated from the floor. However, there are a multitude of sources of vibration in buildings, and it is often not possible to isolate each source. In many cases, it is most efficient to isolate each sensitive instrument from the floor. Sometimes it is necessary to implement both approaches.


==== Factors influencing the selection of passive vibration isolatorsEdit ====
Characteristics of item to be isolated
Size: The dimensions of the item to be isolated help determine the type of isolation which is available and appropriate. Small objects may use only one isolator, while larger items might use a multiple-isolator system.
Weight: The weight of the object to be isolated is an important factor in choosing the correct passive isolation product. Individual passive isolators are designed to be used with a specific range of loading.
Movement: Machines or instruments with moving parts may affect isolation systems. It is important to know the mass, speed, and distance traveled of the moving parts.

Operating Environment
Industrial: This generally entails strong vibrations over a wide band of frequencies and some amount of dust.
Laboratory: Labs are sometimes troubled by specific building vibrations from adjacent machinery, foot traffic, or HVAC airflow.
Indoor or outdoor: Isolators are generally designed for one environment or the other.
Corrosive/non-corrosive: Some indoor environments may present a corrosive danger to isolator components due to the presence of corrosive chemicals. Outdoors, water and salt environments need to be considered.
Clean room: Some isolators can be made appropriate for clean room.
Temperature: In general, isolators are designed to be used in the range of temperatures normal for human environments. If a larger range of temperatures is required, the isolator design may need to be modified.
Vacuum: Some isolators can be used in a vacuum environment. Air isolators may have leakage problems. Vacuum requirements typically include some level of clean room requirement and may also have a large temperature range.
Magnetism: Some experimentation which requires vibration isolation also requires a low-magnetism environment. Some isolators can be designed with low-magnetism components.
Acoustic noise: Some instruments are sensitive to acoustic vibration. In addition, some isolation systems can be excited by acoustic noise. It may be necessary to use an acoustic shield. Air compressors can create problematic acoustic noise, heat, and airflow.
Static or dynamic loads: This distinction is quite important as isolators are designed for a certain type and level of loading.
Static loading
is basically the weight of the isolated object with low-amplitude vibration input. This is the environment of apparently stationary objects such as buildings (under normal conditions) or laboratory instruments.
Dynamic loading
involves accelerations and larger amplitude shock and vibration. This environment is present in vehicles, heavy machinery, and structures with significant movement.

Cost:
Cost of providing isolation: Costs include the isolation system itself, whether it is a standard or custom product; a compressed air source if required; shipping from manufacturer to destination; installation; maintenance; and an initial vibration site survey to determine the need for isolation.
Relative costs of different isolation systems: Inexpensive shock mounts may need to be replaced due to dynamic loading cycles. A higher level of isolation which is effective at lower vibration frequencies and magnitudes generally costs more. Prices can range from a few dollars for bungee cords to millions of dollars for some space applications.

Adjustment: Some isolation systems require manual adjustment to compensate for changes in weight load, weight distribution, temperature, and air pressure, whereas other systems are designed to automatically compensate for some or all of these factors.
Maintenance: Some isolation systems are quite durable and require little or no maintenance. Others may require periodic replacement due to mechanical fatigue of parts or aging of materials.
Size Constraints: The isolation system may have to fit in a restricted space in a laboratory or vacuum chamber, or within a machine housing.
Nature of vibrations to be isolated or mitigated
Frequencies: If possible, it is important to know the frequencies of ambient vibrations. This can be determined with a site survey or accelerometer data processed through FFT analysis.
Amplitudes: The amplitudes of the vibration frequencies present can be compared with required levels to determine whether isolation is needed. In addition, isolators are designed for ranges of vibration amplitudes. Some isolators are not effective for very small amplitudes.
Direction: Knowing whether vibrations are horizontal or vertical can help to target isolation where it is needed and save money.

Vibration specifications of item to be isolated: Many instruments or machines have manufacturer-specified levels of vibration for the operating environment. The manufacturer may not guarantee the proper operation of the instrument if vibration exceeds the spec.


==== Comparison of passive isolatorsEdit ====


=== Negative-stiffness vibration isolatorEdit ===
Negative-Stiffness-Mechanism (NSM) vibration isolation systems offer a unique passive approach for achieving low vibration environments and isolation against sub-Hertz vibrations. "Snap-through" or "over-center" NSM devices are used to reduce the stiffness of elastic suspensions and create compact six-degree-of-freedom systems with low natural frequencies. Practical systems with vertical and horizontal natural frequencies as low as 0.2 to 0.5 Hz are possible. Electro-mechanical auto-adjust mechanisms compensate for varying weight loads and provide automatic leveling in multiple-isolator systems, similar to the function of leveling valves in pneumatic systems. All-metal systems can be configured which are compatible with high vacuums and other adverse environments such as high temperatures.
These isolation systems enable vibration-sensitive instruments such as scanning probe microscopes, micro-hardness testers and scanning electron microscopes to operate in severe vibration environments sometimes encountered, for example, on upper floors of buildings and in clean rooms. Such operation would not be practical with pneumatic isolation systems. Similarly, they enable vibration-sensitive instruments to produce better images and data than those achievable with pneumatic isolators.
The theory of operation of NSM vibration isolation systems is summarized, some typical systems and applications are described, and data on measured performance is presented. The theory of NSM isolation systems is explained in References 1 and 2. It is summarized briefly for convenience.


==== Vertical-motion isolationEdit ====
A vertical-motion isolator is shown . It uses a conventional spring connected to an NSM consisting of two bars hinged at the center, supported at their outer ends on pivots, and loaded in compression by forces P. The spring is compressed by weight W to the operating position of the isolator, as shown in Figure 1. The stiffness of the isolator is K=KS-KN where KS is the spring stiffness and KN is the magnitude of a negative stiffness which is a function of the length of the bars and the load P. The isolator stiffness can be made to approach zero while the spring supports the weight W.


==== Horizontal-motion isolationEdit ====
A horizontal-motion isolator consisting of two beam-columns is illustrated in Figure. 2. Each beam-column behaves like two fixed-free beam columns loaded axially by a weight load W. Without the weight load the beam-columns have horizontal stiffness KS With the weight load the lateral bending stiffness is reduced by the "beam-column" effect. This behavior is equivalent to a horizontal spring combined with an NSM so that the horizontal stiffness is K=KS-KN, and KN is the magnitude of the beam-column effect. Horizontal stiffness can be made to approach zero by loading the beam-columns to approach their critical buckling load.


==== Six-degree-of-freedom (six-DOF) isolationEdit ====
A six-DOF NSM isolator typically uses three isolators stacked in series: a tilt-motion isolator on top of a horizontal-motion isolator on top of a vertical-motion isolator. Figure 3 shows a schematic of a vibration isolation system consisting of a weighted platform supported by a single six-DOF isolator incorporating the isolators of Figures 1 and 2. Flexures are used in place of the hinged bars shown in Figure 1. A tilt flexure serves as the tilt-motion isolator. A vertical-stiffness adjustment screw is used to adjust the compression force on the negative-stiffness flexures thereby changing the vertical stiffness. A vertical load adjustment screw is used to adjust for varying weight loads by raising or lowering the base of the support spring to keep the flexures in their straight, unbent operating positions. 


=== Vibration isolation of supporting jointEdit ===
The equipment or other mechanical components are necessarily linked to surrounding objects (the supporting joint - with the support; the unsupporting joint - the pipe duct or cable), thus presenting the opportunity for unwanted transmission of vibrations. Using a suitably designed vibration-isolator (absorber), vibration isolation of the supporting joint is realized. The accompanying illustration shows the attenuation of vibration levels, as measured before installation of the functioning gear on a vibration isolator as well as after installation, for a wide range of frequencies.


==== The vibration isolatorEdit ====

This is defined as a device that reflects and absorbs waves of oscillatory energy, extending from a piece of working machinery or electrical equipment, and with the desired effect being vibration insulation. The goal is to establish vibration isolation between a body transferring mechanical fluctuations and a supporting body (for example, between the machine and the foundation). The illustration shows a vibration isolator from the series  (~"VI" in Roman characters), as used in shipbuilding in Russia, for example the submarine "St.Petersburg" (Lada). The depicted  devices allow loadings ranging from 5, 40 and 300 kg. They differ in their physical sizes, but all share the same fundamental design. The structure consists of a rubber envelope that is internally reinforced by a spring. During manufacture, the rubber and the spring are intimately and permanently connected as a result of the vulcanization process that is integral to the processing of the crude rubber material. Under action of weight loading of the machine, the rubber envelope deforms, and the spring is compressed or stretched. Therefore, in the direction of the spring's cross section, twisting of the enveloping rubber occurs. The resulting elastic deformation of the rubber envelope results in very effective absorption of the vibration. This absorption is crucial to reliable vibration insulation, because it averts the potential for resonance effects. The amount of elastic deformation of the rubber largely dictates the magnitude of vibration absorption that can be attained; the entire device (including the spring itself) must be designed with this in mind. The design of the vibration isolator must also take into account potential exposure to shock loadings, in addition to the routine everyday vibrations. Lastly, the vibration isolator must also be designed for long-term durability as well as convenient integration into the environment in which it is to be used. Sleeves and flanges are typically employed in order to enable the vibration isolator to be securely fastened to the equipment and the supporting foundation.


=== Vibration isolation of unsupporting jointEdit ===
Vibration isolation of unsupporting joint is realized in the device named branch pipe a of isolating vibration.


==== Branch pipe a of isolating vibrationEdit ====
Branch pipe a of isolating vibration is a part of a tube with elastic walls for reflection and absorption of waves of the oscillatory energy extending from the working pump over wall of the pipe duct. Is established between the pump and the pipe duct. On an illustration is presented the image a vibration-isolating branch pipe of a series . In a structure is used the rubber envelope, which is reinforced by a spring. Properties of an envelope are similar envelope to an isolator vibration. Has the device reducing axial effort from action of internal pressure up to zero.


=== Subframe isolationEdit ===

Another technique used to increase isolation is to use an isolated subframe. This splits the system with an additional mass/spring/damper system. This doubles the high frequency attenuation rolloff, at the cost of introducing additional low frequency modes which may cause the low frequency behaviour to deteriorate. This is commonly used in the rear suspensions of cars with Independent Rear Suspension (IRS), and in the front subframes of some cars. The graph (see illustration) shows the force into the body for a subframe that is rigidly bolted to the body compared with the red curve that shows a compliantly mounted subframe. Above 42 Hz the compliantly mounted subframe is superior, but below that frequency the bolted in subframe is better.


== Active isolationEdit ==
Active vibration isolation systems contain, along with the spring, a feedback circuit which consists of a sensor (for example a piezoelectric accelerometer or a geophone), a controller, and an actuator. The acceleration (vibration) signal is processed by a control circuit and amplifier. Then it feeds the electromagnetic actuator, which amplifies the signal. As a result of such a feedback system, a considerably stronger suppression of vibrations is achieved compared to ordinary damping. Active isolation today is used for applications where structures smaller than a micrometer have to be produced or measured. A couple of companies produce active isolation products as OEM for research, metrology, lithography and medical systems. Another important application is the semiconductor industry. In the microchip production, the smallest structures today are below 20 nm, so the machines which produce and check them have to oscillate much less.


=== Sensors for active isolationEdit ===
Piezoelectric accelerometers and force sensors
MEM accelerometers
Geophones
Proximity sensors
Interferometers


=== Actuators for active isolationEdit ===
Linear motors
Pneumatic actuators
Piezoelectric motors


== See alsoEdit ==
Cushioning
Shock absorber
Bushing (isolator)
Vibration
Noise, vibration, and harshness
Base isolation
Vibration control
Oscillation
Shock mount
Soundproofing
Sorbothane
Passive heave compensation
Active vibration control


== ReferencesEdit ==

Platus PhD, David L., SPIE International Society of Optical Engineering - July 1999, Optomechanical Engineering and Vibration Control Negative-Stiffness-Mechanism Vibration Isolation Systems
Harris, C., Piersol, A., Harris Shock and Vibration Handbook, Fifth Edition, McGraw-Hill, (2002), ISBN 0-07-137081-1
A.Kolesnikov Noise and vibration. Russia. Leningrad. Publ.Shipbuilding. 1988


== External linksEdit ==
White Paper on Active Vibration Isolation for Lithography and Imaging
Passive Isolation of Harmonic Excitation