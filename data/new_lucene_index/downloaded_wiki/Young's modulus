Young's modulus, which is also known as the elastic modulus, is a mechanical property of linear elastic solid materials. It defines the relationship between stress (force per unit area) and strain (proportional deformation) in a material. Young's modulus is named after the 19th-century British scientist Thomas Young. However, the concept was developed in 1727 by Leonhard Euler, and the first experiments that used the concept of Young's modulus in its current form were performed by the Italian scientist Giordano Riccati in 1782, pre-dating Young's work by 25 years. The term modulus is the diminutive of the Latin term modus which means measure.
A solid body deforms when a load is applied to it. If the material is elastic, the body returns to its original shape after the load is removed. The material is linear if the ratio of load to deformation remains constant during the loading process. Not many materials are linear and elastic beyond a small amount of deformation. A constant Young's modulus applies only to linear elastic materials. A perfectly rigid material has an infinite Young's modulus because an infinite force is needed to deform such a material. A material whose Young's modulus is very high can be approximated as rigid.
A stiff material needs more force to deform compared to a soft material. Therefore, the Young's modulus is a measure of the stiffness of a solid material. Do not confuse:
stiffness with strength: the strength of material is the amount of force it can withstand and still recover its original shape;
material stiffness with geometric stiffness: the geometric stiffness depends on shape, e.g. the stiffness of an I beam is much higher than that of a spring made of the same steel thus having the same rigidity;
stiffness with hardness: the hardness of a material defines the relative resistance that its surface imposes against the penetration of a harder body;
stiffness with toughness: toughness is the amount of energy that a material can absorb before fracturing.


== Technical ==
The technical definition is: the ratio of the stress (force per unit area) along an axis to the strain (ratio of deformation over initial length) along that axis in the range of stress in which Hooke's law holds.


=== Units ===
Young's modulus is the ratio of stress (which has units of pressure) to strain (which is dimensionless), and so Young's modulus has units of pressure. Its SI unit is therefore the pascal (Pa or N/m2 or m1kgs2). The practical units used are megapascals (MPa or N/mm2) or gigapascals (GPa or kN/mm2). In United States customary units, it is expressed as pounds (force) per square inch (psi). The abbreviation ksi refers to "kpsi", or thousands of pounds per square inch.


== Usage ==
The Young's modulus enables the calculation of the change in the dimension of a bar made of an isotropic elastic material under tensile or compressive loads. For instance, it predicts how much a material sample extends under tension or shortens under compression. The Young's modulus directly applies to cases of uniaxial stress, that is tensile or compressive stress in one direction and no stress in the other directions. Young's modulus is also used in order to predict the deflection that will occur in a statically determinate beam when a load is applied at a point in between the beam's supports. Other elastic calculations usually require the use of one additional elastic property, such as the shear modulus, bulk modulus or Poisson's ratio. Any two of these parameters are sufficient to fully describe elasticity in an isotropic material.


=== Linear versus non-linear ===
Young's modulus represents the factor of proportionality in Hooke's law, which relates the stress and the strain. However, Hooke's law is only valid under the assumption of an elastic and linear response. Any real material will eventually fail and break when stretched over a very large distance or with a very large force; however all solid materials exhibit nearly Hookean behavior for small enough strains or stresses. If the range over which Hooke's law is valid is large enough compared to the typical stress that one expects to apply to the material, the material is said to be linear. Otherwise (if the typical stress one would apply is outside the linear range) the material is said to be non-linear.
Steel, carbon fiber and glass among others are usually considered linear materials, while other materials such as rubber and soils are non-linear. However, this is not an absolute classification: if very small stresses or strains are applied to a non-linear material, the response will be linear, but if very high stress or strain is applied to a linear material, the linear theory will not be enough. For example, as the linear theory implies reversibility, it would be absurd to use the linear theory to describe the failure of a steel bridge under a high load; although steel is a linear material for most applications, it is not in such a case of catastrophic failure.
In solid mechanics, the slope of the stressstrain curve at any point is called the tangent modulus. It can be experimentally determined from the slope of a stressstrain curve created during tensile tests conducted on a sample of the material.


=== Directional materials ===
Young's modulus is not always the same in all orientations of a material. Most metals and ceramics, along with many other materials, are isotropic, and their mechanical properties are the same in all orientations. However, metals and ceramics can be treated with certain impurities, and metals can be mechanically worked to make their grain structures directional. These materials then become anisotropic, and Young's modulus will change depending on the direction of the force vector. Anisotropy can be seen in many composites as well. For example, carbon fiber has much higher Young's modulus (is much stiffer) when force is loaded parallel to the fibers (along the grain). Other such materials include wood and reinforced concrete. Engineers can use this directional phenomenon to their advantage in creating structures.


== Calculation ==
Young's modulus at a given strain, E(), can be calculated by dividing the tensile stress by the extensional strain in the elastic (initial, linear) portion of the physical stressstrain curve:

where
E is the Young's modulus (modulus of elasticity)
F is the force exerted on an object under tension;
A0 is the actual cross-sectional area through which the force is applied;
L is the amount by which the length of the object changes;
L0 is the original length of the object.


=== Force exerted by stretched or contracted material ===
The Young's modulus of a material can be used to calculate the force it exerts under specific strain.

where F is the force exerted by the material when contracted or stretched by L.
Hooke's law can be derived from this formula, which describes the stiffness of an ideal spring:

where it comes in saturation
 and 


=== Elastic potential energy ===
The elastic potential energy stored in a linear elastic material is given by the integral of the Hooke's law:

now by expliciting the intensive variables:

This means that the elastic potential energy density (i.e., per unit volume) is given by:

or, in simple notation, for a linear elastic material: , since the strain is defined .
In a nonlinear elastic material the Young modulus is a function of the strain, so the second equivalence no longer holds and the elastic energy is not a quadratic function of the strain:


=== Relation among elastic constants ===
For homogeneous isotropic materials simple relations exist between elastic constants (Young's modulus E, shear modulus G, bulk modulus K, and Poisson's ratio ) that allow calculating them all as long as two are known:


== Approximate values ==

Young's modulus can vary somewhat due to differences in sample composition and test method. The rate of deformation has the greatest impact on the data collected, especially in polymers. The values here are approximate and only meant for relative comparison.


== See also ==
Deflection
Deformation
Hardness
Hooke's law
Shear modulus
Bulk Modulus
Bending stiffness
Impulse excitation technique
Toughness
Yield (engineering)
List of materials properties


== References ==


== Further reading ==
ASTM E 111, "Standard Test Method for Young's Modulus, Tangent Modulus, and Chord Modulus," [1]
The ASM Handbook (various volumes) contains Young's Modulus for various materials and information on calculations. Online version (subscription required)


== External links ==
Matweb: free database of engineering properties for over 63,000 materials
Young's Modulus for groups of materials, and their cost