A flame ionization detector (FID) is a scientific instrument that measures the concentration of organic species in a gas stream. It is frequently used as a detector in gas chromatography. Standalone FIDs can also be used in applications such as landfill gas monitoring, fugitive emissions monitoring and internal combustion engine emissions measurement in stationary or portable instruments.


== History ==
The first flame ionization detectors were developed simultaneously and independently in 1957 by McWilliam and Dewar at Imperial Chemical Industries of Australia and New Zealand (ICIANZ, see Orica#History) Central Research Laboratory, Ascot Vale, Melbourne, Australia. and by Harley and Pretorius at the University of Pretoria in Pretoria, South Africa.
In 1959, Perkin Elmer Corp. included a flame ionization detector in its Vapor Fractometer


== Operating principle ==
The operation of the FID is based on the detection of ions formed during combustion of organic compounds in a hydrogen flame. The generation of these ions is proportional to the concentration of organic species in the sample gas stream. Hydrocarbons generally have molar response factors that are equal to number of carbon atoms in their molecule, while oxygenates and other species that contain heteroatoms tend to have a lower response factor. Carbon monoxide and carbon dioxide are not detectable by FID.


== Advantages and disadvantages ==


=== Advantages ===
Flame ionization detectors are used very widely in gas chromatography because of a number of advantages.
Cost: Flame ionization detectors are relatively inexpensive to acquire and operate.
Low maintenance requirements: Apart from cleaning or replacing the FID jet, these detectors require no maintenance.
Rugged construction: FIDs are relatively resistant to misuse.
Linearity and detection ranges: FIDs can measure organic substance concentration at very low and very high levels, having a linear response of 10^7.


=== Disadvantages ===
Flame ionization detectors cannot detect inorganic substances and some highly oxygenated or functionalized species. In some systems, CO and CO2 can be detected in the FID using a methanizer, which is a bed of Ni catalyst that reduces CO and CO2 to methane, which can be in turn detected by the FID. The methanizer is limited by its inability to reduce compounds other than CO and CO2 and its tendency to be poisoned by a number of chemicals commonly found in GC effluents.
An improvement to the methanizer exists in the form of a sequential reactor that oxidizes compounds before reducing them to methane. This method can be used to improve the response of the FID and allow for the detection of many more carbon-containing compounds. The complete conversion of compounds to methane and the now equivalent response in the detector also eliminates the need for calibrations and standards because response factors are all equivalent to those of methane. This allows for the rapid analysis of complex mixtures that contain molecules where standards are not available. The sequential reactor is sold commercially as the PolyarcTM reactor, available online from the Activated Research Company.
Another important disadvantage is that the FID flame oxidizes all compounds that pass through it; all hydrocarbons and oxygenates are oxidized to carbon dioxide and water and other heteroatoms are oxidized according to thermodynamics. For this reason, FIDs tend to be the last in a detector train and also cannot be used for preparatory work.


== Operation ==
In order to detect these ions, two electrodes are used to provide a potential difference. The positive electrode doubles as the nozzle head where the flame is produced. The other, negative electrode is positioned above the flame. When first designed, the negative electrode was either tear-drop shaped or angular piece of platinum. Today, the design has been modified into a tubular electrode, commonly referred to as a collector plate. The ions thus are attracted to the collector plate and upon hitting the plate, induce a current. This current is measured with a high-impedance picoammeter and fed into an integrator. The manner in which the final data is displayed is based on the computer and software. In general, a graph is displayed that has time on the x-axis and total ion on the y-axis.
The current measured corresponds roughly to the proportion of reduced carbon atoms in the flame. Specifically how the ions are produced is not necessarily understood, but the response of the detector is determined by the number of carbon atoms (ions) hitting the detector per unit time. This makes the detector sensitive to the mass rather than the concentration, which is useful because the response of the detector is not greatly affected by changes in the carrier gas flow rate.


== Description of a generic detector ==

The design of the flame ionization detector varies from manufacturer to manufacturer, but the principles are the same. Most commonly, the FID is attached to a gas chromatography system.
The eluent exits the GC column (A) and enters the FID detectors oven (B). The oven is needed to make sure that as soon as the eluent exits the column, it does not come out of the gaseous phase and deposit on the interface between the column and FID. This deposition would result in loss of eluent and errors in detection. As the eluent travels up the FID, it is first mixed with the hydrogen fuel (C) and then with the oxidant (D). The eluent/fuel/oxidant mixture continues to travel up to the nozzle head where a positive bias voltage exists (E). This positive bias helps to repel the reduced carbon ions created by the flame (F) pyrolyzing the eluent. The ions are repelled up toward the collector plates (G) which are connected to a very sensitive ammeter, which detects the ions hitting the plates, then feeds that signal (H) to an amplifier, integrator, and display system. The products of the flame are finally vented out of the detector through the exhaust port (J).


== See also ==
Flame detector
Thermal Conductivity Detector
Gas Chromatography
Active fire protection
Photoionization detector
Photoelectric flame photometer


== References ==


== Sources ==