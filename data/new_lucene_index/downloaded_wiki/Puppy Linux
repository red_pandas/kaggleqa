Puppy Linux operating system is a lightweight Linux distribution that focuses on ease of use and minimal memory footprint. The entire system can be run from RAM with current versions generally taking up about 210 MB, allowing the boot medium to be removed after the operating system has started. Applications such as AbiWord (wordprocessing), Gnumeric (spreadsheets) and MPlayer (multimedia player) are included, along with a choice of lightweight web browsers. The distribution was originally developed by Barry Kauler and other members of the community. The tool Woof can build a Puppy Linux distribution from the binary packages of other Linux distributions.
Kauler retired from Puppy development in 2013.


== FeaturesEdit ==
Puppy Linux is a full-fledged operating system, bundled with a collection of application suites for a wide variety of tasks suitable for general use. Puppy is small, so it can boot from many types of media. It is also useful as a rescue disk, a demonstration system, leaving the original/existing operating system unaltered, as an OS for a system with a blank or missing hard drive, or for keeping old computers useful.
Puppy can boot from:
A live USB drive, including USB flash drives or any other bootable USB storage device (flash-Puppy)
A live CD (live-Puppy)
An SD card or built in Flash drive
A Zip drive or LS-120/240 SuperDisk
An internal hard disk drive
A computer network (thin-Puppy)
An emulator (emulated-puppy)
A floppy boot disk that loads the rest of the operating system from a USB drive, CD-ROM, or internal hard drive
Puppy Linux features built-in tools which can be used to create bootable USB drives, create new Puppy CDs, or remaster a new live CD with different packages.
Puppy Linux has a unique feature which sets it apart from other Linux distributions: the ability to offer a normal persistently updating working environment on a write-once multisession CD/DVD. (It does not require a rewritable CD/DVD.) Puppy automatically detects changes in the file system and saves them incrementally on the disc. This feature works particularly well with DVDs, partly because of the much larger space available. While other distributions offer Live CD versions of their operating systems, they do not allow programs to be permanently added nor do they allow files to be written to the CD.
Puppy also features sophisticated write-caching system designed to extend the life of USB flash drives that Puppy Linux runs from.
Unlike other operating systems, Puppy Linux does not mount (i.e. prepare to read from/write to) hard drives or connect to the network automatically. This reduces the odds that a bug or even intentionally added incompatible software corrupts the contents of a hard drive. Puppy Linux can also run on ARM platform and also on a single board computer such as Raspberry Pi.
Since Puppy Linux fundamentally runs in RAM, all the files, operations and configurations that are created/modified in a session would disappear when the system is shut down. However, it is possible to save files upon shutdown. This feature enables the user to either save the file to disk, (USB, HDD etc.) or write the file system to the same CD Puppy was booted from, if "multisession" was used to create the booted CD and if a CD burner is present. This is true for CD-Rs as well as CD-RWs and DVDs.
It is also possible to save all files to an external hard drive, USB stick, or even a floppy disk instead of the root file system. Puppy can also be installed to a hard disk.


== User interfaceEdit ==

The default window manager in most Puppy releases is JWM.
Packages of the IceWM desktop, Fluxbox and Enlightenment are also available via Puppy's PetGet package (application) management system (see below). Some derivative distributions, called puplets, come with default window managers other than JWM.
When the operating system boots, everything in the Puppy package uncompresses into a RAM area, the "ramdisk". The PC needs to have at least 128 MB of RAM (with no more than 8 MB shared video) for all of Puppy to load into the ramdisk. However, it is possible for it to run on a PC with only about 48 MB of RAM because part of the system can be kept on the hard drive, or less effectively, left on the CD.
Puppy is fairly full-featured for a system that runs entirely in a ramdisk, when booted as Live system or from a 'frugal' installation. However, Puppy also supports the 'full' installation mode, which enables Puppy to run from a hard drive partition, without a ramdisk. Applications were chosen that met various constraints, size in particular. Because one of the aims of the distribution is to be extremely easy to set up, there are many wizards that guide the user through a wide variety of common tasks.


== Package and distribution managementEdit ==

Puppy Linux's package manager, Puppy Package Manager, installs packages in PET (Puppy Enhanced Tarball) format by default but it also accepts packages from other distros (such as .deb, .rpm, .txz, and .tgz packages) or by using third-party tools to convert packages from other distros to PET packages. Puppy Package Manager can also trim the software bloat of a package to reduce the disk space used.


== Building the distributionEdit ==
On earlier release of Puppy Linux, Puppy Unleashed (currently replaced by Woof) is used to create Puppy ISO images. It consists of more than 500 packages that are put together according to the user's needs. However, on later versions starting with Puppy Linux version 5.0, it was replaced by Woof. It is an advanced tool for creating Puppy installations. It requires an Internet connection and some knowledge of Linux to use. It is able to download the binary source packages from another Linux distribution and process them into Puppy Linux packages by just defining the name of that Linux distro. It is equipped with a simpler version control named Bones on earlier releases but on later versions of woof, Fossil version control is used.
Puppy also comes with a remastering tool that takes a "snapshot" of the current system and lets the user create a live CD from it, and an additional remastering tool that is able to remove installed components.
Puppy Linux uses the T2 SDE build scripts to build the base binary packages.


== Official variantsEdit ==
Because of the relative ease with which the Woof tool and the remaster tool can be used to build variants of Puppy Linux, there are many variants available. Variants of Puppy Linux are known as puplets.
After Barry Kauler reduced his involvement with the Puppy Project, he designed two new distributions within the same Puppy Linux family, Quirky and Wary.
Quirky
An embedded distro with all files contained in an initramfs built into the kernel. It has simple module loading management but fewer drivers are included.
Racy
A variant of Wary optimized for newer PCs.
Wary
A Puppy variant targeted at users with old hardware. It uses an older Linux kernel, which has long-term support and the newest applications.


== Release historyEdit ==

Puppy 1 series will run comfortably on very dated hardware, such as a Pentium computer with at least 32 MB RAM. For newer systems, the USB keydrive version might be better (although if USB device booting is not directly supported in the BIOS, the Puppy floppy boot disk can be used to kick-start it). It is possible to run Puppy Linux with Windows 9x/Windows Me. It is also possible, if the BIOS does not support booting from USB drive, to boot from the CD and keep user state on a USB keydrive; this will be saved on shutdown and read from the USB device on bootup.
Puppy 2 uses the Mozilla-based SeaMonkey as its Internet suite (primarily a web browser and e-mail client).
Puppy 3 features Slackware 12 compatibility. This is accomplished by the inclusion of almost all the dependencies needed for the installation of Slackware packages. However, Puppy Linux is not a Slackware-based distribution.
Puppy 4 is built from scratch using the T2 SDE and no longer features native Slackware 12 compatibility in order to reduce the size and include newer package versions than that found in 3. To compensate for this, an optional "compatibility collection" of packages was created that restores some of the lost compatibility.
Puppy 4.2 features changes to the user interface and backend, upgraded packages, language and character support, new in-house software and optimizations, while still keeping the ISO image size under 100 MB.
Puppy 5 is based on a project called Woof which is designed to assemble a Puppy Linux distribution from the packages of other Linux distributions. Woof includes some binaries and software derived from Ubuntu, Debian, Slackware, T2 SDE, or Arch repositories. Puppy 5 came with a stripped down version of the Midori browser to be used for reading help files and a choice of web browsers to be installed, including Chromium, Firefox, SeaMonkey Internet Suite, Iron and Opera.
Puppy 6 is built from Ubuntu 14.04 Trusty Tahr packages, has binary compatibility with Ubuntu 14.04 and access to the Ubuntu package repositories. Tahrpup is built from the woof-CE build system, forked from Barry Kauler's Woof late last year after he announced his retirement from Puppy development. It is built from the latest testing branch, incorporates all the latest woof-CE features and is released in PAE and noPAE isos, with the option to switch kernels.


== ReceptionEdit ==
DistroWatch reviewer Rober Storey concluded about Puppy 5.2.5 in April 2011: "A lot of people like Puppy - it's in the top 10 of the DistroWatch page-hit ranking. I enjoy Puppy too, and it's what I run exclusively on my netbook. Maybe the only thing wrong with Puppy is that users' expectations tend to exceed the developer's intentions."
In a detailed review of Puppy Linux in May 2011 Howard Fosdick of OS News addressed the root user issue, "In theory this could be a problem -- but in practice it presents no downside. I've never heard of a single Puppy user suffering a problem due to this." Fosdick concluded "I like Puppy because it's the lightest Linux distro I've found that is still suitable for end users. Install it on an old P-III or P-IV computer and your family or friends will use it just as effectively for common tasks as any expensive new machine."
In December 2011 Jesse Smith, writing in DistroWatch reviewed Puppy 5.3.0 Slacko Puppy. He praised its simplicity, flexibility and clear explanations, while noting the limitations of running as root. He concluded "I would also like to see an option added during the boot process which would give the user the choice of running in unprivileged mode as opposed to running as root. Always being the administrator has its advantages for convenience, but it means that the user is always one careless click away from deleting their files and one exploit away from a compromised operating system. As a live CD it's hard to beat Puppy Linux for both performance and functional software. It has minimal hardware requirements and is very flexible. It's a great distro as long as you don't push it too far out of its niche."
In December 2011 Howard Fosdick reviewed the versions of Puppy Linux then available. He concluded, "Puppy's diversity and flexibility make it a great community-driven system for computer enthusiasts, hobbyists, and tinkerers. They also make for a somewhat disorderly world. You might have to read a bit to figure out which Puppy release or Puplet is for you. Puppy's online documentation is extensive but can be confusing. It's not always clear which docs pertain to which releases. Most users rely on the active, friendly forum for support." He also noted "Those of us who enjoy computers sometimes forget that many view them with disdain. What's wrong with it now? Why do I have to buy a new one every four years? Why on earth do they change the interface in every release? Can't it just work? Puppy is a great solution for these folks. It's up-to-date, free, and easy to use. And now, it supports free applications from the Ubuntu, Slackware, or Puppy repositories. Now that's user-friendly."


== See alsoEdit ==

Lightweight Linux distribution
List of Linux distributions that run from RAM


== ReferencesEdit ==


== External linksEdit ==
Official website
Community website
Puppy Linux at DistroWatch