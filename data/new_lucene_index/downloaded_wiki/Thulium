Thulium is a chemical element with symbol Tm and atomic number 69. It is the thirteenth and antepenultimate (third-last) element in the lanthanide series. Like the other lanthanides, the most common oxidation state is +3, seen in its oxide, halides and other compounds. In aqueous solution, like compounds of other late lanthanides, soluble thulium compounds form complexes with nine water molecules.
In 1879, Swedish chemist Per Theodor Cleve separated in the rare earth erbia another two previously unknown components, which he called holmia and thulia: these were the oxides of holmium and thulium respectively. A relatively pure sample of thulium metal was only obtained in 1911.
Thulium is the second least abundant of the lanthanides after promethium, which is only found in trace quantities on Earth. It is an easily workable metal with a bright silvery-gray luster. It is fairly soft and slowly tarnishes in air. Despite its high price and rarity, thulium is used as the radiation source in portable X-ray devices and in solid-state lasers. It has no significant biological role and is not particularly toxic.


== Properties ==


=== Physical properties ===
Pure thulium metal has a bright, silvery luster, which tarnishes on exposure to air. The metal can be cut with a knife, as it has a Mohs hardness of 2 to 3; it is malleable and ductile. Thulium is ferromagnetic below 32 K, antiferromagnetic between 32 and 56 K, and paramagnetic above 56 K. Liquid thulium is very volatile.
Thulium has two major allotropes: the tetragonal -Tm and the more stable hexagonal -Tm.


=== Chemical properties ===
Thulium tarnishes slowly in air and burns readily at 150 C to form thulium(III) oxide:
4 Tm + 3 O2  2 Tm2O3
Thulium is quite electropositive and reacts slowly with cold water and quite quickly with hot water to form thulium hydroxide:
2 Tm (s) + 6 H2O (l)  2 Tm(OH)3 (aq) + 3 H2 (g)
Thulium reacts with all the halogens. Reactions are slow at room temperature, but are vigorous above 200 C:
2 Tm (s) + 3 F2 (g)  2 TmF3 (s) (white)
2 Tm (s) + 3 Cl2 (g)  2 TmCl3 (s) (yellow)
2 Tm (s) + 3 Br2 (g)  2 TmBr3 (s) (white)
2 Tm (s) + 3 I2 (g)  2 TmI3 (s) (yellow)
Thulium dissolves readily in dilute sulfuric acid to form solutions containing the pale green Tm(III) ions, which exist as [Tm(OH2)9]3+ complexes:
2 Tm (s) + 3 H2SO4 (aq)  2 Tm3+ (aq) + 3 SO2
4 (aq) + 3 H2 (g)
Thulium reacts with various metallic and non-metallic elements forming a range of binary compounds, including TmN, TmS, TmC2, Tm2C3, TmH2, TmH3, TmSi2, TmGe3, TmB4, TmB6 and TmB12. In those compounds, thulium exhibits valence states +2 and +3, however, the +3 state is most common and only this state has been observed in thulium solutions. Thulium exists as a Tm3+ ion in solution. In this state, the thulium ion is surrounded by nine molecules of water. Tm3+ ions exhibit a bright blue luminescence.
Thulium's only known oxide is Tm2O3. This oxide is sometimes called "thulia". Reddish-purple thulium(II) compounds can be made by the reduction of thulium(III) compounds. Examples of thulium(II) compounds include thulium halides. Some hydrated thulium compounds, such as TmCl37H2O and Tm2(C2O4)36H2O are green or greenish-white. Thulium dichloride reacts very vigorously with water. This reaction results in hydrogen gas and Tm(OH)3 exhibiting a fading reddish color. Combination of thulium and chalcogens results in thulium chalcogenides.
Thulium reacts with hydrogen chloride to produce hydrogen gas and thulium chloride. With nitric acid it yields thulium nitrate, or Tm(NO3)3.


=== Isotopes ===

The isotopes of thulium range from 146Tm to 177Tm. The primary decay mode before the most abundant stable isotope, 169Tm, is electron capture, and the primary mode after is beta emission. The primary decay products before 169Tm are element 68 (erbium) isotopes, and the primary products after are element 70 (ytterbium) isotopes.
Thulium-169 is thulium's longest-lived and most abundant isotope. It is the only isotope of thulium that is thought to be stable, although it is predicted to undergo alpha decay to holmium-165 with a very long half-life. After thulium-169, the next-longest-lived isotopes are thulium-171, which has a half-life of 1.92 years, and thulium-170, which has a half-life of 128.6 days. Most other isotopes have half-lives of a few minutes or less. Thirty-five isotopes and 26 nuclear isomers of thulium have been detected. Most isotopes of thulium lighter than 169 atomic mass units decay via electron capture or beta-plus decay, although some exhibit significant alpha decay or proton emission. Heavier isotopes undergo beta-minus decay.


== History ==
Thulium was discovered by Swedish chemist Per Teodor Cleve in 1879 by looking for impurities in the oxides of other rare earth elements (this was the same method Carl Gustaf Mosander earlier used to discover some other rare earth elements). Cleve started by removing all of the known contaminants of erbia (Er2O3). Upon additional processing, he obtained two new substances; one brown and one green. The brown substance was the oxide of the element holmium and was named holmia by Cleve, and the green substance was the oxide of an unknown element. Cleve named the oxide thulia and its element thulium after Thule, an Ancient Greek place name associated with Scandinavia or Iceland. Thulium's atomic symbol was once Tu, but this was changed to Tm.
Thulium was so rare that none of the early workers had enough of it to purify sufficiently to actually see the green color; they had to be content with spectroscopically observing the strengthening of the two characteristic absorption bands, as erbium was progressively removed. The first researcher to obtain nearly pure thulium was Charles James, a British expatriate working on a large scale at New Hampshire College in Durham. In 1911 he reported his results, having used his discovered method of bromate fractional crystallization to do the purification. He famously needed 15,000 purification operations to establish that the material was homogeneous.
High-purity thulium oxide was first offered commercially in the late 1950s, as a result of the adoption of ion-exchange separation technology. Lindsay Chemical Division of American Potash & Chemical Corporation offered it in grades of 99% and 99.9% purity. The price per kilogram has oscillated between US$4,600 and $13,300 in the period from 1959 to 1998 for 99.9% purity, and it was second highest for lanthanides behind lutetium.


== Occurrence ==

The element is never found in nature in pure form, but it is found in small quantities in minerals with other rare earths. Thulium is often found with minerals containing yttrium and gadolinium. In particular, thulium occurs in the mineral gadolinite. However, thulium also occurs in the minerals monazite, xenotime, and euxenite. Its abundance in the Earth crust is 0.5 mg/kg by weight and 50 parts per billion by moles. Thulium makes up approximately 0.5 parts per million of soil, although this value can range from 0.4 to 0.8 parts per million. Thulium makes up 250 parts per quadrillion of seawater. In the solar system, thulium exists in concentrations of 200 parts per trillion by weight and 1 part per trillion by moles. Thulium ore occurs most commonly in China. However, Australia, Brazil, Greenland, India, Tanzania, and the United States also have large reserves of thulium. Total reserves of thulium are approximately 100,000 tonnes. Thulium is the least abundant lanthanide on earth except for promethium.


== Production ==
Thulium is principally extracted from monazite ores (~0.007% thulium) found in river sands, through ion-exchange. Newer ion-exchange and solvent-extraction techniques have led to easier separation of the rare earths, which has yielded much lower costs for thulium production. The principal sources today are the ion adsorption clays of southern China. In these, where about two-thirds of the total rare-earth content is yttrium, thulium is about 0.5% (or about tied with lutetium for rarity). The metal can be isolated through reduction of its oxide with lanthanum metal or by calcium reduction in a closed container. None of thulium's natural compounds are commercially important. Approximately 50 tonnes per year of thulium oxide are produced. In 1996, thulium oxide cost US$20 per gram, and in 2005, 99%-pure thulium metal powder cost US$70 per gram.


== Applications ==
Despite being rare and expensive, thulium has a few applications.


=== Laser ===
Holmium-chromium-thulium triple-doped Yttrium aluminum garnet (Ho:Cr:Tm:YAG, or Ho,Cr,Tm:YAG) is an active laser medium material with high efficiency. It lases at 2097 nm and is widely used in military applications, medicine, and meteorology. Single-element thulium-doped YAG (Tm:YAG) lasers operate between 1930 and 2040 nm. The wavelength of thulium-based lasers is very efficient for superficial ablation of tissue, with minimal coagulation depth in air or in water. This makes thulium lasers attractive for laser-based surgery.


=== X-ray source ===
Despite its high cost, portable X-ray devices use thulium that has been bombarded in a nuclear reactor as a radiation source. These sources have a useful life of about one year, as tools in medical and dental diagnosis, as well as to detect defects in inaccessible mechanical and electronic components. Such sources do not need extensive radiation protection  only a small cup of lead.
Thulium-170 is gaining popularity as an X-ray source for cancer treatment via brachytherapy. This isotope has a half-life of 128.6 days and five major emission lines of comparable intensity (at 7.4, 51.354, 52.389, 59.4 and 84.253 keV). Thulium-170 is one of the four most popular radioisotopes for use in industrial radiography.


=== Others ===
Thulium has been used in high-temperature superconductors similarly to yttrium. Thulium potentially has use in ferrites, ceramic magnetic materials that are used in microwave equipment. Thulium is also similar to scandium in that it is used in arc lighting for its unusual spectrum, in this case, its green emission lines, which are not covered by other elements. Because thulium fluoresces with a blue color when exposed to ultraviolet light, thulium is put into euro banknotes as a measure against counterfeiting. The blue fluorescence of Tm-doped calcium sulfate has been used in personal dosimeters for visual monitoring of radiation.


== Biological role and precautions ==
There is only a very small amount of thulium in the human body, but the exact amount is unknown. Thulium has not been observed to have a biological role, but small amounts of soluble thulium salts stimulate metabolism. Soluble thulium salts are mildly toxic, but insoluble thulium salts are completely nontoxic. When injected, thulium can cause degeneration of the liver and spleen and can also cause hemoglobin concentration to fluctuate. Liver damage from thulium is more prevalent in male mice than female mice. Despite this, thulium has a low level of toxicity. In humans, thulium occurs in the highest amounts in the liver, kidneys and bones. Humans typically consume several micrograms of thulium per year. The roots of plants do not take up thulium, and the dry weight of vegetables usually contains one part per billion of thulium. Thulium dust and powder are toxic upon inhalation or ingestion and can cause explosions. Radioactive thulium isotopes can cause radiation poisoning.


== See also ==
Thulium compounds


== References ==


== External links ==
Poole, Charles P., Jr. (2004). Encyclopedic Dictionary of Condensed Matter Physics. Academic Press. p. 1395. ISBN 978-0-08-054523-3.