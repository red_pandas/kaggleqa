Wrought iron is an iron alloy with a very low carbon (less than 0.08%) content in contrast to cast iron (2.1% to 4%). It has fibrous inclusions known as slag up to 2% by weight. It is a semi-fused mass of iron with slag inclusions which gives it a "grain" resembling wood, that is visible when it is etched or bent to the point of failure. Wrought iron is tough, malleable, ductile, corrosion-resistant and easily welded. Before the development of effective methods of steelmaking and the availability of large quantities of steel, wrought iron was the most common form of malleable iron.
A modest amount of wrought iron was used as a raw material for refining into steel, which was used mainly to produce swords, cutlery, chisels, axes and other edged tools as well as springs and files. The demand for wrought iron reached its peak in the 1860s with the adaptation of ironclad warships and railways. However, as properties such as brittleness of mild steel improved, it became less costly and more widely available than wrought iron, whose usage then declined.
Many items, before they came to be made of mild steel, were produced from wrought iron, including rivets, nails, wire, chains, rails, railway couplings, water and steam pipes, nuts, bolts, horseshoes, handrails, wagon tires, straps for timber roof trusses, and ornamental ironwork.
Wrought iron is no longer produced on a commercial scale. Many products described as wrought iron, such as guard rails, garden furniture and gates, are actually made of mild steel. They retain that description because in the past they were wrought (worked) by hand.


== Terminology ==
The word "wrought" is an archaic past participle of the verb "to work," and so "wrought iron" literally means "worked iron". Wrought iron is a general term for the commodity, but is also used more specifically for finished iron goods, as manufactured by a blacksmith. It was used in that narrower sense in British Customs records, such manufactured iron was subject to a higher rate of duty than what might be called "unwrought" iron. Cast iron, unlike wrought iron, is brittle and cannot be worked either hot or cold. Cast iron can break if struck with a hammer.
In the 17th, 18th, and 19th centuries, wrought iron went by a wide variety of terms according to its form, origin, or quality.
While the bloomery process produced wrought iron directly from ore, cast iron or pig iron were the starting materials used in the finery forge and puddling furnace. Pig iron and cast iron have higher carbon content than wrought iron, but have a lower melting point than iron or steel. Cast and especially pig iron have excess slag which must be at least partially removed to produce quality wrought iron. At foundries it was common to blend scrap wrought iron with cast iron to improve the physical properties of castings.
For several years after the introduction of Bessemer and open hearth steel, there were different opinions as to what differentiated iron from steel; some believed it was the chemical composition and others that it was whether the iron heated sufficiently to melt and "fuse". Fusion eventually became generally accepted as relatively more important than composition below a given low carbon concentration. Another difference is that steel can be hardened by heat treating.
Historically, wrought iron was known as "commercially pure iron", however, it no longer qualifies because current standards for commercially pure iron require a carbon content of less than 0.008 wt%.


=== Types and shapes ===
Bar iron is a generic term sometimes used to distinguish it from cast iron. It is the equivalent of an ingot of cast metal, in a convenient form for handling, storage, shipping and further working into a finished product.
The bars were the usual product of the finery forge, but not necessarily made by that process.
Rod ironcut from flat bar iron in a slitting mill provided the raw material for spikes and nails.
Hoop ironsuitable for the hoops of barrels, made by passing rod iron through rolling dies.
Plate ironsheets suitable for use as boiler plate.
Blackplatesheets, perhaps thinner than plate iron, from the black rolling stage of tinplate production.
Voyage ironnarrow flat bar iron, made or cut into bars of a particular weight, a commodity for sale in Africa for the Atlantic slave trade. The number of bars per ton gradually increased from 70 per ton in the 1660s to 7580 per ton in 1685 and "near 92 to the ton" in 1731.


=== Origin ===
Charcoal ironuntil the end of the 18th century, wrought iron was smelted from ore using charcoal, by the bloomery process. Wrought iron was also produced from pig iron using a finery forge or in a Lancashire hearth. The resulting metal was highly variable, both in chemistry and slag content.
Puddled ironthe puddling process was the first large-scale process to produce wrought iron. In the puddling process, pig iron is refined in a reverberatory furnace to prevent harmful sulphur from the coal or coke from contaminating the iron. The molten pig iron is manually stirred, which decarburizes the iron. As the iron is stirred, globs of wrought iron are collected into balls by the stirring rod (rabble arm or rod) and those are periodically removed by the puddler. Puddling was patented in 1784 and became widely used after 1800. By 1876, annual production of puddled iron in the UK alone was over 4 million tons. Around that time, the open hearth furnace was able to produce steel of suitable quality for structural purposes, and wrought iron production went into decline.
Oregrounds irona particularly pure grade of bar iron made ultimately from iron ore from the Dannemora mine in Sweden. Its most important use was as the raw material for the cementation process of steelmaking.
Danks ironoriginally iron imported to Great Britain from Gdask, but in the 18th century more probably the kind of iron (from eastern Sweden) that once came from Gdask.
Forest ironiron from the English Forest of Dean, where haematite ore enabled tough iron to be produced.
Lukes ironiron imported from Lige, whose Dutch name is "Luik."
Ames iron or amys ironanother variety of iron imported to England from northern Europe. Its origin has been suggested to be Amiens, but it seems to have been imported from Flanders in the 15th century and Holland later, suggesting an origin in the Rhine valley. Its origins remain controversial.
Botolf iron or Boutall ironfrom Bytw (Polish Pomerania) or Bytom (Polish Silesia).
Sable iron (or Old Sable)iron bearing the mark (a sable) of the Demidov family of Russian ironmasters, one of the better brands of Russian iron.


=== Quality ===
Tough iron 
Tough iron, also spelled "tuf", is not brittle and is strong enough to be used for tools.
Blend iron 
Blend iron is made using a mixture of different types of pig iron.
Best iron 
Best iron is iron that, having gone through several stages of piling and rolling, might reach the stage regarded as the best quality of iron (in the 19th century).
Marked bar iron 
This is iron made by members of the Marked Bar Association and marked with the maker's brand mark as a sign of its quality.


=== Defects ===
Wrought iron is a form of commercial iron, containing less than 0.10% of carbon, less than 0.25% of impurities total of sulfur, phosphorus, silicon and manganese, and less than 2% slag by weight. Wrought iron is redshort if it contains sulfur in excess quantity. It has sufficient tenacity when cold, but cracks when bent or finished at a red heat. It is therefore useless for welding or forging.
Coldshort iron, also known as coldshear, colshire or bloodshot, contains excessive phosphorus. It is very brittle when it is cold. It cracks if bent. It may, however, be worked at high temperature. Historically, coldshort iron was considered good enough for nails.
Phosphorus is not necessarily detrimental to iron. Ancient Indian smiths did not add lime to their furnaces. The absence of calcium oxide in the slag, and the deliberate use of wood with high phosphorus content during the smelting, induces a higher phosphorus content (less than 0.1%, average 0.25%) than in modern iron. Analysis of the Iron Pillar of Delhi gives 0.10% in the slags for 0.18% in the iron itself, for a total phosphorus content of 0.28% in the metal, accounting for much of its corrosion resistance. The presence of phosphorus (without carbon) produces a ductile iron suitable for wire drawing, for piano wire.


== History ==

Wrought iron has been used for many centuries, and is the "iron" that is referred to throughout Western history. The other form of iron, cast iron, was in use in China since ancient times but was not introduced into Western Europe until the 15th century; even then, due to its brittleness, it could be used for only a limited number of purposes. Throughout much of the Middle Ages iron was produced by the direct reduction of ore in manually operated bloomeries, although waterpower had begun to be employed by 1104.
The raw material produced by all indirect processes is pig iron. It has a high carbon content and as a consequence it is brittle and could not be used to make hardware. The osmond process was the first of the indirect processes, developed by 1203, but bloomery production continued in many places. The process depended on the development of the blast furnace, of which medieval examples have been discovered at Lapphyttan, Sweden and in Germany.
The bloomery and osmond processes were gradually replaced from the 15th century by finery processes, of which there were two versions, the German and Walloon. They were in turn replaced from the late 18th century by puddling, with certain variants such as the Swedish Lancashire process. Those, too, are now obsolete, and wrought iron is no longer manufactured commercially.


=== Bloomery process ===

Wrought iron was originally produced by a variety of smelting processes, all described today as "bloomeries". Different forms of bloomery were used at different places and times. The bloomery was charged with charcoal and iron ore and then lit. Air was blown in through a tuyere to heat the bloomery to a temperature somewhat below the melting point of iron. In the course of the smelt, slag would melt and run out, and carbon monoxide from the charcoal would reduce the ore to iron, which formed a spongy mass (called a "bloom") containing iron and also molten silicate minerals (slag) from the ore. The iron remained in the solid state. If the bloomery were allowed to become hot enough to melt the iron, carbon would dissolve into it and form pig or cast iron, but that was not the intention. However, the design of a bloomery made it difficult to reach the melting point of iron and also prevented the concentration of carbon monoxide from becoming high.
After smelting was complete, the bloom was removed, and the process could then be started again. It was thus a batch process, rather than a continuous one such as a blast furnace. The bloom had to be forged mechanically to consolidate it and shape it into a bar, expelling slag in the process.
During the Middle Ages, water-power was applied to the process, probably initially for powering bellows, and only later to hammers for forging the blooms. However, while it is certain that water-power was used, the details remain uncertain. That was the culmination of the direct process of ironmaking. It survived in Spain and southern France as Catalan Forges to the mid 19th century, in Austria as the stuckofen to 1775, and near Garstang in England until about 1770; it was still in use with hot blast in New York in the 1880s.


=== Osmond process ===

Osmond iron consisted of balls of wrought iron, produced by melting pig iron and catching the droplets on a staff, which was spun in front of a blast of air so as to expose as much of it as possible to the air and oxidise its carbon content. The resultant ball was often forged into bar iron in a hammer mill.


=== Finery process ===

In the 15th century, the blast furnace spread into what is now Belgium where it was improved. From there, it spread via the Pays de Bray on the boundary of Normandy and then to the Weald in England. With it, the finery forge spread. Those remelted the pig iron and (in effect) burnt out the carbon, producing a bloom, which was then forged into a bar iron. If rod iron was required, a slitting mill was used.
The finery process existed in two slightly different forms. In Great Britain, France, and parts of Sweden, only the Walloon process was used. That employed two different hearths, a finery hearth for finishing the iron and a chafery hearth for reheating it in the course of drawing the bloom out into a bar. The finery always burnt charcoal, but the chafery could be fired with mineral coal, since its impurities would not harm the iron when it was in the solid state. On the other hand, the German process, used in Germany, Russia, and most of Sweden used a single hearth for all stages.
The introduction of coke for use in the blast furnace by Abraham Darby in 1709 (or perhaps others a little earlier) initially had little effect on wrought iron production. Only in the 1750s was coke pig iron used on any significant scale as the feedstock of finery forges. However, charcoal continued to be the fuel for the finery.


=== Potting and stamping ===
From the late 1750s, ironmasters began to develop processes for making bar iron without charcoal. There were a number of patented processes for that, which are referred to today as potting and stamping. The earliest were developed by John Wood of Wednesbury and his brother Charles Wood of Low Mill at Egremont, patented in 1763. Another was developed for the Coalbrookdale Company by the Cranage brothers. Another important one was that of John Wright and Joseph Jesson of West Bromwich.


=== Puddling process ===

A number of processes for making wrought iron without charcoal were devised as the Industrial Revolution began during the latter half of the 18th century. The most successful of those was puddling, using a puddling furnace (a variety of the reverberatory furnace), which was invented by Henry Cort in 1784. It was later improved by others including Joseph Hall, who was the first to add iron oxide to the charge. In that type of furnace, the metal does not come into contact with the fuel, and so is not contaminated by its impurities . The heat of the combustion products pass over the surface of the puddle and the roof of the furnace reverberates (reflects) the heat onto the metal puddle on the fire bridge of the furnace.
Unless the raw material used is white cast iron, the pig iron or other raw product of the puddling first had to be refined into refined iron, or finers metal. That would be done in a refinery where raw coal was used to remove silicon and convert carbon within the raw material, found in the form of graphite, to a combination with iron called cementite.
In the fully developed process (of Hall), this metal was placed into the hearth of the puddling furnace where it was melted. The hearth was lined with oxidizing agents such as haematite and iron oxide. The mixture was subjected to a strong current of air and stirred with long bars, called puddling bars or rabbles, through working doors. The air, the stirring, and the "boiling" action of the metal helped the oxidizing agents to oxidize the impurities and carbon out of the pig iron. As the impurities oxidize, they formed a molten slag or drifted off as gas while the retaining iron solidified into spongy wrought iron that floated to the top of the puddle and were fished out of the melt as puddle balls using puddle bars.


==== Shingling ====

There was still some slag left in the puddle balls, so while they were still hot they would be shingled to remove the remaining slag and cinder. That was achieved by forging the balls under a hammer, or by squeezing the bloom in a machine. The material obtained at the end of shingling is known as bloom. The blooms are not useful in that form, so they were rolled into a final product.
Sometimes European ironworks would skip the shingling process completely and roll the puddle balls. The only drawback to that is that the edges of the rough bars were not as well compressed. When the rough bar was reheated, the edges might separate and be lost into the furnace.


==== Rolling ====

The bloom was passed through rollers and to produce bars. The bars of wrought iron were of poor quality, called muck bars or puddle bars. To improve their quality, the bars were cut up, piled and tied together by wires, a process known as faggoting or piling. They were then reheated and rolled again in merchant rolls. The process could be repeated several times to produce wrought iron of desired quality. Wrought iron that has been rolled multiple times is called merchant bar or merchant iron.


=== Lancashire process ===

The advantage of puddling was that it used coal, not charcoal as fuel. However, that was of little advantage in Sweden, which lacked coal. Gustaf Ekman observed charcoal fineries at Ulverston, which were quite different from any in Sweden. After his return to Sweden in the 1830s, he experimented and developed a process similar to puddling but used firewood and charcoal, which was widely adopted in the Bergslagen in the following decades.


=== Aston process ===
In 1925, James Aston of the United States developed a process for manufacturing wrought iron quickly and economically. It involved taking molten steel from a Bessemer converter and pouring it into cooler liquid slag. The temperature of the steel is about 1500 C and the liquid slag is maintained at approximately 1200 C. The molten steel contains a large amount of dissolved gases so when the liquid steel hit the cooler surfaces of the liquid slag the gases were liberated. The molten steel then froze to yield a spongy mass having a temperature of about 1370 C. The spongy mass would then be finished by being shingled and rolled as described under puddling (above). Three to four tons could be converted per batch with the method.


=== Decline ===
Steel began to replace iron for railroad rails as soon as the Bessemer process for making steel was adopted (1865 on). Iron remained dominant for structural applications until the 1880s, because of problems with brittle steel, caused by introduced nitrogen, high carbon, excess phosphorus or excessive temperature during rolling, or too-rapid rolling. By 1890 steel had largely replaced iron for structural applications.
Sheet iron (Armco 99.97% pure iron) had good properties for use in appliances, including being well-suited for enamelling and welding, and being rust-resistant.
In the 1960s, the price of steel production was dropping due to recycling, and even using the Aston process, wrought iron production was a labor-intensive process. It has been estimated that the production of wrought iron costs approximately twice as much as the production of low-carbon steel. In the United States the last plant closed in 1969. The last in Great Britain (and the world) was the Atlas Forge of Thomas Walmsley and Sons in Bolton, which closed in 1973. Its equipment, of a type dating from the 1860s, was moved to the Blists Hill site of Ironbridge Gorge Museum for preservation. Some wrought iron is still being produced for heritage restoration purposes, but only by recycling scrap.


== Properties ==

The slag inclusions, or stringers, in wrought iron give it properties not found in other forms of ferrous metal. There are approximately 250,000 inclusions per square inch. A fresh fracture shows a clear bluish color with a high silky luster and fibrous appearance.
Wrought iron lacks the carbon content necessary for hardening through heat treatment, but in areas where steel was uncommon or unknown, tools were sometimes cold-worked (hence cold iron) in order to harden them. An advantage of its low carbon content is its excellent weldability. Furthermore, sheet wrought iron cannot bend as much as steel sheet metal (when cold worked). Wrought iron can be melted and cast, however the product is no longer wrought iron, since the slag stringers characteristic of wrought iron disappear on melting, so the product resembles impure cast Bessemer steel. There is no engineering advantage as compared to cast iron or steel, both of which are cheaper.
Due to the variations in iron ore origin and iron manufacture, wrought iron can be inferior or superior in corrosion resistance compared to other iron alloys. There are many mechanisms behind that corrosion resistance. Chilton and Evans found that nickel enrichment bands reduce corrosion. They also found that in puddled, forged and piled iron, the working-over of the metal spread out copper, nickel and tin impurities, which produces electrochemical conditions that slow down corrosion. The slag inclusions have been shown to disperse corrosion to an even film, enabling the iron to resist pitting. Another study has shown that slag inclusions are pathways to corrosion. Other studies show that sulfur impurities in the wrought iron decrease corrosion resistance, but phosphorus increase corrosion resistance. Environments with a high concentration of chlorine ions also decreases wrought iron's corrosion resistance.
Wrought iron may be welded in the same manner as mild steel, but the presence of oxide or inclusions will give defective results. The material has a rough surface, so it can hold platings and coatings better. For instance, a galvanic zinc finish applied to wrought iron is approximately 2540% thicker than the same finish on steel. In Table 1, the chemical composition of wrought iron is compared to that of pig iron and carbon steel. Although it appears that wrought iron and plain carbon steel have similar chemical compositions, that is deceiving. Most of the manganese, sulfur, phosphorus, and silicon are incorporated into the slag fibers present in the wrought iron, so, really, wrought iron is purer than plain carbon steel.
Amongst its other properties, wrought iron becomes soft at red heat, and can be easily forged and forge welded. It can be used to form temporary magnets, but cannot be magnetized permanently, and is ductile, malleable and tough.


=== Ductility ===
For most purposes, ductility is a more important measure of the quality of wrought iron than tensile strength. In tensile testing, the best irons are able to undergo considerable elongation before failure. Higher tensile wrought iron is brittle.
Because of the large number of boiler explosions on steamboats, the U.S. Congress passed legislation in 1830 which approved funds for correcting the problem. The treasury awarded a $1500 contract to the Franklin Institute to conduct a study. As part of the study, Walter R. Johnson and Benjamin Reeves conducted strength tests on various boiler iron using a tester they had built in 1832 based on the design of one by Lagerhjelm in Sweden. Unfortunately, because of the misunderstanding of tensile strength and ductility, their work did little to reduce failures.
The importance of ductility was recognized by some very early in the development of tube boilers, such as Thurston's comment:

If made of such good iron as the makers claimed to have put into them "which worked like lead," they would, as also claimed, when ruptured, open by tearing, and discharge their contents without producing the usual disastrous consequences of a boiler explosion.

Various 19th-century investigations of boiler explosions, especially those by insurance companies, found causes to be most commonly the result of operating boilers above the safe pressure range, either to get more power or due to defective boiler pressure relief valves and difficulties of obtaining reliable indication of pressure and water level. Poor fabrication was also a common problem. Also, the thickness of the iron in steam drums was low by modern standards.
By the late 19th century, when metallurgists were able to better understand what properties and processes made good iron, it was being displaced by steel. Also, the old cylindrical boilers with fire tubes were displaced by water tube boilers, which are inherently safer.


=== Purity ===
Dr Gerry McDonnell in 2010 England, demonstrated by analysis that a wrought iron bloom, from a traditional smelt, could be worked into 99.7% pure iron with no evidence of carbon. It was found that the stringers common to other wrought irons were not present thus making it very malleable for the smith to work hot and cold. A commercial source of pure iron is available and is used by smiths as an alternative to traditional wrought iron and other new generation ferrous metals.


== Applications ==
Wrought iron furniture has a long history, dating back to Roman times. There are 13th-century wrought iron gates in Westminster Abbey in London, and wrought iron furniture appeared to reach its peak popularity (in Britain) in the 17th century during the reign of William and Mary. However, cast iron and cheaper steel caused a gradual decline in wrought iron manufacture; the last wrought ironworks in Britain closed in 1974.
It is also used to make home decor items such as baker's racks, wine racks, pot racks, etageres, table bases, desks, gates, beds, candle holders, curtain rods, bars and bar stools.
The vast majority of wrought iron available today is from reclaimed materials. Old bridges and anchor chains dredged from harbors are major sources. The greater corrosion resistance of wrought iron is due to the siliceous impurities (naturally occurring in iron ore), namely ferric silicate.
Because of limited availability, the use of wrought iron today is usually reserved for special applications, such as fine carpentry tools and historical restoration for objects of great importance.
Wrought iron has been used for decades as a generic term across the gate and fencing industry, even though mild steel is used for manufacturing these 'wrought iron' gates. This is mainly because of true wrought iron's limited availability. Steel can also be hot-dip galvanised to prevent corrosion, which cannot be done with wrought iron.


== See also ==
Ironworkartisan metalwork for architectural elements, garden features, and ornamental objects
Cast iron
Semi-steel casting
Bronze and brass ornamental work


== References ==


== Further reading ==

Gordon, Robert B (1996). American Iron 16071900. Baltimore and London: Johns Hopkins University Press. ISBN 0-8018-6816-5.