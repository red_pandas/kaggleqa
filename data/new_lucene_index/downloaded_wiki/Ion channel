Not to be confused with: Ion Television or Ion implantation.

Ion channels are pore-forming membrane proteins whose functions include establishing a resting membrane potential, shaping action potentials and other electrical signals by gating the flow of ions across the cell membrane, controlling the flow of ions across secretory and epithelial cells, and regulating cell volume. Ion channels are present in the membranes of all cells. Ion channels are considered to be one of the two traditional classes of ionophoric proteins, with the other class known as ion transporters (including the sodium-potassium pump, sodium-calcium exchanger, and sodium-glucose transport proteins, amongst others).
Study of ion channels (channelomics) often includes biophysics, electrophysiology and pharmacology, utilizing techniques including voltage clamp, patch clamp, immunohistochemistry, X-ray crystallography, fluorescence, and RT-PCR.


== Basic features ==
There are two distinctive features of ion channels that differentiate them from other types of ion transporter proteins:
The rate of ion transport through the channel is very high (often 106 ions per second or greater).
Ions pass through channels down their electrochemical gradient, which is a function of ion concentration and membrane potential, "downhill", without the input (or help) of metabolic energy (e.g. ATP, co-transport mechanisms, or active transport mechanisms).
Ion channels are located within the plasma membrane of nearly all cells and many intracellular organelles. They are often described as narrow, water-filled tunnels that allow only ions of a certain size and/or charge to pass through. This characteristic is called selective permeability. The archetypal channel pore is just one or two atoms wide at its narrowest point and is selective for specific species of ion, such as sodium or potassium. However, some channels may be permeable to the passage of more than one type of ion, typically sharing a common charge: positive (cations) or negative (anions). Ions often move through the segments of the channel pore in single file nearly as quickly as the ions move through free solution. In many ion channels, passage through the pore is governed by a "gate", which may be opened or closed in response to chemical or electrical signals, temperature, or mechanical force.
Ion channels are integral membrane proteins, typically formed as assemblies of several individual proteins. Such "multi-subunit" assemblies usually involve a circular arrangement of identical or homologous proteins closely packed around a water-filled pore through the plane of the membrane or lipid bilayer. For most voltage-gated ion channels, the pore-forming subunit(s) are called the  subunit, while the auxiliary subunits are denoted , , and so on.


== Biological role ==
Because channels underlie the nerve impulse and because "transmitter-activated" channels mediate conduction across the synapses, channels are especially prominent components of the nervous system. Indeed, numerous toxins that organisms have evolved for shutting down the nervous systems of predators and prey (e.g., the venoms produced by spiders, scorpions, snakes, fish, bees, sea snails, and others) work by modulating ion channel conductance and/or kinetics. In addition, ion channels are key components in a wide variety of biological processes that involve rapid changes in cells, such as cardiac, skeletal, and smooth muscle contraction, epithelial transport of nutrients and ions, T-cell activation and pancreatic beta-cell insulin release. In the search for new drugs, ion channels are a frequent target.


== Diversity ==
There are over 300 types of ion channels in a living cell. Ion channels may be classified by the nature of their gating, the species of ions passing through those gates, the number of gates (pores) and localization of proteins.
Further heterogeneity of ion channels arises when channels with different constitutive subunits give rise to a specific kind of current. Absence or mutation of one or more of the contributing types of channel subunits can result in loss of function and, potentially, underlie neurologic diseases.


=== Classification by gating ===
Ion channels may be classified by gating, i.e. what opens and closes the channels. Voltage-gated ion channels open or close depending on the voltage gradient across the plasma membrane, while ligand-gated ion channels open or close depending on binding of ligands to the channel.


==== Voltage-gated ====

Voltage-gated ion channels open and close in response to membrane potential.
Voltage-gated sodium channels: This family contains at least 9 members and is largely responsible for action potential creation and propagation. The pore-forming  subunits are very large (up to 4,000 amino acids) and consist of four homologous repeat domains (I-IV) each comprising six transmembrane segments (S1-S6) for a total of 24 transmembrane segments. The members of this family also coassemble with auxiliary  subunits, each spanning the membrane once. Both  and  subunits are extensively glycosylated.
Voltage-gated calcium channels: This family contains 10 members, though these members are known to coassemble with 2, , and  subunits. These channels play an important role in both linking muscle excitation with contraction as well as neuronal excitation with transmitter release. The  subunits have an overall structural resemblance to those of the sodium channels and are equally large.
Cation channels of sperm: This small family of channels, normally referred to as Catsper channels, is related to the two-pore channels and distantly related to TRP channels.

Voltage-gated potassium channels (KV): This family contains almost 40 members, which are further divided into 12 subfamilies. These channels are known mainly for their role in repolarizing the cell membrane following action potentials. The  subunits have six transmembrane segments, homologous to a single domain of the sodium channels. Correspondingly, they assemble as tetramers to produce a functioning channel.
Some transient receptor potential channels: This group of channels, normally referred to simply as TRP channels, is named after their role in Drosophila phototransduction. This family, containing at least 28 members, is incredibly diverse in its method of activation. Some TRP channels seem to be constitutively open, while others are gated by voltage, intracellular Ca2+, pH, redox state, osmolarity, and mechanical stretch. These channels also vary according to the ion(s) they pass, some being selective for Ca2+ while others are less selective, acting as cation channels. This family is subdivided into 6 subfamilies based on homology: classical (TRPC), vanilloid receptors (TRPV), melastatin (TRPM), polycystins (TRPP), mucolipins (TRPML), and ankyrin transmembrane protein 1 (TRPA).
Hyperpolarization-activated cyclic nucleotide-gated channels: The opening of these channels is due to hyperpolarization rather than the depolarization required for other cyclic nucleotide-gated channels. These channels are also sensitive to the cyclic nucleotides cAMP and cGMP, which alter the voltage sensitivity of the channels opening. These channels are permeable to the monovalent cations K+ and Na+. There are 4 members of this family, all of which form tetramers of six-transmembrane  subunits. As these channels open under hyperpolarizing conditions, they function as pacemaking channels in the heart, particularly the SA node.
Voltage-gated proton channels: Voltage-gated proton channels open with depolarization, but in a strongly pH-sensitive manner. The result is that these channels open only when the electrochemical gradient is outward, such that their opening will only allow protons to leave cells. Their function thus appears to be acid extrusion from cells. Another important function occurs in phagocytes (e.g. eosinophils, neutrophils, macrophages) during the "respiratory burst." When bacteria or other microbes are engulfed by phagocytes, the enzyme NADPH oxidase assembles in the membrane and begins to produce reactive oxygen species (ROS) that help kill bacteria. NADPH oxidase is electrogenic, moving electrons across the membrane, and proton channels open to allow proton flux to balance the electron movement electrically.


==== Ligand-gated ====

Also known as ionotropic receptors, this group of channels open in response to specific ligand molecules binding to the extracellular domain of the receptor protein. Ligand binding causes a conformational change in the structure of the channel protein that ultimately leads to the opening of the channel gate and subsequent ion flux across the plasma membrane. Examples of such channels include the cation-permeable "nicotinic" Acetylcholine receptor, ionotropic glutamate-gated receptors and ATP-gated P2X receptors, and the anion-permeable -aminobutyric acid-gated GABAA receptor.
Ion channels activated by second messengers may also be categorized in this group, although ligands and second messengers are otherwise distinguished from each other.


==== Other gating ====
Other gating include activation/inactivation by, e.g., second messengers from the inside of the cell membrane, rather than from outside, as in the case for ligands. Ions may count to such second messengers, and then causes direct activation, rather than indirect, as in the case were the electric potential of ions cause activation/inactivation of voltage-gated ion channels.
Some potassium channels
Inward-rectifier potassium channels: These channels allow potassium to flow into the cell in an inwardly rectifying manner, i.e., potassium flows effectively into, but not out of, the cell. This family is composed of 15 official and 1 unofficial members and is further subdivided into 7 subfamilies based on homology. These channels are affected by intracellular ATP, PIP2, and G-protein  subunits. They are involved in important physiological processes such as the pacemaker activity in the heart, insulin release, and potassium uptake in glial cells. They contain only two transmembrane segments, corresponding to the core pore-forming segments of the KV and KCa channels. Their  subunits form tetramers.
Calcium-activated potassium channels: This family of channels is, for the most part, activated by intracellular Ca2+ and contains 8 members.
Two-pore-domain potassium channels: This family of 15 members form what is known as leak channels, and they follow Goldman-Hodgkin-Katz (open) rectification.

Light-gated channels like channelrhodopsin are directly opened by the action of light.
Mechanosensitive ion channels open under the influence of stretch, pressure, shear, and displacement.
Cyclic nucleotide-gated channels: This superfamily of channels contains two families: the cyclic nucleotide-gated (CNG) channels and the hyperpolarization-activated, cyclic nucleotide-gated (HCN) channels. It should be noted that this grouping is functional rather than evolutionary.
Cyclic nucleotide-gated channels: This family of channels is characterized by activation due to the binding of intracellular cAMP or cGMP, with specificity varying by member. These channels are primarily permeable to monovalent cations such as K+ and Na+. They are also permeable to Ca2+, though it acts to close them. There are 6 members of this family, which is divided into 2 subfamilies.
Hyperpolarization-activated cyclic nucleotide-gated channels

Temperature-gated channels: Members of the transient receptor potential ion channel superfamily, such as TRPV1 or TRPM8, are opened either by hot or cold temperatures.


=== Classification by type of ions ===
Chloride channels: This superfamily of poorly understood channels consists of approximately 13 members. They include ClCs, CLICs, Bestrophins and CFTRs. These channels are non-selective for small anions; however chloride is the most abundant anion, and hence they are known as chloride channels.
Potassium channels
Voltage-gated potassium channels e.g., Kvs, Kirs etc.
Calcium-activated potassium channels e.g., BKCa or MaxiK, SK, etc.
Inward-rectifier potassium channels
Two-pore-domain potassium channels: This family of 15 members form what is known as leak channels, and they follow Goldman-Hodgkin-Katz (open) rectification.

Sodium channels
Voltage-gated sodium channels (NaVs)
Epithelial sodium channels (ENaCs)

Calcium channels (CaVs)
Proton channels
Voltage-gated proton channels

Non-selective cation channels: These let many types of cations, mainly Na+, K+ and Ca2+, through the channel.
Most transient receptor potential channels


=== Other classifications ===
There are other types of ion channel classifications that are based on less normal characteristics, e.g. multiple pores and transient potentials.
Almost all ion channels have one single pore. However, there are also those with two:
Two-pore channels: This small family of 2 members putatively forms cation-selective ion channels. They are predicted to contain two KV-style six-transmembrane domains, suggesting they form a dimer in the membrane. These channels are related to catsper channels channels and, more distantly, TRP channels.
There are channels that are classified by the duration of the response to stimuli:
Transient receptor potential channels: This group of channels, normally referred to simply as TRP channels, is named after their role in Drosophila phototransduction. This family, containing at least 28 members, is incredibly diverse in its method of activation. Some TRP channels seem to be constitutively open, while others are gated by voltage, intracellular Ca2+, pH, redox state, osmolarity, and mechanical stretch. These channels also vary according to the ion(s) they pass, some being selective for Ca2+ while others are less selective, acting as cation channels. This family is subdivided into 6 subfamilies based on homology: canonical (TRPC), vanilloid receptors (TRPV), melastatin (TRPM), polycystins (TRPP), mucolipins (TRPML), and ankyrin transmembrane protein 1 (TRPA).


== Detailed structure ==
Channels differ with respect to the ion they let pass (for example, Na+, K+, Cl), the ways in which they may be regulated, the number of subunits of which they are composed and other aspects of structure. Channels belonging to the largest class, which includes the voltage-gated channels that underlie the nerve impulse, consists of four subunits with six transmembrane helices each. On activation, these helices move about and open the pore. Two of these six helices are separated by a loop that lines the pore and is the primary determinant of ion selectivity and conductance in this channel class and some others. The existence and mechanism for ion selectivity was first postulated in the 1960s by Clay Armstrong. He suggested that the pore lining could efficiently replace the water molecules that normally shield potassium ions, but that sodium ions were too small to allow such shielding, and therefore could not pass through. This mechanism was finally confirmed when the structure of the channel was elucidated. The channel subunits of one such other class, for example, consist of just this "P" loop and two transmembrane helices. The determination of their molecular structure by Roderick MacKinnon using X-ray crystallography won a share of the 2003 Nobel Prize in Chemistry.
Because of their small size and the difficulty of crystallizing integral membrane proteins for X-ray analysis, it is only very recently that scientists have been able to directly examine what channels "look like." Particularly in cases where the crystallography required removing channels from their membranes with detergent, many researchers regard images that have been obtained as tentative. An example is the long-awaited crystal structure of a voltage-gated potassium channel, which was reported in May 2003. One inevitable ambiguity about these structures relates to the strong evidence that channels change conformation as they operate (they open and close, for example), such that the structure in the crystal could represent any one of these operational states. Most of what researchers have deduced about channel operation so far they have established through electrophysiology, biochemistry, gene sequence comparison and mutagenesis.
Channels can have single (CLICs) to multiple transmembrane (K channels, P2X receptors, Na channels) domains which span plasma membrane to form pores. Pore can determine the selectivity of the channel. Gate can be formed either inside or outside the pore region.


== Ion channel blockers ==
A variety of inorganic and organic molecules can modulate ion channel activity and conductance. Some commonly used blockers include:
Tetrodotoxin (TTX), used by puffer fish and some types of newts for defense. It blocks sodium channels.
Saxitoxin is produced by a dinoflagellate also known as "red tide". It blocks voltage-dependent sodium channels.
Conotoxin is used by cone snails to hunt prey.
Lidocaine and Novocaine belong to a class of local anesthetics which block sodium ion channels.
Dendrotoxin is produced by mamba snakes, and blocks potassium channels.
Iberiotoxin is produced by the Buthus tamulus (Eastern Indian scorpion) and blocks potassium channels.
Heteropodatoxin is produced by Heteropoda venatoria (brown huntsman spider or laya) and blocks potassium channels.


== Diseases ==
There are a number of genetic disorders which disrupt normal functioning of ion channels and have disastrous consequences for the organism. Genetic disorders of ion channels and their modifiers are known as channelopathies. See Category:Channelopathy for a full list.
Shaker gene mutations cause a defect in the voltage gated ion channels, slowing down the repolarization of the cell.
Equine hyperkalaemic periodic paralysis as well as human hyperkalaemic periodic paralysis (HyperPP) are caused by a defect in voltage-dependent sodium channels.
Paramyotonia congenita (PC) and potassium-aggravated myotonias (PAM)
Generalized epilepsy with febrile seizures plus (GEFS+)
Episodic ataxia (EA), characterized by sporadic bouts of severe discoordination with or without myokymia, and can be provoked by stress, startle, or heavy exertion such as exercise.
Familial hemiplegic migraine (FHM)
Spinocerebellar ataxia type 13
Long QT syndrome is a ventricular arrhythmia syndrome caused by mutations in one or more of presently ten different genes, most of which are potassium channels and all of which affect cardiac repolarization.
Brugada syndrome is another ventricular arrhythmia caused by voltage-gated sodium channel gene mutations.
Cystic fibrosis is caused by mutations in the CFTR gene, which is a chloride channel.
Mucolipidosis type IV is caused by mutations in the gene encoding the TRPML1 channel
Mutations in and overexpression of ion channels are important events in cancer cells. In Glioblastoma multiforme, upregulation of gBK potassium channels and ClC-3 chloride channels enables glioblastoma cells to migrate within the brain, which may lead to the diffuse growth patterns of these tumors.


== History ==
The fundamental properties of currents mediated by ion channels were analyzed by the British biophysicists Alan Hodgkin and Andrew Huxley as part of their Nobel Prize-winning research on the action potential, published in 1952. They built on the work of other physiologists, such as Cole and Baker's research into voltage-gated membrane pores from 1941. The existence of ion channels was confirmed in the 1970s by Bernard Katz and Ricardo Miledi using noise analysis. It was then shown more directly with an electrical recording technique known as the "patch clamp", which led to a Nobel Prize to Erwin Neher and Bert Sakmann, the technique's inventors. Hundreds if not thousands of researchers continue to pursue a more detailed understanding of how these proteins work. In recent years the development of automated patch clamp devices helped to increase significantly the throughput in ion channel screening.
The Nobel Prize in Chemistry for 2003 was awarded to two American scientists: Roderick MacKinnon for his studies on the physico-chemical properties of ion channel structure and function, including x-ray crystallographic structure studies, and Peter Agre for his similar work on aquaporins.


== Culture ==

Roderick MacKinnon commissioned Birth of an Idea, a 5-foot (1.5 m) tall sculpture based on the KcsA potassium channel. The artwork contains a wire object representing the channel's interior with a blown glass object representing the main cavity of the channel structure.


== See also ==
Action potential
Active transport
Alpha helix
Channelome
Channelopathy
Electrochemical gradient
Gating (electrophysiology)
Ion channel family as defined in Pfam and InterPro
Ionophore
Ki Database
Lipid bilayer ion channels
Magnesium transport
Membrane potential
Neurotoxin
Passive transport
Potassium ion channels
Sodium ion channel
Synthetic ion channels
Transmembrane receptor
MeSH entry for Ion channels


== References ==


== External links ==
"Voltage-Gated Ion Channels". IUPHAR Database of Receptors and Ion Channels. International Union of Basic and Clinical Pharmacology. 
"TRIP Database". a manually curated database of protein-protein interactions for mammalian TRP channels. 
AurSCOPE Ion Channels Database