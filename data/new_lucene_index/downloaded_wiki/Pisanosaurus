Pisanosaurus (pron.:"PIE-san-uh-SAWR-us") is an extinct genus of primitive ornithischian dinosaur that lived approximately 228 to 216 million years ago during the latter part of the Triassic Period in what is now South America. It was a small, lightly-built, ground-dwelling, bipedal herbivore, that could grow up to an estimated 1 m (3.3 ft) long. Only one species, the type, Pisanosaurus mertii, is known, based on a single partial skeleton discovered in Argentina's Ischigualasto Formation.
The exact classification of Pisanosaurus has been the topic of debate by scientists for over 40 years; the current consensus is that Pisanosaurus is the oldest known ornithischian, part of a diverse group of dinosaurs which lived during nearly the entire span of the Mesozoic Era.


== Etymology ==
The genus name Pisanosaurus, means "Mr. Pisanos lizard", and is derived the "Pisano" in honor of Argentine paleontologist Juan A. Pisano of La Plata Museum, and "sauros" from the Greek () meaning "lizard". Pisanosaurus was described and named by Argentine paleontologist Rodolfo Casamiquela in 1967. The type and only valid species known today is Pisanosaurus mertii. The specific name honors the late Araucanian naturalist Carlos Merti.


== Discovery and naming ==
Pisanosaurus is known from a single fragmented skeleton discovered by Galileo J. Scaglia at the Agua de Las Catas locality in the Ischigualasto Formation, in La Rioja, Argentina. It is based on a specimen given the designation PVL 2577, which consists of a partial skull with a fragmentary right maxilla with teeth, and incomplete right mandibular ramus (lower jaw), six incomplete cervical vertebrae, seven incomplete dorsal vertebrae, molds of five sacral vertebrae, a rib and several rib fragments, a fragmentary scapula, a coracoid, a fragmentary ilium, ischium and pubis, an impression of 3 metacarpals, complete femora, the right tibia, the right fibula, with an articulated astragalus and calcaneum, a tarsal element with a metatarsal, metatarsals III and IV, three phalanges from metatarsal III and four metatarsals and the ungual from metatarsal IV, and an indeterminate long bone fragment.


== Description ==

Based on the known fossil elements from a partial skeleton, Pisanosaurus was a small, lightly built dinosaur approximately 1 m (3 ft 3 in) in length. Its weight was between 2.279.1 kg (520 lb). These estimates vary due to the incompleteness of the holotype specimen PVL 2577. The orientation of the pubis is uncertain, with some skeletal reconstructions having it projecting down and forward (the propubic condition) similar to that of the majority of saurischian dinosaurs. The tail of Pisanosaurus has been reconstructed as being as long as the rest of the body, based on other early ornithischians, but as a tail has not been recovered, this is speculative. It was bipedal and, like all other known ornithischians, was probably herbivorous.


== Classification ==
Pisanosaurus is the type genus of the Pisanosauridae, a family erected by Casamiquela in the same paper which named Pisanosaurus. The pisanosauridae family has fallen into disuse, as a 1976 study considered the group synonymous with the already named Heterodontosauridae.
Pisanosaurus is very basal within Ornithischia; the postcrania seem to lack any good ornithischian synapomorphy and it was even suggested by Paul Sereno in 1991 that the fossil is a chimera. However, recent studies suggest that the fossils belong to a single specimen.
Over the years, Pisanosaurus has been classified as a heterodontosaurid, a fabrosaurid, a hypsilophodont and has also been considered the earliest known ornithischian. A 2008 study placed Pisanosaurus outside of (and more basal than) Heterodontosauridae. In this study, Pisanosaurus is the earliest and most primitive ornithischian. This assignment is also supported by Norman et al. (2004) and Langer et al. (2009). Other primitive ornithischians include Eocursor, Trimucrodon, and possibly Fabrosaurus. On the other hand, a phylogenetic analysis conducted by Agnolin (2015) recovered Pisanosaurus as a non-dinosaurian member of Dinosauriformes related to the silesaurids.


=== Distinguishing anatomical features ===
A diagnosis is a statement of the anatomical features of an organism (or group) that collectively distinguish it from all other organisms. Some, but not all, of the features in a diagnosis are also autapomorphies. An autapomorphy is a distinctive anatomical feature that is unique to a given organism.
According to Bonaparte (1976), Pisanosaurus can be distinguished based on the following characteristics:
the acetabulum is open
the pedicels of the ilium are short, resulting in a low axially long acetabulum
the proximal region of the ischia is wide, larger than that of the pubis
the metacarpal are apparently elongated, measuring about 15 millimeters


== Paleoecology ==


=== Provenance and occurrence ===

The fossils of Pisanosaurus were discovered in the "Agua de las Catas" locality at the Ischigualasto Formation in La Rioja, Argentina. Originally dated to the Middle Triassic, this formation is now believed to belong to the Late Triassic Carnian stage, deposited approximately 228 to 216.5 million years ago. This specimen was collected by Jos Bonaparte, Herbst and the preparators Vince and Scaglia in 1962, and is housed in the collection of the Laboratorio de Paleontologia de Vertebrados, Instituto Miguel Lillo, in San Miguel de Tucumn, Argentina.


=== Fauna and habitat ===
The Ischigualasto Formation was a volcanically active floodplain covered by forests, with a warm and humid climate, though subject to seasonal variations including strong rainfalls. Vegetation consisted of ferns, horsetails, and giant conifers, which formed highland forests along the banks of rivers. Herrerasaurus remains appear to have been the most common among the carnivores of the Ischigualasto Formation. Sereno (1993) noted that Pisanosaurus was found in "close association" with therapsids, rauisuchians, archosaurs, Saurosuchus and the dinosaurs Herrerasaurus and Eoraptor, all of whom lived it its paleoenvironment. Bonaparte (1976) postulated that Pisanosaurus played a role in a fauna dominated by therapsids. The large carnivore Herrerasaurus may have fed upon Pisanosaurus. Herbivores were represented by rhynchosaurs such as Hyperodapedon (a beaked reptile); aetosaurs (spiny armored reptiles); kannemeyeriid dicynodonts (stocky, front-heavy beaked quadrupedal animals) such as Ischigualastia; and traversodontids (somewhat similar in overall form to dicynodonts, but lacking beaks) such as Exaeretodon. These non-dinosaurian herbivores were much more abundant than early dinosaurs


== References ==


== External links ==

Pisanosaurus from the PaleoBiology Database
Pisanosaurus at DinoData.com