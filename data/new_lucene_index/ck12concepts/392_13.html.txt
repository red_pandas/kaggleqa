Now, let’s ask a scientific question Remember that it must be testable
We learned in the previous concept, "Scientific Explanations and Interpretations," that the average global temperature has been on the rise Scientists know that carbon dioxide is a greenhouse gas  Greenhouse gases trap heat in the atmosphere This leads us to a question:
Question: Is the amount of carbon dioxide in Earth’s atmosphere changing?
This is a good scientific question because it is testable
Figure 133 According to data collected at the Mauna Loa Observatory in Hawaii, atmospheric carbon dioxide has been increasing since record keeping began in 1958 The small ups and downs of the red line are due to seasonal changes in the winter and summer The black line traces the annual average
How has carbon dioxide in the atmosphere changed since 1958 ( Figure above )? The line on the graph is going up so carbon dioxide has increased About how much has it increased in parts per million?