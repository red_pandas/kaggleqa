A calorimeter is a device used to measure changes in thermal energy or heat transfer More specifically, it measures calories A calorie is the amount of energy required to raise one gram of water by one degree Celsius As such, the calorimeter measures the temperature change of a known amount of water
The function of the calorimeter depends on the conservation of energy in a closed, isolated system Calorimeters are carefully insulated so that heat transfer in or out is negligible Consider the following example
Example Problem: A 0500 kg sample of water in a calorimeter is at 150ºC A 00400 kg block of zinc at 1150ºC is placed in the water The specific heat of zinc is 388 J/kg•ºC Find the final temperature of the system
Solution: The heat lost by the block of zinc will equal the heat gain by the water in the calorimeter In order to set heat gain mathematically equal to heat loss, either one of the terms must be made negative or the temperature change must be reversed You should also note that the final temperature of the water and the block of zinc will be the same when equilibrium is reached
Example Problem: A 100 g block of aluminum at 1000ºC is placed in 100 g of water at 100ºC The final temperature of the mixture is 250ºC What is the specific heat of the aluminum as determined by the experiment?
Solution:
Summary
A calorimeter is a device used to measure changes in thermal energy or heat transfer If a reaction is carried out in the reaction vessel or if a measured mass of heated substance is placed in the water of the calorimeter, the change in the water temperature allows us to calculate the change in thermal energy
Practice
Questions
The following video covers the calorimetry equation Use this resource to answer the questions that follow
http://wwwyoutubecom/watch?v=L9spPoot3fU
What is the number 418 J/g•°C in the video? In the equation , what does represent? What does it mean if the temperature in the calorimeter goes down?
Solved calorimetry problems:
http://calorimetry-physics-problemsblogspotcom/2010/10/specific-heat-problemshtml
Review
Questions
A 3000 g sample of water at 800ºC is mixed with 3000 g of water at 100ºC Assuming no heat loss to the surroundings, what is the final temperature of the mixture? A 4000 g sample of methanol at 160ºC is mixed with 4000 g of water at 850ºC Assuming no heat loss to the surroundings, what is the final temperature of the mixture? The specific heat of methanol is 2450 J/kg•ºC
0 g brass block at 1000ºC is placed in 2000 g of water at 200ºC The specific heat of brass is 376 J/kg•ºC Assuming no heat loss to the surroundings, what is the final temperature of the mixture?
calorimeter: A device used to measure the heat flow of a chemical reaction or physical change calorimetry: A way to measure the energy change of a reaction or the energy contained in matter
Change of State
Define heat of fusion Define heat of vaporization Calculate heat transfers necessary for changes of state
Before the internal combustion engine was invented, steam engines were the power source for ships, locomotives, tractors, lumber saws, and most industrial machines Coal or wood was burned to boil water into steam, which ran the engine