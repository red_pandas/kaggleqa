The focus of Water was on water's role in the formation of aqueous solutions We examined the primary characteristics of a solution, how water is able to dissolve solid solutes, and we differentiated between a solution, a suspension, and a colloid There are many examples of solutions that do not involve water at all, or that involve solutes that are not solids
Solute-Solvent Combinations
Gas-Gas Solutions
Our air is a homogeneous mixture of many different gases and therefore qualifies as a solution Approximately 78% of the atmosphere is nitrogen, making it the solvent for this solution The next major constituent is oxygen (about 21%), followed by the inert gas argon (09%), carbon dioxide (003%) and trace amounts of neon, methane, helium, and other gases
Solid-Solid Solutions
Solid-solid solutions such as brass, bronze, and sterling silver are called alloys Bronze (composed mainly of copper with added tin) was widely used in making weapons in times past dating back to at least 2400 BC This metal alloy was hard and tough, but was eventually replaced by iron
Liquid-Solid Solutions
Perhaps the most familiar liquid-solid solution is dental amalgam, used to fill teeth when there is a cavity Approximately 50% of the amalgam material is liquid mercury to which a powdered alloy of silver, tin and copper is added Mercury is used because it binds well with the solid metal alloy
Summary
Solutions may be composed of a variety of solid, liquid, or gaseous materials
Practice
Read the material and answer questions 1-3 on the following website:
http://wwwscienceuwaterlooca/~cchieh/cact/c120/solutionhtml
Review Questions
Does a solution have to have water as the solvent? Is there an example of a solution where water is the solute? When we mix ethylene glycol with the water in our car radiator to prevent freezing, which is the solvent and which is the solute?
Rate of Dissolving
List and describe factors that affect the rate of dissolving
How sweet do you like your tea?
Many people enjoy a cold glass of iced tea on a hot summer day Some like it unsweetened, while others like to put sugar in it How fast the sugar dissolved depends on several factors: how much sugar was put in the tea and how cold it is? You usually have to stir the tea for a while to get all the sugar dissolved