antennapedia : Gene that controls the formation of legs during development; first identified as a Drosophila mutant
enhancer : A site on the DNA strand that can be bound by activator(s) in order to loop the DNA, bringing a specific promoter to the initiation complex
gap gene : Gene that controls the shape of a developing zygote early in its development; encodes transcription factors
homeobox gene : Genes that contain a highly conserved DNA sequence known as a homeobox and are involved in the regulation of genes important to development; encodes transcription factors
homeodomain : DNA binding domain within gene products of homeobox genes
hox gene : Genes that function in patterning the body, providing the placement of certain body parts during development
maternal effect : Occurs when an organism shows the phenotype expected from the genotype of the mother, irrespective of its own genotype; often due to the mother supplying mRNA or proteins to the egg
pair-rule gene : A type of gene involved in the development of the segmented embryos of insects
TATA box : A cis-regulatory element found in the promoter of most eukaryotic genes; when the appropriate cellular signals are present, RNA polymerase binds to the TATA box, completing the initiation complex
transcription factor : A protein involved in regulating gene expression; usually bound to a cis-regulatory element on the DNA; also known as a regulatory protein or a trans-acting factor