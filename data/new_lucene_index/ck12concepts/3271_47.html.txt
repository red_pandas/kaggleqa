capsid : The protective protein coat that surrounds the DNA or RNA of a virus particle
gene therapy : Process to potentially cure genetic disorders; involves inserting normal genes into cells with mutant genes
genome : The complete set of an organism's hereditary information; may be either DNA or, for many types of virus, RNA; includes both the genes and the non-coding sequences of the DNA/RNA
virologist : A scientist who studies viruses and virus-like agents
virus : A sub-microscopic particle that can infect living cells; contains DNA (or RNA) and can evolve, but lacks other characteristics of living organisms