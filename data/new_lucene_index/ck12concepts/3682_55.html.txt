Most of the elements we know about do not exist freely in nature Sodium cannot be found by itself (unless we prepare it in the laboratory) because it interacts easily with other materials On the other hand, the element helium does not interact with other elements to any extent We can isolate helium from natural gas during the process of drilling for oil
A chemical change produces a different materials than the ones we started with One aspect of the science of chemistry is the study of the changes that matter undergoes If all we had were the elements and they did nothing, life would be very boring (in fact, life would not exist since the elements are what make up our bodies and sustain us)
One type of chemical change (already mentioned) is when two elements combine to form a compound Another type involves the breakdown of a compound to produce the elements that make it up If we pass an electric current through bauxite (aluminum oxide, the raw material for aluminum metal), we get metallic aluminum as a product
Figure 5516 Electrolytic production of aluminum
However, the vast majority of chemical changes involve one compound being transformed into another compound  There are literally millions of possibilities when we take this approach to chemical change New compounds can be made to produce better fabrics that are easier to clean and maintain; they can help preserve food so it doesn’t spoil as quickly; and, we can make new medicines to treat diseases – all made possible by studying chemical change
Summary
A chemical change produces a different materials than the ones we started with
Practice
Questions
Use the link below to answer the following questions:
http://chemistryaboutcom/od/lecturenotesl3/a/chemphyschangeshtm
Where do chemical changes take place? What does a chemical change produce? What are physical changes concerned with?
Review
Questions
What is a chemical change? List three types of chemical changes
Chemical Symbols and Formulas
Define chemical symbol Define chemical formula Give the Latin name for elements that use the Latin name for their symbol
How do chess players monitor their moves in a game?
Suppose you were walking along and noticed a piece of paper on the ground with markings on it  You pick it up and see the paper in the picture above
To most people, these notes are meaningless (maybe they’re a secret spy code)  But to a chess player, these symbols tell the story of a chess game  Each abbreviation describes a chess piece or a move during the game The use of special symbols allows us to “see” the game without having to read a wordy and possibly incomplete description of what happened
Figure 5517 Chess game