Gymnosperms have seeds, but they do not produce fruit Instead, the seeds of gymnosperms are usually found in cones
There are four phyla of gymnosperms:
Conifers Cycads Ginkgoes Gnetophytes
Conifers
Conifers , members of the phylum Coniferophyta , are probably the gymnosperms that are most familiar to you Conifers include trees such as pines, firs, spruces, cedars, and the coastal redwood trees in California, which are the tallest living vascular plants
Conifers have their reproductive structures in cones, but they are not the only plants to have that trait ( Figure below ) Conifer pollen cones are usually very small, while the seed cones are larger Pollen contains gametophytes that produce the male gamete of seed plants The pollen, which is a powder-like substance, is carried by the wind to fertilize the seed cones that contain the female gamete ( Figure below )
Figure 719 A red pine, which bears seeds in cones, is an example of a conifer
Figure 720 The end of a pine tree branch bears the male cones that produce the pollen
Conifers have many uses They are important sources of lumber and are also used to make paper Resins, the sticky substance you might see oozing out of a wound on a pine tree, are collected from conifers to make a variety of products, such as the solvent turpentine and the rosin used by musicians and baseball players
Cycads
Cycads , in the phylum Cycadophyta , are also gymnosperms They have large, finely-divided leaves and grow as short shrubs and trees in tropical regions Like conifers, they produce cones, but the seed cones and pollen cones are always on separate plants ( Figure below ) One type of cycad, the Sago Palm, is a popular landscape plant
 So you can imagine dinosaurs grazing on cycad seeds and roaming through cycad forests
Figure 721 Cycads bear their pollen and seeds in cones on separate plants
Ginkgoes
Ginkgoes , in the phylum Ginkgophyta , are unique because they are the only species left in the phylum Many other species in the fossil record have gone extinct ( Figure below ) The ginkgo tree is sometimes called a "living fossil" because it is the last species from its phylum
One reason the ginkgo tree may have survived is because it was often grown around Buddhist temples, especially in China The ginkgo tree is also a popular landscape tree today in American cities because it can live in polluted areas better than most plants
Ginkgoes, like cycads, has separate female and male plants The male trees are usually preferred for landscaping because the seeds produced by the female plants smell terrible when they ripen
Figure 722 Ginkgo trees are gymnosperms with broad leaves
Gnetophytes
Gnetophytes , in the phylum Gnetophyta , are a very small and unusual group of plants Ephedra is an important member of this group, since this desert shrub produces the ephedrine used to treat asthma and other conditions Welwitschia produces extremely long leaves and is found in the deserts of southwestern Africa ( Figure below )
Figure 723 One type of gnetophyte is Welwitschia 