A polypeptide is a sequence of amino acids between ten and one hundred in length A protein is a peptide that is greater than one hundred amino acids in length Proteins are very prevalent in living organisms Hair, skin, nails, muscles, and the hemoglobin in red blood cells are some of the important parts of your body that are made of different proteins
 Since proteins generally consist of one hundred or more amino acids, the number of amino acid sequences that are possible is virtually limitless
The three-dimensional structure of a protein is very critical to its function This structure can be broken down into four levels The primary structure is the amino acid sequence of the protein The amino acid sequence of a given protein is unique and defines the function of that protein The secondary structure is a highly regular sub-structure of the protein
 An alpha helix consists of amino acids that adopt a spiral shape A beta sheet is alternating rows of amino acids that line up in a side-by-side fashion In both cases, the secondary structures are stabilized by extensive hydrogen bonding between the side chains The interaction of the various side chains in the amino acid, specifically the hydrogen bonding, leads to the adoption of a particular secondary structure
Figure 7914 Secondary structure: alpha helix and beta sheet
The tertiary structure is the overall three-dimensional structure of the protein A typical protein consists of several sections of a specific secondary structure (alpha helix or beta sheet) along with other areas in which a more random structure occurs These areas combine to produce the tertiary structure
Some protein molecules consist of multiple protein subunits The quaternary structure of a protein refers to the specific interaction and orientation of the subunits of that protein Hemoglobin is a very large protein found in red blood cells and whose function is to bind and carry oxygen throughout the bloodstream
 Hemoglobin also contains four iron atoms, located in the middle of each of the four subunits The iron atoms are part of a structure called a porphyrin, shown in red in the figure
Figure 7915 Hemoglobin
Some proteins consist of only one subunit and thus do not have a quaternary structure The Figure below diagrams the interaction of the four levels of protein structure
Figure 7916 The four levels of protein structure
Summary
Peptide and proteins are defined The four levels of protein structure are listed and defined
Practice
Questions
Watch the video at the link below and answer the following questions:
Click on the image above for more content
http://wwwyoutubecom/watch?v=Q7dxi4ob2O4
What is part of what determines how a protein folds? What holds secondary structure together? What holds the tertiary structure together?
Review
Questions
A protein has the following sequence: ser-his-thr-tyr What component of protein structure is this? What do we call the overall three-dimensional shape of a protein? A protein has one subunit Would it have a quaternary structure?
polypeptide: A sequence of amino acids between ten and one hundred in length primary structure: The amino acid sequence of the protein protein: A peptide that is greater than one hundred amino acids in length quaternary structure: The specific interaction and orientation of the subunits of that protein secondary structure: A highly regular sub-structure of the protein
Enzymes
Define enzyme Define active site Describe the process of an enzyme reaction Describe the process by which a competitive inhibitor alters the rate of an enzyme reaction Describe the process by which a non-competitive inhibitor alters the rate of an enzyme reaction Explain the role of cofactors in enzyme reactions
What did he discover?
The first enzyme to be isolated was discovered in 1926 by American chemist James Sumner, who crystallized the protein The enzyme was urease, which catalyzes the hydrolytic decomposition of urea, a component of urine, into ammonia and carbon dioxide
His discovery was ridiculed at first because nobody believed that enzymes would behave the same way that other chemicals did Sumner was eventually proven right and won the Nobel Prize in Chemistry in 1946