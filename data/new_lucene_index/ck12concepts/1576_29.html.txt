To name simple covalent compounds, follow these rules:
Start with the name of the element closer to the left side of the periodic table Follow this with the name of element closer to the right of the periodic table Give this second name the suffix –ide  Use prefixes to represent the numbers of the different atoms in each molecule of the compound
Q: What is the name of the compound that contains three oxygen atoms and two nitrogen atoms?
A: The compound is named dinitrogen trioxide Nitrogen is named first because it is farther to the left in the periodic table than oxygen Oxygen is given the -ide suffix because it is the second element named in the compound The prefix di- is added to nitrogen to show that there are two atoms of nitrogen in each molecule of the compound
In the chemical formula for a covalent compound, the numbers of the different atoms in a molecule are represented by subscripts For example, the formula for the compound named carbon dioxide is CO 2 
Q: What is the chemical formula for dinitrogen trioxide?
A: The chemical formula is N 2 O 3 