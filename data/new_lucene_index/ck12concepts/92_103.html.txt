In physics, a quantity, such as mass, length, or speed that is completely specified by its magnitude and has no direction is called a scalar  A vector , on the other hand, is a quantity possessing both magnitude and direction A vector quantity is typically represented by an arrow-tipped line segment
 The direction of the arrow indicates the direction of the vector Not only can vectors be represented graphically, but they can also be added graphically
For one dimensional vector addition , the first vector is placed on a number line with the tail of the vector on the origin  The second vector is placed with its tail exactly on the arrow head of the first vector  The sum of the two vectors is the vector that begins at the origin and ends at the arrow head of the final added vector
Consider the following two vectors
The red vector has a magnitude of 11 in the positive direction on the number line The blue vector has a magnitude of -3, indicating 3 units in the negative direction on the number line In order to add these two vectors, we place one of the vectors on a number line and then the second vector is placed on the same number line such that its origin is on the arrow head of the first vector
The sum of these two vectors is the vector that begins at the origin of the first vector (the red one) and ends at the arrow head of the second (blue) vector So the sum of these two vectors is the purple vector, as shown below
The vector sum of the first two vectors is a vector that begins at the origin and has a magnitude of 8 units in the positive direction  If we were adding three or four vectors all in one dimension, we would continue to place them head to toe in sequence on the number line
Adding Vectors in Two Dimensions
In the following image, vectors and represent the two displacements of a person who walked 90 m east and then 50 m north We want to add these two vectors to get the vector sum of the two movements
The graphical process for adding vectors in two dimensions is to place the tail of the second vector on the arrow head of the first vector as shown above
The sum of the two vectors is the vector that begins at the origin of the first vector and goes to the ending of the second vector, as shown below
If we are using totally graphic means of adding these vectors, the magnitude of the sum would be determined by measuring the length of the sum vector and comparing it to the original standard We would also use a compass to measure the angle of the summation vector
If we are using calculation means, we can divide 50 m by 90 m and determine inverse tangent of the dividend The result of 2905 indicates the angle of 29° north of east The length of the sum vector can also be determined mathematically by the Pythagorean theorem,   In this case, the length of the hypotenuse would be the square root of (8100 + 2500) or 103 m
If three or four vectors are to be added by graphical means, we would continue to place each new vector head to toe with the vectors to be added until all the vectors were in the coordinate system The sum vector is the vector from the origin of the first vector to the arrowhead of the last vector
Summary
Scalars are quantities, such as mass, length, or speed, that are completely specified by magnitude and have no direction Vectors are quantities possessing both magnitude and direction and can be represented by an arrow; the direction of the arrow indicates the direction of the quantity and the length of the arrow is proportional to the magnitude
 Vectors that are in two dimensions are added geometrically When vectors are added graphically, graphs must be done to scale and answers are only as accurate as the graphing
Practice
Questions
Use this resource to answer the following questions about adding vectors
https://wwwyoutubecom/watch?v=PqThckE1C3I
Click on the image above for more content
What is a resultant? What are the steps necessary to add vectors in two dimensions?
Review
Questions
1 On the following number line, add the vector 75 m/s and the vector -20 m/s
2 On a sheet of graph paper, add a vector that is 40 km due east and a vector that is 30 km due north
scalar: A quantity, such as mass, length, or speed, that is completely specified by its magnitude and has no direction vector: A quantity possessing both magnitude and direction, represented by an arrow the direction ofwhich indicates the direction of the quantity and the length of which is proportional to the magnitude
Resolving Vectors into Axial Components
Describe the independence of perpendicular vectors Resolve vectors into axial components