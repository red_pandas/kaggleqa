Farsightedness, or hyperopia, is the condition in which distant objects are seen clearly, but nearby objects appear blurry It occurs when the eyeball is shorter than normal (see Figure below ) This causes images to be focused in a spot that would fall behind the retina (if light could pass through the retina)
 The lenses focus images farther forward in the eye, so they fall on the retina instead of behind it
Figure 31144
Q: Joey has hyperopia When is he more likely to need his glasses: when he reads a book or when he watches TV?
A: With hyperopia, Joey is farsighted He can probably see the TV more clearly than the words in a book because the TV is farther away Therefore, he is more likely to need his glasses when he reads than when he watches TV