Electricity and magnetism are inextricably linked Under certain conditions, electric current causes a magnetic field Under other conditions, a magnetic field can cause an electric current A moving charged particle creates a magnetic field around it Additionally, when a moving charged particle moves through a different magnetic field, the two magnetic fields will interact
Magnetic Field Around a Current Carrying Wire
Figure 1191
In sketch (a) above, a current is being pushed through a straight wire Small compasses placed around the wire point in a circle, instead of all towards the north pole This demonstrates the presence of a magnetic field around the wire If the current is turned off, the compass points return to pointing north
The current moving in a straight wire produces a circular magnetic field around the wire When using conventional current, the direction of the magnetic field is determined by using the right hand rule  The rule says to curl your right hand around the wire such that your thumb points in the direction of the conventional current
 Note that the right hand rule is for conventional current If you are dealing with an electron flow current, the charges are flowing in the opposite direction, so you must use your left hand That is, curl your left hand around the wire with your thumb pointing in the direction of the electron flow and your fingers will point in the direction of the magnetic field
Charged Particles Moving Through a Magnetic Field
When a charged particle moves through a magnetic field at right angles to the field, the field exerts a force on the charged particle in a different direction
In the case sketched above, an electron is moving downward through a magnetic field The motion of the electron is perpendicular to the magnetic field The force exerted on the electron can be calculated by the equation,
, where B is the strength of the field, q is the charge on the particle and v is the velocity of the particle
,while q is expressed in coulombs and v is expressed in m/s The unit is also known as a Tesla , after the Serbian physicist Nikola Tesla
You can see that the product of these three units is Newtons, the appropriate unit for force
and since amperes are , amps cancels coulombs/second and meters cancels meters The only unit remaining is Newtons
Again, we can determine the direction of the force acting upon the electron using a hand rule Since the electron has a negative charge, the left hand rule is used The fingers of the left hand are pointed in the direction of the magnetic field and the thumb points in the direction of the initial electron movement
 The direction of the magnetic field, the direction of the moving charge, and the direction of the force on the particle are all perpendicular to each other
In most situations, a positive test charged is used, instead of an electron In these circumstances, the right hand rule is used The right hand rule is the same as the left hand rule; the thumb is the direction of initial charge movement, the fingers are the direction of the field, and the palm is the direction of the acting force
In dealing with the relationships that exist between magnetic fields and electric charges, there are both left hand and right hand rules that we use to indicate various directions – directions of fields, directions of currents, directions of motion To avoid errors, it is absolutely vital to know and express whether the system we are observing is using conventional current or electron current
Example Problem: An electron traveling at passes through a uniform magnetic field The electron is moving at right angles to the magnetic field What force acts on the electron?
Solution:
When the current is traveling through a magnetic field while inside a wire, the magnetic force is still exerted but now it is calculated as the force on the wire rather than on the individual charges in the current
The equation for the force on the wire is given as , where B is the strength of the magnetic field, I is the current in amps and L is the length of the wire in and perpendicular to the field
Example Problem: A wire 010 m long carries a current of 50 A The wire is at right angles to a uniform magnetic field The force the field exerts on the wire is 020 N What is the magnitude of the magnetic field?
Solution:
Summary
A moving charged particle creates a magnetic field around it Charge through a wire creates a magnetic field around it, the properties of which can be determined using a right hand rule When a moving charged particle moves through another magnetic field, that field will exert a force on the moving charged particle that can be expressed using 
 When the current is traveling through a magnetic field while inside a wire, the magnetic force is still exerted but now it is calculated as the force on the wire rather than on the individual charges in the current, calculated using 
Practice
Questions
In this video, a wire is attached to a battery so that a current flows through the wire The wire and battery combination is placed in a magnetic field Use this resource to answer the questions that follow
http://wwwyoutubecom/watch?v=yB0qYHkTWJ4
Click on the image above for more content
What happens to the wire when the current begins to flow? What difference would it make if the magnetic field were stronger? What difference would it make if the battery were 30 V instead of 15 V?
Review
Questions
Find the force on a 115 m long wire at right angles to a magnetic field, if the current through the wire is 400 A Find the force on an electron passing through a 050 T magnetic field if the velocity of the electron is  A stream of doubly ionized particles moves at a velocity of perpendicularly to a magnetic field of 0
 What is the magnitude of the force on the particles? A wire 050 m long carrying a current of 80 A is at right angles to a 10 T magnetic field What force acts on the wire? Suppose a magnetic field exists with the north pole at the top of the computer monitor and the south pole at the bottom of the monitor screen
 If an electron passes through this field from ceiling to floor, which way will the path of the electron bend? west east north south toward the ceiling
left hand rule: A rule in electricity for finding the direction of the force of a magnetic field on a negatively charged particle moving through the magnetic field at right angles  Point the fingers in the direction of the field and the thumb in the direction of the moving charge and the palm will indicate the direction of the force on the charge
  Point the fingers in the direction of the field and the thumb in the direction of the moving charge and the palm will indicate the direction of the force on the charge tesla: The tesla (symbol ) is the SI derived unit of magnetic flux density, commonly denoted as , (which is also known as “magnetic field”)
Electric Motors
Explain the design and operation of an electric motor
As gas prices continue to rise, electric cars and hybrids are becoming increasingly popular These cars are certainly a part of our future On the left in the image above is an all-electric vehicle, and on the right is a hybrid vehicle that uses gas part time and electricity part time