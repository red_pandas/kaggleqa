Naming compounds that involve transition metal cations necessitates the use of the Stock system  Consider the binary ionic compound FeCl 3  To simply name this compound iron chloride would be incomplete because iron is capable of forming two ions with different charges The name of any iron-containing compound must reflect which iron ion is in the compound
 Therefore, the charge of the single iron ion must be 3+ The correct name of FeCl 3 is iron(III) chloride, with the cation charge written as the Roman numeral Here are several other examples
The first two examples are both oxides of copper (shown in the Figure below ) The ratio of copper ions to oxide ions determines the name Since the oxide ion is O 2- , the charges of the copper ion must be 1+ in the first formula and 2+ in the second formula
 This means that the tin must carry a 4+ charge, making the name tin(IV) oxide
Figure 603 Copper(I) oxide, a red solid, and copper(II) oxide, a black solid, are different compounds because of the charge of the copper ion
Summary
The Stock system allows the specification of transition metal ionic charge when naming ionic compounds Roman numerals are used to indicate the amount of positive charge on the cation
Practice
Practice naming compounds at the following web site:
http://wwwchemteaminfo/Nomenclature/Binary-Stock-FormulatoNamehtml
Review
Questions
What is the Stock system? For which group of metal ions would we use the Stock system? What does the Roman numeral stand for? Assign a Roman numeral to each of the following cations: Sn 4+ Fe 3+ Co 2+ Pb 4+
Stock system: Allows the specification of transition metal ionic charge when naming ionic compounds
Formulas for Binary Ionic Compounds
Be able to write formulas for binary ionic compounds when given the name Be able to name the binary ionic compound when given the formula
How does shorthand work?
Shorthand was a very popular way of recording speech, especially in dictating letters and in court testimony  Instead of trying to write out all the words, the person taking the dictation would use a set of symbols that represented syllables or words  The pages above show a shorthand version of “A Christmas Carol” written by Charles Dickens
  But knowing shorthand allows you to read this classic story
Different professions also use a type of shorthand in communication to save time  Chemists use chemical symbols in combination to indicate specific compounds  There are two advantages to this approach:
The compound under discussion is clearly described so there can be no confusion about its identity Chemical symbols represent a universal language that all chemists can understand, no matter what their native language is