Vectors
The first new concept introduced here is that of a vector: a scalar magnitude with a direction In a sense, we are almost as good at natural vector manipulation as we are at adding numbers Consider, for instance, throwing a ball to a friend standing some distance away To perform an accurate throw, one has to figure out both where to throw and how hard
 Such a vector (an arrow between the original and final location of an object) is called a displacement:
Vector Components
From the above examples, it should be clear that two vectors add to make another vector Sometimes, the opposite operation is useful: we often want to represent a vector as the sum of two other vectors This is called breaking a vector into its components When vectors point along the same line, they essentially add as scalars
 The lines we pick to break our vectors into components along are often called a basis  Any basis will work in the way described above, but we usually break vectors into perpendicular components, since it will frequently allow us to use the Pythagorean theorem in time-saving ways Specifically, we usually use the and axes as our basis, and therefore break vectors into what we call their and components:
A final reason for breaking vectors into perpendicular components is that they are in a sense independent: adding vectors along a component perpendicular to an original component one will never change the original component, just like changing the -coordinate of a point can never change its -coordinate
Guidance Break the Initial Velocity into its Components
Example 1
A tennis ball is launched above the horizontal at a speed of 70 m/s What are the horizontal and vertical velocity components?
Question: and
Given:
Equation:
Plug n’ Chug:
Answer: