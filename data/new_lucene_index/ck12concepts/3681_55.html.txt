A compound is a substance that contains two or more elements chemically combined in a fixed proportion  The elements carbon and hydrogen combine to form many different compounds  One of the simplest is called methane, in which there are always four times as many hydrogen particles as carbon particles  Methane is a pure substance because it always has the same composition
Recall that the components of a mixture can be separated from one another by physical means  This is not true for a compound  Table salt is a compound consisting of equal parts of the elements sodium and chlorine  Salt cannot be separated into its two elements by filtering, distillation, or any other physical process
  A chemical change is a change that produces matter with a different composition  Many compounds can be decomposed into their elements by heating  When sugar is heated, it decomposes to carbon and water  Water is still a compound, but one which cannot be broken down into hydrogen and oxygen by heating
The properties of compounds are generally very different than the properties of the elements from which the compound is formed  Sodium is an extremely reactive soft metal that cannot be exposed to air or water  Chlorine is a deadly gas  The compound sodium chloride is a white solid which is essential for all living things ( Figure below )
Figure 5515 (A) Sodium is so reactive that it must be stored under oil (B) Chlorine is a poisonous yellow-green gas (C) Salt crystals, a compound of sodium and chlorine
Summary
A compound is a substance that contains two or more elements chemically combined in a fixed proportion A chemical change is a change that produces matter with a different composition
Practice
Questions
Watch the video at the link below to answer the following questions:
Click on the image above for more content
http://wwwyoutubecom/watch?v=9YPvwsDeLUo
What lists all the elements we know about? What is the compound made out of two hydrogen atoms and one oxygen atom? What is the compound made out of one carbon atom and two oxygen atoms?
Review
Questions
What is a compound? How is a compound different from an element? What is a chemical change?
chemical change: A change that produces matter with a different composition compound: A substance that contains two or more elements chemically combined in a fixed proportion
Chemical Change
Define chemical change Give examples of chemical changes
Do you like to cook?
Cooking is a valuable skill that everyone should have Whether it is fixing a simple grilled cheese sandwich or preparing an elaborate meal, cooking demonstrates some basic ideas in chemistry When you bake bread, you mix some flour, sugar, yeast, and water together After baking, this mixture has been changed to form bread, another substance that has different characteristics and qualities from the original materials