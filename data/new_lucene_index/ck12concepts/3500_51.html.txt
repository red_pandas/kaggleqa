dihybrid : Offspring that result from a dihybrid cross
dihybrid cross : A cross in which the inheritance of two characteristics are tracked at the same time
genetics : The branch of biology that focuses on heredity in organisms; the study of heredity
Law of Independent Assortment : States that the inheritance of one trait will not affect the inheritance of another trait
linked genes : Genes that are inherited together because they are located on the same chromosome
locus (plural, loci ): The specific location of a gene or DNA sequence on a chromosome