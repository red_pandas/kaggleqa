Down syndrome : Trisomy 21; individuals often have some degree of mental retardation, some impairment of physical growth, and a specific facial appearance
karyotype : Depicts, usually in a photograph, the chromosomal complement of an individual, including the number of chromosomes and any large chromosomal abnormalities; usually taken during metaphase of mitosis
Klinefelter’s syndrome : Syndrome due to the presence of one or more extra copies of the X chromosome in a male's cells
nondisjunction : The failure of replicated chromosomes to separate during anaphase II of meiosis
polygenic traits : Traits that are due to the actions of more than one gene; often also involves interactions with the environment
Triple X syndrome : Syndrome that results from an extra copy of the X chromosome in each of a female's cells; Triple X syndrome
trisomy : An abnormal condition where there are three copies, instead of two, of a particular chromosome within a cell
trisomy 21 : Down syndrome; individuals often have some degree of mental retardation, some impairment of physical growth, and a specific facial appearance
Turner’s syndrome : Syndrome that results from a female's cells with only one X chromosome; no other sex chromosome is present (XO)