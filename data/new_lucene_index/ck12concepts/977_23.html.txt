More than 13 million species of organisms have been cataloged Many millions of species have not been discovered yet Scientists estimate that there may be about 87 million species in all Why does the planet have so much biological diversity? (Biological diversity is shortened to biodiversity ) The tremendous variety of organisms is due to the tremendous numbers of habitats that they have evolved to fill
Ways to Live in the Environment
Each organism has the ability to survive in a specific environment Dry desert environments are difficult to live in Desert plants have special stems and leaves to conserve water ( Figure below ) Animals have other ways to live in the desert The Namib Desert receives only 15 inches of rainfall each year
 How do the beetles get enough water to survive? Early morning fog deposits water droplets The droplets collect on a beetle's wings and back ( Figure below ) The beetle tilts its rear end up When the droplet is heavy enough, it slides forward It lands in the beetle’s mouth
Figure 231 Aloe vera plants like these ones have fat, waxy leaves that allow them to conserve water
Figure 232 The Namib Desert Beetle has bumps on its back for collecting water
Figure 233 Stoats change color in the winter, from brown to white, so that they can hide in the snow
Figure 234 Crowned cranes have plumage that helps them attract a mate
Getting Food and Being Food (Or Not)
Organisms must be able to get food and avoid being food Hummingbirds have long, thin beaks that help them drink nectar from flowers Some flowers are tubular Different species of flowers have tubes of different lengths Different species of hummingbirds have different lengths of beaks A particular hummingbird species has evolved to feed from one or a few species of flowers
The battle between needing food and being food plays out in the drama between lions and zebras When a herd of zebras senses a lion, the animals run away The zebras’ dark stripes confuse the lions It becomes hard for them to focus on just one zebra The zebras may get away
 A lion may be able to get a zebra, maybe one that's old or sick