binary fission : Asexual reproduction in prokaryotic organisms; produces two identical cells
cell : The basic unit of structure and function of all living organisms
cell cycle : A repeating series of events, during which the eukaryotic cell carries out its necessary functions, including metabolism, cellular growth, DNA replication and cell division, resulting in two genetically identical daughter cells
cell division : The process of cell formation from the division of older cells
chromosome : The coiled structure of DNA and histone proteins; allows for the precise separation of replicated DNA; forms during prophase of mitosis and meiosis
cytokinesis : Division of the cytoplasm, forming two daughter cells
DNA replication : The process of copying DNA prior to cell division (eukaryotes) or reproduction (prokaryotes)
haploid : The state of a cell containing one set of chromosomes; in human gametes, one set is 23 chromosomes, n 
meiosis : A type of cell division that halves the number of chromosomes; forms gametes
mitosis : The division of the nucleus into two genetically identical nuclei
zygote : A fertilized egg; the first cell of a new organism