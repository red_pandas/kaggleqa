Global winds are winds that occur in belts that go all around the planet ( Figure below ) Like local winds, global winds are caused by unequal heating of the atmosphere
Figure 1929 Global winds occur in belts around the globe
Heating and Global Winds
Earth is hottest at the Equator and gets cooler toward the poles The differences in heating create huge convection currents in the troposphere At the Equator, for example, warm air rises up to the tropopause When it can’t rise any higher, it flows north or south
By the time the moving air reaches 30°N or °S latitude, it has cooled somewhat The cool air sinks to the surface Then it flows over the surface back to the Equator Other global winds occur in much the same way There are three enormous convection cells north of the Equator and three south of the Equator
Global Winds and the Coriolis Effect
Earth is spinning as air moves over its surface This causes the Coriolis effect Winds blow on a diagonal over the surface due to Coriolis effect From which direction do the northern trade winds blow?
Without the Coriolis effect the global winds would blow north to south or south to north But the Coriolis effect makes them blow northeast to southwest or the reverse in the Northern Hemisphere The winds blow northwest to southeast or the reverse in the Southern Hemisphere
The wind belts have names The Trade Winds are nearest the Equator The next belt is the westerlies Finally are the polar easterlies The names are the same in both hemispheres
Jet Streams
Jet streams are fast-moving air currents high in the troposphere They are also the result of unequal heating of the atmosphere Jet streams circle the planet, mainly from west to east The strongest jet streams are the polar jets PIctured below is the northern polar jet ( Figure below )
Figure 1930 This jet stream helps planes fly quickly from west to east over North America How do you think it affects planes that fly from east to west?