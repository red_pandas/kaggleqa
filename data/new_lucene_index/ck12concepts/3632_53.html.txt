biotechnology : Technology based on biological applications
chain-termination method : The method of DNA sequencing using dideoxynucleotide triphosphates (ddNTPs) as DNA chain terminators
DNA sequencing : The method of determining the order of the DNA nucleotide bases
DNA technology : Biotechnology focusing on DNA-based technology
gel electrophoresis : An analytical technique used to separate DNA fragments by size and charge; can also be used to separate RNA and proteins
Human Genome Project : A project to understand the genetic make-up of the human species by determining the DNA sequence of the human genome and the genome of a few model organisms
recombinant DNA : DNA engineered through the combination of two or more DNA strands; combines DNA sequences which would not normally occur together