Two unique properties of liquids are surface tension and viscosity Surface tension is a force that pulls particles at the exposed surface of a liquid toward other liquid particles Surface tension explains why water forms droplets, like the water droplet that has formed on the leaky faucet pictured in the Figure below 
com/5668221/an-experiment-with-soap-water-pepper-and-surface-tension 
Figure 2826 Water drips from a leaky faucet
Viscosity is a liquid’s resistance to flowing You can think of it as friction between particles of liquid Thicker liquids are more viscous than thinner liquids For example, the honey pictured in the Figure below is more viscous than the vinegar You can learn more about viscosity at this URL: http://chemed
wiscedu/chempaths/GenChem-Textbook/Viscosity-840html 
Figure 2827
Q: Which liquid do you think is more viscous: honey or chocolate syrup?
A: The viscosity of honey and chocolate syrup vary by brand and other factors, but chocolate syrup generally is more viscous than honey