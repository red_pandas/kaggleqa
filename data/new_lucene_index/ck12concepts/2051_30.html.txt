Other levers change force or distance in different ways than a hammer removing a nail How a lever changes force or distance depends on the location of the input and output forces relative to the fulcrum The input force is the force applied by the user to the lever The output force is the force applied by the lever to the object
 The Table below describes the three classes You can see animations of the classes, as well as more examples, at this URL:
http://wwwenchantedlearningcom/physics/machines/Leversshtml
Click on the image above to view the table
Δ = fulcrum I = input force O = output force
The Table above includes the ideal mechanical advantage of each class of lever The mechanical advantage is the factor by which a machine changes the input force The ideal mechanical advantage is the increase or decrease in force that would occur if there were no friction to overcome in the use of the machine
Q : Which class of lever is a hammer when it is used to pry a nail out of a board? What is its mechanical advantage?
A : To pry a nail out of a board, the fulcrum is located between the input and output forces Therefore, when a hammer is used in this way it is a first class lever The fulcrum is closer to the output force than the input force, so the mechanical advantage is > 1