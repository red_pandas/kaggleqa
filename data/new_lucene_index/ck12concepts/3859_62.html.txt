As the saying goes, all rules are made to be broken  When it comes to the octet rule, that is true  Exceptions to the octet rule fall into one of three categories: (1) an incomplete octet , (2) odd-electron molecules , and (3) an expanded octet
Incomplete Octet
In some compounds, the number of electrons surrounding the central atom in a stable molecule is fewer than eight  Beryllium is an alkaline earth metal and so may be expected to form ionic bonds  However, its very small size and somewhat higher ionization energy compared to other metals actually lead to beryllium forming primarily molecular compounds
  The Lewis structure of gaseous beryllium hydride (BeH 2 ) consists of two single covalent bonds between Be and H (see Figure below )
Figure 6215 Beryllium hydride
Boron and aluminum, with three valence electrons, also tend to form covalent compounds with an incomplete octet  The central boron atom in boron trichloride (BCl 3 ) has six valence electrons as shown in the Figure below 
Figure 6216 Boron trichloride
Odd-Electron Molecules
There are a number of molecules whose total number of valence electrons is an odd number  It is not possible for all of the atoms in such a molecule to satisfy the octet rule  An example is nitrogen dioxide (NO 2 )  Each oxygen atom contributes six valence electrons and the nitrogen atom contributes five for a total of seventeen
Figure 6217 The Lewis structure for nitrogen dioxide, an odd electron molecule
Expanded Octets
Atoms of the second period cannot have more than eight valence electrons around the central atom  However, atoms of the third period and beyond are capable of exceeding the octet rule by having more than eight electrons around the central atom  Starting with the third period, the d sublevel becomes available, so it is possible to use these orbitals in bonding, resulting in an expanded octet
Phosphorus and sulfur are two elements that react with halogen elements and make stable compounds with expanded octets  In phosphorus pentachloride, the central phosphorus atom makes five single bonds to chlorine atoms and as a result has ten electrons surrounding it (see Figure below )  In sulfur hexafluoride, the central sulfur atom has twelve electrons from its six bonds to fluorine atoms (see Figure below )
Figure 6218 Phosphorus pentachloride Left image: Lewis structure Right image: molecular model
Figure 6219 Sulfur hexafluoride Left image: Lewis structure Right image: molecular model
Summary
Exceptions exist to the rules for covalent bonding These exceptions apply to atoms whose electrons will not accommodate the normal octet rule
Practice
Questions
Use the link below to answer the following questions:
http://chemistryaboutcom/od/workedchemistryproblems/a/How-To-Draw-A-Lewis-Structure-Octet-Exceptionhtm
What is the first step in drawing the Lewis structure? What is the next step? Which atom will be the central atom? Do you complete the octets for the non-central atoms?
Review
Questions
What is an incomplete octet? What is an odd-electron molecule? Why are there extra electrons in the expanded octet?
expanded octet: Use of d sublevel in bonding as well as other sublevels incomplete octet: The number of electrons surrounding the central atom in a stable molecule is fewer than eight odd-electron molecules: Molecules whose total number of valence electrons is an odd number
Bond Energy
Define bond energy Describe the relationship between reactivity and bond energy
How does smog form?
We know that nitrogen makes up about 79% of the gases in the air and that this gas is chemically very inert  However, we also know that a major contributor to the production of  smog is a mixture of nitrogen compounds collectively referred to as NO x   Due to the high combustion temperatures in modern car engines (brought about by the need for better fuel efficiency), we make more NO x that will react with other materials to create smog