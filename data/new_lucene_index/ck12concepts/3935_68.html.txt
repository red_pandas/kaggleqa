Water is a simple molecule consisting of one oxygen atom bonded to two different hydrogen atoms  Because of the higher electronegativity of the oxygen atom, the bonds are polar covalent ( polar bonds )  The oxygen atom attracts the shared electrons of the covalent bonds to a significantly greater extent than the hydrogen atoms
  The molecule adopts a bent structure because of the two lone pairs of electrons on the oxygen atom  The H-O-H bond angle is about 105°, slightly smaller than the ideal 1095° of an sp 3 hybridized atomic orbital
Figure 681 The water molecule, visualized three different ways: ball-and-stick model, space-filling model, and structural formula with partial charges
The bent shape of the water molecule is critical because the polar O-H bonds do not cancel one another and the molecule as a whole is polar Figure below illustrates the net polarity of the water molecule  The oxygen is the negative end of the molecule, while the area between the hydrogen atoms is the positive end of the molecule
Figure 682 Water is a polar molecule, as greater electron density is found around the more electronegative oxygen atom
Polar molecules attract one another by dipole-dipole forces as the positive end of one molecule is attracted to the negative end of the nearby molecule  In the case of water, the highly polar O-H bonds results in very little electron density around the hydrogen atoms  Each hydrogen atom is strongly attracted to the lone-pair electrons on an adjacent oxygen atom
Figure 683 A hydrogen bond is the attraction between a lone pair of electrons on the oxygen atom of one molecule and the electron-deficient hydrogen atom of a nearby molecule
Because each oxygen atom has two lone pairs, it can make hydrogen bonds to the hydrogen atoms of two separate other molecules  The Figure below shows the result – an approximately tetrahedral geometry around each oxygen atom consisting of two covalent bonds and two hydrogen bonds
Figure 684 As a result of two covalent bonds and two hydrogen bonds, the geometry around each oxygen atom is approximately tetrahedral
Summary
Water is a molecular compound consisting of polar molecules that have a bent shape The oxygen atom acquires a partial negative charge while the hydrogen atom acquires a partial positive charge
Practice
Questions
Use the link below to answer the following questions:
http://wwwlsbuacuk/water/moleculehtml
What is the H-O-H bond angle? How far is the center of each H atom from the center of the O atom?
Review
Questions
What type of bond exists in a water molecule? Which part of the molecule has a partial positive charge? Which part of the molecule as a partial negative charge? How do water molecules interact with one another?
electronegativity: A measure of the ability of an atom to attract the electrons when the atom is part of a compound polar bond: A covalent bond in which electrons are shared unequally between two electrons
Structure of Ice
Describe the structure of ice Explain why ice is less dense that liquid water
Have you ever been ice skating?
Ice is an interesting and useful material  It can be used to cool food and keep it fresh  It can provide recreation, such as ice-skating  Ice can do great damage when it freezes – roads can buckle, houses can be damaged, water pipes can burst   All this happens because of a unique property of water and ice