Many factors influence how quickly a liquid evaporates They include:
temperature of the liquid A cup of hot water will evaporate more quickly than a cup of cold water exposed surface area of the liquid The same amount of water will evaporate more quickly in a wide shallow bowl than in a tall narrow glass presence or absence of other substances in the liquid
 air movement Clothes on a clothesline will dry more quickly on a windy day than on a still day concentration of the evaporating substance in the air Clothes will dry more quickly when air contains little water vapor
With the interactive animation at the URL below, you can explore how three of these factors affect the rate of evaporation of water
http://techalivemtuedu/meec/module01/EvaporationandTranspirationhtm