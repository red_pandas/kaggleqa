A wedge is simple machine that consists of two inclined planes, giving it a thin end and thick end, as you can see in the Figure below  A wedge is used to cut or split apart objects Force is applied to the thick end of the wedge, and the wedge, in turn, applies force to the object along both of its sloping sides
Figure 3085
A knife is another example of a wedge In the Figure below , a knife is being used to chop tough pecans The job is easy to do with the knife because of the wedge shape of the blade The very thin edge of the blade easily enters and cuts through the pecans
http://wwwhistoryforkidsorg/scienceforkids/physics/machines/wedgehtm
Figure 3086