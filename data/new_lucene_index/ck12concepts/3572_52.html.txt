gene : A segment of DNA that contains the information necessary to encode an RNA molecule or a protein
gene expression : The process by which the information in a gene is "decoded" to produce a functional gene product, such as an RNA molecule or a polypeptide/protein molecule
protein synthesis : The process in which cells make proteins; includes transcription of DNA and translation of mRNA
Ribonucleic acid (RNA) : Single-stranded nucleic acid; involved in protein synthesis
transcription : The process of making mRNA from the information in the DNA (gene) sequence