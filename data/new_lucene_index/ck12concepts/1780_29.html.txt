For an atom of one element to change into a different element, the number of protons in its nucleus must change That’s because each element has a unique number of protons For example, lead atoms always have 82 protons, and gold atoms always have 79 protons
Q: So how can one element change into another?
A: The starting element must be radioactive, and its nuclei must gain or lose protons