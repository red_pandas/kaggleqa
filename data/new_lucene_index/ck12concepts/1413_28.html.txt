Although atoms are very tiny, they consist of even smaller particles Three main types of particles that make up all atoms are:
protons, which have a positive electric charge electrons, which have a negative electric charge neutrons, which are neutral in electric charge
The model in the Figure below shows how these particles are arranged in an atom The particular atom represented by the model is helium, but the particles of all atoms are arranged in the same way At the center of the atom is a dense area called the nucleus, where all the protons and neutrons are clustered closely together
 Helium has two protons and two neutrons in its nucleus and two electrons moving around the nucleus Atoms of other elements have different numbers of subatomic particles, but the number of protons always equals the number of electrons This makes atoms neutral in charge because the positive and negative charges “cancel out
Figure 2855 Model of a helium atom
Q: Lithium has three protons, four neutrons, and three electrons Sketch a model of a lithium atom, similar to the model above for helium
A: Does your sketch resemble the model in the Figure below ? The model has three protons (blue) and four neutrons (gray) in the nucleus, with three electrons (red) moving around the nucleus You can see an animated model of a lithium atom at this URL: http://ippexppplgov/interactive/matter/elementshtml 
Figure 2856
Q: All atoms of carbon have six protons How many electrons do carbon atoms have?
A: Carbon atoms must have six electrons to “cancel out” the positive charges of the six protons That’s because atoms are always neutral in electric charge