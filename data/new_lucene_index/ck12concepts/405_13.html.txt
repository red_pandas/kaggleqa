Scientists collect evidence to test a hypothesis The evidence may refute the hypothesis In that case, it will be thrown out The evidence may support the hypothesis The scientists will then gather more evidence The scientists will accept the hypothesis if: (1) There is no significant evidence to refute the hypothesis
 The hypothesis may then become a theory
A scientific theory is supported by many types of evidence It is a synthesis of evidence from many experiments and observations A theory has no major inconsistencies A theory is a simple model of reality It is simpler than the phenomenon itself A theory explains a phenomenon Scientists can use a theory to make predictions
A theory is constantly tested If some data does not agree with the theory, the theory can be revised If the theory cannot match the data, it is thrown out That being said, sometimes a theory is well-established; it has a lot of evidence to back it up A well-established theory is unlikely to be overthrown
 But science does not prove anything beyond a shadow of a doubt
Watch this video to understand the difference between hypothesis and theory: http://wwwyoutubecom/watch?v=jdWMcMW54fA (6:39)
Click on the image above for more content
Three Essential Theories
The three theories below are essential in Earth science Each accounts for an enormous amount of data Each is supported by many lines of evidence All can be used to make predications As new evidence arises, any of these theories may need to be altered But none of these three are likely ever to be disproved
The Theory of Evolution
Evolution means change over time Darwin’s theory of evolution says that organisms change over time ( Figure below ) Evolution is seen in the fossil record It is seen in the way organisms develop Evolution is evident in the geographic locations where organism are found It is evident in the genes of living organisms
 The organism that is best suited to its environment is most likely to survive Evolution is described in the chapter Life on Earth 
Figure 138 The theory of evolution maintains that modern humans evolved from ape-like ancestors
The Theory of Plate Tectonics
Plate tectonics theory says that slabs of continents move around on Earth's surface The mechanism for that movement is seafloor spreading Plate tectonics explain many things about Earth: (1) geological activity, why it happens where it does; (2) natural resources, why many are found where they are; and (3) the past and future, what happened in the past and what will happen in the future
The Theory of Climate Change
The theory of climate change is very new In fact, scientists don't even call it a theory But it meets the requirements These are the things we know: (1) average global temperatures are rising, (2) greenhouse gases trap heat in the atmosphere, (3) CO 2 is released into the atmosphere when fossil fuels are burned, (4) CO 2 is a greenhouse gas, (5) more CO 2 in the atmosphere traps more heat so global temperature is rising
 The theory is very effective at predicting future events, which are already taking place This idea will be explored in detail in the chapter Atmospheric Processes 
Laws
Many people think that any idea that is completely accepted in science is a law But that is not true In science, a law is something that always applies under the same conditions A law explains a fairly simple phenomenon A theory is much more complex: it tells you why something happens
 For example, if you hold something above the ground and let go, it will fall This phenomenon is recognized by the law of gravity ( Figure below ) Some people say that evolution is "just a theory" But for a complex explanation like evolution, theory is the right word
Figure 139 Skydiving is possible because of the law of gravity