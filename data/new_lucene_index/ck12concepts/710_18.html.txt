Oil spills are another source of ocean pollution To get at oil buried beneath the seafloor, oil rigs are built in the oceans These rigs pump oil from beneath the ocean floor Huge ocean tankers carry oil around the world If something goes wrong with a rig on a tanker, millions of barrels of oil may end up in the water
 Some of the oil will wash ashore This oil may destroy coastal wetlands and ruin beaches
The Gulf of Mexico Oil Spill
The figure below shows workers trying to clean up oil on a Louisiana beach ( Figure below ) The oil washed ashore after a deadly oil rig explosion in the Gulf of Mexico in 2010
Figure 1867 After an oil rig explosion, hundreds of miles of beaches looked like this one Cleaning them up was a huge task
New drilling techniques allow oil companies to drill in deeper waters than ever before In April 2010 a rig in the Gulf of Mexico exploded Eleven workers were killed and 17 injured When the drill rig sank, a pipe was disconnected and oil gushed into the Gulf Three months later the well was capped
9 million barrels had entered the Gulf, about 16 times more oil than the largest oil spill to date
Cleanup
Once the oil is in the water, there are three ways to try to clean it:
Removal: Corral and then burn the oil Containment: Use containment booms to trap the oil Dispersal: Use chemicals to get the oil to disperse, called chemical dispersants  Some scientists think that the harm to the environment from the dispersants is as great as the harm from the oil
The total effect of the oil spill on the environment of the Gulf is not yet known Oil is found in the sediments on the seafloor Many people who fish or are involved in Gulf tourism were also impacted Studies of the effects of the oil spill on people and animals will continue for many years