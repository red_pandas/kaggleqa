allele : An alternative form or different version of a gene
GenBank : The NIH genetic sequence database, a collection of all publicly available DNA sequences
human genome : All of the hereditary information encoded in the DNA of homo sapiens , including the genes and non-coding sequences
Human Genome Project (HGP) : A project to understand the genetic make-up of the human species by determining the DNA sequence of the human genome and the genome of a few model organisms
National Center for Biotechnology Information (NCBI) : US government-funded national resource for molecular biology information; part of the United States National Library of Medicine on the NIH campus
National Human Genome Research Institute (NHGRI) : One of 27 institutes and centers that make up the NIH; devoted to improving human health through human genetics and genomics basic and clinical research
National Institutes of Health (NIH) : The United States' medical research agency; supporting scientific studies world-wide; composed of 27 institutes and centers
single nucleotide polymorphisms (SNP) : A DNA sequence variation occurring when a single nucleotide differs between members of a species or paired chromosomes in an individual