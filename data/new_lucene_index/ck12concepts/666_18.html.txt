The types of glaciers are:
Continental glaciers are large ice sheets that cover relatively flat ground These glaciers flow outward from where the greatest amounts of snow and ice accumulate Alpine (valley) glaciers flow downhill Ice accumulates near the top of a mountain and then travels down existing valleys ( Figure below )
Figure 1830 An alpine glacier in the Swiss Alps Where do you think the word "alpine" came from?
Ice caps are large glaciers that cover a larger area than just a valley An ice cap can bury an entire mountain range or region ( Figure below ) Glaciers come off of ice caps into valleys
Figure 1831 The continent of Antarctica is covered with an ice cap