An individual measurement may be accurate or inaccurate, depending on how close it is to the true value  Suppose that you are doing an experiment to determine the density of a sample of aluminum metal  The accepted value of a measurement is the true or correct value based on general agreement with a reliable reference
70 g/cm 3   The experimental value of a measurement is the value that is measured during the experiment  Suppose that in your experiment you determine an experimental value for the aluminum density to be 242 g/cm 3   The error of an experiment is the difference between the experimental and accepted values
If the experimental value is less than the accepted value, the error is negative  If the experimental value is larger than the accepted value, the error is positive  Often, error is reported as the absolute value of the difference in order to avoid the confusion of a negative error  The percent error is the absolute value of the error divided by the accepted value and multiplied by 100%
To calculate the percent error for the aluminum density measurement, we can substitute the given values of 245 g/cm 3 for the experimental value and 270 g/cm 3 for the accepted value
If the experimental value is equal to the accepted value, the percent error is equal to 0  As the accuracy of a measurement decreases, the percent error of that measurement rises
Summary
Definitions of accepted value and experimental value are given Calculations of error and percent error are demonstrated
Practice
Read the material at the link below and then do “Your Turn” questions to see how well you did
http://wwwmathsisfuncom/numbers/percentage-errorhtml
Review
Questions
Define accepted value Define experimental value What happens as the accuracy of the measurement decreases?
accepted value: The true or correct value based on general agreement with a reliable reference error: The difference between the experimental and accepted values experimental value: The value that is measured during the experiment percent error: The absolute value of the error divided by the accepted value and multiplied by 100%
Measurement Uncertainty
Describe uncertainty in measurements
How do police officers identify criminals?
After a bank robbery has been committed, police will ask witnesses to describe the robbers They will usually get some answer such as “medium height” Others may say “between 5 foot 8 inches and 5 foot 10 inches” In both cases, there is a significant amount of uncertainty about the height of the criminals