Plant-like protists are called algae (singular, alga) They are a large and diverse group Some algae, the diatoms , are single-celled Others, such as seaweed , are multicellular (see Figure below )
Figure 395 Diatoms are single-celled algae Other forms of algae are multicellular
Why are algae considered plant-like? The main reason is that they contain chloroplasts and produce food through photosynthesis  However, they lack many other structures of true plants For example, algae do not have roots, stems, or leaves Some algae also differ from plants in being motile They may move with pseudopods or flagella
Ecology of Algae
Algae play significant roles as producers in aquatic ecosystems Microscopic forms live suspended in the water column They are the main component of phytoplankton  As such, they contribute to the food base of most marine ecosystems
Multicellular seaweeds called kelp may grow as large as trees They are the food base of ecosystems called kelp forests (see Figure below ) Kelp forests are found throughout the ocean in temperate and arctic climates They are highly productive ecosystems
Figure 396 Kelp Forest This kelp forest supports a large community of many other types of organisms
Classification of Algae
Types of algae include red and green algae, and euglenids , and dinoflagellates (see Table below for examples) Scientists think that red and green algae evolved from endosymbiotic relationships with cyanobacteria Their chloroplasts have two membranes because the cell membranes of the cyanobacteria became additional plasma membranes of the chloroplasts
 This is why their chloroplasts have three membranes Differences in the types of chlorophyll in the four types of algae also support the hypothesized evolutionary relationships
Click on the image above to view the table
Reproduction of Algae
Algae have varied life cycles Two examples are shown in Figure below  Both cycles include phases of asexual reproduction (haploid, n ) and sexual reproduction (diploid, 2n ) Why go to so much trouble to reproduce? Asexual reproduction is fast, but it doesn’t create new genetic variation Sexual reproduction is more complicated and risky, but it creates new gene combinations
 Rapid population growth (asexual reproduction) is adaptive when conditions are favorable Genetic variation (sexual reproduction) helps ensure that some organisms will survive if the environment changes
Figure 397 Life Cycles of Algae: Zygotic Meiosis (A), Gametic Meiosis (B) and Sporic Meiosis (C) In life cycle A (left), diploid (2n) zygotes result from fertilization and then undergo meiosis to produce haploid (n) gametes The gametes undergo mitosis and produce many additional copies of themselves How are life cycles B and C different from life cycle A?
KQED: Algae Power
QUEST explores the potential of algae-–once considered nothing more than pond scum–-to become the fuel of the future Entrepreneurs from throughout California are working to create the next generation of biofuels from algae But will you ever be able to run your car off it? See http://wwwkqedorg/quest/television/algae-power for additional information
Click on the image above for more content