; The total energy does not change in closed systems
; The total momentum does not change in closed systems
Guidance When attacking these problems remember that in a collision, energy is transferred out of the system to the environment, via heat, sound, breaking of molecular bonds, etc Thus this is not a closed system during a collision of two objects Thus you cannot use energy conservation immediately before the collision to immediately after the collision
 Thus break the problem into two parts For the collision part use momentum conservation You can use energy conservation for the other parts, where it is conserved
Example 1
Click on the image above for more content