A voltaic cell uses a spontaneous redox reaction to generate an electric current It is also possible to do the opposite When an external source of direct current is applied to an electrochemical cell, a reaction that is normally nonspontaneous can be made to proceed Electrolysis is the process in which electrical energy is used to cause a nonspontaneous chemical reaction to occur
An electrolytic cell is the apparatus used for carrying out an electrolysis reaction In an electrolytic cell, electric current is applied to provide a source of electrons for driving the reaction in a nonspontaneous direction In a voltaic cell, the reaction goes in a direction that releases electrons spontaneously In an electrolytic cell, the input of electrons from an external source forces the reaction to go in the opposite direction
Figure 769 Zn/Cu cell
The spontaneous direction for the reaction between Zn and Cu is for the Zn metal to be oxidized to Zn 2+ ions, while the Cu 2+ ions are reduced to Cu metal This makes the zinc electrode the anode and the copper electrode the cathode When the same half-cells are connected to a battery via the external wire, the reaction is forced to run in the opposite direction
The standard cell potential is negative, indicating a nonspontaneous reaction The battery must be capable of delivering at least 110 V of direct current in order for the reaction to occur Another difference between a voltaic cell and an electrolytic cell is the signs of the electrodes In a voltaic cell, the anode is negative and the cathode is positive
 The cathode is therefore negative Electrons still flow through the cell form the anode to the cathode
Summary
The function of an electrolytic cell is described Reactions illustrating electrolysis are given
Practice
Questions
Watch the video at the link below and answer the following questions:
Click on the image above for more content
http://wwwyoutubecom/watch?v=y4yYF8gSHdA
What was the source of electricity? What was the purpose of the steel attached to an electrode? What is used to help carry the electric current?
Review
Questions
What would be the products of a spontaneous reaction between Zn/Zn 2+ and Cu/Cu 2+ ? How do we know that the reaction forming Cu 2+ is not spontaneous? What would be the voltage for the reaction where Zn metal forms Zn 2+ ?
electrolysis: The process in which electrical energy is used to cause a nonspontaneous chemical reaction to occur electrolytic cell: The apparatus used for carrying out an electrolysis reaction
Electrolysis of Water
Describe the experimental set-up for the electrolysis of water Write equations for the reactions involved in the process
More energy from the sun?
With fossil fuels becoming more expensive and less available, scientists are looking for other energy sources Hydrogen has long been considered an ideal source, since it does not pollute when it burns The problem has been finding ways to generate hydrogen economically One new approach that is being studied is photoelectrolysis – the generation of electricity using photovoltaic cells to split water molecules