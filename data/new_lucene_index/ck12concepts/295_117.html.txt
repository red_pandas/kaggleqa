Electric Current
Remember that it often requires work to force electrons into a specific location If we have two conducting spheres and we have forced excess electrons onto one of the spheres by doing work on the electrons, then that sphere, and those electrons, will have a higher potential energy than those on the uncharged sphere
 That is, electrons will flow from the high potential energy position to the lower potential energy position The flow will continue until the electrons on the two spheres have the same potential energy A flow of charged particles such as this is called an electric current
It is possible for an electric current to be either a flow of positively charged particles or negatively charged particles In gases, both positive and negative ions can flow The difficulty of freeing protons, however, makes it extremely rare to have an electric current of positive particles in solid conductors
Common Misconceptions
It is easy to assume that current is the flow of positive charges In fact, when the conventions of positive and negative charge were invented two centuries ago, it was assumed that positive charge flowed through a wire In reality, however, we know now that the flow of positive charge is actually a flow of negative charge in the opposite direction
Today, even though we know it is not correct, we still use the historical convention of positive current flow when discussing the direction of a current Conventional current , the current we commonly use and discuss, is the direction positive current would flow When we want to speak of the direction of electron flow, we will specifically state that we are referring to electron flow
Electric current flows from positions of higher potential energy to positions of lower potential energy Electrons acquire higher potential energy from an electron pump that does work on the electrons, moving them from positions of lower PE to positions of higher PE  Electrons in galvanic cells (several cells together comprise a battery) have higher potential energy at one terminal of the battery that at the other
 When the two terminals of the battery are connected to each other via a conducting wire, the electric current will travel from the terminal with higher potential energy to that with lower potential energy This setup is the most simple of electric circuits 
Electric Circuits
An electric circuit is any closed loop that goes from one battery terminal to the other and allows current to flow through it A relatively simple circuit is shown in the image below The charges move from the higher potential energy terminal on the battery, through the light bulb, through the switch, and back to the lower potential energy terminal on the battery
A circuit consists of a battery, or a charge pump, which increases the potential energy of the charges, and one or more devices that decrease the potential energy As the potential energy is reduced, it is converted into some other form of energy In the image above, the device that decreases the charges' potential energy is the light bulb; the excess energy is converted into light energy
 Other resistors include motors, which convert energy into kinetic energy, and heaters, which convert it into thermal energy
The charges in the circuit can neither be created nor destroyed, nor can they pile up in one spot The charged particles moving through the circuit move the same everywhere in the circuit If one coulomb of charge leaves the charge pump, then one coulomb of charge moves through the light, and one coulomb of charge moves through the switch
 That is, the increase in potential energy through the charge pump is exactly equal to the potential drop through the light If the generator (charge pump) does 120 J of work on each coulomb of charge that it transfers, then the light uses 120 J of energy as the charge passes through the light
The electric current is measured in coulombs per second A flow of one coulomb per second is called one ampere, A, of current
The energy carried by an electric current depends on the charge transferred and the potential difference across which it moves,  The voltage or potential difference is expressed in Joules/coulomb and multiplying this by the charge in coulombs yields energy in Joules
Electrical power is a measure of the rate at which energy is transferred, and is expressed in watts, or Joules/second Power can also be obtained by multiplying the voltage by the current:
Power,
Example Problem: What is the power delivered to a light bulb when the circuit has a voltage drop of 120 V and produces a current of 30 ampere?
Solution:
Example Problem: A 600 V battery delivers a 0400 A current to an electric motor that is connected across the battery terminals
What power is consumed by the motor? How much electric energy is delivered in 500 seconds?
Solution:
Summary
Electric current is the flow of electrons from the high potential energy position to the lower potential energy position Current flow is the direction a positive current would be traveling, or the opposite direction that electrons actually flow A closed loop containing current flow is called an electric circuit Electric current is measured in coulombs per second, or amperes
 The energy carried by an electric current depends on the charge transferred and the potential difference across which it moves,  Power,
Practice
Questions
The following video covers electric current Use this resource to answer the questions that follow
http://wwwyoutubecom/watch?v=YNtQFSMjWLY
Click on the image above for more content
What type of current is described in this video (electron or conventional)? What drives the current through the circuit? What inhibits the flow of current in the circuit?
The following link provides instructional material, example problems, and a quiz on electric current: http://librarythinkquestorg/10796/ch13/ch13htm
Review
Questions
The current through a light bulb connected across the terminals of a 120 V outlet is 050 A At what rate does the bulb convert electric energy to light? A 120 V battery causes a current of 20 A to flow through a lamp What is the power used by the lamp? What current flows through a 100
 V outlet? The current through a motor is 210 A If a battery keeps a 120 V potential difference across the motor, what electric energy is delivered to the motor in 100 s?
electric current: A measure of the amount of electrical charge transferred per unit time It represents the flow of electrons through a conductive material or, in the case of conventional current, the flow of positive holes through a conductive material conventional current: In a majority of electric currents, the moving charges are negative electrons
 Although inconvenient, it’s fairly easy to keep straight if you just remember that the actual moving charges, the electrons, flow in a direction opposite that of the electric current resistance: Defined as the ability of a substance to prevent or resist the flow of electrical current ampere: Using the SI definitions for the conventional values, the ampere can be defined as exactly elementary charges per second or 1
Resistance and Ohm's Law
Define resistance Understand the unit for resistance: ohms Use Ohm’s Law to solve problems involving current, potential difference, and resistance
The bands of color on a resistor are a code that indicates the magnitude of the resistance of the resistor There are four color bands identified by letter: A, B, C, and D, with a gap between the C and D bands so that you know which end is A
 Based on the colors of the bands, it is possible to identify the type of resistor the A and B bands represent significant digits; red is 2 and blue is 6 The C band indicates the multiplier, and green indicates 10 5  These three together indicate that this particular resistor is a 26,000 Ohm resistor
 These terms will be explained over the course of this lesson