Like microscopes, telescopes use convex lenses to make enlarged images However, telescopes make enlarged images of objects—such as distant stars—that only appear tiny because they are very far away There are two basic types of telescopes: reflecting telescopes and refracting telescopes The two types are compared in the Figure below 
 You can learn more about telescopes and how they evolved in the video at this URL:
http://wwwvideojugcom/film/how-does-a-telescope-work
Click on the image above for more content
Figure 31136