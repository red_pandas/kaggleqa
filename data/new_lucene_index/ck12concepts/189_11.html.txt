The urinary system controls the amount of water in the body and removes wastes Any problem with the urinary system can also affect many other body systems
Kidney Stones
In some cases, certain mineral wastes can form kidney stones ( Figure below ) Stones form in the kidneys and may be found anywhere in the urinary system Often, stones form when the urine becomes concentrated, allowing minerals to crystallize and stick together They can vary in size, from small stones that can flow through your urinary system, to larger stones that cannot
 Some stones may need to be removed by surgery or ultrasound treatments
Figure 1170 A kidney stone The stones can form anywhere in the urinary system
What are the symptoms of kidney stones? You may have a kidney stone if you have pain while urinating, see blood in your urine, and/or feel a sharp pain in your back or lower abdomen (the area between your chest and hips) The pain may last for a long or short time
 If you have a small stone that passes on its own easily, you may not experience any symptoms If you have some of these symptoms, you should see your doctor
Kidney failure
Kidney failure happens when the kidneys cannot remove wastes from the blood If the kidneys are unable to filter wastes from the blood, the wastes build up in the body Kidney failure can be caused by an accident that injures the kidneys, the loss of a lot of blood, or by some drugs and poisons
 But if the kidneys are not seriously damaged, they may recover
Chronic kidney disease is the slow decrease in kidney function that may lead to permanent kidney failure A person who has lost kidney function may need to get kidney dialysis Kidney dialysis is the process of filtering the blood of wastes using a machine A dialysis machine ( Figure below ) filters waste from the blood by pumping the blood through a fake kidney
Figure 1171 During dialysis, a patient’s blood is sent through a filter that removes waste products The clean blood is returned to the body
Urinary tract infections (UTIs)
Urinary tract infections (UTIs) are bacterial infections of any part of the urinary tract When bacteria get into the bladder or kidney and produce more bacteria in the urine, they cause a UTI The most common type of UTI is a bladder infection Women get UTIs more often than men
Most UTIs are not serious, but some infections can lead to serious problems Long lasting kidney infections can cause permanent damage, including kidney scars, poor kidney function, high blood pressure, and other problems Some sudden kidney infections can be life threatening, especially if the bacteria enter the bloodstream, a condition called septicemia
What are the signs and symptoms of a UTI?
a burning feeling when you urinate, frequent or intense urges to urinate, even when you have little urine to pass, pain in your back or side below the ribs, cloudy, dark, bloody, or foul-smelling urine, fever or chills
You should see your doctor if you have signs of a UTI Your doctor will diagnose a UTIs by asking about your symptoms and then testing a sample of your urine