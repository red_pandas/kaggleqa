Thermal energy and temperature are closely related Both reflect the kinetic energy of moving particles of matter However, temperature is the average kinetic energy of particles of matter, whereas thermal energy is the total kinetic energy of particles of matter Does this mean that matter with a lower temperature has less thermal energy than matter with a higher temperature? Not necessarily
 The other factor is mass
Q: Look at the pot of soup and the tub of water in the Figure below  Which do you think has greater thermal energy?
A: The soup is boiling hot and has a temperature of 100 °C, whereas the water in the tub is just comfortably warm, with a temperature of about 38 °C Although the water in the tub has a much lower temperature, it has greater thermal energy
Figure 3127
The particles of soup have greater average kinetic energy than the particles of water in the tub, explaining why the soup has a higher temperature However, the mass of the water in the tub is much greater than the mass of the soup in the pot This means that there are many more particles of water than soup
 Therefore, the water in the tub has greater thermal energy than the soup To compare the thermal energy of some other materials, go to the following URL and click on the interactive animation “Temperature and Thermal Energy”
http://wwwabsorblearningcom/media/itemaction?quick=ad
Q: Could a block of ice have more thermal energy than a pot of boiling water?
A: Yes, the block of ice could have more thermal energy if its mass was much greater than the mass of the boiling water