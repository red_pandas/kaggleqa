Figure 3041
Corey’s friend Jerod likes to skate on the flat banks at Newton’s Skate Park That’s Jerod in the Figure above  As he reaches the top of a bank, he turns his skateboard to go back down To change direction, he presses down with his heels on one edge of the skateboard
Video Break
Can you turn a skateboard like Jerod? To see how to apply forces to change the direction of a skateboard, watch this video:
http://wwwyoutubecom/watch?v=iOnlcEk50CM
Click on the image above for more content
Do You Get It?
Q : How does Jerod use Newton’s first law of motion to change the direction of his skateboard?
A : Pressing down on just one side of a skateboard creates an unbalanced force The unbalanced force causes the skateboard to turn toward the other side In the picture, Jerod is pressing down with his heels, so the skateboard turns toward his toes