AP site : A location in DNA that has neither a purine nor a pyrimidine base; apurinic or apyrimidinic site; also known as an a basic site
base excision repair : A cellular mechanism that repairs damaged DNA throughout the cell cycle
DNA mismatch repair : A system for recognizing and repairing insertions, deletions and mis-incorporation of bases in the DNA
DNA repair : A collection of processes by which a cell identifies and corrects damage to its DNA
global genomic NER : Repairs damage in both transcribed and untranscribed DNA strands in active and inactive genes throughout the genome
nucleotide excision repair : DNA repair mechanism that repair DNA damaged by a variety of mutagens
transcription-coupled NER : Repair process initiated when RNA polymerase detects a changes in the double helix shape