Two basic types of energy exist: potential energy and kinetic energy Potential energy is stored energy It has not yet been released, but is ready to go Kinetic energy is energy of motion It causes work to be done through movement
Chemical Potential Energy
Energy is the capacity for doing work or supplying heat When you fill your car with gasoline, you are providing it with potential energy Chemical potential energy is the energy stored in the chemical bonds of a substance The various chemicals that make up gasoline contain a large amount of chemical potential energy that is released when the gasoline is burned in a controlled way in the engine of the car
 Some of the potential energy is transformed into work, which is used to move the car At the same time, some of the potential energy is converted to heat, making the car’s engine very hot The energy changes of a system occur as either heat or work, or some combination of both
Figure 701 A dragster is able to accelerate because of the chemical potential energy of its fuel The burning of the fuel also produces large amounts of heat
Dynamite is another example of chemical potential energy The major component of dynamite is nitroglycerin, a very unstable material By mixing it with diatomaceous earth, the stability is increased and it is less likely to explode if it receives a physical shock When ignited, the nitroglycerin explodes rapidly, releasing large amounts of nitrogen and other gases along with a massive amount of heat
Figure 702 Dynamite explosion
Summary
Chemical potential energy is energy available in the chemical bonds of a compound
Practice
Questions
Read the material at the link below and answer the questions:
http://wwwbrighthubeducationcom/science-homework-help/118803-what-is-chemical-potential-energy/
How is chemical potential energy released? Give examples of chemical potential energy What is chemical potential energy dependent upon?
Review
Questions
What is chemical potential energy? How is gasoline used as kinetic energy? How is dynamite used as kinetic energy?
chemical potential energy: The energy stored in the chemical bonds of a substance kinetic energy: Energy of motion potential energy: Stored energy
Heat
Define heat Define thermochemistry
“Under the spreading chestnut tree ” - The Village Blacksmith by Henry Wadsworth Longfellow 1807-1882
Blacksmiths, like the one mentioned in Longfellow's poem, heat solid iron in order to shape it into a variety of different objects Iron is a rigid, solid metal At room temperature, it is extremely difficult to bend iron However, when heated to a high enough temperature, iron can be easily worked