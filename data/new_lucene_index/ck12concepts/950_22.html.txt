Radioactive decay is the breakdown of unstable elements into stable elements To understand this process, recall that the atoms of all elements contain the particles protons, neutrons, and electrons
Isotopes
An element is defined by the number of protons it contains All atoms of a given element contain the same number of protons The number of neutrons in an element may vary Atoms of an element with different numbers of neutrons are called isotopes 
Consider carbon as an example Two isotopes of carbon are shown below ( Figure below ) Compare their protons and neutrons Both contain six protons But carbon-12 has six neutrons and carbon-14 has eight neutrons
Figure 2224 Isotopes are named for their number of protons plus neutrons If a carbon atom had seven neutrons, what would it be named?
Almost all carbon atoms are carbon-12 This is a stable isotope of carbon Only a tiny percentage of carbon atoms are carbon-14 carbon-14 is unstable It is a radioactive isotope of carbon Pictured below is carbon dioxide ( Figure below ), which forms in the atmosphere from carbon-14 and oxygen
 The nitrogen forms carbon-14 Carbon in the atmosphere combines with oxygen to form carbon dioxide Plants take in carbon dioxide during photosynthesis In this way, carbon-14 enters food chains
Figure 2225 Carbon-14 forms in the atmosphere It combines with oxygen and forms carbon dioxide How does carbon-14 end up in fossils?
Decay of Unstable Isotopes
Like other unstable isotopes, carbon-14 breaks down, or decays The original atoms are called the parent isotopes  For carbon-14 decay, each carbon-14 atom loses an beta particle It changes to a stable atom of nitrogen-14 The stable atom at the end is the daughter product ( Figure below )
Figure 2226 Unstable isotopes, such as carbon-14, decay by losing atomic particles They form different, stable elements when they decay
The decay of an unstable isotope to a stable element occurs at a constant rate This rate is different for each parent-daughter isotope pair The decay rate is measured in a unit called the half-life The half-life is the time it takes for half of a given amount of an isotope to decay
 Imagine that you start out with 100 grams of carbon-14 In 5,730 years, half of it decays This leaves 50 grams of carbon-14 Over the next 5,730 years, half of the remaining amount will decay Now there are 25 grams of carbon-14 How many grams will there be in another 5,730 years? The figure below graphs the rate of decay of a substance ( Figure below )
Figure 2227 The rate of decay of a radioactive substance is constant over time