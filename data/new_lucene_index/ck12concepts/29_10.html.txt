Vertebrates are animals with backbones These include fish, amphibians, reptiles, birds, and mammals
Characteristics of Vertebrates
The primary feature shared by all vertebrates is the vertebral column , or backbone The vertebral column protects the spinal cord
Other typical vertebrate traits include:
The cranium (skull) to protect the brain The brain is attached to the spinal cord An internal skeleton The internal skeleton supports the animal, protects internal organs, and allows for movement A defined head region with a brain The head region has an accumulation of sense organs
Living vertebrates range in size from a carp species, as little as 03 inches, to the blue whale, which can be as large as 110 feet ( Figure below )
Figure 103 A species of carp and an image of the blue whale (a mammal), the largest living vertebrate, reaching up to 110 feet long Shown below it is the smallest whale species, Hector's dolphin (about 5 feet in length), and beside it is a human These images are not to scale
Classification of Vertebrates
Vertebrates, or subphylum Vertebrata, are all members of the phylum Chordata Although there is some disagreement on how to classify animals, the traditional system divides the vertebrates into seven classes ( Table below )