Crops need three things for good growth: water, nutrients from the soil, and protection from predators such as insects  Chemistry has made major contributions in all three areas  Water purification uses a number of chemical and physical techniques to remove salts and contaminants that would pollute the soil  Chemical analysis of soil allows the grower to see what nutrients are lacking so they can be added
  These same stores also provide a number of sprays or solid treatments for insects that might otherwise have a snack on the plants
Figure 5415 Idaho wheat field
Water Purification
Fresh water is essential for good crops  In some areas of the world, there is enough rain to accomplish this task  In other locales, water must be provided so the crops will grow  Worldwide, irrigation covers about 18% of farm land and produces some 40% of crops A major source of cleaner water in many parts of the world is provided by the process of desalinization 
Figure 5416 Desalinization equipment
Sea water is treated to remove salts and the resulting water can then be used for irrigation without contaminating the soil with materials that harm the growing plants
Soil Nutrients
In many areas of the world, the soil is deficient in essential nutrients  A number of minerals such as phosphorus, potassium, calcium, and magnesium may not be present in large enough amounts to cause good plant growth  Nitrogen is extremely important for good crops
Figure 5417 Man spreading chemicals on soil
Soil analysis is available from a variety of labs  Local university extension services can provide valuable information as to the composition of a soil and will also make suggestions as to the types and amounts of needed nutrients  Fertilizers can be purchased and added to the soil to enrich it and ensure better yield of crops
Insect Control
Even if the crop grows well, there is still the possibility of insect or pest damage  The insect or pest can consume the crop or can damage it to the point where it will not grow well  Infestations of armyworms can do major damage to corn and grain crops Aphids and boll weevils are major predators of cotton crops
Figure 5418 Potato pests
Watch an armyworm video at http://wwwyoutubecom/watch?v=b1SlOWeMkNQ (3:39)
Click on the image above for more content
A wide variety of pesticides have been developed by chemists and other scientists to deal with all these pests  The basic approach is to have the pesticide interfere with some biochemical process in the pest  Ideally, the pesticide will not affect other living organisms, but this is not always the case
Summary
Plant nutrients are very important for good plant growth Chemical analysis of soil can tell he farmer or gardener what nutrients are needed Chemists have developed many pesticides that will kill plant predators such as the army worm and the boll weevil
Explore More
Use this resource to answer the following questions:
Agricultural and Food Chemistry at http://wwwacsorg/content/acs/en/careers/college-to-career/chemistry-careers/agricultural-and-food-chemistryhtml
Describe the roles of an agricultural and food chemist Discuss basic and applied research of an agricultural and food chemist What are the differences between an agricultural chemist and a food chemist? What are the roles of a soil and plant chemist?
Review
List three things crops need for good growth How much of the water used in farming is provided by irrigation? What fraction of crops are grown using irrigation? Why do nutrients need to be added to the soil? How do pesticides work?
desalinization: A major source of cleaner water in many parts of the world is provided by this process nutrient: Vitamins and Minerals that enable an organism to grow pesticide: Chemicals that will kill plant predators Developed to preserve plant growth, without the interference of plant predators
Materials
List ways chemistry has contributed to the development of materials
How does chemistry affect the clothing that we wear?
Chemistry research is often full of surprises  One such surprise came to Stephanie Kwolek of the DuPont chemical company  She was working on a type of material known as polymers  These chemicals had been around for a while and were being used for new types of textiles  Kwolek was looking for a strong and rigid petroleum product
  But she played a hunch and had it made into threads  This new material had stiffness about nine times that of any of the known polymers of the time  Further research and development led to the production of Kevlar, a material now widely used in body armor (see figure above)