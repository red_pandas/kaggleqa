Scuba diving is a form of underwater diving in which a diver carries his own breathing gas, usually in the form of a tank of compressed air The pressure in most commonly used scuba tanks ranges from 200 to 300 atmospheres Gases are unlike other states of matter in that a gas expands to fill the shape and volume of its container
 If the air in a typical scuba tank were transferred to a container at the standard pressure of 1 atm, the volume of that container would need to be about 2500 liters
Figure 671 Scuba diver
Compressibility is the measure of how much a given volume of matter decreases when placed under pressure If we put pressure on a solid or a liquid, there is essentially no change in volume The atoms, ions, or molecules that make up the solid or liquid are very close together
The kinetic-molecular theory explains why gases are more compressible than either liquids or solids Gases are compressible because most of the volume of a gas is composed of the large amounts of empty space between the gas particles At room temperature and standard pressure, the average distance between gas molecules is about ten times the diameter of the molecules themselves
Compressed gases are used in many situations In hospitals, oxygen is often used for patients who have damaged lungs to help them breathe better If a patient is having a major operation, the anesthesia that is administered will frequently be a compressed gas Welding requires very hot flames produced by compresses acetylene and oxygen mixtures
Figure 672 Oxygen tank
Summary
Gases will compress more easily that solids or liquids because here is so much space between the gas molecules
Practice
Questions
Use the link below to answer the following questions:
http://wwwcdxetextbookcom/engines/motivePower/4gasEng/engcyclehtml
What brings the fuel-air mixture into the cylinder? What is the role of the compression cycle? Does the exhaust cycle compress the gases produced by ignition?
Review
Questions
Why is there no change in volume when pressure is applied to liquids and solids? Why do gases compress more easily than liquids and solids? List uses for compressed gases
compressibility: The measure of how much a given volume of matter decreases when placed under pressure
Factors Affecting Gas Pressure
List factors that affect gas pressure Explain these effects in terms of the kinetic-molecular theory of gases
How high does a basketball bounce?
The pressure of the air in a basketball has to be adjusted so that the ball bounces to the correct height Before a game, the officials check the ball by dropping it from shoulder height and seeing how far back up it bounces What would the official do if the ball did not bounce up as far as it is supposed to? What would he do if it bounced too high?
The pressure inside a container is dependent on the amount of gas inside the container If the basketball does not bounce high enough, the official could remedy the situation by using a hand pump and adding more air to the ball Conversely, if it bounces too high, he could let some air out of the ball