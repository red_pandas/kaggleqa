Did you know that you see the largest organ in your body every day? You wash it, dry it, cover it up to stay warm, and uncover it to cool off Yes, your skin is your body's largest organ Your skin is part of your integumentary system ( Figure below ), which is the outer covering of your body
Functions of Skin
Figure 114 Skin acts as a barrier that stops water and other things, like soap and dirt, from getting into your body
The skin has many important functions The skin:
Provides a barrier It keeps organisms that could harm the body out It stops water from entering or leaving the body Controls body temperature It does this by making sweat (or perspiration ), a watery substance that cools the body when it evaporates Gathers information about your environment Special nerve endings in your skin sense heat, pressure, cold, and pain
 Acts as a sun block A pigment called melanin blocks sunlight from getting to deeper layers of skin cells, which are easily damaged by sunlight
Structure of Skin
Your skin is always exposed to your external environment, so it gets cut, scratched, and worn down You also naturally shed many skin cells every day Your body replaces damaged or missing skin cells by growing more of them Did you know that the layer of skin you can see is actually dead? As the dead cells are shed or removed from the upper layer, they are replaced by the skin cells below them
Two different layers make up the skin: the epidermis and the dermis ( Figure below ) A fatty layer lies under the dermis, but it is not part of your skin
Figure 115 Skin is made up of two layers, the epidermis on top and the dermis below The tissue below the dermis is called the hypodermis, but it is not part of the skin
The Epidermis
The epidermis is the outermost layer of the skin It forms the waterproof, protective wrap over the body's surface Although the top layer of epidermis is only about as thick as a sheet of paper, it is made up of 25 to 30 layers of cells The epidermis also contains cells that produce melanin
 Melanin-producing cells are found in the bottom layer of the epidermis The epidermis does not have any blood vessels The lower part of the epidermis receives blood by diffusion from blood vessels of the dermis
The Dermis
The dermis is the layer of skin directly under the epidermis It is made of a tough connective tissue The dermis contains hair follicles, sweat glands, oil glands, and blood vessels ( Figure above ) It also holds many nerve endings that give you your sense of touch, pressure, heat, and pain
Do you ever notice how your hair stands up when you are cold or afraid? Tiny muscles in the dermis pull on hair follicles which cause hair to stand up The resulting little bumps in the skin are commonly called "goosebumps" ( Figure below )
Figure 116 Goosebumps are caused by tiny muscles in the dermis that pull on hair follicles, which causes the hairs to stand up straight
Oil Glands and Sweat Glands
Glands and hair follicles open out into the epidermis, but they start in the dermis Oil glands ( Figure above ) release, or secrete an oily substance, called sebum , into the hair follicle Sebum “waterproofs” hair and the skin surface to prevent them from drying out It can also stop the growth of bacteria on the skin
 If an oil gland becomes plugged and infected, it develops into a pimple Up to 85% of teenagers get pimples, which usually go away by adulthood Frequent washing can help decrease the amount of sebum on the skin
Sweat glands ( Figure above ) open to the skin surface through skin pores They are found all over the body Evaporation of sweat from the skin surface helps to lower skin temperature The skin also releases excess water, salts, sugars, and other wastes, such as ammonia and urea, in sweat
The Integumentary System Song can be heard at https://wwwyoutubecom/watch?v=MeTaBniB0ok 