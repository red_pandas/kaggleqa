Electron transport is the final stage of aerobic respiration In this stage, energy from NADH and FADH 2 , which result from the Krebs cycle, is transferred to ATP Can you predict how this happens? ( Hint: How does electron transport occur in photosynthesis?)
See http://wwwyoutubecom/watch?v=1engJR_XWVU for an overview of the electron transport chain
Transporting Electrons
High-energy electrons are released from NADH and FADH 2 , and they move along electron transport chains , like those used in photosynthesis The electron transport chains are on the inner membrane of the mitochondrion As the high-energy electrons are transported along the chains, some of their energy is captured
 Electron transport in a mitochondrion is shown in Figure below 
Figure 3348 Electron-transport chains on the inner membrane of the mitochondrion carry out the last stage of cellular respiration
Making ATP
The pumping of hydrogen ions across the inner membrane creates a greater concentration of the ions in the intermembrane space than in the matrix This chemiosmotic gradient causes the ions to flow back across the membrane into the matrix, where their concentration is lower ATP synthase acts as a channel protein, helping the hydrogen ions cross the membrane
 After passing through the electron-transport chain, the “spent” electrons combine with oxygen to form water This is why oxygen is needed; in the absence of oxygen, this process cannot occur
How much ATP is produced? The two NADH produced in the cytoplasm produces 2 to 3 ATP each (4 to 6 total) by the electron transport system, the 8 NADH produced in the mitochondria produces three ATP each (24 total), and the 2 FADH 2 adds its electrons to the electron transport system at a lower level than NADH, so they produce two ATP each (4 total)
A summary of this process can be seen at the following sites: http://wwwyoutubecom/watch?v=fgCcFXUZRk (17:16) and http://wwwyoutubecom/watch?v=W_Q17tqw_7A (4:59)
Click on the image above for more content
Click on the image above for more content