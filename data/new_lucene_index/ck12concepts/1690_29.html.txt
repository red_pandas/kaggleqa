Unsaturated hydrocarbons called aromatic hydrocarbons are cyclic hydrocarbons that have double bonds These compounds have six carbon atoms in a ring with alternating single and double bonds The smallest aromatic hydrocarbon is benzene, which has just one ring Its structural formula is shown in the Figure below  Larger aromatic hydrocarbons consist of two or more rings, which are joined together by bonds between their carbon atoms
 That’s why they are used in air fresheners and mothballs You can learn more about these interesting hydrocarbons at this URL:
http://wwwyoutubecom/watch?v=8gW7H0ReN5g
Click on the image above for more content
Figure 2965
Q: How many bonds does each carbon atom in benzene form?
A: Each carbon atom forms four covalent bonds Carbon atoms always form four covalent bonds, regardless of the atoms to which it bonds