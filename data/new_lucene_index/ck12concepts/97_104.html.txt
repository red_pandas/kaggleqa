The earth is a sphere If you draw a horizontal straight line from a point on the surface of the earth, the surface of the earth drops away from the line The distance that the earth drops away from the horizontal line is very small – so small, in fact, that we cannot represent it well in a drawing
20 m  If the sketch were drawn to scale, the red line would be too short to see
When an object is launched exactly horizontally in projectile motion, it travels some distance horizontally before it strikes the ground In the present discussion, we wish to imagine a projectile fired horizontally on the surface of the earth such that while traveling 1600 m horizontally, the object would fall exactly 0
 If this could occur, then the object would fall exactly the amount necessary during its horizontal motion to remain at the surface of the earth, but not touching it In such a case, the object would travel all the way around the earth continuously and circle the earth, assuming there were no obstacles, such as mountains
What initial horizontal velocity would be necessary for this to occur? We first calculate the time to fall the 020 m:
The horizontal velocity necessary to travel 1600 m in 020 s is 8000 m/s Thus, the necessary initial horizontal velocity is 8000 m/s
In order to keep an object traveling in a circular path, there must be an acceleration toward the center of the circle This acceleration is called centripetal acceleration In the case of satellites orbiting the earth, the centripetal acceleration is caused by gravity If you were swinging an object around your head on a string, the centripetal acceleration would be caused by your hand pulling on the string toward the center of the circle
It is important to note that the object traveling in a circle has a constant speed but does not have a constant velocity This is because direction is part of velocity; when an object changes its direction, it is changing its velocity Hence the object's acceleration The acceleration in the case of uniform circular motion is the change in the direction of the velocity, but not its magnitude
For an object traveling in a circular path, the centripetal acceleration is directly related to the square of the velocity of the object and inversely related to the radius of the circle
Taking a moment to consider the validity of this equation can help to clarify what it means Imagine a yo-yo Instead of using it normally, let it fall to the end of the string, and then spin it around above your head If we were to increase the speed at which we rotate our hand, we increase the velocity of the yo-yo - it is spinning faster
 The acceleration increases Now let's think about the bottom of the equation: the radius If we halve the length of the yo-yo string (bring the yo-yo closer to us), we make the yo-yo's velocity greater Again, it moves faster, which increases the acceleration If we make the string longer again, this decreases the acceleration
Example Problem: A ball at the end of a string is swinging in a horizontal circle of radius 115 m The ball makes exactly 200 revolutions per second What is its centripetal acceleration?
Solution: We first determine the velocity of the ball using the facts that the circumference of the circle is and the ball goes around exactly twice per second
We then use the velocity and radius in the centripetal acceleration equation
Example Problem: The moon's nearly circular orbit around the earth has a radius of about 385,000 km and a period of 273 days Calculate the acceleration of the moon toward the earth
Solution:
As shown in the previous example, the velocity of an object traveling in a circle can be calculated by
Where is the radius of the circle and is the period (time required for one revolution)
This equation can be incorporated into the equation for centripetal acceleration as shown below
Summary
In order to keep an object traveling in a circular path, there must be an acceleration toward the center of the circle This acceleration is called centripetal acceleration  The acceleration in the case of uniform circular motion changes the direction of the velocity but not its magnitude Formulas for centripetal acceleration are and 
Practice
Questions
This video is a demonstration of centripetal force using balloons and trays of water Use this resource to answer the questions that follow
https://wwwyoutubecom/watch?v=EX5DZ2MHlV4
What does centripetal mean? What is uniform circular motion? Why is centripetal acceleration always towards the center?
Review
Questions
An automobile rounds a curve of radius 500 m on a flat road at a speed of 14 m/s What centripetal acceleration is necessary to keep the car on the curve? An object is swung in a horizontal circle on a length of string that is 093 m long If the object goes around once in 1
circular motion: A movement of an object along the circumference of a circle or rotation along a circular path centripetal acceleration: The acceleration toward the center that keeps an object following a circular path
Centripetal Force
Define centripetal force Solve problems involving centripetal force Explain the difference between centripetal and centrifugal forces
Jupiter's moons and ring materials follow all the laws of physics, including centripetal force and centripetal acceleration