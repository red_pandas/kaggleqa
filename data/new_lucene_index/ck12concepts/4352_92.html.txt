Ohm’s Law
; Voltage drop equals current multiplied by resistance
Power
; Power released is equal to the voltage multiplied by the current
Guidance Ohm's Law is the main equation for electric circuits but it is often misused In order to calculate the voltage drop across a light bulb use the formula:  For the total current flowing out of the power source, you need the total resistance of the circuit and the total voltage: 
 As with Ohm’s Law, one must be careful not to mix apples with oranges If you want the power of the entire circuit, then you multiply the total voltage of the power source by the total current coming out of the power source If you want the power dissipated (i
 released) by a light bulb, then you multiply the voltage drop across the light bulb by the current going through that light bulb Power is the rate that energy is released The units for power are Watts , which equal Joules per second  Therefore, a light bulb releases Joules of energy every second
Example 1
Click on the image above for more content
Example 2
A small flash light uses a single AA battery which provides a voltage of 15 V If the bulb has a resistance of , how much power is dissipated by the light bulb
Solution
Since the light bulb is the only object in the circuit, we know the voltage drop across the light bulb is equal to that of the battery Therefore, we can use Ohm's law to solve for the current in the resistor
Now we can determine the power of the bulb