CODIS : The Combined DNA Index System, is maintained by the Federal Bureau of Investigation and stores DNA profiles
DNA fingerprint : A unique DNA pattern that distinguishes between individuals of the same species using only samples of their DNA; also known as a genetic fingerprint
genetic fingerprint : A unique DNA pattern that distinguishes between individuals of the same species using only samples of their DNA; also known as a DNA fingerprint
microsatellite : Short sequences of 100-200 bp, usually due to repeats of 1-6 bp sequences; also known as a STR (Short Tandem Repeat) polymorphism
restriction fragment length polymorphism : Genetic differences due to differences between restriction enzyme sites; produces length variation of DNA segments upon analysis
short tandem repeat (STR) : Short sequences of 100-200 bp, usually due to repeats of 1-6 bp sequences; also known as a micro satellite
Southern blot : A method used to check for the presence of a specific DNA sequence in a DNA sample; named after its inventor Edwin Southern
STR profile : A genetic profile created through the analysis of 13 STR loci; often used in forensic analysis