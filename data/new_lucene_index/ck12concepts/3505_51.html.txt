A Mendelian trait is a trait that is controlled by a single gene that has two alleles One of these alleles is dominant and the other is recessive Many inheritable conditions in humans are passed to offspring in a simple Mendelian fashion Medical professionals use Mendel’s laws to predict and understand the inheritance of certain traits in their patients
Figure 518 Tossing a Coin Competitions often begin with the toss of a coin Why is this a fair way to decide who goes first? If you choose heads, what is the chance that the toss will go your way? Is this similar to the probability of inheriting a particular allele?
The rules of probability that apply to tossing a coin or throwing a die also apply to the laws of segregation and independent assortment Probability is the likelihood that a certain event will occur It is expressed by comparing the number of events that occur to the total number of possible events
Probability = (number of times an event is expected to occur)/(total number of times an event could happen)
For example, in Mendel’s F 2 hybrid generation, the dominant trait of purple flower color appeared 705 times, and the recessive trait appeared 224 times The dominant allele appeared 705 times out of a possible 929 times (705+224=929)
Probability = (705/929)
(705/929)= 076
Probability is normally expressed in a range between 0 and 1, but it can also be expressed as a percentage, fraction, or ratio Expressed as a percentage, the probability that a plant of the F 2 generation will have purple flowers is 76% Expressed as a fraction it is about ¾,and as a ratio it is roughly 3:1
 In fact, Mendel found that all the other dominant “factors” had approximately a ¾ probability of being expressed in the F 2 hybrid generation Review the Table below for the results for the other six characteristics
Results of F1 Generation Crosses for Seven Characteristics in
The probability the recessive trait will appear in the F 2 hybrid generation is calculated in the same way
Probability = (224/929)
(224/929) = 024
The probability of the recessive trait appearing in the F 2 generation is 24% or about ¼
Results predicted by probability are most accurate when many trials are done The best way to illustrate this idea is to toss a coin Because a coin has two sides, every time you toss it the chance of tossing heads or tossing tails is 50% The outcome of each separate toss is unaffected by any previous or future result
 You would think that the next toss is more likely to be a tail, but the possibility of tossing another head is still 50% If you tossed the coin a total of ten times, a total of seven heads and three tails, you would calculate the probability of tossing heads is 70%
 If Mendel had grown only 10 plants, he would have gotten different probabilities for the appearance of dominant and recessive traits However, Mendel carried out many thousands of trials He was therefore sure that his results were due to probability, and not to chance
Probability and the Law of Segregation
Each coin toss is a separate event Likewise, gamete formation is a separate event The probability that a Pp heterozygote produces gametes with a P allele or a p allele is 50% for each gamete cell In a fertilization involving two such plants (as in the F 1 generation self-pollination experiment), each pollen cell and each egg cell have a 50% chance of having the P or p allele
Using Probability to Determine Alleles in Gametes
In Mendel's monohybrid cross, two heterozygous plants are crossed Both plants produce gametes, all of which contain either a P or p allele for flower color The likelihood that any particular gamete contains the allele for a white flower can be calculated Because a gamete can only carry one out of two alleles, there are only two possible outcomes for a gamete
5, or 50% The probability that a gamete will carry the allele for purple flower color is also ½
Figure 519 Formation of gametes by meiosis Paired alleles always separate and go to different gametes during meiosis
Using Probability in a Heterozygous Cross
We can calculate the probability of any one of the offspring being heterozygous ( Pp ) or homozygous ( PP or pp ) for flower color The probability of a plant inheriting the P or p allele from a heterozygous parent is ½ Multiply the probabilities of inheriting both alleles to find the probability that any one plant will be a pp homozygote
½ × ½ = ¼ or 025
Only 25 %, or one outcome out of four, will result in a plant homozygous for white flower color ( pp ) The possibility that any one plant will be a PP homozygote is also 1/4 The heterozygous allele combination can happen twice ( Pp or pP ), so the two probabilities are added together ¼ + ¼ = 2/4, or ½