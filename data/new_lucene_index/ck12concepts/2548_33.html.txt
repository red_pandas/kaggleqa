The first stage of cellular respiration is glycolysis  It does not require oxygen, and it does not take place in the mitochondrion - it takes place in the cytosol of the cytoplasm
When was the last time you enjoyed yogurt on your breakfast cereal, or had a tetanus shot? These experiences may appear unconnected, but both relate to bacteria which do not use oxygen to make ATP In fact, tetanus bacteria cannot survive if oxygen is present However, Lactobacillus acidophilus (bacteria which make yogurt) and Clostridium tetani (bacteria which cause tetanus or lockjaw) share with nearly all organisms the first stage of cellular respiration, glycolysis
Splitting Glucose
The word glycolysis means “glucose splitting,” which is exactly what happens in this stage Enzymes split a molecule of glucose into two molecules of pyruvate (also known as pyruvic acid) This occurs in several steps, as shown in Figure below  You can watch an animation of the steps of glycolysis at this link: http://www
com/watch?v=6JGXayUyNVw 
Figure 3346 In glycolysis, glucose (C6) is split into two 3-carbon (C3) pyruvate molecules This releases energy, which is transferred to ATP How many ATP molecules are made during this stage of cellular respiration?
Results of Glycolysis
Energy is needed at the start of glycolysis to split the glucose molecule into two pyruvate molecules These two molecules go on to stage II of cellular respiration The energy to split glucose is provided by two molecules of ATP As glycolysis proceeds, energy is released, and the energy is used to make four molecules of ATP
 During this stage, high-energy electrons are also transferred to molecules of NAD + to produce two molecules of NADH , another energy-carrying molecule NADH is used in stage III of cellular respiration to make more ATP
A summary of glycolysis can be viewed at http://wwwyoutubecom/watch?v=FE2jfTXAJHg 
Click on the image above for more content