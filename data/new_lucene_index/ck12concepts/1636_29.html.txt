One of the most important series of endothermic reactions is photosynthesis In photosynthesis, plants make the simple sugar glucose (C 6 H 12 O 6 ) from carbon dioxide (CO 2 ) and water (H 2 O) They also release oxygen (O 2 ) in the process The reactions of photosynthesis are summed up by this chemical equation:
6 CO 2 + 6 H 2 O → C 6 H 12 O 6 + 6 O 2
The energy for photosynthesis comes from light Without light energy, photosynthesis cannot occur As you can see in the Figure below , plants can get the energy they need for photosynthesis from either sunlight or artificial light
Figure 2935