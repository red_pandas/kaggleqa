Robert Boyle (1627-1691), an English chemist, is widely considered to be one of the founders of the modern experimental science of chemistry He discovered that doubling the pressure of an enclosed sample of gas while keeping its temperature constant caused the volume of the gas to be reduced by half
 An inverse relationship is described in this way As one variable increases in value, the other variable decreases
Physically, what is happening? The gas molecules are moving and are a certain distance apart from one another An increase in pressure pushes the molecules closer together, reducing the volume If the pressure is decreased, the gases are free to move about in a larger volume
Figure 676 Robert Boyle
Mathematically, Boyle’s law can be expressed by the equation:
The is a constant for a given sample of gas and depends only on the mass of the gas and the temperature The Table below shows pressure and volume data for a set amount of gas at a constant temperature The third column represents the value of the constant for this data and is always equal to the pressure multiplied by the volume
 In this particular case, that constant is 500 atm · ml
Pressure-Volume Data
A graph of the data in the table further illustrates the inverse relationship nature of Boyle’s Law (see Figure below ) Volume is plotted on the -axis, with the corresponding pressure on the -axis
Figure 677 The pressure of a gas decreases as the volume increases, making Boyle’s law an inverse relationship
Boyle’s Law can be used to compare changing conditions for a gas We use and to stand for the initial pressure and initial volume of a gas After a change has been made, and stand for the final pressure and volume The mathematical relationship of Boyle’s Law becomes:
This equation can be used to calculate any one of the four quantities if the other three are known
Sample Problem: Boyle’s Law
A sample of oxygen gas has a volume of 425 mL when the pressure is equal to 387 kPa The gas is allowed to expand into a 175 L container Calculate the new pressure of the gas
Step 1: List the known quantities and plan the problem
Known
Unknown
Use Boyle’s Law to solve for the unknown pressure  It is important that the two volumes ( and ) are expressed in the same units, so has been converted to mL
Step 2: Solve
First, rearrange the equation algebraically to solve for 
Now substitute the known quantities into the equation and solve
Step 3: Think about your result
The volume has increased to slightly over 4 times its original value and so the pressure is decreased by about  The pressure is in kPa and the value has three significant figures Note that any pressure or volume units can be used as long as they are consistent throughout the problem
Pressure vs Volume
Have you ever wondered why your ears pop during airplane take offs and landings? Or why a balloon pops when you squeeze it too much? Find out in this MIT video See https://wwwyoutubecom/watch?v=1WJcC8DvGUo 
Click on the image above for more content
Summary
The volume of a gas is inversely proportional to temperature
Practice
Do the problems at the link below:
http://wwwconcordorg/~ddamelin/chemsite/g_gasses/handouts/Boyle_Problemspdf
Review
Questions
What does “inversely” mean in this law? Explain Boyle’s law in terms of the kinetic-molecular theory of gases Does it matter what units are used?
Boyle’s law: The volume of a given mass of gas varies inversely with the pressure when the temperature is kept constant
Charles's Law
State Charles’ Law Use this law to perform calculations involving volume-temperature relationships
How do you bake bread?
Everybody enjoys the smell and taste of freshly-baked bread It is light and fluffy as a result of the action of yeast on sugar The yeast converts the sugar to carbon dioxide, which at high temperatures causes the dough to expand The end-result is an enjoyable treat, especially when covered with melted butter