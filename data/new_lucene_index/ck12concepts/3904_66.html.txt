A barometer measures gas pressure by the height of the column of mercury One unit of gas pressure is the millimeter of mercury (mmHg) An equivalent unit to the mmHg is called the torr, in honor of the inventor of the barometer, Evangelista Torricelli The pascal (Pa) is the standard unit of pressure
 A kilopascal is equal to 1000 pascals Another commonly used unit of pressure is the atmosphere (atm) Standard atmospheric pressure is called 1 atm of pressure and is equal to 760 mmHg and 1013 kPa Atmospheric pressure is also often stated as pounds/square inch (psi) The atmospheric pressure at sea level is 14
It is important to be able to convert between different units of pressure To do so, we will use the equivalent standard pressures shown above
Sample Problem: Pressure Unit Conversions
The atmospheric pressure in a mountainous location is measured to be 613 mmHg What is this pressure in atm and in kPa?
Step 1: List the known quantities and plan the problem
Known
given: 613 mmHg 1 atm = 760 mmHg 1013 kPa = 760 mmHg
Unknown
pressure = ? atm pressure = ? kPa
Use conversion factors from the equivalent pressure units to convert from mmHg to atm and from mmHg to kPa
Step 2: Solve
Step 3: Think about your result
The air pressure is about 80% that of standard atmospheric pressure at sea level For significant figure purposes, the standard pressure of 760 mmHg has three significant figures
Watch the video to learn more about conversion factors:
Click on the image above for more content
http://wwwyoutubecom/watch?v=qv81QCGNnVo
Summary
Calculations are described for converting between different pressure units
Practice
Use the link below to practice conversion calculations:
http://wwwkentchemistrycom/links/GasLaws/Pressurehtm
Review
Questions
Who invented the barometer? One atm = ___ torr? 147 psi = ___ kPa 760 mm = ___ psi? The pressure in a car tire is 35 psi How many atmospheres is that?
atmosphere: Common unit of pressure Abbreviated as “atm” pascal: The standard unit of pressure Abbreviated as “Pa”
Average Kinetic Energy
Define kinetic energy Describe how temperature influences kinetic energy of particles Define aboslute zero
How much energy does it take to hit a baseball?
Kinetic energy is the energy of motion Any object that is moving possesses kinetic energy Baseball involves a great deal of kinetic energy The pitcher throws a ball, imparting kinetic energy to the ball When the batter swings, the motion of swinging creates kinetic energy in the bat The collision of the bat with the ball changes the direction and speed of the ball, with the idea of kinetic energy being involved again