An Arrhenius base is a compound, which ionizes to yield hydroxide ions (OH − ) in aqueous solution The Table below lists several of the more common bases:
Common Bases
All of the bases listed in the table are solids at room temperature Upon dissolving in water, each dissociates into a metal cation and the hydroxide ion
Sodium hydroxide is a very caustic substance also known as lye Lye is used as a rigorous cleaner and is an ingredient in the manufacture of soaps Care must be taken with strong bases like sodium hydroxide, as exposure can lead to severe burns (see Figure below )
Figure 744 This foot has severe burns due to prolonged contact with a solution of sodium hydroxide, also known as lye
Sodium belongs to the group of elements called the alkali metals An alkaline solution is another name for a solution that is basic All alkali metals react readily with water to produce the metal hydroxide and hydrogen gas The resulting solutions are basic
Bases that consist of an alkali metal cation and the hydroxide anion are all very soluble in water Compounds of the Group 2 metals (the alkaline earth metals) are also basic However, these compounds are generally not as soluble in water Therefore the dissociation reactions for these compounds are shown as equilibrium reactions
These relatively insoluble hydroxides were some of the compounds discussed in the context of the solubility product constant  The solubility of magnesium hydroxide is 00084 g per liter of water at 25°C Because of its low solubility, magnesium hydroxide is not as dangerous as sodium hydroxide In fact, magnesium hydroxide is the active ingredient in a product called milk of magnesia, which is used as an antacid or a mild laxative
Summary
Arrhenius base is defined Examples of Arrhenius bases are given
Practice
Read the material at the link below and then take the quiz:
http://flatworldknowledgelardbucketorg/books/introductory-chemistry/section_16_01html
Review
Questions
What is an Arrhenius base? What is one reaction that will form an Arrhenius base? Are alkaline earth bases very water-soluble?
alkaline solution: Another name for a solution that is basic Arrhenius base: A compound which ionizes to yield hydroxide ions (OH − ) in aqueous solution
Brønsted-Lowry Acids and Bases
Define Brønsted-Lowry acid Define Brønsted-Lowry base Give examples of Brønsted-Lowry acids and bases
A new theory
The Arrhenius concept of acids and bases was a significant contribution to our understanding of acids and bases It replaced and expanded the original idea of Lavoisier that all acids contained oxygen However, the Arrhenius theory had its shortcomings also It did not take into account the role of the solvent
 So, a new theory needed to be formed, which built on the findings of Arrhenius but also went beyond them