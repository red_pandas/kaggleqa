A decomposition reaction occurs when one reactant breaks down into two or more products It can be represented by the general equation:
AB → A + B
In this equation, AB represents the reactant that begins the reaction, and A and B represent the products of the reaction The arrow shows the direction in which the reaction occurs
Q: What is the chemical equation for the decomposition of hydrogen peroxide (H 2 O 2 ) to water (H 2 O) and oxygen (O 2 )?
A: The equation for this decomposition reaction is:
2 H 2 O 2 → 2 H 2 O + O 2