Objects such as ships may float in a fluid like water because of buoyant force This is an upward force that a fluid exerts on any object that is placed in it Archimedes discovered that the buoyant force acting on an object equals the weight of the fluid displaced by the object
 For an entertaining video presentation of Archimedes’ law, go to this URL:
http://videoshowstuffworkscom/discovery/6540-mythbusters-lets-talk-buoyancy-videohtm
Click on the image above for more content