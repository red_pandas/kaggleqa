Particles with opposite electric charges attract each other This explains why negative electrons orbit the positive nucleus Particles with the same electric charge repel each other This means that the positive protons in the nucleus push apart from one another So why doesn’t the nucleus fly apart? An even stronger force—called the strong nuclear force—holds protons and neutrons together in the nucleus
http://wwwyoutubecom/watch?v=PdFsb2sWrW4 (6:27)
Click on the image above for more content
Q: Can you guess why an atomic bomb releases so much energy when it explodes?
A: When an atomic bomb explodes, the nuclei of atoms undergo a process called fission, in which they split apart This releases the huge amount of energy that was holding together subatomic particles in the nucleus