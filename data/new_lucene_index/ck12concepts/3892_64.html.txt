Single-replacement reactions only occur when the element that is doing the replacing is more reactive than the element that is being replaced Therefore, it is useful to have a list of elements in order of their relative reactivities The activity series is a list of elements in decreasing order of their reactivity
 The Table below is an activity series of most common metals and of the halogens
Activity Series For a single-replacement reaction, a given element is capable of replacing an element that is below it in the activity series This can be used to predict if a reaction will occur Suppose that small pieces of the metal nickel were placed into two separate aqueous solutions: one of iron(III) nitrate and one of lead(II) nitrate
 Therefore, the nickel metal will be capable of replacing the lead in a reaction, but will not be capable of replacing iron
In the descriptions that accompany the activity series of metals, a given metal is also capable of undergoing the reactions described below that section For example, lithium will react with cold water, replacing hydrogen It will also react with steam and with acids, since that requires a lower degree of reactivity
Sample Problem: Single-Replacement Reactions
Use the activity series to predict if the following reactions will occur If not, write NR If the reaction does occur, write the products of the reaction and balance the equation
A
B
Step 1: Plan the problem
For A, compare the placements of aluminum and zinc on the activity series For B, compare the placements of silver and hydrogen
Step 2: Solve
Since aluminum is above zinc, it is capable of replacing it and a reaction will occur The products of the reaction will be aqueous aluminum nitrate and solid zinc Take care to write the correct formulas for the products before balancing the equation Aluminum adopts a 3+ charge in an ionic compound, so the formula for aluminum nitrate is Al(NO 3 ) 3 
Since silver is below hydrogen, it is not capable of replacing hydrogen in a reaction with an acid
Summary
Metals and halogens are ranked according to their ability to displacement other metals or halogens below them in the series Practice Questions Take the quiz on the web site below: http://wwwsophiaorg/chemical-reactions-activity-series-concept Review Questions What does the activity series tell us? Can a metal undergo any of the reactions listed below it in the series? List two metals that cobalt will displace and two that will displace it
activity series: A list of elements in decreasing order of their reactivity
Double-Replacement Reactions
Define double-replacement reaction Predict products of double-replacement reactions when given the reactants
Wanna trade?
The practice of barter (trading one thing for another) has been in existence from the beginning of time In the illustration above, Items like chickens were bartered for newspapers You have something I want, and I have something you want So we trade and we each have something new Some chemical reactions are like that