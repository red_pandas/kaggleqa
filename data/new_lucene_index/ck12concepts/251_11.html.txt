When a baby girl is born, her ovaries contain all of the eggs they will ever produce But these eggs are not fully developed They develop only after she starts having menstrual periods at about age 12 or 13 Just one egg develops each month A woman will release an egg once each month until she is in her 40s
 They die off and by puberty about 40,000 remain
Eggs
Eggs are very big cells In fact, they are the biggest cells in the human female body (How many egg cells are in the human male body?) An egg is about 30 times as wide as a sperm cell! You can even see an egg cell without a microscope Like a sperm cell, the egg contains a nucleus with half the number of chromosomes as other body cells
 The egg also does not have a tail
Egg Production
Egg production takes place in the ovaries It takes several steps to make an egg:
Before birth, special cells in the ovaries go through mitosis (cell division), producing identical cells The daughter cells then start to divide by meiosis  But they only go through the first of the two cell divisions of meiosis at that time They go through the second stage of cell division after the female goes through puberty
 The drawing below shows how this happens ( Figure below )
As you can see from the figure, the egg rests in a nest of cells called a follicle  The follicle and egg grow larger and go through other changes The follicle protects the egg as it matures in the ovary
After a couple of weeks, the egg bursts out of the follicle and through the wall of the ovary This is called ovulation , which usually occurs at the midpoint of a monthly cycle In a 28 day cycle, ovulation would occur around day 14 The moving fingers of the nearby fallopian tube then sweep the egg into the tube
Figure 11132 This diagram shows how an egg and its follicle develop in an ovary After it develops, the egg leaves the ovary and enters the fallopian tube (1) Undeveloped eggs, (2) Egg and follicle developing, (3) Egg and follicle developing, (4) Ovulation After ovulation, what remains of the follicle is known as the corpus luteum, which degenerates (5, 6)
Fertilization occurs if a sperm enters the egg while it is passing through the fallopian tube When this happens, the egg finally completes meiosis This results in two daughter cells that are different in size The smaller cell is called a polar body  It contains very little cytoplasm It soon breaks down and disappears
 It contains most of the cytoplasm This will develop into a child