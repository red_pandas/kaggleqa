A wheel and axle is a simple machine that consists of two connected rings or cylinders, one inside the other Both rings or cylinders turn in the same direction around a single center point The inner ring or cylinder is called the axle, and the outer one is called the wheel
 For more examples, go to this URL:
http://wwwmikidscom/SMachinesWheelshtm
Figure 3092
In a wheel and axle, force may be applied either to the wheel or to the axle This force is called the input force A wheel and axle does not change the direction of the input force However, the force put out by the machine, called the output force, is either greater than the input force or else applied over a greater distance
Q : Where is the force applied in a Ferris wheel and a doorknob? Is it applied to the wheel or to the axle?
A : In a Ferris wheel, the force is applied to the axle by the Ferris wheel’s motor In a doorknob, the force is applied to the wheel by a person’s hand