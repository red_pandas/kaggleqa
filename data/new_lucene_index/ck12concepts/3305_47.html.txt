ribonucleoprotein : A nucleoprotein that contains RNA; includes the ribosome, vault ribonucleoproteins, and small nuclear RNPs (snRNPs)
ribosome : A non-membrane bound organelle inside all cells; site of protein synthesis (translation)
ribozyme : An RNA molecule with a tertiary structure that enables it to catalyze a chemical reaction
Svedberg unit : A non-SI unit for sedimentation rate; technically a measure of time that offers a measure of particle size; 10 -13 seconds (100 fs)
translation : The process of synthesizing a polypeptide/protein from the information in a mRNA sequence; occurs on ribosomes