In 1926, Austrian physicist Erwin Schrödinger (1887-1961) used the wave-particle duality of the electron to develop and solve a complex mathematical equation that accurately described the behavior of the electron in a hydrogen atom The quantum mechanical model of the atom comes from the solution to Schrödinger’s equation Quantization of electron energies is a requirement in order to solve the equation
Recall that in the Bohr model, the exact path of the electron was restricted to very well-defined circular orbits around the nucleus The quantum mechanical model is a radical departure from that Solutions to the Schrödinger wave equation, called wave functions , give only the probability of finding an electron at a given point around the nucleus
The location of the electrons in the quantum mechanical model of the atom is often referred to as an electron cloud  The electron cloud can be thought of in the following way: Imagine placing a square piece of paper on the floor with a dot in the circle representing the nucleus
 If you drop the marker many, many times, the overall pattern of dots will be roughly circular If you aim toward the center reasonably well, there will be more dots near the nucleus and progressively fewer dots as you move away from it Each dot represents a location where the electron could be at any given moment
 An electron cloud has variable densities: a high density where the electron is most likely to be and a low density where the electron is least likely to be ( Figure below )
Figure 5815 An electron cloud: the darker region nearer the nucleus indicates a high probability of finding the electron, while the lighter region further from the nucleus indicates a lower probability of finding the electron
In order to specifically define the shape of the cloud, it is customary to refer to the region of space within which there is a 90% probability of finding the electron This is called an orbital , the three-dimensional region of space that indicates where there is a high probability of finding an electron
Summary
The Schrödinger wave equation replaced the Bohr ideas about electron location with an uncertainty factor The location of the electron can only be given as a probability that the electron is somewhere in a certain area
Practice
Questions
Use the link below to answer the following questions:
http://sciencehowstuffworkscom/atom8htm
What was one problem with the Bohr model of the atom? What did Heisenberg show about electrons? What did Schrödinger derive?
Review
Questions
What does the quantum mechanical view of the atom require? What is a wave function? What does a high density electron cloud suggest?
electron cloud: The location of the electrons in the quantum mechanical model of the atom orbital: The three-dimensional region of space that indicates where there is a high probability of finding an electron quantum mechanical model: A model of the atom that derives from the Schrödinger wave equation and deals with probabilities
Quantum Numbers
Define the four quantum numbers Determine quantum numbers for specific electrons when given appropriate data
Can you guess how many people are in this stadium?
If you attend a college or professional football game, you need a ticket to get in It is very likely that your ticket may specify a gate number, a section number, a row, and a seat number No other ticket can have the same four parts to it It may have the same gate, section, and seat number, but it would have to be in a different row