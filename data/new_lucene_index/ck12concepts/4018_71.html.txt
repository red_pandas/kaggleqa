Reaction mechanisms describe how the material in a chemical reaction gets from the initial reactants to the final products One reaction that illustrates a reaction mechanism is the reaction between nitrogen monoxide and oxygen to form nitrogen dioxide:
It may seem as though this reaction would occur as the result of a collision between two NO molecules with one O 2 molecule However, careful analysis of the reaction has detected the presence of N 2 O 2 during the reaction A proposed mechanism for the reaction consists of two elementary steps:
Step 1:
Step 2:
In the first step, two molecules of NO collide to form a molecule of N 2 O 2  In the second step, that molecule of N 2 O 2 collides with a molecule of O 2 to produce two molecules of NO 2  The overall chemical reaction is the sum of the two elementary steps:
The N 2 O 2 molecule is not part of the overall reaction It was produced in the first elementary step, then reacts in the second elementary step An intermediate is a species which appears in the mechanism of a reaction, but not in the overall balanced equation An intermediate is always formed in an early step in the mechanism and consumed in a later step
Figure 719 Nitrogen dioxide (left) and dinitrogen tetroxide (right)
Summary
The role of intermediates in reaction mechanisms is described
Practice
Read the material at the link below and answer the questions at the end:
http://chemwikiucdavisedu/Physical_Chemistry/Kinetics/Rate_Laws/Reaction_Mechanisms/Reaction_Mechanisms
Review
Questions
What is the intermediate in the reaction described above? Do we see this intermediate in the actual reaction mix? Where do we first see an intermediate in a reaction mechanism? What happens to the intermediate?
intermediate: A species which appears in the mechanism of a reaction, but not in the overall balanced equation
Molecularity
Define molecularity Give reaction examples to illustrate the definition
One piece at a time
Many people enjoy putting jigsaw puzzles together Often these puzzles come in a box, so you have to spread all the pieces out before you start Nowadays you can also find internet sites that have jigsaw puzzles You can choose the level of difficulty, the shape of the pieces – and you can time yourself to see how well you did compare to others that tried the puzzle
 However, in assembling the puzzle, you have a series of elementary steps and the puzzle goes together one piece at a time