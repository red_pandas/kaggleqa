Fertilization joins haploid gametes into a diploid zygote How do gametes end up with half the amount, a haploid amount, of DNA? The mechanism that produces haploid cells is meiosis Meiosis is a type of cell division that halves the number of chromosomes Meiosis is specific to gamete producing cells in the gonads
 These cells eventually differentiate into mature sperm or egg cells During meiosis the pairs of homologous chromosomes separate and segregate randomly to produce gametes with one chromosome from each pair Only germ cells like spermatocytes and oocytes, can undergo meiosis
Figure 5026 Overview of Meiosis During meiosis, homologous chromosomes separate and go to different daughter cells This diagram shows just the nuclei of the cells Notice the exchange of genetic material that occurs prior to the first cell division
Prior to meiosis, the cell’s DNA is replicated, generating chromosomes with two sister chromatids Meiosis involves two nuclear and cell divisions without an interphase in between, so the DNA is not replicated prior to the second round of divisions Each division, named meiosis I and meiosis II, has four stages: prophase, metaphase, anaphase, and telophase, followed by cytokinesis
A human cell prior to meiosis will have 46 chromosomes, 22 pairs of homologous autosomes, and 1 pair of sex chromosomes At the end of meiosis, each haploid cell will have 22 autosomes (not pairs) and 1 sex chromosome, either an X chromosome or a Y chromosome You inherit one chromosome of each pair from your mother and the other one from your father
An overview of meiosis can be seen at http://wwwyoutubecom/watch?v=D1_-mQS_FZ0 (1:49)
Click on the image above for more content
The Eight Phases
Meiosis I begins after DNA replicates during interphase of the cell cycle In both meiosis I and meiosis II , cells go through the same four phases as mitosis - prophase, metaphase, anaphase and telophase However, there are important differences between meiosis I and mitosis The eight stages of meiosis are summarized below
Figure 5027 Phases of Meiosis This flowchart of meiosis shows meiosis I in greater detail than meiosis II Meiosis I—but not meiosis II—differs somewhat from mitosis Compare meiosis I in this flowchart with the earlier figure featuring mitosis How does meiosis I differ from mitosis?
Meiosis I
Meiosis I is referred to as a reductional division; it separates homologous chromosomes, producing two haploid cells The starting diploid cell has 46 chromosomes (23 pairs of homologous chromosomes), each containing two sister chromatids attached at the centromere After cytokinesis at the end of meiosis I, two haploid cells result
 Meiosis I separates the homologous pairs of chromosomes; one from each pair ends up in each resulting cell
Prophase I
Prophase I is the longest phase of meiosis It is very similar to prophase of mitosis, but with one very significant difference In Prophase I, the nuclear envelope breaks down, the chromatin condenses into chromosomes, and the centrioles begin to migrate to opposite poles of the cell, with the spindle fibers growing between them
 These homologous chromosomes line up gene-for-gene down their entire length tetrads , allowing crossing-over to occur This process is also called homologous recombination , and is an important step in creating genetic variation
Prophase I can be further divided into 5 distinct stages: Leptotene, Zygotene, Pachytene, Diplotene and Diakinesis
Leptotene
The first stage of prophase I is the leptotene stage During this stage, chromatin condenses into visible (under the microscope) chromosomes within the nucleus Each chromosome contains two sister chromatids, and each chromosome has a homologue present in the nucleus During this stage, the synaptonemal complex begins to assemble
Zygotene
The zygotene stage occurs as the chromosomes pair with their homologue forming homologous chromosome pairs, a process called synapsis  Homologous chromosomes are equal in size and genetic content, so the genes from the two chromosomes line up along the length of the chromosome Therefore, the pairing is highly specific and exact
Pachytene
The pachytene stage is the stage when chromosomal crossover (crossing-over) occurs This is the process where non-sister chromatids of homologous chromosomes exchange segments over regions of homology At the sites of genetic exchange, chiasmata form This exchange of DNA results in genetic recombination , the formation of new combinations of alleles
Diplotene
The diplotene stage is when the synaptonemal complex degrades and homologous chromosomes disassociate slightly from each other, though they are still bound at the chiasmata (the regions of recombination)
In human fetal oogenesis, all developing oocytes develop to this stage and the stop This happens in all females prior to birth This suspended state of the oocytes is referred to as the dictyotene stage and the eggs remains in this stage until released following puberty
Diakinesis
During the diakinesis stage, chromosomes further condense The chiasmata remain intact during this stage The remainder of this stage is similar to prometaphase of mitosis: the nucleoli disappear, the nuclear membrane disintegrates, and the meiotic spindle begins to form
Metaphase I
In metaphase I, the 23 pairs of homologous chromosomes line up along the equator or the metaphase plate of the cell During mitosis, 46 individual chromosomes line up during metaphase, however during meiosis I, the 23 homologous pairs of chromosomes line up Chromosomes pairs are moved into position by the spindle fibers, which are now attached to the kinetochores at the centromeres
 This is the basis of independent assortment of chromosomes as suggested by Gregor Mendel
Anaphase I
During anaphase I the spindle fibers shorten, and the homologous chromosome pairs are separated from each other This occurs as the chiasmata are severed, pulling homologous chromosomes apart One chromosome from each pair moves toward one pole, with the other moving toward the other pole, resulting in a cell with 23 chromosomes at one pole and the other 23 at the other pole
Because human cells have 23 pairs of chromosomes, this independent assortment of chromosomes produces 2 23 , or 8,388,608 possible chromosome configurations More on independent assortment of chromosomes will be presented in the concepts on Mendelian Genetics
Telophase I
Telophase I followed by cytokinesis ends the first division of meiosis This reduction division results in two haploid cells, each with a unique combination of chromosomes, some from the father and the rest from the mother At this time, each chromosome is comprised of a pair of sister chromatids, even though one has undergone recombination with its homologue
During telophase I, the spindle fiber disassembles and the nucleus reforms The genetic material briefly uncoils back into chromatin This is quickly followed by cytokinesis Cells may enter a period of rest known as interkinesis or interphase II, or immediately enter meiosis II No DNA replication occurs between meiosis I and meiosis II
Meiosis II
Meiosis II includes a division of the chromosomes, similar to that of mitosis This division separates the sister chromatids After cytokinesis, four haploid cells result, each with 23 chromosomes Now each chromosome contains the equivalent of material from one chromatid Thus, when two of these cells join in fertilization, the resulting diploid zygote has just as much DNA as a cell after mitotic cell division
Prophase II
In prophase II, once again the nucleolus disappears and the nucleus breaks down The chromatin condenses into chromosomes The spindle begins to reform as the centrioles move to opposite sides of the cell
Metaphase II
During metaphase II, the spindle fibers align the 23 chromosomes, each made out of two chromatids, along the equator of the cell The new metaphase plate is rotated by 90 degrees when compared to meiosis I, perpendicular to the previous plate
Anaphase II
Anaphase II separates chromatids, similar to anaphase of mitosis During anaphase II, sister chromatids are separated as the centromeres are cleaved The chromatids move to opposite poles of the cell As the chromatids separate, each is known as a chromosome Anaphase II results in a cell with 23 chromosomes at each pole of the cell; each chromosome contains half as much genetic material as at the start of anaphase II
Telophase II
Telophase II and cytokinesis end meiosis During this last phase of meiosis, the nucleus reforms and the spindle fibers break down The chromosomes uncoil into chromatin Each cell undergoes cytokinesis, producing four haploid cells, each with a unique combination of genes and chromosomes
Figure 5028 Meiosis, divided into meiosis I and meiosis II, is a process in which a diploid cell divides itself into four haploid cells Note that meiosis II immediately follows meiosis I; DNA replication does not occur after meiosis I Notice how the chromosomes align in prophase I The alignment of homologous chromosomes allows for crossing-over, a process that increases genetic variability, which will be discussed in more detail in another concept
A detailed look at the phases of meiosis is available at https://wwwyoutubecom/watch?v=ijLc52LmFQg (27:23)
Click on the image above for more content