The table salt pictured in the Figure below contains two elements that are so reactive they are rarely found alone in nature Instead, they undergo chemical reactions with other elements and form compounds Table salt is the compound named sodium chloride (NaCl) It forms when an atom of sodium (Na) gives up an electron and an atom of chlorine (Cl) accepts it
 The two ions are attracted to each and join a matrix of interlocking sodium and chloride ions, forming a crystal of salt
Figure 28102 Table salt (sodium chloride)
Q: Why does sodium give up an electron?
A: An atom of a group 1 element such as sodium has just one valence electron It is “eager” to give up this electron in order to have a full outer energy level, because this will give it the most stable arrangement of electrons You can see how this happens in the animation at the following URL and in the Figure below 
http://wwwvisionlearningcom/img/app/library/objects/Flash/VLObject-1349-030213040210swf
Figure 28103
Q: Why does chlorine accept the electron from sodium?
A: An atom of a group 17 element such as chlorine has seven valence electrons It is “eager” to gain an extra electron to fill its outer energy level and gain stability Group 16 elements with six valence electrons are almost as reactive for the same reason
Atoms of group 18 elements have eight valence electrons (or two in the case of helium) These elements already have a full outer energy level, so they are very stable As a result, they rarely if ever react with other elements Elements in other groups vary in their reactivity but are generally less reactive than elements in groups 1, 2, 16, or 17
Q: Find calcium (Ca) in the periodic table (see Figure above ) Based on its position in the table, how reactive do you think calcium is? Name another element with which calcium might react
A: Calcium is a group 2 element with two valence electrons Therefore, it is very reactive and gives up electrons in chemical reactions It is likely to react with an element with six valence electrons that “wants” to gain two electrons This would be an element in group 6, such as oxygen