Beta decay occurs when an unstable nucleus emits a beta particle and energy A beta particle is either an electron or a positron An electron is a negatively charged particle, and a positron is a positively charged electron (or anti-electron) When the beta particle is an electron, the decay is called beta-minus decay
 Beta-minus decay occurs when a nucleus has too many neutrons relative to protons, and beta-plus decay occurs when a nucleus has too few neutrons relative to protons
Q: Nuclei contain only protons and neutrons, so how can a nucleus emit an electron in beta-minus decay or a positron in beta-plus decay?
A: Beta decay begins with a proton or neutron You can see how in the Figure below 
Figure 29112
Q: How does beta decay change an atom to a different element?
A: In beta-minus decay an atom gains a proton, and it beta-plus decay it loses a proton In each case, the atom becomes a different element because it has a different number of protons