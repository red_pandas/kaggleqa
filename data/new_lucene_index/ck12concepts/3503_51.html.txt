allele : An alternative form or different version of a gene
gene : A segment of DNA that contains information to encode an RNA molecule or a single polypeptide
genetic disorder : A phenotype due to a mutation in a gene or chromosome; a genetic disease
genotype : The genetic makeup of an organism; specifically, the two alleles present
heterozygous : Describes a genotype or individual having two different alleles for a gene
homozygous : Describes a genotype or individual having two copies of the same allele for a gene
loci (singular, locus ): The specific location of a gene or DNA sequence on a chromosome
melanin : The pigment that gives human skin, hair, and eyes their color
phenotype : The physical appearance of an organism determined by a particular genotype (and sometimes also by the environment)