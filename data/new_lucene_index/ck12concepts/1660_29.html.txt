Carbon is a nonmetal in group 14 of the periodic table Like other group 14 elements, carbon has four valence electrons Valence electrons are the electrons in the outer energy level of an atom that are involved in chemical bonds The valence electrons of carbon are shown in the electron dot diagram in the Figure below 
Figure 2945
Q: How many more electrons does carbon need to have a full outer energy level?
A: Carbon needs four more valence electrons, or a total of eight valence electrons, to fill its outer energy level A full outer energy level is the most stable arrangement of electrons
Q: How can carbon achieve a full outer energy level?
A: Carbon can form four covalent bonds Covalent bonds are chemical bonds that form between nonmetals In a covalent bond, two atoms share a pair of electrons By forming four covalent bonds, carbon shares four pairs of electrons, thus filling its outer energy level and achieving stability