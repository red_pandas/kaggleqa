Jupiter is the largest planet in our solar system Jupiter is named for the king of the gods in Roman mythology The Romans named the largest planet for their most important god They followed the tradition of the Greeks, who had similarly named the planet Zeus The Romans built a temple to Jupiter on the hill
Jupiter is truly a giant! The planet has 318 times the mass of Earth, and over 1,300 times Earth’s volume So Jupiter is much less dense than Earth Because Jupiter is so large, it reflects a lot of sunlight When it is visible, it is the brightest object in the night sky besides the Moon and Venus
 The planet is more than five times as far from Earth as the Sun It takes Jupiter about 12 Earth years to orbit once around the Sun
A Ball of Gas and Liquid
Since Jupiter is a gas giant, could a spacecraft land on its surface? The answer is no There is no solid surface at all! Jupiter is made mostly of hydrogen, with some helium, and small amounts of other elements The outer layers of the planet are gas Deeper within the planet, the intense pressure condenses the gases into a liquid
A Stormy Atmosphere
Jupiter's atmosphere is unlike any other in the solar system! The upper layer contains clouds of ammonia The ammonia is different colored bands These bands rotate around the planet The ammonia also swirls around in tremendous storms The Great Red Spot ( Figure below ) is Jupiter's most noticeable feature
 It is more than three times as wide as Earth! Clouds in the storm rotate counterclockwise They make one complete turn every six days or so The Great Red Spot has been on Jupiter for at least 300 years It may have been observed as early as 1664 It is possible that this storm is a permanent feature on Jupiter
Figure 2524 The Great Red Spot has been on Jupiter since we've had telescopes powerful enough to see it
Moons and Rings
Jupiter has lots of moons As of 2012, we have discovered over 66 natural satellites of Jupiter Four are big enough and bright enough to be seen from Earth using a pair of binoculars These four moons were first discovered by Galileo in 1610 They are called the Galilean moons 
 These moons are named Io, Europa, Ganymede, and Callisto The Galilean moons are larger than even the biggest dwarf planets, Pluto and Eris Ganymede is the biggest moon in the solar system It is even larger than the planet Mercury!
Figure 2525 The Galilean moons are as large as small planets
Scientists think that Europa is a good place to look for extraterrestrial life Europa is the smallest of the Galilean moons The moon's surface is a smooth layer of ice Scientists think that the ice may sit on top of an ocean of liquid water How could Europa have liquid water when it is so far from the Sun? Europa is heated by Jupiter
 This could produce enough heat for there to be liquid water Numerous missions have been planned to explore Europa, including plans to drill through the ice and send a probe into the ocean However, no such mission has yet been attempted
Photos from the Voyager missions showed that Jupiter has a ring system This ring system is very faint, so it is very difficult to observe from Earth
Exploration
Jupiter has fascinated scientists at least since Galileo turned his first telescope on the planet In 1979, two spacecrafts, Voyager 1 and Voyager 2, visited Jupiter and its moons The Galileo orbiter orbited the planet for seven years and flew by all of the Galilean moons plus one other moon
 Future missions to Jupiter are underway Juno will arrive in late 2016