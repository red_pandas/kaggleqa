There are six types of simple machines that are the basis of all other machines They are the inclined plane, lever, wedge, screw, pulley, and wheel and axle The six types are pictured in the Figure below  You’ve probably used some of these simple machines yourself Most machines are combinations of two or more simple machines
 An example of a compound machine is a wheelbarrow (see bottom of Figure below ) It consists of two simple machines: a lever and a wheel and axle Many compound machines are much more complex and consist of many simple machines Examples include washing machines and cars
Figure 3079
You can learn more about each of the six types of simple machines at these URLs:
http://wwwengquestorgau/students-sm-upcfm
http://idahoptvorg/dialogue4kids/season7/simplemachines/factscfm