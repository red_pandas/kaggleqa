Lakes, ponds, streams, springs, and wetlands are fresh water bodies The organisms that live in them are part of freshwater ecosystems These ecosystems vary by temperature, pressure (in lakes), and the amount of light that penetrates The type of plants that grow in these ecosystems varies
Lake Ecosystems
Limnology is the study of the plants and animals that live in fresh water bodies A lake has zones just like the ocean The ecosystem of a lake is divided into three distinct zones ( Figure below ):
The surface (littoral) zone is the sloped area closest to the edge of the water The open-water zone (also called the photic zone ) has sunlight The deep-water zone (also called the aphotic zone ) has little or no sunlight
There are several life zones found within a lake:
In the littoral zone , sunlight allows plants to grow Plants provide food and shelter to animals such as snails, insects, and fish Other plants and fish live in the open-water zone These include bass and trout The deep-water zone is dark so there is no photosynthesis Most deep-water organisms are scavengers
 Crabs and catfish are two of these types of organisms Fungi and bacteria are decomposers that live in the deep zone
Figure 2332 The three primary zones of a lake are the littoral, open-water, and deep-water zones
Wetlands
Some of Earth’s freshwater is found in wetlands A wetland is an area that is covered with water, or at least has very soggy soil, during all or part of the year Certain species of plants thrive in wetlands, and they are rich ecosystems Freshwater wetlands are usually found at the edges of streams, rivers, ponds, or lakes
Types of Freshwater Wetlands
Not all wetlands are alike, as you can see below ( Figure below ) Wetlands vary in how wet they are and how much of the year they are soaked Wetlands also vary in the kinds of plants that live in them This depends mostly on the climate where the wetland is found
A marsh is a wetland that is usually under water It has grassy plants, such as cattails A swamp is a wetland that may or may not be covered with water but is always soggy It has shrubs or trees A bog is a wetland that has soggy soil It is generally covered with mosses
Figure 2333 These are just three of many types of wetlands
Importance of Wetlands
People used to think that wetlands were useless Many wetlands were filled in with rocks and soil to create solid land This land was then developed with roads, golf courses, and buildings Now we know that wetlands are very important Laws have been passed to help protect them Why are wetlands so important?
Wetlands have great biodiversity They provide homes or breeding sites to a huge variety of species Because so much wetland area has been lost, many of these species are endangered Wetlands purify water They filter sediments and toxins from runoff before it enters rivers, lakes, and oceans Wetlands slow rushing water
Although the rate of loss has slowed, wetlands are still being destroyed today