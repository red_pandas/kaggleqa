The first archaea described could survive in extremely harsh environments in which no other organisms could survive As a result, archaea are often distinguished by the environment in which they live
Halophiles
The halophiles , which means "salt-loving," live in environments with high levels of salt ( Figure below ) They have been identified in the Great Salt Lake in Utah and in the Dead Sea between Israel and Jordan, which have salt concentrations several times that of the oceans
Figure 59 Halophiles, like the Halobacterium shown here, require high salt concentrations
Thermophiles
The thermophiles live in extremely hot environments For example, they can grow in hot springs, geysers, and near volcanoes Unlike other organisms, they can thrive in temperatures near 100°C, the boiling point of water!
Methanogens
Methanogens can also live in some strange places, such as swamps and inside the guts of cows and termites They help these animals break down cellulose, a tough carbohydrate made by plants ( Figure below ) This is an example of a mutualistic relationship Methanogens are named for their waste product, a gas called methane
Figure 510 Cows are able to digest grass with the help of the methanogens in their gut
Common Environments
Although archaea are known for living in unusual environments, such as the Dead Sea, inside hot springs, and in the guts of cows, they also live in more common environments For example, new research shows that archaea are abundant in the soil They also live among the plankton in the ocean ( Figure below )
Figure 511 Thermococcus gammatolerans are another type of archaea