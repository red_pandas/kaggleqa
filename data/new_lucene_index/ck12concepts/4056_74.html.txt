The and values have been determined for a great many acids and bases, as shown in Tables 215 and 216 These can be used to calculate the pH of any solution of a weak acid or base whose ionization constant is known
Sample Problem: Calculating the pH of a Weak Acid
Calculate the pH of a 200 M solution of nitrous acid (HNO 2 ) The for nitrous acid is 45 × 10 -4 
Step 1: List the known values and plan the problem
Known
initial [HNO 2 ] = 200 M
Unknown
pH = ?
First, an ICE table is set up with the variable used to signify the change in concentration of the substance due to ionization of the acid Then the expression is used to solve for and calculate the pH
Step 2: Solve
The expression and value is used to set up an equation to solve for 
The quadratic equation is required to solve this equation for  However, a simplification can be made because of the fact that the extent of ionization of weak acids is small The value of will be significantly less than 200, so the " ” in the denominator can be dropped
Since the variable represents the hydrogen-ion concentration, the pH of the solution can now be calculated
Step 3: Think about your result
The pH of a 200 M solution of a strong acid would be equal to  The higher pH of the 200 M nitrous acid is consistent with it being a weak acid and therefore not as acidic as a strong acid would be
The procedure for calculating the pH of a solution of a weak base is similar to that of the weak acid in the sample problem However, the variable will represent the concentration of the hydroxide ion The pH is found by taking the negative logarithm to get the pOH, followed by subtracting from 14 to get the pH
Summary
The procedure for calculating the pH of a weak acid or base is illustrated
Practice
Perform the calculations at the site below:
http://wwwsciencegeeknet/APchemistry/APtaters/pHcalculationshtm
Review
Questions
What does stand for in the equation? What simplifying assumption is made? What would stand for if we were calculating pOH?
Neutralization Reaction and Net Ionic Equations for Neutralization Reactions
Define neutralizations reaction Write balanced equations for neutralization reactions Write net ionic equations for neutralization reactions
Who cleans up afterwards?
Pouring concrete and working it are messy jobs In the process, a lot of wastewater with an alkaline pH is generated Often, regulations require that this wastewater be cleaned up at the site One practical way to neutralize the basic pH is to bubble CO 2 into the water The carbon dioxide forms a weak acid (carbonic acid, H 2 CO 3 ) in solution which serves to bring the alkaline pH down to something closer to neutral