The other type of curved mirror, a convex mirror, is shaped like the outside of a bowl Because of its shape, it can gather and reflect light from a wide area As you can see in the Figure below , a convex mirror forms only virtual images that are right-side up and smaller than the actual object
slssie/resources/convex%20mirrorswf
Figure 31129
Q: Convex mirrors are used as side mirrors on cars You can see one in the Figure below  Why is a convex mirror good for this purpose?
A: Because it gathers light over a wide area, a convex mirror gives the driver a wider view of the area around the vehicle than a plane mirror would
Figure 31130