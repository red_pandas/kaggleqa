What is physics ? Physics is the branch of science that studies the physical world, including objects as small as subatomic particles and as large as galaxies It studies the nature of matter and energy and how they interact Physicists are inquisitive people who want to know the causes of what they see
Common Misconceptions
People commonly believe that physics is all about solving word problems and memorizing equations While it is true that many physics classes focus on the equations, it is important to remember that the purpose of physics is less about the problems and more about using equations, laws, and theories to understand the world we live in
Summary
Physics is the branch of science that studies matter and energy and how they interact
Practice
Questions
Canadian astronaut Chris Hadfield filmed an experiment in outer space Watch the video, and answer the questions below
https://wwwyoutubecom/watch?v=lMtXfwk7PXg
Click on the image above for more content
Why can’t Hadfield dip the washcloth in a bag full of water? Pause the video at 1:55 What do you expect will happen as he wrings out the washcloth? What does the water do? Why?
Review
Questions
Give your own definition of physics What do you already know about physics? What do you think you know? Physics is all around us, all the time Give a few examples of physics you have experienced
physics : Physics is the science of matter and energy and of interactions between the two, grouped in traditional fields such as acoustics, optics, mechanics, thermodynamics, and electromagnetism, as well as in modern extensions including atomic and nuclear physics, cryogenics, solid-state physics, particle physics, and plasma physics
Scientific Method
Understand the steps in the scientific method Define hypothesis Differentiate between a theory and a law 
Don Frazier, a NASA chemist, conducting an experiment using a laser imaging system
In science, we need to make observations on various phenomena to form and test hypotheses Some phenomena can be found and studied in nature, but scientists often need to create an experiment  Experiments are tests under controlled conditions designed to demonstrate something scientists already know or to test something scientists wish to know
 The process of designing and performing experiments is a part of the scientific method