Radioactive decay involves the emission of a particle and/or energy as one atom changes into another In most instances, the atom changes its identity to become a new element There are four different types of emissions that occur
Alpha Emission
Alpha (α) decay involves the release of helium ions from the nucleus of an atom This ion consists of two protons and two neutrons and has a 2+ charge Release of an α-particle produces a new atom that has an atomic number two less than the original atom and an atomic weight that is four less
We see a decrease of two in the atomic number (uranium to thorium) and a decrease of four in the atomic weight (238 to 234) Usually the emission is not written with atomic number and weight indicated since it is a common particle whose properties should be memorized Quite often the alpha emission is accompanied by gamma (γ) radiation, a form of energy release
Figure 773 Emission of an alpha particle from the nucleus
Beta Emission
Beta (β) decay is a more complicated process Unlike the α-emission, which simply expels a particle, the β-emission involves the transformation of a neutron in the nucleus to a proton and an electron The electron is then ejected from the nucleus In the process, the atomic number increases by one while the atomic weight stays the same
Figure 774 Beta emission
A typical beta decay process involves carbon-14, often used in radioactive dating techniques The reaction forms nitrogen-14 and an electron:
Again, the beta emission is usually simply indicated by the Greek letter β; memorization of the process is necessary in order to follow nuclear calculations in which the Greek letter β without further notation
Gamma Emission
Gamma (γ) radiation is simply energy It may be released by itself or more commonly in association with other radiation events There is no change of atomic number or atomic weight in a simple γ-emission Often, an isotope may produce γ-radiation as a result of a transition in a metastable isotope
 The composition of the atom is not altered, but the nucleus could be considered more “comfortable” after the shift This shift increases the stability of the isotope from the energetically unstable (or “metastable”) isotope to a more stable form of the nucleus
Positron Emission
A positron is a positive electron (a form of antimatter) This rare type of emission occurs when a proton is converted to a neutron and a positron in the nucleus, with ejection of the positron The atomic number will decrease by one while the atomic weight does not change A positron is often designated by β + 
Carbon-11 emits a positron to become boron-11:
Summary
Radioactive decay processes are described
Practice
Read the material at the link below and answer the questions at Practice Problem 3
Review
Questions
What is gamma emission? What isotope is formed when U-238 emits an alpha particle? What isotope produces boron-11 when it emits a positron?
alpha decay: Emission of two protons plus two neutrons beta decay: Emission of an electron gamma emission: Emission of energy positron emission: Emission of a positive electron
Detection of Radioactivity
List and define terms used to discuss the amount of radioactivity present Describe devices used to measure radioactivity
Invest in uranium!
Uranium is going to be worth more on the open market after a steep decline in prices several years ago So people are out there looking for more uranium But they are not waving Geiger counters around to find this material All the surface uranium has been found Instead, they must study geology, such as the rocks in the picture above
 No more clicking – just modern-day geology and chemistry