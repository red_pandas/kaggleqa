A natural resource is anything in nature that humans need Metals and fossil fuels are natural resources But so are water, sunlight, soil, and wind Even living things are natural resources
Using Natural Resources
We need natural resources for just about everything we do We need them for food and clothing, for building materials, and energy We even need them to have fun Listed below are examples of how we use natural resources ( Table below ) Can you think of other ways we use natural resources?
Click on the image above to view the table
Some natural resources are renewable Others are non-renewable It depends on what they are and how we use them