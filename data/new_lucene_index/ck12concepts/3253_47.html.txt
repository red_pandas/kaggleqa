cell : The basic unit of structure and function of all living organisms
compound microscope : An optical microscopes that has a series of lenses; has uses in many fields of science, particularly biology and geology
microscope : An instrument used to view objects that are too small to be seen by the naked eye
microscopist : A scientist who specializes in research with the use of microscopes