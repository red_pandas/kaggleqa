Four unifying principles form the basis of biology Whether biologists are interested in ancient life, the life of bacteria, or how humans could live on the moon, they base their overall understanding of biology on these four principles:
cell theory gene theory homeostasis evolution
The Cell Theory
According to the cell theory , all living things are made up of cells, which is the structural unit of living organisms, and living cells always come from other living cells In fact, each living thing begins life as a single cell Some living things, such as bacteria, remain single-celled
 Your own body is made up of an amazing 100 trillion cells! But even you—like all other living things—began life as a single cell
Figure 328 Tiny diatoms and whale sharks are all made of cells Diatoms are about 20 µm in diameter and are made up of one cell, whereas whale sharks can measure up to 12 meters in length and are made up of billions of cells
The Gene Theory
The gene theory is the idea that the characteristics of living organisms are controlled by genes, which are passed from parents to their offspring A gene is a segment of DNA that has the instructions to encode a protein Genes are located on larger structures, called chromosomes , that are found inside every cell
 Molecules of DNA are encoded with instructions that tell cells what to do To see how this happens, click on the animation titled Journey into DNA at the following link: http://wwwpbsorg/wgbh/nova/genome/dnahtml 
Homeostasis
Homeostasis , which is maintaining a stable internal environment or keeping things constant, is not just a characteristic of living things It also applies to nature as a whole Consider the concentration of oxygen in Earth’s atmosphere Oxygen makes up 21% of the atmosphere, and this concentration is fairly constant
 Most living things need oxygen to survive, and when they breathe, they remove oxygen from the atmosphere On the other hand, many living things, including plants, give off oxygen when they make food, and this adds oxygen to the atmosphere The concentration of oxygen in the atmosphere is maintained mainly by the balance between these two processes
youtubecom/watch?v=DFyt7FJn-UM 
Evolution
Evolution is a change in the characteristics of living things over time Evolution occurs by a process called natural selection In natural selection, some living things produce more offspring than others, so they pass more genes to the next generation than others do Over many generations, this can lead to major changes in the characteristics of living things
As living things evolve, they generally become better suited for their environment This is because they evolve adaptations An adaptation is a characteristic that helps a living thing survive and reproduce in a given environment Look at the mole in Figure below  It has tentacles around its nose that it uses to sense things by touch
 However, by using its touch organ, it can detect even tiny food items in the soil in total darkness The touch organ is an adaptation because it helps the mole survive in its dark, underground environment
Figure 329 This mole uses its star-shaped nose organ to sense food by touch in the dark The mole’s very large front claws are also an adaptation for its life in the soil Can you explain why?