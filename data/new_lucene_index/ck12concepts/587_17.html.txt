Seismic waves are the energy from earthquakes Seismic waves move outward in all directions away from their source Each type of seismic wave travels at different speeds in different materials All seismic waves travel through rock, but not all travel through liquid or gas Geologists study seismic waves to learn about earthquakes and the Earth’s interior
Wave Structure
Seismic waves are just one type of wave Sound and light also travel in waves Every wave has a high point called a crest and a low point called a trough  The height of a wave from the center line to its crest is its amplitude  The horizontal distance between waves from crest to crest (or trough to trough) is its wavelength ( Figure below )
Figure 1732 The energy from earthquakes travels in waves, such as the one shown in this diagram
Types of Seismic Waves
There are two major types of seismic waves Body waves travel through the Earth’s interior Surface waves travel along the ground surface In an earthquake, body waves are responsible for the sharp jolts Surface waves are responsible for the rolling motions that do most of the damage in an earthquake
Body Waves
Primary waves (P-waves) and secondary waves (S-waves) are the two types of body waves ( Figure below ) Body waves move at different speeds through different materials
P-waves are faster They travel at about 6 to 7 kilometers (about 4 miles) per second Primary waves are so named because they are the first waves to reach a seismometer P-waves squeeze and release rocks as they travel The material returns to its original size and shape after the P-wave goes by
 P-waves travel through solids, liquids, and gases
S-waves are slower than P-waves They are the second waves to reach a seismometer S-waves move up and down They change the rock’s shape as they travel S-waves are about half as fast as P-waves, at about 35 km (2 miles) per second S-waves can only move through solids This is because liquids and gases don’t resist changing shape
Figure 1733 P-waves and S-waves are the two types of body waves
Surface Waves
Figure 1734 Love waves and Rayleigh waves are the two types of surface waves
Surface waves travel along the ground outward from an earthquake’s epicenter Surface waves are the slowest of all seismic waves They travel at 25 km (15 miles) per second There are two types of surface waves Love waves move side-to-side, much like a snake Rayleigh waves produce a rolling motion as they move up and backward ( Figure above )
 They also cause objects to sway back and forth These motions cause damage to rigid structures during an earthquake