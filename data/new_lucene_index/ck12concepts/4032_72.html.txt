Solubility is normally expressed in g/L of saturated solution However, solubility can also be expressed as the moles per liter Molar solubility is the number of moles of solute in one liter of saturated solution In other words, the molar solubility of a given compound represents the highest molarity solution that is possible for that compound
 Given that the solubility of Zn(OH) 2 is 42 × 10 -4 g/L, the molar solubility can be calculated as shown below:
Solubility data can be used to calculate the for a given compound The following steps need to be taken
Convert from solubility to molar solubility Use the dissociation equation to determine the concentration of each of the ions in mol/L Apply the equation
Sample Problem: Calculating from Solubility
The solubility of lead(II) fluoride is found experimentally to be 0533 g/L Calculate the for lead(II) fluoride
Step 1: List the known quantities and plan the problem 
Known
solubility of PbF 2 = 0533 g/L molar mass = 24520 g/mol
Unknown
of PbF 2 = ?
The dissociation equation for PbF 2 and the corresponding expression
The steps above will be followed to calculate the for PbF 2 
Step 2: Solve 
The dissociation equation shows that for every mole of PbF 2 that dissociates, 1 mol of Pb 2+ and 2 mol of F − are produced Therefore, at equilibrium the concentrations of the ions are:
Substitute into the expression and solve for the 
Step 3: Think about your result 
The solubility product constant is significantly less than 1 for a nearly insoluble compound such as PbF 2 
Summary
Molar solubility calculations are described Calculations of using molar solubility are described
Practice
Read the material at the link below and do the problems at the end:
http://wwwchemteaminfo/Equilibrium/Calc-Ksp-FromMolSolubhtml
Review
Questions
What are the solution requirements for determining molar solubility? Why do we need to convert mass to molarity to determine ? What values would you expect for very insoluble compounds?
molar solubility: The number of moles of solute in one liter of saturated solution
Conversion of Ksp to Solubility
Perform calculations converting to solubility
How do you purify water?
Purification of water for drinking and other uses is a complicated process Heavy metals need to be removed, a process accomplished by addition of carbonates and sulfates Lead contamination can present major health problems, especially for younger children Lead sulfates and carbonates are very insoluble, so will precipitate out of solution very easily