Doppler Shifts :
Guidance The Doppler Effect occurs when either the source of a wave or the observer of a wave (or both) are moving When a source of a wave is moving towards you, the apparent frequency of the wave you detect is higher than that emitted For instance, if a car approaches you while playing a note at 500 Hz, the sound you hear will be slightly higher frequency
 It’s important to note that the speed of the wave does not change –it’s traveling through the same medium so the speed is the same Due to the relative motion between the source and the observer the frequency changes, but not the speed of the wave Note that while the effect is similar for light and electromagnetic waves the formulas are not exactly the same as for sound
Example 1
Question : How fast would a student playing an A note ( ) have to move towards you in order for you to hear a G note ( )?
Answer : We will use the Doppler shift equation for when the objects are getting closer together and solve for the speed of the student (the source)
Now we plug in the known values to solve for the velocity of the student