All objects on Earth accelerate downward due to gravity at a rate of 98 m/s 2  Therefore, if you know the mass of an object, you can calculate its weight using this equation:
F = m × 98 m/s 2
Q : Sam has a mass of 50 kilograms What is his weight in Newtons?
A : You can calculate Sam’s weight in Newtons by substituting his mass in kilograms into the weight formula:
F = m × 98 m/s 2 = 50 kg × 98 m/s 2 = 490 kg *bull; m/s 2 , or 490 N
You’re probably more familiar with weight in pounds (lb) than in Newtons One Newton equals 0225 pounds In other words, there are 0225 pounds per Newton You can use this ratio to convert Newtons to pounds
Q : What is Sam’s weight in pounds?
A : In pounds, Sam’s weight is 490 N x 0225 lb/N = 110 lb