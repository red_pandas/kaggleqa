After implantation occurs, the blastocyst is called an embryo  The embryonic stage lasts through the eighth week following fertilization During this time, the embryo grows in size and becomes more complex It develops specialized cells and tissues and starts to form most organs For an interactive animation of embryonic development, go to this link: http://health
com/adam-200129htm 
Formation of Cell Layers
During the second week after fertilization, cells in the embryo migrate to form three distinct cell layers, called the ectoderm, mesoderm , and endoderm  Each layer will soon develop into different types of cells and tissues, as shown in Figure below 
Figure 44107 Cell Layers of the Embryo The migration of cells into three layers occurs in the 2-week-old embryo What organs eventually develop from the ectoderm cell layer? Which cell layer develops into muscle tissues?
Differentiation of Cells
A zygote is a single cell How does a single cell develop into many different types of cells? During the third week after fertilization, the embryo begins to undergo cellular differentiation Differentiation is the process by which unspecialized cells become specialized As illustrated in Figure below , differentiation occurs as certain genes are expressed ("switched on") while other genes are switched off
 You can explore cell differentiation by watching the video at this link: http://videoshowstuffworkscom/hsw/10313-the-cell-cell-differentiation-videohtm 
Figure 44108 Cellular differentiation occurs in the 3-week-old embryo
Organ Formation
After cells differentiate, all the major organs begin to form during the remaining weeks of embryonic development A few of the developments that occur in the embryo during weeks 4 through 8 are listed in Figure below  As the embryo develops, it also grows in size By the eighth week of development, the embryo is about 30 millimeters (just over 1 inch) in length
Figure 44109 Embryonic Development (Weeks 4–8) Most organs develop in the embryo during weeks 4 through 8 If the embryo is exposed to toxins during this period, the effects are likely to be very damaging Can you explain why? (Note: the drawings of the embryos are not to scale)