Hydrogen is an example of an element that has isotopes Three isotopes of hydrogen are modeled in the Figure below  Most hydrogen atoms have just one proton and one electron and lack a neutron These atoms are just called hydrogen Some hydrogen atoms have one neutron as well These atoms are the isotope named deuterium
 These atoms are the isotope named tritium For animated versions of these hydrogen isotopes, go to this URL: http://wwws-coolcouk/a-level/physics/atomic-structure/revise-it/isotopes 
Figure 2871
Q: The mass number of an atom is the sum of its protons and neutrons What is the mass number of each isotope of hydrogen shown in the Figure above ?
A: The mass numbers are: hydrogen = 1, deuterium = 2, and tritium = 3