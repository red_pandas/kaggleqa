There are three types of mechanical waves: transverse, longitudinal, and surface waves They differ in how particles of the medium move You can see this in the Figure below and in the animation at the following URL http://wwwacspsuedu/drussell/Demos/waves/wavemotionhtml
Figure 3146
In a transverse wave, particles of the medium vibrate up and down perpendicular to the direction of the wave In a longitudinal wave, particles of the medium vibrate back and forth parallel to the direction of the wave In a surface wave, particles of the medium vibrate both up and down and back and forth, so they end up moving in a circle
Q: How do you think surface waves are related to transverse and longitudinal waves?
A: A surface wave is combination of a transverse wave and a longitudinal wave