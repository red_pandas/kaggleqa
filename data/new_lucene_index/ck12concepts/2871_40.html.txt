Angiosperms , or flowering seed plants, form seeds in ovaries As the seeds develop, the ovaries may develop into fruits Flowers attract pollinators, and fruits encourage animals to disperse the seeds
Parts of a Flower
A flower consists of male and female reproductive structures The main parts of a flower are shown in Figure below  They include the stamen, pistil, petals, and sepals
The stamen is the male reproductive structure of a flower It consists of a stalk-like filament that ends in an anther  The anther contains pollen sacs, in which meiosis occurs and pollen grains form The filament raises the anther up high so its pollen will be more likely to blow in the wind or be picked up by an animal pollinator
 It consists of a stigma, style , and ovary  The stigma is raised and sticky to help it catch pollen The style supports the stigma and connects it to the ovary, which contains the egg Petals attract pollinators to the flower Petals are often brightly colored so pollinators will notice them
 Sepals are usually green, which camouflages the bud from possible consumers
Figure 4021 A flower includes both male and female reproductive structures
Flowers and Pollinators
Many flowers have bright colors, strong scents, and sweet nectar to attract animal pollinators They may attract insects, birds, mammals, and even reptiles While visiting a flower, a pollinator picks up pollen from the anthers When the pollinator visits the next flower, some of the pollen brushes off on the stigma
See The beauty of pollination at http://wwwyoutubecom/watch?v=xHkq1edcbk4 for an amazing look at this process
Click on the image above for more content
Other Characteristics of Flowering Plants
Although flowers and their components are the major innovations of angiosperms, they are not the only ones Angiosperms also have more efficient vascular tissues Additionally, in many flowering plants the ovaries ripen into fruits Fruits are often brightly colored, so animals are likely to see and eat them and disperse their seeds (see Figure below )
Figure 4022 Brightly colored fruits attract animals that may disperse their seeds It’s hard to miss the bright red apples on these trees
Evolution of Flowering Plants
Flowering plants are thought to have evolved at least 200 million years ago from gymnosperms like Gnetae The earliest known fossils of flowering plants are about 125 million years old The fossil flowers have male and female reproductive organs but no petals or sepals
Scientists think that the earliest flowers attracted insects and other animals, which spread pollen from flower to flower This greatly increased the efficiency of fertilization over wind-spread pollen, which might or might not actually land on another flower To take better advantage of this “animal labor,” plants evolved traits such as brightly colored petals to attract pollinators
Giving free nectar to any animal that happened to come along was not an efficient use of resources Much of the pollen might be carried to flowers of different species and therefore wasted As a result, many plants evolved ways to “hide” their nectar from all but very specific pollinators, which would be more likely to visit only flowers of the same species
 Two examples of this type of co-evolution are shown in Figure below 
Figure 4023 The hummingbird has a long narrow bill to reach nectar at the bottom of the tube-shaped flowers The bat is active at night, so bright white, night-blooming flowers attract it In each case, the flowering plant and its pollinator co-evolved to become better suited for their roles in the symbiotic relationship
Some of the most recent angiosperms to evolve are grasses Humans started domesticating grasses such as wheat about 10,000 years ago Why grasses? They have many large, edible seeds that contain a lot of nutritious stored food They are also relatively easy to harvest Since then, humans have helped shaped the evolution of grasses, as illustrated by the example in Figure below 
 What other grass seeds do you eat?
Figure 4024 The plant on the left, called teosinte, is the ancestor of modern, domesticated corn, shown on the right An intermediate stage is pictured in the middle How were humans able to change the plant so dramatically?
Classification of Flowering Plants
There are more than a quarter million species of flowering plants, and they show tremendous diversity Nonetheless, almost all flowering plants fall into one of three major groups: monocots, eudicots , or magnolids  The three groups differ in several ways For example, monocot embryos form just one cotyledon , whereas eudicot and magnolid embryos form two cotyledons
 Examples of the three groups of flowering plants are given in Table below 
Click on the image above to view the table