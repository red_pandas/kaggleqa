When a stream or river slows down, it starts dropping its sediments Larger sediments are dropped in steep areas, but smaller sediments can still be carried Smaller sediments are dropped as the slope becomes less steep
Alluvial Fans
In arid regions, a mountain stream may flow onto flatter land The stream comes to a stop rapidly The deposits form an alluvial fan ( Figure below )
Figure 2134 (A) Alluvial fans in Death Valley, California (B) Nile River Delta in Egypt
Deltas
Deposition also occurs when a stream or river empties into a large body of still water In this case, a delta forms A delta is shaped like a triangle It spreads out into the body of water An example is pictured below ( Figure above )
Deposition by Flood Waters
A flood occurs when a river overflows it banks This might happen because of heavy rains
Floodplains
As the water spreads out over the land, it slows down and drops its sediment If a river floods often, the floodplain develops a thick layer of rich soil because of all the deposits That’s why floodplains are usually good places for growing plants For example, the Nile River in Egypt provides both water and thick sediments for raising crops in the middle of a sandy desert
Natural Levees
A flooding river often forms natural levees along its banks A levee ( Figure below ) is a raised strip of sediments deposited close to the water’s edge Levees occur because floodwaters deposit their biggest sediments first when they overflow the river’s banks
Figure 2135 This diagram shows how a river builds natural levees along its banks