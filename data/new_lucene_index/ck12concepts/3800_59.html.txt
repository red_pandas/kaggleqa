The size of atoms is important when trying to explain the behavior of atoms or compounds  One of the ways we can express the size of atoms is with the atomic radius   This data helps us understand why some molecules fit together and why other molecules have parts that get too crowded under certain conditions
The size of an atom is defined by the edge of its orbital However, orbital boundaries are fuzzy and in fact are variable under different conditions In order to standardize the measurement of atomic radii, the distance between the nuclei of two identical atoms bonded together is measured The atomic radius is defined as one-half the distance between the nuclei of identical atoms that are bonded together
Figure 5923 The atomic radius (r) of an atom can be defined as one half the distance (d) between two nuclei in a diatomic molecule
Atomic radii have been measured for elements The units for atomic radii are picometers, equal to 10 -12 meters As an example, the internuclear distance between the two hydrogen atoms in an H 2 molecule is measured to be 74 pm Therefore, the atomic radius of a hydrogen atom is 
Figure 5924 Atomic radii of the representative elements measured in picometers
Periodic Trend
The atomic radius of atoms generally decreases from left to right across a period There are some small exceptions, such as the oxygen radius being slightly greater than the nitrogen radius Within a period, protons are added to the nucleus as electrons are being added to the same principal energy level
 Since the force of attraction between nuclei and electrons increases, the size of the atoms decreases The effect lessens as one moves further to the right in a period because of electron-electron repulsions that would otherwise cause the atom’s size to increase
Group Trend
The atomic radius of atoms generally increases from top to bottom within a group As the atomic number increases down a group, there is again an increase in the positive nuclear charge However, there is also an increase in the number of occupied principle energy levels Higher principal energy levels consist of orbitals which are larger in size than the orbitals from lower energy levels
Figure 5925 A graph of atomic radius plotted versus atomic number Each successive period is shown in a different color As the atomic number increases within a period, the atomic radius decreases
Summary
Atomic radius is determined as the distance between the nuclei of two identical atoms bonded together The atomic radius of atoms generally decreases from left to right across a period The atomic radius of atoms generally increases from top to bottom within a group
Practice
Questions
Use the link below to answer the following questions:
http://chemwikiucdavisedu/Inorganic_Chemistry/Descriptive_Chemistry/Periodic_Table_of_the_Elements/Atomic_Radi
What influences the atomic size of an atom? What is a covalent radius? What is an ionic radius?
Review
Questions
Define “atomic radius” What are the units for measurement of atomic radius? How does the atomic radius change across a period? How does atomic radius change from top to bottom within a group? Explain why the atomic radius of hydrogen is so much smaller that the atomic radius for potassium
atomic radius: The atomic radius is defined as one-half the distance between the nuclei of identical atoms that are bonded together
Periodic Trends: Ionization Energy
Define ionization energy Describe factors affecting ionization energy Describe how ionization energy changes across a period Describe how ionization energy changes down a group
Why do sheep travel in herds?
Like many other animals, sheep travel in herds  The tendency is for each individual sheep to stay with the herd However, a sheep may sometimes wander off, depending on how strong the attraction is for a particular food or water supply  At other times, a sheep may become frightened and run off
There is an on-going tension between the electrons and protons in an atom  Reactivity of the atom depends in part on how easily the electrons can be removed from the atom  We can measure this quantity and use it to make predictions about the behaviors of atoms