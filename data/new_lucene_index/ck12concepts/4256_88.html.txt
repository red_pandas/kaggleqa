Guidance You can use energy conservation in rotation problems just like you did in the energy conservation lessons for linear motion Note that rotating objects also have kinetic energy of rotation and this must be included when accounting for energy conservation
Example 1
A hoop of mass 1 kg and radius 75 m, begins at rest at the top of a ramp, 3m above the ground What is it's rotational velocity at the bottom of the ramp if the hoop rolls down the ramp without slipping?
Solution
To solve this problem, we'll apply energy conservation to the hoop The hoop only has gravitational potential energy at the top of the ramp, and it has both rotational and linear kinetic energy at the bottom of the ramp