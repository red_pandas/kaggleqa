Fog ( Figure below ) is a cloud on the ground Fog forms when humid air near the ground cools below its dew point Each type of fog forms in a different way
Radiation fog forms at night Skies are clear, and the relative humidity is high The ground cools as temperature goes down Air near the ground cools below its dew point Tule fog is an extreme form of radiation fog This type of fog strikes the Central Valley of California in winter
 Warm, moist air comes in from the Pacific Ocean The air blows over the cold California current When the air cools below its dew point, fog forms Sea breezes bring the fog onshore Steam fog appears in autumn when cool air moves over a warm lake Water evaporates from the lake surface
 The fog appears like steam Warm humid air travels up a hillside and cools below its dew point to create upslope fog 
Figure 205 (a) Tule fog in the Central Valley of California (b) Advection fog in San Francisco (c) Steam fog over a lake in Algonquin Park, Canada (d) Upslope fog around the peak of Sanqing Mountains in China
Fog levels are declining along the California coast as climate warms The change in fog may have big ecological changes for the state
Learn more at: http://wwwkqedorg/quest/television/science-on-the-spot-science-of-fog 
Click on the image above for more content