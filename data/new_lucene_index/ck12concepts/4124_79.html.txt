Another category of lipid molecule is waxes Waxes are esters of long-chain fatty acids and long-chain alcohols  Waxes are soft solids with generally low melting points and are insoluble in water  The Figure below shows the structure of cetyl palmitate, a natural wax present in sperm whales
Figure 7926 Cetyl palmitate belongs to the category of compounds called waxes It is derived from a fatty acid that is 15 carbons in length and an alcohol that contains 16 carbon atoms
One of the best known natural waxes is beeswax, though many other animals and plants synthesize waxes naturally  Waxes can be found on leaves of plants and on the skin, hair, or feathers of animals, where they function to keep these structures pliable and waterproof  Humans take advantage of the protective properties of natural and synthetic waxes in such applications as floor polish and car wax
Summary
The structure of waxes is shown Properties of waxes are described Common waxes are listed
Practice
Questions
Read the material at the link below and answer the following questions:
http://lipidlibraryaocsorg/lipids/waxes/indexhtm
Besides wax esters, what other organic components can be found in waxes? What is another name for wool wax? What are the three major non-polar lipids on the skin surface of humans?
Review
Questions
What is a wax? Name two properties of waxes Name a wax found in sperm whales
wax: Ester of long-chain fatty acids and long-chain alcohols
Nucleic Acids
Define nucleic acid Define nucleotide Describe the structures of common nucleotides
Treating Cancer with Chemistry
Cancer treatment is a complex and challenging effort Cancer cells grow without the usual controls that act on normal cells  One approach to treating cancer is to alter the structure of the DNA in order to slow down or stop the growth of the abnormal cells Compounds that structurally resemble the normal building blocks of DNA have been shown to be very effective in stopping some forms of cancer from spreading throughout the body