; Period is the inverse of frequency
; Period of mass m on a spring with constant k
; the force of a spring equals the spring constant multiplied by the amount the spring is stretched or compressed from its equilibrium point The negative sign indicates it is a restoring force (ie direction of the force is opposite its displacement from equilibrium position
; the potential energy of a spring is equal to one half times the spring constant times the distance squared that it is stretched or compressed from equilibrium
Guidance The oscillating object does not lose any energy in SHM Friction is assumed to be zero In harmonic motion there is always a restorative force, which attempts to restore the oscillating object to its equilibrium position The restorative force changes during an oscillation and depends on the position of the object
 In SHM, is the time it takes the object to return to its exact starting point and starting direction The frequency, is the number of cycles an object goes through in second Frequency is measured in Hertz  cycle per sec The amplitude, , is the distance from the equilibrium (or center) point of motion to either its lowest or highest point ( end points )
 The amplitude can vary in harmonic motion, but is constant in SHM
Example 1
Click on the image above for more content