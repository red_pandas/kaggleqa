Anions are negative ions that are formed when a nonmetal atom gains one or more electrons Anions are so named because they are attracted to the anode (positive field) in an electric field  Atoms typically gain electrons so that they will have the electron configuration of a noble gas All the elements in Group 17 have seven valence electrons due to the outer ns 2 np 5 configuration
 Likewise, Group 16 elements form ions with a 2− charge, and the Group 15 nonmetals form ions with a 3− charge
Naming anions is slightly different than naming cations The ending of the element’s name is dropped and replaced with the – ide suffix For example, F - is the fluoride ion, while O 2- is the oxide ion  As is the case with cations, the charge on the anion is indicated by a superscript following the symbol
Uses for Anions
Fluoride ion is widely used in water supplies to help prevent tooth decay Chloride is an important component in ion balance in blood Iodide ion is needed by the thyroid gland to make the hormone thyroxine
Summary
Anions are formed by the addition of one or more electrons to the outer shell of an atom Group 17 elements add one electron to the outer shell, group 16 elements add two electrons, and group 15 elements add three electrons Anions are named by dropping the ending of the element’s name and adding - ide
Practice
Questions
Use the link below to answer the following questions:
http://preparatorychemistrycom/bishop_anion_names_formulas_helphtm
Why do elements form anions? Why do group 17 elements form anions more readily than group 1 elements?
Review
Questions
What is an anion? How are anions formed? Why do anions form? How are anions named? List three examples of anions with names, charges, and chemical symbols List three ways anions are used
anion: Negative ions that are formed when a nonmetal atom gains one or more electrons anode: Positively charged electrode, when electric current runs through a cathode ray tube nonmetal: Lacking the chemical and physical properties of metals
Transition Metal Ions
Describe the electron configuration of the transition metals Explain how transition metals form ions List uses for transition metal ions
What kind of coin is this?
Most of us are familiar with the common coins: penny, nickel, quarter  In some areas (such as Las Vegas), you might see large amounts of silver dollars (these get a little heavy in your pocket)  But most of us have probably never seen a platiunum eagle – an eagle coin, but one that is held primarily by collectors
  It would also be awkward and annoying if you lost one of these coins out of your pocket  Platinum is just one of several transition metals that is worth a lot of money (gold is another one)
The group 1 and 2 elements form cations through a simple process that involves the loss of one or more outer shell electrons  These electrons come from the s orbital and are removed very readily