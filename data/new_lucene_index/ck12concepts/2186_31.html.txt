Constructive interference occurs when the crests, or highest points, of one wave overlap the crests of the other wave You can see this in the Figure below  As the waves pass through each other, the crests combine to produce a wave with greater amplitude You can see an animation of constructive interference at this URL: http://phys23p
psuedu/phys_anim/waves/embederQ120100html
Figure 3166