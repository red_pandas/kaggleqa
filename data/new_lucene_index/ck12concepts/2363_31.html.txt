You can use the equation for current (above) to calculate the amount of current flowing through a circuit when the voltage and resistance are known Consider an electric wire that is connected to a 12-volt battery If the wire has a resistance of 2 ohms, how much current is flowing through the wire?
Current = = 6 amps
Q: If a 120-volt voltage source is connected to a wire with 10 ohms of resistance, how much current is flowing through the wire?
A: Substitute these values into the equation for current:
Current = = 12 amps