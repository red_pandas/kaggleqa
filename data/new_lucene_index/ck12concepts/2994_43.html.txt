Birds live and breed in most terrestrial habitats on all seven continents, from the Arctic to Antarctica Because they are endothermic, birds can live in a wider range of climates than reptiles or amphibians, although the greatest diversity of birds occurs in tropical regions Birds are important members of every ecosystem in which they live, occupying a wide range of ecological positions
Bird Diets
Some birds are generalists A generalist is an organism that can eat many different types of food Other birds are highly specialized in their food needs and can eat just one type of food
Raptors such as hawks and owls are carnivores They hunt and eat mammals and other birds Vultures are scavengers  They eat the remains of dead animals, such as roadkill Aquatic birds generally eat fish or water plants Perching birds may eat insects, fruit, honey, or nectar Many fruit-eating birds play a key role in seed dispersal, and some nectar-feeding birds are important pollinators 
Bird beaks are generally adapted for the food they eat For example, the sharp, hooked beak of a raptor is well suited for killing and tearing apart prey The long beak of the hummingbird in Figure below co-evolved with the tube-shaped flowers from which it sips nectar
Figure 4338 Hummingbird Sipping Nectar A hummingbird gets nectar from flowers and pollinates the flowers in return What type of relationship exists between the bird and the flowering plant?
Birds at Risk
Hundreds of species of birds have gone extinct as a result of human actions A well-known example is the passenger pigeon It was once the most common bird in North America, but overhunting and habitat destruction led to its extinction in the 1800s Habitat destruction and use of the pesticide DDT explain the recent extinction of the dusky seaside sparrow
Today, some 1,200 species of birds are threatened with extinction by human actions Humans need to take steps to protect this precious and important natural resource What can you do to help?
The Golden Eagle
Although not as famous as its bald cousin, Golden Eagles are much easier to find in Northern California - one of the largest breeding populations for Golden Eagles The largest of the raptors, Golden Eagles weigh typically between 8 and 12 pounds, and their wing span is around 6 to 7 feet
kqedorg/quest/television/cool-critters-the-golden-eagle 
Click on the image above for more content
The Great Horned Owl
Owls are amazing creatures They have many adaptations that allow them to thrive in their environments Their claws are enormous and powerful, they have excellent hearing, and fantastic vision in low light And the Great Horned Owl can fly almost silently due to "fringes" on their feathers that help to break up the sound of air passing over their wings
kqedorg/quest/video/cool-critters-great-horned-owls/ 
Click on the image above for more content