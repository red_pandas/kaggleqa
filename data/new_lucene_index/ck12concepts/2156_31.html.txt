A longitudinal wave is a type of mechanical wave A mechanical wave is a wave that travels through matter, called the medium In a longitudinal wave, particles of the medium vibrate in a direction that is parallel to the direction that the wave travels You can see this in the Figure below 
 The energy of this disturbance passes through the coils of the spring to the other end You can see a video of a longitudinal wave in a spring at this URL: http://wwwyoutubecom/watch?v=ubRlaCCQfDk 
Click on the image above for more content
Figure 3150