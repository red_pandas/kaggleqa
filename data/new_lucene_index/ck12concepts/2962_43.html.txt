There are about 28,000 existing species of fish, and they are placed in five different classes The classes are commonly referred to as hagfish, lampreys, cartilaginous fish, ray-finned fish, and lobe-finned fish (see the table in the previous lesson)
Hagfish
Hagfish are very primitive fish They retain their notochord throughout life rather than developing a backbone, and they lack scales and fins They are classified as vertebrates mainly because they have a cranium Hagfish are noted for secreting large amounts of thick, slimy mucus The mucus makes them slippery, so they can slip out of the jaws of predators
Lampreys
Like hagfish, lampreys also lack scales, but they have fins and a partial backbone The most striking feature of lampreys is a large round sucker, lined with teeth, that surrounds the mouth (see Figure below ) Lampreys use their sucker to feed on the blood of other fish species
Figure 4311 Sucker Mouth of a Lamprey The mouth of a lamprey is surrounded by a tooth-lined sucker
Cartilaginous Fish
Cartilaginous fish include sharks, rays, and ratfish (see Figure below ) In addition to an endoskeleton composed of cartilage, these fish have a complete backbone They also have a relatively large brain They can solve problems and interact with other members of their species They are generally predators with keen senses
 Instead, they stay afloat by using a pair of muscular fins to push down against the water and create lift
Figure 4312 Cartilaginous Fish All of these fish belong to the class of cartilaginous fish with jaws (a) Oceanic whitetip shark (b) Ray (c) Ratfish
One of the most important traits of cartilaginous fish is their jaws Jaws allow them to bite food and break it into smaller pieces This is a big adaptive advantage because it greatly expands the range of food sources they can consume Jaws also make cartilaginous fish excellent predators It you’ve ever seen the film Jaws, then you know that jaws make sharks very fierce predators (see also Figure below )
Figure 4313 Jaws of a Shark Sharks have powerful jaws with multiple rows of sharp, saw-like teeth Most other fish are no match for these powerful predators
Ray-Finned Fish
Ray-finned fish include the majority of living fish species, including goldfish, tuna, salmon, perch, and cod They have a bony endoskeleton and a swim bladder Their thin fins consist of webs of skin over flexible bony rays, or spines The fins lack muscle, so their movements are controlled by muscles in the body wall
Figure 4314 Fins of Bony Fish The fins of ray-finned and lobe-finned fish are quite different How is the form of the fins related to their different functions in the two classes of fish? Ray Fin (left), Lobe Fin (right)
Lobe-Finned Fish
Lobe-finned fish are currently far fewer in number than ray-finned fish Their fins, like the one shown in Figure above , contain a stump-like appendage of bone and muscle There are two groups of lobe-finned fish still alive today: coelacanths and lungfish
Coelacanths are ancient fish with just two living species They are at risk of extinction because of their very small numbers Lungfish have a lung-like organ for breathing air The organ is an adaptation of the swim bladder It allows them to survive for long periods out of water