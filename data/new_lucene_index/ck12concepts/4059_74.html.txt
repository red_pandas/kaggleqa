At the equivalence point in a neutralization, the moles of acid are equal to the moles of base
Recall that the molarity of a solution is defined as the moles of the solute divided by the liters of solution  So the moles of solute are therefore equal to the molarity of a solution multiplied by the volume in liters
We can then set the moles of acid equal to the moles of base
is the molarity of the acid, while is the molarity of the base and are the volumes of the acid and base, respectively
Suppose that a titration is performed and 2070 mL of 0500 M NaOH is required to reach the end point when titrated against 1500 mL of HCl of unknown concentration The above equation can be used to solve for the molarity of the acid
The higher molarity of the acid compared to the base in this case means that a smaller volume of the acid is required to reach the equivalence point
The above equation works only for neutralizations in which there is a 1:1 ratio between the acid and the base The sample problem below demonstrates the technique to solve a titration problem for a titration of sulfuric acid with sodium hydroxide
Sample Problem: Titration
In a titration of sulfuric acid against sodium hydroxide, 3220 mL of 0250 M NaOH is required to neutralize 2660 mL of H 2 SO 4  Calculate the molarity of the sulfuric acid
Step 1: List the known values and plan the problem
Known
molarity NaOH = 0250 M volume NaOH = 3220 mL volume H 2 SO 4 = 2660 mL
Unkonwn
molarity H 2 SO 4 = ?
First determine the moles of NaOH in the reaction From the mole ratio, calculate the moles of H 2 SO 4 that reacted Finally, divide the moles H 2 SO 4 by its volume to get the molarity
Step 2: Solve
Step 3: Think about your result
The volume of H 2 SO 4 required is smaller than the volume of NaOH because of the two hydrogen ions contributed by each molecule
Summary
The process of calculating concentration from titration data is described and illustrated
Practice
Do the problems at the link below:
http://wwwsophiaorg/acidbase-titration-calculations-concept
Review
Questions
What assumption is made about the amounts of materials at the neutral point? What is different about the calculation using sulfuric acid? Why is the mole ratio important?
Titration Curves
Describe titration curves of acid-base neutralization reactions
Where did graphs come from?
The plot that we know of as a graph was the brainchild of the French mathematician-philosopher Rene Descartes (1596-1650) His studies in mathematics led him to develop what was known as “Cartesian geometry”, including the idea of our current graphs The coordinates are often referred to as Cartesian coordinates