An alkene is a hydrocarbon with one or more carbon-carbon double covalent bonds  The simplest alkene is composed of two carbon atoms and is called ethene (shown below) Each carbon is bonded to two hydrogen atoms in addition to the double bond between them
The hybridization of each carbon atom is sp 2 with trigonal planar geometry All the atoms of the molecule lay in one plane Like the alkane series, the names of alkenes are based on the number of atoms in the parent chain Naming follows the same rules as for alkanes, with the addition of using a number to indicate the location of the double bond
 The general formula for alkenes with one double bond is C n H 2n  Alkenes are called unsaturated hydrocarbons An unsaturated hydrocarbon is a hydrocarbon that contains less than the maximum number of hydrogen atoms that can possibly bond with the number of carbon atoms present
The location of the carbon-carbon double bond can vary The 4-carbon alkene generic name is butene Since the double bond can be located in more than one place, we have 1-butene and 2-butene:
Molecules with multiple double bonds are also quite common The formula below shows a four-carbon chain with double bonds between carbons 1 and 2 and between carbons 3 and 4 This molecule is called 1,3-butadiene
Alkynes
An alkyne is a hydrocarbon with one or more carbon-carbon triple covalent bonds  The simplest alkyne consists of two carbon atoms and is called ethyne (common name: acetylene)
The ethyne molecule is linear, with sp hybridization for each carbon atom The general formula of alkynes with one triple bond is C n H 2n-2  Alkynes are also unsaturated hydrocarbons Other alkynes exist, such as 2-pentyne:
Summary
Structures of alkene and alkyne are given Structures of typical alkenes and alkynes are shown
Practice
Answer the questions at the site below:
http://wwwdocbrowninfo/page06/PRalkenes/alkeneQmchtm
Review
Questions
What is an alkene? What is an alkyne? Can a compound have more than one carbon-carbon double bond in it?
alkene: A hydrocarbon with one or more carbon-carbon double covalent bonds alkyne: A hydrocarbon with one or more carbon-carbon triple covalent bonds unsaturated hydrocarbon: A hydrocarbon that contains less than the maximum number of hydrogen atoms that can possibly bond with the number of carbon atoms present
Isomers
Define isomer Define structural isomer and give examples Define geometric isomer and give examples
What difference does the isomer make?
As we get more into the complexities of organic chemistry, we will see how molecular shape affects reactions One common reaction for alkenes is the addition of hydrogen across the double bond to form the corresponding alkane Because of the geometry of the reaction, the different 2-butene shapes have different heats of reaction
 Greater energy requirements mean a higher cost and a more expensive product