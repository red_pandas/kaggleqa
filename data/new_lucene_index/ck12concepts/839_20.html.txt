We saw that the amount of solar radiation an area receives depends on its latitude The amount of solar radiation affects the temperature of a region Latitude has other effects on climate
Latitude and Prevailing Winds
Global air currents cause global winds The figure below shows the direction that these winds blow ( Figure below ) Global winds are the prevailing, or usual, winds at a given latitude The winds move air masses, which causes weather
The direction of prevailing winds determines which type of air mass usually moves over an area For example, a west wind might bring warm moist air from over an ocean An east wind might bring cold dry air from over a mountain range Which wind prevails has a big effect on the climate
 How would they affect the climate?
Figure 2038 The usual direction of the wind where you live depends on your latitude This determines where you are in the global wind belts
Latitude and Precipitation
Global air currents affect precipitation How they affect it varies with latitude ( Figure below ) Where air rises, it cools and there is precipitation Where air sinks, it warms and causes evaporation These patterns are part of the global wind belts
Figure 2039 Global air currents are shown on the left You can see how they affect climate on the right
"Five Factors that Affect Climate" takes a very thorough look at what creates the climate zones The climate of a region allows certain plants to grow, creating an ecological biome: http://wwwyoutubecom/watch?v=E7DLLxrrBV8 (5:22)
Click on the image above for more content