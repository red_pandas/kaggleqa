How fast a chemical reaction occurs is called the reaction rate  Several factors affect the rate of a given chemical reaction They include the:
temperature of reactants concentration of reactants surface area of reactants presence of a catalyst
At the following URL, you can see animations showing how these factors affect the rate of chemical reactions
http://wwwkentchemistrycom/links/Kinetics/FactorsAffectinghtm