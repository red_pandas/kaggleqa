To understand minerals, we must first understand matter Matter is the substance that physical objects are made of
Atoms and Elements
The basic unit of matter is an atom ( Figure below ) At the center of an atom is its nucleus  Protons are positively charged particles in the nucleus Also in the nucleus are neutrons with no electrical charge Orbiting the nucleus are tiny electrons Electrons are negatively charged An atom with the same number of protons and electrons is electrically neutral
 An ion will have positive charge if it has more protons than electrons It will have negative charge if it has more electrons than protons
Figure 151 The nucleus of an atom is made up of protons (blue) and neutrons (yellow) Electrons (red) orbit around the nucleus
An atom is the smallest unit of a chemical element  That is, an atom has all the properties of that element All atoms of the same element have the same number of protons
Molecules and Compounds
A molecule is the smallest unit of a chemical compound  A compound is a substance made of two or more elements The elements in a chemical compound are always present in a certain ratio
Water is probably one of the simplest compounds that you know A water molecule is made of two hydrogen atoms and one oxygen atom ( Figure below ) All water molecules have the same ratio: two hydrogen ions to one oxygen ion
Figure 152 A water molecule has two hydrogen ions (shown in gray) bonded to one oxygen ion (shown in red)