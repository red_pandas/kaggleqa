Scientists used to think that fungi were members of the plant kingdom They thought this because fungi had several similarities to plants For example:
Fungi and plants have similar structures Plants and fungi live in the same kinds of habitats, such as growing in soil Plants and fungi cells both have a cell wall, which animals do not have
How Fungi and Plants Differ
However, there are a number of characteristics that make fungi different from plants:
Fungi cannot make their own food like plants can, since they do not have chloroplasts and cannot carry out photosynthesis Fungi are more like animals because they are heterotrophs, as opposed to autotrophs, like plants, that make their own food Fungi have to obtain their food, nutrients and glucose, from outside sources
 Chitin is tough carbohydrate found in the shells of animals such as beetles and lobsters The cell wall of a plant is made of cellulose, not chitin Unlike many plants, most fungi do not have structures, such as xylem and phloem, that transfer water and nutrients
The Types of Fungi
The Kingdom Fungi can be broken down into several phyla Each phyla has some unique traits And even within the same phyla there are many differences among the fungi Various types of fungi are pictured below ( Table below ) Notice how different each of these organisms are from one another
Click on the image above to view the table