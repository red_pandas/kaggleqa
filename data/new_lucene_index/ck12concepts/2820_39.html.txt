Whenever you eat pizza, you eat fungi, even if you don’t like your pizza with mushrooms That’s because pizza dough contains yeast  Do you know other foods that are made with fungi?
Fungi for Food
Humans have collected and grown mushrooms for food for thousands of years Figure below shows some of the many types of mushrooms that people eat Yeasts are used in bread baking and brewing alcoholic beverages Other fungi are used in fermenting a wide variety of foods, including soy sauce, tempeh, and cheeses
Figure 3923 These are just a few of the many species of edible mushrooms consumed by humans
Figure 3924 Blue Cheese The dark blue strands running through this cheese are a fungus In fact, this cheese is moldy! The fungus is Penicillium roqueforti , a type of mold
Fungi for Pest Control
Harmless fungi can be used to control pathogenic bacteria and insect pests on crops Fungi compete with bacteria for nutrients and space, and they parasitize insects that eat plants Fungi reduce the need for pesticides and other toxic chemicals
Other Uses of Fungi
Fungi are useful for many other reasons
They are a major source of citric acid (vitamin C) They produce antibiotics such as penicillin , which has saved countless lives They can be genetically engineered to produce insulin and other human hormones They are model research organisms To see how one lab is using yeast in cancer research, watch the video, Yeast Unleashed, at this link: http://college
edu/news/stories/727/yeast-unleashed/ 