Reactions are favorable when they result in a decrease in enthalpy and an increase in entropy of the system When both of these conditions are met, the reaction occurs naturally A spontaneous reaction is a reaction that favors the formation of products at the conditions under which the reaction is occurring
 The products of a fire are composed partly of gases such as carbon dioxide and water vapor The entropy of the system increases during a combustion reaction The combination of energy decrease and entropy increase dictates that combustion reactions are spontaneous reactions
Figure 732 Bonfire
A nonspontaneous reaction is a reaction that does not favor the formation of products at the given set of conditions In order for a reaction to be nonspontaneous, it must be endothermic, accompanied by a decrease in entropy, or both Our atmosphere is composed primarily of a mixture of nitrogen and oxygen gases
Fortunately, this reaction is nonspontaneous at normal temperatures and pressures It is a highly endothermic reaction with a slightly positive entropy change  Nitrogen monoxide is capable of being produced at very high temperatures and has been observed to form as a result of lightning strikes
One must be careful not to confuse the term spontaneous with the notion that a reaction occurs rapidly A spontaneous reaction is one in which product formation is favored, even if the reaction is extremely slow A piece of paper will not suddenly burst into flames, although its combustion is a spontaneous reaction
 If the paper were to be heated to a high enough temperature, it would begin to burn, at which point the reaction would proceed spontaneously until completion
In a reversible reaction, one reaction direction may be favored over the other Carbonic acid is present in carbonated beverages It decomposes spontaneously to carbon dioxide and water according to the following reaction
If you were to start with pure carbonic acid in water and allow the system to come to equilibrium, more than 99% of the carbonic acid would be converted into carbon dioxide and water The forward reaction is spontaneous because the products of the forward reaction are favored at equilibrium
 When carbon dioxide is bubbled into water, less than 1% is converted to carbonic acid when the reaction reaches equilibrium The reverse reaction, as written above, is not spontaneous
Summary
Spontaneous and nonspontaneous reactions are defined Examples of both types of reactions are given
Practice
Questions
Read the material at the link below and answer the following questions:
http://chemistryaboutcom/od/workedchemistryproblems/a/Entropy-And-Reaction-Spontaneity-Example-Problemhtm
Why is system I a spontaneous reaction? Why is system II not spontaneous? Why is system III spontaneous?
Review
Questions
Why is a combustion reaction spontaneous? Is NO formation spontaneous at room temperature? How do we know that the equilibrium between carbonic acid and CO 2 goes strongly to the right?
nonspontaneous reaction: A reaction that does not favor the formation of products at the given set of conditions spontaneous reaction: A reaction that favors the formation of products at the conditions under which the reaction is occurring
Free Energy
Define free energy Describe how changes in and affect 
All aboard!
The steam engine pictured above is slowly going out of style, but is still a picturesque part of the modern railroad The water in a boiler is heated by a fire (usually fueled by coal) and turned to steam This steam then pushes the pistons that drive the wheels of the train