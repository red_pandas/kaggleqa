To connect two points accurately you need a direction This occurs when you need to go from point A to point B It also happens if you want to describe where something is going If you are walking across a field, for example, you might say you're going north 20° east
Compass
The most common way to find direction is by using a compass  A compass is a device with a floating needle ( Figure below ) The needle is a small magnet that aligns itself with the Earth’s magnetic field The compass needle always points to magnetic north If you have a compass and you find north, you can then know any other direction
, on a compass rose 
Figure 1333 (a) A compass is a device that is used to determine direction The needle points to Earth’s magnetic north pole (b) A compass rose shows the four major directions, plus intermediates between them
A compass needle lines up with Earth’s magnetic north pole This is different from Earth’s geographic north pole, also called true north The geographic north pole is the top of an imaginary line This line is the axis around which Earth rotates The geographic north pole is much like the spindle of a spinning top
 However, the magnetic north pole shifts in location over time Depending on where you live, you can correct for the difference between the two poles when you use a map and a compass ( Figure below )
Figure 1334 Earth’s magnetic north pole is about 11 degrees offset from its geographic north pole
Some maps have a double compass rose This allows users to make the corrections between magnetic north and true north An example is a nautical chart that boaters use to chart their positions at sea ( Figure below )
Figure 1335 Nautical maps include a double compass rose that shows both magnetic directions (inner circle) and geographic compass directions (outer circle)
Polar Coordinate System
You can also use a polar coordinate system Your location is marked by an angle and distance from some reference point The angle is usually the angle between your location, the reference point, and a line pointing north The distance is given in meters or kilometers To find your location or to move from place to place, you need a map, a compass, and some way to measure your distance, such as a range finder
Suppose you need to go from your location to a marker that is 20°E and 500 m from your current position You must do the following:
Use the compass and compass rose on the map to orient your map with north Use the compass to find which direction is 20°E Walk 500 meters in that direction to reach your destination Polar coordinates are used in a sport called orienteering People who do orienteering use a compass and a map with polar coordinates
 They move to various checkpoints along the course The winner is the person who completes the course in the fastest time
Figure 1336 A topographic map like one that you might use for the sport of orienteering