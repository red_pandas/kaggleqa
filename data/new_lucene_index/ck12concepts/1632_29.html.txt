The fuel that burns in a combustion reaction contains compounds called hydrocarbons Hydrocarbons are compounds that contain only carbon (C) and hydrogen (H) The charcoal pictured in the Figure above consists of hydrocarbons So do fossil fuels such as natural gas Natural gas is a fuel that is commonly used in home furnaces and gas stoves
 You can see a methane flame in the Figure below  The combustion of methane is represented by the equation:
CH 4 + 2O 2 → CO 2 + 2H 2 O
Figure 2933 The combustion of methane gas heats a pot on a stove
Q: Sometimes the flame on a gas stove isn’t just blue but has some yellow or orange in it Why might this occur?
A: If the flame isn’t just blue, the methane isn’t getting enough oxygen to burn completely, leaving some of the carbon unburned The flame will also not be as hot as a completely blue flame for the same reason
You can simulate the combustion of hydrocarbons, including methane, at this URL: http://groupchemiastateedu/Greenbowe/sections/projectfolder/flashfiles/stoichiometry/stoic_excess_oxyhtml 