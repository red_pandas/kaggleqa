While it must be assumed that many more scientists, philosophers and others studied the composition of matter after Democritus, a major leap forward in our understanding of the composition of matter took place in the 1800s with the work of the British scientist John Dalton He started teaching school at age twelve, and was primarily known as a teacher
  His work in several areas of science brought him a number of honors  When he died, over 40,000 people in Manchester marched at his funeral
Dalton studied the weights of various elements and compounds He noticed that matter always combined in fixed ratios based on weight, or volume in the case of gases Chemical compounds always contain the same proportion of elements by mass, regardless of amount, which provided further support for Proust’s law of definite proportions
Figure 578 Dalton
Dalton’s Atomic Theory (1804)
From his experiments and observations, as well as the work from peers of his time, Dalton proposed a new theory of the atom  This later became known as Dalton’s atomic theory  The general tenets of this theory were as follows:
All matter is composed of extremely small particles called atoms Atoms of a given element are identical in size, mass, and other properties Atoms of different elements differ in size, mass, and other properties Atoms cannot be subdivided, created, or destroyed Atoms of different elements can combine in simple whole number ratios to form chemical compounds
Dalton’s atomic theory has been largely accepted by the scientific community, with the exception of three changes We know now that (1) an atom can be further sub-divided, (2) all atoms of an element are not identical in mass, and (3) using nuclear fission and fusion techniques, we can create or destroy atoms by changing them into other atoms
Figure 579 Dalton's symbols
Summary
Dalton proposed his atomic theory in 1804 The general tenets of this theory were as follows: All matter is composed of extremely small particles called atoms Atoms of a given element are identical in size, mass, and other properties Atoms of different elements differ in size, mass, and other properties
 Atoms of different elements can combine in simple whole number ratios to form chemical compounds In chemical reactions, atoms are combined, separated, or rearranged
Practice
Use the link below to do the exercise  Read the sections and take the quiz at the end
http://antoinefrostburgedu/chem/senese/101/atoms/daltonshtml
Review
Questions
How did the Greek and Roman philosophers study nature? When did John Dalton start teaching school? Did Dalton believe that atoms could be created or destroyed? List the basic components of Dalton’s atomic theory What parts of the theory are not considered valid any more?
atom: The smallest unit of an element atomic theory: The general tenets of Dalton's atomic theory were as follows: All matter is composed of extremely small particles called atoms Atoms of a given element are identical in size, mass, and other properties Atoms of different elements differ in size, mass, and other properties
 Atoms of different elements can combine in simple whole number ratios to form chemical compounds In chemical reactions, atoms are combined, separated, or rearranged
Cathode Ray Tube
Describe how the Crooke’s tube functioned Describe experiments that showed that cathode rays had mass
How old do you think this TV is?
The TV set seen above is becoming harder and harder to find these days The main reason is because they are older and based on outdated technology  The new TV sets are flat screen technology that take up less space and give better picture quality, especially with the advent of high-definition broadcasting
  A beam of electrons was sprayed to a picture tube which was treated to react with the electrons to produce an image  Similar CRT devices were used in computer monitors, now also replaced by flat screen monitors