Electric Potential
In the study of mechanics, the concept of energy, and the conservation thereof, was extremely useful The same will be true for the study of electrical interactions The work done moving a charged particle in an electric field can result in the particle gaining or losing both kinetic and potential energy
Lifting an object in a gravitational field requires work and increases the object's potential energy A similar situation occurs when you move two charged objects relative to each other We already know that each object has an electric field surrounding it, which effects the other charge If the two charged objects have the same charge, they repel each other
 Conversely, moving two like charges apart will decrease the potential energy If the objects attract each other, the opposite situtations occur: if you pull them apart, you do work against the force, which increases the potential energy of the system, but bringing attractive charges closer together decreases the potential energy
It is often easy to think of the change in energy as a mountain or an inverted cone, depending on the situation Imagine a positive point charge, with the corresponding electric field around it If you are moving another positive point charge, the situation is like a mountain, with the original point charge at the peak
 This requires work, and increases the potential energy of the system If, however, the second charge is a negative point charge, the two charges attract each other and the situation is like a cone: the second charge easily falls towards the first, decreasing the potential energy It would then require work to get the second charge up and out of the cone, away from the first charge
Uniform Electric Fields
As we know from Coulomb's law, the electric field around a point charge decreases as the distance from the point increases However, it is possible to create a constant electric field between two large, flat conducting plates parallel to each other If one of the plates is positively charged and the other negatively charged, the space between the plates will have a constant electric field except near the edges of the plates
A uniform electric field makes it easier to measure the difference in electric potential energy This energy, also called the electric potential difference is commonly referred to as the voltage , based on the unit, volt (V) To measure the voltage across some distance, it is necessary to pick a position to be the relative zero, because voltage is the change in potential difference
 The voltage is measured using a voltmeter, which measures the electric potential difference across two points
The electrical potential difference between the two plates is expressed as , the electric field strength times the distance between the plates The units in this expression are Newtons/coulomb times meters, which gives the final units Joules/coulomb Voltage is an expression of the amount of potential energy per unit charge The work done moving a charge against the field can be calculated by multiplying the electric field potential by the charge, 
Example Problem: Two large parallel metal plates are 50 cm apart The magnitude of the electric field between them is 800 N/C
(a) What is the potential difference between the plates?
(b) What work is done when one electron is moved from the positive to the negative plate?
Solution: (a)
(b)
Example Problem: A voltmeter measures the potential difference between two large parallel plates to be 500 volts The plates are 30 cm apart What is the magnitude of the electric field strength between the plates?
Solution:
Summary
The work done moving a charged particle in an electric field can result in the particle gaining or losing both kinetic and potential energy The difference in electric potential energy is measured with a voltmeter in units called volts A constant electric field can be produced by placing two large flat conducting plates parallel to each other
 The work done moving a charge against the field can be found by 
Practice
Questions
The following video covers potential difference and electric potential Use this resource to answer the questions that follow
http://wwwyoutubecom/watch?v=wT9AsY79f1k
Click on the image above for more content
If you do work to move a charged object in an electric field, where does the work go? Points A and B in an electric field have a difference in potential energy  This difference in electrical potential energy is called ___________________
Practice problems about electric potential energy with answers: http://higheredmcgraw-hillcom/sites/0072828625/student_view0/chapter12/practice_problemshtml
Review
Questions
Two large parallel plates are 000630 m apart and the voltage across them is 100 volts What is the electric field strength between the plates? The potential difference between points A and B in an electric field is 250 volts How much work is required to transfer 100 coulombs of charge from A to B? 10
00 coulombs of charge from point X to point Y in an electric field  What is the difference in potential between these two points? The electric field between two parallel plates connected to a 45 V battery (which produces a 45 V difference in potential between the plates) is 1500 N/C
electric potential difference : The difference in electric potential energy between two points in an electric field; the work that has to be done in transferring a unit positive charge from one point to the other, measured in volts voltmeter: An instrument for measuring potential differences in volts voltage: Electric potential difference, usually expressed in volts
Millikan Oil Drop Experiment
Explain how Millikan used electric fields to find the charge of the electron
This is the original equipment used by Robert Millikan and Harvey Fletcher in 1909 at the Univeristy of Chicago to measure the electric charge on a single electron With incredible perserverence, they were able to determine the charge to within 1% of the currently accepted value