One of the interesting aspects of organic chemistry is that it is three-dimensional A molecule can have a shape in space that may contribute to its properties Molecules can differ in the way the atoms are arranged – the same combination of atoms can be assembled in more than one way
 Isomers are molecules with the same molecular formulas, but different arrangements of atoms We will look at some isomer possibilities for alkanes and alkenes
Structural Isomers
A structural isomer is one in which two or more or organic compounds have the same molecular formulas but different structures The two pentane molecules below differ only in the location of the methyl group
Alkenes can also demonstrate structural isomerism In alkenes, there are multiple structural isomers based on where in the chain the double bond occurs The condensed structural formulas of 1-butene and 2-butene show this
The number in the name of the alkene refers to the lowest numbered carbon in the chain that is part of the double bond
Geometric Isomers
With a molecule such as 2-butene, a different type of isomerism called geometric isomerism can be observed Geometric isomers are isomers in which the order of atom bonding is the same but the arrangement of atoms in space is different The double bond in an alkene is not free to rotate because of the nature of the pi bond
 The image below shows the two geometric isomers, called cis -2-butene and trans -2-butene
The cis isomer has the two single hydrogen atoms on the same side of the molecule, while the trans isomer has them on opposite sides of the molecule In both molecules, the bonding order of the atoms is the same In order for geometric isomers to exist, there must be a rigid structure in the molecule to prevent free rotation around a bond
 In addition, the two carbon atoms must each have two different groups attached in order for there to be geometric isomers Propene has no geometric isomers because one of the carbon atoms has two single hydrogens bonded to it
Physical and chemical properties of geometric isomers are generally different While cis -2-butene is a polar molecule, trans -2-butene is nonpolar Heat or irradiation with light can be used to bring about the conversion of one geometric isomer to another The input of energy must be large enough to break the pi bond between the two carbon atoms, which is weaker than the sigma bond
As with alkenes, alkynes display structural isomerism beginning with 1-butyne and 2-butyne However, there are no geometric isomers with alkynes because there is only one other group bonded to the carbon atoms that are involved in the triple bond
Summary
Structural and geometric isomers are defined Examples of alkane and alkene isomers are given
Practice
Questions
Watch the video at the link below and answer the following questions:
Click on the image above for more content
http://wwwyoutubecom/watch?v=CGPyTUrSF2E
What does the molecular formula tell us? What does the structural formula tell us? What do you add to the name if two double bonds are present in the molecule?
Review
Questions
What is a structural isomer? What is a geometric isomer? Could 1-butene have geometric isomers?
geometric isomers: Isomers in which the order of atom bonding is the same but the arrangement of atoms in space is different isomers: Molecules with the same molecular formulas, but different arrangements of atoms structural isomer: One in which two or more or organic compounds have the same molecular formulas but different structures
Cyclic Hydrocarbons
Define cyclic hydrocarbon Define cycloalkane Draw structures and name typical cyclic hydrocarbons
From Benzene To Balloons
Although cyclohexane can be isolated from petroleum products, a major source of this chemical is the hydrogenation of benzene Much of the cyclohexane produced is used to manufacture intermediates for the production of nylon The nylon balloons pictured above no doubt had their start in a chemical plant where hydrogen gas and benzene were reacted at high temperatures to form cyclohexane