Guidance Capacitors in parallel have the same voltage, but different charge stored Capacitors in series have the same charge stored, but different voltages Remember that if a capacitor are hooked up to the battery they will have the same voltage as the battery If the capacitor is unhooked from a battery and other capacitors are attached to it, then the voltage can change but the total amount of charge must remain constant
 When solving problems involving capacitor circuits, we use the equation for the charge on a capacitor much like we use Ohm's Law
Example 1
Two capacitors, one of ( ) and one of ( ), are connected to a 10V battery in series A diagram of the circuit is shown below Determine (a) the total capacitance, (b) the charge stored on the capacitor, and (c) the voltage drop across the 
Solution
(a): To find the total capacitance, we'll use the equation give above for determining the equivalent capacitance of capacitors in series
(b): Since charge is the same across capacitors in series, we can use the charge found using the total capacitance and the total voltage drop to find the charge in the capacitor
(c): Since we know the charge and the capacitance of , we can find the voltage drop
Example 2
The two capacitors used in the previous example problem are now connected to the battery in parallel What is (a) the total capacitance and (b) the charge on  A diagram of the circuit is shown below
Solution
(a): To find the total capacitance, we'll us the equation given above for capacitors in parallel
(b): Now, since the voltage across capacitors in parallel is equal, we can find the charge on 