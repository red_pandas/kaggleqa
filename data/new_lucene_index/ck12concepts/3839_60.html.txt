Not all ionic compounds are composed of only monatomic ions A ternary ionic compound is an ionic compound composed of three or more elements In a typical ternary ionic compound, there is still one type of cation and one type of anion involved The cation, the anion, or both, is a polyatomic ion
Naming Ternary Ionic Compounds
The process of naming ternary ionic compounds is the same as naming binary ionic compounds The cation is named first, followed by the anion Some examples are shown in the Table below :
Examples of Ternary Ionic Compounds
When more than one polyatomic ion is present in a compound, the formula of the ion is placed in parentheses with a subscript outside of the parentheses that indicates how many of those ions are in the compound In the last example above, there is one Fe 3+ cation and three OH - anions
Writing Formulas for Ternary Ionic Compounds
Writing a formula for a ternary ionic compound also involves the same steps as for a binary ionic compound Write the symbol and charge of the cation followed by the symbol and charge of the anion Use the crisscross method to ensure that the final formula is neutral Calcium nitrate is composed of a calcium cation and a nitrate anion
The charge is balanced by the presence of two nitrate ions and one calcium ion Parentheses are used around the nitrate ion because more than one of the polyatomic ion is needed If only one polyatomic ion is in a formula, parentheses are not used As an example, the formula for calcium carbonate is CaCO 3 
There are two polyatomic ions that produce unusual formulas The Hg 2 2+ ion is called either the dimercury ion or, preferably, the mercury(I) ion When bonded with an anion with a 1− charge, such as chloride, the formula is Hg 2 Cl 2  Because the cation consists of two Hg atoms bonded together, this formula is not reduced to HgCl
 For example, the formula for potassium peroxide is K 2 O 2 
Summary
Ternary compounds are composed of three or more elements Ternary compounds are named by stating the cation first, followed by the anion Positive and negative charges must balance
Practice Questions
Use the link below to answer the following questions:
Click on the image above for more content
What are ionic compounds containing polyatomic ions called? Does the “criss-cross” method work for naming ternary compounds?
Review Questions
What is a ternary compound? What is the basic rule for naming ternary compounds? Write the formulas for the following compounds: mercury(II) nitrate ammonium phosphate calcium silicate lead(II) chromate Name the following compounds: KClO 3 Rb 2 SO 4 Cd(NO 3 ) 2 NaCN
ternary: A compound made up of three parts ternary ionic compound: An ionic compound composed of three or more elements
Naming Binary Molecular Compounds
Define molecular compound Explain how molecular compounds are different from ionic compounds Be able to name the compound when given the formula
Why do so many relatives in royalty share the same name?
Some families name a son (usually the firstborn) after his father  So it is somewhat common to find John Smith, Jr named after John Smith the father  A few families may take it further and name the grandson John Smith III  Countries with long histories of royalty take the naming even further
  The use of numbering for names adds clarity to a system –we always know which Henry we are talking about
Inorganic chemical compounds can be broadly classified into two groups: ionic compounds and molecular compounds  The structure of all ionic compounds is an extended three-dimensional array of alternating positive and negative ions  Since ionic compounds do not take the form of individual molecules, they are represented by empirical formulas  Now we will begin to examine the formulas and nomenclature of molecular compounds