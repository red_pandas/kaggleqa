There are two basic types of strategies for reducing pollution from fossil fuels:
Use less fossil fuel Prevent pollutants from entering the air
Use Less Fossil Fuel
We can reduce our use of fossil fuels in several ways:
Conserve fossil fuels For example, turning out lights when we aren’t using them saves electricity Why does this help? A lot of the electricity we use comes from coal-burning power plants Use fossil fuels more efficiently For example, driving a fuel-efficient car lets you go farther on each gallon of gas
 Keep up with technology Hybrid cars run on electricity that would be wasted during braking These cars use gas only as a backup fuel As a result, they produce just 10 percent of the air pollution produced by regular cars
Keeping Pollutants out of the Air
Some of the pollutants from fossil fuels can be filtered out of exhaust before it is released into the air ( Figure below ) Other pollutants can be changed to harmless compounds before they are released Two widely used technologies are scrubbers and catalytic converters
Scrubbers are used in factories and power plants They remove particulates and waste gases from exhaust before it is released to the air Catalytic converters are used on motor vehicles They break down pollutants in exhaust to non-toxic compounds For example, they change nitrogen oxides to harmless nitrogen and oxygen gases
Figure 1942 Before catalytic converters were required, cars spewed a lot of pollutants into the air On the left is a car without a catalytic converter The car on the right has one
Increasing Alternative Energy Sources
Developing alternative energy sources is important What are some of the problems facing wider adoption of alternative energy sources?
The technologies are still being developed This includes sources of alternative energy, like solar and wind Solar and wind are still expensive relative to fossil fuels The technology needs to advance so that the price falls Some areas get low amounts of sunlight and are not suited for solar Others do not have much wind
 The desert Southwest will need to develop solar The Great Plains can use wind energy Perhaps some locations will rely on nuclear power
Ways You Can Reduce Air Pollution
Everyone can help to reduce air pollution Just use less fossil fuels! How can you do this?
Ride a bike or walk instead of driving Take a bus or carpool Buy a fuel efficient car Turn off lights and appliances when they are not in use Use energy efficient light bulbs and appliances Buy fewer things that are manufactured using fossil fuels
All these actions reduce the amount of energy that power plants need to produce
National Geographic videos exploring energy conservation are found in Environment Videos, Energy: http://videonationalgeographiccom/video/environment/energy-environment
Alternative Energy Fuel Cells Solar Power
What you can do to your home to help reduce energy use: http://wwwyoutubecom/watch?v=6h8QjZvcp0I (3:17)
Click on the image above for more content
A very simple thing you can do to conserve energy is discussed in “This Bulb”: http://wwwyoutubecom/watch?v=FvOBHMb6Cqc (1:45)
Click on the image above for more content