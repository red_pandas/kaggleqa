Long-term or high-dose exposure to radiation can harm both living and nonliving things Radiation knocks electrons out of atoms and changes them to ions It also breaks bonds in DNA and other compounds in living things One source of radiation that is especially dangerous to people is radon Radon is a radioactive gas that forms in rocks underground
 Then it may build up and become harmful to people who breathe it Long-term exposure to radon can cause lung cancer
Exposure to higher levels of radiation can be very dangerous, even if the exposure is short-term A single large dose of radiation can burn the skin and cause radiation sickness Symptoms of this illness include extreme fatigue, destruction of blood cells, and loss of hair To learn more about the harmful health effects of radiation, go to this URL: http://www
com/video/watch/?id=7359819n 
Click on the image above for more content
Nonliving things can also be damaged by radiation For example, high levels of radiation can weaken metals by removing electrons This is a problem in nuclear power plants and space vehicles because they are exposed to very high levels of radiation
Q: Can you tell when you are being exposed to radiation? For example, can you sense radon in the air?
A: Radiation can’t be detected with the senses This adds to its danger However, there are other ways to detect it