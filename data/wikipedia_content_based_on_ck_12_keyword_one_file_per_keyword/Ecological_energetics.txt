Ecological energetics is the quantitative study of the flow of energy through ecological systems. It aims to uncover the principles which describe the propensity of such energy flows through the trophic, or 'energy availing' levels of ecological networks. In systems ecology the principles of ecosystem energy flows or "ecosystem laws" (i.e. principles of ecological energetics) are considered formally analogous to the principles of energetics.


== History ==
Ecological energetics appears to have grown out of the Age of Enlightenment and the concerns of the Physiocrats. It began in the works of Sergei Podolinksy in the late 1800s, and subsequently was developed by the Soviet ecologist Vladmir Stanchinsky, the Austro-American Alfred J. Lotka, and American limnologists, Raymond Lindeman and G. Evelyn Hutchinson. It underwent substantial development by Howard T. Odum and was applied by systems ecologists, and radiation ecologists.


== References ==
S. Podolinsky (2004) "Socialism and the Unity of Physical Forces," Organization & Environment 17(1): 61-75.
D.R. Weiner (2000) Models of Nature: Ecology, Conservation and Cultural Revolution in Soviet Russia. University of Pittsburgh Press, U.S.A.