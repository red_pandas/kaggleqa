Murray Raney (October 14, 1885  March 3, 1966) was an American mechanical engineer born in Carrollton, Kentucky. He was the developer of a nickel catalyst that became known as Raney nickel, which is often used in industrial processes and scientific research for the hydrogenation of multiple covalent bonds present in molecules.


== Biography ==
Raney was born in Carrollton, Kentucky, to William Wallace and Katherine Raney. Without having attended high school, he obtained his Bachelor's degree in Mechanical Engineering from the University of Kentucky in 1909. Following his graduation he became a teacher at the Eastern Kentucky State Normal College and was also in charge of the heating and lighting facilities there until 1910.
From 1910 until 1911 he worked in the beater room of the Fort Orange Paper Company in Castleton-on-Hudson, New York. In 1911 he moved to Springfield, Illinois to work in the production of steam engines at A. L. Ide Engine Company, where he stayed until 1913. That same year he moved to his final residence in Chattanooga, Tennessee, to work for the Chattanooga Railway, Light & Power Co as a power salesman.
Raney joined the Lookout Oil & Refining Company in 1915. He was assigned to work as an assistant manager in the production of hydrogen used in the hydrogenation of vegetable oils. It was during this time he started to work in the preparation of what later became known as "Raney" catalysts. He left Lookout Oil in 1925 to take a sales manager position at Gilman Paint and Varnish Co., eventually becoming president of the company. In 1950 he left Gilman Paint and founded the Raney Catalyst Company. He then started to dedicate full-time to the production of his catalysts. This company was bought by W.R. Grace and Company in 1963 and still produces Raney nickel to this day.
Raney was twice married, first on June 12, 1920, to Katherine Elizabeth Macrae (d. June 13, 1935) with whom he had one daughter. His second marriage was to Laura Ogden McClellan (d. April 13, 1953) on March 31, 1939. He was awarded an honorary Doctor of Science degree in 1951. He was member of the American Chemical Society and the American Oil Chemists' Society. He was granted a total of six American and five European patents for the development of his catalysts and metallurgical processes needed for their preparation.


== Development of Raney nickel ==
During his time at Lookout Oil and Refining Raney was responsible for the production of hydrogen which was used in the hydrogenation of vegetable oils. During that time the industry used a nickel catalyst prepared from nickel(II) oxide. Believing that better catalysts could be produced, around 1921 he started to perform independent research on this matter while still working for Lookout Oil. In 1924 he produced a nickel-silicon alloy which, after being treated with sodium hydroxide, was found to be five times more active than the best catalyst used in the hydrogenation of cottonseed oil. He was granted a US patent for this discovery in 1925.
In 1926 Raney produced a nickel-aluminium alloy, also in a 1:1 ratio, following a procedure similar to the one used for the nickel-silicon catalyst, and he found that the resulting catalyst was even more active than the previous one. This catalyst, now commonly known as Raney nickel, was the subject of a patent he obtained in 1927.
It may be of interest to note that Raney's choice of the nickel-aluminium ratio was fortuitous and without any real scientific basis. However, this is the preferred alloy composition for production of Raney nickel catalysts currently in use. To this, Raney himself said in an interview "I was just lucky... I had an idea for a catalyst and it worked the first time."


== The Murray Raney Award ==
In 1992 the Organic Reactions Catalysis Society (ORCS) created the Murray Raney Award "for contributions in the use of sponge metal catalysts in organic synthesis". The award is given out every two years at the annual ORCS conference. As of 2006 the following scientists have been recipients of the award:
1992  Stewart Montgomery
1994  Pierre Fouilloux
1996  Mark Wainwright
1998  Anatoly Fasman
2000  Jozsef Petr
2002  Akira Tai
2004  Jean Lessard
2006  Isamu Yamauchi


== Notes ==


== References ==
University of Kentucky Alumni Association (2005). UK Alumni Association - Hall of Distinguished Alumni. Retrieved December 25, 2005.
Who's Who in the South and Southwest: a Biographical Dictionary of Noteworthy Men and Women of the Southern and Southwestern States. 6th ed. Chicago:Marquis - Who's Who. 1959.
W.R. Grace & Co. (2004) History Murray Raney. Retrieved December 25, 2005.
W.R. Grace & Co. (2004) ORCS. Retrieved December 25, 2005.