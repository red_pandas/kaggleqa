The common ion effect is responsible for the reduction in the solubility of an ionic precipitate when a soluble compound combining one of the ions of the precipitate is added to the solution in equilibrium with the precipitate. It states that if the concentration of any one of the ions is increased, then, according to Le Chatelier's principle, the ions in excess should combine with the oppositely charged ions. Some of the salt will be precipitated until the ionic product is equal to the solubility of the product. In simple words, common ion effect is defined as the suppression of the degree of dissociation of a weak electrolyte containing a common ion.


== Solubility effectsEdit ==

The solubility of a sparingly soluble salt is reduced in a solution that contains an ion in common with that salt. For instance, the solubility of silver chloride in water is reduced if a solution of sodium chloride is added to a suspension of silver chloride in water.
A practical example used very widely in areas drawing drinking water from chalk or limestone aquifers is the addition of sodium carbonate to the raw water to reduce the hardness of the water. In the water treatment process, highly soluble sodium carbonate salt is added to precipitate out sparingly soluble calcium carbonate. The very pure and finely divided precipitate of calcium carbonate that is generated is a valuable by-product used in the manufacture of toothpaste.
The salting out process used in the manufacture of soaps benefits from the common ion effect. Soaps are sodium salts of fatty acids. Addition of sodium chloride reduces the solubility of the soap salts. The soaps precipitate due to a combination of common ion effect and increased ionic strength.
Sea, brackish and other waters that contain appreciable amount of Na+ interfere with the normal behavior of soap because of common ion effect. In the presence of excess Sodium ions the solubility of soap salts is reduced, making the soap less effective.


== Buffering effectEdit ==

A buffer solution contains an acid and its conjugate base or a base and its conjugate acid. Addition of the conjugate ion will result in a change of pH of the buffer solution. For example, if both sodium acetate and acetic acid are dissolved in the same solution they both dissociate and ionize to produce acetate ions. Sodium acetate is a strong electrolyte so it dissociates completely in solution. Acetic acid is a weak acid so it only ionizes slightly. According to Le Chatelier's principle, the addition of acetate ions from sodium acetate will suppress the ionization of acetic acid and shift its equilibrium to the left. Thus the percent dissociation of the acetic acid will decrease and the pH of the solution will increase. The ionization of an acid or a base is limited by the presence of its conjugate base or acid.
NaCH3CO2(s)  Na+(aq) + CH3CO2(aq)
CH3CO2H (aq)  H+(aq) + CH3CO2(aq)
This will decrease the hydrogen ion concentration and thus the common-ion solution will be less acidic than a solution containing only acetic acid.


== ExceptionsEdit ==
Many transition metal compounds violate this rule due to the formation of complex ions. For example, copper(I) chloride is insoluble in water, but it dissolves when chloride ions are added, such as when hydrochloric acid is added. This is due to the formation of soluble CuCl2 complex ions.


== ReferencesEdit ==
Mendham, J.; Denney, R. C.; Barnes, J. D.; Thomas, M. J. K. (2000), Vogel's Quantitative Chemical Analysis (6th ed.), New York: Prentice Hall, ISBN 0-582-22628-7 


== See alsoEdit ==
Chelate effect