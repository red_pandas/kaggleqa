The smuts are multicellular fungi characterized by their large numbers of teliospores. The smuts get their name from a Germanic word for dirt because of their dark, thick-walled, and dust-like teliospores. They are mostly Ustilaginomycetes (of the class Teliomycetae, subphylum Basidiomycota) and can cause plant disease. The smuts are grouped with the other basidiomycetes because of their commonalities concerning sexual reproduction.
Smuts are cereal and crop pathogens that most notably affect members of the grass family (Graminaceae). Economically important hosts include maize, barley, wheat, oats, sugarcane, and forage grasses. They eventually hijack the plants' reproductive systems, forming galls which darken and burst, releasing fungal teliospores which infect other plants nearby. Before infection can occur, the smuts need to undergo a successful mating to form dikaryotic hyphae (two haploid cells fuse to form a dikaryon).


== Sugarcane smutEdit ==
Sugarcane smut or Ustilago scitaminea Sydow is caused by the fungus Sporisorium scitamineum; smut was previously known as Ustilago scitaminea. The smut 'whip' is a curved black structure which emerges from the leaf whorl, and which aids in the spreading of the disease. Sugarcane smut causes significant losses to the economic value of a sugarcane crop. Sugarcane smut has recently been found in the eastern seaboard areas of Australia, one of the world's highest-yielding sugar areas.
For the sugarcane crop to be infected by the disease, large spore concentrations are needed. The fungi uses its smut-whip to ensure that the disease is spread to other plants, which usually occurs over a time period of three months. As the inoculum is spread, the younger sugarcane buds just coming out of the soil will be the most susceptible. Because water is necessary for spore germination, irrigation has been shown to be a factor in spreading the disease. Therefore, special precautions need to be taken during irrigation to prevent spreading of the smut.
Another way to prevent the disease from occurring in the sugarcane is to use fungicide. This can be done by either pre-plant soaking or post-plant spraying with the specific fungicide. Pre-plant soaking has been proven to give the best results in preventing the disease, but post-plant spraying is a practical option for large sugarcane cultivations.


== Corn smutEdit ==
Corn smut (Ustilago maydis) infects maize and is a delicacy in Mexico, where it was historically enjoyed by the Aztecs. It grows in the ears of the corn crops and converts the kernels into black, powdery fungal tissues. The smut is sold in the markets in Mexico while other parts of the world (including the United States) continue to reject it as an ingredient for food dishes. Corn smut is currently called huitlacoche by some Mexicans, and the Aztecs formerly called it cuitlacoche. Investigators have recently found that the amount of protein in corn smut is greater than that of the corn, and also greater than that of oats and clover hay.
Huitlacoche is used for some of the several recipes including soups, stews, steak sauces and crepes.


== See alsoEdit ==

Covered smut Tilletia tritici (syn. T. caries) and T. laevis (syn. T. foetida)
Ergot, which includes the species Claviceps purpurea
Loose smut, Ustilago nuda
TCK smut Tilletia controversa


== ReferencesEdit ==


== External linksEdit ==
Smut Fungi from Deacon, J: "Fungal Biology", Blackwell Publishing, 2005