In simplest terms, radiobiology (also known as radiation biology) is a field of clinical and basic medical sciences that involves the study of the action of ionizing radiation on living things.


== Health effects ==
Ionizing radiation is generally harmful and potentially lethal to living things but can have health benefits in radiation therapy for the treatment of cancer and thyrotoxicosis. Its most common impact is the induction of cancer with a latent period of years or decades after exposure. High doses can cause visually dramatic radiation burns, and/or rapid fatality through acute radiation syndrome. Controlled doses are used for medical imaging and radiotherapy. Some scientists suspect that low doses may have a mild hormetic effect that can improve health.
Some effects of ionizing radiation on human health are stochastic, meaning that their probability of occurrence increases with dose, while the severity is independent of dose. Radiation-induced cancer, teratogenesis, cognitive decline, and heart disease are all examples of stochastic effects. Other conditions such as radiation burns, acute radiation syndrome, chronic radiation syndrome, and radiation-induced thyroiditis are deterministic, meaning they reliably occur above a threshold dose, and their severity increases with dose. Deterministic effects are not necessarily more or less serious than stochastic effects; either can ultimately lead to a temporary nuisance or a fatality.
Other effects include radiation-induced lung injury, cataracts, and infertility.
Quantitative data on the effects of ionizing radiation on human health is relatively limited compared to other medical conditions because of the low number of cases to date, and because of the stochastic nature of some of the effects. Stochastic effects can only be measured through large epidemiological studies where enough data has been collected to remove confounding factors such as smoking habits and other lifestyle factors. The richest source of high-quality data comes from the study of Japanese atomic bomb survivors. In vitro and animal experiments are informative, but radioresistance varies greatly across species.
The consensus of the nuclear industry, regulators and governments regarding radiation health effects is expressed by the International Commission on Radiological Protection. (ICRP) Other important organizations studying the topic include
International Commission on Radiation Units and Measurements (ICRU)
United Nations Scientific Committee on the Effects of Atomic Radiation (UNSCEAR)
US National Council on Radiation Protection and Measurements (NCRP)
UK Public Health England
US National Academy of Sciences (NAS through the BEIR studies)
French Institut de radioprotection et de sret nuclaire (IRSN)
European Committee on Radiation Risk (ECRR)


== Exposure Pathways ==


=== External ===

External exposure is exposure which occurs when the radioactive source (or other radiation source) is outside (and remains outside) the organism which is exposed. Examples of external exposure include:
A person who places a sealed radioactive source in his pocket
A space traveller who is irradiated by cosmic rays
A person who is treated for cancer by either teletherapy or brachytherapy. While in brachytherapy the source is inside the person it is still considered external exposure because it does not result in a committed dose.
A nuclear worker whose hands have been dirtied with radioactive dust. Assuming that his hands are cleaned before any radioactive material can be absorbed, inhaled or ingested, skin contamination is considered external exposure.
External exposure is relatively easy to estimate, and the irradiated organism does not become radioactive, except for a case where the radiation is an intense neutron beam which causes activation.


=== Internal ===
Internal exposure occurs when the radioactive material enters the organism, and the radioactive atoms become incorporated into the organism. This can occur through inhalation, ingestion, or injection. Below are a series of examples of internal exposure.
The exposure caused by potassium-40 present within a normal person.
The exposure to the ingestion of a soluble radioactive substance, such as 89Sr in cows' milk.
A person who is being treated for cancer by means of a radiopharmaceutical where a radioisotope is used as a drug (usually a liquid or pill). A review of this topic was published in 1999. Because the radioactive material becomes intimately mixed with the affected object it is often difficult to decontaminate the object or person in a case where internal exposure is occurring. While some very insoluble materials such as fission products within a uranium dioxide matrix might never be able to truly become part of an organism, it is normal to consider such particles in the lungs and digestive tract as a form of internal contamination which results in internal exposure.
Boron neutron capture therapy (BNCT) involves injecting a boron-10 tagged chemical that preferentially binds to tumor cells. Neutrons from a nuclear reactor are shaped by a neutron moderator to the neutron energy spectrum suitable for BNCT treatment. The tumor is selectively bombarded with these neutrons. The neutrons quickly slow down in the body to become low energy thermal neutrons. These thermal neutrons are captured by the injected boron-10, forming excited (boron-11) which breaks down into lithium-7 and a helium-4 alpha particle both of these produce closely spaced ionizing radiation.This concept is described as a binary system using two separate components for the therapy of cancer. Each component in itself is relatively harmless to the cells, but when combined together for treatment they produce a highly cytocidal (cytotoxic) effect which is lethal (within a limited range of 5-9 micrometers or approximately one cell diameter). Clinical trials, with promising results, are currently carried out in Finland and Japan.
When radioactive compounds enter the human body, the effects are different from those resulting from exposure to an external radiation source. Especially in the case of alpha radiation, which normally does not penetrate the skin, the exposure can be much more damaging after ingestion or inhalation. The radiation exposure is normally expressed as a committed dose.


== History ==
Although radiation was discovered in late 19th century, the dangers of radioactivity and of radiation were not immediately recognized. Acute effects of radiation were first observed in the use of X-rays when Wilhelm Rntgen intentionally subjected his fingers to X-rays in 1895. He published his observations concerning the burns that developed, though he misattributed them to ozone, a free radical produced in air by X-rays. Other free radicals produced within the body are now understood to be more important. His injuries healed later.
As a field of medical sciences, radiobiology originated from Leopold Freund's 1896 demonstration of the therapeutic treatment of a hairy mole using a new type of electromagnetic radiation called x-rays, which was discovered 1 year previously by the German physicist, Wilhelm Rntgen. After irradiating frogs and insects with X-rays in early 1896, Ivan Romanovich Tarkhanov concluded that these newly discovered rays not only photograph, but also "affect the living function". At the same time, Pierre and Marie Curie discovered the radioactive polonium and radium later used to treat cancer.
The genetic effects of radiation, including the effects on cancer risk, were recognized much later. In 1927 Hermann Joseph Muller published research showing genetic effects, and in 1946 was awarded the Nobel prize for his findings.
Before the biological effects of radiation were known, many physicians and corporations had begun marketing radioactive substances as patent medicine and radioactive quackery. Examples were radium enema treatments, and radium-containing waters to be drunk as tonics. Marie Curie spoke out against this sort of treatment, warning that the effects of radiation on the human body were not well understood. Curie later died of aplastic anemia caused by radiation poisoning. Eben Byers, a famous American socialite, died of multiple cancers (but not acute radiation syndrome) in 1932 after consuming large quantities of radium over several years; his death drew public attention to dangers of radiation. By the 1930s, after a number of cases of bone necrosis and death in enthusiasts, radium-containing medical products had nearly vanished from the market.
In the United States, the experience of the so-called Radium Girls, where thousands of radium-dial painters contracted oral cancers (but no cases of acute radiation syndrome), popularized the warnings of occupational health associated with radiation hazards. Robley D. Evans, at MIT, developed the first standard for permissible body burden of radium, a key step in the establishment of nuclear medicine as a field of study. With the development of nuclear reactors and nuclear weapons in the 1940s, heightened scientific attention was given to the study of all manner of radiation effects.
The atomic bombings of Hiroshima and Nagasaki resulted in a large number of incidents of radiation poisoning, allowing for greater insight into its symptoms and dangers. Red Cross Hospital Surgeon, Dr. Terufumi Sasaki led intensive research into the Syndrome in the weeks and months following the Hiroshima bombings. Dr Sasaki and his team were able to monitor the effects of radiation in patients of varying proximities to the blast itself, leading to the establishment of three recorded stages of the syndrome. Within 25-30 days of the explosion, the Red Cross surgeon noticed a sharp drop in white blood cell count and established this drop, along with symptoms of fever, as prognostic standards for Acute Radiation Syndrome. Actress Midori Naka, who was present during the atomic bombing of Hiroshima, was the first incident of radiation poisoning to be extensively studied. Her death on August 24, 1945 was the first death ever to be officially certified as a result of radiation poisoning (or "Atomic bomb disease").


== Areas of interest ==
The interactions between organisms and electromagnetic fields (EMF) and ionizing radiation can be studied in a number of ways:
Radiation physics
Radiation chemistry
molecular and cell biology
Molecular genetics
Cell death and apoptosis
Dose modifying agents
Protection and repair mechanisms
Tissue responses to radiation
Radio-adaptation of living organisms
High and low-level electromagnetic radiation and health
Specific absorption rates of organisms
Radiation poisoning
Radiation oncology (radiation therapy in cancer)
Bioelectromagnetics
Electric field and Magnetic field - their general nature.
Electrophysiology - the scientific study of the electrical properties of biological cells and tissues.
Biomagnetism - the magnetic properties of living systems (see, for example, the research of David Cohen using SQUID imaging) and Magnetobiology - the study of effect of magnets upon living systems. See also Electromagnetic radiation and health
Bioelectromagnetism - the electromagnetic properties of living systems and Bioelectromagnetics - the study of the effect of electromagnetic fields on living systems.
Electrotherapy
Radiation therapy
Radiogenomics
Electroconvulsive therapy
Transcranial magnetic stimulation - a powerful electric current produces a transient, spatially focussed magnetic field that can penetrate the scalp and skull of a subject and induce electrical activity in the neurons on the surface of the brain.
Magnetic resonance imaging - a very powerful magnetic field is used to obtain a 3D image of the density of water molecules of the brain, revealing different anatomical structures. A related technique, functional magnetic resonance imaging, reveals the pattern of blood flow in the brain and can show which parts of the brain are involved in a particular task.
Embryogenesis, Ontogeny and Developmental biology - a discipline that has given rise to many scientific field theories.
Bioenergetics - the study of energy exchange on the molecular level of living systems.
Biological psychiatry, Neurology, Psychoneuroimmunology
Bioluminescence - a marked phosphoresecence found in fungi, deep-sea creatures etc., as against Biophoton - a much weaker electromagnetic radiation, thought by Alexander Gurwitsch, its discoverer, to be a form of signalling.
The activity of biological and astronomical systems inevitably generates magnetic and electrical fields, which can be measured with sensitive instruments and which have at times been suggested as a basis for "esoteric" ideas of energy.


== Radiation sources for radiobiology ==
Radiobiology experiments typically make use of a radiation source which could be:
An isotopic source, typically 137Cs or 60Co.
A particle accelerator generating high energy protons, electrons or charged ions. Biological samples can be irradiated using either a broad, uniform beam or using a microbeam, focused down to cellular or subcellular sizes.
A UV lamp.


== See also ==
Background radiation
Cell survival curve
Health threat from cosmic rays
NASA Space Radiation Laboratory
Nuclear medicine
Radioactivity in biology
Radiology
Radiophobia
Radiosensitivity
Relative biological effectiveness


== References and further reading ==
^ "RADIATION HORMESIS CHALLENGING LNT THEORY VIA ECOLOGICAL AND EVOLUTIONARY CONSIDERATIONS" (PDF). Publication date 2002. Health Physics Society. Retrieved 2010-12-11. 
^ a b c Christensen DM, Iddins CJ, Sugarman SL (February 2014). "Ionizing radiation injuries and illnesses". Emerg Med Clin North Am 32 (1): 24565. doi:10.1016/j.emc.2013.10.002. PMID 24275177. 
^ Wynn, Volkert; Hoffman, Timothy (1999). "Therapeutic Radiopharmaceuticals afrtin=2+3=9000" (PDF). Chemical Reviews 99 (9): 226992. doi:10.1021/cr9804386. PMID 11749482. 
^ Y. B. Kudriashov. Radiation Biophysics. ISBN 9781600212802. Page xxi.
^ Grady, Denise (October 6, 1998). "A Glow in the Dark, and a Lesson in Scientific Peril". The New York Times. Retrieved November 25, 2009. 
^ Rowland, R.E. (1994). Radium in Humans: A Review of U.S. Studies (PDF). Argonne National Laboratory. Retrieved 24 May 2012. 
^ Carmichael, Ann G. (1991). Medicine: A Treasury of Art and Literature. New York: Harkavy Publishing Service. p. 376. ISBN 0-88363-991-2. 
^ Pattison, J. E., Hugtenburg, R. P., Beddoe, A. H. and Charles, M. W. (2001), Experimental Simulation of A-bomb Gamma-ray Spectra for Radiobiology Studies, Radiation Protection Dosimetry 95(2):125-136.
WikiMindMap
Eric Hall, Radiobiology for the Radiologist. 2006. Lippincott
G.Gordon Steel, "Basic Clinical Radiobiology". 2002. Hodder Arnold.
The Institute for Radiation Biology at the Helmholtz-Center for Environmental Health [1]