PSR J1719-1438 b is an extrasolar planet that was discovered on August 25, 2011 in orbit around PSR J1719-1438, a millisecond pulsar. The pulsar planet is most likely composed largely of crystalline carbon, but with a density far greater than diamond. PSR J1719-1438 b orbits so closely to its host star, the planet's orbit would fit inside the Sun. The existence of such carbon planets had been theoretically postulated.


== Observational history ==
PSR J1719-1438 was first observed in 2009 by a team headed by Matthew Bailes of Swinburne University of Technology in Melbourne, Australia. The orbiting planet was published in the journal Science on August 25, 2011. The planet was confirmed through pulsar timing, in which small modulations detected in the highly regular pulsar signature are measured and extrapolated. Observatories in Britain, Hawaii, and Australia were used to confirm these observations.


== Host star ==

PSR J1719-1438 is a pulsar some 4,000 light years away from Earth in the Serpens Cauda constellation, approximately one minute from the border with Ophiuchus. The pulsar completes more than 10,000 rotations a minute. It is approximately 12 miles across, but has a mass that is 1.4 solar masses.


== Characteristics ==
PSR J1719-1438 b was, at the time of its August 25, 2011 discovery, the densest planet ever discovered, at nearly 20 times the density of Jupiter (about 23 times the density of water). It is slightly more massive than Jupiter. It is thought to be composed of oxygen and carbon (as opposed to hydrogen and helium, the main components of gas giants like Jupiter and Saturn).
The oxygen is most likely on the surface of the planet, with increasingly higher quantities of carbon deeper inside the planet. The intense pressure acting upon the planet suggests that the carbon is crystallized, much like diamond is.
PSR J1719-1438 b orbits its host star with a period of 2.177 hours and at a distance of a little bit less than one (0.89) solar radius.


== See also ==
WASP-12b, a carbon planet
BPM 37093, a carbon star
EF Eridani, a star system with a compact star and a degraded planetary-mass former star


== References ==


== Further reading ==
Bailes, M.; Bates, S. D.; Bhalerao, V.; Bhat, N. D. R.; Burgay, M.; Burke-Spolaor, S.; d'Amico, N.; Johnston, S.; Keith, M. J.; et al. (2011). "Transformation of a Star into a Planet in a Millisecond Pulsar Binary" (PDF). Science 333 (6050): 171720. arXiv:1108.5201. Bibcode:2011Sci...333.1717B. doi:10.1126/science.1208890. PMID 21868629. 
"A Planet made of Diamond" (Press release). Max Planck Institute for Radio Astronomy. August 25, 2011. Retrieved August 26, 2011. 
Correa, Alfredo A.; Bonev, Stanimir A.; Galli, Giulia (2006). "Carbon under extreme conditions: Phase boundaries and electronic properties from first-principles theory". Proceedings of the National Academy of Sciences 103 (5): 12048. Bibcode:2006PNAS..103.1204C. doi:10.1073/pnas.0510489103. PMC 1345714. PMID 16432191.