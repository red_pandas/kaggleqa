A cartoon is a form of two-dimensional illustrated visual art. While the specific definition has changed over time, modern usage refers to a typically non-realistic or semi-realistic drawing or painting intended for satire, caricature, or humor, or to the artistic style of such works. An artist who creates cartoons is called a cartoonist.
The concept originated in the Middle Ages and first described a preparatory drawing for a piece of art, such as a painting, fresco, tapestry, or stained glass window. In the 19th century, it came to refer to humorous illustrations in magazines and newspapers, and after the early 20th century, it referred to comic strips and animated films.


== Fine artEdit ==

A cartoon (from Italian: cartone and Dutch: kartonwords describing strong, heavy paper or pasteboard) is a full-size drawing made on sturdy paper as a study or modello for a painting, stained glass or tapestry. Cartoons were typically used in the production of frescoes, to accurately link the component parts of the composition when painted on damp plaster over a series of days (giornate).
Such cartoons often have pinpricks along the outlines of the design so that a bag of soot patted or "pounced" over the cartoon, held against the wall, would leave black dots on the plaster ("pouncing"). Cartoons by painters, such as the Raphael Cartoons in London, and examples by Leonardo da Vinci, are highly prized in their own right. Tapestry cartoons, usually coloured, were followed with the eye by the weavers on the loom.


== Print mediaEdit ==

In modern print media, a cartoon is a piece of art, usually humorous in intent. This usage dates from 1843, when Punch magazine applied the term to satirical drawings in its pages, particularly sketches by John Leech. The first of these parodied the preparatory cartoons for grand historical frescoes in the then-new Palace of Westminster. The original title for these drawings was Mr Punch's face is the letter Q and the new title "cartoon" was intended to be ironic, a reference to the self-aggrandizing posturing of Westminster politicians.
Modern single-panel gag cartoons, found in magazines, generally consist of a single drawing with a typeset caption positioned beneath, ormuch less oftena speech balloon. Newspaper syndicates have also distributed single-panel gag cartoons by Mel Calman, Bill Holman, Gary Larson, George Lichty, Fred Neher and others. Many consider New Yorker cartoonist Peter Arno the father of the modern gag cartoon (as did Arno himself). The roster of magazine gag cartoonists includes names like Charles Addams, Charles Barsotti and Chon Day.
Bill Hoest, Jerry Marcus and Virgil Partch began as magazine gag cartoonists and moved to syndicated comic strips. Richard Thompson is noteworthy in the area of newspaper cartoon illustration; he illustrated numerous feature articles in The Washington Post before creating his Cul de Sac comic strip. The sports section of newspapers usually featured cartoons, sometimes including syndicated features such as Chester "Chet" Brown's All in Sport.
Editorial cartoons are found almost exclusively in news publications and news websites. Although they also employ humor, they are more serious in tone, commonly using irony or satire. The art usually acts as a visual metaphor to illustrate a point of view on current social and/or political topics. Editorial cartoons often include speech balloons and sometimes use multiple panels. Editorial cartoonists of note include Herblock, David Low, Jeff MacNelly, Mike Peters and Gerald Scarfe.
Comic strips, also known as cartoon strips in the United Kingdom, are found daily in newspapers worldwide, and are usually a short series of cartoon illustrations in sequence. In the United States, they are not commonly called "cartoons" themselves, but rather "comics" or "funnies". Nonetheless, the creators of comic stripsas well as comic books and graphic novelsare usually referred to as "cartoonists". Although humor is the most prevalent subject matter, adventure and drama are also represented in this medium. Some noteworthy cartoonists of humorous comic strips are Scott Adams, Steve Bell, Charles Schulz, E. C. Segar, Mort Walker and Bill Watterson.


== Political cartoonsEdit ==

Political cartoons are like illustrated editorial that serve visual commentaries on political events. They offer subtle criticism which are cleverly quoted with humour and satire to the extent that the criticized does not get embitered. The pictorial satire of William Hogarth is regarded as a precursor to the development of political cartoons in 18th century England. George Townshend produced some of the first overtly political cartoons and caricatures in the 1750s. The medium began to develop in the latter part of the 18th century under the direction of its great exponents, James Gillray and Thomas Rowlandson, both from London. Gillray explored the use of the medium for lampooning and caricature, and has been referred to as the father of the political cartoon. By calling the king, prime ministers and generals to account for their behaviour, many of Gillray's satires were directed against George III, depicting him as a pretentious buffoon, while the bulk of his work was dedicated to ridiculing the ambitions of revolutionary France and Napoleon. George Cruikshank became the leading cartoonist in the period following Gillray, from 1815 until the 1840s. His career was renowned for his social caricatures of English life for popular publications.

By the mid 19th century, major political newspapers in many other countries featured cartoons commenting on the politics of the day. Thomas Nast, in New York City, showed how realistic German drawing techniques could redefine American cartooning. His 160 cartoons relentlessly pursued the criminal characteristic of the Tweed machine in New York City, and helped bring it down. Indeed, Tweed was arrested in Spain when police identified him from Nast's cartoons. Sir John Tenniel was the toast of London.
Political cartoons can be humorous or satirical, sometimes with piercing effect. The target of the humor may complain, but they can seldom fight back. Lawsuits have been very rare; the first successful lawsuit against a cartoonist in over a century in Britain came in 1921, when J. H. Thomas, the leader of the National Union of Railwaymen (NUR), initiated libel proceedings against the magazine of the British Communist Party. Thomas claimed defamation in the form of cartoons and words depicting the events of "Black Friday", when he allegedly betrayed the locked-out Miners' Federation. To Thomas, the framing of his image by the far left threatened to grievously degrade his character in the popular imagination. Soviet-inspired communism was a new element in European politics, and cartoonists unrestrained by tradition tested the boundaries of libel law. Thomas won the lawsuit and restored his reputation.


== Scientific cartoonsEdit ==
Cartoons have also found their place in the world of science, mathematics and technology. In the U.S., one well-known cartoonist for these fields is Sidney Harris. Not all, but many of Gary Larson's cartoons have a scientific flavor.
Cartoons related to chemistry are, for example, xkcd, which varies its subject matter, and the Wonderlab, which looks at daily life in the lab.


== BooksEdit ==
Books with cartoons are usually reprints of newspaper cartoons.
On some occasions, new gag cartoons have been created for book publication, as was the case with Think Small, a 1967 promotional book distributed as a giveaway by Volkswagen dealers. Bill Hoest and other cartoonists of that decade drew cartoons showing Volkswagens, and these were published along with humorous automotive essays by such humorists as H. Allen Smith, Roger Price and Jean Shepherd. The book's design juxtaposed each cartoon alongside a photograph of the cartoon's creator.


== AnimationEdit ==

Because of the stylistic similarities between comic strips and early animated movies, cartoon came to refer to animation, and the word "cartoon" is currently used in reference to both animated cartoons and gag cartoons. While animation designates any style of illustrated images seen in rapid succession to give the impression of movement, the word "cartoon" is most often used as a descriptor for television programs and short films aimed at children, possibly featuring anthropomorphized animals, superheroes, the adventures of child protagonists and/or related themes.
At the end of the 1980s, "cartoon" was shortened to make the word "toon", which came into use with the live-action/animated film Who Framed Roger Rabbit (1988), followed two years later by the animated TV series Tiny Toon Adventures (1990).


== See alsoEdit ==

Billy Ireland Cartoon Library & Museum
Caricature
Comics
Comics Studies
Editorial cartoon
List of comic strips
List of cartoonists
List of editorial cartoonists


== ReferencesEdit ==


== Further readingEdit ==
Becker, Stephen D. and Rube Goldberg. Comic Art in America: A Social History of the Funnies, the Political Cartoons, Magazine Humor, Sporting Cartoons, and Animated Cartoons (1959)
Blackbeard, Bill, ed. The Smithsonian Collection of Newspaper Comics (1977, Smithsonian Inst. Press)
Robinson, Jerry, The Comics: An Illustrated History of Comic Strip Art (1974) G.P. Putnam's Sons
Horn, Maurice, The World Encyclopedia of Comics (1976) Chelsea House, (1982)
Walasek, Helen (2009). The Best of Punch Cartoons: 2,000 Humor Classics. England: Overlook Press. ISBN 1590203089. 


== External linksEdit ==
Dan Becker, History of Cartoons
Marchand collection cartoons & photos
Stamp Act 1765 with British & American cartoons
Harper's Weekly 150 cartoons on elections 1860-1912; Reconstruction topics; Chinese exclusion; plus American Political Prints from the Library of Congress, 17661876
"Graphic Witness" political caricatures in history
Keppler cartoons
current editorial cartoons
Index of cartoonists in the Fred Waring Collection
International Society for Humor Studies
Fiore, R. (2010-01-31). "Adventures in Nomenclature: Literal, Liberal and Freestyle". The Comics Journal. Fantagraphics Books. Retrieved 2013-02-05.