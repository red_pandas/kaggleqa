In chemistry and physics, Dalton's law (also called Dalton's law of partial pressures) states that in a mixture of non-reacting gases, the total pressure exerted is equal to the sum of the partial pressures of the individual gases. This empirical law was observed by John Dalton in 1801 and is related to the ideal gas laws.


== Formula ==
Mathematically, the pressure of a mixture of non-reactive gases can be defined as the summation:
       or      
where  represent the partial pressure of each component.

where  is the mole fraction of the i-th component in the total mixture of n components .


== Volume-based concentration ==
The relationship below provides a way to determine the volume-based concentration of any individual gaseous component

where  is the concentration of the i-th component expressed in ppm.
Dalton's law is not strictly followed by real gases, with deviations being considerably large at high pressures. Under such conditions the volume occupied by the molecules can become significant compared to the free space between them. In particular, the short average distances between molecules raises the intensity of intermolecular forces between gas molecules enough to substantially change the pressure exerted by them. Neither of those effects are considered by the ideal gas model.


== References ==


== See also ==

Henry's law
Amagat's law
Boyle's law
Combined gas law
Gay-Lussac's law
Mole (unit)
Partial pressure
Vapour pressure