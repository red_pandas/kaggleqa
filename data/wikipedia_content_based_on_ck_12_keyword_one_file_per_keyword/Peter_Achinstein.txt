Peter Achinstein (born June 30, 1935) is an American philosopher of science. He is the Jay and Jeanie Schottenstein University Professor of Philosophy at Yeshiva University, director of the Yeshiva Center for History and Philosophy of Science, and a professor at Johns Hopkins University.


== Biography ==
Achinstein received his B.A. and Ph.D. from Harvard with a dissertation on Carnap's theory of probability. It was the German philosopher Carl G. Hempel, in a visit to Harvard in 19534 (replacing W.V. Quine who was on leave), that motivated him to pursue Philosophy of Science. Upon getting a Harvard Traveling Fellowship, Achinstein spent a year in Oxford in 1959 working under the guidance of P. F. Strawson. In Oxford he attended seminars and lectures delivered by Gilbert Ryle, A.J. Ayer, and J.L. Austin. Achinstein specializes in philosophy of science with strong interests in the history of science.
Achinstein has taught for many years at Johns Hopkins University, where he is currently Professor of Philosophy. In Spring 2009, Achinstein began teaching at Yeshiva University as the Jay and Jeanie Schottenstein University Professor of Philosophy and is the founder and director of the Center for History and Philosophy of Science of Yeshiva University, New York. He returned to Johns Hopkins in Spring 2011. He has held Guggenheim, NEH, and NSF fellowships, and has served as a visiting professor at MIT, Stanford, and the Hebrew University of Jerusalem.
He is the author of five influential books in the history and philosophy of science. Among them are Particles and Waves, which received the prestigious Lakatos Award in 1993. This book is a study of methodological problems arising from three episodes in 19th century physics: the wave-particle debate about light, the development of the kinetic-molecular theory, and the discovery of the electron. In 2001, Achinstein published The Book of Evidence, a philosophical and historical study of various concepts of evidence employed in the sciences. A volume of his important collected essays over the years, Evidence, Explanation, and Realism, was published in the spring of 2010. A special volume honoring him, Philosophy of Science Matters: The Philosophy of Peter Achinstein, was published in 2011. In 2013, Achinstein published Evidence and Method: Scientific Strategies of Isaac Newton and James Clerk Maxwell.


== Former students of Peter Achinstein ==
Many of Professor Achinstein's students have gone on in careers as philosophers. Among his best-known students are Alexander Rosenberg and Helen Longino.


== Publications ==


=== Books, monographs, and collected papers ===
Concepts of Science: A Philosophical Analysis (1968)
Law and Explanation: An Essay in the Philosophy of Science (1971)
The Nature of Explanation (1983)
The Concept of Evidence (1983)
Particles and Waves: Historical Essays in the Philosophy of Science (1991)
The Book of Evidence (2003)
Evidence, Explanation, and Realism: Essays in Philosophy of Science by Peter Achinstein (2010)
Evidence and Method: Scientific Strategies of Isaac Newton and James Clerk Maxwell (2013)


=== Edited volumes ===
The Legacy of Logical Positivism in the Philosophy of Science (co-editor with Stephen F. Barker, 1969)
Studies in the philosophy of science; essays by Peter Achinstein [and others] (1969)
Explanations: Papers and Discussions (Achinstein, et al.; edited by Stephan Krner. 1975)
Observation, Experiment, and Hypothesis in Modern Physical Science (co-editor with Owen Hannaway, 1985)
Kelvin's Baltimore Lectures and Modern Theoretical Physics: Historical and Philosophical Perspectives (co-editor with Robert Kargon, 1987)
Scientific Methods: Conceptual and Historical Problems (co-editor with Laura J. Snyder, 1994)
Science Rules: A Historical Introduction to Scientific Methods (editor, 2004)
Scientific Evidence: Philosophical Theories and Applications (editor, 2005)


=== Selected articles ===
"Is There a Valid Experimental Argument for Scientific Realism?" Journal of Philosophy (2002).
"What to do if you want to Defend a Theory you can't Prove?" Journal of Philosophy (2010).


== See also ==
American philosophy
List of American philosophers
List of Jewish American Philosophers


== External links ==
Official Page