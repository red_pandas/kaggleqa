A monatomic ion is an ion consisting of a single atom. If an ion contains more than one atom, even if these atoms are of the same element, it is called a polyatomic ion. For example, calcium carbonate consists of the monatomic ion Ca2+ and the polyatomic ion CO32.
A type I binary ionic compound contains a metal (cation) that forms only one type of ion. A type II ionic compound contains a metal that forms more than one type of ion, i.e., ions with different charges.


== See also ==
monatomic
polyatomic ion


== References ==