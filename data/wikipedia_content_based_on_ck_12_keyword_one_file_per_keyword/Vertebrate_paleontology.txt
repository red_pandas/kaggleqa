Vertebrate paleontology is a large subfield to paleontology seeking to discover the behavior, reproduction and appearance of extinct animals with vertebrae or a notochord, through the study of their fossilized remains. It also tries to connect, by using the evolutionary timeline, the animals of the past and their modern-day relatives.
The fossil record shows aspects of the meandering evolutionary path from early aquatic vertebrates to mammals, with a host of transitional fossils, though there are still large blank areas. The earliest known fossil vertebrates were heavily armored fish discovered in rocks from the Ordovician Period about 500 to 430 Ma (megaannum, million years ago). The Devonian Period (395 to 345 Ma) brought in the changes that allowed primitive air-breathing fish to remain on land as long as they wished, thus becoming the first terrestrial vertebrates, the amphibians.
Amphibians developed forms of reproduction and locomotion and a metabolism better suited for life exclusively on land, becoming more reptilian. Full-fledged reptiles appeared in the Carboniferous Period (345 to 280 Ma).
The reptilian changes and adaptations to diet and geography are chronicled in the fossil record of the varying forms of therapsida. True mammals showed up in the Triassic Period (225 to 190 Ma) around the same time as the dinosaurs, which also sprouted from the reptilian line.
Birds first diverged from dinosaurs between 100 Ma and 60 Ma.


== History ==
One of the people who helped figure out the vertebrate progression was French zoologist Georges Cuvier (17691832), who realized that fossils found in older rock strata differed greatly from more recent fossils or modern animals. He published his findings in 1812 and, although he steadfastly refuted evolution, his work proved the (at the time) contested theory of extinction of species.
Thomas Jefferson is credited with initiating the science of vertebrate paleontology in the United States with the reading of a paper to the American Philosophical Society in Philadelphia in 1797. Jefferson presented fossil bones of a ground sloth found in a cave in western Virginia and named the genus (Megalonyx). The species was ultimately named Megalonyx jeffersonii in his honor. Jefferson corresponded with Cuvier, including sending him a shipment of highly desirable bones of the American mastodon and the woolly mammoth.
Paleontology really got started though, with the publication of Recherches sur les poissons fossils (18331843) by Swiss naturalist Louis Agassiz (18071873). He studied, described and listed hundreds of species of fossil fish, beginning the serious study into the lives of extinct animals. With the publication of the Origin of Species by Charles Darwin in 1849, the field got a theoretical framework. Much of the subsequent work has been to map the relationship between fossil and extant organisms, as well as their history through time.
In modern times, Alfred Romer (18941973) wrote what has been termed the definitive textbook on the subject, called Vertebrate Paleontology. It shows the progression of evolution in fossil fish, and amphibians and reptiles through comparative anatomy, including a list of all the (then) known fossil vertebrate genera. Romer became the first president of the Society of Vertebrate Paleontology in 1940, alongside co-founder Howard Chiu. An updated work that largely carried on the tradition from Romer, and by many considered definitive book on the subject was written by Robert L. Carroll of McGill University, the 1988 text Vertebrate Paleontology and Evolution. Carroll was president of the Society of Vertebrate Paleontology in 1983. The Society keeps its members informed on the latest discoveries through newsletters and the Journal of Vertebrate Paleontology.


== Paleontological Vertebrate Classification ==

The 'traditional' vertebrate classification scheme employ evolutionary taxonomy where several of the taxa listed are paraphyletic, i.e. have given rise to another taxa that have been given the same rank. For instance, birds are generally considered to be the descendants of reptiles (Saurischian dinosaurs to be precise), but in this system both are listed as separate classes. Under phylogenetic nomenclature, such an arrangement is unacceptable, though it offers excellent overview.
This classical scheme is still used in works where systematic overview is essential, e.g. Benton (1998), Hildebrand and Goslow (2001) and Knobill and Neill (2006). While mostly seen in general works, it is also still used in some specialist works like Fortuny & al. (2011).
Kingdom Animalia
Phylum Chordata (vertebrates)
Class Agnatha (jawless fish)
Subclass Cyclostomata (hagfish and lampreys)
Subclass Ostracodermi (armoured jawless fish) 

Class Chondrichthyes (cartilaginous fish)
Subclass Elasmobranchii (sharks and rayes)
Subclass Holocephali (chimaeras and extinct relatives)

Class Placodermi (armoured fish) 
Class Acanthodii ("spiny sharks", sometimes classified under bony fishes) 
Class Osteichthyes (bony fish)
Subclass Actinopterygii
Subclass Sarcopterygii

Class Amphibia
Subclass Labyrinthodontia 
Subclass Lepospondyli 
Subclass Lissamphibia

Class Reptilia
Subclass Anapsida
Order Cotylosauria 
Order Testudines

Subclass Synapsida
Order Pelycosauria 
Order Therapsida 

Subclass Euryapsida
Order Sauropterygia 
Order Ichthyosauria 

Subclass Diapsida (lizards & snakes too)
Order Crocodilia (crocodiles, alligators etc.)
Order Sphenodontia (Tuatara and relatives)
Order Squamata (Lizards and snakes)
Order Thecodonts 
Order Pterosauria 
Order Saurischia (dinosaurs) 
Order Ornithischia (dinosaurs) 

Class Aves
Subclass Archaeornithes (primitive dinosaur-like birds like Archaeopteryx) 
Subclass Neornithes (modern birds and some advanced Cretaceous forms)
Superorder Odontognathae (Cretaceous toothed birds) 
Superorder Palaeognathae (ratites)
Superorder Neognathae (All other extant birds)

Class Mammalia
Subclass Prototheria
Order Monotremata (platypus and the echidnas)

Subclass Theria
Infraclass Metatheria
Order Marsupialia (kangaroos, dunnarts, opossums, wombats etc.)

Infraclass Eutheria (placentals)
Order Insectivora
Order Chiroptera (bats)
Order Creodonta
Order Carnivora (dogs/cats)
Order Perissodactyla (horses)
Order Artiodactyla (cattle and other ungulates)
Order Proboscidea (elephants)
Order Edentata
Order Cetacea (whales and dolphins)
Order Rodentia (mice, rats etc.)
Order Lagomorpha (rabbits)
Order Primates (monkeys, apes and primates)


== See also ==
Evolution of fish
Evolution of mammals
Evolution of reptiles


== References ==


== Further reading ==
Anderson, Jason S.and Sues, Hans-Dieter (eds.) (2007). Major Transitions in Vertebrate Evolution. Bloomington, Ind.: Indiana University Press. ISBN 0253349265. 
Carroll, Robert L. (1997). Patterns and Processes of Vertebrate Evolution. New York: Cambridge University Press. ISBN 978-0521478090.