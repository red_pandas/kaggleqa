Dinosaur Train is an American/Canadian/Singaporean animated series created by Craig Bartlett. The series features a curious young Tyrannosaurus rex named Buddy who, together with his adopted Pteranodon family, takes the Dinosaur Train to explore his time period, and have adventures with all kinds of dinosaurs. It is produced by The Jim Henson Company in association with Media Development Authority, Sparky Animation, FableVision, and Snee-Oosh, Inc.
It is the second show by The Jim Henson Company to use CGI animation (the first of which was Sid the Science Kid).


== Overview ==
The show is set in a whimsical prehistoric world of jungles, swamps, active volcanoes and oceans, all teeming with dinosaur and other animal life, and connected by a train line known as the Dinosaur Train. This steam-engine train can be customized for dinosaurs of all kinds: windows accommodate the long-necked sauropods, there's plenty of headroom in the Observation Car for the larger theropods, and the Aquacar is an aquarium for sea-going passengers. The train itself is run by Troodons, as the smartest dinosaurs in this fictional universe. The Dinosaur Train circles the whole worldit even crosses the oceans and inland seas, with stops to visit undersea prehistoric animals. It can travel through the entire Mesozoic Era, the "Age of Dinosaurs", passing through magical Time Tunnels to the Triassic, Jurassic, and Cretaceous time periods.
The world of Dinosaur Train is seen through the eyes of Buddy the Tyrannosaurus. In the show's main title song, we learn that Buddy was adopted by Mr. and Mrs. Pteranodon. He hatched at the same time as his pteranodon siblings Tiny, Shiny, and Don. By traveling in the Dinosaur Train around the Mesozoic, supplied with all dinosaur facts by the train's Troodon Conductor, Buddy learns that he is a Tyrannosaurus. As an adopted kid in a mixed-species family, Buddy is curious about the differences between species and vows to learn about all the dinosaurs he can by riding the Dinosaur Train. The dinosaurs featured in the show are actual dinosaurs discovered by paleontologists.
Dinosaur Train is co-produced and animated by Sparky Animation Studios in Singapore, with casting by Vidaspark and voice-overs recorded at Kozmic Sound in Vancouver, British Columbia, Canada. It premiered on Labor Day 2009 and airs daily on PBS Kids, and in various countries around the world. 40 half-hour episodes have been ordered by PBS Kids so far. Each episode is followed by a live-action segment featuring Dinosaur Train educational consultant and world-renowned paleontologist Dr. Scott D. Sampson, who appears onscreen to explain the show's dinosaur curriculum in greater detail.


== Characters ==


== Cast ==
Phillip Corlett  Buddy Tyrannosaurus (first voice)
Sean Thomas  Buddy Tyrannosaurus (second voice)
Claire Corlett  Tiny Pteranodon
Ian James Corlett  Mr. Conductor, Adam Adocus, Alan Alamosaurus, Elliott Enantiornithine (in "Now with Feathers"), Morris Stegosaurus, Mr. Argentinosaurus, Mr. Daspletosaurus, Mr. Elasmosaurus, Stuart Stygimoloch, Travis Troodon, Triceratops, Troodon Official, Vincent Velociraptor
Erika-Shaye Gair  Shiny Pteranodon, Annie Tyrannosaurus, Cory Corythosaurus
Ellen Kennedy  Mrs. Pteranodon, Mrs. Einiosaurus, Mrs. Elasmosaurus
Alexander Matthew Marr  Don Pteranodon (first voice), Tank Triceratops
Laura Marr  Don Pteranodon (second voice)
Colin Murdock  Mr. Pteranodon, Announcer, Elliott Enantiornithine, Hank Ankylosaurus, Ralph Einiosaurus, Station Master, Stuart Styracosaurus, Ziggy Zhejiangopterus, Carson Carcharodontosaurus


=== Additional voices ===
Mark Acheson  Marvin Mosasaurus
Michael Adamthwaite  Jess Hesperornis, Reggie Raptorex
Ashleigh Ball  Arnie Argentinosaurus, Crystal Cryolophosaurus, Gilbert Troodon, Lorraine Lambeosaurus, Maiasaura Mom, Millie Maiasaura, Mrs. Therizinosaurus, Oren Ornithomimus, Reba Rhabdodon
Kathleen Barr  Angela Avisaurus (in "T.rex Migration"), Dolores Tyrannosaurus, Erma Eoraptor, Fanny Fabrosaurus, Jacqueline Jaxartosaurus, Laura Giganotosaurus, Mrs. Corythosaurus, Mrs. Ornithomimus, Ned Brachiosaurus, Ollie Ornithomimus, Pauline Proganochelys, Peggy Peteinosaurus, Trudy Triceratops, Tuck Triceratops, Velma Velociraptor, Velociraptor Mom
Craig Bartlett  Spider
Nicole Bouma  Soren Saurornitholestes
Jason Bryden  Tommy Ptilodus
Jim Byrnes  Percy Paramacellodus
Natasha Calis  Leslie Lesothosaurus, Maisie Mosasaurus
Adrienne Carter  Kiera Chirostenotes
Garry Chalk  Marco Megaraptor
Shannon Chan-Kent  Allie Alamosaurus
Allison Cohen  Carla Cretoxyrhina
Dylan Sloane Cowan  Rodney Raptorex
Michelle Creber  Michelle Maiasaura
Brenda Crichlow  Denise Diplodocus
Deb Demille  Deanna Deinosuchus
Trevor Devall  Boris Tyrannosaurus, Bucky Masiakasaurus, Mr. Deinonychus, Thurston Troodon
Michael Dobson  Old Spinosaurus
Brian Drummond  Alvin Allosaurus, Apollo Apatosaurus, Eugene Euoplocephalus, Larry Lambeosaurus, Mr. Quetzalcoatalus, Mr. Therizinosaurus, Quinn Qantassaurus, Sammy the Slug, Ulysses Utahraptor, Zhuang Zigongosaurus
Brynna Drummond  Daphne Daspletosaurus
Mitchell Duffield  Mookie Maiasaura
Alex Ferris  Paulie Pliosaurus
Andrew Francis  Patrick Pachycephalosaurus
Alberto Gishi  Mitch Michelinoceras
Nico Gishi  Leroy Lambeosaurus, Max Michelinoceras
Gordon Grice  Derek Deinonychus, Elmer Elasmosaurus
Olivia Hamilton  Lily Lambeosaurus
Phil Hayes  Chung Confuciusornis, King Cryolophosaurus
Maryke Hendrikse  Penelope Protoceratops
Ryan Hirakida  Dylan Dilophosaurus
Alessandro Juliani  Martin Amargasaurus
Diana Kaarina  Tricia (first voice)
James Kirk  Perry Parasaurolophus, Rick Oryctodromeus, Nick Oryctodromeus
Andrea Libman  Pamela Pachycephalosaurus
Alan Marriott  Henry Hermit Crab, Herbie Hermit Crab, Iggy Iguanodon, Mayor Kosmoceratops
Erin Mathews  Judy Jeholornis, Stacle Styracosaurus, Vera Velociraptor
Donnie McNeil  Devlin Dilophosaurus
Jason Michas  Henry Hermit Crab
Bill Mondy  Jack Einiosaurus
Peter New  Sydney Sinovenator
Nicole Oliver  Brenda Brachiosaurus, Mrs. Pliosaurus
Cedric Payne  Petey Peteinosaurus
Adrian Petriw  Vlad Volaticotherium
Joseph Purdy  Mr. Disclaimer
Kelly Sheridan  Olivia Oviraptor
Valin Shinyei  Sonny Sauroposeidon
Rebecca Shoichet  Tricia (second voice)
Tabitha St. Germain  Angela Avisaurus, Arlene Archaeopteryx, Cindy Cimolestes, Gabby Gallimimus, Keenan Chirostenotes, Computer Voice, Mikey Microraptor, Minnie Microraptor, Mom Archelon, Mrs. Conductor, Mrs. Deinonychus, Patricia Palaeobatrachus, Rita Raptorex, Sana Sanajeh, Selma Cimolestes, Shirley Stygimoloch, Sonja Styracosaurus, Spiky Stygimoloch, Stella Sea Star, Stephie Styracosaurus
Chantal Strand  Valerie Velociraptor
Lee Tockar  Crab, Craig Cretoxyrhina
Kira Tozer  Ella Brachiosaurus
Chiara Zanni  Shoshana Shonisaurus


== Segments ==
Time for a Tiny Ditty  Tiny either tries singing about something she learned on the show or about her favorite dish (fish).
Buddy has a Hypothesis  Children learn from Buddy and Tiny what a hypothesis means.
Dr. Scott the Paleontologist  He appears on the show to teach children about the dinosaurs which have appeared in each episode, and how dinosaurs compare to present day animals (including humans). Dr. Scott Sampson is a paleontologist. He received his PhD in zoology from the University of Toronto in 1993 and currently works as an adjunct associate professor at the University of Utah.


== Songs ==
"Theme Song"  A ballad about a Mom named Mrs. Pteranodon and her children with a T. Rex, revealed to be written by King in his debut episode.
"Hungry Hungry Herbivore"  An adult Brachiosaurus (judging by his deep voice) shows up to sing a song about how herbivores love to eat green food.
"I'm a T-Rex" - Buddy sings that he finally realizes that he is T-Rex, and sings about living in the Cretaceous forest.
"Dinosaurs A-Z"  Mr. Conductor sings the Dinosaur Alphabet that his mother taught him.
"Cryolophosaurus Crests"  King sings about his crest.
"My Tiny Place"  Tiny sings about her "Tiny Place", which is actually her hideout in the form of a small hole in a tree. She sometimes shares it with Cindy Cimolestes.
"Nice To Meet You (My Name is Tiny)"  Tiny's song she sings to Leslie to tell her how she can talk to other dinosaurs without getting scared.
"All Aboard"  The original theme song before the official Dinosaur Train theme song.
"Tiny Loves Fish"  Tiny sings a song about how she loves fish.
"I Love Trains"  The Conductor sings about his love for trains (particularly the Dinosaur Train).
"Dinosaur Feet"  Daphne Daspletosaurus and the gang sings about their great big stomping dinosaur feet.
"Sleep Little Dinosaur"  The Pteranodon family and Tank Triceratops sing Tank's lullaby that his mom sings to him every night.
"I Learned a New Way to Improvise"  Shiny sings in a concert with Buddy, Tiny, Don, Cory Corythosaurus and Cory's cousin, Perry.
"Ecosystem"  Mr. Conductor sings about living in an ecosystem.
"Get into Nature Song"  Song about the Nature Trackers club and getting into nature.
"That's Not a Dinosaur"  Tiny sings about how not every animal in the Mesozoic Era is a dinosaur.
"No Place Like Our Nest"  The Pteranodon family sings about how their nest is the only home for them.
"The Biggest Dinosaurs"  Over several episodes the Pteranodon family go on trips to see the biggest sauropods, and this song is specific to those episodes.


== Episodes ==


== Live show ==
A live show, "Jim Henson's Dinosaur Train Live: Buddy's Big Adventure", began touring the United States and Canada in October 2013 and ended in March 2014.


== References ==