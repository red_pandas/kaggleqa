Projectile motion is a form of motion in which an object or particle (called a projectile) is thrown near the earth's surface, and it moves along a curved path under the action of gravity only. The only force of significance that acts on the object is gravity, which acts downward to cause a downward acceleration. Because of the object's inertia, no external horizontal force is needed to maintain the horizontal motion.


== The initial velocity ==
Let the projectile be launched with an initial velocity , which can be expressed as the sum of horizontal and vertical components as follows:
.
The components  and  can be found if the angle  is known:
,
.
If the projectile's range (R), launch angle ( or ), and drop height (h) are known, launch speed can be found using Newton's formula:
.


== Kinematic quantities of projectile motion ==
In projectile motion, the horizontal motion and the vertical motion are independent of each other; that is, neither motion affects the other. This is the principle of compound motion established by Galileo in 1638.


=== Acceleration ===
Since there is only acceleration in the vertical direction, the velocity in the horizontal direction is constant, being equal to . The vertical motion of the projectile is the motion of a particle during its free fall. Here the acceleration is constant, being equal to . The components of the acceleration are:
,
.


=== Velocity ===
The horizontal component of the velocity of the object remains unchanged throughout the motion. The downward vertical component of the velocity increases linearly, because the acceleration due to gravity is constant. The accelerations in the  and  directions can be integrated to solve for the components of velocity at any time , as follows:
,
.
The magnitude of the velocity (under the Pythagorean theorem, also known as the triangle law):
.


=== Displacement ===

At any time , the projectile's horizontal and vertical displacement are:
,
.
The magnitude of the displacement is:
.


== Parabolic equation ==

Consider the equations,
,
.
If t is eliminated between these two equations the following equation is obtained:
,
This equation is the equation of a parabola. Since , , and  are constants, the above equation is of the form
,
in which  and  are constants. This is the equation of a parabola, so the path is parabolic. The axis of the parabola is vertical.


== Time of flight or total time of the whole journey ==
The total time  for which the projectile remains in the air is called the time of flight.

After the flight, the projectile returns to the horizontal axis, so y=0

Note that we have neglected air resistance on the projectile.


== Maximum height of projectile ==

The greatest height that the object will reach is known as the peak of the object's motion. The increase in height will last until , that is,
.
Time to reach the maximum height:
.
From the vertical displacement of the maximum height of projectile:

 .


== Relation between horizontal range and maximum height ==
The relation between the range  on the horizontal plane and the maximum height  reached at  is:


=== Proof ===

  


== Maximum distance of projectile ==

It is important to note that the range and the maximum height of the projectile does not depend upon its mass. Hence range and maximum height are equal for all bodies that are thrown with the same velocity and direction.
The horizontal range d of the projectile is the horizontal distance it has travelled when it returns to its initial height (y = 0).
.
Time to reach ground:
.
From the horizontal displacement the maximum distance of projectile:
,
so
.
Note that  has its maximum value when
,
which necessarily corresponds to
,
or
.


== Application of the work energy theorem ==
According to the work-energy theorem the vertical component of velocity is:
.


== Projectile motion in art ==
The sixth panel of Hwaseonghaenghaengdo Byeongpun ( ) describes King Shooting Arrows at Deukjung Pavilion, 1795-02-14. According to palace records, Lady Hyegyeong, the King's mother, was so pleased to be presented with this 8-panel screen of such magnificent scale and stunning precision that she rewarded each of the seven artists who participated in its production. The artists were Choe Deuk-hyeon, Kim Deuk-sin, Yi Myeong-gyu, Jang Han-jong (1768 - 1815), Yun Seok-keun, Heo Sik (1762 - ?) and Yi In-mun.


== References ==
Bud goston: Ksrleti fizika I.,Budapest, Tanknyvkiad, 1986. ISBN 963 17 8772 9 (Hungarian)
Ifj. Ztonyi Sndor: Fizika 9.,Budapest, Nemzeti Tanknyvkiad, 2009. ISBN 978-963-19-6082-2 (Hungarian)
Hack Frigyes: Ngyjegy fggvnytblzatok, sszefggsek s adatok, Budapest, Nemzeti Tanknyvkiad, 2004. ISBN 963-19-3506-X (Hungarian)


== Notes ==

The equations in this article are approximations that hold when the projectile's velocity is small relative to the escape velocity. Faster-moving projectiles will follow a sub-orbital trajectory or go into orbit.


== External links ==
Open Source Physics computer model
A Java simulation of projectile motion, including first-order air resistance