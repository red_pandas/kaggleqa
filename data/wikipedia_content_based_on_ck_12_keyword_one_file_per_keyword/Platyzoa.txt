The Platyzoa /pltzo./ are a group of protostome animals proposed by Thomas Cavalier-Smith in 1998. Cavalier-Smith included in Platyzoa the Phylum Platyhelminthes or flatworms, and a new phylum, Acanthognatha, into which he gathered several previously described phyla of microscopic animals. Subsequent studies have supported Platyzoa as a clade, a monophyletic group of organisms with a common ancestor, while differing on the phyla included and on relationships within Platyzoa.


== Phyla ==

One current scheme places the following traditional phyla in Platyzoa:
Platyhelminthes
Gastrotricha
Gnathifera
Rotifera
Acanthocephala
Gnathostomulida
Micrognathozoa
Cycliophora


== Characteristics ==
None of the Platyzoa groups have a respiration or circulation system because of their small size, flat body or parasitic lifestyle. The Platyhelminthes and Gastrotricha are acoelomate. The other phyla have a pseudocoel, and share characteristics such as the structure of their jaws and pharynx, although these have been secondarily lost in the parasitic Acanthocephala. They form a monophyletic subgroup called the Gnathifera.
The name "Platyzoa" is used because most members are flat, though rotifers are not.


== Classification ==
The Platyzoa are close relatives of the Lophotrochozoa, and are sometimes included in that group. Together the two make up the Spiralia.


== References ==
^ Passamaneck Y, Halanych KM (July 2006). "Lophotrochozoan phylogeny assessed with LSU and SSU data: evidence of lophophorate polyphyly". Mol. Phylogenet. Evol. 40 (1): 208. doi:10.1016/j.ympev.2006.02.001. PMID 16556507. 
^ "Explanations.html". Retrieved 2009-06-28. 
The Taxonomicon - Taxon: Infrakingdom Platyzoa Cavalier-Smith, 1998 - retrieved January 31, 2006
Triploblastic Relationships with Emphasis on the Acoelomates and the Position of Gnathostomulida, Cycliophora, Plathelminthes, and Chaetognatha: A Combined Approach of 18S rDNA Sequences and Morphology - retrieved January 31, 2006
Myzostomida Are Not Annelids: Molecular and Morphological Support for a Clade of Animals with Anterior Sperm Flagella - retrieved January 31, 2006
Current advances in the phylogenetic reconstruction of metazoan evolution. A new paradigm for the Cambrian explosion? - retrieved January 31, 2006