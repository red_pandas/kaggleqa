The Center for Advanced Biotechnology and Medicine (CABM) is located on Busch Campus in Piscataway, New Jersey. It was established in 1985 to advance knowledge in the life sciences for the improvement of human health. It is administered by Rutgers, The State University of New Jersey. The building was completed in 1990, and has 100,000 square feet (10,000 m2) of lab and office space.


== Official Website ==
http://www3.cabm.rutgers.edu/home.php