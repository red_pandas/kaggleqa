Hydrometallurgy is a method for obtaining metals from their ores. It is a technique within the field of extractive metallurgy involving the use of aqueous chemistry for the recovery of metals from ores, concentrates, and recycled or residual materials.  Metal chemical processing techniques that complement hydrometallurgy are pyrometallurgy, vapour metallurgy and molten salt electrometallurgy. Hydrometallurgy is typically divided into three general areas:
Leaching
Solution concentration and purification
Metal or metal compound recovery


== Leaching ==
Leaching involves the use of aqueous solutions to extract metal from metal bearing materials which is brought into contact with a material containing a valuable metal. The lixiviant solution conditions vary in terms of pH, oxidation-reduction potential, presence of chelating agents and temperature, to optimize the rate, extent and selectivity of dissolution of the desired metal component into the aqueous phase. Through the use of chelating agents, one can selectively extract certain metals. Such chelating agents are typically amines of schiff bases.
The five basic leaching reactor configurations are in-situ, heap, vat, tank and autoclave.


=== In-situ leaching ===
In-situ leaching is also called "solution mining." The process initially involves drilling of holes into the ore deposit. Explosives or hydraulic fracturing are used to create open pathways within the deposit for solution to penetrate into. Leaching solution is pumped into the deposit where it makes contact with the ore. The solution is then collected and processed. The Beverley uranium deposit is an example of in-situ leaching.


=== Heap leaching ===
In heap leaching processes, crushed (and sometimes agglomerated) ore is piled in a heap which is lined with an impervious layer. Leach solution is sprayed over the top of the heap, and allowed to percolate downward through the heap. The heap design usually incorporates collection sumps, which allow the "pregnant" leach solution (i.e. solution with dissolved valuable metals) to be pumped for further processing. An example is gold cyanidation, where pulverized ores are extracted with a solution of sodium cyanide, which, in the presence of air, dissolves the gold, leaving behind the nonprecious residue.


=== Vat leaching ===
Vat leaching, also called agitation leaching, involves contacting material, which has usually undergone size reduction and classification, with leach solution in large vats.


=== Tank leaching ===
Stirred tank, also called agitation leaching, involves contacting material, which has usually undergone size reduction and classification, with leach solution in agitated tanks. The agitation can enhance reaction kinetics by enhancing mass transfer. Tanks are often configured as reactors in series.


=== Autoclave leaching ===
Autoclave reactors are used for reactions at higher temperatures, which can enhance the rate of the reaction. Similarly, autoclaved enable the use gaseous reagents in the system.


== Solution concentration and purification ==
After leaching, the leach liquor must normally undergo concentration of the metal ions that are to be recovered. Additionally, undesirable metal ions sometimes require removal.
Precipitation is the selective removal of a compound of the targeted metal or removal of a major impurity by precipitation of one of its compounds. Copper is precipitated as its sulfide as a means to purify nickel leachates.
Cementation is the conversion of the metal ion to the metal by a redox reaction. A typical application involves addition of scrap iron to a solution of copper ions. Iron dissolves and copper metal is deposited.
Solvent Extraction
Ion Exchange
Gas reduction. Treating a solution of nickel and ammonia with hydrogen affords nickel metal as its powder.
Electrowinning is a particularly selective if expensive electrolysis process applied to the isolation of precious metals. Gold can be electroplated from its solutions.


=== Solvent extraction ===
In the solvent extraction is a mixture of an extractant in a diluent is used to extract a metal from one phase to another. In solvent extraction this mixture is often referred to as the "organic" because the main constituent (diluent) is some type of oil.
The PLS (pregnant leach solution) is mixed to emulsification with the stripped organic and allowed to separate. The metal will be exchanged from the PLS to the organic they are modified. The resulting streams will be a loaded organic and a raffinate. When dealing with electrowinning, the loaded organic is then mixed to emulsification with a lean electrolyte and allowed to separate. The metal will be exchanged from the organic to the electrolyte. The resulting streams will be a stripped organic and a rich electrolyte. The organic stream is recycled through the solvent extraction process while the aqueous streams cycle through leaching and electrowinning processes respectively.


=== Ion exchange ===
Chelating agents, natural zeolite, activated carbon, resins, and liquid organics impregnated with chelating agents are all used to exchange cations or anions with the solution. Selectivity and recovery are a function of the reagents used and the contaminants present.


== Metal Recovery ==
Metal recovery is the final step in a hydrometallurgical process. Metals suitable for sale as raw materials are often directly produced in the metal recovery step. Sometimes, however, further refining is required if ultra-high purity metals are to be produced. The primary types of metal recovery processes are electrolysis, gaseous reduction, and precipitation. For example, a major target of hydrometallurgy is copper, which is conveniently obtained by electrolysis. Cu2+ ions reduce at mild potentials, leaving behind other contaminating metals such as Fe2+ and Zn2+.


=== Electrolysis ===
Electrowinning and electrorefining respectively involve the recovery and purification of metals using electrodeposition of metals at the cathode, and either metal dissolution or a competing oxidation reaction at the anode.


=== Precipitation ===
Precipitation in hydrometallurgy involves the chemical precipitation of either metals and their compounds or of the contaminants from aqueous solutions. Precipitation will proceed when, through reagent addition, evaporation, pH change or temperature manipulation, any given species exceeds its limit of solubility.


== References ==


== External links ==
Hydrometallurgy, BioMineWiki