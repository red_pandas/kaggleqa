The Eupelycosauria originally referred to a suborder of 'pelycosaurs' (Reisz 1987), but has been redefined (Laurin and Reisz 1997) to designate a clade of synapsids that includes most pelycosaurs, as well as all therapsids and mammals. They first appear during the Early Pennsylvanian epoch (i.e.: Archaeothyris, and perhaps an even earlier genus, Protoclepsydrops), and represent just one of the many stages in the acquiring of mammal-like characteristics (Kemp 1982), in contrast to their earlier amniote ancestors. The defining characteristics which separate these animals from the Caseasauria (also pelycosaurs) are based on details of proportion of certain bones of the skull. These include a long, narrow supratemporal bone (in contrast to caseasaurs where this bone is almost as wide as it is long), and a frontal bone with a wider connection to the upper margin of the orbit (Laurin and Reisz 1997).


== Evolution ==
Many non-therapsid eupelycosaurs were the dominant land animals from the latest Carboniferous to the end of the early Permian epoch. Ophiacodontids were common from their appearance in the late Carboniferous (Pennsylvanian) to the early Permian, but they became progressively smaller as the early Permian progressed. The edaphosaurids, along with the caseids, were the dominant herbivores in the early part of the Permian, ranging from the size of a pig to the size of a rhinoceros. The most renowned edaphosaurid is Edaphosaurus, a large [1012-foot-long (3.03.7 m)] herbivore which had a sail on its back, probably used for thermoregulation and mating. Sphenacodontids, a family of carnivorous eupelycosaurs, included the famous Dimetrodon, which is sometimes mistaken for a dinosaur, and was the largest predator of the period. Like Edaphosaurus, Dimetrodon also had a distinctive sail on its back, and it probably served the same purpose - regulating heat. The varanopid family passingly resembled today's monitor lizards and may have had the same lifestyle.
Therapsids descended from a clade closely related to the sphenacodontids. They became the succeeding dominant land animals for the rest of the Permian, and in the latter part of the Triassic, therapsids gave rise to the first true mammals. All non-therapsid pelycosaurs, as well as many other life forms, became extinct at the end of Permian period.


== Classification ==

The following cladogram follows the one found on Mikko's Phylogeny Archive.


== References ==
Kemp. T.S., 1982, Mammal-like Reptiles and the Origin of Mammals. Academic Press, New York
Laurin, M. and Reisz, R. R., 1997, Autapomorphies of the main clades of synapsids - Tree of Life Web Project
Reisz, R. R., 1986, Handbuch der Paloherpetologie  Encyclopedia of Paleoherpetology, Part 17A Pelycosauria Verlag Dr. Friedrich Pfeil, ISBN 3-89937-032-5
^ Paleos Synapsida


== External links ==
Eupelycosauria