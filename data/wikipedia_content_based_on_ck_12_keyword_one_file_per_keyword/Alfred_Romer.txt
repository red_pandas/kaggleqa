Alfred Sherwood Romer (December 28, 1894  November 5, 1973) was an American paleontologist and biologist and a specialist in vertebrate evolution.


== Biography ==
Alfred Romer was born in White Plains, New York, and studied at Amherst College achieving a Bachelor of Science Honours degree in Biology and Columbia University pursuing with a M.Sc in Biology and graduating with a doctorate in zoology in 1921. Romer joined the department of geology and paleontology at the University of Chicago as an associate professor in 1923. He was an active researcher and teacher. His collecting program added important Paleozoic specimens to the Walker Museum of Paleontology. In 1934 he was appointed professor of biology at Harvard University. In 1946, he also became director of the Harvard Museum of Comparative Zoology. In 1954 Romer was awarded the Mary Clark Thompson Medal from the National Academy of Sciences. He was awarded the Academy's Daniel Giraud Elliot Medal in 1956.


== Evolutionary research ==
Romer was very keen in investigating vertebrate evolution. Comparing facts from paleontology, comparative anatomy, and embryology, he taught the basic structural and functional changes that happened during the evolution of fishes to primitive terrestrial vertebrates and from these to all other tetrapods. He always emphasized the evolutionary significance of the relationship between the form and function of animals and the environment.
Through his textbook Vertebrate Paleontology Romer laid the foundation for the traditional classification of vertebrates. He drew together the (then) widely scattered taxonomy of the different vertebrate groups and combined them in a simplified scheme, emphasizing orderliness and overview. Based on his research of early amphibians, he reorganised the labyrinthodontians. Romer's classification was followed by many subsequent authors, notably Robert L. Carroll, and is still in use.


== Namesakes ==
A genus of early captorhinids is named Romeria after Romer, as is Romeriida (the name for a clade that contains the diapsids and their closest relatives). In July 2007 a species of non-dinosaurian dinosauromorph was named Dromomeron romeri, "the first part meaning 'running femur,' the latter in honor of paleontologist Alfred Sherwood Romer, a key figure in evolution research". The finding of these fossils was hailed as a breakthrough proving dinosaurs and other dinosauromorphs "lived together for as long as 15 million to 20 million years." The early Pennsylvanian (Late Carboniferous) Romeriscus was also named after Romer. It was initially described as the oldest known amniote, but this is because limnoscelids were, at that time, considered amniotes by some authors. A subsequent study showed that the fossil lacks diagnostic characters and can only be assigned to Tetrapoda.
Romer was the first to recognise the gap in the fossil record between the tetrapods of the Devonian and the later Carboniferous period, a gap that has borne the name Romer's gap since 1995.


== Books ==
Romer, A.S. 1933. Vertebrate Paleontology. University of Chicago Press, Chicago. (2nd ed. 1945; 3rd ed. 1966)
Romer, A.S. 1933. Man and the Vertebrates. University of Chicago Press, Chicago. (2nd ed. 1937; 3rd ed. 1941; 4th ed., retitled The Vertebrate Story, 1949)
Romer, A.S. 1949. The Vertebrate Body. W.B. Saunders, Philadelphia. (2nd ed. 1955; 3rd ed. 1962; 4th ed. 1970)
Romer, A.S. 1949. The Vertebrate Story. University of Chicago Press, Chicago. (4th ed. of Man and the Vertebrates)
Romer, A.S. 1956. Osteology of the Reptiles. University of Chicago Press, Chicago.
Romer, A.S. 1968. Notes and Comments on Vertebrate Paleontology. University of Chicago Press, Chicago.
Romer, A.S. & T.S. Parsons. 1977. The Vertebrate Body. 5th ed. Saunders, Philadelphia. (6th ed. 1985)


== Sources ==


== External links ==
http://people.wku.edu/charles.smith/chronob/ROME1894.htm
Alfred Romer  Biographical Memoirs of the National Academy of Sciences