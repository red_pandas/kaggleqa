Five Equations That Changed the World: The Power and Poetry of Mathematics is a book by Michael Guillen, published in 1995. It is divided into five chapters that talk about five different equations in physics and the people who have developed them. The scientists and their equations are:
Isaac Newton  (Universal Law of Gravity)
Daniel Bernoulli (Law of Hydrodynamic Pressure)
Michael Faraday (Law of Electromagnetic Induction)
Rudolf Clausius (Second Law of Thermodynamics)
Albert Einstein (Theory of Special Relativity)
The book is a light study in science and history, portraying the preludes to and times and settings of discoveries that have been the basis of further development, including space travel, flight and nuclear power. Each chapter of the book is divided into sections titled Veni, Vidi, Vici.
The reviews of the book have been mixed. Publishers Weekly called it "wholly accessible, beautifully written", Kirkus Reviews wrote that it is a "crowd-pleasing kind of book designed to make the science as palatable as possible", and Frank Mahnke wrote that Guillen "has a nice touch for the history of mathematics and physics and their impact on the world". However, in contrast, Charles Stephens panned "the superficiality of the author's treatment of scientific ideas", and the editors of The Capital Times called the book a "miserable failure" at its goal of helping the public appreciate the beauty of mathematics.


== References ==
^ "Five Equations That Changed the World", Kirkus Reviews, September 15, 1995 
^ Mahncke, Frank C. (April 1, 1997), "Five Equations That Changed the World", Naval War College Review 
^ Stephens, Charles F. (March 1996), "Skimming the depths", IEEE Spectrum 33 (3), doi:10.1109/MSPEC.1996.485767 
"Nonfiction book review: Five Equations That Changed the World", Publishers Weekly, September 4, 1995 
McWilliams, Brendan (October 24, 1995), "The elation of the equation", Irish Times 
"Math's beauty hard to show in words", The Capital Times, March 15, 1996