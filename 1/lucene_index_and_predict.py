import lucene
from lupyne import engine
import os
import os.path as osp
import time
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from textblob import TextBlob
import sys

CURRENT_FILE_PATH = osp.dirname(osp.realpath(__file__))

DATA_PATH = osp.join(CURRENT_FILE_PATH, '../data')
DOCS_DIR = osp.join(DATA_PATH, 'new_lucene_index')
VAL_SET_PATH = osp.join(DATA_PATH, 'validation_set.tsv')
TRAIN_SET_PATH = osp.join(DATA_PATH, 'training_set.tsv')
ck_keywords_path = 'ck12_keywords.txt'
stemmer = SnowballStemmer('english')

def prepare_index(DOCS_DIR, stemmer):
    # Store the index in memory:
    indexer = engine.Indexer()  # Indexer combines Writer and Searcher; RAMDirectory and StandardAnalyzer are defaults
    indexer.set('contents', stored=True)  # settings for all documents of indexer; indexed and tokenized is the default
    indexer.set('path', stored=True)

    index_start = time.clock()
    indexed_documents_count = 0
    for root, dirs, files in os.walk(DOCS_DIR):
        for f in files:
            f_path = osp.join(root, f)
            with open(f_path, 'r') as doc:
                content = doc.readlines()
                # content = ' '.join([x.strip() for x in content])  # concatenate whole file into one string
                for article in content:
                    if len(article.split()) >= 5:
                        article = stem_sentence_and_remove_stopwords(article)
                        indexer.add(path=f_path, contents=article)  # add document
                        indexed_documents_count += 1
    indexer.commit()  # commit changes and refresh searcher
    print 'Added', indexed_documents_count, 'documents to index, spent', time.clock() - index_start, 'sec'
    return indexer


def stem_sentence_and_remove_stopwords(sentence):
    result = ""
    sentence = ''.join(ch for ch in sentence if ch.isalnum() or ch.isspace() or ch == '.,-')
    for x in sentence.split():
        x = x.lower()
        if x not in stop:
            x = stemmer.stem(x)
            result += x + " "
    return result


def removeDuplicateWords(q):
    q = q.lower()
    s = q.split(' ')
    r = []
    for w in s:
        if w not in r:
            r.append(w)
    return ' '.join(r)

def remove_stopword(sentence):
    sentence_without_stop = ""
    for x in sentence.split():
        if x not in stop:
            sentence_without_stop += x + " "
    return  sentence_without_stop.strip()

def stem_sentence_and_remove_stopwords_boost(sentence):
    result = ""
    sentence = unicode(sentence, errors='ignore')
    blob = TextBlob(sentence)
    keywords = blob.noun_phrases
    sentence = ''.join(ch for ch in sentence if ch.isalnum() or ch.isspace() or ch == '.,-')
    sentence = sentence.lower()
    sentence = removeDuplicateWords(sentence)
    sentence = remove_stopword(sentence)
    setOfKeywords = set(keywords)
    allkeywords = set()
    for keyword in setOfKeywords:
        for word in keyword.split():
            allkeywords.add(word)
    if len(sentence.split()) <= 2 and len(allkeywords) == 0:
        for x in sentence.split():
            x = x.lower()
            if x not in stop:
                x = stemmer.stem(x)
                result += x + " "
        result = "\"" + result + "\"^1.4"
        return  result
    for word_to_replace in sentence.split():
        found=False
        for keyword in allkeywords:
            for word in keyword.split():
                if word_to_replace==word:
                    result = result + " " + stemmer.stem(word_to_replace) + "^1.4"
                    found = True
                    break
        if not found:
            result = result + " " + stemmer.stem(word_to_replace)
    return result

def stem_sentence_and_remove_stopwords_ck12_keywords(sentence, is_question):
    result = ""
    sentence = ''.join(ch for ch in sentence if ch.isalnum() or ch.isspace() or ch == '.,-')
    sentence = sentence.lower()
    sentence = removeDuplicateWords(sentence)
    for x in sentence.split():
        x = x.lower()
        if x not in stop:
            x = stemmer.stem(x)
            result += x + " "
    keywords=[]
    for keyword in ck_keywords:
        if len(keyword.split())>1 and keyword in result:
            keywords.append(keyword)
            break
        else:
            if keyword in result.split():
                keywords.append(keyword)
                break
    keywords_duplicate =[]
    for keyword in keywords:
        for keyword_comp in keywords:
            if keyword != keyword_comp and keyword in keyword_comp:
                keywords_duplicate.append(keyword)
                break
    for keyword in keywords_duplicate:
        keywords.remove(keyword)
    for keyword in keywords:
        if is_question:
            for word in keyword.split():
                result = result.replace(word, "\""+word+"\"^1.5")
        else:
            for word in keyword.split():
                result = result.replace(word, "\""+word+"\"^1.3")
    return result

def prepare_sentence(sentence,is_question):
    # remove all non alphanum or space characters (lucene doesn't like braces)
    sentence = stem_sentence_and_remove_stopwords_ck12_keywords(sentence, is_question)
    return sentence



def predict_answer(indexer, question, answerA, answerB, answerC, answerD, verbose=True):
    question = prepare_sentence(question, True)
    answerA = prepare_sentence(answerA, False)
    answerB = prepare_sentence(answerB, False)
    answerC = prepare_sentence(answerC, False)
    answerD = prepare_sentence(answerD, False)

    queryA = question + ' ' + answerA
    queryB = question + ' ' + answerB
    queryC = question + ' ' + answerC
    queryD = question + ' ' + answerD

    if verbose:
        print 'Question:', question
        print 'A:', answerA
        print 'B:', answerB
        print 'C:', answerC
        print 'D:', answerD

    # Now search the index:
    # hitsQ = indexer.search(question, count=10, scores=True, field='contents')  # parsing handled if necessary
    hitsA = indexer.search(queryA, count=10, scores=True, field='contents')  # parsing handled if necessary
    hitsB = indexer.search(queryB, count=10, scores=True, field='contents')  # parsing handled if necessary
    hitsC = indexer.search(queryC, count=10, scores=True, field='contents')  # parsing handled if necessary
    hitsD = indexer.search(queryD, count=10, scores=True, field='contents')  # parsing handled if necessary

    if verbose:
        print 'Q'
        # if hitsQ:
        #   print hitsQ[0]['path']
        #  print hitsQ[0].score
        print 'A'
        if hitsA:
            print hitsA[0]['path']
            print hitsA[0].score
        if hitsB:
            print 'B'
            print hitsB[0]['path']
            print hitsB[0].score
        if hitsC:
            print 'C'
            print hitsC[0]['path']
            print hitsC[0].score
        if hitsD:
            print 'D'
            print hitsD[0]['path']
            print hitsD[0].score


            # scoreA = get_score_from_query(hitsA, answerA)
            # scoreB = get_score_from_query(hitsB, answerB)
            # scoreC = get_score_from_query(hitsC, answerA)
            # scoreD = get_score_from_query(hitsD, queryD)

            # if hitsQ:
            #  pathQ = hitsQ[0]['path']
            # ansQ, scoreQ = get_answer_from_question_page(pathQ, queryA, queryB, queryC, queryD )
            # else:
        scoreQA = 0
        scoreQB = 0
        scoreQC = 0
        scoreQD = 0

    scoreA = get_score(hitsA)
    scoreB = get_score(hitsB)
    scoreC = get_score(hitsC)
    scoreD = get_score(hitsD)
    max_score = max(scoreA, scoreB, scoreC, scoreD)
    if max_score == scoreA:
        ans = 'A'
    elif max_score == scoreB:
        ans = 'B'
    elif max_score == scoreC:
        ans = 'C'
    else:
        ans = 'D'
    # if max_score > scoreQ:
    #    return ans
    # else:
    #    return ansQ
    return ans, scoreA, scoreB, scoreC, scoreD


def get_score(hits):
    if hits:
        return hits[0].score
    else:
        return 0


def get_score_from_query(hits, query):
    if hits:
        path = hits[0]['path']
        return get_answer_from_answer_page(path, query)
    else:
        return 0


def get_answer_from_question_page(pathQ, queryA, queryB, queryC, queryD):
    indexerQ = engine.Indexer()  # Indexer combines Writer and Searcher; RAMDirectory and StandardAnalyzer are defaults
    indexerQ.set('contents', stored=True)  # settings for all documents of indexer; indexed and tokenized is the default
    indexerQ.set('path', stored=True)
    with open(pathQ, 'r') as doc:
        content = doc.readlines()
        # content = ' '.join([x.strip() for x in content])  # concatenate whole file into one string
        # content = stem_sentence_and_remove_stopwords(content)
        for x in content:
            indexer.add(path=pathQ, contents=x)  # add document
    hitsA = indexer.search(queryA, count=10, scores=True, field='contents')  # parsing handled if necessary
    hitsB = indexer.search(queryB, count=10, scores=True, field='contents')  # parsing handled if necessary
    hitsC = indexer.search(queryC, count=10, scores=True, field='contents')  # parsing handled if necessary
    hitsD = indexer.search(queryD, count=10, scores=True, field='contents')  # parsing handled if necessary

    max_score = max(hitsA[0].score, hitsB[0].score, hitsC[0].score, hitsD[0].score)
    if max_score == hitsA[0].score:
        ans = 'A'
    elif max_score == hitsB[0].score:
        ans = 'B'
    elif max_score == hitsC[0].score:
        ans = 'C'
    else:
        ans = 'D'
    return ans, max_score


def get_answer_from_answer_page(path, query):
    indexerQ = engine.Indexer()  # Indexer combines Writer and Searcher; RAMDirectory and StandardAnalyzer are defaults
    indexerQ.set('contents', stored=True)  # settings for all documents of indexer; indexed and tokenized is the default
    indexerQ.set('path', stored=True)
    with open(path, 'r') as doc:
        content = doc.readlines()
        # content = ' '.join([x.strip() for x in content])  # concatenate whole file into one string
        # content = stem_sentence_and_remove_stopwords(content)
        for x in content:
            indexer.add(path=path, contents=x)  # add document
    hits = indexer.search(query, count=10, scores=True, field='contents')  # parsing handled if necessary
    return hits[0].score


def get_accuracy_on_train_set(indexer):
    true_answers = []
    predicted_answers = []
    with open('lucene_scores.csv', 'w') as lucene_scores:
        lucene_scores.write('id,lucene_score_A,lucene_score_B,lucene_score_C,lucene_score_D\n')
        with open(TRAIN_SET_PATH, 'r') as train_file:
            train_questions = [x.strip() for x in train_file.readlines()]
            with open('resultTrain.csv', 'w') as result_file:
                for s in train_questions[1:]:
                    id, question, right_answer, answerA, answerB, answerC, answerD = s.split('\t')
                    true_answers.append(right_answer)
                    ans, scoreA, scoreB, scoreC, scoreD = predict_answer(indexer, question, answerA, answerB, answerC,
                                                                         answerD)
                    if ans != right_answer:
                        result_file.write(" ".join(
                            [question, "\n", right_answer, ans, "\n", answerA, str(scoreA), "\n", answerB, str(scoreB), "\n",
                             answerC, str(scoreC), "\n", answerD, str(scoreD)]) + "\n")
                    lucene_scores.write('{0},{1},{2},{3},{4}\n'.format(id, scoreA, scoreB, scoreC, scoreD))
                    predicted_answers.append(ans)

    count_of_right_answers = 0
    for i in xrange(0, len(true_answers)):
        if predicted_answers[i] == true_answers[i]:
            count_of_right_answers += 1
    acc = float(count_of_right_answers) / float(len(true_answers))
    return acc


def get_submission_from_val(indexer):
    with open('val_lucene_scores.csv', 'w') as lucene_scores:
        lucene_scores.write('id,lucene_score_A,lucene_score_B,lucene_score_C,lucene_score_D\n')
        with open('result.csv', 'w') as result_file:
            result_file.write('id,correctAnswer\n')
            with open(VAL_SET_PATH, 'r') as val_file:
                val_questions = [x.strip() for x in val_file.readlines()]

                for s in val_questions[1:]:
                    id, question, answerA, answerB, answerC, answerD = s.split('\t')

                    ans, scoreA, scoreB, scoreC, scoreD = predict_answer(indexer, question, answerA, answerB, answerC,
                                                                         answerD)
                    lucene_scores.write('{0},{1},{2},{3},{4}\n'.format(id, scoreA, scoreB, scoreC, scoreD))
                    result_file.write(id + ',' + ans + '\n')


if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding('utf8')
    stop = stopwords.words('english')
    ck_keywords = []
    with open(ck_keywords_path, 'r') as ck_file:
        ck_keywords = [stemmer.stem(remove_stopword(x.strip())) for x in ck_file.readlines()]
    ck_keywords = set(ck_keywords)
    keywords_duplicate =[]
    for keyword in ck_keywords:
        if len(keyword) ==0:
            keywords_duplicate.append(keyword)
    for keyword in keywords_duplicate:
         ck_keywords.remove(keyword)
    lucene.initVM()

    indexer = prepare_index(DOCS_DIR, stemmer)
    get_submission_from_val(indexer)
    print 'Accuracy on train set:', get_accuracy_on_train_set(indexer)
