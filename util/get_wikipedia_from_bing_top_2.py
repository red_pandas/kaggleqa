import wikipedia
import os
from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool
import util
import json

pool = ThreadPool(8)

LINKS_FILE = '/home/sokolov/KaggleAllenAI/data/output.json'
OUTPUT_DIR = '/home/sokolov/KaggleAllenAI/data/lucene_index/bing_wikipedia_top_2'

if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)

parsed_json = json.load(open(LINKS_FILE, 'r'))

wiki_queries = []
for bing_query in parsed_json:
    count = 0
    for res in bing_query['results']:
        title = res['title']
        wiki_queries.append(title.split(' - ')[0])
        count += 1
        if count == 2:
            break

print 'files to donwload', len(wiki_queries)

def save_wikipedia_page_content(query):
    try:
        content = wikipedia.page(query).content.encode('ascii', 'ignore')

        with open(os.path.join(OUTPUT_DIR, query), 'w') as res_file:
            for line in content.split('\n'):
                line = ' '.join(map(util.norm_word, line.split()))
                if line:
                    res_file.write(line + '\n')
        print 'done', query
    except Exception as e:
        print e


#save_wikipedia_page_content(wiki_queries[0])
results = pool.map(save_wikipedia_page_content, wiki_queries)

pool.close()
pool.join()
