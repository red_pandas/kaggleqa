#!/usr/bin/env python

import sys
import os
import os.path
import codecs

QA_FILE_PATH = "training_set.tsv"
RESULT_FILE_PATH = "training_set_statement.tsv"


def removeDuplicateWords(q):
    q = q.lower()
    s = q.split(' ')
    r = []
    for w in s:
        if w not in r:
            r.append(w)
    return ' '.join(r)


def getStatementForOneSentQ(q, a):
    # default, just concat and remove '?'
    q = q.replace('?', ' ').replace(".", " ").replace(",", " ").replace("most likely", "")
    default = q + " " + a

    if q.find("__________") == 0:
        return q.replace("__________", a).replace('?', ' ')

    if q.find("__________?") == 0:
        return q.replace("__________?", a).replace('?', ' ')

    # sligthly wrong grammar
    if q.find("In which way are") == 0:
        return q.replace("In which way are", '').replace('?', ' ') + a
    if q.find("In which way is") == 0:
        return q.replace("In which way is", '').replace('?', ' ') + a
    if q.find("How should") == 0:
        return q.replace("How should", '').replace('?', ' ') + a
    if q.find("How would") == 0:
        return q.replace("How would", '').replace('?', ' ') + a
    if q.find("how would") == 0:
        return q.replace("how would", '').replace('?', ' ') + a
    if q.find("Where would") == 0:
        return q.replace("Where would", '').replace('?', ' ') + a
    if q.find("When would") == 0:
        return q.replace("When would", '').replace('?', ' ') + a
    if q.find("How could") == 0:
        return q.replace("How could", '').replace('?', ' ') + a
    if q.find("How are") == 0:
        return q.replace("How are", '').replace('?', ' ') + a
    if q.find("How is") == 0:
        return q.replace("How is", '').replace('?', ' ') + a
    if q.find("How did") == 0:
        return q.replace("How did", '').replace('?', ' ') + a
    if q.find("How does") == 0:
        return q.replace("How does", '').replace('?', 'by ') + a
    if q.find("How do") == 0:
        return q.replace("How do", '').replace('?', 'by ') + a
    if q.find("How can") == 0:
        return q.replace("How can", '').replace('?', ' ') + a
    if q.find("How will") == 0:
        return q.replace("How will", '').replace('?', ' ') + a
    if q.find("How has") == 0:
        return q.replace("How has", '').replace('?', ' ') + a
    if q.find("How many") == 0:
        return q.replace("How many", '').replace('?', ' ') + a
    if q.find("How might") == 0:
        return q.replace("How might", '').replace('?', ' ') + a
    if q.find("Why is") == 0:
        return q.replace("Why is", '').replace('?', ' ') + a
    if q.find("Which would") == 0:
        return q.replace("Which would", "").replace('?', ' ') + a
    if q.find("Which best explains") == 0:
        return q.replace("Which best explains", "").replace('?', ' ') + a
    if q.find("Which of these statements describes") == 0:
        return q.replace("Which of these statements describes", "").replace('?', ' ') + a
    if q.find("Which statement correctly identifies") == 0:
        return q.replace("Which statement correctly identifies", "").replace('?', ' ') + a
    if q.find("Which statement") == 0:
        return q.replace("Which statement", '').replace('?', ' ') + a
    if q.find("Who is") == 0:
        return q.replace("Why is", a).replace('?', ' ')
    if q.find("When is") == 0:
        return q.replace("When is", "").replace('?', ' ') + a
    if q.find("When are") == 0:
        return q.replace("When are", "").replace('?', ' ') + a
    if q.find("How was") == 0:
        return q.replace("How was", "").replace('?', ' ') + a
    if q.find("Why are") == 0:
        return q.replace("Why are", '').replace('?', ' ') + a
    if q.find("Where are") == 0:
        return q.replace("Where are", '').replace('?', ' ') + a
    if q.find("Why do") == 0:
        return q.replace("Why do", '').replace('?', ' ') + a
    if q.find("How long does it take for") == 0:
        return q.replace("How long does it take for", '').replace('?', ' ') + a

    if q.find("Which of the following statements best explains") == 0:
        return q.replace("Which of the following statements best explains", "").replace('?', ' ') + a
    if q.find("Which of the following statements best defines") == 0:
        return q.replace("Which of the following statements best defines", "").replace('?', ' ') + a
    if q.find("Which of the following statements about") == 0:
        return q.replace("Which of the following statements about", a).replace('?', ' ')
    if q.find("Which of these statements describes") == 0:
        return q.replace("Which of these statements describes", "").replace('?', ' ') + a
    if q.find('Which of these statements') == 0:
        return q.replace("Which of these statements", a).replace('?', ' ')
    if q.find("Which of the following statements") == 0:
        return q.replace("Which of the following statements", a).replace('?', ' ')
    if q.find("Which of these procedures") == 0:
        return q.replace("Which of these procedures", a).replace('?', ' ')
    if q.find("Which of the following choices") == 0:
        return q.replace("Which of the following choices", a).replace('?', ' ')
    if q.find("Which of the following") == 0:
        return q.replace("Which of the following", a).replace('?', ' ')
    if q.find("Which statement best describes") == 0:
        return q.replace("Which statement best describes", "").replace('?', ' ') + a
    if q.find("Which of these") == 0:
        return q.replace("Which of these", a).replace('?', ' ')
    if q.find("Which statement") == 0:
        return q.replace("Which statement", a).replace('?', ' ')
    if q.find("What type of") == 0:
        return q.replace("What type of", a).replace('?', ' ')
    if q.find("Which type of") == 0:
        return q.replace("Which type of", a).replace('?', ' ')

    if q.find("At what") == 0:
        return q.replace("At what", "At " + a).replace('?', ' ')
    if q.find("In what") == 0:
        return q.replace("In what", "In " + a).replace('?', ' ')
    if q.find("In which of these investigations") == 0:
        return q.replace("In which of these investigations", a).replace('?', ' ')
    if q.find("In which") == 0:
        return q.replace("In which", "In " + a).replace('?', ' ')

    # can be anythere
    if q.find("which of these statements") != -1:
        return q.replace("which of these statements", a).replace('?', ' ')
    if q.find("which of the following choices") != -1:
        return q.replace("which of the following choices", a).replace('?', ' ')
    if q.find("which of the following") != -1:
        return q.replace("which of the following", a).replace('?', ' ')
    if q.find("Which of the following") != -1:
        return q.replace("which of the following", a).replace('?', ' ')
    if q.find("what type of") != -1:
        return q.replace("what type of", a).replace('?', ' ')
    if q.find("which type of") != -1:
        return q.replace("which type of", a).replace('?', ' ')

    if q.find("which of these") != -1:
        return q.replace("which of these", a).replace('?', ' ')
    if q.find("Which of these") != -1:
        return q.replace("Which of these", a).replace('?', ' ')
    # again sligthly wrong grammar
    if q.find("Which term best describe") != -1:
        return q.replace("Which term best describe", '').replace('?', ' ') + a
    if q.find("Which conclusion can be drawn from this phenomenon") != -1:
        return q.replace("Which conclusion can be drawn from this phenomenon", '').replace('?', ' ') + a
    if q.find("Which best explains") != -1:
        return q.replace("Which best explains", '').replace('?', ' ') + a
    if q.find("Which statement correctly identifies") != -1:
        return q.replace("Which statement correctly identifies", '').replace('?', ' ') + a
    if q.find("What term best classifies") != -1:
        return q.replace("What term best classifies", '').replace('?', ' ') + a
    if q.find("Which statement best explains") != -1:
        return q.replace("Which statement best explains", '').replace('?', ' ') + a
    if q.find("Which statement best describes") != -1:
        return q.replace("Which statement best describes", '').replace('?', ' ') + a
    if q.find("What characteristic") != -1:
        return q.replace("What characteristic", '').replace('?', ' ') + a
    if q.find("Which statement explains") != -1:
        return q.replace("Which statement explains", '').replace('?', ' ') + a
    if q.find("Which statement describes") != -1:
        return q.replace("Which statement describes", '').replace('?', ' ') + a
    if q.find("why is") != -1:
        return q.replace("why is", '').replace('?', ' ') + a
    if q.find("Why is") != -1:
        return q.replace("Why is", '').replace('?', ' ') + a
    if q.find("Which is") != -1:
        return q.replace("Which is", '').replace('?', ' ') + a
    if q.find("What is") != -1:
        return q.replace("What is", '').replace('?', ' ') + a
    if q.find("What will") != -1:
        return q.replace("What will", '').replace('?', ' ') + a
    if q.find("Where is") != -1:
        return q.replace("Where is", '').replace('?', ' ') + a
    if q.find("why are") != -1:
        return q.replace("why are", '').replace('?', ' ') + a
    if q.find("why do") != -1:
        return q.replace("why do", '').replace('?', ' ') + a
    if q.find("how did") != -1:
        return q.replace("how did", '').replace('?', ' ') + a
    if q.find("how was") != -1:
        return q.replace("how was", '').replace('?', ' ') + a
    if q.find("Who was") != -1:
        return q.replace("Who was", a).replace('?', ' ')
    if q.find("how does") != -1:
        return q.replace("how does", '').replace('?', 'by ') + a
    if q.find("how do") != -1:
        return q.replace("how do", '').replace('?', 'by ') + a
    if q.find("When do") != -1:
        return q.replace("When do", '').replace('?', 'by ') + a
    if q.find("how many") != -1:
        return q.replace("how many", '').replace('?', 'by ') + a

    if q.find("What") == 0:
        return q.replace("What", a).replace('?', '')
    if q.find("Where") == 0:
        return q.replace("Where", "").replace('?', ' ') + a
    if q.find("Which") == 0:
        return q.replace("Which", a).replace('?', '')
    if q.find("Why") == 0:
        return q.replace("Why", "").replace('?', ' ') + a

    if q.find("which") != -1 and q.find("which", q.find("which") + 1) == -1:
        return q.replace("which", a).replace('?', ' ')
    if q.find("what") != -1 and q.find("what", q.find("what") + 1) == -1:
        return q.replace("what", a).replace('?', ' ')

    print "Can't find match for question", q
    return default


def setFinalDot(q):
    if q[-1] == ',':
        q = q[:-1] + '.'
    return q;


def getStatement(q, a):
    q = q.replace('."', '".')
    a = a.replace('.', ' ').replace('\n', '')
    if q.find('?') == -1:
        r = q + ' ' + a
        return r

    r = getStatementForOneSentQ(q, a)
    return r.lstrip();


if __name__ == "__main__":
    qa_file = codecs.open(QA_FILE_PATH, "r", "utf-8")
    result_file = codecs.open(RESULT_FILE_PATH, "w+", "utf-8")

    print 'Process qa file', QA_FILE_PATH
    first = True
    for line in qa_file:
        splitted = line.split('\t')
        if first:
            first = False
            continue
        num = splitted[0]
        q = splitted[1]
        c = splitted[2]
        a1 = splitted[3]
        a2 = splitted[4]
        a3 = splitted[5]
        a4 = splitted[6]
        s1 = getStatement(q, a1)
        s2 = getStatement(q, a2)
        s3 = getStatement(q, a3)
        s4 = getStatement(q, a4)
        s1 = removeDuplicateWords(s1)
        s2 = removeDuplicateWords(s2)
        s3 = removeDuplicateWords(s3)
        s4 = removeDuplicateWords(s4)
        r = num + '\t' + c + '\t' + s1 + '\t' + s2 + '\t' + s3 + '\t' + s4 + '\n'
        result_file.write(r)
