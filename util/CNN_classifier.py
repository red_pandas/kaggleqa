import pandas
import os
import numpy as np
import math
from sklearn.preprocessing import PolynomialFeatures
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten, Reshape
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.optimizers import RMSprop, SGD, Adagrad, Adadelta, Adam
from sklearn.preprocessing import StandardScaler, LabelEncoder, LabelBinarizer
from sklearn.metrics import mean_squared_error
from sklearn.datasets import make_friedman1
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.ensemble import GradientBoostingClassifier
from keras.layers.advanced_activations import PReLU

def substract(x):
    if np.mean(x) > 2:
        x = x - 1
        for elem in x:
            if elem <0:
                elem=0
    elif np.mean(x) < 0.5:
        x += 1
    return x

def softmax(x):
    if np.max(x) > 30:
        x /=10
    elif np.max(x) > 10:
        x /= 4
    elif np.max(x) > 4:
        x /= 2
    #return x
    else:
        x = x*10
    e_x = np.exp(x)
    e_x_sum = e_x.sum()
    if e_x_sum > 0:
        out = e_x / e_x.sum()
    else:
        out = np.zeros_like(x)
    return out

def hardmax(x):
    index = np.argmax(x)
    out = np.zeros_like(x)
    out[index]=1
    return out

def norm(x):
    #return (norm1(x) + norm2(x)) / 2
    sum_x = x.sum()
    if sum_x > 0:
        out = x / x.sum()
    else:
        out = np.zeros_like(x)
    return out

def norm1(x):
    x= x - np.min(x)
    if np.max(x) > 0:
        return x / np.max(x)
    else:
        return np.zeros_like(x)

def norm2(x):
    sum_x = x.sum()
    if sum_x > 0:
        out = x / x.sum()
    else:
        out = np.zeros_like(x)
    return out

DATA_PATH = '/home/wesee/GitRepo/KaggleAllenAI/lucene_experiments/'
train_paths = ['train_PMI_scores.csv', 'lucene_scores_10_from_100.csv']##,'lucene_scores_ans_q.csv',
               #'glove_scores.csv', 'train_pred_answs_cummulative.csv', 'train_pred_probas_word_to_word.csv',
               #'words_count.csv', 'words_aq_count.csv']
val_paths = ['val_PMI_scores.csv', 'val_lucene_scores_10_from_100.csv']# 'val_lucene_scores_ans_q.csv', 'val_glove_scores.csv',
             #'val_pred_answs_cummulative.csv', 'val_pred_probas_word_to_word.csv', 'val_words_count.csv', 'val_words_aq_count.csv']
abs_train_path = []
for path in train_paths:
    abs_train_path.append(os.path.join(DATA_PATH, path))

abs_val_path = []
for path in val_paths:
    abs_val_path.append(os.path.join(DATA_PATH, path))


TRAINING_SET = os.path.join(DATA_PATH, 'training_set.tsv')
training_set_df = pandas.read_csv(TRAINING_SET,sep='\t', index_col='id')

train_scores = []
for path in abs_train_path:
    scores_df=pandas.read_csv(path, index_col='id')
    train_scores.append(scores_df.values)

val_scores=[]
val_scores_df = []
for path in abs_val_path:
    scores_df=pandas.read_csv(path, index_col='id')
    val_scores_df.append(scores_df)
    val_scores.append(scores_df.values)


def prepare_training_data(scores):
    num_samples = scores[0].shape[0]
    poly = PolynomialFeatures(degree=1)
    score_images = []
    for i in range(num_samples):
        scores_to_append = []
        for score in scores:
            scores_to_append.append(score[i])
        to_append = np.vstack((scores_to_append))
        #to_append = poly.fit_transform(to_append.T).T
        score_images.append(to_append[np.newaxis])
    return np.array(score_images)

X_train = prepare_training_data(train_scores)
X_val = prepare_training_data(val_scores)

label_encoder = LabelBinarizer()
Y_train = label_encoder.fit_transform(training_set_df['correctAnswer'].values)

model = Sequential()
# input: 2x4 images with 1 channels -> (1, 2, 4) tensors.
# this applies 1 convolution filters of size 2x1 each.
model.add(Convolution2D(1, 2, 1, input_shape=(1, 2, 4), activation="relu"))
#model.add(PReLU())
model.add(Flatten())

model.add(Activation('softmax'))

#sgd = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss='categorical_crossentropy', optimizer=Adadelta())

model.fit(X_train, Y_train, batch_size=128, nb_epoch=500, show_accuracy=True, shuffle=True)

predicted_values = model.predict(X_val, batch_size=128)
predictions = label_encoder.inverse_transform(predicted_values)

ids = val_scores_df[0].index.values

with open('result.csv', 'w') as result:
    result.write('id,correctAnswer\n')
    for i, id in enumerate(ids):
        result.write('{0},{1}\n'.format(id, predictions[i]))
