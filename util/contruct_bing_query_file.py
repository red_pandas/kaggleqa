
TRAIN_FILE_PATH = '/home/sokolov/KaggleAllenAI/data/training_set.tsv'
QUERY_FILE = '/home/sokolov/KaggleAllenAI/data/queries.txt'

with open(TRAIN_FILE_PATH, 'r') as val_file:
    val_questions = [x.strip() for x in val_file.readlines()]
    with open(QUERY_FILE, 'w') as query_file:
        for s in val_questions[1:]:
            id, question, right_answer, answerA, answerB, answerC, answerD = s.split('\t')
            query_file.write('site:wikipedia.org' + ' ' + question + '\n')
