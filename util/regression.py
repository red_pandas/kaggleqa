import pandas
import numpy as np

from sklearn import grid_search
from sklearn.preprocessing import LabelBinarizer, LabelEncoder
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import cross_val_score, KFold

TRAINING_SET = '../lucene_experiments/training_set.tsv'
T1 = '../lucene_experiments/train_PMI_scores.csv'
T2 = '../lucene_experiments/train_PMI_scores_0.4372.csv'
T3 = '../lucene_experiments/lucene_scores_10_from_100.csv'
#T3 = './data/train_PMI_scores_0.4372_norm.csv'

T4 = '../lucene_experiments/lucene_scores_best_lucene.csv'
T5 = '../lucene_experiments/train_lucene_scores_q_a_word.csv'
T6 = '../lucene_experiments/lucene_scores_new_index.csv'
T7 = '../lucene_experiments/voting_comitee_classifier.csv'
#VAL_SET = './data/validation_set.tsv'

#F1 = './lucene_experiments/val_PMI_scores.csv'
#F2 = './lucene_experiments/val_PMI_scores_0.4372.csv'
#F3 = './data/train_PMI_scores_0.4372_norm.csv'
#F3 = './lucene_experiments/val_lucene_scores_10_from_100.csv'
#F4 = './lucene_experiments/val_lucene_scores_best_lucene.csv'
#F5 = './lucene_experiments/val_lucene_scores_q_a_word.csv'
#F6 = './lucene_experiments/val_lucene_scores_new_index.csv'

def get_accuracy(scores, labels):
    predicted_labels = np.argmax(scores, axis=1)
    correct_labels = np.count_nonzero(predicted_labels == labels)
    num_samples = labels.shape[0]
    return correct_labels / float(num_samples)

def softmax(x):
    e_x = np.exp(x)
    e_x_sum = e_x.sum()
    if e_x_sum > 0:
        out = e_x / e_x.sum()
    else:
        out = np.zeros_like(x)
    return out

def norm(x):
    sum_x = x.sum()
    if sum_x > 0:
        out = x / x.sum()
    else:
        out = np.zeros_like(x)
    return out

def build_dataset(raw_data, y_train):
    raw = raw_data.values
    nor = np.apply_along_axis(norm, 1, raw)
    sof = np.apply_along_axis(softmax, 1, raw)
    log = np.apply_along_axis(lambda x: np.log(x+1), 1, raw)
    sqr = np.apply_along_axis(lambda x: np.power(x, 2), 1, raw)

    X, Y = np.column_stack((raw[:, 0], nor[:, 0], sof[:, 0], log[:, 0], sqr[:, 0])), y_train[:, 0]

    for i in range(1, 4):
        x, y = np.column_stack((raw[:, i], nor[:, i], sof[:, i], log[:, i], sqr[:, i])), y_train[:, i]
        X, Y = np.vstack((X, x)), np.hstack((Y, y))

    return X, Y

def build_dataset_val(raw_data):
    raw = raw_data.values
    nor = np.apply_along_axis(norm, 1, raw)
    sof = np.apply_along_axis(softmax, 1, raw)
    log = np.apply_along_axis(lambda x: np.log(x+1), 1, raw)
    sqr = np.apply_along_axis(lambda x: np.power(x, 2), 1, raw)

    X = np.column_stack((raw[:, 0], nor[:, 0], sof[:, 0], log[:, 0], sqr[:, 0]))

    for i in range(1, 4):
        x = np.column_stack((raw[:, i], nor[:, i], sof[:, i], log[:, i], sqr[:, i]))
        X = np.vstack((X, x))

    return X

def train_accuracy(score_predict):
    scores_df = pandas.read_csv(TRAINING_SET, sep='\t', index_col='id')
    chunck_size = score_predict.size / 4

    scores_df.answerA = score_predict[0 * chunck_size: 1 * chunck_size]
    scores_df.answerB = score_predict[1 * chunck_size: 2 * chunck_size]
    scores_df.answerC = score_predict[2 * chunck_size: 3 * chunck_size]
    scores_df.answerD = score_predict[3 * chunck_size: 4 * chunck_size]

    scores_df['model_ans'] = np.argmax(scores_df[['answerA', 'answerB', 'answerC', 'answerD']].values, axis=1)
    scores_df['model_ans'] = map(chr, scores_df['model_ans'].values + ord('A'))

    scores_df = scores_df[['model_ans', 'correctAnswer']]

    correct = training_set_df[scores_df.model_ans == scores_df.correctAnswer].shape[0] * 1.0
    print correct * 1.0 / training_set_df.shape[0]

def make_subm(score_predict):
    scores_df = pandas.read_csv(VAL_SET, sep='\t', index_col='id')
    chunck_size = score_predict.size / 4

    scores_df.answerA = score_predict[0 * chunck_size: 1 * chunck_size]
    scores_df.answerB = score_predict[1 * chunck_size: 2 * chunck_size]
    scores_df.answerC = score_predict[2 * chunck_size: 3 * chunck_size]
    scores_df.answerD = score_predict[3 * chunck_size: 4 * chunck_size]

    scores_df[['answerA', 'answerB', 'answerC', 'answerD']].to_csv('result_probs.csv')

    scores_df['model_ans'] = np.argmax(scores_df[['answerA', 'answerB', 'answerC', 'answerD']].values, axis=1)
    scores_df['model_ans'] = map(chr, scores_df['model_ans'].values + ord('A'))

    return scores_df

def train_first_stage(X, Y, clf_, params=dict()):
    Y_pred = np.zeros(X.shape[0])

    for train, test in KFold(X.shape[0], n_folds=10):
        clf = clf_(**params)
        clf.fit(X[train], Y[train])
        Y_pred[test] = clf.predict_proba(X[test])[:, 1]

    print 'mean at fist', np.mean(cross_val_score(clf_(**params), X, Y, cv=10))
    clf = clf_(**params).fit(X, Y)

    return clf, Y_pred


training_set_df = pandas.read_csv(TRAINING_SET, sep='\t', index_col='id')
Y_train = LabelBinarizer().fit_transform(training_set_df['correctAnswer'].values)

t1_df = pandas.read_csv(T1, index_col='id')
t2_df = pandas.read_csv(T2, index_col='id')
t3_df = pandas.read_csv(T3, index_col='id')
t4_df = pandas.read_csv(T4, index_col='id')
t5_df = pandas.read_csv(T5, index_col='id')
t6_df = pandas.read_csv(T6, index_col='id')
t7_df = pandas.read_csv(T7, index_col='id')

binary_labels = LabelEncoder().fit_transform(training_set_df['correctAnswer'].values)
print 't1:', get_accuracy(t1_df.values, binary_labels)
print 't2:', get_accuracy(t2_df.values, binary_labels)
print 't3:', get_accuracy(t3_df.values, binary_labels)
print 't4:', get_accuracy(t4_df.values, binary_labels)
print 't5:', get_accuracy(t5_df.values, binary_labels)
print 't6:', get_accuracy(t6_df.values, binary_labels)
print 't7:', get_accuracy(t7_df.values, binary_labels)


X1, y = build_dataset(t1_df, Y_train)
X2, _ = build_dataset(t2_df, Y_train)
X3, _ = build_dataset(t3_df, Y_train)
X4, _ = build_dataset(t4_df, Y_train)
X5, _ = build_dataset(t5_df, Y_train)
X6, _ = build_dataset(t6_df, Y_train)
X7, _ = build_dataset(t7_df, Y_train)

X_join = np.column_stack([X1,X2, X4,X5,X6, X3, X7])

grid = {'penalty': ['l1', 'l2'], 'C': [0.01, 0.1, 0.5, 1, 10, 100, 1000]}
params = grid_search.GridSearchCV(LogisticRegression(), grid, cv=10, n_jobs=4).fit(X_join, y).best_params_
clf, Y_pred = train_first_stage(X_join, y, LogisticRegression, params)
print 'accuracy', train_accuracy(Y_pred)


# # Apply
#
# val_set_df = pandas.read_csv(VAL_SET, sep='\t', index_col='id')
# f1_df = pandas.read_csv(F1, index_col='id')
# f2_df = pandas.read_csv(F2, index_col='id')
# #f3_df = pandas.read_csv(F3, index_col='id')
# f4_df = pandas.read_csv(F4, index_col='id')
# f5_df = pandas.read_csv(F5, index_col='id')
# f6_df = pandas.read_csv(F6, index_col='id')
#
# f1 = build_dataset_val(f1_df)
# f2 = build_dataset_val(f2_df)
# #f3, _ = build_dataset(f3_df, Y_train)
# f4 = build_dataset_val(f4_df)
# f5 = build_dataset_val(f5_df)
# f6 = build_dataset_val(f6_df)
#
# X_join = np.column_stack([f1, f2, f4, f5, f6])
# Y_pred = clf.predict_proba(X_join)[:, 1]
# df = make_subm(Y_pred)
#
# df[['model_ans']].to_csv('result.csv')
#
#
#
#
# df = df[['model_ans', 'correctAnswer']]
#
# correct = training_set_df[df.model_ans == df.correctAnswer].shape[0] * 1.0
# print '1234', correct * 1.0 / training_set_df.shape[0]

