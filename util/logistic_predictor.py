import pandas
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.cross_validation import cross_val_score

import os
import numpy as np

DATA_PATH = '/home/sokolov/KaggleAllenAI/data'

TRAINING_SET = os.path.join(DATA_PATH, 'training_set.tsv')
PMI_SCORES = os.path.join(DATA_PATH, 'PMI_scores.csv')
LUCENE_SCORES = os.path.join(DATA_PATH, 'lucene_scores.csv')

VAL_PMI_SCORES = os.path.join(DATA_PATH, 'val_PMI_scores.csv')
VAL_LUCENE_SCORES = os.path.join(DATA_PATH, 'val_lucene_scores.csv')

pmi_scores_df = pandas.read_csv(PMI_SCORES, index_col='id')
lucene_scores_df = pandas.read_csv(LUCENE_SCORES, index_col='id')
training_set_df = pandas.read_csv(TRAINING_SET, sep='\t', index_col='id')

val_pmi_scores_df = pandas.read_csv(VAL_PMI_SCORES, index_col='id')
val_lucene_scores_df = pandas.read_csv(VAL_LUCENE_SCORES, index_col='id')

#merged_df = pandas.concat([pmi_scores_df, lucene_scores_df], axis=1)

#print merged_df
PMI_scaler = StandardScaler()
lucene_scaler = StandardScaler()
label_encoder = LabelEncoder()

lucene_scores = lucene_scaler.fit_transform(lucene_scores_df.values)
PMI_scores = PMI_scaler.fit_transform(pmi_scores_df.values)

X = np.hstack((lucene_scores, PMI_scores))
Y = label_encoder.fit_transform(training_set_df['correctAnswer'].values)

logreg = LogisticRegression()

print cross_val_score(logreg, X, Y)

logreg.fit(X, Y)

val_lucene_scores = lucene_scaler.transform(val_lucene_scores_df.values)
val_PMI_scores = PMI_scaler.transform(val_pmi_scores_df.values)
X_val = np.hstack((val_lucene_scores, val_PMI_scores))

answers = label_encoder.inverse_transform(logreg.predict(X_val))
ids = val_lucene_scores_df.index.values

with open('results.csv', 'w') as result:
    result.write('id,correctAnswer\n')
    for i, id in enumerate(ids):
        result.write('{0},{1}\n'.format(id, answers[i]))