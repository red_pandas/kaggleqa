import bs4
from bs4 import BeautifulSoup
from itertools import takewhile
import os
import os.path as osp
import re

UTIL_DIR_PATH = osp.dirname(osp.realpath(__file__))

CK12_CONCEPTS_DIR = osp.join(UTIL_DIR_PATH, '../data/ck12concepts')
OUTPUT_DIR = osp.join(UTIL_DIR_PATH, '../data/lucene_index/ck12concepts')

HTML_DIR = osp.join(CK12_CONCEPTS_DIR, 'OEBPS')

if not osp.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)

invalid_sections = ['Explore More', 'Review', 'Practice']
valid_textbook_html = re.compile('^\d*\.html')

for root, dirs, files in os.walk(HTML_DIR):
    i = 0
    for f in sorted(files):
        if valid_textbook_html.match(f):
            html_path = osp.join(root, f)
            with open(html_path, 'r') as html:
                soup = BeautifulSoup(html)

                h3s = soup('h3')  # find all <h3> elements
                h3_with_h3next = zip(h3s, h3s[1:])
                j = 0
                while j < len(h3_with_h3next):
                    h3, h3next = h3_with_h3next[j]

                    # get elements in between
                    between_it = list(takewhile(lambda el: el is not h3next, h3.nextSiblingGenerator()))

                    # include summary section into current file
                    # if 'Summary' in h3next.text:
                    #     next_h3, next_h3_next = h3_with_h3next[j + 1]
                    #     next_between_it = list(takewhile(lambda el: el is not next_h3_next, next_h3.nextSiblingGenerator()))
                    #     between_it += next_between_it
                    #     j += 2
                    # else:

                    j += 1

                    # don't save text from invalid_sections
                    if not any(x for x in invalid_sections if x in h3.text):
                        i += 1
                        result_string = u''
                        for t in between_it:
                            try:
                                # remove tables. Maybe bad idea
                                if type(t) is bs4.element.Tag:
                                    tags_for_removal = t.findAll(['tr'])
                                    for tag in tags_for_removal:
                                        tag.decompose()
                                result_string += u' '.join([x for x in t.stripped_strings])
                                result_string += '\n'
                            except AttributeError as e:
                                if type(t) is not bs4.element.Comment and t.string != '\n':
                                    print e

                                pass

                        output_file_name = str(i) + '_' + f + '.txt'
                        output_file_path = osp.join(OUTPUT_DIR, output_file_name)
                        with open(output_file_path, 'w') as output_file:
                            output_file.write(result_string.encode('utf-8'))
