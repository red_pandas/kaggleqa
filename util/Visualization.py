import pandas
import numpy as np
import math

from collections import OrderedDict
from sklearn.preprocessing import LabelBinarizer

TRAINING_SET = './lucene_experiments/training_set.tsv'
TRAIN_PMI_SCORES = './lucene_experiments/train_PMI_scores.csv'
TRAIN_LUCENE_SCORES = './lucene_experiments/lucene_scores_best_lucene.csv'
VAL_PMI_SCORES = './lucene_experiments/val_PMI_scores_best.csv'
VAL_LUCENE_SCORES = './lucene_experiments/val_lucene_scores_best_lucene.csv'

def softmax(x):
    x = x * 10
    e_x = np.exp(x - np.min(x))
    e_x_sum = e_x.sum()
    if e_x_sum > 0:
        out = e_x / e_x.sum()
    else:
        out = np.zeros_like(x)
    return out

def norm(x):
    sum_x = x.sum()
    if sum_x > 0:
        out = x / sum_x
    else:
        out = np.zeros_like(x)
    return out

def build_dataset(raw_data, y_train):
    raw_data = raw_data.values
    norm_scores = np.apply_along_axis(norm, 1, raw_data)
    softmax_scores = np.apply_along_axis(softmax, 1, raw_data)

    X, Y = np.column_stack((raw_data[:, 0], norm_scores[:, 0], softmax_scores[:, 0])), y_train[:, 0]

    for i in range(1, 4):
        x, y = np.column_stack((raw_data[:, i], norm_scores[:, i], softmax_scores[:, i])), y_train[:, i]
        X, Y = np.vstack((X, x)), np.hstack((Y, y))

    return X, Y

def correct_incorrect_split(X, Y):
    correct_answers_idxs = (Y == 1)
    X_correct = X[correct_answers_idxs]
    Y_correct = Y[correct_answers_idxs]

    X_incorrect = X[~correct_answers_idxs]
    Y_incorrect = Y[~correct_answers_idxs]

    return X_correct, Y_correct, X_incorrect, Y_incorrect

def add_legend_to_plot(plt):
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(), numpoints=1)

def plot_numpy_hist(hist, bins, plt, color):
    center = (bins[:-1] + bins[1:]) / 2
    width = 0.7 * (bins[1] - bins[0])
    plt.bar(center, hist, align='center', color=color, width=width)

def count_score(hist_correct, hist_incorrect, probability_hist, threshold):
    trimmed_prob_hist = probability_hist - threshold
    trimmed_prob_hist[trimmed_prob_hist < 0] = 0
    trimmed_prob_hist[trimmed_prob_hist > 0] = 1
    cor_raw_pmi = sum(np.multiply(hist_correct, trimmed_prob_hist))
    inc_raw_pmi = sum(np.multiply(hist_incorrect, trimmed_prob_hist))

    print 'correct values', cor_raw_pmi
    print 'incorrect values', inc_raw_pmi
    print 'score', cor_raw_pmi / float(inc_raw_pmi)

pmi_scores_df = pandas.read_csv(TRAIN_PMI_SCORES, index_col='id')
lucene_scores_df = pandas.read_csv(TRAIN_LUCENE_SCORES, index_col='id')
training_set_df = pandas.read_csv(TRAINING_SET, sep='\t', index_col='id')

label_encoder = LabelBinarizer()
Y_train = label_encoder.fit_transform(training_set_df['correctAnswer'].values)

X_pmi, y = build_dataset(pmi_scores_df, Y_train)
X_luc, y = build_dataset(lucene_scores_df, Y_train)

import matplotlib.pyplot as plt

#fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(nrows=2, ncols=1)


num_bins = 100
correct_color = 'blue'
incorrect_color = 'red'

X_correct_pmi, Y_correct_pmi, X_incorrect_pmi, Y_incorrect_pmi = correct_incorrect_split(X_pmi, y)

plt.subplot(911)
plt.xlabel('Raw PMI value')
hist_correct_pmi, bins_correct_pmi, patches = plt.hist(X_correct_pmi[:, 0], num_bins, color=correct_color)

plt.subplot(912)
plt.xlabel('Raw PMI value')
hist_incorrect_pmi, bins_incorrect_pmi, patches = plt.hist(X_incorrect_pmi[:, 0], num_bins, color=incorrect_color)

plt.subplot(913)
plt.xlabel('PMI value reciprocal')
probability_hist = np.divide(hist_correct_pmi, hist_incorrect_pmi)
probability_hist[hist_incorrect_pmi == 0] = 0
plot_numpy_hist(probability_hist, bins_correct_pmi, plt, 'green')
print 'raw PMI'
count_score(hist_correct_pmi, hist_incorrect_pmi, probability_hist, 0.5)

plt.subplot(914)
plt.xlabel('Norm PMI value')
hist_cor_norm_pmi, bins_cor_norm_pmi, patches = plt.hist(X_correct_pmi[:, 1], num_bins, color=correct_color)

plt.subplot(915)
plt.xlabel('Norm PMI value')
hist_inc_norm_pmi, bins_inc_norm_pmi, patches = plt.hist(X_incorrect_pmi[:, 1], num_bins, color=incorrect_color)

plt.subplot(916)
plt.xlabel('Norm PMI value reciprocal')
probability_hist = np.divide(hist_cor_norm_pmi, hist_inc_norm_pmi)
probability_hist[hist_inc_norm_pmi == 0] = 0
plot_numpy_hist(probability_hist, bins_cor_norm_pmi, plt, 'green')
print 'norm PMI'
count_score(hist_cor_norm_pmi, hist_inc_norm_pmi, probability_hist, 0.5)

plt.subplot(917)
plt.xlabel('Softmax PMI value')
hist_cor_softmax_pmi, bins_cor_softmax_pmi, patches = plt.hist(X_correct_pmi[:, 2], num_bins, color=correct_color)

plt.subplot(918)
plt.xlabel('Softmax PMI value')
hist_inc_softmax_pmi, bins_inc_softmax_pmi, patches = plt.hist(X_incorrect_pmi[:, 2], num_bins, color=incorrect_color)

plt.subplot(919)
plt.xlabel('Softmax PMI value reciprocal')
probability_hist = np.divide(hist_cor_softmax_pmi, hist_inc_softmax_pmi)
probability_hist[hist_inc_softmax_pmi == 0] = 0
plot_numpy_hist(probability_hist, bins_cor_softmax_pmi, plt, 'green')
print 'softmax PMI'
count_score(hist_cor_softmax_pmi, hist_inc_softmax_pmi, probability_hist, 0.5)

plt.show()